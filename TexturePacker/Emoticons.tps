<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>6</int>
        <key>texturePackerVersion</key>
        <string>7.0.2</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrQualityLevel</key>
        <uint>3</uint>
        <key>astcQualityLevel</key>
        <uint>2</uint>
        <key>basisUniversalQualityLevel</key>
        <uint>2</uint>
        <key>etc1QualityLevel</key>
        <uint>70</uint>
        <key>etc2QualityLevel</key>
        <uint>70</uint>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>Emoticons-{n1}.tpsheet</filename>
            </struct>
        </map>
        <key>multiPackMode</key>
        <enum type="SettingsBase::MultiPackMode">MultiPackAuto</enum>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0023-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/002a-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0030-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0031-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0032-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0033-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0034-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0035-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0036-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0037-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0038-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0039-fe0f-20e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f004.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f170-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f171-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f17e-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f17f-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f18e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f191.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f192.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f193.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f194.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f195.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f196.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f197.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f198.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f199.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f19a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1e7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ef.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1ef.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1ef.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1e7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ef-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ef-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ef-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ef-1f1f5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1f5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1e7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1f5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f4-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f6-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f7-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f7-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f7-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f7-1f1fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f7-1f1fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1e7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ef.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1ef.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fc-1f1eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fc-1f1f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fd-1f1f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fe-1f1ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fe-1f1f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ff-1f1e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ff-1f1f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ff-1f1fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f201.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f202-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f21a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f22f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f232.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f233.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f234.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f235.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f236.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f237-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f238.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f239.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f23a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f250.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f251.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f300.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f301.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f302.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f303.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f304.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f305.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f306.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f307.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f308.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f309.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f310.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f311.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f312.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f313.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f314.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f315.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f316.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f317.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f318.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f319.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f320.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f321-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f324-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f325-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f326-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f327-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f328-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f329-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32a-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32b-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32c-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f330.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f331.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f332.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f333.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f334.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f335.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f336-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f337.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f338.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f339.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f340.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f341.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f342.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f343.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f344.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f345.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f346.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f347.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f348.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f349.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f350.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f351.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f352.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f353.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f354.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f355.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f356.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f357.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f358.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f359.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f360.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f361.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f362.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f363.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f364.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f365.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f366.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f367.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f368.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f369.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f370.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f371.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f372.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f374.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f375.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f376.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f377.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f378.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f379.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37d-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f380.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f381.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f382.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f383.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f384.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f386.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f387.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f388.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f389.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f390.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f391.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f392.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f396-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f397-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f399-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f39a-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f39b-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f39e-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f39f-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3aa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ab.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ac.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ad.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ae.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ba.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3be.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3bf.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ca.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3cb-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3cc-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3cd-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ce-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3cf.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d4-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d5-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d6-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d7-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d8-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d9-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3da-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3db-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3dc-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3dd-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3de-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3df-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ef.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f3-fe0f-200d-1f308.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f3-fe0f-200d-26a7-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f3-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f4-200d-2620-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f4-e0067-e0062-e0065-e006e-e0067-e007f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f4-e0067-e0062-e0073-e0063-e0074-e007f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f4-e0067-e0062-e0077-e006c-e0073-e007f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f5-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f7-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f400.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f401.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f402.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f403.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f404.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f405.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f406.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f407.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f408-200d-2b1b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f408.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f409.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f410.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f411.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f412.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f413.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f414.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f415-200d-1f9ba.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f415.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f416.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f417.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f418.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f419.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f420.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f421.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f422.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f423.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f424.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f425.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f426.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f427.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f428.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f429.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f430.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f431.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f432.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f433.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f434.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f435.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f436.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f437.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f438.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f439.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43b-200d-2744-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43f-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f440.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f441-fe0f-200d-1f5e8-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f441-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f444.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f445.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f451.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f452.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f453.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f454.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f455.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f456.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f457.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f458.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f459.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f460.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f461.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f462.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f463.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f464.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f465.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f91d-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f91d-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f91d-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f91d-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f91d-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f91d-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f91d-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f91d-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f91d-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f91d-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f91d-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f91d-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f91d-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f91d-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f91d-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f91d-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f91d-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f91d-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f91d-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f91d-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f466-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f467-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f467-200d-1f467.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f467.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f468-200d-1f466-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f468-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f468-200d-1f467-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f468-200d-1f467-200d-1f467.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f468-200d-1f467.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f469-200d-1f466-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f469-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f469-200d-1f467-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f469-200d-1f467-200d-1f467.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f469-200d-1f467.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-2764-fe0f-200d-1f468.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-2764-fe0f-200d-1f48b-200d-1f468.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f466-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f467-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f467-200d-1f467.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f467.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f469-200d-1f466-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f469-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f469-200d-1f467-200d-1f466.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f469-200d-1f467-200d-1f467.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f469-200d-1f467.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2764-fe0f-200d-1f468.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2764-fe0f-200d-1f469.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2764-fe0f-200d-1f48b-200d-1f468.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2764-fe0f-200d-1f48b-200d-1f469.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46f-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46f-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f479.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f480.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f484.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f488.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f489.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f490.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f492.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f493.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f494.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f495.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f496.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f497.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f498.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f499.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ab.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ac.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ad.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ae.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ba.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4be.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4bf.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ca.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4cb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4cc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4cd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ce.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4cf.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4da.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4db.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4dc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4dd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4de.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4df.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ef.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4fd-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f500.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f501.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f502.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f503.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f504.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f505.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f506.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f507.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f508.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f509.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f510.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f511.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f512.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f513.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f514.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f515.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f516.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f517.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f518.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f519.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f520.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f521.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f522.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f523.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f524.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f525.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f526.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f528.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f529.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f530.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f531.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f532.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f533.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f534.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f535.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f536.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f537.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f538.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f539.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f53a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f53b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f53c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f53d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f549-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f54a-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f54b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f54c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f54d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f54e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f550.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f551.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f552.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f553.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f554.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f555.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f556.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f557.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f558.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f559.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f560.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f561.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f562.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f563.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f564.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f565.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f566.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f567.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f56f-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f570-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f573-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-fe0f-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-fe0f-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f576-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f577-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f578-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f579-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f587-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f58a-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f58b-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f58c-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f58d-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5a5-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5a8-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5b1-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5b2-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5bc-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5c2-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5c3-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5c4-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5d1-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5d2-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5d3-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5dc-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5dd-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5de-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5e1-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5e3-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5e8-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5ef-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5f3-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5fa-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f600.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f601.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f602.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f603.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f604.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f605.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f606.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f607.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f608.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f609.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f610.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f611.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f612.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f613.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f614.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f615.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f616.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f617.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f618.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f619.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f620.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f621.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f622.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f623.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f624.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f625.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f626.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f627.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f628.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f629.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62e-200d-1f4a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f630.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f631.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f632.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f633.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f634.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f635-200d-1f4ab.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f635.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f636-200d-1f32b-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f636.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f637.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f638.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f639.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f640.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f641.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f642.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f643.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f644.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f648.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f649.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f681.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f682.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f683.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f684.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f685.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f686.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f687.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f688.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f689.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f690.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f691.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f693.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f694.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f695.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f696.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f697.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f698.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f699.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6aa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ab.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ac.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ad.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ae.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ba.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6be.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6bf.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6cb-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6cc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6cd-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ce-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6cf-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6dd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6de.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6df.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e0-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e1-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e2-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e3-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e4-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e5-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e9-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f0-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f3-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f910.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f911.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f912.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f913.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f914.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f915.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f916.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f917.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f920.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f921.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f922.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f923.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f924.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f925.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f927.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f928.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f929.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93c-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93c-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f940.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f941.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f942.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f943.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f944.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f945.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f947.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f948.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f949.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f950.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f951.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f952.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f953.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f954.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f955.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f956.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f957.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f958.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f959.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f960.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f961.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f962.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f963.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f964.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f965.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f966.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f967.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f968.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f969.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f970.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f971.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f972.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f973.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f974.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f975.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f976.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f978.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f979.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f980.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f981.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f982.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f983.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f984.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f985.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f986.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f987.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f988.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f989.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f990.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f991.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f992.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f993.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f994.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f995.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f996.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f997.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f998.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f999.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9aa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ab.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ac.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ad.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ae.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ba.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9be.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bf.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ca.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9cb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9cc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9cd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ce.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9cf.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f384.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f91d-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f91d-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f91d-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f91d-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f91d-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f384.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f91d-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f91d-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f91d-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f91d-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f91d-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f384.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f91d-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f91d-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f91d-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f91d-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f91d-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f384.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f91d-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f91d-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f91d-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f91d-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f91d-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f384.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f91d-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f91d-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f91d-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f91d-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f91d-200d-1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f9d1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f9d1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f9d1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f9d1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f33e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f373.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f37c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f384.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f393.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f3a4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f3a8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f3eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f3ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f4bb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f4bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f527.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f52c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f680.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f692.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f91d-200d-1f9d1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9af.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9b1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9b2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9b3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9bc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9de-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9de-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9de.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9df-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9df-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9df.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ed.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ee.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ef.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa70.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa71.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa72.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa73.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa74.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa78.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa79.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa7a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa7b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa7c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa80.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa81.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa82.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa83.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa84.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa85.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa86.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa90.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa91.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa92.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa93.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa94.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa95.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa96.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa97.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa98.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa99.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faaa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faab.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faac.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faba.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad8.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae7.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fb-200d-1faf2-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fb-200d-1faf2-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fb-200d-1faf2-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fb-200d-1faf2-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fc-200d-1faf2-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fc-200d-1faf2-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fc-200d-1faf2-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fc-200d-1faf2-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fd-200d-1faf2-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fd-200d-1faf2-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fd-200d-1faf2-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fd-200d-1faf2-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fe-200d-1faf2-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fe-200d-1faf2-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fe-200d-1faf2-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fe-200d-1faf2-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3ff-200d-1faf2-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3ff-200d-1faf2-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3ff-200d-1faf2-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3ff-200d-1faf2-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/203c-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2049-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2122-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2139-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2194-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2195-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2196-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2197-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2198-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2199-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/21a9-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/21aa-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/231a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/231b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2328-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23cf-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23e9.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23eb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23ec.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23ed-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23ee-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23ef-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f1-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f2-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f8-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f9-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23fa-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/24c2-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25aa-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25ab-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25b6-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25c0-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25fb-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25fc-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2600-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2601-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2602-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2603-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2604-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/260e-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2611-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2614.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2615.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2618-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2620-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2622-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2623-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2626-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/262a-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/262e-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/262f-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2638-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2639-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/263a-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2648.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2649.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264d.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2650.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2651.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2652.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2653.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/265f-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2660-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2663-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2665-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2666-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2668-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/267b-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/267e-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/267f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2692-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2693.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2694-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2695-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2696-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2697-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2699-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/269b-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/269c-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26a0-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26a1.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26a7-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26aa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26ab.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26b0-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26b1-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26bd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26be.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26c4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26c5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26c8-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26ce.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26cf-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26d1-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26d3-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26d4.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26e9-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26ea.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f0-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f1-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f2.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f3.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f4-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f5.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f7-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f8-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fb-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fb-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fc-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fc-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fd-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fd-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fe-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fe-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3ff-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3ff-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-fe0f-200d-2640-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-fe0f-200d-2642-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26fa.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2702-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2705.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2708-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2709-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-1f3fb.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-1f3fc.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-1f3fd.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-1f3fe.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-1f3ff.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270f-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2712-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2714-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2716-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/271d-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2721-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2728.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2733-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2734-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2744-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2747-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/274c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/274e.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2753.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2754.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2755.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2757.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2763-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2764-fe0f-200d-1f525.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2764-fe0f-200d-1fa79.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2764-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2795.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2796.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2797.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/27a1-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/27b0.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/27bf.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2934-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2935-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b05-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b06-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b07-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b1b.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b1c.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b50.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b55.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/3030-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/303d-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/3297-fe0f.png</key>
            <key type="filename">C:/Users/raph_/Downloads/emoji-data-master/img-google-64/3299-fe0f.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileLists</key>
        <map type="SpriteSheetMap">
            <key>default</key>
            <struct type="SpriteSheet">
                <key>files</key>
                <array>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0023-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/002a-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0030-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0031-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0032-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0033-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0034-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0035-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0036-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0037-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0038-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/0039-fe0f-20e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f004.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f170-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f171-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f17e-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f17f-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f18e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f191.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f192.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f193.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f194.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f195.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f196.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f197.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f198.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f199.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f19a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e6-1f1ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1e7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ef.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e7-1f1ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e8-1f1ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1ef.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1e9-1f1ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ea-1f1fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1ef.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1eb-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1e7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ec-1f1fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ed-1f1fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ee-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ef-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ef-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ef-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ef-1f1f5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1f5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f0-1f1ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1e7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f1-1f1fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f2-1f1ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1f5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f3-1f1ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f4-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f5-1f1fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f6-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f7-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f7-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f7-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f7-1f1fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f7-1f1fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1e7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ef.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f8-1f1ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1ef.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1f9-1f1ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fa-1f1ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fb-1f1fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fc-1f1eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fc-1f1f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fd-1f1f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fe-1f1ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1fe-1f1f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ff-1f1e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ff-1f1f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f1ff-1f1fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f201.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f202-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f21a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f22f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f232.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f233.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f234.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f235.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f236.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f237-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f238.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f239.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f23a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f250.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f251.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f300.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f301.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f302.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f303.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f304.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f305.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f306.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f307.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f308.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f309.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f30f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f310.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f311.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f312.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f313.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f314.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f315.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f316.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f317.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f318.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f319.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f31f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f320.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f321-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f324-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f325-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f326-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f327-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f328-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f329-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32a-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32b-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32c-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f32f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f330.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f331.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f332.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f333.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f334.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f335.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f336-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f337.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f338.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f339.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f33f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f340.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f341.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f342.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f343.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f344.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f345.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f346.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f347.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f348.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f349.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f34f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f350.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f351.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f352.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f353.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f354.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f355.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f356.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f357.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f358.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f359.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f35f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f360.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f361.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f362.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f363.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f364.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f365.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f366.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f367.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f368.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f369.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f36f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f370.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f371.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f372.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f374.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f375.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f376.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f377.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f378.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f379.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37d-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f37f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f380.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f381.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f382.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f383.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f384.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f385.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f386.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f387.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f388.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f389.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f38f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f390.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f391.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f392.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f396-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f397-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f399-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f39a-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f39b-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f39e-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f39f-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3a9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3aa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ab.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ac.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ad.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ae.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3b9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ba.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3be.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3bf.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3c9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ca.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3cb-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3cc-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3cd-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ce-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3cf.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d4-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d5-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d6-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d7-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d8-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3d9-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3da-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3db-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3dc-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3dd-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3de-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3df-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ef.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f3-fe0f-200d-1f308.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f3-fe0f-200d-26a7-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f3-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f4-200d-2620-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f4-e0067-e0062-e0065-e006e-e0067-e007f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f4-e0067-e0062-e0073-e0063-e0074-e007f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f4-e0067-e0062-e0077-e006c-e0073-e007f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f5-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f7-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f400.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f401.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f402.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f403.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f404.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f405.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f406.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f407.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f408-200d-2b1b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f408.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f409.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f40f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f410.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f411.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f412.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f413.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f414.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f415-200d-1f9ba.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f415.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f416.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f417.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f418.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f419.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f41f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f420.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f421.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f422.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f423.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f424.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f425.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f426.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f427.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f428.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f429.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f42f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f430.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f431.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f432.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f433.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f434.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f435.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f436.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f437.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f438.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f439.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43b-200d-2744-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f43f-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f440.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f441-fe0f-200d-1f5e8-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f441-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f442.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f443.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f444.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f445.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f446.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f447.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f448.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f449.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f44f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f450.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f451.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f452.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f453.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f454.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f455.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f456.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f457.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f458.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f459.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f45f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f460.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f461.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f462.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f463.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f464.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f465.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f467.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f91d-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f91d-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f91d-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f91d-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f91d-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f91d-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f91d-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f91d-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f91d-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f91d-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f91d-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f91d-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f91d-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f91d-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f91d-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f91d-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f91d-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f91d-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f91d-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f91d-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f466-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f467-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f467-200d-1f467.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f467.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f468-200d-1f466-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f468-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f468-200d-1f467-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f468-200d-1f467-200d-1f467.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f468-200d-1f467.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f469-200d-1f466-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f469-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f469-200d-1f467-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f469-200d-1f467-200d-1f467.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f469-200d-1f467.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-2764-fe0f-200d-1f468.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468-200d-2764-fe0f-200d-1f48b-200d-1f468.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f468.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f91d-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f91d-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f91d-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f91d-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f91d-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f468-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f466-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f467-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f467-200d-1f467.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f467.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f469-200d-1f466-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f469-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f469-200d-1f467-200d-1f466.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f469-200d-1f467-200d-1f467.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f469-200d-1f467.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2764-fe0f-200d-1f468.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2764-fe0f-200d-1f469.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2764-fe0f-200d-1f48b-200d-1f468.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469-200d-2764-fe0f-200d-1f48b-200d-1f469.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f469.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46f-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46f-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f46f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f470.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f471.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f472.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f473.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f474.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f475.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f476.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f477.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f478.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f479.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f47f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f480.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f481.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f482.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f483.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f484.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f485.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f486.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f487.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f488.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f489.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f48f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f490.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f491.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f492.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f493.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f494.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f495.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f496.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f497.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f498.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f499.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f49f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4a9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4aa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ab.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ac.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ad.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ae.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4b9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ba.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4be.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4bf.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4c9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ca.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4cb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4cc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4cd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ce.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4cf.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4d9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4da.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4db.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4dc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4dd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4de.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4df.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ef.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4fd-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f4ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f500.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f501.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f502.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f503.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f504.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f505.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f506.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f507.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f508.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f509.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f50f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f510.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f511.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f512.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f513.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f514.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f515.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f516.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f517.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f518.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f519.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f51f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f520.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f521.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f522.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f523.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f524.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f525.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f526.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f528.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f529.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f52f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f530.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f531.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f532.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f533.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f534.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f535.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f536.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f537.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f538.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f539.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f53a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f53b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f53c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f53d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f549-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f54a-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f54b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f54c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f54d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f54e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f550.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f551.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f552.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f553.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f554.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f555.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f556.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f557.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f558.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f559.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f55f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f560.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f561.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f562.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f563.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f564.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f565.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f566.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f567.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f56f-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f570-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f573-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f574-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-fe0f-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-fe0f-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f575-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f576-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f577-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f578-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f579-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f57a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f587-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f58a-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f58b-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f58c-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f58d-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f590-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f595.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f596.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5a5-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5a8-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5b1-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5b2-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5bc-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5c2-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5c3-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5c4-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5d1-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5d2-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5d3-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5dc-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5dd-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5de-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5e1-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5e3-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5e8-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5ef-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5f3-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5fa-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f5ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f600.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f601.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f602.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f603.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f604.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f605.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f606.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f607.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f608.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f609.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f60f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f610.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f611.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f612.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f613.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f614.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f615.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f616.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f617.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f618.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f619.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f61f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f620.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f621.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f622.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f623.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f624.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f625.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f626.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f627.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f628.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f629.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62e-200d-1f4a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f62f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f630.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f631.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f632.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f633.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f634.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f635-200d-1f4ab.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f635.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f636-200d-1f32b-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f636.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f637.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f638.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f639.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f63f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f640.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f641.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f642.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f643.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f644.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f645.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f646.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f647.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f648.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f649.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f64f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f681.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f682.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f683.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f684.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f685.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f686.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f687.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f688.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f689.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f68f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f690.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f691.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f693.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f694.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f695.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f696.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f697.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f698.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f699.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f69f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6a9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6aa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ab.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ac.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ad.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ae.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6b9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ba.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6be.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6bf.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6c5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6cb-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6cc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6cd-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ce-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6cf-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6d7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6dd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6de.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6df.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e0-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e1-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e2-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e3-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e4-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e5-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6e9-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f0-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f3-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f6fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f7f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f90f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f910.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f911.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f912.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f913.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f914.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f915.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f916.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f917.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f918.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f919.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f91f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f920.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f921.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f922.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f923.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f924.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f925.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f926.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f927.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f928.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f929.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f92f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f930.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f931.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f932.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f933.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f934.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f935.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f936.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f937.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f938.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f939.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93c-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93c-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f93f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f940.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f941.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f942.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f943.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f944.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f945.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f947.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f948.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f949.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f94f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f950.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f951.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f952.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f953.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f954.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f955.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f956.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f957.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f958.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f959.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f95f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f960.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f961.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f962.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f963.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f964.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f965.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f966.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f967.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f968.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f969.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f96f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f970.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f971.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f972.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f973.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f974.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f975.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f976.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f977.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f978.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f979.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f97f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f980.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f981.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f982.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f983.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f984.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f985.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f986.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f987.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f988.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f989.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f98f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f990.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f991.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f992.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f993.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f994.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f995.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f996.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f997.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f998.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f999.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f99f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9a9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9aa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ab.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ac.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ad.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ae.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9b9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ba.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9be.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9bf.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9c9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ca.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9cb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9cc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9cd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ce.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9cf.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f384.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f91d-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f91d-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f91d-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f91d-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f91d-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb-200d-2764-fe0f-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f384.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f91d-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f91d-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f91d-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f91d-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f91d-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc-200d-2764-fe0f-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f384.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f91d-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f91d-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f91d-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f91d-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f91d-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd-200d-2764-fe0f-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f384.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f91d-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f91d-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f91d-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f91d-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f91d-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe-200d-2764-fe0f-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f384.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f91d-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f91d-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f91d-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f91d-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f91d-200d-1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f48b-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f9d1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f9d1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f9d1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff-200d-2764-fe0f-200d-1f9d1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f33e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f373.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f37c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f384.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f393.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f3a4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f3a8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f3eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f3ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f4bb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f4bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f527.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f52c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f680.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f692.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f91d-200d-1f9d1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9af.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9b1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9b2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9b3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9bc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-1f9bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1-200d-2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9d9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9da.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9db.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9dd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9de-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9de-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9de.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9df-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9df-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9df.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ed.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ee.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ef.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9f9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1f9ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa70.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa71.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa72.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa73.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa74.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa78.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa79.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa7a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa7b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa7c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa80.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa81.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa82.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa83.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa84.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa85.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa86.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa90.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa91.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa92.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa93.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa94.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa95.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa96.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa97.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa98.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa99.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fa9f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faa9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faaa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faab.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faac.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fab9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faba.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fac5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad8.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fad9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1fae7.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fb-200d-1faf2-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fb-200d-1faf2-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fb-200d-1faf2-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fb-200d-1faf2-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fc-200d-1faf2-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fc-200d-1faf2-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fc-200d-1faf2-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fc-200d-1faf2-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fd-200d-1faf2-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fd-200d-1faf2-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fd-200d-1faf2-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fd-200d-1faf2-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fe-200d-1faf2-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fe-200d-1faf2-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fe-200d-1faf2-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fe-200d-1faf2-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3ff-200d-1faf2-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3ff-200d-1faf2-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3ff-200d-1faf2-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3ff-200d-1faf2-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/1faf6.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/203c-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2049-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2122-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2139-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2194-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2195-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2196-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2197-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2198-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2199-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/21a9-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/21aa-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/231a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/231b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2328-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23cf-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23e9.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23eb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23ec.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23ed-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23ee-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23ef-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f1-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f2-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f8-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23f9-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/23fa-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/24c2-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25aa-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25ab-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25b6-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25c0-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25fb-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25fc-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/25fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2600-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2601-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2602-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2603-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2604-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/260e-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2611-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2614.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2615.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2618-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/261d-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2620-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2622-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2623-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2626-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/262a-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/262e-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/262f-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2638-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2639-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/263a-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2648.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2649.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264d.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/264f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2650.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2651.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2652.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2653.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/265f-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2660-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2663-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2665-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2666-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2668-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/267b-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/267e-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/267f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2692-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2693.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2694-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2695-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2696-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2697-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2699-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/269b-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/269c-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26a0-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26a1.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26a7-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26aa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26ab.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26b0-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26b1-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26bd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26be.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26c4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26c5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26c8-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26ce.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26cf-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26d1-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26d3-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26d4.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26e9-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26ea.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f0-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f1-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f2.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f3.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f4-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f5.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f7-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f8-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fb-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fb-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fc-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fc-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fd-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fd-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fe-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fe-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3ff-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3ff-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-fe0f-200d-2640-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-fe0f-200d-2642-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26f9-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26fa.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/26fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2702-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2705.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2708-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2709-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270a.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270c-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-1f3fb.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-1f3fc.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-1f3fd.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-1f3fe.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-1f3ff.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270d-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/270f-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2712-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2714-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2716-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/271d-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2721-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2728.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2733-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2734-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2744-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2747-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/274c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/274e.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2753.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2754.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2755.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2757.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2763-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2764-fe0f-200d-1f525.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2764-fe0f-200d-1fa79.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2764-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2795.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2796.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2797.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/27a1-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/27b0.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/27bf.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2934-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2935-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b05-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b06-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b07-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b1b.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b1c.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b50.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/2b55.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/3030-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/303d-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/3297-fe0f.png</filename>
                    <filename>C:/Users/raph_/Downloads/emoji-data-master/img-google-64/3299-fe0f.png</filename>
                </array>
            </struct>
        </map>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
