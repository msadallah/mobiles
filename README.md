# MOBILES

## Project Overview
The MOBILES project investigates international students' experiences in urban environments, focusing on their digital practices to understand how they navigate, interact, and adapt to new surroundings. By analyzing their spatial behaviors alongside sensory and material experiences, the project sheds light on the rhythms, social interactions, and emotional aspects of settling in a new city. This interdisciplinary initiative combines insights from sociolinguistics, geography, and computer science to explore experiential learning spaces and support students' cultural, linguistic, and social integration.  

Collaborating with three research labs—ICAR (sociolinguistics and didactics), EVS (geography and geomatics), and LIRIS (computer science)—MOBILES merges diverse perspectives on language socialization, spatial practices, and digital tool usage. The project’s centerpiece is a mobile application that enables students to document and reflect on their experiences, fostering deeper learning through real-world exploration and interaction.  

For a detailed overview of the project, its actions, and associated research, visit the official website : [https://mobiles-projet.huma-num.fr/](https://mobiles-projet.huma-num.fr/).

## MOBILES App
The MOBILES application offers international students the opportunity to collect, document, and enrich their city journeys using GPS technology. They can create personalized routes, both automatically via GPS and manually by drawing their own paths on an interactive map. Users can add detailed annotations, including text, icons, photos, tags, and moods, to express their personal and sensory experiences, reflecting their appropriation of the city. This allows them to create narratives of their settlement, express their impressions, and share their discoveries with others, turning the city into an open journal to personalize and exchange.

## Features
- **User Registration and Account Management**: Simple steps to create a functional account, with options for login, logout, and background usage.
- **Main Interface and Navigation Access**: Explore the main interface to view different map views, search specific locations, use filtering features to display data as per preferences, manage favorites, and utilize the notification center to stay informed about recent activities.
- **Route Creation Options**: Create routes automatically with GPS to record real-time trajectories, or manually draw your own paths on the map.
- **GPS Route Creation**: Utilize tools dedicated to automatic route creation through GPS collection, capturing real-time movements.
- **Manual Route Creation**: Use tools to manually draw routes and points, allowing full flexibility in designing your explorations.
- **Annotations Creation, Sharing, and Management**: Add detailed annotations, including text, icons, photos, tags, and moods. Share annotations with other users or keep them private. Use tags to organize and categorize annotations, personalizing each one to reflect your experience and emotions.

## Installation
### Prerequisites
- Unity version 2021.3.24f1 (If you choose to use a different version, ensure that the application works correctly with that version)
- MySQL
- kTBS instance (for interaction logging)
- Firebase Cloud Messaging and Realtime Database
- API Platform

### Steps

1. **Open the project in Unity**:
   - Launch Unity Hub.
   - Click on `Open` and select the project directory.

2. **Set up the MySQL Database**:
   - Install MySQL if not already installed.
   - Create a new database and import the schema file located in `Docs/mobiles.sql`.

3. **Update Configuration Files**:
   - **Data Management Email**: Update the email address in `Assets/LirisAssets/Scripts/UI/LinkOpener.cs` by replacing `"contact@mail.edu"`.
   - **Localization Settings**: In `Assets/LirisAssets/Scripts/ApplicationManager.cs`, configure the localization file for the default language (French):
     ```cs
     StartCoroutine(L.LoadLocalizationFromCloud("URL_Localisation", _localisationsOnCloud));
     ```
     If cloud localization is not used, the default localization file is located here: `Assets/Resources/Texts/LocalisationMobiles.json`. Also, update the contact email in this file by replacing `"contact@mail.edu"`.

4. **Configure API Platform**:
   - Update the base URL in `Assets/LirisAssets/Scripts/WebServices/WebServiceBase.cs`:
     ```cs
     public const string URL_BASE = "PROJECT_DB_URL";
     ```
     Replace `PROJECT_DB_URL` with your API URL.

5. **Set Up kTBS System**:
   - Configure the kTBS system instance for recording interactions by adding a line with `key=ktbsURL` and the URL value of the instance in the `config` table of the database.
   - The model for kTBS is found in `Docs/mobiles.json`. Without this configuration, the application will function but interaction logs will not be recorded.

6. **Set Up Firebase for Push Notifications**:
   - Follow the instructions on [Firebase Cloud Messaging](https://firebase.google.com/docs/cloud-messaging) for Android and iOS.
   - Create a Firebase project and get the files `google-services.json` (for Android) and `GoogleService-Info.plist` (for iOS), then place them in the `Assets` folder.
   - Tokens are saved in the user table, column `devices`.
   - Messages are sent to a specific device using the token associated with the connected user (`user_id`).
   - Firebase dependencies need to be installed and a new module is created in the application.
   - Necessary permissions must be granted by the user for notifications.

## Usage
### Building and Testing the Application

1. **Build the Project**:
   - Go to `File > Build Settings`.
   - Select your target platform (Android, iOS, Windows, etc.).
   - Click `Switch Platform` if necessary.
   - Click `Build and Run` to compile your project.

2. **Testing on an Emulator or Device**:
   - For Android: Use Android Studio or a physical device to run the APK.
   - For iOS: Use Xcode or a physical device to run the IPA.

3. **Creating the APK (Android)**:
   - Ensure the Android platform is selected in `Build Settings`.
   - Click `Build` and choose a destination folder for your APK.
   - Unity will generate an APK file that you can install on an Android device.

4. **Creating the IPA (iOS)**:
   - Ensure the iOS platform is selected in `Build Settings`.
   - Click `Build` and choose a destination folder for your IPA.
   - Unity will generate an IPA file that you can install on an iOS device.

5. **Debugging and Optimization**:
   - Use Unity's debugging tools to identify and fix any errors.
   - Optimize the application to improve performance and reduce file size.


## Contribution
Contributions are welcome! Please reach out to us if you are interested in contributing to this project.

## License
This project is licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/).

## Acknowledgements
This project is supported by the ANR under grant number ANR-20-CE38-0009. We thank the members of the partner laboratories for their contributions to the project's conception and ideas.


### Contact
- Marie Lefevre - [Send a mail](marie.lefevre@liris.cnrs.fr)
- Raphaël Benedetto: [Send a mail]( raphael.benedetto.pro@gmail.com)
- Madjid Sadallah:  [Send a mail](madjid.sadallah@liris.cnrs.fr)

---

If you have any questions or need assistance, please contact the project members via their respective emails.
