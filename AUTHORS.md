# AUTHORS

This file lists all the individuals who have contributed to the development of the MOBILES project.

## Principal Investigators
- **Marie Lefevre** - Development Project Leader
- **Raphaël Benedetto** - Principal Developer for the mobile application in Unity
- **Madjid Sadallah** - Contributor to the application and responsible for the implementation of the tracking and recommendation module

## Institutional Support
- **LIRIS, CNRS** - Lyon Research Center for Images and Intelligent Information Systems

## Funding
This project is supported by the ANR (Agence Nationale de la Recherche, France) under grant number ANR-20-CE38-0009.

## Additional Acknowledgements 
While the development was led and executed by members of the LIRIS, CNRS, we gratefully acknowledge the valuable contributions and insights from our partner laboratories. Members from ICAR and EVS have played an essential role in shaping the project's conception and ideas. Their collaborative efforts have significantly enriched the project. 

| **EVS**                      | **ICAR**                  |
|------------------------------|---------------------------|
| Claire Cunty                 | Jean-François Grassin     |
| Hélène Mathian               | Félix Danos               |
| Dominique Chevalier          | Justine Lascar            |
| Thierry Joliveau             |                           |
| Camille Scheffler            |                           |
