﻿namespace AllMyScripts.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    using AllMyScripts.Common.Tools;
    //! @sealed class MultiSet
    //!
    //! @brief	Multiset (set accepting multiple occurences of the same element)
    public sealed class MultiSet<T>
	{
		public enum Policy
		{
			Append,
			Prepend
		}

		//! @class Enumerator
		//!
		//!	@brief	Enumerator structure of this collection
		public class Enumerator : IEnumerator<T>
		{
			//! Accessor to the current item of the collection
			public T Current
			{
				get{ return m_currentItem; }
			}

			object IEnumerator.Current
			{
				get{ return Current; }
			}

			//! Construct an enumerator based on the given collection
			//!
			//!	@param	collection	collection we enumerates
			public Enumerator( MultiSet<T> collection )
			{
				GlobalTools.Assert( collection!=null );
				m_collection = collection;
				Reset();
			}

			//! Move to the next item of the collection
			//!
			//!	@return true if the next item exists, false otherwise
			public bool MoveNext()
			{
				++m_nCurrentIndex;
				if( m_nCurrentIndex>=m_collection.Count )
				{
					return false;
				}
				else
				{
					m_currentItem = m_collection.m_internalList[m_nCurrentIndex];
					return true;
				}
			}

			//!	Reset the enumerator to its initial position
			public void Reset()
			{
				m_nCurrentIndex = -1;
				m_currentItem = default( T );
			}

			void IDisposable.Dispose() {}

	#region Private
		#region Attributes
			private MultiSet<T> m_collection;
			private int m_nCurrentIndex;
			private T m_currentItem;
		#endregion
	#endregion
		}
	
		// Constructors
	
		// Construct a multi set with default values
		public MultiSet() : this( Policy.Append, null, null )
		{
		
		}

		// Construct a multi set with default values
		//!
		//!	@param	nCapacity	capacity of the collection
		public MultiSet( int nCapacity ) : this( nCapacity, Policy.Append, null, null )
		{

		}

		// Construct a multi set using a policy
		//!
		//!	@param	policy	policy to apply in case of multiple occurences
		public MultiSet( Policy policy ) : this( policy, null, null )
		{

		}

		// Construct a multi set using a policy
		//!
		//!	@param	nCapacity	capacity of the collection
		//!	@param	policy	policy to apply in case of multiple occurences
		public MultiSet( int nCapacity, Policy policy ) : this( nCapacity, policy, null, null )
		{

		}

		//Construct a multi set using a comparer instance
		//!
		//!	@param	comparer	Comparer instance to use
		public MultiSet( IComparer<T> comparer ) : this( Policy.Append, null, comparer )
		{
		
		}

		//Construct a multi set using a comparer instance
		//!
		//!	@param	nCapacity	capacity of the collection
		//!	@param	comparer	Comparer instance to use
		public MultiSet( int nCapacity, IComparer<T> comparer ) : this( nCapacity, Policy.Append, null, comparer )
		{

		}

		//Construct a multi set using a comparer instance
		//!
		//!	@param	policy		policy to apply in case of multiple occurences
		//!	@param	comparer	Comparer instance to use
		public MultiSet( Policy policy, IComparer<T> comparer ) : this( policy, null, comparer )
		{

		}

		//Construct a multi set using a comparer instance
		//!
		//!	@param	nCapacity	capacity of the collection
		//!	@param	policy		policy to apply in case of multiple occurences
		//!	@param	comparer	Comparer instance to use
		public MultiSet( int nCapacity, Policy policy, IComparer<T> comparer ) : this( nCapacity, policy, null, comparer )
		{

		}

		// Construct a multi set with an existing collection
		//!
		//!	@param	enumerable	collection source
		public MultiSet( IEnumerable<T> enumerable ) : this( Policy.Append, enumerable, null )
		{
		
		}

		// Construct a multi set with an existing collection
		//!
		//!	@param	nCapacity	capacity of the collection
		//!	@param	enumerable	collection source
		public MultiSet( int nCapacity, IEnumerable<T> enumerable ) : this( nCapacity, Policy.Append, enumerable, null )
		{

		}

		// Construct a multi set with an existing collection
		//!
		//!	@param	policy		policy to apply in case of multiple occurences
		//!	@param	enumerable	collection source
		public MultiSet( Policy policy, IEnumerable<T> enumerable ) : this( policy, enumerable, null )
		{

		}

		// Construct a multi set with an existing collection
		//!
		//!	@param	nCapacity	capacity of the collection
		//!	@param	policy		policy to apply in case of multiple occurences
		//!	@param	enumerable	collection source
		public MultiSet( int nCapacity, Policy policy, IEnumerable<T> enumerable ) : this( nCapacity, policy, enumerable, null )
		{

		}

		// Construct a multi set with an existing collection using a comparer instance
		//!
		//!	@param	enumerable	collection source
		//!	@param	comparer	Comparer instance to use
		public MultiSet( IEnumerable<T> enumerable, IComparer<T> comparer ) : this( Policy.Append, enumerable, comparer )
		{
		}

		// Construct a multi set with an existing collection using a comparer instance
		//!
		//!	@param	nCapacity	capacity of the collection
		//!	@param	enumerable	collection source
		//!	@param	comparer	Comparer instance to use
		public MultiSet( int nCapacity, IEnumerable<T> enumerable, IComparer<T> comparer ) : this( nCapacity, Policy.Append, enumerable, comparer )
		{
		}

		// Construct a multi set with an existing collection using a comparer instance
		//!
		//!	@param	policy		policy to apply in case of multiple occurences
		//!	@param	enumerable	collection source
		//!	@param	comparer	Comparer instance to use
		public MultiSet( Policy policy, IEnumerable<T> enumerable, IComparer<T> comparer )
		{
			m_policy = policy;
			m_internalList = new List<T>();
			m_comparer = comparer ?? Comparer<T>.Default;

			if(enumerable != null)
			{
				IEnumerator<T> enumerator = enumerable.GetEnumerator();
				while( enumerator.MoveNext() )
				{
					Add( enumerator.Current );
				}
				enumerator.Dispose();
			}
		}

		// Construct a multi set with an existing collection using a comparer instance
		//!
		//!	@param	nCapacity	capacity of the collection
		//!	@param	policy		policy to apply in case of multiple occurences
		//!	@param	enumerable	collection source
		//!	@param	comparer	Comparer instance to use
		public MultiSet( int nCapacity, Policy policy, IEnumerable<T> enumerable, IComparer<T> comparer )
		{
			m_policy = policy;
			m_internalList = new List<T>( nCapacity );
			m_comparer = comparer==null? Comparer<T>.Default : comparer;

			if(enumerable != null)
			{
				IEnumerator<T> enumerator = enumerable.GetEnumerator();
				while( enumerator.MoveNext() )
				{
					Add( enumerator.Current );
				}
				enumerator.Dispose();
			}
		}

		// Properties

		//! Access to the comparer used
		public IComparer<T> Comparer
		{
			get{ return m_comparer; }
		}

		//! Access to the number of elements in the collection
		public int Count
		{
			get{ return m_internalList.Count; }
		}

		// Methods

		//! Add an item into the collection
		//!
		//!	@param	item	the item to add
		//!
		//!	@return true if the element has been added, false otherwise
		public bool Add( T item )
		{
			int nInsertIndex = FindIndex( item );
			if( nInsertIndex<m_internalList.Count && m_comparer.Compare( item, m_internalList[nInsertIndex] )==0 && object.Equals( item, m_internalList[nInsertIndex] ) )
			{
				return false;
			}
			else
			{
				m_internalList.Insert( nInsertIndex, item );
				return true;
			}
		}

		//!	Add items into the collection
		//!
		//!	@param	itemCollection
		//!
		//!	@return	true if at least one item has been added, false otherwise
		public bool AddRange( IEnumerable<T> itemCollection )
		{
			bool bHasAtLeastOneItemAdded = false;
			IEnumerator<T> enumerator = itemCollection.GetEnumerator();
			while(enumerator.MoveNext())
			{
				bHasAtLeastOneItemAdded = Add(enumerator.Current)  ||  bHasAtLeastOneItemAdded;
			}
			enumerator.Dispose();
			return bHasAtLeastOneItemAdded;
		}

		//!	Clear the collection
		public void Clear()
		{
			m_internalList.Clear();
		}

		//!	Check if the collection contains a given item
		//!
		//!	@param	item	element to check
		//!
		//!	@return true if the element is contained inside the collection, false otherwise
		public bool Contains( T item )
		{
			int nIndex = FindIndex( item );
			return nIndex  < m_internalList.Count  &&  m_comparer.Compare( m_internalList[nIndex], item )==0 && object.Equals( item,  m_internalList[nIndex] );
		}

		//!	Creates an enumerator for the collection
		//!
		//!	@return the enumerator created
		public Enumerator GetEnumerator()
		{
			return new Enumerator( this );
		}

		//!	Copy the collection into a given array
		//!
		//!	@param	arrayArray	destination array (must be sized properly)
		public void CopyTo( T[] arrayArray )
		{
			m_internalList.CopyTo( arrayArray, 0 );
		}

		//!	Copy the collection into a given array from a specific index
		//!
		//!	@param	arrayArray	destination array (must be sized properly)
		//!	@param	nIndex		starting index of the elements to copy
		public void CopyTo( T[] arrayArray, int nIndex )
		{
			m_internalList.CopyTo( arrayArray, nIndex );
		}

		//!	Copy the collection into a given array from a specific index and for a specific number of elements
		//!
		//!	@param	arrayArray	destination array (must be sized properly)
		//!	@param	nIndex		starting index of the elements to copy
		//!	@param	nCount		number of elements to copy
		public void CopyTo( T[] arrayArray, int nIndex, int nCount )
		{
			m_internalList.CopyTo( nIndex, arrayArray, 0, nCount );
		}

		//!	Remove an element from the collection
		//!
		//!	@param	item	the element to remove
		//!
		//!	@return true if the element has been removed, false otherwise
		public bool Remove( T item )
		{
			int nRemoveIndex = FindIndex( item );
			if( nRemoveIndex<m_internalList.Count && m_comparer.Compare( m_internalList[nRemoveIndex], item )==0 && object.Equals( m_internalList[nRemoveIndex], item ) )
			{
				m_internalList.RemoveAt( nRemoveIndex );
				return true;
			}
			else
			{
				return false;
			}
		}

		//! Find an item in the collection based on a predicate
		//!
		//!	@param	predicate	predicate to find the element
		//!
		//!	@return	the element if found, asserts if not found
		public T Find( System.Func<T, bool> predicate )
		{
			GlobalTools.Assert(predicate != null);

			int nIndex = 0;
			while( nIndex<m_internalList.Count  &&  predicate( m_internalList[nIndex] )==false )
			{
				++nIndex;
			}

			if( nIndex<m_internalList.Count )
			{
				return m_internalList[nIndex];
			}
			else
			{
				GlobalTools.Assert(false);
				return default(T);
			}
		}

		//!	Creates a new array from the collection
		//!
		//!	@return the new array
		public T[] ToArray()
		{
			return m_internalList.ToArray();
		}

	#region Private
		#region Methods
		private int FindIndex( T item )
		{
			int nMaxIndex = m_internalList.Count;
			int nMinIndex = 0;

			while( nMinIndex<nMaxIndex )
			{
				int nMidIndex = ( nMaxIndex+nMinIndex )/2;

				int nCompareValue = m_comparer.Compare( item, m_internalList[nMidIndex] );
				GlobalTools.Assert( m_comparer.Compare( m_internalList[nMidIndex], item )==-nCompareValue, "Comparer used for lwSortedSet is not consistent with data." );
				if( nCompareValue<0 )
				{
					nMaxIndex = nMidIndex;
				}
				else if( nCompareValue>0 )
				{
					nMinIndex = nMidIndex+1;
				}
				else
				{
					if( object.Equals( item, m_internalList[nMidIndex] ) )
					{
						nMinIndex = nMidIndex;
						nMaxIndex = nMidIndex;
					}
					else
					{
						bool elementFound = false;

						// check previous elements with same comparison value
						int previousIndex = nMidIndex - 1;
						while( previousIndex>=nMinIndex && m_comparer.Compare( item, m_internalList[previousIndex] )==0 && elementFound==false )
						{
							elementFound = object.Equals( item, m_internalList[previousIndex] );
							--previousIndex;
						}
						++previousIndex;	// increment it to point to the first element (for insertion purposes)

						if( elementFound )
						{
							nMinIndex = previousIndex;
							nMaxIndex = previousIndex;
						}
						else
						{
							// check next element with same comparison value
							int nextIndex = nMidIndex + 1;
							while( nextIndex<nMaxIndex && m_comparer.Compare( item, m_internalList[nextIndex] )==0 && elementFound==false )
							{
								elementFound = object.Equals( item, m_internalList[nextIndex] );
								++nextIndex;
							}

							if( elementFound )
							{
								nMinIndex = nextIndex-1;
								nMaxIndex = nextIndex-1;
							}
							else
							{
								switch( m_policy )
								{
									case Policy.Append:
									{
										nMinIndex = nextIndex;
										nMaxIndex = nextIndex;
									}
									break;
									case Policy.Prepend:
									{
										nMinIndex = previousIndex;
										nMaxIndex = previousIndex;
									}
									break;
									default:
									{
										GlobalTools.Assert(false);
										nMinIndex = nextIndex;
										nMaxIndex = nextIndex;
									}
									break;
								}
							}
						}
					}
				}
			}

			GlobalTools.Assert( nMinIndex==nMaxIndex );
			return nMinIndex;
		}
		#endregion

		#region Attributes
		private Policy m_policy;
		private List<T> m_internalList;
		private IComparer<T> m_comparer;
		#endregion
	#endregion
	}
}