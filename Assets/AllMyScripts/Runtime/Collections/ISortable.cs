﻿namespace AllMyScripts.Collections
{ 
    using System;
    using UnityEngine;
    public interface ISortable
    {
	    IComparable GetValueToCompareBy(string id);
	    GameObject GetComparedGameObject();
    }
}