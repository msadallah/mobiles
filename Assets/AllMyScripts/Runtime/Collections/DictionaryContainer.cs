﻿namespace AllMyScripts.Collections
{
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;

	public class DictionaryContainer<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerable, IReadOnlyCollection<KeyValuePair<TKey, TValue>>, IReadOnlyDictionary<TKey, TValue>
	{
		private Dictionary<TKey,TValue> _dictionary;

		public DictionaryContainer(Dictionary<TKey, TValue> dictionary)
		{
			UpdateDatas(dictionary);
		}

		public void UpdateDatas(Dictionary<TKey, TValue> dictionary)
		{
			_dictionary = dictionary;
		}

		public bool isNull { get { return (this == null || this._dictionary == null); } }
		public bool isNullOrEmpty { get { return (isNull || this._dictionary.Count < 1); } }


		public bool ContainsKey(TKey key) { return _dictionary.ContainsKey(key); }

		public bool ContainsValue(TValue value) { return _dictionary.ContainsValue(value); }

		public TValue this[TKey key]
		{
			get { return _dictionary[key]; }
		}

		private Dictionary<string, List<string>> _factorListFromHardwareTypeID = new Dictionary<string, List<string>>();

		public bool FactorListFromHardwareTypeIDContains(string hardwareTypeID) { return _factorListFromHardwareTypeID.ContainsKey(hardwareTypeID); }

		public List<string> GetFactorListFromHardwareTypeID(string hardwareTypeID) { return _factorListFromHardwareTypeID[hardwareTypeID]; }



		public int Count => ((_dictionary != null) ? _dictionary.Count : 0);

		public IEnumerable<TKey> Keys => ((IReadOnlyDictionary<TKey, TValue>)_dictionary).Keys;

		public IEnumerable<TValue> Values => ((IReadOnlyDictionary<TKey, TValue>)_dictionary).Values;

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			return _dictionary.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _dictionary.GetEnumerator();
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			return ((IReadOnlyDictionary<TKey, TValue>)_dictionary).TryGetValue(key, out value);
		}

		public Dictionary<TKey, TValue> CreateAndGetDictionaryCloneOfData()
		{
			Dictionary <TKey, TValue> clone = new Dictionary<TKey, TValue>(_dictionary.Count);
			foreach (KeyValuePair<TKey, TValue> kvp in _dictionary)
			{
				clone.Add(kvp.Key, kvp.Value);
			}
			return clone;
		}
	}
}