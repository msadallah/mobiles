namespace AllMyScripts.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using AllMyScripts.Common.Tools;

    //! @sealed class SortedMultiList
    //!
    //! @brief	MultiList (sorted list accepting multiple occurences of the same key)
    public sealed class SortedMultiList<t_Key, t_Value>
	{
		//! Add policy
		public enum Policy
		{
			Append,
			Prepend
		}

		//! @class Enumerator
		//!
		//!	@brief	Enumerator structure of this collection
		public class Enumerator : IEnumerator<KeyValuePair<t_Key, t_Value>>
		{
			//! Accessor to the current item of the collection
			public KeyValuePair<t_Key, t_Value> Current
			{
				get { return m_currentItem; }
			}

			object IEnumerator.Current
			{
				get { return Current; }
			}

			//! Construct an enumerator based on the given collection
			//!
			//!	@param	collection	collection we enumerates
			public Enumerator( SortedMultiList<t_Key, t_Value> collection )
			{
				GlobalTools.Assert( collection != null );
				m_collection = collection;
				Reset();
			}

			//! Move to the next item of the collection
			//!
			//!	@return true if the next item exists, false otherwise
			public bool MoveNext()
			{
				++m_nCurrentIndex;
				if( m_nCurrentIndex >= m_collection.Count )
				{
					return false;
				}
				else
				{
					m_currentItem = m_collection.m_internalList[m_nCurrentIndex];
					return true;
				}
			}

			//!	Reset the enumerator to its initial position
			public void Reset()
			{
				m_nCurrentIndex = -1;
				m_currentItem = default ( KeyValuePair<t_Key, t_Value> );
			}

			void IDisposable.Dispose() {}

			#region Private

			#region Attributes

			private SortedMultiList<t_Key, t_Value> m_collection;
			private int m_nCurrentIndex;
			private KeyValuePair<t_Key, t_Value> m_currentItem;

			#endregion

			#endregion
		}

		// Constructors

		// Construct a sorted multi list with default values
		public SortedMultiList() : this( Policy.Append, null, null ) {}

		// Construct a sorted multi list using a policy
		//!
		//!	@param	policy	policy to apply in case of multiple occurences
		public SortedMultiList( Policy policy ) : this( policy, null, null ) {}

		//Construct a sorted multi list using a comparer instance
		//!
		//!	@param	comparer	Comparer instance to use
		public SortedMultiList( IComparer<t_Key> comparer ) : this( Policy.Append, null, comparer ) {}

		//Construct a sorted multi list using a comparer instance
		//!
		//!	@param	policy		policy to apply in case of multiple occurences
		//!	@param	comparer	Comparer instance to use
		public SortedMultiList( Policy policy, IComparer<t_Key> comparer ) : this( policy, null, comparer ) {}

		// Construct a sorted multi list with an existing collection
		//!
		//!	@param	enumerable	collection source
		public SortedMultiList( IEnumerable<KeyValuePair<t_Key, t_Value>> enumerable ) : this( Policy.Append, enumerable, null ) {}

		// Construct a sorted multi list with an existing collection
		//!
		//!	@param	policy		policy to apply in case of multiple occurences
		//!	@param	enumerable	collection source
		public SortedMultiList( Policy policy, IEnumerable<KeyValuePair<t_Key, t_Value>> enumerable ) : this( policy, enumerable, null ) {}

		// Construct a sorted multi list with an existing collection using a comparer instance
		//!
		//!	@param	enumerable	collection source
		//!	@param	comparer	Comparer instance to use
		public SortedMultiList( IEnumerable<KeyValuePair<t_Key, t_Value>> enumerable, IComparer<t_Key> comparer ) : this( Policy.Append, enumerable, comparer ) {}

		// Construct a sorted multi list with an existing collection using a comparer instance
		//!
		//!	@param	policy		policy to apply in case of multiple occurences
		//!	@param	enumerable	collection source
		//!	@param	comparer	Comparer instance to use
		public SortedMultiList( Policy policy, IEnumerable<KeyValuePair<t_Key, t_Value>> enumerable, IComparer<t_Key> comparer )
		{
			m_policy = policy;
			m_internalList = new List<KeyValuePair<t_Key, t_Value>>();
			m_comparer = comparer == null ? Comparer<t_Key>.Default : comparer;

			if( enumerable != null )
			{
				IEnumerator<KeyValuePair<t_Key, t_Value>> enumerator = enumerable.GetEnumerator();
				while( enumerator.MoveNext() )
				{
					Add( enumerator.Current );
				}
				enumerator.Dispose();
			}
		}

		// Properties

		//! Access to the comparer used
		public IComparer<t_Key> Comparer
		{
			get { return m_comparer; }
		}

		//! Access to the number of elements in the collection
		public int Count
		{
			get { return m_internalList.Count; }
		}

		//! Access an item in the list
		public KeyValuePair<t_Key, t_Value> this[ int nIndex ]
		{
			get { return m_internalList[nIndex]; }
		}

		//! Access a list of all keys
		public List<t_Key> Keys
		{
			get { return m_internalList.ConvertAll( item => item.Key ); }
		}

		//! Access a list of all values
		public List<t_Value> Values
		{
			get { return m_internalList.ConvertAll( item => item.Value ); }
		}

		// Methods

		//! Add an item into the collection (items must be unique given the comparator used)
		//!
		//!	@param	item key to add
		//!	@param	item value to add
		//!
		//!	@return true if the element has been added, false otherwise
		public bool Add( t_Key key, t_Value value )
		{
			return Add( new KeyValuePair<t_Key, t_Value>( key, value ) );
		}

		//! Add an item into the collection (items must be unique given the comparator used)
		//!
		//!	@param	item	the item to add
		//!
		//!	@return true if the element has been added, false otherwise
		public bool Add( KeyValuePair<t_Key, t_Value> item )
		{
			int nInsertIndex = FindIndex( item );
			if( nInsertIndex < m_internalList.Count &&
				m_comparer.Compare( item.Key, m_internalList[nInsertIndex].Key ) == 0 &&
				object.Equals( item.Value, m_internalList[nInsertIndex].Value ) )
			{
				return false;
			}
			else
			{
				m_internalList.Insert( nInsertIndex, item );
				return true;
			}
		}

		//!	Clear the collection
		public void Clear()
		{
			m_internalList.Clear();
		}

		//!	Check if the collection contains a given item
		//!
		//!	@param	item	element to check
		//!
		//!	@return true if the element is contained inside the collection, false otherwise
		public bool Contains( KeyValuePair<t_Key, t_Value> item )
		{
			int nIndex = FindIndex( item );
			return nIndex < m_internalList.Count &&
					m_comparer.Compare( m_internalList[nIndex].Key, item.Key ) == 0 &&
					object.Equals( item.Value, m_internalList[nIndex].Value );
		}

		//!	Creates an enumerator for the collection
		//!
		//!	@return the enumerator created
		public Enumerator GetEnumerator()
		{
			return new Enumerator( this );
		}

		//! Get an element at index
		//!
		//!	@param	nIndex	index of the element to get
		//!
		//!	@return the element at given index
		public KeyValuePair<t_Key, t_Value> GetItem( int nIndex )
		{
			GlobalTools.Assert( nIndex >= 0 && nIndex < m_internalList.Count );
			return m_internalList[nIndex];
		}

		//!	Copy the collection into a given array
		//!
		//!	@param	arrayArray	destination array (must be sized properly)
		public void CopyTo( KeyValuePair<t_Key, t_Value>[] arrayArray )
		{
			m_internalList.CopyTo( arrayArray, 0 );
		}

		//!	Copy the collection into a given array from a specific index
		//!
		//!	@param	arrayArray	destination array (must be sized properly)
		//!	@param	nIndex		starting index of the elements to copy
		public void CopyTo( KeyValuePair<t_Key, t_Value>[] arrayArray, int nIndex )
		{
			m_internalList.CopyTo( arrayArray, nIndex );
		}

		//!	Copy the collection into a given array from a specific index and for a specific number of elements
		//!
		//!	@param	arrayArray	destination array (must be sized properly)
		//!	@param	nIndex		starting index of the elements to copy
		//!	@param	nCount		number of elements to copy
		public void CopyTo( KeyValuePair<t_Key, t_Value>[] arrayArray, int nIndex,
							int nCount )
		{
			m_internalList.CopyTo( nIndex, arrayArray, 0, nCount );
		}

		//!	Remove an element from the collection
		//!
		//!	@param	item	the element to remove
		//!
		//!	@return true if the element has been removed, false otherwise
		public bool Remove( KeyValuePair<t_Key, t_Value> item )
		{
			int nRemoveIndex = FindIndex( item );
			if( nRemoveIndex < m_internalList.Count &&
				m_comparer.Compare( m_internalList[nRemoveIndex].Key, item.Key ) == 0 &&
				object.Equals( m_internalList[nRemoveIndex].Value, item.Value ) )
			{
				m_internalList.RemoveAt( nRemoveIndex );
				return true;
			}
			else
			{
				return false;
			}
		}

		//! Remove the element at index from the collection
		//!
		//!	@param	nIndex	index of the element to remove
		public void RemoveAt( int nIndex )
		{
			GlobalTools.Assert( nIndex >= 0 && nIndex < m_internalList.Count );
			m_internalList.RemoveAt( nIndex );
		}

		//!	Remove an element from the collection from its value
		//!
		//!	@param	itemValue	the item value to remove
		//!
		//!	@return true if the element has been removed, false otherwise
		public bool RemoveValue( t_Value itemValue )
		{
			int nListIndex = 0;
			while( nListIndex < m_internalList.Count &&
					object.Equals( m_internalList[nListIndex].Value, itemValue ) == false )
			{
				++nListIndex;
			}

			if( nListIndex < m_internalList.Count )
			{
				m_internalList.RemoveAt( nListIndex );
				return true;
			}
			else
			{
				return false;
			}
		}

		//! Remove n elements starting at index from the collection
		//!
		//!	@param	nIndex	index of the first element to remove
		//!	@param	nCount	number of elements to remove
		public void RemoveRange( int nIndex, int nCount )
		{
			GlobalTools.Assert( nIndex >= 0 );
			GlobalTools.Assert( nCount > 0 );
			GlobalTools.Assert( nIndex + nCount <= m_internalList.Count );
			m_internalList.RemoveRange( nIndex, nCount );
		}

		//!	Creates a new array from the collection
		//!
		//!	@return the new array
		public KeyValuePair<t_Key, t_Value>[] ToArray()
		{
			return m_internalList.ToArray();
		}

		#region Private

		#region Methods

		private int FindIndex( KeyValuePair<t_Key, t_Value> item )
		{
			int nMaxIndex = m_internalList.Count;
			int nMinIndex = 0;

			while( nMinIndex < nMaxIndex )
			{
				int nMidIndex = ( nMaxIndex + nMinIndex ) / 2;

				int nCompareValue = m_comparer.Compare( item.Key,
														m_internalList[nMidIndex].Key );
				GlobalTools.Assert( m_comparer.Compare( m_internalList[nMidIndex].Key, item.Key ) == -nCompareValue, "Comparer used for SortedSet is not consistent with data." );
				if( nCompareValue < 0 )
				{
					nMaxIndex = nMidIndex;
				}
				else if( nCompareValue > 0 )
				{
					nMinIndex = nMidIndex + 1;
				}
				else
				{
					if( object.Equals( item.Value, m_internalList[nMidIndex].Value ) )
					{
						nMinIndex = nMidIndex;
						nMaxIndex = nMidIndex;
					}
					else
					{
						bool bIsElementFound = false;

						// check previous elements with same comparison value
						int nPreviousIndex = nMidIndex - 1;
						while( nPreviousIndex >= nMinIndex &&
								m_comparer.Compare( item.Key, m_internalList[nPreviousIndex].Key ) == 0 &&
								bIsElementFound == false )
						{
							bIsElementFound = object.Equals( item.Value,
															m_internalList[nPreviousIndex].Value );
							--nPreviousIndex;
						}
						++nPreviousIndex;
							// increment it to point to the first element (for insertion purposes)

						if( bIsElementFound )
						{
							nMinIndex = nPreviousIndex;
							nMaxIndex = nPreviousIndex;
						}
						else
						{
							// check next element with same comparison value
							int nNextIndex = nMidIndex + 1;
							while( nNextIndex < nMaxIndex &&
									m_comparer.Compare( item.Key, m_internalList[nNextIndex].Key ) == 0 &&
									bIsElementFound == false )
							{
								bIsElementFound = object.Equals( item.Value,
																m_internalList[nNextIndex].Value );
								++nNextIndex;
							}

							if( bIsElementFound )
							{
								nMinIndex = nNextIndex - 1;
								nMaxIndex = nNextIndex - 1;
							}
							else
							{
								switch( m_policy )
								{
									case Policy.Append:
									{
										nMinIndex = nNextIndex;
										nMaxIndex = nNextIndex;
									}
										break;
									case Policy.Prepend:
									{
										nMinIndex = nPreviousIndex;
										nMaxIndex = nPreviousIndex;
									}
										break;
									default:
									{
										GlobalTools.Assert( false );
										nMinIndex = nNextIndex;
										nMaxIndex = nNextIndex;
									}
										break;
								}
							}
						}
					}
				}
			}

			GlobalTools.Assert( nMinIndex == nMaxIndex );
			return nMinIndex;
		}

		#endregion

		#region Attributes

		private Policy m_policy;
		private List<KeyValuePair<t_Key, t_Value>> m_internalList;
		private IComparer<t_Key> m_comparer;

		#endregion

		#endregion
	}
}