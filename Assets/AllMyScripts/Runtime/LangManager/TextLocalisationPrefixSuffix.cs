﻿namespace AllMyScripts.Common.UI
{
    using System.Text;

    public class TextLocalisationPrefixSuffix : TextLocalisation
    {
        public string prefix, suffix;
        protected override string GetText()
        {
            return new StringBuilder()
                .Append(prefix)
                .Append(base.GetText())
                .Append(suffix)
                .ToString();
        }
    }
}