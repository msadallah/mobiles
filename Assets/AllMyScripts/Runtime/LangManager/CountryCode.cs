namespace AllMyScripts.LangManager
{
    using UnityEngine;

    using System.Collections.Generic;

    public sealed class Country
	{
		public SystemLanguage m_eLang = SystemLanguage.Unknown;
		public string m_sLanguageISO6391 = string.Empty;
		public string m_sThreeLetterISOLanguageName = string.Empty;
		public string m_sLanguageCulture = string.Empty;
		public string m_sNativeLanguageName = string.Empty;
		public string m_sEnglishLanguageName = string.Empty;
		public bool m_bIsLatin = true;

		public Country( SystemLanguage eLang, string sLanguageISO6391, string sThreeLetterISOLanguageName, string sLanguageCulture, string sNativeLanguageName, 
			string sEnglishLanguageName, bool bIsLatin )
		{
			m_eLang = eLang;
			m_sLanguageISO6391 = sLanguageISO6391;
			m_sThreeLetterISOLanguageName = sThreeLetterISOLanguageName;
			m_sLanguageCulture = sLanguageCulture;
			m_sNativeLanguageName = sNativeLanguageName;
			m_sEnglishLanguageName = sEnglishLanguageName;
			m_bIsLatin = bIsLatin;
		}
	}

	public sealed class CountryCode
	{
		private const string LANGS_PATH = "Assets/Resources/Texts/Langs.txt";

		private static readonly Country[] s_countries =
		{
			new Country( SystemLanguage.German,             "de", "deu", "de-DE", "Deutsch",       "German",       true ),
			new Country( SystemLanguage.English,            "en", "eng", "en-US", "English",       "English",      true ),
			new Country( SystemLanguage.Spanish,            "es", "spa", "es-ES", "Español",       "Spanish",      true ),
			new Country( SystemLanguage.French,             "fr", "fra", "fr-FR", "Français",      "French",       true ),
			new Country( SystemLanguage.Italian,            "it", "ita", "it-IT", "Italiano",      "Italian",      true ),
			new Country( SystemLanguage.Japanese,           "ja", "jpn", "ja-JP", "日本語",           "Japanese",     false ),
			new Country( SystemLanguage.Korean,             "ko", "kor", "ko-KR", "한국의",           "Korean",       false ),
			new Country( SystemLanguage.Dutch,              "nl", "nld", "nl-NL", "Nederlands",    "Dutch",        true ),
			new Country( SystemLanguage.Polish,             "pl", "pol", "pl-PL", "Polski",        "Polish",       true ),
			new Country( SystemLanguage.Portuguese,         "pt", "por", "pt-PT", "Português",     "Portuguese",   true ),
			new Country( SystemLanguage.Russian,            "ru", "rus", "ru-RU", "Pусский",       "Russian",      false ),
			new Country( SystemLanguage.Swedish,            "sv", "swe", "sv-SE", "Svenska",       "Swedish",      true ),
			new Country( SystemLanguage.Turkish,            "tr", "tur", "tr-TR", "Türkçe",        "Turkish",      true ),
			new Country( SystemLanguage.Chinese,            "zh", "zho", "zh-CN", "中文",            "Chinese",      false ),
			new Country( SystemLanguage.ChineseSimplified,  "zh", "zho", "zh-CN", "中文",            "Chinese",      false ),
			new Country( SystemLanguage.ChineseTraditional, "zh", "zho", "zh-CN", "中文",            "Chinese",      false ),
			new Country( SystemLanguage.Hungarian,          "hu", "hun", "hu-HU", "Magyar",        "Hungarian",    true ),
			new Country( SystemLanguage.Romanian,           "ro", "ron", "ro-RO", "Română",        "Romanian",     true ),
			new Country( SystemLanguage.Czech,              "cs", "ces", "cs-CZ", "Čeština",       "Czech",        true ),
			new Country( SystemLanguage.Portuguese,         "pt", "por", "pt-BR", "Português BR",  "Brazilian",    true ),
			new Country( SystemLanguage.Slovak,             "sk", "slk", "sk-SK", "Slovenčina",    "Slovak",       true ),
			new Country( SystemLanguage.Finnish,            "fi", "fin", "fi-FI", "Suomi",         "Finnish",      true ),
			new Country( SystemLanguage.Norwegian,          "nn", "nno", "nn-NO", "Norsk",         "Norwegian",    true ),
			new Country( SystemLanguage.Danish,             "da", "dan", "da-DK", "Dansk",         "Danish",       true ),
			new Country( SystemLanguage.Greek,              "el", "ell", "el-GR", "ελληνικά",      "Greek",        false )
		};

		// Font countries are fake latin country + non latin languages
		public static Country[] GetFontCountries()
		{
			List<Country> fontCountries = new List<Country>();
			fontCountries.Add( new Country( SystemLanguage.Unknown, "LN", "LTN", "Latin", "Latin", "Latin", true ) );
			for ( int i = 0 ; i < s_countries.Length ; i++ )
			{
				if ( !s_countries[ i ].m_bIsLatin )
				{
					fontCountries.Add( s_countries[ i ] );
				}
			}
			return fontCountries.ToArray();
		}

		public static Country[] GetAllCountries()
		{
			return s_countries;
		}

		public static Country GetCountry( string sLanguageCulture )
		{
			for ( int i = 0 ; i < s_countries.Length ; i++ )
			{
				if ( s_countries[ i ].m_sLanguageCulture == sLanguageCulture )
				{
					return s_countries[ i ];
				}
			}
			return null;
		}

		public static Country GetCountryFromISO6391( string sId )
		{
			for ( int i = 0 ; i < s_countries.Length ; i++ )
			{
				if ( s_countries[ i ].m_sLanguageISO6391 == sId )
				{
					return s_countries[ i ];
				}
			}
			return null;
		}

		public static Country GetCountry( SystemLanguage eLang )
		{
			for ( int i = 0 ; i < s_countries.Length ; i++ )
			{
				if ( s_countries[ i ].m_eLang == eLang )
				{
					return s_countries[ i ];
				}
			}
			return null;
		}

		public static SystemLanguage GetSystemLanguage( string sLanguageCulture )
		{
			Country country = GetCountry( sLanguageCulture );
			return ( country != null ) ? country.m_eLang : SystemLanguage.Unknown;
		}

		public static SystemLanguage GetSystemLanguageFromISO6391( string sId )
		{
			Country country = GetCountryFromISO6391( sId );
			return ( country != null ) ? country.m_eLang : SystemLanguage.Unknown;
		}

		public static string GetLanguageCulture( SystemLanguage eLang )
		{
			Country country = GetCountry( eLang );
			return ( country != null ) ? country.m_sLanguageCulture : string.Empty;
		}

		public static string GetLanguageISO6391( SystemLanguage eLang )
		{
			Country country = GetCountry( eLang );
			return ( country != null ) ? country.m_sLanguageISO6391 : string.Empty;
		}

		public static bool IsLatinLanguage( SystemLanguage eLang )
		{
			Country country = GetCountry( eLang );
			return ( country != null ) ? country.m_bIsLatin : true;
		}

		#region Writing the languages
#if UNITY_EDITOR
		// return the first language culture that is not in CountryCode
		public static string CheckUnhandledLanguage( List<string> sLanguageCultures )
		{
			if ( sLanguageCultures != null )
			{

				for ( int i = 0 ; i < sLanguageCultures.Count ; i++ )
				{
					Country country = GetCountry( sLanguageCultures[ i ] );
					if ( country == null )
						return sLanguageCultures[ i ];
				}
			}
			return string.Empty;
		}

		public static bool WriteLangs( List<string> sLanguageCultures, out string sError )
		{
			bool bError = false;
			sError = string.Empty;
			try
			{
				string sUnhandledLanguage = CheckUnhandledLanguage( sLanguageCultures );
				if ( !string.IsNullOrEmpty( sUnhandledLanguage ) )
				{
					sError = sUnhandledLanguage + " is not a language handled in Country";
					bError = true;
				}
				else
				{
					System.IO.StreamWriter sw = new System.IO.StreamWriter( LANGS_PATH );
					sw.Write( string.Join( "\n", sLanguageCultures.ToArray() ) );
					sw.Close();

					UnityEditor.AssetDatabase.Refresh();
				}
			}
			catch ( System.IO.IOException )
			{
				sError = "Unable to write to '" + LANGS_PATH + "'.";
				bError = true;
			}

			return !bError;
		}
#endif
		#endregion
	}
}