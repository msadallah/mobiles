namespace AllMyScripts.LangManager
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using AllMyScripts.Common.Tools;
	using AllMyScripts.Common.Utils;

	public interface ITextLocalizer
    {
        void OnLanguageChanged();
    }
    /// <summary>
    /// Localisation tool
    /// </summary>
    public static class L
    {
        /// <summary>
        /// Current chosen language
        /// </summary>
        public static int CurrentLanguageIdx { private set; get; }

        /// <summary>
        /// Lists the found languages
        /// </summary>
        public static List<Country> AvailableLanguages { private set; get; }

        /// <summary>
        /// Lists the found ids
        /// </summary>
        private static List<string> AvailableIds;

        /// <summary>
        /// Localisation dictionary
        /// </summary>
        private static Dictionary<string, string> Data;

        public static List<ITextLocalizer> OnLanguageChanged = new List<ITextLocalizer>();

        public static string CultureISOCode
        {
            get
            {
                return AvailableLanguages[CurrentLanguageIdx].m_sThreeLetterISOLanguageName;
            }
        }

        /// <summary>
        /// Creates the localisation module
        /// </summary>
        static L()
        {
            CurrentLanguageIdx = -1;
            AvailableLanguages = new List<Country>();
            AvailableIds = new List<string>();
            Data = new Dictionary<string, string>();

            TextAsset languageListAsset = Resources.Load<TextAsset>("Texts/Langs");
            if (languageListAsset == null)
            {
                Debug.LogError("Language Manager : the file 'Langs' has not been found in any 'Resources/Texts/' folder ! We set at least on language : english");
                AvailableLanguages.Add(CountryCode.GetCountry(SystemLanguage.English));
            }
            else
            {
                string[] sLanguageCodes = languageListAsset.text.Split(new string[] { "\r\n", "\n" }, System.StringSplitOptions.RemoveEmptyEntries);
                for (int nLanguageCodeIndex = 0; nLanguageCodeIndex < sLanguageCodes.Length; ++nLanguageCodeIndex)
                {
                    Country countryStructure = CountryCode.GetCountry(sLanguageCodes[nLanguageCodeIndex]);
                    if (countryStructure != null)
                    {
                        AvailableLanguages.Add(countryStructure);
                    }
                }
            }

            SetDefaultLanguage();
        }

        public static void SetDefaultLanguage()
        {
            UnityEngine.Assertions.Assert.IsTrue(AvailableLanguages.Count > 0, "Localisation : no available language registered.");

            // Look for system language and if the language is not available, switch to the first english language.
            // If there is no english language, switch to the first available language.

            int nSystemLanguageIndex = -1;
            int nEnglishLanguageIndex = -1;

            int nLanguageIndex = 0;
            while (nLanguageIndex < AvailableLanguages.Count && nSystemLanguageIndex < 0)
            {
                if (AvailableLanguages[nLanguageIndex].m_eLang == Application.systemLanguage)
                {
                    nSystemLanguageIndex = nLanguageIndex;
                }
                else
                {
                    if (AvailableLanguages[nLanguageIndex].m_eLang == SystemLanguage.English && nEnglishLanguageIndex < 0)
                    {
                        nEnglishLanguageIndex = nLanguageIndex;
                    }
                    ++nLanguageIndex;
                }
            }

            if (nSystemLanguageIndex >= 0)
            {
                SetLanguage(nSystemLanguageIndex);
            }
            else if (nEnglishLanguageIndex >= 0)
            {
                SetLanguage(nEnglishLanguageIndex);
            }
            else
            {
                SetLanguage(0);
            }
        }

        /// <summary>
        /// Return true if the language is enabled
        /// </summary>
        /// <param name="sLanguageCode">Code of the language to set.</param>
        public static bool IsLanguageEnabled(string sLanguageCode)
        {
            int nLanguageIndex = AvailableLanguages.FindIndex(item => item.m_sLanguageISO6391 == sLanguageCode);
            return nLanguageIndex >= 0 && nLanguageIndex < AvailableLanguages.Count;
        }

        /// <summary>
        /// Sets the language.
        /// </summary>
        /// <param name="sLanguageCode">Code of the language to set.</param>
        public static void SetLanguage(string sLanguageCode)
        {
            int nLanguageIndex = AvailableLanguages.FindIndex(item => item.m_sLanguageISO6391 == sLanguageCode);
            GlobalTools.AssertFormat(nLanguageIndex >= 0 && nLanguageIndex < AvailableLanguages.Count, "Language '{0}' not found in available languages.", sLanguageCode);
            SetLanguage(nLanguageIndex);
        }

        /// <summary>
        /// Sets the language.
        /// </summary>
        /// <param name="systemLanguage">System language.</param>
        public static void SetLanguage(SystemLanguage systemLanguage)
        {
            int nLanguageIndex = AvailableLanguages.FindIndex(item => item.m_eLang == systemLanguage);
            GlobalTools.AssertFormat(nLanguageIndex >= 0 && nLanguageIndex < AvailableLanguages.Count, "Language '{0}' not found in available languages.", systemLanguage);
            SetLanguage(nLanguageIndex);
        }


        /// <summary>
        /// Sets the language.
        /// </summary>
        /// <param name="Country">System language.</param>
        public static void SetLanguage(Country systemLanguage)
        {
            int nLanguageIndex = AvailableLanguages.FindIndex(item => item == systemLanguage);
            GlobalTools.AssertFormat(nLanguageIndex >= 0 && nLanguageIndex < AvailableLanguages.Count, "Language '{0}' not found in available languages.", systemLanguage);
            SetLanguage(nLanguageIndex);
        }

        public static void SetLanguage(int nLanguageIdx)
        {
            CurrentLanguageIdx = nLanguageIdx;
            for (int i = 0; i < OnLanguageChanged.Count; i++)
                OnLanguageChanged[i].OnLanguageChanged();
        }

        public static void AddTextLocalizerListener(ITextLocalizer toLocalize)
        {
            OnLanguageChanged.Add(toLocalize);
        }
        public static void RemoveTextLocalizerListener(ITextLocalizer toLocalize)
        {
            OnLanguageChanged.Remove(toLocalize);
        }

        /// <summary>
        /// Loads localisation settings from a JSON file
        /// </summary>
        /// <param name="txt">File containing localisation strings</param>
        public static void LoadLocalisationFile(TextAsset txt)
        {
            List<string> jsonStrings = new List<string>();
            jsonStrings.Add(txt.text);
            LoadLocalisation(jsonStrings);
        }

        public static void LoadLocalisationFile(List<TextAsset> listTexts)
        {
            List<string> jsonStrings = new List<string>();
            foreach (TextAsset ta in listTexts)
            {
                jsonStrings.Add(ta.text);
            }

            LoadLocalisation(jsonStrings);
        }

        public static IEnumerator LoadLocalizationFromCloud(string urlBase, List<string> files)
        {
            List<string> result = new List<string>();
            foreach (var file in files)
            {
                yield return CloudResources.LoadResourceFromCloud(urlBase, file, (string json) =>
                {
                    result.Add(json);
                });
            }
            yield return null;
            if (result.Count > 0)
                L.LoadLocalisation(result);
        }

        /// <summary>
        /// Loads localisation settings from a JSON string
        /// </summary>
        /// <param name="jsonFiles">List of JSON containing localisation strings</param>
        public static void LoadLocalisation(List<string> jsonFiles)
        {


#if VERBOSE_LOCALISATION
		DLog.Log("Localisation : " + json);
#endif

            foreach (string json in jsonFiles)
            {

                object root = JSON.Deserialize(json);

                if (root is Dictionary<string, object>)
                {
                    // There shoul be only one key here but we don't wnat to assume it's name
                    foreach (KeyValuePair<string, object> kvpRoot in (root as Dictionary<string, object>))
                    {
                        IList dataMerge = kvpRoot.Value as IList;

                        if (dataMerge is IList)
                        {
                            foreach (object localized_item_o in (IList)dataMerge)
                            {
                                if (localized_item_o is Dictionary<string, object>)
                                {
                                    Dictionary<string, object> localized_item_ht = localized_item_o as Dictionary<string, object>;
                                    if (localized_item_ht.ContainsKey("ID"))
                                    {
                                        object id_o = DicTools.GetValue(localized_item_ht, "ID");
                                        if (id_o is string)
                                        {
                                            string id = id_o as string;
                                            foreach (KeyValuePair<string, object> kvp in localized_item_ht)
                                            {
                                                string language = kvp.Key;

                                                if (language != "ID")
                                                {
                                                    if (kvp.Value is string)
                                                    {
                                                        string value = kvp.Value as string;

                                                        if (language != null && value != null)
                                                            SetData(language.ToLower(), id, value);
                                                    }
                                                    else
                                                        Debug.LogWarning("Localisation error : value (" + kvp.Key + ") not string");
                                                }
                                            }
                                        }
                                        else
                                            Debug.LogWarning("Localisation error : ID not string");
                                    }
                                    else
                                        Debug.LogWarning("Localisation error : missing ID");
                                }
                                else
                                    Debug.LogWarning("Localisation error : item not Hashtable");
                            }
                        }
                        else
                            Debug.LogWarning("Localisation error : root not list");

                    }
                }
            }

#if VERBOSE_LOCALISATION
		DLog.Log("Parsing complete");
#endif
        }

        //		Hashtable localized_item_ht = localized_item_o as Hashtable;
        //		if (localized_item_ht.ContainsKey("Id"))
        //		{
        //			object id_o = localized_item_ht["Id"];
        //			if (id_o is string)
        //			{
        //				string id = id_o as string;
        //
        //				if (localized_item_ht.ContainsKey("Localisation"))
        //				{
        //					object localisation_list = localized_item_ht["Localisation"];
        //					if (localisation_list is IList)
        //					{
        //						foreach (object localisation_item_o in (IList)localisation_list)
        //						{
        //							if (localisation_item_o is Hashtable)
        //							{
        //								Hashtable localisation_item_ht = localisation_item_o as Hashtable;
        //
        //								string language = null;
        //								string value = null;
        //
        //								if (localisation_item_ht.ContainsKey("Language"))
        //								{
        //									object language_o = localisation_item_ht["Language"];
        //									if (language_o is string)
        //										language = language_o as string;
        //									else
        //										DLog.LogWarning("Localisation error : Language not string");
        //								}
        //								else
        //									DLog.LogWarning("Localisation error : missing Language");
        //
        //								if (localisation_item_ht.ContainsKey("Value"))
        //								{
        //									object value_o = localisation_item_ht["Value"];
        //									if (value_o is string)
        //										value = value_o as string;
        //									else
        //										DLog.LogWarning("Localisation error : Value not string");
        //								}
        //								else
        //									DLog.LogWarning("Localisation error : missing Value");
        //
        //								if (language != null && value != null)
        //									SetData(language, id, value);
        //							}
        //							else
        //								DLog.LogWarning("Localisation error : Localisation item not hashtable");
        //						}
        //					}
        //					else
        //						DLog.LogWarning("Localisation error : Localisation not list");
        //				}
        //				else
        //					DLog.LogWarning("Localisation error : missing Localisation");
        //			}
        //			else
        //				DLog.LogWarning("Localisation error : Id not string");
        //		}
        //		else
        //			DLog.LogWarning("Localisation error : missing Id");

        /// <summary>
        /// Check whether an id exists or not
        /// </summary>
        /// <param name="id">Id to check</param>
        /// <returns>True if the id has been found</returns>
        public static bool IsIdSetted(string id)
        {
            return AvailableIds.Contains(id.Replace(".", "_"));
        }

        /// <summary>
        /// Add a new id
        /// </summary>
        /// <param name="id">id to add</param>
        public static void AddId(string id)
        {
            if (!IsIdSetted(id))
                AvailableIds.Add(id);
        }

        /// <summary>
        /// Check whether a translation has been done for an id in a given language 
        /// </summary>
        /// <param name="lang">language to check</param>
        /// <param name="id">Id to check</param>
        /// <returns>true if the translation exists</returns>
        public static bool IsDataSetted(string lang, string id)
        {
            return Data.ContainsKey(GetDataKey(lang, id));
        }

        /// <summary>
        /// Returns the key used for the given lang/id pair
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private static string GetDataKey(string lang, string id)
        {
            return lang.ToLower() + ':' + id.ToLower();
        }

        /// <summary>
        /// Sets the translation of id in the given language
        /// </summary>
        /// <param name="lang">Language to set</param>
        /// <param name="id">Id to set</param>
        /// <param name="val">Translation</param>
        public static void SetData(string lang, string id, string val)
        {
            if (!IsIdSetted(id))
                AddId(id);

            if (lang != null)
            {
                if (!IsDataSetted(lang, id))
                    Data.Add(GetDataKey(lang, id), val);

                Data[GetDataKey(lang, id)] = val;
#if VERBOSE_LOCALISATION
			DLog.Log(lang + ' ' + id + ' ' + val);
#endif
            }
        }

        /// <summary>
        /// Returns the translation of id in the given language
        /// </summary>
        /// <param name="lang">Language to get</param>
        /// <param name="id">Id to get</param>
        /// <returns>Translation</returns>
        private static string GetData(Country lang, string id)
        {
            id = id.Replace(".", "_");

            if (!IsIdSetted(id))
            {
                AddId(id);
            }

            string data = "[[" + id + "]]";
            string langCode = lang.m_sLanguageISO6391;
            if (IsDataSetted(langCode, id))
            {
                string newData = Data[GetDataKey(langCode, id)].Trim();
#if NOLOC_KEY
            if( !string.isNullOrEmpty( newData ) )
				data = newData.Replace("\\\n", "\n");
#else
                // In this case we use english by default
                if ((string.IsNullOrEmpty(newData) || newData == "INSERT TEXT") && lang.m_eLang != SystemLanguage.English)
                    return GetData(CountryCode.GetCountry(SystemLanguage.English), id);
                else if (!string.IsNullOrEmpty(newData))
                {
                    data = newData.Replace("\\\n", "\n");
                }
#endif
            }
            return data;
        }

        /// <summary>
        /// Returns the translation of id in the current language
        /// </summary>
        /// <param name="id">Id to get</param>
        /// <returns>Translation</returns>
        public static string Get(string id)
        {
            return GetData(AvailableLanguages[CurrentLanguageIdx], id);
        }

        /// <summary>
        /// Check if this key exist in the available IDs
        /// </summary>
        /// <param name="id">id to check</param>
        /// <returns>True if found, false otherwise</returns>
        public static bool HasKey(string id)
        {
            if (string.IsNullOrEmpty(id)) return false;
            id = id.Replace(".", "_");
            return AvailableIds.Contains(id);
        }

        public static List<char> GetAllCharacters()
        {
            List<char> charList = new List<char>();
            foreach (var kv in Data)
            {
                string sVal = kv.Value;
                foreach (char s in sVal)
                {
                    if (!charList.Contains(s))
                        charList.Add(s);
                }
            }
            return charList;
        }

        /// <summary>
        /// Gets the current language.
        /// </summary>
        /// <value>The current language.</value>
        public static Country CurrentLanguage
        {
            get { return AvailableLanguages[CurrentLanguageIdx]; }
        }

        /// <summary>
        /// Gets the language at specified index.
        /// </summary>
        /// <param name="nLanguageIndex">Index.</param>
        public static Country GetLanguagateAt(int nLanguageIndex)
        {
            GlobalTools.Assert(nLanguageIndex >= 0 && nLanguageIndex < AvailableLanguages.Count);
            return AvailableLanguages[nLanguageIndex];
        }

        /// <summary>
        /// Gets the amount of available languages
        /// </summary>
        /// <value>The language count.</value>
        public static int nLanguageCount
        {
            get { return AvailableLanguages.Count; }
        }

    }
}