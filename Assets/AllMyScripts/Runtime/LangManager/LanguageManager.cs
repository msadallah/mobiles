﻿namespace AllMyScripts.LangManager
{
    using UnityEngine;

    using System;
    using System.Collections.Generic;

    using AllMyScripts.Common.Tools;

    /// <summary>
    /// Manages the language available in a game
    /// Load and parse text files to register all keys and their localized text for each language.
    /// /!\ Text files must be retrieved from resource paths.
    /// /!\ Text files are csv formated list where the first column is the localization key and the second column is :
    /// 	- the localized text for texts (the key must start with 'str_')
    /// 	- the name of asset array and the path of the asset in the asset array separated by a '|' (the key must not start with 'str_')
    /// </summary>
    public class LanguageManager : Singleton<LanguageManager>
	{
		public interface ILanguageChanged { void OnLanguageChanged(); }
		public InterfaceDelegate<ILanguageChanged> m_onLanguageChangedEvent = new InterfaceDelegate<ILanguageChanged>();

		public enum TimeUnitTextId
		{
			Days,
			Day,
			Hours,
			Hour,
			Minutes,
			Minute,
			Seconds,
			Second,
		}
		public static readonly int timeUnitTextIdCount = System.Enum.GetValues( typeof( TimeUnitTextId ) ).Length;

		/// <summary>
		/// Gets the index of the current language among all available languages.
		/// </summary>
		/// <value>The index of the current language.</value>
		public int nCurrentLanguageIndex
		{
			get
			{
				GlobalTools.Assert( m_nLanguageIndex >= 0 && m_nLanguageIndex < m_availableLanguages.Length );
				return m_nLanguageIndex;
			}
		}

		/// <summary>
		/// Gets the current language.
		/// </summary>
		/// <value>The current language.</value>
		public Country currentLanguage
		{
			get
			{
				GlobalTools.Assert( m_nLanguageIndex >= 0 && m_nLanguageIndex < m_availableLanguages.Length );
				return m_availableLanguages[ m_nLanguageIndex ];
			}
		}

		/// <summary>
		/// Gets the language at specified index.
		/// </summary>
		/// <param name="nLanguageIndex">Index.</param>
		public Country this[ int nLanguageIndex ]
		{
			get
			{
				GlobalTools.Assert( nLanguageIndex >= 0 && nLanguageIndex < m_availableLanguages.Length );
				return m_availableLanguages[ nLanguageIndex ];
			}
		}

		/// <summary>
		/// Gets the amount of available languages
		/// </summary>
		/// <value>The language count.</value>
		public int nLanguageCount
		{
			get { return m_availableLanguages.Length; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="LanguageManager"/> class.
		/// </summary>
		public LanguageManager()
		{
			m_availableLanguages = new Country[ 0 ];
			m_sTextFilesPerLanguage = new HashSet<string>[ 0 ];

			TextAsset languageListAsset = Resources.Load<TextAsset>( "Texts/Langs" );
			if ( languageListAsset == null )
			{
				Debug.LogError( "Language Manager : the file 'Langs' has not been found in any 'Resources/Texts/' folder !" );
			}
			else
			{
				string[] sLanguageCodes = languageListAsset.text.Split( new string[] { "\r\n", "\n" }, System.StringSplitOptions.RemoveEmptyEntries );
				List<Country> availableLanguages = new List<Country>();
				for ( int nLanguageCodeIndex = 0 ; nLanguageCodeIndex < sLanguageCodes.Length ; ++nLanguageCodeIndex )
				{
					Country countryStructure = CountryCode.GetCountry( sLanguageCodes[ nLanguageCodeIndex ] );
					if ( countryStructure != null )
					{
						availableLanguages.Add( countryStructure );
					}
				}

				m_availableLanguages = availableLanguages.ToArray();
				m_sTextFilesPerLanguage = new HashSet<string>[ m_availableLanguages.Length ];
				for ( int nLanguageIndex = 0 ; nLanguageIndex < m_sTextFilesPerLanguage.Length ; ++nLanguageIndex )
				{
					m_sTextFilesPerLanguage[ nLanguageIndex ] = new HashSet<string>();
				}
			}

			m_localizedTexts = new Dictionary<string, string>();
		}

		/// <summary>
		/// Adds a text file to a language.
		/// </summary>
		/// <param name="sLanguageCode">Language code.</param>
		/// <param name="sFileResourcePath">Resource path of the file.</param>
		public void AddTextFile( string sLanguageCode, string sFileResourcePath )
		{
			GlobalTools.Assert( String.IsNullOrEmpty( sLanguageCode ) == false );
			GlobalTools.Assert( String.IsNullOrEmpty( sFileResourcePath ) == false );

			int nLanguageIndex = System.Array.FindIndex( m_availableLanguages, item => item.m_sLanguageCulture == sLanguageCode );
			if ( nLanguageIndex >= 0 && nLanguageIndex < m_availableLanguages.Length )
			{
				m_sTextFilesPerLanguage[ nLanguageIndex ].Add( sFileResourcePath );

				// if the language is the current one, load the file immediately
				if ( nLanguageIndex == m_nLanguageIndex )
				{
					List<string> sFilesContent = new List<string>();
					FillCsvContents( ref sFilesContent, sFileResourcePath );
					for ( int nFileContentIndex = 0 ; nFileContentIndex < sFilesContent.Count ; ++nFileContentIndex )
					{
						FillDictionaryWithCsvContent( sFilesContent[ nFileContentIndex ] );
					}
				}
			}
			else
			{
				Debug.LogErrorFormat( "Language Manager : language code '{0}' is not an available language code.\nThis error happened while registering file at path '{1}'.", sLanguageCode, sFileResourcePath );
			}
		}

		/// <summary>
		/// Sets the default language.
		/// </summary>
		public void SetDefaultLanguage()
		{
			GlobalTools.Assert( m_availableLanguages.Length > 0, "Language manager : no available language registered." );

			// Look for system language and if the language is not available, switch to the first english language.
			// If there is no english language, switch to the first available language.

			int nSystemLanguageIndex = -1;
			int nEnglishLanguageIndex = -1;

			int nLanguageIndex = 0;
			while ( nLanguageIndex < m_availableLanguages.Length && nSystemLanguageIndex < 0 )
			{
				if ( m_availableLanguages[ nLanguageIndex ].m_eLang == Application.systemLanguage )
				{
					nSystemLanguageIndex = nLanguageIndex;
				}
				else
				{
					if ( m_availableLanguages[ nLanguageIndex ].m_eLang == SystemLanguage.English && nEnglishLanguageIndex < 0 )
					{
						nEnglishLanguageIndex = nLanguageIndex;
					}
					++nLanguageIndex;
				}
			}

			if ( nSystemLanguageIndex >= 0 )
			{
				SetLanguage( nSystemLanguageIndex );
			}
			else if ( nEnglishLanguageIndex >= 0 )
			{
				SetLanguage( nEnglishLanguageIndex );
			}
			else
			{
				SetLanguage( 0 );
			}
		}

		/// <summary>
		/// Sets the language.
		/// </summary>
		/// <param name="sLanguageCode">Code of the language to set.</param>
		public void SetLanguage( string sLanguageCode )
		{
			int nLanguageIndex = System.Array.FindIndex( m_availableLanguages, item => item.m_sLanguageCulture == sLanguageCode );
			GlobalTools.AssertFormat( nLanguageIndex >= 0 && nLanguageIndex < m_availableLanguages.Length, "Language '{0}' not found in available languages.", sLanguageCode );
			SetAndLoadLanguage( nLanguageIndex );
		}

		/// <summary>
		/// Sets the language.
		/// </summary>
		/// <param name="systemLanguage">System language.</param>
		public void SetLanguage( SystemLanguage systemLanguage )
		{
			int nLanguageIndex = System.Array.FindIndex( m_availableLanguages, item => item.m_eLang == systemLanguage );
			GlobalTools.AssertFormat( nLanguageIndex >= 0 && nLanguageIndex < m_availableLanguages.Length, "Language '{0}' not found in available languages.", systemLanguage );
			SetAndLoadLanguage( nLanguageIndex );
		}

		/// <summary>
		/// Sets the language.
		/// </summary>
		/// <param name="nLanguageIndex">Index of the language to set among all available languages.</param>
		public void SetLanguage( int nLanguageIndex )
		{
			if ( nLanguageIndex == -1 )
			{
				SetDefaultLanguage();
			}
			else
			{
				SetAndLoadLanguage( nLanguageIndex );
			}
		}

		/// <summary>
		/// Sets the language to next available.
		/// </summary>
		public void SetLanguageToNextAvailable()
		{
			GlobalTools.Assert( m_nLanguageIndex >= 0 && m_nLanguageIndex < m_availableLanguages.Length, "Language manager does not have a current language." );
			int nLanguageIndex = ( m_nLanguageIndex + 1 ) % m_availableLanguages.Length;
			SetAndLoadLanguage( nLanguageIndex );
		}

		/// <summary>
		/// Sets the language to previous available.
		/// </summary>
		public void SetLanguageToPreviousAvailable()
		{
			GlobalTools.Assert( m_nLanguageIndex >= 0 && m_nLanguageIndex < m_availableLanguages.Length, "Language manager does not have a current language." );
			int nLanguageIndex = ( m_nLanguageIndex + m_availableLanguages.Length - 1 ) % m_availableLanguages.Length;
			SetAndLoadLanguage( nLanguageIndex );
		}

		/// <summary>
		/// Determines whether this instance has the specified key.
		/// </summary>
		/// <param name="sKey">Key.</param>
		public bool HasStringKey( string sKey )
		{
			GlobalTools.Assert( m_nLanguageIndex >= 0 && m_nLanguageIndex < m_availableLanguages.Length, "Language manager does not have a current language." );
			return m_localizedTexts.ContainsKey( sKey );
		}

		/// <summary>
		/// Gets the localized text associated with the key given.
		/// </summary>
		/// <param name="sKey">Localization key.</param>
		public string GetString( string sKey )
		{
			GlobalTools.Assert( m_nLanguageIndex >= 0 && m_nLanguageIndex < m_availableLanguages.Length, "Language manager does not have a current language." );

			string sValue;
			if ( m_localizedTexts.TryGetValue( sKey, out sValue ) )
			{
				return sValue;
			}
			else
			{
				Debug.LogWarningFormat( "Language manager : key '{0}' not found for language '{1}'.", sKey, m_availableLanguages[ m_nLanguageIndex ].m_sLanguageCulture );
				return string.Empty;
			}
		}

		/// <summary>
		/// Gets the localized text associated to the given time.
		/// </summary>
		/// <param name="nTimeInSeconds">Time in seconds.</param>
		/// <param name="sTimeUnitKeys">Array of keys for each time unit defined by the enumeration TimeUnit.</param>
		/// <param name="sSeparator">Separator to use between each value.</param>
		/// <param name="sNumbersFont">Font used for numbers</param>
		public string GetTime( uint nTimeInSeconds, string[] sTimeUnitKeys, string sSeparator = " ", string sNumbersFont = null )
		{
			GlobalTools.Assert( sTimeUnitKeys != null );
			GlobalTools.AssertFormat( sTimeUnitKeys.Length == timeUnitTextIdCount, "Language Manager : the length of the array of time unit keys given does not match the amount of time unit.\nArray length is {0} and the number of time units is {1}.", sTimeUnitKeys.Length, timeUnitTextIdCount );

			uint nDays = 0;
			uint nHours = 0;
			uint nMinutes = 0;
			uint nSeconds = 0;
			GlobalTools.ConvertTime( nTimeInSeconds, out nDays, out nHours, out nMinutes, out nSeconds );

			string sFontStart;
			string sFontEnd;
			if ( string.IsNullOrEmpty( sNumbersFont ) )
			{
				sFontStart = "";
				sFontEnd = "";
			}
			else
			{
				sFontStart = "<font=\"" + sNumbersFont + "\">";
				sFontEnd = "</font>";
			}

			if ( nDays > 0 )
			{
				string sDays = GetString( sTimeUnitKeys[ (int)( ( nDays > 1 ) ? TimeUnitTextId.Days : TimeUnitTextId.Day ) ] );
				if ( nHours > 0 )
				{
					string sHours = GetString( sTimeUnitKeys[ (int)( ( nHours > 1 ) ? TimeUnitTextId.Hours : TimeUnitTextId.Hour ) ] );
					return String.Format( "{5}{1:D}{6}{0}{2} {5}{3:D}{6}{0}{4}", sSeparator, nDays, sDays, nHours, sHours, sFontStart, sFontEnd );
				}
				else
				{
					return String.Format( "{3}{1:D}{4}{0}{2}", sSeparator, nDays, sDays, sFontStart, sFontEnd );
				}
			}
			else if ( nHours > 0 )
			{
				string sHours = GetString( sTimeUnitKeys[ (int)( ( nHours > 1 ) ? TimeUnitTextId.Hours : TimeUnitTextId.Hour ) ] );
				if ( nMinutes > 0 )
				{
					string sMinutes = GetString( sTimeUnitKeys[ (int)( ( nMinutes > 1 ) ? TimeUnitTextId.Minutes : TimeUnitTextId.Minute ) ] );
					return String.Format( "{5}{1:D}{6}{0}{2} {5}{3:D}{6}{0}{4}", sSeparator, nHours, sHours, nMinutes, sMinutes, sFontStart, sFontEnd );
				}
				else
				{
					return String.Format( "{3}{1:D}{4}{0}{2}", sSeparator, nHours, sHours, sFontStart, sFontEnd );
				}
			}
			else if ( nMinutes > 0 )
			{
				string sMinutes = GetString( sTimeUnitKeys[ (int)( ( nMinutes > 1 ) ? TimeUnitTextId.Minutes : TimeUnitTextId.Minute ) ] );
				if ( nSeconds > 0 )
				{
					string sSeconds = GetString( sTimeUnitKeys[ (int)( ( nSeconds > 1 ) ? TimeUnitTextId.Seconds : TimeUnitTextId.Second ) ] );
					return String.Format( "{5}{1:D}{6}{0}{2} {5}{3:D}{6}{0}{4}", sSeparator, nMinutes, sMinutes, nSeconds, sSeconds, sFontStart, sFontEnd );
				}
				else
				{
					return String.Format( "{3}{1:D}{4}{0}{2}", sSeparator, nMinutes, sMinutes, sFontStart, sFontEnd );
				}
			}
			else
			{
				string sSeconds = GetString( sTimeUnitKeys[ (int)( ( nSeconds > 1 ) ? TimeUnitTextId.Seconds : TimeUnitTextId.Second ) ] );
				return String.Format( "{3}{1:D}{4}{0}{2}", sSeparator, nSeconds, sSeconds, sFontStart, sFontEnd );
			}
		}

		/// <summary>
		/// Reloads the texts from the resource text files.
		/// </summary>
		public void ReloadTexts()
		{
			GlobalTools.Assert( m_nLanguageIndex >= 0 && m_nLanguageIndex < m_availableLanguages.Length, "Language manager does not have a current language." );

			LoadCurrentLanguage();
			for (int i = 0; i < m_onLanguageChangedEvent.Count; ++i)
			{
				m_onLanguageChangedEvent[i].OnLanguageChanged();
			}
		}

		#region Private

		#region Methods
		private void SetAndLoadLanguage( int nLanguageIndex )
		{
			GlobalTools.AssertFormat( nLanguageIndex >= 0 && nLanguageIndex < m_availableLanguages.Length, "Invalid language index : {0}.", nLanguageIndex );
			if ( m_nLanguageIndex != nLanguageIndex )
			{
				m_nLanguageIndex = nLanguageIndex;

				LoadCurrentLanguage();


				for (int i = 0; i < m_onLanguageChangedEvent.Count; ++i)
				{
					m_onLanguageChangedEvent[i].OnLanguageChanged();
				}
			}
		}

		private void LoadCurrentLanguage()
		{
			m_localizedTexts.Clear();

			List<string> sFilesContent = new List<string>();
			HashSet<string>.Enumerator filePathEnumerator = m_sTextFilesPerLanguage[ m_nLanguageIndex ].GetEnumerator();
			while ( filePathEnumerator.MoveNext() )
			{
				FillCsvContents( ref sFilesContent, filePathEnumerator.Current );
			}
			filePathEnumerator.Dispose();

			for ( int nFileContentIndex = 0 ; nFileContentIndex < sFilesContent.Count ; ++nFileContentIndex )
			{
				FillDictionaryWithCsvContent( sFilesContent[ nFileContentIndex ] );
			}
		}

		private void FillCsvContents( ref List<string> sCsvContents, string sFileResourcePath )
		{
			TextAsset textAsset = Resources.Load<TextAsset>( sFileResourcePath );
			if ( textAsset == null )
			{
				Debug.LogErrorFormat( "Language Manager : Resource not found at path '{0}' .", sFileResourcePath );
			}
			else
			{
				sCsvContents.Add( textAsset.text );
			}
		}

		private void FillDictionaryWithCsvContent( string sCsvContent )
		{
			string[] sLines = sCsvContent.Split( new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries );
			// omit first line (header)
			for ( int nLineIndex = 1 ; nLineIndex < sLines.Length ; ++nLineIndex )
			{
				if ( string.IsNullOrEmpty( sLines[ nLineIndex ] ) == false )
				{
					string[] sKeyValuePair = sLines[ nLineIndex ].Split( new char[] { ';' } );
					if ( sKeyValuePair.Length == 2 )
					{
						if ( string.IsNullOrEmpty( sKeyValuePair[ 0 ] ) )
						{
							Debug.LogErrorFormat( "Language Manager : empty localization key found while parsing '{0}'.", sLines[ nLineIndex ] );
						}
						else
						{
							if ( m_localizedTexts.ContainsKey( sKeyValuePair[ 0 ] ) )
							{
								Debug.LogErrorFormat( "Language manager : localization key '{0}' already registered.", sKeyValuePair[ 0 ] );
							}
							else
							{
								m_localizedTexts.Add( sKeyValuePair[ 0 ], sKeyValuePair[ 1 ].Replace( "\\n", "\n" ) );
							}
						}
					}
					else
					{
						Debug.LogErrorFormat( "Language Manager : parsing a CSV file resulted in a parsing error with line '{0}'.", sLines[ nLineIndex ] );
					}
				}
			}
		}
		#endregion

		#region Attributes
		private int m_nLanguageIndex;
		private readonly Country[] m_availableLanguages;
		private readonly HashSet<string>[] m_sTextFilesPerLanguage;

		private readonly Dictionary<string, string> m_localizedTexts;
		#endregion
		#endregion
	}
}