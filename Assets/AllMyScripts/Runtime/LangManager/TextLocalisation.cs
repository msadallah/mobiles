﻿namespace AllMyScripts.Common.UI
{
    using UnityEngine;

    using TMPro;

    using System;

    using AllMyScripts.LangManager;

    [RequireComponent(typeof(TextMeshProUGUI))]
    [DisallowMultipleComponent]
    public class TextLocalisation : MonoBehaviour, ITextLocalizer
    {

        [Tooltip("Localisation Id")]
        [SerializeField]
        protected string id = default;

        private TextMeshProUGUI text;
        private bool bIsSkinned = false;

        public Action<TextLocalisation> OnTextChanged;
        protected virtual void Awake()
        {
            text = this.GetComponent<TextMeshProUGUI>();
            bIsSkinned = text is SkinnedTMProUGUI;

            if (text)
            {
                L.AddTextLocalizerListener(this);
                this.OnLanguageChanged();
            }
        }

        private void OnDestroy()
        {
            L.RemoveTextLocalizerListener(this);
        }

        public void SetId(string newId)
        {
            this.id = newId;
            OnLanguageChanged();
        }

        public void OnLanguageChanged()
        {
            if (text)
            {
                text.text = GetText();
                if (OnTextChanged != null)
                    OnTextChanged(this);

                if (bIsSkinned)
                {
                    // Try to update skin now
                    UpdateSkin();
                    // Try to update skin later
                    Invoke("UpdateSkin", 0.3f);
                }
            }
        }

        protected virtual string GetText()
        {
            return L.Get(this.id);
        }

        private void UpdateSkin()
        {
            (text as SkinnedTMProUGUI).UpdateSkin();
        }
    }
}