namespace AllMyScripts.Common.Gx
{
	internal sealed class GxSprite
	{
		private uint m_nWidth;
		private uint m_nHeight;
		private int m_nU0;
		private int m_nV0;
		private int m_nU1;
		private int m_nV1;

		public GxSprite( uint nWidth, uint nHeight, int nU0, int nV0, int nU1,
							int nV1 )
		{
			SetSize( nWidth, nHeight );
			SetUV( nU0, nV0, nU1, nV1 );
		}

		public void SetSize( uint nWidth, uint nHeight )
		{
			m_nWidth = nWidth;
			m_nHeight = nHeight;
		}

		public void SetUV( int nU0, int nV0, int nU1, int nV1 )
		{
			m_nU0 = nU0;
			m_nV0 = nV0;
			m_nU1 = nU1;
			m_nV1 = nV1;
		}

		public uint GetWidth()
		{
			return m_nWidth;
		}

		public uint GetHeight()
		{
			return m_nHeight;
		}

		public int GetU0()
		{
			return m_nU0;
		}

		public int GetV0()
		{
			return m_nV0;
		}

		public int GetU1()
		{
			return m_nU1;
		}

		public int GetV1()
		{
			return m_nV1;
		}
	}
}
