namespace AllMyScripts.Common.Gx
{
	sealed class GxColor
	{
		// inARGB <-- 0xAARRGGBB
		// toRGBA --> 0xAARRGGBB
	
		private byte m_nR;
		private byte m_nG;
		private byte m_nB;
		private byte m_nA;
	
		public GxColor()
		{
			Set( 0, 0, 0 );
		}

		public GxColor( byte nRed, byte nGreen, byte nBlue )
		{
			Set( nRed, nGreen, nBlue );
		}

		public GxColor( byte nRed, byte nGreen, byte nBlue, byte nAlpha )
		{
			Set( nRed, nGreen, nBlue, nAlpha );
		}
	
		public void Set( byte nRed, byte nGreen, byte nBlue )
		{
			m_nR = nRed;
			m_nG = nGreen;
			m_nB = nBlue;
			m_nA = 0xFF;
		}

		public void Set( byte nRed, byte nGreen, byte nBlue, byte nAlpha )
		{
			m_nR = nRed;
			m_nG = nGreen;
			m_nB = nBlue;
			m_nA = nAlpha;
		}
	
		public GxColor Clone()
		{
			return new GxColor( m_nR, m_nG, m_nB, m_nA );
		}

		public UnityEngine.Color32 GetColor32()
		{
			return new UnityEngine.Color32( m_nR, m_nG, m_nB, m_nA );
		}
	
		public byte GetRed()
		{
			return m_nR;
		}
	
		public byte GetGreen()
		{
			return m_nG;
		}
	
		public byte GetBlue()
		{
			return m_nB;
		}
	
		public byte GetAlpha()
		{
			return m_nA;
		}
	
		public float GetLuminance()
		{
			return ( (m_nR*0.299f) + (m_nG*0.587f) + (m_nB*0.114f) ) / 255.0f;
		}
	
		public uint ToRGB888()
		{
			return (uint)( m_nR + ( m_nG << 8 ) + ( m_nB << 16 ) );
		}
	
		public uint ToBGR888()
		{
			return (uint)( m_nB + ( m_nG << 8 ) + ( m_nR << 16 ) );
		}

		public uint ToRGBA8888()
		{
			if( m_nR>=248 && m_nG==0 && m_nB>=248 )
				return 0;
			else
				return (uint)( m_nR + ( m_nG << 8 ) + ( m_nB << 16 ) + ( m_nA << 24 ) );
		}
	
		public uint ToBGRA8888()
		{
			if( m_nR>=248 && m_nG==0 && m_nB>=248 )
				return 0;
			else
				return (uint)( m_nB + ( m_nG << 8 ) + ( m_nR << 16 ) + ( m_nA << 24 ) );
		}
	
		public uint ToRGB555()
		{
			return (uint)( m_nR + ( m_nG << 5 ) + ( m_nB << 10 ) );
		}
	
		public void InRGB888( uint nVal )
		{
			m_nB = (byte)( nVal & 0xFF );
			nVal >>= 8;
			m_nG = (byte)( nVal & 0xFF );
			nVal >>= 8;
			m_nR = (byte)( nVal & 0xFF );
			m_nA = 0xFF;
		}
	
		public void InARGB8888( uint nVal )
		{
			m_nB = (byte)( nVal & 0xFF );
			nVal >>= 8;
			m_nG = (byte)( nVal & 0xFF );
			nVal >>= 8;
			m_nR = (byte)( nVal & 0xFF );
			nVal >>= 8;
			m_nA = (byte)( nVal & 0xFF );
		}
	
		public void InABGR8888( uint nVal )
		{
			m_nR = (byte)( nVal & 0xFF );
			nVal >>= 8;
			m_nG = (byte)( nVal & 0xFF );
			nVal >>= 8;
			m_nB = (byte)( nVal & 0xFF );
			nVal >>= 8;
			m_nA = (byte)( nVal & 0xFF );
		}
	
		public void InBGR555( uint nVal )
		{
			m_nR = (byte)( (nVal & 0x1F) << 3 );
			nVal >>= 5;
			m_nG = (byte)( (nVal & 0x1F) << 3 );
			nVal >>= 5;
			m_nB = (byte)( (nVal & 0x1F) << 3 );
			nVal >>= 5;
			m_nA = 0xFF;
		}
	
		public void InABGR1555( uint nVal )
		{
			m_nR = (byte)( (nVal & 0x1F) << 3 );
			nVal >>= 5;
			m_nG = (byte)( (nVal & 0x1F) << 3 );
			nVal >>= 5;
			m_nB = (byte)( (nVal & 0x1F) << 3 );
			nVal >>= 5;
			m_nA = (byte)( ((nVal & 0x01)!=0) ? 0xFF : 0 );
		}
	}
}
