namespace AllMyScripts.Common.Gx
{
    using AllMyScripts.Common.Tools;
    using UnityEngine;

    sealed class GxArrangedTexture
	{
		public enum TextureChunk
		{
			VERSION = 0x5645522E,	// .REV
			HEADER = 0x54455848,	// HXET
			SPRITES = 0x53505254,	// TRPS
			SPRITEIDS = 0x53504944,	// DIPS
			FILE = 0x46494C45,		// ELIF
			PALETTE = 0x50414C54,	// TLAP
			TEXELS = 0x5445584C,	// LXET
			INDEXES = 0x49445853	// SXDI
		}
	
		public enum TexFormat
		{
			NONE = 0,
			A3I5,
			PLTT4,
			PLTT16,
			PLTT256,
			COMP4x4,
			A5I3,
			DIRECT,
			R5G6B5,
			R8G8B8,
			A1R5G5B5,
			A4R4G4B4,
			A8R8G8B8,
			DXT1,
			DXT2,
			DXT3,
			DXT4,
			DXT5,
			PVRTC2,
			PVRTC4,
			PNG,
			JPG
		};
	
		private uint m_nTexWidth;
		private uint m_nTexHeight;
		private TexFormat m_nTexFormat = TexFormat.NONE;
		private uint m_nTexUsedLines;
		private uint m_nTexNbrSprites;
		GxColor[] m_colorArray;
		private uint m_nPaletteSize;
		private uint[] m_pBitmapData;
		public GxSprite[] m_spriteArray;
		private bool m_bIsLoaded;
		
		public void Delete()
		{
			Destroy();
		}

		public void Create( uint nWidth, uint nHeight )
		{
			m_nTexWidth = nWidth;
			m_nTexHeight = nHeight;
			m_nTexUsedLines = nHeight;
			m_nTexNbrSprites = 1;
			m_spriteArray = new GxSprite[1];
			m_spriteArray[0] = new GxSprite( nWidth, nHeight, 0, 0, (int)nWidth, (int)nHeight );
			m_pBitmapData = new uint[m_nTexWidth*m_nTexUsedLines];
			m_bIsLoaded = true;
		}
	
		public bool Load( string sPath )
		{
			return Load( GlobalTools.LoadBinaryFile( sPath, "" ) );
		}

		public bool Load( byte[] pData )
		{
			m_bIsLoaded = false;
		
			if( pData==null ) return false;
			BinaryStream bs = new BinaryStream( pData );
		
			byte[] pTexels = null;
			byte[] pPalette = null;
			byte[] pSprites = null;
			byte[] pSpriteIds = null;
		
			uint nSize = 0;
			bool bVersion = false;
		
			while( !bs.IsEof() )
			{
				TextureChunk nChunk = (TextureChunk)bs.ReadU32();
				if( !bs.IsEof() )
				{
					nSize = bs.ReadU32();
					if( ( nSize & 3 ) > 0 ) nSize += 4 - ( nSize & 3 );
				}
				else nSize = 0;
				switch( nChunk )
				{
					// Version
					case TextureChunk.VERSION:
						bs.Seek( (int)nSize, BinaryStream.SeekMode.CURRENT );
						bVersion = true;
						break;
					
					// Header
					case TextureChunk.HEADER:
						m_nTexWidth = (uint)(1 << bs.ReadU8()) * 8;
						m_nTexHeight = (uint)(1 << bs.ReadU8()) * 8;
						m_nTexFormat = (TexFormat)bs.ReadU8();
						bs.ReadU8(); // m_nTexFlags
						m_nTexUsedLines = bs.ReadU16();
						m_nTexNbrSprites = bs.ReadU16();
						break;
					
					// Sprites
					case TextureChunk.SPRITES:
						pSprites = bs.ReadBytes( nSize );
						break;
					
					// Sprite IDs
					case TextureChunk.SPRITEIDS:
						pSpriteIds = bs.ReadBytes( nSize );
						break;
					
					// File
					case TextureChunk.FILE:
						bs.Seek( (int)nSize, BinaryStream.SeekMode.CURRENT );
						break;
					
					// Palette
					case TextureChunk.PALETTE:
						pPalette = bs.ReadBytes( nSize );
						m_nPaletteSize = nSize;
						break;
					
					// Texels
					case TextureChunk.TEXELS:
						pTexels = bs.ReadBytes( nSize );
						//m_nTexelsSize = nSize;
						break;
					
					// Indexes
					case TextureChunk.INDEXES:
						bs.Seek( (int)nSize, BinaryStream.SeekMode.CURRENT );
						break;
					
					// Unknown chunk
					default:
						if( bVersion )
						{
							if( nSize>0 ) bs.Seek( (int)nSize, BinaryStream.SeekMode.CURRENT );
						}
						else
						{
							return false;
						}
						break;
				}
			}
		
			// Construct Bitmap Data
			m_pBitmapData = new uint[m_nTexWidth*m_nTexUsedLines];
			GxColor c = new GxColor();
		
			if( pSprites!=null )
			{
				uint[] nSpriteIdArray = null;
				if( pSpriteIds!=null )
				{
					nSpriteIdArray = new uint[m_nTexNbrSprites];
					BinaryStream bsSpriteIds = new BinaryStream( pSpriteIds );
					for( int i=0; i<m_nTexNbrSprites; i++ )
					{
						nSpriteIdArray[i] = bsSpriteIds.ReadU16();
					}
				}
			
				m_spriteArray = new GxSprite[m_nTexNbrSprites];
				BinaryStream bsSprites = new BinaryStream( pSprites );
				for( int i=0; i<m_nTexNbrSprites; i++ )
				{
					uint nSprWidth = bsSprites.ReadU16();
					uint nSprHeight = bsSprites.ReadU16();
					int nSprU0 = (int)bsSprites.ReadU16();
					int nSprV0 = (int)bsSprites.ReadU16();
					int nSprU1 = (int)bsSprites.ReadU16();
					int nSprV1 = (int)bsSprites.ReadU16();
					// Clear opaque flag
					nSprWidth = nSprWidth & 0x7FFF;
					if( nSpriteIdArray!=null )
					{
						m_spriteArray[(int)nSpriteIdArray[i]] = new GxSprite( nSprWidth, nSprHeight, nSprU0, nSprV0, nSprU1, nSprV1 );
					}
					else
					{
						m_spriteArray[i] = new GxSprite( nSprWidth, nSprHeight, nSprU0, nSprV0, nSprU1, nSprV1 );
					}
				}
			}
		
			if( pPalette!=null )
			{
				nSize = GetPaletteColors();
				m_colorArray = new GxColor[nSize];
				BinaryStream bsPalette = new BinaryStream( pPalette );
				for( int i=0; i<nSize; i++ )
				{
					c.InBGR555( bsPalette.ReadU16() );
					m_colorArray[i] = new GxColor( c.GetRed(), c.GetGreen(), c.GetBlue() );
				}
			}
		
			if( pTexels!=null )
			{
				BinaryStream bsTexels = new BinaryStream( pTexels );
			
				switch( m_nTexFormat )
				{
					case TexFormat.PLTT256:
						if( m_colorArray!=null )
						{
							for( uint j=0; j<m_nTexUsedLines; j++ )
							{
								for( uint i=0; i<m_nTexWidth; i++ )
								{
									c = m_colorArray[bsTexels.ReadU8()];
									if( c!=null )
										SetPixel( i, j, c.ToBGRA8888() );
									else
										break;
								}
							}
						}
						break;
					
					case TexFormat.R8G8B8:
						for( uint j=0; j<m_nTexUsedLines; j++ )
						{
							for( uint i=0; i<m_nTexWidth; i++ )
							{
								byte nB = bsTexels.ReadU8();
								byte nG = bsTexels.ReadU8();
								byte nR = bsTexels.ReadU8();
								c.Set( nR, nG, nB );
								SetPixel( i, j, c.ToBGRA8888() );
							}
						}
						break;
					
					case TexFormat.A8R8G8B8:
						for( uint j=0; j<m_nTexUsedLines; j++ )
						{
							for( uint i=0; i<m_nTexWidth; i++ )
							{
								SetPixel( i, j, bsTexels.ReadU32() );
							}
						}
						break;
				}
			}

			return true;
		}
	
		public void Destroy()
		{
			if( m_bIsLoaded )
			{
				m_pBitmapData = null;
				m_colorArray = null;
				m_spriteArray = null;
				m_bIsLoaded = false;
			}
		}
	
		public bool IsValid()
		{
			return m_bIsLoaded;
		}

		public uint GetWidth()
		{
			return m_nTexWidth;
		}
	
		public uint GetHeight()
		{
			return m_nTexHeight;
		}
	
		public uint GetPaletteColors()
		{
			return ( m_nPaletteSize >> 1 );
		}

		public GxColor GetPaletteColor( uint nIndex )
		{
			return m_colorArray[(int)nIndex];
		}

		public GxColor[] GetPaletteColorArray()
		{
			return m_colorArray;
		}
	
		public int FindPaletteColor( uint nTexel )
		{
			for( int i=0; i<m_colorArray.Length; i++ )
			{
				if( m_colorArray[i].ToBGRA8888()==nTexel )
				{
					return (int)i;
				}
			}
			return -1;
		}
	
		public uint GetNbrSprites()
		{
			return m_nTexNbrSprites;
		}
	
		public GxSprite GetSprite( uint nSprite )
		{
			if( m_spriteArray!=null )
				return m_spriteArray[(int)nSprite];
			else
				return null;
		}
	
		public void SetPixel( uint nX, uint nY, uint nColor )
		{
			m_pBitmapData[(int)(nY*m_nTexWidth+nX)] = nColor;
		}

		public uint GetPixel( uint nX, uint nY )
		{
			return m_pBitmapData[(int)(nY*m_nTexWidth+nX)];
		}

		public uint[] GetBitmapData()
		{
			return m_pBitmapData;
		}
	
		public Texture2D BuildTexture2D( uint nSprite, bool bReadable=false )
		{
			GxColor cPix = new GxColor();
			GxSprite s = GetSprite( nSprite );
			uint nWidth = s.GetWidth();
			uint nHeight = s.GetHeight();
			uint nU0 = (uint)s.GetU0();
			uint nV0 = (uint)s.GetV0();
			uint nU1 = (uint)s.GetU1();

			Texture2D t2D = new Texture2D( (int)nWidth, (int)nHeight, TextureFormat.RGB24, false );
			t2D.filterMode = FilterMode.Point;

			Color32[] cArray = new Color32[nWidth*nHeight];
			uint nIndex = (nHeight-1)*nWidth;

			for( uint nY=0; nY<nHeight; nY++ )
			{
				for( uint nX=0; nX<nWidth; nX++ )
				{
					uint nPosX = ( nU0>nU1 ) ? ( nU0-nY-1 ) : ( nU0+nX );
					uint nPosY = ( nU0>nU1 ) ? ( nV0+nX ) : ( nV0+nY );
					cPix.InARGB8888( GetPixel( nPosX, nPosY ) );
					cArray[(int)(nIndex+nX)] = cPix.GetColor32();
				}
				nIndex -= nWidth;
			}
			t2D.SetPixels32( cArray );
			t2D.Apply( false, !bReadable );
		
			return t2D;
		}
	
		public Texture2D[] BuildTexture2DArray()
		{
			Texture2D[] t2DArray = new Texture2D[GetNbrSprites()];
		
			for( uint i=0; i<GetNbrSprites(); i++ )
			{
				t2DArray[(int)i] = BuildTexture2D( i );
			}
		
			return t2DArray;
		}
	}
}
