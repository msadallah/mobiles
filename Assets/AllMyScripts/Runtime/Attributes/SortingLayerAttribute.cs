namespace AllMyScripts.Common.Attributes
{
    using UnityEngine;
    using System;

    /// <summary>
    /// Attribute to set a string as a sorting layer
    /// </summary>
    [AttributeUsage( AttributeTargets.Field )]
    public class SortingLayerAttribute : PropertyAttribute
    {}
}

