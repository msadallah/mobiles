namespace AllMyScripts.Common.Attributes
{
    using UnityEngine;
    using System;
    using AllMyScripts.Common.Tools;

    [AttributeUsage(AttributeTargets.Field)]
	public class MinMaxVectorAttribute : PropertyAttribute 
	{
		public float fMinValue { get { return m_fMinValue; } }
		public float fMaxValue { get { return m_fMaxValue; } }
		public bool bUseInteger { get { return m_bUseInteger; } }
	
		private readonly float m_fMinValue;
		private readonly float m_fMaxValue;
		private readonly bool m_bUseInteger;

		public MinMaxVectorAttribute( float fMinValue, float fMaxValue, bool bUseInteger=false )
		{
			GlobalTools.Assert( fMinValue<=fMaxValue );
			m_fMinValue = fMinValue;
			m_fMaxValue = fMaxValue;
			m_bUseInteger = bUseInteger;
		}
	}
}
