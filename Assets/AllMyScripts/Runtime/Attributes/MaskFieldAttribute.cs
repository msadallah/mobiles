namespace AllMyScripts.Common.Attributes
{
    using UnityEngine;
    /// <summary>
    /// Attribute that allow an enumeration to be represented as a bit mask
    /// </summary>
    public class MaskFieldAttribute : PropertyAttribute
    {
    }
}
