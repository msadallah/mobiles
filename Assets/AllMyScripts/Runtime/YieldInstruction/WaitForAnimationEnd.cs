namespace AllMyScripts.Common.Tools
{
    using UnityEngine;

    /// <summary>
    /// Custom yield instruction that waits until the end of an animation (or at least a stable state)
    /// </summary>
    public class WaitForAnimationEnd : CustomYieldInstruction
	{
		public override bool keepWaiting
		{
			get
			{
				bool bIsAnimationPlaying = false;
				int nLayerArrayIndex = 0;
				while( bIsAnimationPlaying==false && nLayerArrayIndex<m_nLayerIndexes.Length )
				{
					int nLayerIndex = m_nLayerIndexes[nLayerArrayIndex];
					GlobalTools.Assert( nLayerIndex>=0 && nLayerIndex < m_animator.layerCount );
				
					if( m_animator.IsInTransition( nLayerIndex ) )
					{
						bIsAnimationPlaying = true;
					}
					else
					{
						AnimatorStateInfo stateInfo = m_animator.GetCurrentAnimatorStateInfo( nLayerIndex );
						if( stateInfo.normalizedTime<1.0f )
						{
							bIsAnimationPlaying = true;
						}
					}

					++nLayerArrayIndex;
				}
				return bIsAnimationPlaying;
			}
		}
	
		public WaitForAnimationEnd( Animator animator, params int[] nLayerIndexes )
		{
			GlobalTools.Assert( animator!=null );
			m_animator = animator;

			if( nLayerIndexes==null || nLayerIndexes.Length==0 )
			{
				m_nLayerIndexes = new int[m_animator.layerCount];
				for( int nLayerIndex = 0; nLayerIndex<m_animator.layerCount; ++nLayerIndex )
				{
					m_nLayerIndexes[nLayerIndex] = nLayerIndex;
				}
			}
			else
			{
				m_nLayerIndexes = nLayerIndexes;
			}
		}

		private readonly Animator m_animator;
		private readonly int[] m_nLayerIndexes;
	}
}
