﻿namespace AllMyScripts.Common.Utils
{
	using UnityEngine;
	public static partial class Math
	{
		public static float Average(params float[] val)
		{
			float returnValue = 0f;

			if (val.Length > 0)
			{
				foreach (var item in val)
					returnValue += item;

				returnValue /= val.Length;
			}

			return returnValue;
		}

		/// <summary>
		/// Return the always positive modulus result of x modulo  m. 
		///https://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain
		/// </summary>
		/// <param name="x">The param to get the modulus of</param>
		/// <param name="m">The Modulo</param>
		/// <returns>The always positive modulus result</returns>
		public static int mod(int x, int m)
		{
			return (x % m + m) % m;
		}

		public static bool AreValuesEqual(float val1, float val2)
		{
			return AreValuesEqual(val1, val2, Mathf.Epsilon);
		}


		public static bool AreValuesEqual(float val1, float val2, float tolerance)
		{
			if (val1 >= (val2 - tolerance) && val1 <= (val2 + tolerance))
			{
				return true;
			}

			return false;
		}

		public static bool AreValuesEqual(Vector2 val1, Vector2 val2)
		{
			return AreValuesEqual(val1, val2, Mathf.Epsilon);
		}


		public static bool AreValuesEqual(Vector2 val1, Vector2 val2, float tolerance)
		{
			if (
					val1.x >= (val2.x - tolerance) && val1.y >= (val2.y - tolerance) 
					&&  
					val1.x <= (val2.x + tolerance) && val1.y <= (val2.y + tolerance)
				)
			{
				return true;
			}

			return false;
		}
		public static bool AreValuesEqual(Vector3 val1, Vector3 val2)
		{
			return AreValuesEqual(val1, val2, Mathf.Epsilon);
		}


		public static bool AreValuesEqual(Vector3 val1, Vector3 val2, float tolerance)
		{
			if (
					val1.x >= (val2.x - tolerance) && val1.y >= (val2.y - tolerance) && val1.z >= (val2.z - tolerance)
					&&
					val1.x <= (val2.x + tolerance) && val1.y <= (val2.y + tolerance) && val1.z <= (val2.z + tolerance)
				)
			{
				return true;
			}

			return false;
		}
	}
}
