namespace AllMyScripts.Common.Version
{
    using System;
    using UnityEngine;
    using System.IO;
    using System.Collections.Generic;

    public sealed class Version
	{
		static readonly string s_sFilePath = "Assets/Resources/";
		static readonly string s_sFileNameWithoutExt = "Version";
		static readonly string s_sPathName = s_sFilePath + s_sFileNameWithoutExt;
		static readonly string s_sCompletePathName = s_sPathName + ".txt";

		public static string GetVersion()
		{
			TextAsset textAsset = Resources.Load( s_sFileNameWithoutExt ) as TextAsset;

			if ( textAsset == null )
				return "Unknown";
			
			return textAsset.ToString();
		}

		public static string GetVersionNumber()
		{
			string sVersion = GetVersion();
			int nSpace = sVersion.IndexOf( " ", StringComparison.Ordinal );
			if( nSpace != -1 )
				return sVersion.Substring( 0, nSpace );
			else
				return sVersion;
		}

		public static string GetDate()
		{
			string sVersion = GetVersion();
			int nSpace = sVersion.IndexOf(" ", StringComparison.Ordinal);
			if (nSpace != -1)
				return sVersion.Substring(nSpace + 1);
			else
				return null;
		}

		/// <summary>
		/// a function to compare two versions encoded x.xx.x
		/// </summary>
		/// <param name="sVersionA">The first version</param>
		/// <param name="sVersionB">The version to compare it to</param>
		/// <returns>1 if sVersionA is lower than sVersionB
		/// 0 if equals
		/// -1 if sVersionA is greater than sVersionB</returns>
		public static int CompareVersions( string sVersionA, string sVersionB )
		{
			List<string> sA = new List<string>(sVersionA.Split('.'));
			List<string> sB = new List<string>(sVersionB.Split('.'));

			//fill the gaps with zeros (we consider that 1.6.0 = 1.6.0.0.0.0.0)
			while (sA.Count < sB.Count)
			{
				sA.Add("0");
			}
			while (sB.Count < sA.Count)
			{
				sB.Add("0");
			}
			
			for(int i=0;i<sA.Count;++i)
			{
				int nA = Tools.ParseTools.ParseIntSafe(sA[i]);
				int nB = Tools.ParseTools.ParseIntSafe(sB[i]);
				if (nA > nB) return -1;
				if (nB > nA) return 1;
			}
			return 0;
		}

		public static void WriteVersion( string sVersion )
		{
			if( !Directory.Exists( s_sFilePath ) )
				Directory.CreateDirectory( s_sFilePath );
			string sData = sVersion + " " + GetSystemDate();
			File.WriteAllText( s_sCompletePathName, sData );
			Debug.Log( "Write '" + sData + "' to " + s_sCompletePathName );
		}

		private static string GetSystemDate()
		{
			return System.DateTime.Now.ToString( @"yyyy/MM/dd HH:mm" );
		}
	}
}
