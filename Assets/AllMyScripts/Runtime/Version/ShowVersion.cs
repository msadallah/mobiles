namespace AllMyScripts.Common.Version
{
    using UnityEngine;
    public sealed class ShowVersion : MonoBehaviour
	{
		public Rect m_rectGui = new Rect( Screen.width - 150f, 5f, 150f, 20f );
		public GUISkin m_skin;
	
		private string m_versionLabel;
	
		void Start()
		{
			m_versionLabel = Version.GetVersion();
		}
	
		void OnGUI()
		{
			if( m_skin != null )
				GUI.skin = m_skin;
			GUIStyle style = GUI.skin.box;
			GUI.Label( m_rectGui, m_versionLabel, style );
		}
	}
}
