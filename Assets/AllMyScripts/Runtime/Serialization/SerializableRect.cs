﻿namespace AllMyScripts.Common.Serialization
{
    using System.Runtime.Serialization;
    using UnityEngine;

    [System.Serializable]
    public class SerializableRect : ISerializable
    {
        private Rect _rect;
        public Rect rect
        {
            get
            {
                return _rect;
            }
        }
        public SerializableRect(Rect rect)
        {
            _rect = rect;
        }
        protected SerializableRect(SerializationInfo info, StreamingContext context)
        {
            float[] arrayValue = (float[])info.GetValue("rect", typeof(float[]));
            _rect = new Rect(arrayValue[0], arrayValue[1], arrayValue[2], arrayValue[3]);
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("rect", new float[] { _rect.x, _rect.y, _rect.width, _rect.height }, typeof(float[]));
        }
        public static implicit operator Rect (SerializableRect sRect)
        {
            return sRect.rect;
        }

        //The Json dosn't serialize the class anymore, add temporarly static serialize function
        public float[] ToFloatArray()
        {
            return SerializableRect.ToFloatArray(this);
        }
        public static float[] ToFloatArray(SerializableRect serializableRect)
        {
            return new float[] { serializableRect._rect.x, serializableRect._rect.y, serializableRect._rect.width, serializableRect._rect.height };
        }

        public static SerializableRect FromFloatArray(float[] floatArray)
        {
            return new SerializableRect(new Rect(floatArray[0], floatArray[1], floatArray[2], floatArray[3]));
        }

    }
}