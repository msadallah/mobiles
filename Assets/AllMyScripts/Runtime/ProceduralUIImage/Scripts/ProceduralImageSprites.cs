﻿namespace UnityEngine.UI.ProceduralImage
{
	using UnityEngine;

	public class ProceduralImageSprites : ScriptableObject
	{
		[System.Serializable]
		public class ProceduralSprite
		{
			public Sprite sprite = null;
			public float radius = 0;
			public bool isBorder = false;
			public float falloffMin = 0;
		}

		public ProceduralSprite[] proceduralSprites;

		private static ProceduralImageSprites _instance;
		public static ProceduralImageSprites instance
		{
			get
			{
				if(_instance==null)
				{
					_instance = Resources.Load<ProceduralImageSprites>("Procedural Image Sprites");
				}
				return _instance;
			}
		}

		public static UnityEngine.Sprite GetUniformSprite(ProceduralImage proceduralImage, UniformModifier modifier)
		{
			ProceduralSprite pick = null;
			bool isBorder = !Mathf.Approximately(proceduralImage.BorderWidth, 0);
			foreach (ProceduralSprite sprite in instance.proceduralSprites)
			{
				if(sprite.isBorder == isBorder)
				{
					//they both have a border
					if(pick == null)
					{
						pick = sprite;
					}
					else
					{
						if (sprite.falloffMin <= proceduralImage.FalloffDistance &&
							proceduralImage.FalloffDistance - pick.falloffMin >= proceduralImage.FalloffDistance - sprite.falloffMin)
						{
							if (Mathf.Abs(pick.radius - modifier.Radius) >= Mathf.Abs(sprite.radius - modifier.Radius))
							{
								//the sprite we had picked isn't the closest
								pick = sprite;
							}
						}
					}
				}
			}
			return pick?.sprite;
		}

		public static bool IsUniformSprite(Sprite sprite)
		{
			if (instance != null)
			{
				foreach (var ps in instance.proceduralSprites)
				{
					if (ps.sprite == sprite)
						return true;
				}
			}
			return false;
		}
	}
}