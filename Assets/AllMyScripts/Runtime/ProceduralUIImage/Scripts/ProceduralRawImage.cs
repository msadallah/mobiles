using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

/* Author: Josh H.
 * Procedural UI Image
 * assetstore.joshh@gmail.com for feedback or questions
 */

namespace UnityEngine.UI.ProceduralImage
{

    [ExecuteInEditMode]
    [AddComponentMenu("UI/Procedural Raw Image")]
    public class ProceduralRawImage : RawImage
    {
        [SerializeField] private float borderWidth;
        private ProceduralImageModifier modifier;
		private static Material materialInstance;
		private static Material materialInstanceNoMask;
		[SerializeField] private float falloffDistance = 1f;
		/// <summary>
		/// CUSTOM ADD - Adapt radius corners with falloff distance
		/// </summary>
		[SerializeField] private bool adaptFalloffCorners = false;
		/// <summary>
		/// CUSTOM ADD - Adapt size of image to draw with falloff distance
		/// </summary>
		[SerializeField] private bool adaptFalloffSize = false;
		[SerializeField] private bool ignoreMask = false;

		public float BorderWidth
        {
            get
            {
                return borderWidth;
            }
            set
            {
                borderWidth = value;
                this.SetVerticesDirty();
            }
        }

        public float FalloffDistance
        {
            get
            {
                return falloffDistance;
            }
            set
            {
                falloffDistance = value;
                this.SetVerticesDirty();
            }
        }

        protected ProceduralImageModifier Modifier
        {
            get
            {
                if (modifier == null)
                {
                    //try to get the modifier on the object.
                    modifier = this.GetComponent<ProceduralImageModifier>();
                    //if we did not find any modifier
                    if (modifier == null)
                    {
                        //Add free modifier
                        ModifierType = typeof(UniformModifier);
                    }
                }
                return modifier;
            }
            set
            {
                modifier = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of the modifier. Adds a modifier of that type.
        /// </summary>
        /// <value>The type of the modifier.</value>
        public System.Type ModifierType
        {
            get
            {
                return Modifier.GetType();
            }
            set
            {
                if (modifier != null && modifier.GetType() != value)
                {
                    if (this.GetComponent<ProceduralImageModifier>() != null)
                    {
                        DestroyImmediate(this.GetComponent<ProceduralImageModifier>());
                    }
                    this.gameObject.AddComponent(value);
                    Modifier = this.GetComponent<ProceduralImageModifier>();
                    this.SetAllDirty();
                }
                else if(modifier == null){
                    this.gameObject.AddComponent(value);
                    Modifier = this.GetComponent<ProceduralImageModifier>();
                    this.SetAllDirty();
                }
            }
        }

        override protected void OnEnable()
        {
            base.OnEnable();
            this.Init();
        }

        override protected void OnDisable()
        {
            base.OnDisable();
            this.m_OnDirtyVertsCallback -= OnVerticesDirty;
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        void Init()
        {
			this.m_OnDirtyVertsCallback += OnVerticesDirty;
			if( materialInstance == null )
			{
				materialInstance = new Material( Shader.Find( "UI/Procedural UI Raw Image" ) );
			}
			if( materialInstanceNoMask == null )
			{
				materialInstanceNoMask = new Material( Shader.Find( "UI/Procedural UI Raw Image No Stencil" ) );
			}
			UpdateNoMask();

		}
		public void UpdateNoMask()
		{
			this.material = ignoreMask ? materialInstanceNoMask : materialInstance;
		}

        protected void OnVerticesDirty()
        {
			UpdateNoMask();
		}

        public virtual void Update()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
            {
                this.UpdateGeometry();
			}
#endif

			if (Modifier.IsChanged)
			{
				this.UpdateGeometry();
			}
		}

        /// <summary>
        /// Prevents radius to get bigger than rect size
        /// </summary>
        /// <returns>The fixed radius.</returns>
        /// <param name="vec">border-radius as Vector4 (starting upper-left, clockwise)</param>
        private Vector4 FixRadius(Vector4 vec)
        {
            Rect r = this.rectTransform.rect;
            vec = new Vector4(Mathf.Max(vec.x, 0), Mathf.Max(vec.y, 0), Mathf.Max(vec.z, 0), Mathf.Max(vec.w, 0));
            float scaleFactor = Mathf.Min(r.width / (vec.x + vec.y), r.width / (vec.z + vec.w), r.height / (vec.x + vec.w), r.height / (vec.z + vec.y), 1);
            return vec * scaleFactor;
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            base.OnPopulateMesh(vh);
            EncodeAllInfoIntoVertices(vh, CalculateInfo());
        }

        ProceduralImageInfo CalculateInfo()
        {
            var r = GetPixelAdjustedRect();

			Rect rect = rectTransform.rect;
			float pixelSize = ( rect.xMax - rect.xMin ) / r.width;
            pixelSize = pixelSize / Mathf.Max(0, falloffDistance);

            Vector4 radius = FixRadius(Modifier.CalculateRadius(r));

			// CUSTOM ADD - adapt radius with half of falloffDistance to keep the same center of corner
			if( adaptFalloffSize && adaptFalloffCorners && falloffDistance > 0f )
			{
				float fAdd = falloffDistance * 0.5f;
				radius += new Vector4( fAdd, fAdd, fAdd, fAdd );
			}

			float fAddSize = adaptFalloffSize ? falloffDistance : 0f;
			float fW = r.width + fAddSize;
			float fH = r.height + fAddSize;
			float minside = Mathf.Min(fW, fH);

			ProceduralImageInfo info = new ProceduralImageInfo( fW, fH, falloffDistance, Mathf.Clamp( pixelSize, 1f/2048f, 2048f), radius / minside, borderWidth / minside * 2);

            return info;
        }

        void EncodeAllInfoIntoVertices(VertexHelper vh, ProceduralImageInfo info)
        {
            UIVertex vert = new UIVertex();

			float minside = Mathf.Min(info.width, info.height);

            Vector2 uv1 = new Vector2( info.width, info.height );
            Vector2 uv2 = new Vector2(EncodeFloats_0_1_16_16(info.radius.x, info.radius.y), EncodeFloats_0_1_16_16(info.radius.z, info.radius.w));
			Vector2 uv3 = new Vector2( 0f, info.pixelSize );

			Vector2 v2Center = rectTransform.rect.center;

			for (int i = 0; i < vh.currentVertCount; i++)
            {
                vh.PopulateUIVertex(ref vert, i);

				float fPosX = (vert.position.x - v2Center.x) / info.width + 0.5f;
				float fPosY = (vert.position.y - v2Center.y) / info.height + 0.5f;
				uv3.x = EncodeFloats_0_1_16_16( fPosX / minside, fPosY / minside );
				// TO DEBUG
				//Vector2 v2Enc = DecodeFloat_16_16_0_1( uv3.y );

				// Dont change vertices position
				if( adaptFalloffSize )
					vert.position += new Vector3( fPosX - 0.5f, fPosY - 0.5f ) * info.fallOffDistance;

				//vert.uv0 = vert.uv0;
				vert.uv1 = uv1;
                vert.uv2 = uv2;
                vert.uv3 = uv3;

                vh.SetUIVertex(vert, i);
            }
        }

		/// <summary>
		/// Encode two values between [0,1] into a single float. Each using 16 bits.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		float EncodeFloats_0_1_16_16( float a, float b )
		{
			Vector2 kDecodeDot = new Vector2(1.0f, 1f / 65535.0f);
			return Vector2.Dot( new Vector2( Mathf.Floor( a * 65534 ) / 65535f, Mathf.Floor( b * 65534 ) / 65535f ), kDecodeDot );
		}

		// TO DEBUG
		//Vector2 DecodeFloat_16_16_0_1( float value )
		//{
		//	int nMax = 4096;
		//	int nMaxMinusOne = nMax - 1;
		//	Vector2 kEncodeMul = new Vector2(1f, nMax);
		//	float kEncodeBit = 1f / nMax;
		//	Vector2 enc = kEncodeMul * value;
		//	enc = new Vector2( enc.x % 1f, enc.y % 1f );
		//	enc.x -= enc.y * kEncodeBit;
		//	return enc * nMax / nMaxMinusOne;
		//}

#if UNITY_EDITOR
		protected override void Reset()
        {
            base.Reset();
            OnEnable();
        }

        /// <summary>
        /// Called when the script is loaded or a value is changed in the
        /// inspector (Called in the editor only).
        /// </summary>
        protected override void OnValidate()
        {
            base.OnValidate();

            //Don't allow negative numbers for fall off distance
            falloffDistance = Mathf.Max(0, falloffDistance);

            //Don't allow negative numbers for fall off distance
            borderWidth = Mathf.Max(0, borderWidth);
        }
#endif
    }
}
