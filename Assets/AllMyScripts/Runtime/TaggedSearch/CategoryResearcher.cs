﻿namespace AllMyScripts.Common.Utils
{
    using System.Collections.Generic;
    /// <summary>
    /// class used to perform a research in ISearchables
    /// </summary>
    /// <typeparam name="T">class(es) inheriting ISearchable</typeparam>
    public class CategoryResearcher<T> : Researcher<T> where T : ICategorySearchable
    {
        public class CategorySearch
        {
            public string category { private set; get; }
            public string search { private set; get; }
            public bool bPerfectlyEqual { private set; get; }
            public CategorySearch(string category, string search, bool perfectlyEqual = false)
            {
                this.category = category;
                this.search = search;
                bPerfectlyEqual = perfectlyEqual;
            }
        }

        /// <summary>
        /// performs a research
        /// </summary>
        public T[] SearchCategory(string tagSearch, params CategorySearch[] categorySearchs)
        {
            T[] generalResults = Search(tagSearch);

            for (int i = 0; i < categorySearchs.Length; ++i)
            {
                List<T> categorySearchResult = new List<T>(generalResults);
                string[] SearchedTags;
                if (!categorySearchs[i].bPerfectlyEqual)
                    SearchedTags = categorySearchs[i].search.Split(s_separator);
                else
                    SearchedTags = new string[] { categorySearchs[i].search };

                //the category search is empty, keep going
                if (string.IsNullOrEmpty(categorySearchs[i].search))
                {
                    continue;
                }

                //search in the category
                List<List<string>> tagsPerSearchable = new List<List<string>>();

                foreach (ICategorySearchable r in categorySearchResult)
                {
                    tagsPerSearchable.Add(new List<string>(r.GetCategoriesTags(categorySearchs[i].category)));
                }

                for (int searchedTagIndex = 0; searchedTagIndex < SearchedTags.Length; ++searchedTagIndex)
                {
                    //for each tag

                    for (int nResultIdx = 0; nResultIdx < categorySearchResult.Count; ++nResultIdx)
                    {
                        //check all the results that we haven't removed yet
                        bool bHasTag = false;
                        for (int searchableTagIndex = 0; searchableTagIndex < tagsPerSearchable[nResultIdx].Count; ++searchableTagIndex)
                        {
                            if (!categorySearchs[i].bPerfectlyEqual)
                            {
                                //classic search using contains
                                if (
                                    //case insensitive test
                                    !isCaseSensitive &&
                                    tagsPerSearchable[nResultIdx][searchableTagIndex].ToLowerInvariant().Contains(SearchedTags[searchedTagIndex].ToLowerInvariant())

                                    || //or
                                       //case sensitive search
                                    isCaseSensitive &&
                                    tagsPerSearchable[nResultIdx][searchableTagIndex].Contains(SearchedTags[searchedTagIndex])
                                    )
                                {
                                    bHasTag = true;
                                    break;
                                }
                            }
                            else
                            {
                                //precise search using equality
                                if (
                                    //case insensitive test
                                    !isCaseSensitive &&
                                    tagsPerSearchable[nResultIdx][searchableTagIndex].ToLowerInvariant() == SearchedTags[searchedTagIndex].ToLowerInvariant()
                                    || //or
                                       //case sensitive search
                                    isCaseSensitive &&
                                    tagsPerSearchable[nResultIdx][searchableTagIndex] == SearchedTags[searchedTagIndex]
                                    )
                                {
                                    bHasTag = true;
                                    break;
                                }
                            }
                        }

                        if (!bHasTag)
                        {
                            //a tag is missing, remove the result and its tags
                            categorySearchResult.RemoveAt(nResultIdx);
                            tagsPerSearchable.RemoveAt(nResultIdx);
                            --nResultIdx;
                        }
                    }
                }
                generalResults = categorySearchResult.ToArray();
            }
            return generalResults;
        }
    }
}