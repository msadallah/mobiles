﻿namespace AllMyScripts.Common.Utils
{
    using System.Collections.Generic;

    /// <summary>
    /// class used to perform a research in ISearchables
    /// </summary>
    /// <typeparam name="T">class(es) inheriting ISearchable</typeparam>
    public class Researcher<T> where T : ISearchable
    {
        protected List<T> searchables = new List<T>();
        public List<T> targets
        {
            get
            {
                return searchables;
            }
        }

        /// <summary>
        /// the character used to split search terms. ' ' by default
        /// </summary>
        public static readonly char s_separator = ' ';

        /// <summary>
        /// is the research case sensitive? False by default
        /// </summary>
        public bool isCaseSensitive = false;

        public Researcher(params T[] targets)
        {
            AddTarget(targets);
        }

        public void SetTargets(params T[] targets)
        {
            searchables.Clear();
            AddTarget(targets);
        }

        /// <summary>
        /// performs a research
        /// </summary>
        /// <param name="search">the term(s) to research</param>
        /// <returns>the results of the search</returns>
        public T[] Search(string search)
        {
            string[] SearchedTags = search.Split(s_separator);

            List<T> result = new List<T>(searchables);

            if (string.IsNullOrEmpty(search))
            {
                return result.ToArray();
            }

            List<List<string>> tagsPerSearchable = new List<List<string>>();

            foreach (ISearchable r in result)
            {
                tagsPerSearchable.Add(new List<string>(r.GetSearchableTags()));
            }

            for (int searchedTagIndex = 0; searchedTagIndex < SearchedTags.Length; ++searchedTagIndex)
            {
                //for each tag

                for (int nResultIdx = 0; nResultIdx < result.Count; ++nResultIdx)
                {
                    //check all the results that we haven't removed yet
                    bool bHasTag = false;
                    for (int searchableTagIndex = 0; searchableTagIndex < tagsPerSearchable[nResultIdx].Count; ++searchableTagIndex)
                    {
                        if (
                            //case insensitive test
                            !isCaseSensitive &&
                            tagsPerSearchable[nResultIdx][searchableTagIndex].ToLowerInvariant().Contains(SearchedTags[searchedTagIndex].ToLowerInvariant())

                            || //or
                               //case sensitive search
                            isCaseSensitive &&
                            tagsPerSearchable[nResultIdx][searchableTagIndex].Contains(SearchedTags[searchedTagIndex])
                            )
                        {
                            bHasTag = true;
                            break;
                        }
                    }

                    if (!bHasTag)
                    {
                        //a tag is missing, remove the result and its tags
                        result.RemoveAt(nResultIdx);
                        tagsPerSearchable.RemoveAt(nResultIdx);
                        --nResultIdx;
                    }
                }
            }
            return result.ToArray();
        }

		public static bool IsTagged(string search, ISearchable searchable, bool useCaseSensitive = false)
		{
			if (string.IsNullOrEmpty(search))
				return true;

			string[] searchingTags = search.Split(s_separator);
			string[] searchableTags = searchable.GetSearchableTags();
			
			for (int i = searchingTags.Length - 1; i >= 0; --i)
			{
				if (string.IsNullOrEmpty(searchingTags[i]))
					continue;

				for (int y = searchableTags.Length - 1; y >= 0; --y)
				{
					if (string.IsNullOrEmpty(searchableTags[y]))
						continue;

					if (useCaseSensitive && searchableTags[y].Contains(searchingTags[i]))
						return true;
					else if (!useCaseSensitive && searchableTags[y].ToLowerInvariant().Contains(searchingTags[i].ToLowerInvariant()))
						return true;
				}
			}

			return false;
		}

        public void AddTarget(params T[] v)
        {
            searchables.AddRange(v);
        }
        public void RemoveTarget(T v)
        {
            searchables.Remove(v);
        }

        public void Clear()
        {
            searchables.Clear();
        }
    }
}