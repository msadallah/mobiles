﻿namespace AllMyScripts.Common.Utils
{
    public interface ISearchable
    {
        /// <summary>
        /// returns the list of tags that match this object
        /// </summary>
        string[] GetSearchableTags();
    }

    public interface ICategorySearchable : ISearchable
    {
        /// <summary>
        /// returns the strings matching the category
        /// </summary>
        string[] GetCategoriesTags(string category);
    }
}