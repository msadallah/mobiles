
// LWS XML parser is enabled by default, to avoid using System.Xml class
// which is not available on Unity Flash builds
#define USE_LWXMLPARSER

namespace AllMyScripts.Common.Tools.Xml
{

    using UnityEngine;
    using System.IO;
    using AllMyScripts.Common.Tools;
#if !USE_LWXMLPARSER
    using System.Xml;
#endif
    public sealed class XmlReader
	{
		public delegate void ItemParseDelegate( XmlReader reader );
		public enum XmlNodeType { None, Element, CDATA, ProcessingInstruction, EndElement, WhiteSpace, Comment, XmlDeclaration, Text };

		public const string INNER_SUBTREE_ROOT_NAME = "INNER_SUBTREE";

#if USE_LWXMLPARSER
		XmlParser m_xr;
#else
		XmlReader m_xr;
#endif
	
		public XmlReader()
		{
		}
	
		public bool Init( string sXml )
		{
			return Init( sXml, false );
		}
	
		public bool Init( string sXml, bool bFromResource )
		{
			if( string.IsNullOrEmpty( sXml ) ) return false;
	#if USE_LWXMLPARSER
			m_xr = new XmlParser();
			m_xr.Create( sXml );
			return true;
	#else
			StringReader sr = new StringReader( sXml );
		#if !UNITY_4_0
			if( bFromResource ) sr.Read();	// skip BOM
		#endif
			m_xr = XmlReader.Create( sr );
			return( m_xr!=null );
	#endif
		}
	
		public void Close()
		{
			m_xr.Close();
		}
	
		public void Clean()
		{
			m_xr = null;
		}
	
		public bool Read()
		{
			return m_xr.Read();
		}
	
		public string ReadInnerXml()
		{
			return m_xr.ReadInnerXml();
		}

		public XmlReader ReadSubtree()
		{
			XmlReader xrInner = null;
			string sInnerXml = ReadInnerXml();
			if ( !string.IsNullOrEmpty( sInnerXml ) )
			{
				xrInner = new XmlReader();
				xrInner.Init ( "<" + INNER_SUBTREE_ROOT_NAME + ">" + sInnerXml + "</" + INNER_SUBTREE_ROOT_NAME + ">" );
			}
			return xrInner;
		}

		public XmlNodeType NodeType
		{
 			get { return GetNodeType(); }
		}
	
		public string Name
		{
 			get { return GetName(); }
		}
	
		public string Value
		{
 			get { return GetValue(); }
		}
	
		public bool IsNodeElement()
		{
	#if USE_LWXMLPARSER
			return ( m_xr.NodeType==XmlNodeType.Element );
	#else
			return ( m_xr.NodeType==XmlNodeType.Element );
	#endif
		}

		public bool HasEndElement()
		{
	#if USE_LWXMLPARSER
			return m_xr.HasEnd;
	#else
			return m_xr.IsEmptyElement;
	#endif
		}
	
		public XmlNodeType GetNodeType()
		{
	#if USE_LWXMLPARSER
			return m_xr.NodeType;
	#else
			switch( m_xr.NodeType )
			{
				case XmlNodeType.Element:				return XmlNodeType.Element;
				case XmlNodeType.EndElement:			return XmlNodeType.EndElement;
				case XmlNodeType.CDATA:					return XmlNodeType.CDATA;
				case XmlNodeType.ProcessingInstruction:	return XmlNodeType.ProcessingInstruction;
				case XmlNodeType.Comment:				return XmlNodeType.Comment;
				case XmlNodeType.Whitespace:			return XmlNodeType.WhiteSpace;
				case XmlNodeType.XmlDeclaration:		return XmlNodeType.XmlDeclaration;
			}
			Debug.LogWarning( "Type not yet implemented in XmlReader ! : " + m_xr.NodeType );
			return XmlNodeType.None;
	#endif
		}
	
		public string GetName()
		{
			return m_xr.Name;
		}
	
		public string GetValue()
		{
			return m_xr.Value;
		}

		public string[] GetAttributeNames()
		{
	#if USE_LWXMLPARSER
			return m_xr.GetAttributeNames();
	#else
			int nCount = m_xr.AttributeCount;
			string[] sNames = new string[nCount];
			for( int i=0; i<nCount; i++ )
			{
				m_xr.MoveToAttribute( i );
				sNames[i] = m_xr.Name;
			}
			m_xr.MoveToElement();
			return sNames;
	#endif
		}
	
		public string[] GetAttributeValues()
		{
	#if USE_LWXMLPARSER
			return m_xr.GetAttributeValues();
	#else
			int nCount = m_xr.AttributeCount;
			string[] sValues = new string[nCount];
			for( int i=0; i<nCount; i++ )
			{
				m_xr.MoveToAttribute( i );
				sValues[i] = m_xr.Value;
			}
			m_xr.MoveToElement();
			return sValues;
	#endif
		}

		public string GetAttribute( string sAttr )
		{
			return GetAttribute( sAttr, null );
		}
	
		public string GetAttribute( string sAttr, string sDefault )
		{
	#if USE_LWXMLPARSER
			string sValue = m_xr.GetAttribute( sAttr );
			if( string.IsNullOrEmpty( sValue ) )
				return sDefault;
			else
				return sValue;
	#else
			return XmlTools.GetAttribute( m_xr, sAttr, sDefault );
	#endif
		}
	
		public float GetAttributeFloat( string sAttr, float fDefault )
		{
	#if USE_LWXMLPARSER
			return ParseTools.ParseFloatSafe( GetAttribute( sAttr, fDefault.ToString() ) );
	#else
			return XmlTools.GetAttributeFloat( m_xr, sAttr, fDefault );
	#endif
		}
	
		public int GetAttributeInt( string sAttr, int nDefault )
		{
	#if USE_LWXMLPARSER
			return ParseTools.ParseIntSafe( GetAttribute( sAttr, nDefault.ToString() ) );
	#else
			return XmlTools.GetAttributeInt( m_xr, sAttr, nDefault );
	#endif
		}

		public uint GetAttributeUint( string sAttr, uint nDefault )
		{
	#if USE_LWXMLPARSER
			return ParseTools.ParseUIntSafe( GetAttribute( sAttr, nDefault.ToString() ) );
	#else
			return XmlTools.GetAttributeUint( m_xr, sAttr, nDefault );
	#endif
		}
	
		public bool GetAttributeBool( string sAttr, bool bDefault )
		{
	#if USE_LWXMLPARSER
			return GlobalTools.ReadBool( GetAttribute( sAttr, bDefault.ToString() ), bDefault );
	#else		   
			return XmlTools.GetAttributeBool( m_xr, sAttr, bDefault );
	#endif
		}

		public T GetAttributeEnum<T>( string sAttr, T nDefault )
		{
	#if USE_LWXMLPARSER
			return ParseTools.ParseEnumSafe<T>( GetAttribute( sAttr, nDefault.ToString() ), nDefault );
	#else
			return XmlTools.GetAttributeEnum<T>( m_xr, sAttr, nDefault );
	#endif
		}

		public Vector2 GetAttributeVector2( Vector2 vDefault )
		{
	#if USE_LWXMLPARSER
			return new Vector2( GetAttributeFloat( "x", vDefault.x ),		
								GetAttributeFloat( "y", vDefault.y ) );
	#else
			return XmlTools.GetAttributeVector2( m_xr, vDefault );
	#endif
		}

		public Vector3 GetAttributeVector3( Vector3 vDefault )
		{
	#if USE_LWXMLPARSER
			return new Vector3( GetAttributeFloat( "x", vDefault.x ),		
								GetAttributeFloat( "y", vDefault.y ),
								GetAttributeFloat( "z", vDefault.z ) );
	#else
			return XmlTools.GetAttributeVector3( m_xr, vDefault );
	#endif
		}

		public Color GetAttributeColor( string sAttr, Color cDefault )
		{
			Color c = cDefault;
			string sAttrContent = GetAttribute( sAttr, string.Empty );
			if( !string.IsNullOrEmpty( sAttrContent ) )
			{
				string[] values = sAttrContent.Split( new char[] { ',' } );
				if( values.Length==1 )
				{
					float fValue = ParseTools.ParseFloatSafe( values[ 0 ] );
					c.r = c.g = c.b = fValue;
					c.a = 1f;
				}
				else
				{
					float.TryParse( values[0], out c.r );
					float.TryParse( values[1], out c.g );
					float.TryParse( values[2], out c.b );
					if( values.Length>3 )
						float.TryParse( values[3], out c.a );
					else
						c.a = 1f;
				}
			}
			else
			{
				c = GetAttributeColor32( string.Concat( sAttr, "32" ), cDefault );
			}
			return c;
		}

		public Color32 GetAttributeColor32( string sAttr, Color32 cDefault )
		{
			Color32 c = cDefault;
			string sAttrContent = GetAttribute( sAttr, string.Empty );
			if( !string.IsNullOrEmpty( sAttrContent ) )
			{
				string[] values = sAttrContent.Split( new char[] { ',' } );
				if( values.Length==1 )
				{
					byte nValue = byte.Parse( values[0] );
					c.r = c.g = c.b = nValue;
					c.a = 255;
				}
				else
				{
					byte.TryParse( values[0], out c.r );
					byte.TryParse( values[1], out c.g );
					byte.TryParse( values[2], out c.b );
					if( values.Length>3 )
						byte.TryParse( values[3], out c.a );
					else
						c.a = 255;
				}
			}
			return c;
		}
	
		public Color GetAttributeColorHex( string sAttr, Color cDefault )
		{
			Color c = cDefault;
			string sAttrContent = GetAttribute( sAttr, string.Empty );
			if( !string.IsNullOrEmpty( sAttrContent ) )
			{
				uint nData = (uint)System.Convert.ToInt32( sAttrContent, 16 );
				byte b = (byte)( nData & 0xFF );
				nData >>= 8;
				byte g = (byte)( nData & 0xFF );
				nData >>= 8;
				byte r = (byte)( nData & 0xFF );
				return new Color( (float)r/255f, (float)g/255f, (float)b/255f, 1f );
			}
			return c;
		}

		public void ParseElement( string sName, ItemParseDelegate Dlg )
		{
			while( Read() && ( GetNodeType() != XmlNodeType.EndElement || !GetName().Equals(sName, System.StringComparison.OrdinalIgnoreCase )))
			{
				if( GetNodeType() == XmlNodeType.Element )
					Dlg( this );
			}
		}
	
		// ------------------------------------------------------------------
		// imported from MB 17/10/11
		// ------------------------------------------------------------------
		public bool FindStartElement( string sName )
		{
			sName = sName.ToUpper();
			while( Read() && !( IsNodeElement() && Name.Equals( sName, System.StringComparison.OrdinalIgnoreCase )));
			if( !IsNodeElement() || !Name.Equals( sName, System.StringComparison.OrdinalIgnoreCase ))
			{
				//Debug.Log( "Error in XML File, can't find expected " + sName );
				return false;
			}
			return true;
		}
	
		// ------------------------------------------------------------------
		// imported from MB 17/10/11
		// ------------------------------------------------------------------
		public float ReadFloatFromXml()
		{
			if( Read() )
				return ParseTools.ParseFloatSafe( GetValue() );
			else
				return 0f;
		}
	
		// ------------------------------------------------------------------
		// imported from MB 17/10/11
		// ------------------------------------------------------------------
		public Vector3 ReadVector3()
		{
			Vector3 vResult = Vector3.zero;
			if( FindStartElement( "X" ) ) vResult.x = ReadFloatFromXml();
			if( FindStartElement( "Y" ) ) vResult.y = ReadFloatFromXml();
			if( FindStartElement( "Z" ) ) vResult.z = ReadFloatFromXml();
			return vResult;
		}
	
		// ------------------------------------------------------------------
		// imported from MB 17/10/11
		// ------------------------------------------------------------------
		public Vector2 ReadVector2()
		{
			Vector2 vResult = Vector2.zero;
			if( FindStartElement( "X" ) ) vResult.x = ReadFloatFromXml();
			if( FindStartElement( "Y" ) ) vResult.y = ReadFloatFromXml();
			return vResult;
		}
	}
}
