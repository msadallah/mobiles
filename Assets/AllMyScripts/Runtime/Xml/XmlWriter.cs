namespace AllMyScripts.Common.Tools.Xml
{
    using UnityEngine;
    using System.Collections.Generic;
    using System.Globalization;

    public sealed class XmlWriter
	{
		private string m_sData;
		private bool m_bFormatting;
		private List<string> m_sNames;
		private bool m_bPendingStart;
		private bool m_bNodeContainsText;
	
		public XmlWriter( bool bFormatting=true, string sEncoding="utf-8" )
		{
			m_sData = "<?xml version=\"1.0\" encoding=\"" + sEncoding + "\"?>";
			m_bFormatting = bFormatting;
			m_sNames = new List<string>();
			m_bPendingStart = false;
			m_bNodeContainsText = false;
		}
	
		public string GetString()
		{
			return m_sData;
		}
	
		public void WriteStartElement( string sName )
		{
			GlobalTools.Assert( m_bNodeContainsText==false );
			CloseNode();
			m_bPendingStart = true;
			m_sNames.Add( sName );
			Indent();
			AddText( "<" + sName );
		}
	
		public void WriteEndElement()
		{
			int nIndex = m_sNames.Count-1;
			if( nIndex>=0 )
			{
				if( m_bPendingStart )
				{
					AddText( " />" );
				}
				else
				{
					if( m_bNodeContainsText==false )
					{
						Indent();
					}
					AddText( "</" + m_sNames[nIndex] + ">" );
					m_bNodeContainsText = false;
				}
				m_sNames.RemoveAt( nIndex );
			}
			m_bPendingStart = false;
		}
	
		public void WriteText( string sValue )
		{
			CloseNode();
			AddText( sValue );
			m_bNodeContainsText = true;
		}

		public void WriteCommentLine( string sValue, bool bWithSpaces=true )
		{
			CloseNode();
			Indent( 1 );
			if( bWithSpaces )
				AddText( "<!-- " + sValue + " -->" );
			else
				AddText( "<!--" + sValue + "-->" );
		}

		public void WriteAttribute( string sName, string sValue )
		{
			GlobalTools.Assert( m_bPendingStart );
			GlobalTools.Assert( m_bNodeContainsText==false );
			AddText( " " + sName + "=\"" + sValue + "\"" );
		}
	
		public void WriteAttributeFloat( string sName, float fValue )
		{
			WriteAttribute( sName, fValue.ToString( CultureInfo.InvariantCulture ) );
		}
	
		public void WriteAttributeInt( string sName, int nValue )
		{
			WriteAttribute( sName, nValue.ToString( CultureInfo.InvariantCulture ) );
		}

		public void WriteAttributeUint( string sName, uint nValue )
		{
			WriteAttribute( sName, nValue.ToString( CultureInfo.InvariantCulture ) );
		}

		public void WriteAttributeBool( string sName, bool bValue )
		{
			WriteAttribute( sName, bValue ? "true" : "false" );
		}
	
		public void WriteAttributeColor( string sName, Color cValue )
		{
			string sColor = cValue.r + "," + cValue.g + "," + cValue.b;
			if( Mathf.Approximately(cValue.a, 1f))
				sColor += "," + cValue.a.ToString( CultureInfo.InvariantCulture );
			WriteAttribute( sName, sColor );
		}

		public void WriteAttributeColor32( string sName, Color32 cValue )
		{
			string sColor = cValue.r + "," + cValue.g + "," + cValue.b;
			if( cValue.a!=255 )
				sColor += "," + cValue.a.ToString( CultureInfo.InvariantCulture );
			WriteAttribute( sName, sColor );
		}

		public void WriteAttributeVector2( Vector2 v2Value )
		{
			WriteAttributeFloat( "x", v2Value.x );
			WriteAttributeFloat( "y", v2Value.y );
		}

		public void WriteAttributeVector3( Vector3 v3Value )
		{
			WriteAttributeFloat( "x", v3Value.x );
			WriteAttributeFloat( "y", v3Value.y );
			WriteAttributeFloat( "z", v3Value.z );
		}

		private void AddText( string sText )
		{
			m_sData += sText;
		}
	
		private void CloseNode()
		{
			if( m_bPendingStart )
			{
				AddText( ">" );
				m_bPendingStart = false;
			}
		}
	
		private void Indent( int nTabs=0 )
		{
			if( m_bFormatting )
			{
				AddText( "\r\n" );
				for( int i=0; i<m_sNames.Count-1+nTabs; i++ )
				{
					AddText( "\t" );
				}
			}
		}
	}
}
