namespace AllMyScripts.Common.Tools.Xml
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    public sealed class XmlParser
	{
		internal sealed class DefaultHandler : XmlParser.IContentHandler
		{
			public void OnStartParsing( XmlParser parser )
			{
				//UnityEngine.Debug.Log( "### OnStartParsing" );
			}

			public void OnEndParsing( XmlParser parser )
			{
				//UnityEngine.Debug.Log( "### OnEndParsing" );
			}

			public void OnStartElement( string sName, XmlParser.IAttrList attrs )
			{
				//UnityEngine.Debug.Log( "### OnStartElement " + sName + " ## " + attrs.Length );
				//for( int i=0; i<attrs.Length; i++ )
				//{
				//	UnityEngine.Debug.Log( "\t### elem : " + i + " : " + attrs.GetName( i ) + " ## " + attrs.GetValue( i ) );
				//}
			}

			public void OnEndElement( string sName )
			{
				//UnityEngine.Debug.Log( "### OnEndElement " + sName );
			}

			public void OnChars( string sText )
			{
				//UnityEngine.Debug.Log( "### OnChars " + sText );
			}

			public void OnIgnorableWhitespace( string sText )
			{
				//UnityEngine.Debug.Log( "### OnIgnorableWhitespace " + sText );
			}

			public void OnProcessingInstruction( string sName, string sText )
			{
				//UnityEngine.Debug.Log( "### OnProcessingInstruction " + sName + " ## " + sText );
			}
		}
	
		public interface IContentHandler
		{
			void OnStartParsing( XmlParser parser );
			void OnEndParsing( XmlParser parser );
			void OnStartElement( string sName, IAttrList attrs );
			void OnEndElement( string sName );
			void OnProcessingInstruction( string sName, string sText );
			void OnChars( string sText );
			void OnIgnorableWhitespace( string sText );
		}
	
		public interface IAttrList
		{
			int Length { get; }
			bool IsEmpty { get; }
			string GetName( int nIndex );
			string GetValue( int nIndex );
			string GetValue( string sName );
			string[] Names { get; }
			string[] Values { get; }
		}

		sealed class AttrListImpl : IAttrList
		{
			public int Length
			{
				get { return m_attrNames.Count; }
			}
			public bool IsEmpty
			{
				get { return m_attrNames.Count==0; }
			}
			public string GetName( int nIndex )
			{
				return m_attrNames[nIndex];
			}
			public string GetValue( int nIndex )
			{
				return m_attrValues[nIndex];
			}
			public string GetValue( string sName )
			{
				for( int i=0; i<Length; i++ )
				{
					if( GetName( i )==sName )
					{
						return GetValue( i );
					}
				}
				return null;
			}
			public string[] Names
			{
				get { return (string[])m_attrNames.ToArray(); }
			}
			public string[] Values
			{
				get { return (string[])m_attrValues.ToArray(); }
			}

			private List<string> m_attrNames = new List<string>();
			private List<string> m_attrValues = new List<string>();

			internal void Clear()
			{
				m_attrNames.Clear();
				m_attrValues.Clear();
			}

			internal void Add( string sName, string sValue )
			{
				m_attrNames.Add( sName );
				m_attrValues.Add( sValue );
			}
		}
	
		private IContentHandler m_handler;
		private StringReader m_reader;
		private Stack m_elementNames = new Stack();
		private Stack m_xmlSpaces = new Stack();
		private string m_sXmlSpace;
		private StringBuilder m_buffer = new StringBuilder(200);
		private StringBuilder m_nameBuffer = new StringBuilder(32);
		private StringBuilder m_commentBuffer = new StringBuilder(128);
		private string m_sName;
		private string m_sValue;
		private bool m_bHasEnd;
		private XmlReader.XmlNodeType m_NodeType = XmlReader.XmlNodeType.None;
		private bool m_bIsWhitespace;
		private AttrListImpl m_attributes = new AttrListImpl();
		private int m_nLine = 1;
		private int m_nColumn;
		
		private Exception Error( string sMsg )
		{
			return new XmlParserException( sMsg, m_nLine, m_nColumn );
		}

		private Exception UnexpectedEndError()
		{
			string[] sArray = new string[m_elementNames.Count];
			// COMPACT FRAMEWORK NOTE: CopyTo is not visible through the Stack class
			(m_elementNames as ICollection).CopyTo( sArray, 0 );
			return Error( String.Format( "Unexpected end of stream. Element stack content is {0}", String.Join( ",", sArray ) ) );
		}

		private bool IsNameChar( char cValue, bool bStart )
		{
			switch( cValue )
			{
			case ':':
			case '_':
				return true;
			case '-':
			case '.':
				return !bStart;
			}
			if( cValue>0x100 )
			{
				// optional condition for optimization
				switch( cValue )
				{
				case '\u0559':
				case '\u06E5':
				case '\u06E6':
					return true;
				}
				if( '\u02BB'<=cValue && cValue<='\u02C1' )
				{
					return true;
				}
			}
		
			switch( cValue )
			{
			case ' ':
			case '\t':
			case '\n':
			case '=':
			case '"':
			case '>':
			case '<':	
			case '?':
			case '/':
				return false;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				return !bStart;
			default:
				return true;
			}
		
			/*switch( Char.GetUnicodeCategory( c ) )
			{
			case UnicodeCategory.LowercaseLetter:
			case UnicodeCategory.UppercaseLetter:
			case UnicodeCategory.OtherLetter:
			case UnicodeCategory.TitlecaseLetter:
			case UnicodeCategory.LetterNumber:
				return true;
			case UnicodeCategory.SpacingCombiningMark:
			case UnicodeCategory.EnclosingMark:
			case UnicodeCategory.NonSpacingMark:
			case UnicodeCategory.ModifierLetter:
			case UnicodeCategory.DecimalDigitNumber:
				return !start;
			default:
				return false;
			}*/
		}

		private bool IsWhitespace( int nChar )
		{
			switch( nChar )
			{
			case ' ':
			case '\r':
			case '\t':
			case '\n':
				return true;
			default:
				return false;
			}
		}

		private void SkipWhitespaces()
		{
			SkipWhitespaces( false );
		}

		private void HandleWhitespaces()
		{
			while( IsWhitespace( Peek() ) )
			{
				m_buffer.Append( (char)ReadChar() );
			}
			if( Peek()!='<' && Peek()>=0 )
			{
				m_bIsWhitespace = false;
			}
		}

		private void SkipWhitespaces( bool bExpected )
		{
			while( true )
			{
				if( IsWhitespace( Peek() ) )
				{
					ReadChar();
					if( bExpected ) bExpected = false;
					continue;
				}
				if( bExpected ) throw Error( "Whitespace is expected, got "+Peek() );
				return;
			}
		}

		private int Peek()
		{
			return m_reader.Peek();
		}

		private int ReadChar()
		{
			int nChar = m_reader.Read();
			if( nChar=='\n' )
			{
				m_nLine++;
				m_nColumn = 1;
			}
			else
			{
				m_nColumn++;
			}
			return nChar;
		}

		private void Expect( int nChar )
		{
			int nRead = ReadChar();
			if( nRead<0 ) throw UnexpectedEndError();
			else if( nRead!=nChar ) throw Error( String.Format( "Expected '{0}' but got {1}", (char)nChar, (char)nRead ) );
		}

		private string ReadUntil( char cUntil, bool bHandleReferences )
		{
			while( true )
			{
				if( Peek()<0 ) throw UnexpectedEndError();
				char c = (char)ReadChar();
				if( c==cUntil )
					break;
				else if( bHandleReferences && c=='&' )
					ReadReference();
				else
					m_buffer.Append( c );
			}
			string sReturn = m_buffer.ToString();
			m_buffer.Length = 0;
			return sReturn;
		}

		private string ReadName()
		{
			m_nameBuffer.Length = 0;
		
			if( Peek()<0 || !IsNameChar( (char)Peek(), true ) ) throw Error( "XML name start character is expected." );
		
			for( int i=Peek(); i>=0; i=Peek() )
			{
				char c = (char)i;
				if( !IsNameChar( c, false ) ) break;
				m_nameBuffer.Append( c );
				ReadChar();
			}
		
			if( m_nameBuffer.Length==0 ) throw Error( "Valid XML name is expected." );
		
			return m_nameBuffer.ToString();
		}


		public void Parse( StringReader input, IContentHandler handler )
		{
			this.m_reader = input;
			this.m_handler = handler;
		
			if( m_handler!=null ) m_handler.OnStartParsing( this );

			while( Peek()>=0 ) ReadContent();
			HandleBufferedContent();
			if( m_elementNames.Count>0 ) throw Error( String.Format( "Insufficient close tag: {0}", m_elementNames.Peek() ) );

			if( m_handler!=null ) m_handler.OnEndParsing( this );

			Cleanup();
		}
	
		private void Cleanup()
		{
			m_nLine = 1;
			m_nColumn = 0;
			m_handler = null;
			Close();
	#if CF_1_0
			m_elementNames = new Stack();
			m_xmlSpaces = new Stack();
	#else
			m_elementNames.Clear();
			m_xmlSpaces.Clear();
	#endif
			m_attributes.Clear();
			m_buffer.Length = 0;
			m_sXmlSpace = null;
			m_bIsWhitespace = false;
			m_NodeType = XmlReader.XmlNodeType.None;
		}

		private bool ReadContent()
		{
			if( IsWhitespace( Peek() ) )
			{
				if( m_buffer.Length==0 ) m_bIsWhitespace = true;
				HandleWhitespaces();
			}
			if( Peek()!='<' )
			{
				m_NodeType = XmlReader.XmlNodeType.Text;
				m_buffer.Length = 0;
				ReadCharacters();
				m_sValue = m_buffer.ToString();
				m_buffer.Length = 0;
				return false;
			}
			ReadChar();
			switch( Peek() )
			{
				case '!':
				// declarations
					m_NodeType = XmlReader.XmlNodeType.CDATA;
					ReadChar();
					if( Peek()=='[' )
					{
						ReadChar();
						if( ReadName()!="CDATA" ) throw Error( "Invalid declaration markup" );
						Expect( '[' );
						ReadCDATASection();
						break;
					}
					else if( Peek()=='-' )
					{
						m_NodeType = XmlReader.XmlNodeType.Comment;
						m_sValue = ReadComment();
						break;
					}
					else if( ReadName()!="DOCTYPE" ) throw Error( "Invalid declaration markup." );
					else throw Error( "This parser does not support document type." );
			
				case '?':
				// PIs
					m_NodeType = XmlReader.XmlNodeType.ProcessingInstruction;
					HandleBufferedContent();
					ReadChar();
					m_sName = ReadName();
					SkipWhitespaces();
					string sText = String.Empty;
					if( Peek()!='?' )
					{
						while( true )
						{
							sText += ReadUntil( '?', false );
							if( Peek()=='>' ) break;
							sText += "?";
						}
					}
					m_bHasEnd = true;
					if( m_handler!=null ) m_handler.OnProcessingInstruction( m_sName, sText );
					Expect( '>' );
					break;
			
				case '/':
				// end tags
					m_NodeType = XmlReader.XmlNodeType.EndElement;
					HandleBufferedContent();
					if( m_elementNames.Count==0 ) throw UnexpectedEndError();
					ReadChar();
					m_sName = ReadName();
					SkipWhitespaces();
					string sExpected = (string)m_elementNames.Pop();
					m_xmlSpaces.Pop();
					if( m_xmlSpaces.Count>0 )
						m_sXmlSpace = (string)m_xmlSpaces.Peek();
					else
						m_sXmlSpace = null;
					if( m_sName!=sExpected ) throw Error( String.Format( "End tag mismatch: expected {0} but found {1}", sExpected, m_sName ) );
					m_bHasEnd = true;
					if( m_handler!=null ) m_handler.OnEndElement( m_sName );
					Expect( '>' );
					break;
			
				default:
				// start tags (including empty tags)
					m_NodeType = XmlReader.XmlNodeType.Element;
					HandleBufferedContent();
					m_sName = ReadName();
					m_bHasEnd = false;
					m_attributes.Clear();
					while( Peek()!='>' && Peek()!='/' )
					{
						ReadAttribute( m_attributes );
					}
					if( m_handler!=null ) m_handler.OnStartElement( m_sName, m_attributes );
					SkipWhitespaces();
					if( Peek()=='/' )
					{
						ReadChar();
						m_bHasEnd = true;
						if( m_handler!=null ) m_handler.OnEndElement( m_sName );
					}
					else
					{
						m_elementNames.Push( m_sName );
						m_xmlSpaces.Push( m_sXmlSpace );
					}
					Expect( '>' );
					break;
			}
			return true;
		}

		private void HandleBufferedContent()
		{
			if( m_buffer.Length==0 ) return;
			if( m_bIsWhitespace )
			{
				if( m_handler!=null ) m_handler.OnIgnorableWhitespace( m_buffer.ToString() );
			}
			else
			{
				if( m_handler!=null ) m_handler.OnChars( m_buffer.ToString() );
			}
			m_buffer.Length = 0;
			m_bIsWhitespace = false;
		}

		private void ReadCharacters()
		{
			m_bIsWhitespace = false;
			while( true )
			{
				int i = Peek();
				switch( i )
				{
				case -1:
				case '<':
					return;
				case '&':
					ReadChar();
					ReadReference();
					continue;
				default:
					m_buffer.Append( (char)ReadChar() );
					continue;
				}
			}
		}

		private void ReadReference()
		{
			if( Peek()=='#' )
			{
				// character reference
				ReadChar();
				ReadCharacterReference();
			}
			else
			{
				string sName = ReadName();
				Expect( ';' );
				switch( sName )
				{
				case "amp":
					m_buffer.Append( '&' );
					break;
				case "quot":
					m_buffer.Append( '"' );
					break;
				case "apos":
					m_buffer.Append( '\'' );
					break;
				case "lt":
					m_buffer.Append( '<' );
					break;
				case "gt":
					m_buffer.Append( '>' );
					break;
				default:
					throw Error( "General non-predefined entity reference is not supported in this parser." );
				}
			}
		}

		private int ReadCharacterReference()
		{
			int n = 0;
			if( Peek()=='x' )
			{
				// hex
				ReadChar();
				for( int i=Peek(); i>=0; i=Peek() )
				{
					if( '0'<=i && i<='9' )
						n = n << 4 + i - '0';
					else if( 'A'<=i && i<='F' )
						n = n << 4 + i - 'A' + 10;
					else if( 'a'<=i && i<='f' )
						n = n << 4 + i - 'a' + 10;
					else
						break;
					ReadChar();
				}
			}
			else
			{
				for( int i=Peek(); i>=0; i=Peek() )
				{
					if( '0'<=i && i<='9' )
						n = n << 4 + i - '0';
					else
						break;
					ReadChar();
				}
			}
			return n;
		}

		private void ReadAttribute( AttrListImpl a )
		{
			SkipWhitespaces( true );
		
			if( Peek()=='/' || Peek()=='>' )
			{
				// came here just to spend trailing whitespaces
				return;
			}

			string sName = ReadName();
			string sValue;
			SkipWhitespaces();
			Expect( '=' );
			SkipWhitespaces();
			switch( ReadChar() )
			{
			case '\'':
				sValue = ReadUntil( '\'', true );
				break;
			case '"':
				sValue = ReadUntil( '"', true );
				break;
			default:
				throw Error( "Invalid attribute value markup." );
			}
			if( sName=="xml:space" ) m_sXmlSpace = sValue;
			a.Add( sName, sValue );
		}

		private void ReadCDATASection()
		{
			int nBracket = 0;
			while( true )
			{
				if( Peek()<0 ) throw UnexpectedEndError();
				char c = (char)ReadChar();
				if( c==']' )
				{
					nBracket++;
				}
				else if( c=='>' && nBracket>1 )
				{
					for( int i=nBracket; i>2; i-- )
					{
						m_buffer.Append( ']' );
					}
					break;
				}
				else
				{
					for( int i=0; i<nBracket; i++ )
					{
						m_buffer.Append( ']' );
					}
					nBracket = 0;
					m_buffer.Append( c );
				}
			}
		}

		private string ReadComment()
		{
			Expect( '-' );
			Expect( '-' );
			m_commentBuffer.Length = 0;
			while( true )
			{
				char c = (char)ReadChar();
				m_commentBuffer.Append( c );
				if( c!='-' ) continue;
				c = (char)ReadChar();
				m_commentBuffer.Append( c );
				if( c!='-' ) continue;
				c = (char)ReadChar();
				m_commentBuffer.Append( c );
				if( c!='>' ) throw Error( "'--' is not allowed inside comment markup." );
				break;
			}
			m_commentBuffer.Remove( m_commentBuffer.Length-3, 3 );
			return m_commentBuffer.ToString();
		}
	
		public void Create( string sXml )
		{
			this.m_reader = new StringReader( sXml );
			this.m_handler = null;
		}
	
		public void Close()
		{
			this.m_reader = null;
		}

		public bool Read()
		{
			if( m_reader!=null )
			{
				if( Peek()>=0 )
				{
					ReadContent();
					return true;
				}
				else
				{
					HandleBufferedContent();
					if( m_elementNames.Count>0 ) throw Error( String.Format( "Insufficient close tag: {0}", m_elementNames.Peek() ) );
					//Cleanup();
					return false;
				}
			}
			return false;
		}

		public string ReadInnerXml()
		{
			if( HasEnd ) return string.Empty;
		
			int nPos = m_reader.Position;
			int nLastPos = nPos;
			int nRead = 0;
			int nDepth = 1;
			string sName = Name.ToUpper();
		
			while( Read() )
			{
				nRead = m_reader.Position - nLastPos;
				nLastPos = m_reader.Position;
				if( NodeType == XmlReader.XmlNodeType.Element && !HasEnd )
				{
					nDepth++;
				}
				else if( NodeType == XmlReader.XmlNodeType.EndElement )
				{
					nDepth--;
					if( nDepth==0 && Name.Equals( sName, StringComparison.OrdinalIgnoreCase ))
					{
						break;
					}
				}
			}
		
			int nSize = nLastPos - nPos - nRead;
			m_reader.Position = nPos;
		
			string sContent = m_reader.Read( nSize );
			m_reader.Skip( nRead );
			return sContent;
		}
	
		public XmlReader.XmlNodeType NodeType
		{
			get { return m_NodeType; }
		}
	
		public string Name
		{
			get { return m_sName; }
		}

		public string Value
		{
			get { return m_sValue; }
		}

		public bool HasEnd
		{
			get { return m_bHasEnd; }
		}
	
		public string GetAttribute( string sAttr )
		{
			if( m_attributes!=null )
				return m_attributes.GetValue( sAttr );
			else
				return null;
		}
	
		public string[] GetAttributeNames()
		{
			if( m_attributes!=null )
				return m_attributes.Names;
			else
				return null;
		}
	
		public string[] GetAttributeValues()
		{
			if( m_attributes!=null )
				return m_attributes.Values;
			else
				return null;
		}
	}

	internal sealed class XmlParserException : SystemException
	{
		int m_nLine;
		int m_nColumn;

		public XmlParserException( string sMsg, int nLine, int nColumn )
		: base( String.Format( "{0}. At ({1},{2})", sMsg, nLine, nColumn ) )
		{
			m_nLine = nLine;
			m_nColumn = nColumn;
		}

		public int Line
		{
			get { return m_nLine; }
		}

		public int Column
		{
			get { return m_nColumn; }
		}
	}
}
