// Enable the following defines only if you want to use System.Xml class,
// which is not available on Unity Flash builds
#if UNITY_EDITOR
#define SYSTEMXML_READ
#define SYSTEMXML_WRITE
#endif


namespace AllMyScripts.Common.Tools.Xml
{
    using UnityEngine;
    using System.IO;
#if SYSTEMXML_READ || SYSTEMXML_WRITE
    using System.Xml;
#endif

    using AllMyScripts.Common.Tools;

    public sealed class XmlTools
	{
	#if SYSTEMXML_READ
		static public float ReadFloat( ref System.Xml.XmlReader xr )
		{
			if ( xr.Read() )
				return ParseTools.ParseFloatSafe( xr.Value );
			return 0;
		}
	
		static public int ReadInt( ref System.Xml.XmlReader xr )
		{
			if ( xr.Read() )
				return ParseTools.ParseIntSafe( xr.Value );
			return 0;
		}

		static public Vector3 ReadVector3( ref System.Xml.XmlReader xr )
		{
			Vector3 vResult = Vector3.zero;
			if ( FindStartElement( "X", ref xr ) )
				vResult.x = ReadFloat( ref xr );
			if ( FindStartElement( "Y", ref xr ) )
				vResult.y = ReadFloat( ref xr );
			if ( FindStartElement( "Z", ref xr ) )
				vResult.z = ReadFloat( ref xr );
			return vResult;
		}

		static public bool FindStartElement( string sName, ref System.Xml.XmlReader xr )
		{
			sName = sName.ToUpper();
			while( xr.Read() && !( xr.NodeType == XmlNodeType.Element && xr.Name.Equals(sName, System.StringComparison.OrdinalIgnoreCase )));
			if ( xr.NodeType != XmlNodeType.Element || !xr.Name.Equals(sName, System.StringComparison.OrdinalIgnoreCase ))
			{
				Debug.LogError( "Error in XML File, can't find expected " + sName );
				return false;
			}
			return true;
		}
	
		static public string GetAttribute( System.Xml.XmlReader xr, string sAttribute, string sDefault )
		{
			try
			{
				string sResult = xr.GetAttribute( sAttribute );
				if ( string.IsNullOrEmpty( sResult ) )
					return sDefault;
				else
					return sResult;
			}
			catch
			{
				return sDefault;
			}
		}
	
		static public string GetAttribute( XmlNode node, string sAttribute, string sDefault )
		{
			try
			{
				string sResult = node.Attributes[sAttribute].InnerXml;

				if ( string.IsNullOrEmpty( sResult ) )
					return sDefault;

				return sResult;
			}
			catch
			{
				return sDefault;
			}
		}
	
		static public T GetAttributeEnum<T>( System.Xml.XmlReader xr, string sAttribute, T nDefault )
		{
			return ParseTools.ParseEnumSafe<T>( GetAttribute( xr, sAttribute, nDefault.ToString() ), nDefault );
		}

		static public int GetAttributeInt( System.Xml.XmlReader xr, string sAttribute, int nDefault )
		{
			return ParseTools.ParseIntSafe( GetAttribute( xr, sAttribute, nDefault.ToString() ) );
		}
		static public int GetAttributeInt( System.Xml.XmlNode node, string sAttribute, int nDefault )
		{
			return ParseTools.ParseIntSafe( GetAttribute( node, sAttribute, nDefault.ToString() ) );
		}

		static public uint GetAttributeUint( System.Xml.XmlReader xr, string sAttribute, uint nDefault )
		{
			return ParseTools.ParseUIntSafe( GetAttribute( xr, sAttribute, nDefault.ToString() ) );
		}

		static public float GetAttributeFloat( System.Xml.XmlReader xr, string sAttribute, float fDefault )
		{
			return ParseTools.ParseFloatSafe( GetAttribute( xr, sAttribute, fDefault.ToString() ) );
		}
	
		static public bool GetAttributeBool( System.Xml.XmlReader xr, string sAttribute, bool bDefault )
		{
			return GlobalTools.ReadBool( GetAttribute( xr, sAttribute, bDefault.ToString() ), bDefault );
		}
	
		static public Vector2 GetAttributeVector2( System.Xml.XmlReader xr, Vector2 vDefault )
		{
			return new Vector2( GetAttributeFloat( xr, "x", vDefault.x ),		
								GetAttributeFloat( xr, "y", vDefault.y ) );
		}
		static public Vector3 GetAttributeVector3( System.Xml.XmlReader xr, Vector3 vDefault )
		{
			return new Vector3( GetAttributeFloat( xr, "x", vDefault.x ),		
								GetAttributeFloat( xr, "y", vDefault.y ),
								GetAttributeFloat( xr, "z", vDefault.z ) );
		}
		static public Vector3 GetAttributeVectorScale3( System.Xml.XmlReader xr, Vector3 vDefault )
		{
			return new Vector3( GetAttributeFloat( xr, "scalex", vDefault.x ),		
								GetAttributeFloat( xr, "scaley", vDefault.y ),
								GetAttributeFloat( xr, "scalez", vDefault.z ) );
		}

		static public System.Xml.XmlReader OpenFile( string sPath, bool bGameSpecificPath )
		{
			string sXml = string.Empty;
			string sFileNameWithoutExt = Path.GetFileNameWithoutExtension( sPath );
			string sFileName = Path.GetDirectoryName( sPath ) + "/" + sFileNameWithoutExt;
			sXml = GlobalTools.LoadTextFile( sFileName, "xml" );
			if ( string.IsNullOrEmpty( sXml ) )
			{
				Debug.LogError( "Can't read XML file " + sFileName );
				return null;
			}
			else
			{
				return System.Xml.XmlReader.Create( new System.IO.StringReader(sXml) );
			}
		}
	#endif

	#if SYSTEMXML_WRITE
		static public XmlWriterSettings CreateSettingsUTF8NoBOM()
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = ( "\t" );
			settings.Encoding = new System.Text.UTF8Encoding(false); // The false means, do not emit the BOM. 
			return settings;
		}
	#endif

		static public void WriteShallowNode( XmlReader reader, XmlWriter writer )
		{
			switch( reader.NodeType )
			{
				case XmlReader.XmlNodeType.Element:
					writer.WriteStartElement( reader.Name );
					string[] sNames = reader.GetAttributeNames();
					string[] sValues = reader.GetAttributeValues();
					for( int i=0; i<sNames.Length; i++ )
					{
						writer.WriteAttribute( sNames[i], sValues[i] );
					}
					if( reader.HasEndElement() ) writer.WriteEndElement();
					break;
				case XmlReader.XmlNodeType.Text:
					writer.WriteText( reader.Value );
					break;
				case XmlReader.XmlNodeType.WhiteSpace:
					writer.WriteText( " " );
					break;
				case XmlReader.XmlNodeType.Comment:
					writer.WriteCommentLine( reader.Value, false );
					break;
				case XmlReader.XmlNodeType.EndElement:
					writer.WriteEndElement();
					break;
			}
		}

	#if SYSTEMXML_READ && SYSTEMXML_WRITE
		static public void WriteShallowNode( System.Xml.XmlReader reader, System.Xml.XmlWriter writer )
		{
			switch( reader.NodeType )
			{
				case XmlNodeType.Element:
					writer.WriteStartElement( reader.Prefix, reader.LocalName, reader.NamespaceURI );
					writer.WriteAttributes( reader, true );
					if( reader.IsEmptyElement ) writer.WriteEndElement();
					break;
				case XmlNodeType.Text:
					writer.WriteString( reader.Value );
					break;
				case XmlNodeType.SignificantWhitespace:
					writer.WriteWhitespace( reader.Value );
					break;
				case XmlNodeType.CDATA:
					writer.WriteCData( reader.Value );
					break;
				case XmlNodeType.EntityReference:
					writer.WriteEntityRef( reader.Name );
					break;
				case XmlNodeType.XmlDeclaration:
				case XmlNodeType.ProcessingInstruction:
					writer.WriteProcessingInstruction( reader.Name, reader.Value );
					break;
				case XmlNodeType.DocumentType:
					writer.WriteDocType( reader.Name, reader.GetAttribute( "PUBLIC" ), reader.GetAttribute( "SYSTEM" ), reader.Value );
					break;
				case XmlNodeType.Comment:
					writer.WriteComment( reader.Value );
					break;
				case XmlNodeType.EndElement:
					writer.WriteFullEndElement();
					break;
			}
		}
	#endif
	}
}
