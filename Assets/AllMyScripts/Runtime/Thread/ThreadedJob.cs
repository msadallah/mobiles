﻿namespace AllMyScripts.Common.Thread
{
	using System.Threading;
    using System.Collections;

	/// <summary>
	/// Class imported from : https://answers.unity.com/questions/357033/unity3d-and-c-coroutines-vs-threading.html
	/// Manage a thread. You need to create your own Thread (with your custom code) heriting from this class
	/// </summary>
	public class ThreadedJob
	{
		private bool _isDone = false;
		private object _handle = new object();
		private Thread _thread = null;

		public bool IsDone
		{
			get
			{
				bool tmp;
				lock (_handle)
				{
					tmp = _isDone;
				}
				return tmp;
			}
			set
			{
				lock (_handle)
				{
					_isDone = value;
				}
			}
		}

		public virtual void Start()
		{
			_thread = new Thread(Run);
			_thread.Start();
		}

		public virtual void Abort()
		{
			_thread.Abort();
		}

		protected virtual void ThreadFunction() { }

		protected virtual void OnFinished() { }

		public virtual bool Update()
		{
			if (IsDone)
			{
				OnFinished();
				return true;
			}
			return false;
		}

		public IEnumerator WaitFor()
		{
			while (!Update())
				yield return null;
		}

		private void Run()
		{
			ThreadFunction();
			IsDone = true;
		}
	}
}