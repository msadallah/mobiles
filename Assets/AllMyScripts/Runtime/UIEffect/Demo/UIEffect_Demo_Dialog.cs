﻿using UnityEngine;

namespace Coffee.UIExtensions
{
	public class UIEffect_Demo_Dialog : MonoBehaviour
	{
		[SerializeField]
		private Animator m_Animator = default;

		public void Open()
		{
			gameObject.SetActive(true);
		}

		public void Close()
		{
			m_Animator.SetTrigger("Close");
		}

		public void Closed()
		{
			gameObject.SetActive(false);
		}
	}
}