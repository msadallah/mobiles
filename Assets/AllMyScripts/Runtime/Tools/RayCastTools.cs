﻿namespace AllMyScripts.Common.Tools
{
	using UnityEngine;

	public static class RayCastTools
	{
		public static int SortRaycastHit(RaycastHit i1, RaycastHit i2)
		{
			return i1.distance.CompareTo(i2.distance);
		}
	}
}