﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AllMyScripts.Common.Tools
{
	public static class LayerSetterInHierarchy
	{
		public static void SetLayerInGameObjectAndChildren(GameObject go, string layer)
		{
			SetLayerInGameObjectAndChildren(go, LayerMask.NameToLayer(layer));
		}
		public static void SetLayerInGameObjectAndChildrenWithComponent<T>(GameObject go, string layer) where T : Component
		{
			SetLayerInGameObjectAndChildrenWithComponent<T>(go, LayerMask.NameToLayer(layer));
		}
		public static void SetLayerInGameObjectAndChildren(GameObject go, int layer)
		{
			SetLayerInGameObjectAndChildrenWithComponent<Transform>(go, layer);
		}
		public static void SetLayerInGameObjectAndChildrenWithComponent<T>(GameObject go, int layer) where T : Component
		{
			T[]components = go.GetComponentsInChildren<T>(true);
			if (components != null && components.Length > 0)
			{
				for (int i = 0; i < components.Length; ++i)
				{
					components[i].gameObject.layer = layer;
				}
			}
		}
	}

}