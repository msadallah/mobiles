﻿namespace AllMyScripts.Common.Tools
{
#if !UNITY_EDITOR
using System.Diagnostics;
#endif
	using AllMyScripts.Common.Tools.DebugHelper;
	using UnityEngine;
	public class DLog
	{
		public static void Log(string log, Object context = null)
		{
#if USELOG
#if !UNITY_EDITOR
		StackTrace stackTrace = new StackTrace();
		log += "\n\n" + stackTrace.ToString();
#endif
			UnityEngine.Debug.Log(log, context);
#endif
		}

		public static void LogWarning(string log, Object context = null)
		{
#if USELOG
#if !UNITY_EDITOR
		StackTrace stackTrace = new StackTrace();
		log += "\n\n" + stackTrace.ToString();
#endif
			UnityEngine.Debug.LogWarning(log, context);
#endif
		}

		public static void LogError(string log, Object context = null)
		{
#if USELOG
#if !UNITY_EDITOR
		StackTrace stackTrace = new StackTrace();
		log += "\n\n" + stackTrace.ToString();
#endif
			UnityEngine.Debug.LogError(log, context);
#endif
		}

		public static void LogWebservice(string log, Object context = null)
		{
#if USELOG
#if UNITY_EDITOR
			log += "\n[CUSTOMLOGTYPE: WEBSERVICE]\n";
#else
		StackTrace stackTrace = new StackTrace();
		log += "\n\n" + stackTrace.ToString();
#endif
			UnityEngine.Debug.Log(log, context);
#endif
		}

		public static void LogWarningWebservice(string log, Object context = null)
		{
#if USELOG
#if UNITY_EDITOR
			log += "\n[CUSTOMLOGTYPE: WARNINGWEBSERVICE]\n";
#else
		StackTrace stackTrace = new StackTrace();
		log += "\n\n" + stackTrace.ToString();
#endif
			UnityEngine.Debug.Log(log, context);
#endif
		}

		#region DebugKey
		public static void Log<T>(string log, T enumKey, Object context = null) where T : System.Enum
		{
			Log(log, enumKey.ToString(), context);
		}
		public static void Log(string log, string key, Object context = null)
		{
			if (DebugKeys.ShowDebugLog(key))
			{
				Log(log, context);
			}
		}
		public static void LogWarning<T>(string log, T enumKey, Object context = null) where T : System.Enum
		{
			LogWarning(log, enumKey.ToString(), context);
		}
		public static void LogWarning(string log, string key, Object context = null)
		{
			if (DebugKeys.ShowDebugLog(key))
			{
				LogWarning(log, context);
			}
		}
		public static void LogError<T>(string log, T enumKey, Object context = null) where T : System.Enum
		{
			LogError(log, enumKey.ToString(), context);
		}
		public static void LogError(string log, string key, Object context = null)
		{
			if (DebugKeys.ShowDebugLog(key))
			{
				LogError(log, context);
			}
		}

		public static void LogWebservice<T>(string log, T enumKey, Object context = null) where T : System.Enum
		{
			LogWebservice(log, enumKey.ToString(), context);
		}
		public static void LogWebservice(string log, string key, Object context = null)
		{
			if (DebugKeys.ShowDebugLog(key))
			{
				LogWebservice(log, context);
			}
		}
		public static void LogWarningWebservice<T>(string log, T enumKey, Object context = null) where T : System.Enum
		{
			LogWarningWebservice(log, enumKey.ToString(), context);
		}
		public static void LogWarningWebservice(string log, string key, Object context = null)
		{
			if (DebugKeys.ShowDebugLog(key))
			{
				LogWarningWebservice(log, context);
			}
		}
		#endregion
	}
}