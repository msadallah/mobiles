namespace AllMyScripts.Common.Tools
{
    using UnityEngine;
    using System.Collections.Generic;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.IO;
    using AllMyScripts.Common.Utils;

    [System.Serializable]
    public class SpriteInfo
    {
		public string m_id;
        public long m_timestamp;
        public byte[] m_textureBytes;
        public string m_lastDate;
        public string m_format;

        [System.NonSerialized]
        private Texture2D _texture;

		public Texture2D GetSprite()
        {
            return this._texture;
        }

        public SpriteInfo(string id)
        {
            m_id = id;
            m_lastDate = "";
            m_timestamp = UtilsDateFormat.GetTimestampFromUnixEpoch(System.DateTime.UtcNow);
            m_format = "jpg";
            m_textureBytes = null;
        }

        public SpriteInfo(string id, string lastDate, Texture2D texture, string format = "jpg")
        {
            m_id = id;
            m_lastDate = lastDate;
            m_timestamp = UtilsDateFormat.GetTimestampFromUnixEpoch(System.DateTime.UtcNow);
            m_format = format;
            this._texture = texture;
        }

        public void Save()
        {
            try
            {
				string dirPath = SpriteLibrary.GetDirPath();
				if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
                if (_texture != null)
				{
                    switch (m_format)
					{
                        case "png":
                            m_textureBytes = _texture.EncodeToPNG();
                            break;
                        case "tga":
                            m_textureBytes = _texture.EncodeToTGA();
                            break;
                        case "jpg":
                        default:
                            m_textureBytes = _texture.EncodeToJPG();
                            break;
                    }
				}
                BinaryFormatter bf = new BinaryFormatter();
                FileStream stream = File.Open(dirPath + m_id, FileMode.OpenOrCreate);
                bf.Serialize(stream, this);
                stream.Close();

                //DLog.Log ("SpriteLibrary.Save : success at path "+dirPath+m_id+" with date "+m_lastDate);
            }
            catch (IOException e)
            {
				Debug.LogWarning("SpriteLibrary.Save : " + e);
            }
        }

        public void Delete()
        {
            try
            {

                string dirPath = SpriteLibrary.GetDirPath();
                if (Directory.Exists(dirPath) && !string.IsNullOrEmpty(m_id) && File.Exists(dirPath + m_id))
                {

                    File.Delete(dirPath + m_id);

                }

            }
            catch (IOException e)
            {
                Debug.LogWarning("SpriteLibrary.Delete : " + e);
            }

        }

        /// <summary>
        /// Tells if the picture is recent (less than a minute)
        /// </summary>
        public bool IsRecent()
        {
            long nowMilliseconds = UtilsDateFormat.GetTimestampFromUnixEpoch(System.DateTime.UtcNow);
            //		DLog.Log ("picture time = " + m_timestamp);
            //		DLog.Log("now time = "+ nowMilliseconds);
            if (m_timestamp + 60000 > nowMilliseconds)
                return true;
            return false;
        }

        public static SpriteInfo Load(string _id)
        {
            try
            {
                string path = SpriteLibrary.GetDirPath() + _id;
                if (File.Exists(path))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream stream = File.Open(path, FileMode.Open);

                    SpriteInfo instance = null;
                    try
                    {
						object o = bf.Deserialize(stream);
						if (o is SpriteInfo)
							instance = o as SpriteInfo;
					}
                    catch (System.TypeLoadException tle)
                    {
						Debug.LogWarning("SpriteLibrary.Load - Exception during loading : type is not right");
						Debug.LogError(tle.Message);
                    }
                    catch (System.Exception e)
                    {
						Debug.LogWarning("SpriteLibrary.Load - Exception during loading");
						Debug.LogError(e.Message);
                    }

                    //DLog.Log ("SpriteLibrary.Load : success at path "+path);
                    stream.Close();

                    if (instance != null)
                    {
                        // Build a sprite from a byte array
                        if (instance.m_textureBytes != null)
                        {
                            instance._texture = new Texture2D(0, 0, TextureFormat.ARGB32, false);
                            instance._texture.LoadImage(instance.m_textureBytes);
                            instance.m_textureBytes = null;
                        }
                    }

                    return instance;
                }
                //DLog.Log ("SpriteLibrary.Load : no file "+path);
                return null;
            }
            catch (IOException e)
            {
				Debug.LogError("SpriteLibrary.Load : " + e);
                return null;
            }
        }

    }

    /// <summary>
    /// Image library (cache)
    /// </summary>
    public class SpriteLibrary
    {
        //static
        private static SpriteLibrary _instance;
        private static SpriteLibrary Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SpriteLibrary();
                return _instance;
            }
        }

		#region Properties

		public static string defaultDirPath => Application.persistentDataPath + "/images/";

		private bool m_CacheEnabled
        {
            get
            {
                return true;
            }

        }

        public static bool CacheEnable
        {
            get
            {
                return Instance.m_CacheEnabled;
            }
        }
        private Dictionary<string, SpriteInfo> _sprites;
        private List<SpriteInfo> _pendingSavedSprites;
        private List<string> _requestingSprites;
		private string _dirPath;

		#endregion

		private SpriteLibrary()
        {
            _sprites = new Dictionary<string, SpriteInfo>();
            _pendingSavedSprites = new List<SpriteInfo>();
            _requestingSprites = new List<string>();
        }

        //methods
        public static bool HasSprite(string id)
        {
            //		if(!Instance.m_cacheEnabled)
            //			return false;
            //DLog.Log ("checking picture for id " + id);
            //DLog.Log ("SpriteLibrary.HasSprite : checking sprite from "+(Sprites.ContainsKey(id)?"cache" : "file"));
            SpriteInfo info = null;
            if (!Instance._sprites.ContainsKey(id))
            {
                if (Instance.m_CacheEnabled)
                    info = SpriteInfo.Load(id);
                Instance._sprites.Add(id, info);
            }

            return (Instance._sprites[id] != null);
        }

        public static void BeginRequest(string id)
        {
            if (!Instance._requestingSprites.Contains(id))
                Instance._requestingSprites.Add(id);
        }

        public static void EndRequest(string id)
        {
            Instance._requestingSprites.Remove(id);
        }


        /// <summary>
        /// Gets the saved texture. returns null if no texture found
        /// </summary>
        /// <returns>The texture.</returns>
        /// <param name="id">Identifier.</param>
        public static Texture2D GetSprite(string id)
        {
            SpriteInfo info = null;
            if (Instance._sprites.ContainsKey(id))
            {
                info = Instance._sprites[id];
            }
            else if (Instance.m_CacheEnabled)
            {
                info = SpriteInfo.Load(id);
                Instance._sprites.Add(id, info);
            }

            if (info != null)
                return info.GetSprite();

            return null;
        }

        /// <summary>
        /// return texture if before a delay
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tex"></param>
        /// <param name="nDelayWithTexInMin"></param>
        /// <param name="nDelayWithoutTexInMin"></param>
        /// <returns></returns>
        public static bool GetSpriteWithDelayMax(string id, ref Texture2D tex, float fDelayWithTexInMin = 5f, float fDelayWithoutTexInMin = 2f)
        {
            SpriteInfo spriteInfo = GetSpriteInfo(id);
            if (spriteInfo != null)
            {
                tex = spriteInfo.GetSprite();
                float fDelayInMinute = tex != null ? fDelayWithTexInMin : fDelayWithoutTexInMin;
                if (UtilsDateFormat.GetTimespanFromUTCTimestampToNow(spriteInfo.m_timestamp).TotalMinutes < fDelayInMinute)
                {
                    return true;
                }
            }
            tex = null;
            return false;
        }

        public static SpriteInfo GetSpriteInfo(string id)
        {
            //Debug.Log( "GetSpriteInfo " + id );
            SpriteInfo info = null;
            if (Instance._sprites.ContainsKey(id))
            {
                info = Instance._sprites[id];
            }
            else if (Instance.m_CacheEnabled)
            {
                info = SpriteInfo.Load(id);
                Instance._sprites.Add(id, info);
            }

            return info;
        }

        public static bool IsBeingRequested(string id)
        {
            return Instance._requestingSprites.Contains(id);
        }

        public static void UpdateTimestamp(string id, bool saveSprite = true)
        {
            SpriteInfo spriteTarget = GetSpriteInfo(id);
            if (spriteTarget != null)
            {
                spriteTarget.m_timestamp = UtilsDateFormat.GetTimestampFromUnixEpoch(System.DateTime.UtcNow);

                // If we should add this sprite to the list of sprite waiting to be saved
                if (Instance.m_CacheEnabled && saveSprite)
                    Instance._pendingSavedSprites.Add(spriteTarget);
            }
        }

        public static void SetSprite(string id, Texture2D _texture, string lastDate, bool saveSprite = true, string format = "jpg")
        {
            SpriteInfo info;
            if (_texture != null)
            {
                info = new SpriteInfo(id, lastDate, _texture, format);
            }
            else
            {
                // Delete previous sprite in cache
                DeleteSprite(id);
                info = new SpriteInfo(id);
            }

            // If we should add this sprite to the list of sprite waiting to be saved
            if (Instance.m_CacheEnabled && saveSprite)
                Instance._pendingSavedSprites.Add(info);

            if (Instance._sprites.ContainsKey(id))
                Instance._sprites[id] = info;
            else
                Instance._sprites.Add(id, info);
        }

        public static void DeleteSprite(string id)
        {

            SpriteInfo spriteTarget = GetSpriteInfo(id);
            if (spriteTarget != null)
            {

                Instance._sprites.Remove(id);
                Instance._pendingSavedSprites.Remove(spriteTarget);
                Instance._requestingSprites.Remove(id);

                spriteTarget.Delete();
            }

        }


        public static void ApplySaving()
        {
            if (Instance._pendingSavedSprites.Count > 0)
            {
                Instance._pendingSavedSprites[0].Save();
                Instance._pendingSavedSprites.RemoveAt(0);
            }
        }

		public static void SetDirPath(string dirPath)
		{
			Instance._dirPath = dirPath;
		}

		public static string GetDirPath()
		{
			if (string.IsNullOrEmpty(Instance._dirPath))
				Instance._dirPath = defaultDirPath;
			return Instance._dirPath;
		}

		
	}


}