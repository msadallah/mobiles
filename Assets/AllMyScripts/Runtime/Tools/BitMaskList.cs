namespace AllMyScripts.Common.Tools
{
    using System;
    using System.Collections.Generic;
    using System.Collections;
    using UnityEngine;
    [Serializable]
	public sealed class BitMaskItem
	{
		[SerializeField]
		private string _sValue = "";

		[SerializeField]
		private int _nMask = default;

		public BitMaskItem( string sValue, int nMask )
		{
			_sValue = sValue;
			_nMask = nMask;
		}

		public string sValue
		{
			get { return _sValue; }
			set { _sValue = value; }
		}

		public int nMask
		{
			get { return _nMask; }
			set { _nMask = value; }
		}

		public bool ContainsMask( int nFlags )
		{
			return ( _nMask & nFlags ) == nFlags;
		}

	}

	[Serializable]
	public sealed class BitMaskList
	{
		[SerializeField] private List<BitMaskItem> _list = new List<BitMaskItem>();


		public void Add( BitMaskItem item )
		{
			_list.Add( item );
		}

		public void Clear()
		{
			_list.Clear();
		}

		public BitMaskItem this[ int index ]
		{
			get { return _list[index]; }
			set { _list[index] = value; }
		}

		public IEnumerator GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		public int Count
		{
			get { return _list.Count; }
		}

		public List<BitMaskItem> List
		{
			get { return _list; }
		}

		public ListContainer ContainerList( int nBit )
		{
			ListContainer list = new ListContainer();
			foreach( BitMaskItem item in _list )
			{
				if( item.ContainsMask( nBit ) )
				{
					list.Add( item.sValue );
				}
			}
			return list;
		}
	}
}
