namespace AllMyScripts.Common.Tools
{
    using System;
    using System.Globalization;

	using AllMyScripts.Common.Tools;
    using AllMyScripts.LangManager;

    public class UtilsDateFormat
    {
        public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// Date pattern type
        /// </summary>
        public enum DatePattern
        {
            /// <summary>
            /// Full date
            /// </summary>
            FULL,

            /// <summary>
            /// Abr. date
            /// </summary>
            ABREVIATION,

            /// <summary>
            /// Abr. date, just the day.
            /// </summary>
            ABREVIATION_DAY,

            /// <summary>
            /// Abr. date, dd/mm/yy
            /// </summary>
            SHORTDATE,

            /// <summary>
            /// Abr. date, dd/mm
            /// </summary>
            SHORTDATE_WITHOUT_YEAR,

        }

        private static readonly string[] DATE_PATTERN_IDS =
        {
        "Global_Date_Pattern", // FULL
		"Global_Date_MinPattern", // ABREVIATION
		"Global_Date_DayMinPattern", // ABREVIATION_DAY
        "Global_Date_ShortDatePattern", // SHORTDATE
		"Global_Date_ShortDatePatternWithoutYear" // SHORTDATE_WITHOUT_YEAR
	};

        /// <summary>
        /// Returns the datetime from its timestamp
        /// </summary>
        /// <param name="timestamp">datetime's timestamp (milliseconds)</param>
        /// <returns>Retrieved datetime</returns>
        public static DateTime GetDateTimeFromUTCTimeStamp(long timestamp)
        {
            return UnixEpoch.AddMilliseconds(timestamp);
        }

        /// <summary>
        /// Returns the timespan from now from its timestamp
        /// </summary>
        /// <param name="timestamp">datetime's timestamp (milliseconds)</param>
        /// <returns>Time span</returns>
        public static TimeSpan GetTimespanFromUTCTimestampToNow(long timestamp)
        {
            TimeSpan elapsedTime = DateTime.UtcNow - GetDateTimeFromUTCTimeStamp(timestamp);
            return elapsedTime;
        }

        /// <summary>
        /// Returns the timespan from now from its timestamp
        /// </summary>
        /// <param name="timestamp">datetime's timestamp (milliseconds)</param>
        /// <returns>Time span</returns>
        public static TimeSpan GetTimespanFromUnixEpoch(DateTime time)
        {
            TimeSpan elapsedTime = time - UnixEpoch;
            return elapsedTime;
        }

        /// <summary>
        /// Returns the timestamp in milliseconds from date time
        /// </summary>
        /// <param name="time">datetime</param>
        /// <returns>Timestamp in milliseconds</returns>
        public static long GetTimestampFromUnixEpoch(DateTime time)
        {
            return (long)GetTimespanFromUnixEpoch(time).TotalMilliseconds;
        }

        /// <summary>
        /// Returns the timestamp in milliseconds from date time
        /// </summary>
        /// <param name="time">datetime</param>
        /// <returns>Timestamp in milliseconds</returns>
        public static long GetRelativeTimestampFromNow(long timestamp)
        {
            return timestamp - GetTimestampFromUnixEpoch(DateTime.UtcNow);
        }

        /// <summary>
        /// Localises the time from now to a given timestamp
        /// </summary>
        /// <param name="timestamp">datetime's timestamp (milliseconds, utc)</param>
        /// <param name="pattern">Timespan pattern</param>
        /// <param name="defaultTime">default value</param>
        /// <returns>the localised string</returns>
        public static string GetLocalizedTimeSpan(long timestamp, string pattern, string defaultTime)
        {
            TimeSpan elapsedTime = GetTimespanFromUTCTimestampToNow(timestamp);

            return GetLocalizedTimeSpan(elapsedTime, pattern, defaultTime);
        }

        public static string GetLocalizedTimeSpan(TimeSpan elapsedTime, string pattern, string defaultTime)
        {
            int n = 0;
            string l = defaultTime;

            if (elapsedTime.Days >= 30)
            {
                n = (elapsedTime.Days / 30);
                l = L.Get("Global_Time_Month" + (n > 1 ? "s" : ""));
            }

            else if (elapsedTime.Days >= 1)
            {
                n = elapsedTime.Days;
                l = L.Get("Global_Time_Day" + (n > 1 ? "s" : ""));
            }

            else if (elapsedTime.Hours >= 1)
            {
                n = elapsedTime.Hours;
                l = L.Get("Global_Time_Hour" + (n > 1 ? "s" : ""));
            }
            else if (elapsedTime.Minutes >= 1)
            {
                n = elapsedTime.Minutes;
                l = L.Get("Global_Time_Minute" + (n > 1 ? "s" : ""));
            }

            else if (elapsedTime.Seconds >= 1)
            {
                n = elapsedTime.Seconds;
                l = L.Get("Global_Time_Second" + (n > 1 ? "s" : ""));
            }

            if (n == 0)
                return l;

            string time = l.Replace("$n", n.ToString());
            return pattern.Replace("$time", time);
        }

        /// <summary>
        /// Returns the given date, localized with the given pattern
        /// </summary>
        /// <param name="pattern">Pattern type</param>
        /// <param name="date">Datetime to localise</param>
        /// <param name="hourIfToday">If true, if the date is today, print the hour instead</param>
        /// <param name="yesterday">If true, if the date is yesterday, prints yesterday</param>
        /// <returns>The date as a localised string</returns>
        public static string GetLocalizedDateTime(DatePattern pattern, DateTime date, bool hourIfToday, bool yesterday, DateTime? now = null )
        {
            string result = "";

            if( now == null )
				now = DateTime.UtcNow;

            if (hourIfToday && now.Value.Date == date.Date)
                result = GetTimeToString(date);
            else if (yesterday && now.Value.Date - TimeSpan.FromDays(1) == date.Date)
                result = L.Get("Global_Date_Yesterday");
            else
                result = GetLocalizedDateTime(pattern, date);

            return result;
        }

        /// <summary>
        /// Returns the time of the date in a string
        /// </summary>
        /// <param name="time">Time to print</param>
        /// <returns>the time in a string</returns>
        public static string GetTimeToString(DateTime time)
        {
            return GetTimeToString(time.Hour, time.Minute);
        }

        public static string GetTimeToString(int hour, int minute)
        {
            return hour.ToString("D2") + ':' + minute.ToString("D2");
        }

        /// <summary>
        /// Returns the given date, localized with the given pattern
        /// </summary>
        /// <param name="pattern">Pattern type</param>
        /// <param name="date">Datetime to localise</param>
        /// <returns>The date as a localised string</returns>
        public static string GetLocalizedDateTime(DatePattern pattern, DateTime date)
        {
            string sPattern = GetDatePattern(pattern);

            int nDayOfWeek = ConvertTools.ToMondayIdx(date.DayOfWeek) + 1; // Start at 1 with Monday

            return sPattern
                .Replace("$dayName", L.Get("Global_Date_Days." + nDayOfWeek.ToString()))
                .Replace("$minDayName", L.Get("Global_Date_MinDays." + nDayOfWeek.ToString()))
                .Replace("$day", date.Day.ToString("D2"))
                .Replace("$minMonthName", L.Get("Global_Date_MinMonths." + date.Month))
                .Replace("$monthName", L.Get("Global_Date_Months." + date.Month))
                .Replace("$month", date.Month.ToString("D2"))
                .Replace("$yearMin", (date.Year % 100).ToString("D2"))
                .Replace("$year", date.Year.ToString("D4"))

                ;
        }

        public static string GetDatePattern(DatePattern pattern)
        {
            return L.Get(DATE_PATTERN_IDS[(int)pattern]);
        }

        public static string GetLocalizedTime(int hour, int minute)
        {
            var culture = CultureInfo.CurrentCulture;
            var pattern = culture.DateTimeFormat.ShortTimePattern; // or pick which one you want to use;
            var newPattern = pattern.Replace("h", "H").Replace("t", "");
            return new DateTime(1, 1, 1, hour, minute, 0).ToString(newPattern); // or use whatever DateTime you want to use
        }

        /// <summary>
        /// Return the day of week in legit normal french format : Monday = 0, Tuesday = 1 ... Sunday = 6
        /// </summary>
        public static int GetDayOfWeek(DateTime dt)
        {

            return ConvertTools.ToMondayIdx(dt.DayOfWeek);
        }

    }
}