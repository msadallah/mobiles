﻿namespace AllMyScripts.Common.Tools
{
	using UnityEngine;

	public static class DebugDrawer
	{
		public enum Plan { X = 0, Y = 1, Z = 2 }

		/// <summary>
		/// Draw a 2D Circle on the given plan
		/// </summary>
		/// <param name="plan">The plan in witch to draw the circle</param>
		/// <param name="heightOnPlan">height on the plan</param>
		/// <param name="point">a Vector2 coordinate on the plan to be used the circle's center</param>
		/// <param name="radius">the radius of the circle</param>
		/// <param name="color">Color of the drawing</param>
		/// <param name="duration">Who much time the drawing stay visible</param>
		public static void DrawCircle(Plan plan, float heightOnPlan, Vector2 point, float radius, Color color, float duration = 0f)
		{
			switch (plan)
			{
				case Plan.X:
					DrawCircle(plan, new Vector3(heightOnPlan, point.x, point.y), radius, color, duration);
					break;
				case Plan.Y:
					DrawCircle(plan, new Vector3(point.x, heightOnPlan, point.y), radius, color, duration);
					break;
				case Plan.Z:
					DrawCircle(plan, new Vector3(point.x, point.y, heightOnPlan), radius, color, duration);
					break;
			}
		}

		/// <summary>
		/// Draw a 2D Circle on the given plan at the world position point
		/// </summary>
		/// <param name="plan">The plan in witch to draw the circle</param>
		/// <param name="point">a Vector3 coordinate in space to be used the circle's center</param>
		/// <param name="radius">the radius of the circle</param>
		/// <param name="color">Color of the drawing</param>
		/// <param name="duration">Who much time the drawing stay visible</param>
		public static void DrawCircle(Plan plan, Vector3 point, float radius, Color color, float duration = 0f)
		{
			int pointCount  = 16;
			float angle = 360f/(float)pointCount;
			angle *= Mathf.Deg2Rad;
			Vector3 origin = point + GetVector3OnPlanFromSinCos(plan,0f,radius);
			for (int i = 1; i <= pointCount; ++i)
			{
				float currentAngle = angle  * i;
				Vector3 destination =  point + GetVector3OnPlanFromSinCos(plan, currentAngle, radius);
				Debug.DrawLine(origin, destination, color, 5f);
				origin = destination;
			}
		}

		/// <summary>
		/// Get a Vector3 made of cos and sin of given angle multiply by radius, used to draw circle
		/// </summary>
		/// <param name="plan">The working 2D space</param>
		/// <param name="angleRadian">The angle in radian</param>
		/// <param name="radius">The radius in meter</param>
		/// <returns>The Vector3 made of cos and sin of given angle multiply by radius</returns>
		private static Vector3 GetVector3OnPlanFromSinCos(Plan plan, float angleRadian, float radius)
		{
			switch (plan)
			{
				case Plan.X:
					return new Vector3(0f, Mathf.Sin(angleRadian) * radius, Mathf.Cos(angleRadian) * radius);
				case Plan.Y:
					return new Vector3(Mathf.Cos(angleRadian) * radius, 0f, Mathf.Sin(angleRadian) * radius);
				case Plan.Z:
					return new Vector3(Mathf.Cos(angleRadian) * radius, Mathf.Sin(angleRadian) * radius, 0f);
				default:
					return new Vector3();
			}
		}

		public static void DrawPoint(Plan plan, float heightOnPlan, Color color, Vector2 point, float size = 0.1f, float duration = 0f)
		{
			switch (plan)
			{
				case Plan.X:
					DrawPoint(new Vector3(heightOnPlan, point.x, point.y), color, size, duration);
					break;
				case Plan.Y:
					DrawPoint(new Vector3(point.x, heightOnPlan, point.y), color, size, duration);
					break;
				case Plan.Z:
					DrawPoint(new Vector3(point.x, point.y, heightOnPlan), color, size, duration);
					break;
			}
		}

		public static void DrawPoint(Vector3 point, Color color, float size = 0.1f, float duration  = 0f)
		{
			Vector3 x1 = point;
			x1.x += size;
			Vector3 x2 = point;
			x2.x -= size;

			Vector3 y1 = point;
			y1.y += size;
			Vector3 y2 = point;
			y2.y -= size;

			Vector3 z1 = point;
			z1.z += size;
			Vector3 z2 = point;
			z2.z -= size;

			Debug.DrawLine(x1, x2, color, duration);
			Debug.DrawLine(y1, y2, color, duration);
			Debug.DrawLine(z1, z2, color, duration);
		}

		public static void DrawRect(Rect rect, Plan plan, float heightOnPlan, Color color, float duration = 0f)
		{
			/*
			 *	a-----b
			 *	|	  |
			 *	|	  |
			 *	c-----d
			 * 
			 */
			switch (plan) 
			{
				case Plan.X:
					Debug.DrawLine(new Vector3(heightOnPlan, rect.xMin, rect.yMin), new Vector3(heightOnPlan, rect.xMax, rect.yMin), color, duration);
					Debug.DrawLine(new Vector3(heightOnPlan, rect.xMax, rect.yMin), new Vector3(heightOnPlan, rect.xMax, rect.yMax), color, duration);
					Debug.DrawLine(new Vector3(heightOnPlan, rect.xMax, rect.yMax), new Vector3(heightOnPlan, rect.xMin, rect.yMax), color, duration);
					Debug.DrawLine(new Vector3(heightOnPlan, rect.xMin, rect.yMax), new Vector3(heightOnPlan, rect.xMin, rect.yMin), color, duration);
					break;
				case Plan.Y:
					Debug.DrawLine(new Vector3(rect.xMin, heightOnPlan, rect.yMin), new Vector3(rect.xMax, heightOnPlan, rect.yMin), color, duration);
					Debug.DrawLine(new Vector3(rect.xMax, heightOnPlan, rect.yMin), new Vector3(rect.xMax, heightOnPlan, rect.yMax), color, duration);
					Debug.DrawLine(new Vector3(rect.xMax, heightOnPlan, rect.yMax), new Vector3(rect.xMin, heightOnPlan, rect.yMax), color, duration);
					Debug.DrawLine(new Vector3(rect.xMin, heightOnPlan, rect.yMax), new Vector3(rect.xMin, heightOnPlan, rect.yMin), color, duration);
					break;
				case Plan.Z:
					Debug.DrawLine(new Vector3(rect.xMin, rect.yMin, heightOnPlan), new Vector3(rect.xMax, rect.yMin, heightOnPlan), color, duration);
					Debug.DrawLine(new Vector3(rect.xMax, rect.yMin, heightOnPlan), new Vector3(rect.xMax, rect.yMax, heightOnPlan), color, duration);
					Debug.DrawLine(new Vector3(rect.xMax, rect.yMax, heightOnPlan), new Vector3(rect.xMin, rect.yMax, heightOnPlan), color, duration);
					Debug.DrawLine(new Vector3(rect.xMin, rect.yMax, heightOnPlan), new Vector3(rect.xMin, rect.yMin, heightOnPlan), color, duration);
					break;
			}

		}

		public static void DrawShape(Plan plan, float heightOnPlan, Color color, params Vector2[] points)
		{
			DrawShape(plan, heightOnPlan, color, 20f, points);
		}

		public static void DrawShape(Plan plan, float heightOnPlan, Color color, float duration, params Vector2[] points)
		{
			Vector3[] vector3Points = new Vector3[points.Length];

			switch (plan)
			{
				case Plan.X:
					for (int i = 0; i < points.Length; ++i)
					{
						vector3Points[i] = new Vector3(heightOnPlan, points[i].x, points[i].y);
					}
					break;
				case Plan.Y:
					for (int i = 0; i < points.Length; ++i)
					{
						vector3Points[i] = new Vector3(points[i].x, heightOnPlan, points[i].y);
					}
					break;
				case Plan.Z:
					for (int i = 0; i < points.Length; ++i)
					{
						vector3Points[i] = new Vector3(points[i].x, points[i].y, heightOnPlan);
					}
					break;
			}

			DrawShape(color, duration, vector3Points);
		}

		public static void DrawShape(Color color, params Vector3[] points)
		{
			DrawShape(color, 20f, points);
		}

		public static void DrawShape(Color color, float duration, params Vector3[] points)
		{
			for (int i = 0; i < points.Length; ++i)
			{
				Vector3 firstPoint = points[i];
				Vector3 secondPoint = points[(i+1) % points.Length];
				Debug.DrawLine(firstPoint,secondPoint, color, duration);
			}
		}

		public static void DrawVector3(Color color, float duration, Vector3 initialPos, Vector3 vectorToDraw)
		{
			Vector3[][] linesToDraw = new Vector3[5][];
			linesToDraw[0] = new Vector3[2];
			linesToDraw[0][0] = initialPos;

			Vector3 vectorInvert = vectorToDraw * -0.1f;

			Vector3 VectorEndPos = linesToDraw[0][1] = initialPos + vectorToDraw;
			for (int i = 1; i < 5; ++i)
			{
				linesToDraw[i] = new Vector3[2];
				linesToDraw[i][0] = VectorEndPos;
				Vector3 rotationPlan = Quaternion.AngleAxis(i * 90f,vectorInvert) *  Vector3.up;
				linesToDraw[i][1] = VectorEndPos  + (Quaternion.AngleAxis(33f, rotationPlan) * vectorInvert);
			}
			for (int i = 0; i < 5; ++i)
			{

				Debug.DrawLine(linesToDraw[i][0], linesToDraw[i][1], color, duration);
			}
		}

		public static void DrawVector2(Plan plan, float heightOnPlan, Color color, float duration, Vector2 initialPos, Vector2 vectorToDraw)
		{
			switch (plan)
			{
				case Plan.X:
					DrawVector3(color, duration, new Vector3(heightOnPlan, initialPos.x, initialPos.y), new Vector3(heightOnPlan, vectorToDraw.x, vectorToDraw.y));
					break;
				case Plan.Y:
					DrawVector3(color, duration, new Vector3(initialPos.x, heightOnPlan, initialPos.y), new Vector3(vectorToDraw.x, heightOnPlan, vectorToDraw.y));
					break;
				case Plan.Z:
					DrawVector3(color, duration, new Vector3(initialPos.x, initialPos.y, heightOnPlan), new Vector3(vectorToDraw.x, vectorToDraw.y, heightOnPlan));
					break;
			}
		}

		public static void DrawBounds(Bounds bounds, Color color, float duration = 0f)
		{
			/*    Bounds points
			*       e--------f 
			*      /|       /| 
			*     a--------b | 
			*     | |      | | 
			*     | |      | | 
			*     | h------|-g
			*     |/       |/
			*     d--------c
			*/

			Vector3 a = bounds.center + new Vector3(-bounds.extents.x, bounds.extents.y, bounds.extents.z);
			Vector3 b = bounds.center + new Vector3(bounds.extents.x, bounds.extents.y, bounds.extents.z);
			Vector3 c = bounds.center + new Vector3(bounds.extents.x, -bounds.extents.y, bounds.extents.z);
			Vector3 d = bounds.center + new Vector3(-bounds.extents.x, -bounds.extents.y, bounds.extents.z);
			Vector3 e = bounds.center + new Vector3(-bounds.extents.x, bounds.extents.y, -bounds.extents.z);
			Vector3 f = bounds.center + new Vector3(bounds.extents.x, bounds.extents.y, -bounds.extents.z);
			Vector3 g = bounds.center + new Vector3(bounds.extents.x, -bounds.extents.y, -bounds.extents.z);
			Vector3 h = bounds.center + new Vector3(-bounds.extents.x, -bounds.extents.y, -bounds.extents.z);

			Debug.DrawLine(a, b, color, duration);
			Debug.DrawLine(b, c, color, duration);
			Debug.DrawLine(c, d, color, duration);
			Debug.DrawLine(d, a, color, duration);
			Debug.DrawLine(e, f, color, duration);
			Debug.DrawLine(f, g, color, duration);
			Debug.DrawLine(g, h, color, duration);
			Debug.DrawLine(h, e, color, duration);
			Debug.DrawLine(a, e, color, duration);
			Debug.DrawLine(b, f, color, duration);
			Debug.DrawLine(c, g, color, duration);
			Debug.DrawLine(d, h, color, duration);
		}
	}
}