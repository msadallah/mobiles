﻿namespace AllMyScripts.Common.Utils
{
    using UnityEngine;

    public static class UtilsGraphic
    {
        public static float[] ComputeRoundedScaleValues(float fMin, float fMax, int nValueCount = 4, int nDigitMax = 2)
        {
            float[] values = new float[nValueCount];

            if (nValueCount >= 2)
            {
                float fRange = fMax - fMin;

                if (Mathf.Approximately(fRange, 0f))
                {
                    if (Mathf.Approximately(fMin, 0f))
                    {
                        fMax = 1f;
                    }
                    else
                    {
                        if (fMin > 0f)
                            fMin = 0f;
                        else
                            fMax = 0f;
                    }
                    fRange = fMax - fMin;
                }

                float fLog10 = Mathf.Log(fRange, 10);
                float fBaseCount = Mathf.FloorToInt(fLog10);
                float fBase = Mathf.Pow(10, fBaseCount - nDigitMax + 1);

                float fRoundedMin = Mathf.FloorToInt(fMin / fBase) * fBase;
                float fRoundedMax = Mathf.CeilToInt(fMax / fBase) * fBase;

                for (int i = 0; i < nValueCount; ++i)
                {
                    if (i == 0)
                        values[i] = fRoundedMin;
                    else if (i == nValueCount - 1)
                        values[i] = fRoundedMax;
                    else
                        values[i] = fRoundedMin + Mathf.RoundToInt((i * (fRoundedMax - fRoundedMin) / (nValueCount - 1)) / fBase) * fBase;
                }
            }

            return values;
        }
    }
}
