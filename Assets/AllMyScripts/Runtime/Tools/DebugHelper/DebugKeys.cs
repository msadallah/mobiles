﻿namespace AllMyScripts.Common.Tools.DebugHelper
{ 
	using System.Collections.Generic;
	using UnityEngine;
	public static class DebugKeys
	{
		public static Dictionary <string, bool> visualDebugsShowState;
		public static Dictionary <string, bool> logDebugsShowState;

		public static bool ShowVisualDebug(string key)
		{
			if (visualDebugsShowState.ContainsKey(key))
			{
				return (visualDebugsShowState[key]);
			}
			return false;
		}

		public static bool ShowVisualDebug<T>(T enumKey) where T : System.Enum
		{
			return ShowVisualDebug(enumKey.ToString());
		}

		public static bool ShowDebugLog(string key)
		{
			if (logDebugsShowState.ContainsKey(key))
			{
				return (logDebugsShowState[key]);
			}
			return false;
		}

		public static bool ShowDebugLog<T>(T enumKey)where T : System.Enum
		{
			return ShowDebugLog(enumKey.ToString());
		}

		#region DebugLog
		public static void DebugLog<T>(object message, T enumKey) where T : System.Enum
		{
			DebugLog(message, enumKey.ToString());
		}

		public static void DebugLog(object message, string key)
		{
			if (ShowDebugLog(key))
			{
				Debug.Log(message);
			}
		}

		public static void DebugLogWarning<T>(object message, T enumKey) where T : System.Enum
		{
			DebugLogWarning(message, enumKey.ToString());
		}

		public static void DebugLogWarning(object message, string key)
		{
			if (ShowDebugLog(key))
			{
				Debug.LogWarning(message);
			}
		}

		public static void DebugLogError<T>(object message, T enumKey) where T : System.Enum
		{
			DebugLogError(message, enumKey.ToString());
		}

		public static void DebugLogError(object message, string key)
		{
			if (ShowDebugLog(key))
			{
				Debug.LogError(message);
			}
		}

		#endregion
	}
}