namespace AllMyScripts.Common.Tools
{
    using System;
    using System.Collections.Generic;
    using System.Collections;
    using UnityEngine;

    [Serializable]
	public sealed class ListContainer
	{
		[SerializeField]
		private List<string> _list = new List<string>();
	
	
		public void Add( string item )
		{
			_list.Add( item );
		}
	
		public void Clear()
		{
			_list.Clear();
		}
	
		public string this[int index]
		{
			get
			{
				return _list[index];
			}
			set
			{
				_list[index] = value;
			}
		}
	
		public IEnumerator GetEnumerator()
		{
			return _list.GetEnumerator();
		}
	
		public int Count
		{
			get
			{
				return _list.Count;
			}
		}
	
		public List<string> List
		{
			get
			{
				return _list;
			}
		}

		public bool Contains( string s )
		{
			foreach( string item in _list )
			{
				if( item == s )
					return true;
			}
			return false;
		}
	}


	[Serializable]
	public sealed class MasterList
	{
		[SerializeField]
		private List<ListContainer> _list = new List<ListContainer>();
	
	
		public void Add( ListContainer item )
		{
			_list.Add( item );
		}
	
		public void Clear()
		{
			_list.Clear();
		}
	
		public ListContainer this[int index]
		{
			get
			{
				return _list[index];
			}
			set
			{
				_list[index] = value;
			}
		}
	
		public IEnumerator GetEnumerator()
		{
			return _list.GetEnumerator();
		}
	
		public int Count
		{
			get
			{
				return _list.Count;
			}
		}
	
		public List<ListContainer> List
		{
			get
			{
				return _list;
			}
		}
	}
}
