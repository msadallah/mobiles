﻿namespace UnityEngine
{
	public static class GameObjectExtensions
	{

		/// <summary>
		/// Gets the component. If it doesn't exist, add it
		/// </summary>
		public static T GetAddComponent<T>(this GameObject gameObject) where T : Component
		{
			T component = gameObject.GetComponent<T>();
			if (component == null)
			{
				component = gameObject.AddComponent<T>();
			}
			return component;
		}

		public static void SetLayerRecursively(this GameObject trans, int layer)
		{
			trans.layer = layer;
			foreach (Transform child in trans.transform)
				SetLayerRecursively(child.gameObject, layer);
		}
	}
}