﻿namespace AllMyScripts.Common.UI
{
	using System;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.EventSystems;

	public static class UtilsUI
	{

		/// <summary>
		/// used to check if UI is being used by the user
		/// </summary>
		/// <param name="ShouldIgnore">a function that'll tell if certain UI objects should be ignored from the test or not</param>
		/// <returns>True if UI is being used</returns>
		public static bool IsPointerOverUIObject(Func<RaycastResult, bool> ShouldIgnore = null)
		{
			if (EventSystem.current == null)
				return false;
			List<RaycastResult> results = GetUIObjectsUnderPointer();
			if (ShouldIgnore != null)
			{
				for (int i = results.Count - 1; i >= 0; --i)
				{
					if (ShouldIgnore(results[i]))
					{
						results.RemoveAt(i);
					}
				}
			}
			return results.Count > 0;
		}

		/// <summary>
		/// used to check if UI is being used by the user
		/// </summary>
		/// <param name="ShouldIgnore">a function that'll tell if certain UI objects should be ignored from the test or not</param>
		/// <returns>True if UI is being used</returns>
		public static List<RaycastResult> GetUIObjectsUnderPointer()
		{
			if (EventSystem.current == null)
				return new List<RaycastResult>();
			PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
			eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			List<RaycastResult> results = new List<RaycastResult>();
			EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
			return results;
		}

		/// <summary>
		/// used to check if UI is being used by the user
		/// </summary>
		/// <returns>True if UI is being used</returns>
		public static bool IsPointerOverCamera(Camera camera)
		{
			return camera.pixelRect.Contains(Input.mousePosition);
			
		}

		/// <summary>
		/// used to check if UI is manipuling from user
		/// </summary>
		/// <param name="ShouldIgnore">a function that'll tell if certain UI objects should be ignored from the test or not</param>
		/// <returns></returns>
		public static bool HasUIObjectSelected(Func<GameObject, bool> ShouldIgnore = null)
		{
			if (EventSystem.current == null)
				return false;
			GameObject currentGo = EventSystem.current.currentSelectedGameObject;
			if (currentGo == null)
				return false;
			if (ShouldIgnore != null && ShouldIgnore(currentGo))
				return false;
			return true;
		}

		/// <summary>
		/// Do action for the first parent
		/// </summary>
		public static void DoForParents<T>(Transform me, Action<T> action) where T : IEventSystemHandler
		{
			Transform parent = me.parent;
			while (parent != null)
			{
				bool bFound = false;
				foreach (var component in parent.GetComponents<Component>())
				{
					if (component is T)
					{
						bFound = true;
						action((T)(IEventSystemHandler)component);
					}
				}

				if (bFound) return;

				parent = parent.parent;
			}
		}
	}
}