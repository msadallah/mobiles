namespace AllMyScripts.Common.Tools
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using System;

    using UnityEngine;

#if NET_4_6 || NET_STANDARD_2_0
    using System.Globalization;
#endif

	using AllMyScripts.Common.Tools.Xml;

	/// <summary>
	/// This class encodes and decodes JSON strings.
	/// Spec. details, see http://www.json.org/
	///
	/// JSON uses Arrays and Objects.
	/// These correspond here to the datatypes IList and IDictionary.
	/// All numbers are parsed to longs or doubles.
	/// </summary>
	public static class JSON
	{
		public static object result => _result;
		private static object _result = null;

		/// <summary>
		/// Parses the string sJSON into an object
		/// </summary>
		/// <param name="sJSON">A JSON string.</param>
		/// <returns>An List&lt;object&gt;, a Dictionary&lt;string, object&gt;, a double, an integer, a string, null, true, or false</returns>
		public static object Deserialize( string sJSON )
		{
			if( string.IsNullOrEmpty( sJSON ) )
				return null;
			else
				return Parser.Parse( sJSON );
		}

		public static IEnumerator DeserializeASync(string sJSON, int cycle = 1000)
		{
			_result = null;

			if (string.IsNullOrEmpty(sJSON))
				yield break;

			yield return Parser.ParseASync(sJSON, cycle);

			_result = Parser.result;
		}

		sealed class Parser
		{
			public static object result => _result;
			public static object _result = null;

			const string WORD_BREAK = "{}[],:\"";

			public object lastObject => _objects.Peek();

			private Stack<Dictionary<string,object>> _tables = null;
			private Stack<object> _objects = null;
			private Stack<List<object>> _arrays = null;

			private static bool IsWordBreak( char c )
			{
				return char.IsWhiteSpace( c ) || ( WORD_BREAK.IndexOf( c )!=-1 );
			}

			enum Token
			{
				NONE,
				CURLY_OPEN,
				CURLY_CLOSE,
				SQUARED_OPEN,
				SQUARED_CLOSE,
				COLON,
				COMMA,
				STRING,
				NUMBER,
				TRUE,
				FALSE,
				NULL
			};

			StringReader reader;
			StringBuilder builder;

			Parser( string sJSON )
			{
				reader = new StringReader( sJSON );
				builder = new StringBuilder();
			}

			public void Dispose()
			{
				builder.Length = 0;
				builder = null;
				reader.Dispose();
				reader = null;
			}

			public static object Parse( string sJSON )
			{
				Parser instance = new Parser( sJSON );
				object oResult = instance.ParseValue();
				instance.Dispose();
				return oResult;
			}

			public static IEnumerator ParseASync(string sJSON, int cycle, int counter = 0)
			{
				_result = null;
				Parser instance = new Parser( sJSON );
				IEnumerator iter = instance.ParseValueASync(counter, cycle);
				while (iter.MoveNext())
				{
					if (iter.Current is int)
					{
						counter = (int)iter.Current;
						if (counter % cycle == 0)
						{
							yield return null;
						}
					}
				}
				instance.Dispose();
				_result = instance.lastObject;
			}

			private Dictionary<string,object> ParseObject()
			{
				Dictionary<string,object> table = new Dictionary<string, object>();

				// Skip opening brace "{"
				reader.Read();

				while( true )
				{
					switch( ReadToken() )
					{
						case Token.NONE:
							return null;
						case Token.COMMA:
							continue;
						case Token.CURLY_CLOSE:
							return table;
						default:
							// name
							string sName = ParseString();
							if( sName==null ) return null;

							// Skip the colon ":"
							if( ReadToken()!=Token.COLON ) return null;
							reader.Read();

							// value
							table[sName] = ParseValue();
							break;
					}
				}
			}

			private IEnumerator ParseObjectASync(int counter, int cycle)
			{
				Dictionary<string, object> table = new Dictionary<string, object>();

				// Skip opening brace "{"
				reader.Read();

				bool read = true;
				while (read)
				{
					switch (ReadToken())
					{
						case Token.NONE:
							table = null;
							read = false;
							break;
						case Token.COMMA:
							continue;
						case Token.CURLY_CLOSE:
							read = false;
							break;
						default:
							// name
							string sName = ParseString();
							if (sName == null)
							{
								table = null;
								read = false;
								break;
							}

							// Skip the colon ":"
							if (ReadToken() != Token.COLON)
							{
								table = null;
								read = false;
								break;
							}
							reader.Read();

							// value
							IEnumerator iter = ParseValueASync(counter, cycle);
							while (iter.MoveNext())
							{
								counter = (int)iter.Current;
								yield return counter;
							}
							table[sName] = _objects.Pop();
							break;
					}
				}
				if (_tables == null)
					_tables = new Stack<Dictionary<string, object>>();
				_tables.Push(table);
			}

			private List<object> ParseArray()
			{
				List<object> array = new List<object>();

				// Skip opening bracket "["
				reader.Read();

				bool bParsing = true;
				while( bParsing )
				{
					Token nextToken = ReadToken();
					switch( nextToken )
					{
						case Token.NONE:
							return null;
						case Token.COMMA:
							continue;
						case Token.SQUARED_CLOSE:
							bParsing = false;
							break;
						default:
							object value = ParseByToken( nextToken );
							array.Add( value );
							break;
					}
				}

				return array;
			}

			private IEnumerator ParseArrayASync(int counter, int cycle)
			{
				List<object> array = new List<object>();

				// Skip opening bracket "["
				reader.Read();

				bool bParsing = true;
				while (bParsing)
				{
					Token nextToken = ReadToken();
					switch (nextToken)
					{
						case Token.NONE:
							array = null;
							bParsing = false;
							break;
						case Token.COMMA:
							continue;
						case Token.SQUARED_CLOSE:
							bParsing = false;
							break;
						default:
							IEnumerator iter = ParseByTokenASync(nextToken, counter, cycle);
							while (iter.MoveNext())
							{
								counter = (int)iter.Current;
								yield return counter;
							}
							array.Add(_objects.Pop());
							break;
					}
				}
				if (_arrays == null)
					_arrays = new Stack<List<object>>();
				_arrays.Push(array);
			}

			private object ParseValue()
			{
				return ParseByToken( ReadToken() );
			}

			private IEnumerator ParseValueASync(int counter, int cycle)
			{
				counter++;
				yield return counter;
				IEnumerator iter = ParseByTokenASync(ReadToken(), counter, cycle);
				while (iter.MoveNext())
				{
					int oldCounter = counter;
					counter = (int)iter.Current;
					if (counter > oldCounter)
						yield return counter;
				}
			}

			private object ParseByToken( Token token )
			{
				switch( token )
				{
					case Token.STRING:			return ParseString();
					case Token.NUMBER:			return ParseNumber();
					case Token.CURLY_OPEN:		return ParseObject();
					case Token.SQUARED_OPEN:	return ParseArray();
					case Token.TRUE:			return true;
					case Token.FALSE:			return false;
					case Token.NULL:			return null;
					default:					return null;
				}
			}

			private IEnumerator ParseByTokenASync(Token token, int counter, int cycle)
			{
				object obj;
				IEnumerator iter;
				switch (token)
				{
					case Token.STRING:
						obj = ParseString();
						break;
					case Token.NUMBER:
						obj = ParseNumber();
						break;
					case Token.CURLY_OPEN:
						iter = ParseObjectASync(counter, cycle);
						while (iter.MoveNext())
						{
							counter = (int)iter.Current;
							yield return counter;
						}
						obj = _tables.Pop();
						break;
					case Token.SQUARED_OPEN:
						iter = ParseArrayASync(counter, cycle);
						while (iter.MoveNext())
						{
							counter = (int)iter.Current;
							yield return counter;
						}
						obj = _arrays.Pop();
						break;
					case Token.TRUE:
						obj = true;
						break;
					case Token.FALSE:
						obj = false;
						break;
					case Token.NULL:
					default:
						obj = null;
						break;
				}
				if (_objects == null)
					_objects = new Stack<object>();
				_objects.Push(obj);
			}

			private string ParseString()
			{
				builder.Length = 0;

				// Skip opening quote
				reader.Read();

				bool bParsing = true;
				while( bParsing )
				{
					if( IsEOF() )
					{
						bParsing = false;
						break;
					}
					char c = ReadChar();
					switch( c )
					{
						case '"':
							bParsing = false;
							break;
						case '\\':
							if( IsEOF() )
							{
								bParsing = false;
								break;
							}
							c = ReadChar();
							switch( c )
							{
								case '"':
								case '\\':
								case '/':
									builder.Append( c );
									break;
								case 'b':
									builder.Append( '\b' );
									break;
								case 'f':
									builder.Append( '\f' );
									break;
								case 'n':
									builder.Append( '\n' );
									break;
								case 'r':
									builder.Append( '\r' );
									break;
								case 't':
									builder.Append( '\t' );
									break;
								case 'u':
									char[] cHexs = new char[4];
									for( int i=0; i<4; i++ ) cHexs[i] = ReadChar();
									builder.Append( (char)Convert.ToInt32( new string(cHexs), 16 ) );
									break;
							}
							break;
						default:
							builder.Append( c );
							break;
					}
				}

				return builder.ToString();
			}

			private object ParseNumber()
			{
				string sNumber = ReadWord();

				if( sNumber.IndexOf( '.' ) == -1 )
					return ParseTools.ParseLongSafe(sNumber, 0);

				return ParseTools.ParseDoubleSafe(sNumber, 0);
			}
		
			private bool IsEOF()
			{
				return( reader.Peek()==-1 );
			}

			private void EatWhitespaces()
			{
				while( char.IsWhiteSpace( PeekChar() ) )
				{
					reader.Read();
					if( IsEOF() ) break;
				}
			}

			private char PeekChar()
			{
				int c = reader.Peek();
				if (c < 0)
					return default;
				else
					return (char)c;
			}

			private char ReadChar()
			{
				int c = reader.Read();
				if (c < 0)
					return default;
				else
					return (char)c;
			}

			private string ReadWord()
			{
				builder.Length = 0;
				while( !IsWordBreak( PeekChar() ) )
				{
					builder.Append( ReadChar() );
					if( IsEOF() ) break;
				}
				return builder.ToString();
			}

			private Token ReadToken()
			{
				EatWhitespaces();

				if( IsEOF() ) return Token.NONE;

				switch( PeekChar() )
				{
					case '{':
						return Token.CURLY_OPEN;
					case '}':
						reader.Read();
						return Token.CURLY_CLOSE;
					case '[':
						return Token.SQUARED_OPEN;
					case ']':
						reader.Read();
						return Token.SQUARED_CLOSE;
					case ',':
						reader.Read();
						return Token.COMMA;
					case '"':
						return Token.STRING;
					case ':':
						return Token.COLON;
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
					case '-':
						return Token.NUMBER;
				}

				switch( ReadWord() )
				{
					case "false":	return Token.FALSE;
					case "true":	return Token.TRUE;
					case "null":	return Token.NULL;
				}

				return Token.NONE;
			}
		}

		/// <summary>
		/// Converts a IDictionary / IList object or a simple type (string, int, etc.) into a JSON string
		/// </summary>
		/// <param name="obj">A Dictionary&lt;string, object&gt; / List&lt;object&gt;</param>
		/// <param name="bPretty">To generate a nice display with "\n" after special JSON like "{"</param>
		/// <returns>A JSON encoded string, or null if object 'obj' is not serializable</returns>
		public static string Serialize( object obj, bool bPretty=false )
		{
			return Serializer.Serialize( obj, bPretty );
		}

		sealed class Serializer
		{
			private StringBuilder _builder;
			private bool _bPretty;
			private int _nTabs;

			Serializer()
			{
				_builder = new StringBuilder();
			}

			public void Dispose()
			{
				_builder.Length = 0;
				_builder = null;
			}

			public static string Serialize( object obj, bool bPretty=false )
			{
				Serializer instance = new Serializer();
				instance._bPretty = bPretty;
				instance._nTabs = 0;
				instance.SerializeValue( obj );
				string sResult = instance._builder.ToString();
				instance.Dispose();
				return sResult;
			}

			private void SerializeValue( object value )
			{
				if ( value==null )
					_builder.Append( "null" );
				else if( value is string )
					SerializeString( (string)value );
				else if( value is bool )
					_builder.Append( (bool)value ? "true" : "false" );
				else if( (value as IList)!=null )
					SerializeArray( value as IList );
				else if( (value as IDictionary)!=null )
					SerializeObject( value as IDictionary );
				// Unity Flash build considers 'char' as 'int'...
				else if( value is char )
					SerializeString( new string( (char)value, 1 ) );
				else
					SerializeOther( value );
			}

			private void SerializeObject( IDictionary obj )
			{
				if( _bPretty )
				{
					_builder.Append( "\n" );
					AddTabs();
				}
				_builder.Append( '{' );
				if( _bPretty )
				{
					_builder.Append( "\n" );
					_nTabs++;
				}

				bool bFirst = true;
				foreach( object e in obj.Keys )
				{
					if( !bFirst )
					{
						_builder.Append( ',' );
						if( _bPretty ) _builder.Append( "\n" );
					}
					if( _bPretty ) AddTabs();

					SerializeString( e.ToString() );
					_builder.Append( ':' );

					SerializeValue( obj[e] );

					bFirst = false;
				}

				if( _bPretty )
				{
					_builder.Append( "\n" );
					_nTabs--;
					AddTabs();
				}
				_builder.Append( '}' );
			}

			private void SerializeArray( IList array )
			{
				_builder.Append( '[' );

				bool bFirst = true;
				foreach( object obj in array )
				{
					if( !bFirst )
						_builder.Append( ',' );
					SerializeValue( obj );
					bFirst = false;
				}

				_builder.Append( ']' );
			}

			private void SerializeVector2(Vector2 vec2)
			{
				_builder.Append('[');
				if (_bPretty)
				{
					_builder.Append("\n");
					AddTabs();
					_nTabs++;
				}
				_builder.Append('{');
				if (_bPretty)
				{
					_builder.Append("\n");
					AddTabs();
				}
				SerializeString("x");
				_builder.Append(':');
				SerializeValue(vec2.x);
				_builder.Append(',');

				if (_bPretty)
				{
					_builder.Append("\n");
					AddTabs();
				}

				SerializeString("y");
				_builder.Append(':');
				SerializeValue(vec2.y);

				if (_bPretty)
				{
					_builder.Append("\n");
					AddTabs();
				}

				if (_bPretty)
				{
					_builder.Append("\n");
					_nTabs--;
					AddTabs();
				}
				_builder.Append('}');
				_builder.Append(']');
			}

			private void SerializeVector3( Vector3 vec3 )
			{
				if (_bPretty)
				{
					_builder.Append("\n");
					AddTabs();
					_nTabs++;
				}
				_builder.Append('{');
				if (_bPretty)
				{
					_builder.Append("\n");
					AddTabs();
				}

				SerializeString("x");
				_builder.Append(':');
				SerializeOther(vec3.x);
				_builder.Append(',');

				if (_bPretty)
				{
					_builder.Append("\n");
					AddTabs();
				}

				SerializeString("y");
				_builder.Append(':');
				SerializeOther(vec3.y);
				_builder.Append(',');

				if (_bPretty)
				{
					_builder.Append("\n");
					AddTabs();
				}

				SerializeString("z");
				_builder.Append(':');
				SerializeOther(vec3.z);

				if (_bPretty)
				{
					_builder.Append("\n");
					_nTabs--;
					AddTabs();
				}
				_builder.Append('}');
			}

			private void SerializeString( string sValue )
			{
				_builder.Append( '\"' );

				char[] charArray = sValue.ToCharArray();
				foreach( char c in charArray )
				{
					switch( c )
					{
						case '"':
							_builder.Append( "\\\"" );
							break;
						case '\\':
							_builder.Append( "\\\\" );
							break;
						case '\b':
							_builder.Append( "\\b" );
							break;
						case '\f':
							_builder.Append( "\\f" );
							break;
						case '\n':
							_builder.Append( "\\n" );
							break;
						case '\r':
							_builder.Append( "\\r" );
							break;
						case '\t':
							_builder.Append( "\\t" );
							break;
						default:
							int nCodepoint = Convert.ToInt32( c );
							if( nCodepoint>=32 && nCodepoint<=126 )
							{
								_builder.Append( c );
							}
							else
							{
								_builder.Append( "\\u" );
								_builder.Append( nCodepoint.ToString( "x4" ) );
							}
							break;
					}
				}

				_builder.Append( '\"' );
			}

			private void SerializeOther( object value )
			{
				if( value is float )
				{
#if NET_4_6 || NET_STANDARD_2_0
					_builder.Append(Convert.ToSingle(value).ToString("R", NumberFormatInfo.InvariantInfo));
#else
					_builder.Append( Convert.ToSingle( value ).ToString( "R" ));
#endif
				}
				else if( value is int
					|| value is uint
					|| value is long
					|| value is sbyte
					|| value is byte
					|| value is short
					|| value is ushort
					|| value is ulong )
				{
					_builder.Append( value );
				}
				else if( value is double || value is decimal )
				{
#if NET_4_6 || NET_STANDARD_2_0
					_builder.Append( Convert.ToDouble( value ).ToString( "R", NumberFormatInfo.InvariantInfo));
#else
					_builder.Append( Convert.ToDouble( value ).ToString( "R" ));
#endif
				}
				else if (value is Vector2 vec2ToSerialized)
				{
					SerializeVector2(vec2ToSerialized);
				}
				else if( value is Vector3 vec3ToSerialized)
				{
					SerializeVector3(vec3ToSerialized);
				}
				else
				{
					SerializeString( value.ToString() );
				}
			}
		
			private void AddTabs()
			{
				for( int i=0; i<_nTabs; i++ )
				{
					_builder.Append( "\t" );
				}
			}
		}
	}
}
