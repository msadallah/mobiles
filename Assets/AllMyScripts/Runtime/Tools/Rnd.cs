namespace AllMyScripts.Common.Tools
{
	public sealed class Rnd
	{
		private static int DoRnd( int nMax )
		{
			if( nMax <= 0 ) return 0;
			int nSize = nMax;
			if( nSize < 0x7FFFFFFF ) nSize++;
			return (int)System.Math.Floor( RndHandler.Rnd() * nSize );
		}

		public static int Get()
		{
			return DoRnd( 0x7FFFFFFF );
		}

		public static int Max( int nMax )
		{
			return DoRnd( nMax );
		}

		public static int MinMax( int nMin, int nMax )
		{
			return nMin + DoRnd( nMax - nMin );
		}
	}
}
