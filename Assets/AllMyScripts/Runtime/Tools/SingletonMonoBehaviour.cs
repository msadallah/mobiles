namespace AllMyScripts.Common.Tools
{
    using UnityEngine;

    public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
	{
		public static T instance
		{
			get
			{
				if( SearchInstance() == null )
				{
					CreateInstance();
				}
				return s_instance;
			}
		}

		private static T s_instance;

		private static void SetInstance( T newInstance )
		{
			if( newInstance != null )
			{
				s_instance = newInstance;
				if( s_instance.transform.parent==null )
					DontDestroyOnLoad( s_instance.gameObject );
			}
		}

		public static T SearchInstance()
		{
			if( s_instance == null )
			{
				SetInstance( FindObjectOfType( typeof( T ) ) as T );
			}
			return s_instance;
		}

		public static T CreateInstance()
		{
			if( s_instance == null )
			{
				GameObject go = new GameObject( typeof( T ).ToString() );
				SetInstance( go.AddComponent<T>() );
			}
			return s_instance;
		}

		public static void DestroyInstance()
		{
			if( IsInstanceValid() )
			{
				Destroy( s_instance.gameObject );
				FreeInstance();
			}
		}

		public static void FreeInstance()
		{
			s_instance = null;
		}

		public static bool IsInstanceValid()
		{
			return ( s_instance != null );
		}

		public static bool IsInstanceEqualTo( T otherInstance )
		{
			return ( s_instance == otherInstance );
		}
	}
}
