﻿namespace AllMyScripts.Common.Tools
{
	using UnityEngine;
	public static class UtilsRect
	{
		/// <summary>
		/// Return a Rectangle englobing all points
		/// </summary>
		/// <param name="points">The Array  of points</param>
		/// <returns>Rect the rectangle englobing all points</returns>
		public static Rect EnglobingRect(Vector2[] points)
		{
			float xmin = float.PositiveInfinity, ymin = float.PositiveInfinity;
			float xmax = float.NegativeInfinity, ymax = float.NegativeInfinity;
			for (int i = 0; i <  points.Length; ++i)
			{
				if (points[i].x < xmin)
				{
					xmin = points[i].x;
				}
				if (points[i].x > xmax)
				{
					xmax = points[i].x;
				}
				if (points[i].y < ymin)
				{
					ymin = points[i].y;
				}
				if (points[i].y > ymax)
				{
					ymax = points[i].y;
				}
			}
			//Debug.Log("Vals : xmin : " + xmin + ", ymin : " + ymin + ", xmax : " + xmax + ", ymax : " + ymax);
			return new Rect(xmin, ymin, xmax - xmin, ymax - ymin);
		}
		
		/// <summary>
		/// Return the bigger Rect with given ratio fitting inside the given rectangle
		/// </summary>
		/// <param name="rectangle">The Rect to fit  into</param>
		/// <param name="ratio">The target ratio in Vector2</param>
		/// <returns>Rect with ratio fitting inside given rectangle</returns>
		public static Rect FitInRectWithRatio(Rect rectangle, Vector2 ratio)
		{
			return (FitInRectWithRatio(rectangle, Mathf.Abs(ratio.x / ratio.y)));
		}
		/// <summary>
		/// Return the bigger Rect with given ratio fitting inside the given rectangle
		/// </summary>
		/// <param name="rectangle">The Rect to fit  into</param>
		/// <param name="ratio">The target ratio</param>
		/// <returns>Rect with ratio fitting inside given rectangle</returns>
		public static Rect FitInRectWithRatio(Rect rectangle, float ratio)
		{
			float ratioRect = rectangle.size.x/rectangle.size.y;
			float absRationRect = Mathf.Abs(ratioRect);
			if (absRationRect == ratio)
			{
				return rectangle;
			}
			else if (absRationRect > ratio)
			{
				float newwidth = (rectangle.width * ratio) / absRationRect;
				float diff = rectangle.width - newwidth;
				return new Rect(rectangle.x + (diff * 0.5f), rectangle.y, newwidth, rectangle.height);
			}
			else
			{
				float newHeight  = (rectangle.height * absRationRect) / ratio;
				float diff = rectangle.height - newHeight;
				return new Rect(rectangle.x, rectangle.y + (diff * 0.5f), rectangle.width, newHeight);
			}
		}
		/// <summary>
		/// Return true if the other rectangle is contained inside this rectangle. If allowInverse is present and true, the width and height of the Rect are allowed to take negative values (ie, the min value is greater than the max), and the test will still work.
		/// </summary>
		/// <param name="rect">this</param>
		/// <param name="rectangle">The rectangle to contains</param>
		/// <returns>True if the other rectangle is contained inside this rectangle</returns>
		public static bool Contains(this Rect rect, Rect rectangle)
		{
			return rect.Contains(rectangle, false);
		}
		/// <summary>
		/// Return true if the other rectangle is contained inside this rectangle. If allowInverse is present and true, the width and height of the Rect are allowed to take negative values (ie, the min value is greater than the max), and the test will still work.
		/// </summary>
		/// <param name="rect">this</param>
		/// <param name="rectangle">The rectangle to contains</param>
		/// <param name="allowInverse">Are the width and height of the Rect allowed to take negative values ? (ie, the min value is greater than the max)</param>
		/// <returns>True if the other rectangle is contained inside this rectangle</returns>
		public static bool Contains(this Rect rect, Rect rectangle, bool allowInverse)
		{
			return (rect.Contains(rectangle.min, allowInverse) && rect.Contains(rectangle.max, allowInverse));
		}
	}

}