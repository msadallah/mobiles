namespace AllMyScripts.Common.Tools
{
    using UnityEngine;

    public sealed class GCMemory : MonoBehaviour
	{
		public Rect m_rectGui = new Rect( Screen.width - 150f, Screen.height - 20f, 150f, 20f );
		public GUISkin m_skin;
		public float refreshDelay = 1f;

		private float m_fLastRefreshTime;
		private string m_sTextToShow = string.Empty;

		void OnGUI()
		{
			float fTime = Time.realtimeSinceStartup;
			if( Mathf.Approximately(m_fLastRefreshTime, 0) || fTime - m_fLastRefreshTime > refreshDelay )
			{
				System.GC.Collect();
				m_sTextToShow = System.GC.GetTotalMemory( false ).ToString();
				m_fLastRefreshTime = fTime;
			}
			if( m_skin != null ) GUI.skin = m_skin;
			GUI.Label( m_rectGui, m_sTextToShow, GUI.skin.box );
		}
	}
}