namespace AllMyScripts.Common.Pooling
{
    using TMPro;

    using UnityEngine;
    using System.Collections.Generic;

	using AllMyScripts.Common.Tools;
	
	[AddComponentMenu("AllMyScripts/Tools/Object Pool")]
	public sealed class ObjectPool : MonoBehaviour
	{
		private GameObject m_prefab;
		private bool[] m_uiBehaviourStates;
		private List<GameObject> m_instanceList;
		private List<GameObject> m_usedList;
		private Transform m_trContainer;
	#if UNITY_EDITOR
		// Only useful to name prefab instances in editor
		private string m_sName = "";
		private int m_nInstanceIndex;
#endif
        private bool m_bDisableObjects = true;

        /// <summary>
        /// If true, UI object will be disabled on pooling, and reenabled when getting out of the pool. You want to set this to false if your poolable object instantiate UIBehaviour.
        /// </summary>
        public bool DisableObjects {
            get { return this.m_bDisableObjects; }
            set { this.m_bDisableObjects = value; }
        }

		// ------------------------------------------------------------------------------- //
		// Created Raph 23/01/13
		// ------------------------------------------------------------------------------- //
		/// <summary>
		/// Init the specified prefab and nCount.
		/// </summary>
		/// <param name='prefab'>
		/// Prefab.
		/// </param>
		/// <param name='nCount'>
		/// N count.
		/// </param>
		/// <param name="trContainer">Futur parent for objects in pool</param>
		/// <param name="bDisableObjects">Object must be disabled when set in pool</param>
		public void Init( GameObject prefab, int nCount, Transform trContainer, bool bDisableObjects = true )
		{
			Init( prefab, nCount, trContainer, Vector3.zero, bDisableObjects );
		}
	
		public void Init( GameObject prefab, int nCount, Transform trContainer, Vector3 v3Position, bool bDisableObjects = true )
		{
			if( prefab!=null && m_instanceList==null )
			{
                m_bDisableObjects = bDisableObjects;
				m_prefab = prefab;
				m_trContainer = trContainer;
				m_instanceList = new List<GameObject>( nCount );
				m_usedList = new List<GameObject>();
	#if UNITY_EDITOR
				m_sName = prefab.name;
	#endif
				for( int i=0; i<nCount; i++ )
				{
					GameObject instance = Instantiate( m_prefab, v3Position, Quaternion.identity ) as GameObject;
	#if UNITY_EDITOR
					m_nInstanceIndex = i;
					instance.name = m_sName + "_" + ( i+1 );
	#endif
					PoolObject( instance );
				}

				UnityEngine.EventSystems.UIBehaviour[] uiBehaviours = prefab.GetComponentsInChildren<UnityEngine.EventSystems.UIBehaviour>( true );
				int nUiCount = uiBehaviours.Length;
				m_uiBehaviourStates = new bool[nUiCount];
				for( int i = 0; i < nUiCount; ++i )
					m_uiBehaviourStates[i] = uiBehaviours[i].enabled;
			}
		}
	
		// ------------------------------------------------------------------------------- //
		// Created Raph 24/01/13
		// ------------------------------------------------------------------------------- //
		/// <summary>
		/// Destroies the pool.
		/// </summary>
		public void Destroy()
		{
			if( m_instanceList!=null )
			{
				for( int i=0; i<m_instanceList.Count; i++ )
				{
					GameObject.Destroy( m_instanceList[i] );
				}
			}
			if( m_usedList!=null )
			{
				for( int i=0; i<m_usedList.Count; i++ )
				{
					GameObject.Destroy( m_usedList[i] );
				}
			}
			m_prefab = null;
			m_instanceList = null;
			m_usedList = null;
			m_trContainer = null;
			m_uiBehaviourStates = null;
		}

		// ------------------------------------------------------------------------------- //
		// Created Raph 24/01/13
		// ------------------------------------------------------------------------------- //
		/// <summary>
		/// Destroy Callback from unity
		/// </summary>
		public void OnDestroy()
		{
			Destroy();
		}

		// ------------------------------------------------------------------------------- //
		// Created Raph 23/01/13
		// ------------------------------------------------------------------------------- //
		public GameObject GetPrefab()
		{
			return m_prefab;
		}
	
		// ------------------------------------------------------------------------------- //
		// Created Raph 23/01/13
		// ------------------------------------------------------------------------------- //
		/// <summary>
		/// Gets the pooled instance count.
		/// </summary>
		/// <returns>
		/// The pooled instance count.
		/// </returns>
		public int GetPooledInstanceCount()
		{
			if( m_instanceList!=null )
				return m_instanceList.Count;
			else
				return -1;
		}
	
		// ------------------------------------------------------------------------------- //
		// Created Raph 24/01/13
		// ------------------------------------------------------------------------------- //
		/// <summary>
		/// Gets the used instance count.
		/// </summary>
		/// <returns>
		/// The used instance count.
		/// </returns>
		public int GetUsedInstanceCount()
		{
			if( m_usedList!=null )
				return m_usedList.Count;
			else
				return -1;
		}
	
		// ------------------------------------------------------------------------------- //
		// Created Raph 23/01/13
		// ------------------------------------------------------------------------------- //
		/// <summary>
		/// Gets the instance.
		/// </summary>
		/// <returns>
		/// The instance.
		/// </returns>
		public GameObject GetInstance()
		{
			return GetInstance( Vector3.zero, transform );
		}

		public GameObject GetInstance( Transform trans )
		{
			return GetInstance( Vector3.zero, trans );
		}

		public GameObject GetInstance( Vector3 v3Position )
		{
			return GetInstance( v3Position, transform );
		}

		public GameObject GetInstance( Vector3 v3Position, Transform trans )
		{
			if( m_instanceList!=null && m_usedList!=null && m_prefab!=null )
			{
				GameObject instance = null;
				if( m_instanceList.Count>0 )
				{
					instance = m_instanceList[0];
					m_instanceList.RemoveAt( 0 );
					instance.transform.SetParent(trans);
				}
				else
				{
					instance = Instantiate( m_prefab, v3Position, Quaternion.identity, trans) as GameObject;
	#if UNITY_EDITOR
					instance.name = m_sName + "_" + ( GetUsedInstanceCount()+1 ).ToString();
					Debug.LogWarning( "Pool instantiation " + m_prefab + " Pool max = " + ( GetUsedInstanceCount()+1 ) );
	#endif
				}
				m_usedList.Add( instance );

				if( instance==null )
				{
					Debug.LogWarning( "Pool instance is null!" );
					return null;
				}

				instance.transform.localPosition = v3Position;
				if ( m_bDisableObjects )
				{
					instance.SetActive( true );
				}
				else
				{
					UnityEngine.EventSystems.UIBehaviour[] uiBehaviours = instance.GetComponentsInChildren<UnityEngine.EventSystems.UIBehaviour>( true );
					int nLength = m_uiBehaviourStates.Length;
					if( uiBehaviours.Length != nLength )
					{
						TMP_SubMeshUI[] tmp = instance.GetComponentsInChildren<TMP_SubMeshUI>( true );
						if( tmp != null )
						{
							foreach( TMP_SubMeshUI sub in tmp )
								GameObject.DestroyImmediate( sub.gameObject );
							uiBehaviours = instance.GetComponentsInChildren<UnityEngine.EventSystems.UIBehaviour>( true );
							nLength = m_uiBehaviourStates.Length;
						}
						if( uiBehaviours.Length != nLength )
						{
							nLength = Mathf.Min( nLength, uiBehaviours.Length );
							Debug.LogError( "ObjectPool " + m_prefab.name + " - not same size " + m_uiBehaviourStates.Length + " / " + uiBehaviours.Length );
						}
					}
					for( int i = 0; i < nLength; ++i )
						uiBehaviours[i].enabled = m_uiBehaviourStates[i];
				}

				return instance;
			}
			return null;
		}
	
		// ------------------------------------------------------------------------------- //
		// Created Raph 23/01/13
		// ------------------------------------------------------------------------------- //
		/// <summary>
		/// Pools the object.
		/// </summary>
		/// <param name='instance'>
		/// Instance.
		/// </param>
		public void PoolObject( GameObject instance )
		{
			if( m_instanceList!=null && instance!=null )
			{
                if( m_bDisableObjects )
				{
					instance.SetActive( false );
				}
				else
				{
					UnityEngine.EventSystems.UIBehaviour[] uiBehaviours = instance.GetComponentsInChildren<UnityEngine.EventSystems.UIBehaviour>( true );
					for (int i = 0; i < uiBehaviours.Length; ++i)
						uiBehaviours[i].enabled = false;
				}


				GlobalTools.SetParent( instance.transform, m_trContainer, Vector3.zero, Vector3.one );
				instance.transform.localRotation = Quaternion.identity;
	#if UNITY_EDITOR
				if( m_prefab!=null ) instance.name = string.Concat( m_sName, string.Concat( "_", m_nInstanceIndex ) );
				m_nInstanceIndex++;
	#endif
				m_instanceList.Add( instance );
				m_usedList.Remove( instance );
			}
		}
	
		// ------------------------------------------------------------------------------- //
		// Created Raph 19/03/13
		// ------------------------------------------------------------------------------- //
		public GameObject[] CopyInstanceArray()
		{
			if( m_instanceList!=null )
				return m_instanceList.ToArray();
			else
				return null;
		}
	
		// ------------------------------------------------------------------------------- //
		// Created Raph 19/03/13
		// ------------------------------------------------------------------------------- //
		public GameObject[] CopyUsedInstanceArray()
		{
			if( m_usedList!=null )
				return m_usedList.ToArray();
			else
				return null;
		}
	
		public List<GameObject> UsedInstanceArray
		{
			get
			{
				return m_usedList;
			}
		}
	}
}
