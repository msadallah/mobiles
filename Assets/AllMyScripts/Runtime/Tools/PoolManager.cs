﻿namespace AllMyScripts.Common.Pooling
{
	using System.Collections.Generic;
	
	using UnityEngine;

	public class PoolManager : MonoBehaviour
	{
		public interface IPoolable
		{
			void OnPooled();
			string GetPoolName();
			GameObject GetGameObject();
		}

		[SerializeField]
		private GameObject[] m_prefabToPool = default;

		private Dictionary<string, ObjectPool> m_objectPoolPerPrefab;

		private static PoolManager instance = null;
		public static PoolManager Instance { get { return instance; } }

		private void Awake()
		{
			DontDestroyOnLoad(this.transform.root.gameObject);
			instance = this;
			m_objectPoolPerPrefab = new Dictionary<string, ObjectPool>();
			if (m_prefabToPool != null)
			{
				for (int i = 0; i < m_prefabToPool.Length; i++)
				{
					IPoolable poolableElement = m_prefabToPool[i].GetComponent<IPoolable>();
					if (poolableElement != null)
						CreatePoolFromPrefab(poolableElement);
				}
			}
		}

		/// <summary>
		/// Put an object into the pool of this type
		/// </summary>
		/// <param name="poolableInstance">Instance to pool</param>
		public static bool PoolObject(IPoolable poolableInstance)
		{
			string sName = poolableInstance.GetPoolName();
			if (instance.m_objectPoolPerPrefab.TryGetValue(sName, out ObjectPool objectPool))
			{
				poolableInstance.OnPooled();
				objectPool.PoolObject(poolableInstance.GetGameObject());
				return true;
			}
			return false;
		}

		public static GameObject GetInstance(string sPoolableName)
		{
			if (instance.m_objectPoolPerPrefab.TryGetValue(sPoolableName, out ObjectPool objectPool))
				return objectPool.GetInstance();

			return null;
		}

		public static void CreatePoolFromPrefab(IPoolable poolablePrefab, bool bDisabledObjectOnPool = false)
		{
			string sPoolName = poolablePrefab.GetPoolName();
			if (!instance.m_objectPoolPerPrefab.ContainsKey(sPoolName))
			{
				GameObject goObjectPool = new GameObject("Pool_" + sPoolName);
				goObjectPool.transform.SetParent(instance.transform);
				goObjectPool.transform.localPosition = Vector3.zero;

				ObjectPool objectPool = goObjectPool.AddComponent<ObjectPool>();
				objectPool.Init(poolablePrefab.GetGameObject(), 10, goObjectPool.transform, bDisabledObjectOnPool);
				instance.m_objectPoolPerPrefab[sPoolName] = objectPool;
			}
		}
    }
}