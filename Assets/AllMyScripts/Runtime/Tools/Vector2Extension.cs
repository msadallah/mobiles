﻿using UnityEngine;

 namespace UnityEngine
{
	/// <summary>
	/// Extension for the Vector2 Struct
	/// </summary>
	public static class Vector2Extension
	{
		/// <summary>
		/// Return a new Vector 2 with a rotation of degrees relative to this Vector2 : https://answers.unity.com/questions/661383/whats-the-most-efficient-way-to-rotate-a-vector2-o.html
		/// </summary>
		/// <param name="v">This Vector2</param>
		/// <param name="degrees">Number of degree to rotate</param>
		/// <returns>The new rotated Vector2</returns>
		public static Vector2 Rotate(this Vector2 v, float degrees)
		{
			float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
			float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

			float tx = v.x;
			float ty = v.y;
			v.x = (cos * tx) - (sin * ty);
			v.y = (sin * tx) + (cos * ty);
			return v;
		}
		public static float[] ToArray(this Vector2 v)
		{
			return new float[] { v.x, v.y };
		}
	}
}