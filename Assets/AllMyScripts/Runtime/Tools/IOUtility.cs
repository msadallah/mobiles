namespace AllMyScripts.Common.Tools
{
    using UnityEngine;
    using System.IO;

    /// <summary>
    ///  Class used to manage all particular file / path action
    /// </summary>
    public static class IOUtility
	{

		/// <summary>
		/// Combine Path with a pre-check on left and right string
		/// </summary>
		/// <param name="sPath1">Left path argument</param>
		/// <param name="sPath2">Right path argument</param>
		/// <returns>Return the 2 strings parameters combined</returns>
		public static string CombinePaths( string sPath1, string sPath2 )
		{
			if( string.IsNullOrEmpty( sPath1 ) )
				return sPath2;
			if( string.IsNullOrEmpty( sPath2 ) )
				return sPath1;

			return Path.Combine( sPath1, sPath2 );
		}

		/// <summary>
		/// Method to return the filename
		/// </summary>
		/// <param name="sPath">the file path</param>
		/// <returns>Return the file name with extension</returns>
		public static string GetFileName( string sPath )
		{
			if( sPath == null )
				return null;

			string sName = sPath;
			int nSlash = sName.LastIndexOf( '/' );
			if( nSlash >= 0 ) sName = sName.Substring( nSlash + 1 );
			int nBackSlash = sName.LastIndexOf( '\\' );
			if( nBackSlash >= 0 ) sName = sName.Substring( nBackSlash + 1 );
			return sName;
		}

		/// <summary>
		/// Method to return the filename without extension
		/// </summary>
		/// <param name="sPath">the file path</param>
		/// <returns>Return the file name without extension</returns>
		public static string GetFileNameWithoutExtension( string sPath )
		{
			if( sPath == null )
				return null;

			string sName = GetFileName( sPath );
			int nDot = sName.LastIndexOf( '.' );
			if( nDot >= 0 ) sName = sName.Substring( 0, nDot );
			return sName;
		}

		/// <summary>
		/// Method to replace separator in param string with "/" on windows platform or "\" on all others
		/// </summary>
		/// <param name="sPathToReplace">String with separator to change</param>
		/// <returns>Return the parameter string with the right separator depending on the platform</returns>
		public static string ReplacePathSeparator( string sPathToReplace )
		{
			if( sPathToReplace == null )
				return null;

			char cCurrentSeparator = '\\';
			if( Application.platform == RuntimePlatform.WindowsEditor )
				cCurrentSeparator = '/';

			return sPathToReplace.Replace( cCurrentSeparator, Path.DirectorySeparatorChar );
		}

		/// <summary>
		/// Method to combine an infinite number of string and combine them with right separator char
		/// </summary>
		/// <param name="sPathArray">Dynamic string array to combine</param>
		/// <returns>Return all parameter combined in one string</returns>
		public static string CombineMultiplePaths( params string[] sPathArray )
		{
			if( sPathArray == null )
				return null;

			if( sPathArray.Length == 0 )
				return string.Empty;

			string sFinalPath = sPathArray[0];

			for( int nStringIdx = 1; nStringIdx < sPathArray.Length; ++nStringIdx )
			{
				if( !string.IsNullOrEmpty( sFinalPath ) &&
					!string.IsNullOrEmpty( sPathArray[nStringIdx] ) &&
					(
						sPathArray[nStringIdx][0] == Path.DirectorySeparatorChar ||
						sPathArray[nStringIdx][0] == Path.AltDirectorySeparatorChar ||
						sPathArray[nStringIdx][0] == Path.VolumeSeparatorChar ) )
				{
					sPathArray[nStringIdx] = sPathArray[nStringIdx].Substring( 1 );
				}

				sFinalPath = CombinePaths( sFinalPath, sPathArray[nStringIdx] );
				if( sFinalPath == null )
					return null;
			}

			return ReplacePathSeparator( sFinalPath );
		}

		/// <summary>
		/// Method to get the last separator index
		/// </summary>
		/// <param name="sPath">Path to analyze</param>
		/// <returns>Return the index of the last separator</returns>
		public static int LastIndexOfDirectorySeparator( string sPath )
		{
			if( sPath == null )
				return -1;

			sPath = ReplacePathSeparator( sPath );
			return sPath.LastIndexOf( Path.DirectorySeparatorChar );
		}

		/// <summary>
		/// Method to get folder name
		/// </summary>
		/// <param name="sPath">Path to analyze</param>
		/// <param name="bKeepLastSeparator">Param to keep the last separator (or not)</param>
		/// <returns>Return the folder name with or without last separator</returns>
		public static string GetFolderName( string sPath, bool bKeepLastSeparator = false )
		{
			if( sPath == null )
				return null;

			string sDirName = new DirectoryInfo( sPath ).Name;
			if( bKeepLastSeparator )
				sDirName += Path.DirectorySeparatorChar;
			return sDirName;
		}

#if UNITY_EDITOR
		/// <summary>
		/// Method to delete a file with a "File.Exists" precheck to avoid DirectoryNotFoundException
		/// </summary>
		/// <param name="sFilePath">Path to check</param>
		public static void FileDeleteSafe( string sFilePath )
		{
			if( File.Exists( sFilePath ) )
				File.Delete( sFilePath );
		}

		/// <summary>
		/// Method to move a file with a "File.Exists" precheck to avoid DirectoryNotFoundException
		/// </summary>
		/// <param name="sSource">Original file path</param>
		/// <param name="sDestination">Destination file path</param>
		public static void FileMoveSafe( string sSource, string sDestination )
		{
			if( File.Exists( sSource ) )
				File.Move( sSource, sDestination );
		}

		/// <summary>
		/// Method to create a folder with a "Directory.Exists" precheck
		/// </summary>
		/// <param name="sDirectory">Folder path to create</param>
		public static void DirectoryCreateSafe( string sDirectory )
		{
			if( !Directory.Exists( sDirectory ) )
				Directory.CreateDirectory( sDirectory );
		}

		/// <summary>
		/// Method to delete a file with a "Directory.Exists" precheck to avoid DirectoryNotFoundException
		/// </summary>
		/// <param name="sDirectory">Path to check</param>
		/// <param name="bRecursive">true to remove directory, subdirectories and files in path, false otherwise</param>
		public static void DirectoryDeleteSafe( string sDirectory, bool bRecursive )
		{
			if( Directory.Exists( sDirectory ) )
				Directory.Delete( sDirectory, bRecursive );
		}

		/// <summary>
		/// Method to delete a file with a "Directory.Exists" precheck to avoid DirectoryNotFoundException
		/// </summary>
		/// <param name="sSource">Original folder path</param>
		/// <param name="sDestination">Destination folder path</param>
		public static void DirectoryMoveSafe( string sSource, string sDestination )
		{
			if( Directory.Exists( sSource ) )
			{
				string sDestName = GetFileName( sDestination );
				string sDestParent = sDestination.Substring( 0, sDestination.Length - sDestName.Length );
				DirectoryCreateSafe( sDestParent );
				Directory.Move( sSource, sDestination );
			}
		}
#endif
	}
}
