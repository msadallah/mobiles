namespace AllMyScripts.Common.Tools
{

    using System;
    using System.Globalization;

    using UnityEngine;
    /// <summary>
    /// Static helper functions to convert advanced types (such as objcts) to primary types
    /// </summary>
    public sealed class TryConvertTools
	{
		public static bool ToInt16(object oValue, out short result, short nDefault = 0)
		{
			if (oValue != null)
			{
				try
				{
					result = Convert.ToInt16(oValue);
					return true;
				}
				catch (FormatException)
				{
					try
					{
						result = Convert.ToInt16(oValue, CultureInfo.InvariantCulture);
						return true;
					}
					catch (FormatException)
					{
						Debug.Log("oValue : " + oValue);
						result = nDefault;
						return false;
					}
				}
			}
			else
			{
				result = nDefault;
				return false;
			}
		}

		public static bool ToInt32( object oValue, out int result, int nDefault = 0 )
		{
			if (oValue != null)
			{
				try
				{
					result = Convert.ToInt32(oValue);
					return true;
				}
				catch (FormatException)
				{
					try
					{
						result = Convert.ToInt32(oValue, CultureInfo.InvariantCulture);
						return true;
					}
					catch (FormatException)
					{
						Debug.Log("oValue : " + oValue);
						result = nDefault;
						return false;
					}
				}
			}
			else
			{
				result = nDefault;
				return false;
			}
		}

		public static bool ToLong(object oValue, out long result, long nDefault = 0)
		{
			if (oValue != null)
			{
				try
				{
					result = Convert.ToInt64(oValue);
					return true;
				}
				catch (FormatException)
				{
					try
					{
						result = Convert.ToInt64(oValue, CultureInfo.InvariantCulture);
						return true;
					}
					catch (FormatException)
					{
						Debug.Log("oValue : " + oValue);
						result = nDefault;
						return false;
					}
				}
			}
			else
			{
				result = nDefault;
				return false;
			}
		}

		public static bool ToUInt32( object oValue, out uint result, uint nDefault = 0 )
		{
			if (oValue != null)
			{
				try
				{
					result = Convert.ToUInt32(oValue);
					return true;
				}
				catch (FormatException)
				{
					try
					{
						result = Convert.ToUInt32(oValue, CultureInfo.InvariantCulture);
						return true;
					}
					catch (FormatException)
					{
						Debug.Log("oValue : " + oValue);
						result = nDefault;
						return false;
					}
				}
			}
			else
			{
				result = nDefault;
				return false;
			}
		}

		public static bool ToFloat( object oValue, out float result, float fDefault = 0.0f )
		{
			if (oValue != null)
			{
				try
				{
					result = Convert.ToSingle(oValue);
					return true;
				}
				catch (FormatException)
				{
					try
					{
						result = Convert.ToSingle(oValue, CultureInfo.InvariantCulture);
						return true;
					}
					catch (FormatException)
					{
						Debug.Log("oValue : " + oValue);
						result = fDefault;
						return false;
					}
				}
			}
			else
			{
				result = fDefault;
				return false;
			}
		}

		public static bool ToDouble( object oValue, out double result, double fDefault = 0.0 )
		{
			if (oValue != null)
			{
				try
				{
					result = Convert.ToDouble(oValue);
					return true;
				}
				catch (FormatException)
				{
					try
					{
						result = Convert.ToDouble(oValue, CultureInfo.InvariantCulture);
						return true;
					}
					catch (FormatException)
					{
						Debug.Log("oValue : " + oValue);
						result = fDefault;
						return false;
					}
				}
			}
			else
			{
				result = fDefault;
				return false;
			}
		}

		public static bool ToDoubleFromNumberOrInt(object oValue, out double result, double fDefault = 0.0)
		{
			if (ToDouble(oValue, out result))
			{
				return true;
			}
			int val;
			if (ToInt32(oValue, out val))
			{
				result = (double)val;
				return true;
			}
			result = fDefault;
			return false;
		}

		public static double ToDoubleFromNumberOrInt(object oValue, double fDefault = 0.0)
		{
			double result;
			if (ToDoubleFromNumberOrInt(oValue, out result, fDefault))
			{
				return result;
			}
			return fDefault;
		}

		public static bool ToBool( object oValue, out bool result, bool bDefault = false )
		{
			if (oValue != null)
			{
				try
				{
					result = Convert.ToBoolean(oValue);
					return true;
				}
				catch (FormatException)
				{
					try
					{
						result = Convert.ToBoolean(oValue, CultureInfo.InvariantCulture);
						return true;
					}
					catch (FormatException)
					{
						Debug.Log("oValue : " + oValue);
						result = bDefault;
						return false;
					}
				}
			}
			else
			{
				result = bDefault;
				return false;
			}
		}

		public static bool ToChar( object oValue, out char result, char cDefault = default ( char ) )
		{
			if (oValue != null)
			{
				try
				{
					result = Convert.ToChar(oValue);
					return true;
				}
				catch (FormatException)
				{
				}
			}
			result = cDefault;
			return false;
		}

		public static bool ToString( object oValue, out string result, string sDefault = null )
		{
			if( oValue != null )
			{
				result = oValue.ToString();
				return true;
			}
			else
			{
				result = sDefault;
				return false;
			}
		}

		public static bool ToDateTime( object oValue, out DateTime result, DateTime dDefault = default(DateTime))
		{
			if (oValue != null)
			{
				try
				{
					result = Convert.ToDateTime(oValue);
					return true;
				}
				catch (FormatException)
				{
					try
					{
						result = Convert.ToDateTime(oValue, CultureInfo.InvariantCulture);
						return true;
					}
					catch (FormatException)
					{
						Debug.Log("oValue : " + oValue);
						result = dDefault;
						return false;
					}
				}
			}
			else
			{
				result = dDefault;
				return false;
			}
		}
	}
}
