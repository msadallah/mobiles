﻿namespace AllMyScripts.Common.Tools
{
    using System.Collections.Generic;
    using System.Globalization;

    public static class CurrencyTools
    {
        public class CurrencyModel
        {
            public string ISOCurrencySymbol;
            public string CurrencySymbol;
        }

        private static Dictionary<string, CurrencyModel> currencyModelPerCountryEnglishName;
        private static Dictionary<string, CurrencyModel> currencyModelPerISOCurrencySymbol;
		private static bool isInitialized = false;

        private static void InitCurrencyTools()
        {
			if( isInitialized ) return;

            currencyModelPerCountryEnglishName = new Dictionary<string, CurrencyModel>();
            currencyModelPerISOCurrencySymbol = new Dictionary<string, CurrencyModel>();
            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
            for (int i = 0; i < cultures.Length; i++)
            {
                if (cultures[i].IsNeutralCulture)
                    continue;

                RegionInfo region = null;
                try
                {
                    region = new RegionInfo(cultures[i].LCID);
                }
                catch { }

                if (region == null)
                    continue;

                string currencySymbol = region.CurrencySymbol;
                if (string.IsNullOrEmpty(currencySymbol))
                    currencySymbol = cultures[i].NumberFormat.CurrencySymbol;

                CurrencyModel currencyModel = new CurrencyModel();
                currencyModel.ISOCurrencySymbol = region.ISOCurrencySymbol;
                currencyModel.CurrencySymbol = currencySymbol;

                if (!currencyModelPerCountryEnglishName.ContainsKey(region.EnglishName))
                {
                    currencyModelPerCountryEnglishName[region.EnglishName] = currencyModel;
                }

                if (!currencyModelPerISOCurrencySymbol.ContainsKey(region.ISOCurrencySymbol))
                {
                    currencyModelPerISOCurrencySymbol[region.ISOCurrencySymbol] = currencyModel;
                }
            }

			isInitialized = true;

		}

        public static bool TryGetCurrencyModelWithEnglishCountryName(
                              string EnglishName,
                              out CurrencyModel currencyModel)
        {
			InitCurrencyTools();
			return currencyModelPerCountryEnglishName.TryGetValue(EnglishName, out currencyModel);
        }

        public static bool TryGetCurrencyModelWithISOCurrencySymbol(
                              string ISOCurrencySymbol,
                              out CurrencyModel currencyModel)
        {
			InitCurrencyTools();
			return currencyModelPerISOCurrencySymbol.TryGetValue(ISOCurrencySymbol, out currencyModel);
        }

        public static string GetCurrencyDisplay(string currencyCode, string currencySymbol)
        {
			List<char> missingCharacters;
            if (!string.IsNullOrEmpty(currencySymbol) &&
                TMPro.TMP_Settings.defaultFontAsset.HasCharacters(currencySymbol, out missingCharacters))
                return currencySymbol;

            return currencyCode;
        }
    }
}