namespace AllMyScripts.Common.Tools
{
    using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using System;

	//! @class lwCoroutineTools
	//!
	//! @brief	Tools for coroutine
	public static class CoroutineTools
	{
		//! Continue the coroutine where it paused
		//!
		//!	@param	oEnumerator	the routine enumerator
		//!
		//!	@return true if the routine needs to continue, false it the routine reaches its end
		public static bool ContinueRoutine(IEnumerator oEnumerator)
		{
			if (oEnumerator == null)
			{
				return false;
			}

			if (oEnumerator.Current != null && oEnumerator.Current is IEnumerator)
			{
				IEnumerator nextEnumerator = oEnumerator.Current as IEnumerator;
				if (ContinueRoutine(nextEnumerator))
				{
					return true;
				}
			}
			return oEnumerator.MoveNext();
		}

		public class CoroutineWithData
		{
			public Coroutine coroutine { get; private set; }
			public object result;
			private IEnumerator target;
			public CoroutineWithData(MonoBehaviour owner, IEnumerator target)
			{
				this.target = target;
				this.coroutine = owner.StartCoroutine(Run());
			}

			private IEnumerator Run()
			{
				while (target.MoveNext())
				{
					result = target.Current;
					yield return result;
				}
			}
		}

		#region Global coroutines

		public class GlobalCoroutineHandler : MonoBehaviour
		{
		}

		public struct GlobalCoroutineData
		{
			public IEnumerator enumerator;
			public string routineKey;
			public MonoBehaviour owner;

			public GlobalCoroutineData(IEnumerator e, string k, MonoBehaviour o )
			{
				enumerator = e;
				routineKey = k;
				owner = o;
			}
		}

		private static GlobalCoroutineHandler s_owner = null;
		private static Dictionary<Coroutine, GlobalCoroutineData> s_routines = null;
		private static List<string> s_logs = new List<string>();

		public static Dictionary<Coroutine, GlobalCoroutineData> routines { get { return s_routines; } }
		public static List<string> logs { get { return s_logs; } }
		public static Coroutine StartGlobalCoroutine(IEnumerator target, MonoBehaviour owner = null, string routineKey = null, float timeOut = 0f, Action<GlobalCoroutineData> actionOnTimeOut = null )
		{
			if (s_owner == null)
			{
				GameObject go = new GameObject( "GlobalCoroutineHandler" );
				s_owner = go.AddComponent<GlobalCoroutineHandler>();
				GameObject.DontDestroyOnLoad(s_owner);
			}
			if (owner == null)
				owner = s_owner;

			Coroutine routine = owner.StartCoroutine(RunGlobalCoroutine(target));
			if (timeOut>0f)
				owner.StartCoroutine(RunTimeOutOnGlobalCoroutine(target, timeOut, actionOnTimeOut));
			if (routine != null)
			{
				if (s_routines == null)
					s_routines = new Dictionary<Coroutine, GlobalCoroutineData>();
				s_routines[routine] = new GlobalCoroutineData(target, routineKey, owner);
				AddLog("+ " + routineKey);
				return routine;
			}
			return null;
		}

		public static Coroutine FindGlobalCoroutine(IEnumerator target)
		{
			Coroutine routine = null;
			if (s_routines != null)
			{
				foreach (var keyval in s_routines)
				{
					if (keyval.Value.enumerator == target)
					{
						routine = keyval.Key;
						break;
					}
				}
			}
			return routine;
		}

		public static List<Coroutine> FindGlobalCoroutines(string routineKey)
		{
			List<Coroutine> routines = new List<Coroutine>();
			if (s_routines != null)
			{	
				foreach (var keyval in s_routines)
				{
					if (keyval.Value.routineKey == routineKey)
						routines.Add(keyval.Key);
				}
			}
			return routines;
		}

		public static void StopGlobalCoroutine(IEnumerator target)
		{
			Coroutine routine = FindGlobalCoroutine( target );
			if (routine != null)
				StopGlobalCoroutine( routine );
		}

		public static void StopGlobalCoroutine(string routineKey)
		{
			List<Coroutine> routines = FindGlobalCoroutines(routineKey);
			for( int i = routines.Count - 1; i >=0; --i )
				StopGlobalCoroutine(routines[i]);
		}

		public static void StopGlobalCoroutine(Coroutine routine)
		{
			if (s_routines.ContainsKey(routine))
			{
				GlobalCoroutineData data = s_routines[routine];
				MonoBehaviour owner = data.owner;
				if (owner != null)
					owner.StopCoroutine(routine);
				AddLog("- " + data.routineKey);
				s_routines.Remove(routine);
			}
		}

		private static IEnumerator RunGlobalCoroutine(IEnumerator target)
		{
			while (target.MoveNext())
				yield return target.Current;
			StopGlobalCoroutine(target);
		}

		private static IEnumerator RunTimeOutOnGlobalCoroutine(IEnumerator target, float timeOut, Action<GlobalCoroutineData> actionOnTimeOut)
		{
			yield return new WaitForSeconds(timeOut);

			Coroutine routine = FindGlobalCoroutine(target);
			if (routine != null)
			{
				if(actionOnTimeOut!=null)
					actionOnTimeOut(s_routines[routine]);
				else
					StopGlobalCoroutine(routine);
			}
		}

		private static void AddLog( string msg, int limit=20 )
		{
			s_logs.Add("[GLOBAL_COROUTINE]["+DateTime.Now+"] " + msg);
			if(s_logs.Count> limit)
				s_logs.RemoveAt(0);
		}

		#endregion
	}
}
