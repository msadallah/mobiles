﻿using System;
using UnityEngine;
using UnityEngine.UI;

public static class UnityExtensions
{
	public static void RunOnChildrenRecursive(this GameObject go, Action<GameObject> action)
	{
		if (go == null) return;
		foreach (var trans in go.GetComponentsInChildren<Transform>(true))
		{
			action(trans.gameObject);
		}
	}


	private static Toggle.ToggleEvent emptyEvent = new Toggle.ToggleEvent();
	public static void SetIsOnNoCallback(this Toggle toggle, bool isOn)
	{
		var vnt = toggle.onValueChanged;
		toggle.onValueChanged = emptyEvent;
		toggle.isOn = isOn;
		toggle.onValueChanged = vnt;

	}
}