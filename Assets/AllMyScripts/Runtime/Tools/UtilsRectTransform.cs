﻿namespace UnityEngine.UI
{
    using UnityEngine;

    public static class UtilsRectTransform
    {
        public static RectTransform GetRT(this GameObject g)
        {
            if (g == null)
                return null;
            if (!(g.transform is RectTransform))
                return null;
            return g.transform as RectTransform;
        }

        public static RectTransform GetRT(this Component g)
        {
            if (g == null)
                return null;
            if (!(g.transform is RectTransform))
                return null;
            return g.transform as RectTransform;
        }

        public static Rect GetWorldRect(this RectTransform trans)
        {
            Rect rect = new Rect();

            Vector3[] corners = new Vector3[4];
            trans.GetWorldCorners(corners);

            rect.xMax = corners[2].x;
            rect.xMin = corners[0].x;
            rect.yMax = corners[2].y;
            rect.yMin = corners[0].y;

            return rect;
        }

		public static Rect GetRelativeRect(this RectTransform trans, Transform trRef)
		{
			Rect rect = new Rect();

			Vector3[] corners = new Vector3[4];
			trans.GetWorldCorners(corners);

			Vector3 corner0 = trRef.InverseTransformPoint(corners[0]);
			Vector3 corner2 = trRef.InverseTransformPoint(corners[2]);

			rect.xMax = corner2.x;
			rect.xMin = corner0.x;
			rect.yMax = corner2.y;
			rect.yMin = corner0.y;

			return rect;
		}


		public static Rect GetRectTransformToScreenSpace(this RectTransform transform)
        {
            Canvas canvas = transform.root.GetComponentInChildren<Canvas>();
            RectTransform rtCanvas = canvas.GetComponent<RectTransform>();
            float fCanvasScale = rtCanvas.localScale.x;
            Rect rect = transform.GetWorldRect();
            float fScale = canvas.scaleFactor / fCanvasScale;
            Vector2 v2Pos = rect.position * fScale;
            Vector2 v2Size = rect.size * fScale;
            v2Pos.x += Screen.width * 0.5f - canvas.transform.position.x * fScale;
            v2Pos.y += Screen.height * 0.5f - canvas.transform.position.y * fScale;
            return new Rect(v2Pos, v2Size);
        }

        public static void SetDefaultScale(this RectTransform trans)
        {
            trans.localScale = new Vector3(1, 1, 1);
		}
		public static void SetPivotAndAnchors(this RectTransform trans, Vector2 aVec)
		{
			trans.pivot = aVec;
			trans.anchorMin = aVec;
			trans.anchorMax = aVec;
		}
		public static void SetAnchorsAndOffset(this RectTransform trans, Vector2 anchorMin,Vector2 anchorMax, Vector2 offsetMin,Vector2 offsetMax)
		{
			trans.anchorMin = anchorMin;
			trans.anchorMax = anchorMax;
			trans.offsetMin = offsetMin;
			trans.offsetMax = offsetMax;
		}


		public static Canvas GetCanvas(this RectTransform trans)
        {
            return trans.GetComponentInParent<Canvas>();
        }


        public static Canvas GetRootCanvas(this RectTransform trans)
        {
            Canvas c = trans.GetComponentInParent<Canvas>();
            while (c.transform.parent != null && c.transform.parent.GetComponentInParent<Canvas>() != null)
            {
                c = c.transform.parent.GetComponentInParent<Canvas>();
            }
            return c;
        }

        public static void SetPositionOfPivot(this RectTransform trans, Vector2 newPos)
        {
            trans.localPosition = new Vector3(newPos.x, newPos.y, trans.localPosition.z);
        }

        public static void SetLeftBottomPosition(this RectTransform trans, Vector2 newPos)
        {
            trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width), newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
        }
        public static void SetLeftTopPosition(this RectTransform trans, Vector2 newPos)
        {
            trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width), newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
        }
        public static void SetRightBottomPosition(this RectTransform trans, Vector2 newPos)
        {
            trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width), newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
        }
        public static void SetRightTopPosition(this RectTransform trans, Vector2 newPos)
        {
            trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width), newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
        }

        public static void SetSize(this RectTransform trans, Vector2 newSize)
        {
            if (!float.IsNaN(newSize.x) && !float.IsNaN(newSize.y))
            {
                Vector2 oldSize = trans.rect.size;
                Vector2 deltaSize = newSize - oldSize;
                trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
                trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
            }
        }
        public static void SetWidth(this RectTransform trans, float newSize)
        {
            SetSize(trans, new Vector2(newSize, trans.rect.size.y));
        }
        public static void SetHeight(this RectTransform trans, float newSize)
        {
            SetSize(trans, new Vector2(trans.rect.size.x, newSize));
        }
    }
}