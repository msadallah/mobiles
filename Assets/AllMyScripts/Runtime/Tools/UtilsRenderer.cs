﻿namespace AllMyScripts.Common.Utils
{
    using UnityEngine;

    public class UtilsRenderer
    {
        public static bool AreVisible(Renderer[] renderers)
        {
            for (int i = 0; i < renderers.Length; ++i)
            {
                if (renderers[i].isVisible) return true;
            }
            return false;
        }

        public static Bounds GetBounds(GameObject gameObject, float margin = 0, bool useColliders=false)
        {//calculate bounds
            Bounds bounds = new Bounds(gameObject.transform.position, Vector3.zero);
            Renderer[] rendering = gameObject.GetComponentsInChildren<Renderer>();
            Bounds localBounds;
            if (rendering.Length > 0)
            {
                for (int i = 0; i < rendering.Length; ++i)
                {
					Renderer renderer = rendering[i];
					if (renderer.enabled && renderer.gameObject.activeInHierarchy)
					{
						localBounds = renderer.bounds;
						if (AreBoundsExtended(localBounds))
						{
							if (AreBoundsExtended(bounds))
								bounds.Encapsulate(localBounds);
							else
								bounds = localBounds;
						}
					}
                }
            }
			if (useColliders)
			{
				Collider[] colliders = gameObject.GetComponentsInChildren<Collider>();
				if (colliders != null)
				{
					foreach(var col in colliders)
					{
						if (col.enabled && col.gameObject.activeInHierarchy)
						{
							localBounds = col.bounds;
							if (AreBoundsExtended(localBounds))
							{
								if (AreBoundsExtended(bounds))
									bounds.Encapsulate(localBounds);
								else
									bounds = localBounds;
							}
						}
					}
				}
			}
            bounds.extents += margin * Vector3.one;
            return bounds;
        }

        public static bool AreBoundsExtended(Bounds bounds)
        {
            return bounds.extents != Vector3.zero;
        }
    }
}