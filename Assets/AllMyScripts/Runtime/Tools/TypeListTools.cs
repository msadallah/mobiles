﻿namespace AllMyScripts.Common.Tools
{
    using System;
    using System.Collections.Generic;
    public static class TypeListTools
    {
        public static Type FirstAssignableTypeInListForObject(List<Type> types, object obj)
        {
            if (types.Count > 0 && obj != null)
            {
                foreach( Type t in types)
                {
                    if (t.IsAssignableFrom(obj.GetType()))
                    {
                        return t;
                    }
                }
            }
            return null;
        }
        public static bool HasAssignableTypeInListForObject(List<Type> types, object obj)
        {
            return FirstAssignableTypeInListForObject(types, obj) != null;
        }

        public static Type FirstAssignableTypeForObject(this List<Type> types, object obj)
        {
            return FirstAssignableTypeInListForObject(types, obj);
        }
        public static bool HasAssignableTypeForObject(this List<Type> types, object obj)
        {
            return HasAssignableTypeInListForObject(types, obj);
        }
    }
}