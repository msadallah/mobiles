﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastedCollidersContainer : MonoBehaviour
{
	[SerializeField] private Collider[] _collidersToRayCast;

	/// <summary>
	/// Set the default value of '_collidersToRayCast' array (Default value is : containing all the collider children)
	/// </summary>
	private void Reset()
	{
		_collidersToRayCast = GetComponentsInChildren<Collider>(true);
	}
	public bool PocessCollider(Collider c)
	{
		for (int i = 0; i < _collidersToRayCast.Length; ++i)
		{
			if (_collidersToRayCast[i] == c)
			{
				return true;
			}
		}
		return false;
	}
}
