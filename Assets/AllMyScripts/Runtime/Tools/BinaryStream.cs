namespace AllMyScripts.Common.Tools
{
    using UnityEngine;
    internal class BinaryStream
	{
		public enum SeekMode
		{
			BEGIN = 0,
			END,
			CURRENT
		}

		public enum LenMode
		{
			SIZE8 = 0,
			SIZE16,
			SIZE32
		}

		private byte[] m_pBuffer;
		private uint m_nOffset;
		private uint m_nSize;

		public BinaryStream()
		{
			Init( 2048 );
		}

		public BinaryStream( uint nSize )
		{
			Init( nSize );
		}

		public BinaryStream( byte[] pData )
		{
			m_pBuffer = pData;
			m_nOffset = 0;
			m_nSize = (uint)pData.Length;
		}

		public byte[] GetData()
		{
			return m_pBuffer;
		}

		public uint GetOffset()
		{
			return m_nOffset;
		}

		public uint GetSize()
		{
			return m_nSize;
		}

		public uint GetAllocated()
		{
			return (uint)m_pBuffer.Length;
		}

		public int Seek( int nOffset, SeekMode nMode )
		{
			uint nNewOffset = m_nOffset;
			switch( nMode )
			{
				case SeekMode.BEGIN:
					nNewOffset = (uint)nOffset;
					break;
				case SeekMode.END:
					nNewOffset = (uint)( m_nSize - nOffset );
					break;
				case SeekMode.CURRENT:
					nNewOffset = (uint)( m_nOffset + nOffset );
					break;
				default:
					nNewOffset = 0;
					break;
			}
			int nDelta = (int)( m_nOffset - nNewOffset );
			m_nOffset = nNewOffset;
			return nDelta;
		}

		public bool IsEof()
		{
			return ( m_nOffset >= m_pBuffer.Length );
		}

		public bool ReadBool()
		{
			return ( ReadByte() != 0 );
		}

		public byte ReadU8()
		{
			return ReadByte();
		}

		public uint ReadU16()
		{
			uint n = ReadByte();
			n |= (uint)( ReadByte() << 8 );
			return n;
		}

		public uint ReadU32()
		{
			uint n = ReadByte();
			n |= (uint)( ReadByte() << 8 );
			n |= (uint)( ReadByte() << 16 );
			n |= (uint)( ReadByte() << 24 );
			return n;
		}

		public ulong ReadU64()
		{
			ulong n = ReadU32();
			n |= (ulong)( ReadU32() << 32 );
			return n;
		}

		public string ReadString( LenMode nLenMode )
		{
			byte[] pTemp = ReadBuffer( nLenMode );
			return System.Text.Encoding.UTF8.GetString( pTemp );
		}

		public string ReadString()
		{
			return ReadString( LenMode.SIZE16 );
		}

		public string ReadString8()
		{
			return ReadString( LenMode.SIZE8 );
		}

		public byte[] ReadBytes( uint nLen )
		{
			byte[] pTemp = new byte[nLen];
			if( nLen > 0 )
			{
				uint nLeft = GetAllocated() - m_nOffset;
				if( nLen > nLeft )
				{
					Debug.Log( "[BinaryStream] Size overflow : " + m_nSize + "/" +
								m_pBuffer.Length );
					nLen = nLeft;
				}
				System.Buffer.BlockCopy( m_pBuffer, (int)m_nOffset, pTemp, 0, (int)nLen );
				m_nOffset += nLen;
			}
			return pTemp;
		}

		public byte[] ReadBuffer( LenMode nLenMode )
		{
			uint nLen = 0;
			switch( nLenMode )
			{
				case LenMode.SIZE8:
					nLen = ReadU8();
					break;
				case LenMode.SIZE32:
					nLen = ReadU32();
					break;
				default:
					nLen = ReadU16();
					break;
			}
			return ReadBytes( nLen );
		}

		public void WriteBool( bool bValue )
		{
			WriteByte( (byte)( bValue ? 1 : 0 ) );
		}

		public void WriteU8( byte nValue )
		{
			WriteByte( nValue );
		}

		public void WriteU16( uint nValue )
		{
			WriteByte( (byte)( nValue & 0xFF ) );
			WriteByte( (byte)( ( nValue >> 8 ) & 0xFF ) );
		}

		public void WriteU32( uint nValue )
		{
			WriteByte( (byte)( nValue & 0xFF ) );
			WriteByte( (byte)( ( nValue >> 8 ) & 0xFF ) );
			WriteByte( (byte)( ( nValue >> 16 ) & 0xFF ) );
			WriteByte( (byte)( ( nValue >> 24 ) & 0xFF ) );
		}

		public void WriteU64( ulong nValue )
		{
			WriteU32( (uint)( nValue & 0xFFFFFF ) );
			WriteU32( (uint)( nValue >> 32 ) );
		}

		public void WriteString( string sText, LenMode nLenMode )
		{
			WriteBuffer( System.Text.Encoding.UTF8.GetBytes( sText ), nLenMode );
		}

		public void WriteString( string sText )
		{
			WriteString( sText, LenMode.SIZE16 );
		}

		public void WriteString8( string sText )
		{
			WriteString( sText, LenMode.SIZE8 );
		}

		public void WriteBytes( byte[] pBuffer )
		{
			int nLen = pBuffer.Length;
			for( int i = 0; i < nLen; i++ )
			{
				WriteByte( pBuffer[i] );
			}
		}

		public void WriteBuffer( byte[] pBuffer, LenMode nLenMode )
		{
			uint nLen = (uint)pBuffer.Length;
			switch( nLenMode )
			{
				case LenMode.SIZE8:
					WriteU8( (byte)nLen );
					break;
				case LenMode.SIZE32:
					WriteU32( nLen );
					break;
				default:
					WriteU16( nLen );
					break;
			}
			WriteBytes( pBuffer );
		}

		protected void Init( uint nSize )
		{
			m_pBuffer = new byte[nSize];
			m_nOffset = 0;
			m_nSize = 0;
		}

		private void Realloc( uint nSize )
		{
			byte[] pTemp = new byte[nSize];
			System.Buffer.BlockCopy( m_pBuffer, 0, pTemp, 0, m_pBuffer.Length );
			m_pBuffer = pTemp;
		}

		private byte ReadByte()
		{
			if( IsEof() )
			{
				Debug.Log( "[BinaryStream] Size overflow : " + m_nSize + "/" +
							m_pBuffer.Length );
				return 0;
			}
			return m_pBuffer[(int)( m_nOffset++ )];
		}

		private void WriteByte( byte nByte )
		{
			if( IsEof() )
			{
				uint nAlloc = (uint)m_pBuffer.Length << 1;
				Debug.Log( "[BinaryStream] Realloc from " + m_pBuffer.Length + " to " +
							nAlloc );
				Realloc( nAlloc );
			}
			m_pBuffer[(int)( m_nOffset++ )] = nByte;
			if( m_nSize < m_nOffset )
			{
				m_nSize = m_nOffset;
			}
		}
	}
}
