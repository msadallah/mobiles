namespace AllMyScripts.Common.Tools
{
    using UnityEngine;
    using System.Collections.Generic;

    public sealed class MaterialModifier
	{
		private static Dictionary<string,Material> s_materialDico;
	
		// Init MaterialModifier Dictionary
		public static void InitDictionary()
		{
			if( s_materialDico == null )
				s_materialDico = new Dictionary<string, Material>();
			else
				s_materialDico.Clear();
		}
	
		// Destroy MaterialModifier Dictionary
		public static void DestroyDictionary()
		{
			if( s_materialDico != null )
			{
				s_materialDico.Clear();
				s_materialDico = null;
			}
		}
	
		// Reset MaterialModifier Dictionary
		public static void ResetDictionary()
		{
			if( s_materialDico != null )
			{
				s_materialDico.Clear();
			}
		}
	
		// Recursive function to replace shader by another shader with a new material
		public static void ReplaceMaterialShader( Transform tr, Shader oldShader, Shader newShader )
		{
			ReplaceMaterialShader( tr, oldShader, newShader, Color.magenta );
		}
		public static void ReplaceMaterialShader( Transform tr, Shader oldShader, Shader newShader, Color newColor )
		{
			GlobalTools.Assert( tr != null, "ReplaceMaterialShader no valid transform !!!" );
			GlobalTools.Assert( oldShader != null, "ReplaceMaterialShader no valid oldShader !!!" );
			GlobalTools.Assert( newShader != null, "ReplaceMaterialShader no valid newShader !!!" );

			Renderer matRenderer = tr.GetComponent<Renderer>();

			if( matRenderer != null && matRenderer.materials != null && matRenderer.materials.Length > 0 )
			{
				Material[] matArray = matRenderer.materials;
				Material oldMat = null;
				Material newMat = null;
				int nMatCount = matArray.Length;
				for( int i = 0; i < nMatCount; ++i )
				{
					oldMat = matArray[i];
	//				Debug.Log( "oldMat.shader.name " + oldMat.shader.name + " in " + tr.name );
					if( oldMat.shader == oldShader )
					{
	//					Debug.Log( "ReplaceMaterialShader found " + oldShader + " in " + oldMat.name );
						if( !s_materialDico.TryGetValue( oldMat.name, out newMat ) )
						{
							newMat = new Material( oldMat );
	//						Debug.Log( "ReplaceMaterialShader newMat " + newMat );
							newMat.shader = newShader;
							if( newColor != Color.magenta )
							{
								if( newMat.HasProperty( "_Color" ) )
									newMat.SetColor( "_Color", newColor );
								else if( newMat.HasProperty( "_TintColor" ) )
									newMat.SetColor( "_TintColor", newColor );
							}
							s_materialDico.Add( oldMat.name, newMat );
						}
						matArray[i] = newMat;
					}
				}
				matRenderer.materials = matArray;
			}
		
			foreach( Transform child in tr )
				ReplaceMaterialShader( child, oldShader, newShader, newColor );
		}
	
		// Function to Optimise a shader by another if a specific color is found
		public static void OptimiseMaterialShader( Transform tr, Shader oldShader, Shader newShader, Color testColor )
		{
			GlobalTools.Assert( tr != null, "ReplaceMaterialShader no valid transform !!!" );
			GlobalTools.Assert( oldShader != null, "ReplaceMaterialShader no valid oldShader !!!" );
			GlobalTools.Assert( newShader != null, "ReplaceMaterialShader no valid newShader !!!" );

			Renderer matRenderer = tr.GetComponent<Renderer>();

			if( matRenderer != null && matRenderer.materials != null && matRenderer.materials.Length > 0 )
			{
				Material[] matArray = matRenderer.materials;
				Material oldMat = null;
				Material newMat = null;
				int nMatCount = matArray.Length;
				Color matColor;
				for( int i = 0; i < nMatCount; ++i )
				{
					oldMat = matArray[i];
					if( oldMat.shader == oldShader )
					{
						if( oldMat.HasProperty( "_Color" ) )
							matColor = oldMat.GetColor( "_Color" );
						else if( oldMat.HasProperty( "_TintColor" ) )
							matColor = oldMat.GetColor( "_TintColor" );
						else
							matColor = Color.magenta;
						if( Mathf.Approximately( matColor.r, testColor.r ) && Mathf.Approximately(matColor.g, testColor.g ) && Mathf.Approximately(matColor.b, testColor.b ))
						{
	//						Debug.Log( "ReplaceMaterialShader found " + oldShader + " in " + oldMat.name );
							if( !s_materialDico.TryGetValue( oldMat.name, out newMat ) )
							{
								newMat = new Material( oldMat );
								newMat.shader = newShader;
								s_materialDico.Add( oldMat.name, newMat );
							}
							matArray[i] = newMat;
						}
					}
				}
				matRenderer.materials = matArray;
			}
		
			foreach( Transform child in tr )
				OptimiseMaterialShader( child, oldShader, newShader, testColor );
		}
	
		public static void ReplaceCubeMapInMaterials( Transform tr, Cubemap cubemap )
		{
			Renderer rd = tr.GetComponent<Renderer>();
			if( rd != null )
			{
				Material[] matArray = rd.materials;
				int nMatCount = matArray.Length;
				if( nMatCount > 0 )
				{
					for( int i = 0; i < nMatCount; ++i )
					{
						if( matArray[i].HasProperty( "_Cube" ) )
						{
							matArray[i].SetTexture( "_Cube", cubemap );
						}
					}
					rd.materials = matArray;
				}
			}
		
			foreach( Transform child in tr )
				ReplaceCubeMapInMaterials( child, cubemap );
		}
	
		// Recursive function to change specific Material to specific Low Material
		public static void ChangeMaterialByMaterialLM( Transform tr, Material mat, string nameMat="" )
		{
			if( mat!=null )
			{
				Renderer rd = tr.GetComponent<Renderer>();
				if( rd!=null )
				{
					if( rd.material!=null )
					{
						if( string.IsNullOrEmpty(nameMat) || rd.material.name==nameMat )
						{
							rd.material = mat;
						}
					}
				}
				foreach( Transform child in tr )
					ChangeMaterialByMaterialLM( child, mat, nameMat );
			}
		}
	
		// Recursive function to change Color of specific Material
		public static void ChangeColorMaterial( Transform tr, string nameMat, Color col )
		{
			ChangeColorMaterial( tr, nameMat, col, "_Color" );
		}
	
		// Recursive function to change Color of specific Material
		public static void ChangeColorMaterial( Transform tr, string nameMat, Color col, string sColorName )
		{
			if( !string.IsNullOrEmpty( nameMat ) )
			{
				Renderer matRenderer = tr.GetComponent<Renderer>();
				if( matRenderer != null )
				{
					Material[] mats = Application.isPlaying ? matRenderer.materials : matRenderer.sharedMaterials;
					if( mats != null )
					{
						foreach( Material mat in mats )
						{
							if( mat.name.Contains( nameMat ) )
							{
	//							Debug.Log( mat.name + " Contains " + nameMat + " has " + sColorName + " -> " + mat.HasProperty( sColorName ) );
								if( mat.HasProperty( sColorName ) )
								{
									mat.SetColor( sColorName, col);
	//								Debug.Log( mat.name + " change color to " + col );
								}
							}
						}
					}
				}
				foreach( Transform child in tr )
					ChangeColorMaterial( child, nameMat, col, sColorName );
			}
		}
	
		//Recursive function to change shader for a material
		public static void ChangeShaderForEachMat( Transform tr, Shader shaderToChange, string nameMat )
		{
			if( shaderToChange != null && !string.IsNullOrEmpty( nameMat ) )
			{
				Renderer matRenderer = tr.GetComponent<Renderer>();
				if( matRenderer != null && matRenderer.sharedMaterial != null )
				{
	//				Debug.Log( "matRenderer.sharedMaterial.name " + matRenderer.sharedMaterial.name );
					if( matRenderer.sharedMaterial.name == nameMat )
					{
						matRenderer.material.shader = shaderToChange;
						Debug.Log( matRenderer.material.shader + " change to " + shaderToChange );
						return;
					}
				}
				foreach( Transform child in tr )
					ChangeShaderForEachMat( child, shaderToChange, nameMat );
			}
		}
	
		//Recursive function to change shader by another shader
		public static void ChangeShaderForEachShader( Transform tr, Shader shaderToChange, string shaderName )
		{
			if( shaderToChange != null && !string.IsNullOrEmpty( shaderName ) )
			{
				Renderer matRenderer = tr.GetComponent<Renderer>();
				if( matRenderer != null && matRenderer.sharedMaterial != null )
				{
	//				Debug.Log( "matRenderer.sharedMaterial.name " + matRenderer.sharedMaterial.name );
					if( matRenderer.sharedMaterial.shader.name == shaderName )
					{
						matRenderer.material.shader = shaderToChange;
						Debug.Log( matRenderer.material.shader + " change to " + shaderToChange );
						return;
					}
				}
				foreach( Transform child in tr )
					ChangeShaderForEachShader( child, shaderToChange, shaderName );
			}
		}
	
		// function to change all useful materials
		public static void FindAndChangeAllUsefullMaterials()
		{
			Material[] matTemp = (Material[]) Resources.FindObjectsOfTypeAll( typeof(Material) );

	//		Debug.Log("FindAndChangeAllUsefullMaterials Size : " + mat.Length );

			foreach( Material mater in matTemp )
			{
				if( !mater.name.StartsWith("Hidden") && !mater.name.StartsWith("__EDITOR") && !mater.name.StartsWith("__GUI") && !mater.name.StartsWith("Default") )
				{
					if( mater.shader.name.StartsWith("iPhone/Reflective") && !mater.shader.name.Contains("Colored") )
					{
						Shader shaderToChange = Shader.Find( "iPhone/Diffuse" );
						mater.shader = shaderToChange;
					}
					else if( mater.shader.name.StartsWith("iPhone/Reflective") )
					{
						Shader shaderToChange = Shader.Find( "iPhone/Reflective" );
						mater.shader = shaderToChange;
					}
				}
			}
		}
	}
}
