﻿namespace AllMyScripts.Common.Tools
{
    using UnityEngine;

    public class FullscreenCallback : MonoBehaviour
    {
        public interface IFullscreenCallback
        {
            void OnFullscreenChanged();
        }
        public static InterfaceDelegate<IFullscreenCallback> onFullscreenChanged = new InterfaceDelegate<IFullscreenCallback>();

        public static bool bFullscreen;

        private void Awake()
        {
            bFullscreen = Screen.fullScreen;
        }

		private void Update()
		{
			CheckFullscreen();
		}
		/// <summary>
		/// regularly called, if the value changed, callback
		/// </summary>
		private void CheckFullscreen()
        {
            if (Screen.fullScreen != bFullscreen)
            {
                bFullscreen = Screen.fullScreen;
                for (int i = 0; i < onFullscreenChanged.Count; ++i) onFullscreenChanged[i].OnFullscreenChanged();
            }
        }
    }
}