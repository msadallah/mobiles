﻿using UnityEngine;

[AddComponentMenu(""),ExecuteAlways]
public class GameObjectCommentHolder : MonoBehaviour
{
	[HideInInspector] public string text;
#if !UNITY_EDITOR && DEVELOPMENT_BUILD
	public void OnGUI()
	{
		//GUILayout.Label("If you can read this text, GameObjectComments are NOT working as intended\nI'm "+gameObject.transform.parent.name+"\nMy tag is "+gameObject.tag+"\n"+text);
	}
#endif
	private void OnEnable()
	{
		gameObject.hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector;
	}
}