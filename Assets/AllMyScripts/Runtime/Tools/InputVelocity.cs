﻿namespace AllMyScripts.Common.Tools
{
    using System.Collections.Generic;
    using UnityEngine;

    public class InputVelocity : MonoBehaviour
    {
        public float averageVelocityTime = 0.25f;

        public struct TimedInput
        {
            public float time;
            public Vector3 position;
        }

        public bool bDebug = false;
        private void OnDrawGizmosSelected()
        {
            if (bDebug)
            {
                foreach (TimedInput input in timedInputs)
                {
                    Gizmos.DrawCube(input.position, Vector3.one * 5f);
                }
            }
        }

        private List<TimedInput> timedInputs = new List<TimedInput>();
        private Vector3 inputVelocity;
        private bool bRecalculate = true;
        private static InputVelocity instance;

        // Use this for initialization
        void Awake()
        {
            if (instance != null)
            {
                Destroy(instance);
            }
            instance = this;
        }

        private void Update()
        {
            if (!float.IsNaN(Input.mousePosition.x))
            {
                TimedInput i = new TimedInput();
                i.time = Time.time;
                i.position = Input.mousePosition;
                timedInputs.Add(i);
            }

            for (int i = 0; i < timedInputs.Count; ++i)
            {
                if (timedInputs[i].time < Time.time - averageVelocityTime)
                {
                    timedInputs.RemoveAt(i);
                    --i;
                }
            }
            bRecalculate = true;
        }

        private Vector3 GetVelocity()
        {
            if (bRecalculate)
            {
                bRecalculate = false;
                inputVelocity = Vector3.zero;
                for (int i = 0; i < timedInputs.Count - 1; ++i)
                {
                    inputVelocity += timedInputs[i + 1].position - timedInputs[i].position;
                }
                inputVelocity /= timedInputs.Count;
                inputVelocity /= averageVelocityTime;
            }
            return inputVelocity;
        }

        /// <summary>
        /// the velocity of the finger in pixels
        /// </summary>
        public static Vector3 velocity
        {
            get
            {
                return GetInputVelocity();
            }
        }

        private static Vector3 GetInputVelocity()
        {
            return instance.GetVelocity();
        }
    }
}