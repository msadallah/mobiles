﻿ namespace UnityEngine
{
	/// <summary>
	/// Extension for the Vector3 Struct
	/// </summary>
	public static class Vector3Extension
	{
		public static float[] ToArray(this Vector3 v)
		{
			return new float[] { v.x, v.y, v.z };
		}
	}
}