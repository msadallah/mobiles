﻿namespace AllMyScripts.Common.Tools
{
    using System;
    using System.Collections.Generic;
    using AllMyScripts.Common.UI;
    using UnityEngine;
	using UnityEngine.Serialization;

	[DefaultExecutionOrder(-90)]
	[RequireComponent(typeof(Camera))]
	public class PrioritizedRaycaster : MonoBehaviour
	{
		[System.Flags]
		public enum PrioritizedRayCasterEventType
		{
			None = 0,
			mouseHoverEnded = 1,
			mouseHover = 2,
			clickDown = 4,
			clickUp = 8,
			click = 16,
			doubleClick = 32,
			rightClick = 64
		}
		public enum ScreenDistanceType { click = 0, doubleClickDistance = 1, nextClickDistance = 2 }

		/// <summary>
		/// interface to listen to the raycasts. 
		/// OnRaycast returns true if it consumed the raycast
		/// and wants to prevent other callback from being called.
		/// Otherwise, it returns false
		/// </summary>
		public interface IPrioritizedRaycastListener
		{
			/// <summary>
			/// OnPrioritizedRaycast returns true if it consumed the raycast
			/// and wants to prevent other callback from being called.
			/// Otherwise, it returns false
			/// </summary>
			/// <param name="layer">the raycasted layer</param>
			/// <param name="raycast">the result of the raycast</param>
			/// <param name="eventType">What happened on this event</param>
			/// <param name="raycastsList">the list of all the hits of this raycast - CAN BE THE SAME AS <paramref name="raycast"/></param>
			/// <returns>true if raycast has been consumed (and shouldn't call other IListeners of lower priority), false otherwise</returns>
			bool OnPrioritizedRaycast(int[] layer, RaycastHit raycast, PrioritizedRayCasterEventType eventType, RaycastHit[] raycastsList);
		}

		public interface IBeginRaycastListener
		{
			/// <summary>
			/// OnBeginRaycast is called before the launch of raycasts to prepare layers & colliders
			/// </summary>
			/// <param name="leftEventClick"></param>
			/// <param name="rightEventClick"></param>
			/// <param name="mouseDown"></param>
			/// <param name="mouseUp"></param>
			/// <param name="mousePos"></param>
			void OnBeginRaycast(ClickData leftEventClick, ClickData rightEventClick, bool mouseDown, bool mouseUp, Vector3 mousePos);
		}

		[System.Serializable]
		public class PrioritizedLayer
		{
			[SerializeField] public int[] layers;
			[FormerlySerializedAs("priority")]
			public int executeOrder;

			[HideInInspector] public Vector2 lastClickPosition = Vector2.zero;
			[HideInInspector] public List<ListenerHitPair> _lastHovered;
			[HideInInspector] public RaycastHit[] _lastSortedHits;
			[HideInInspector] public List<RaycastHit> _hitsClickOrder;

			public static LayerMask GetLayerMask(int[] layers)
			{
				if (layers == null || layers.Length == 0)
				{
					return 0;
				}
				else
				{
					string[] names = new string[layers.Length];
					for (int i = 0; i < layers.Length; ++i)
					{
						names[i] = LayerMask.LayerToName(layers[i]);
					}
					return LayerMask.GetMask(names);
				}
			}
			public LayerMask GetLayerMask()
			{
				return GetLayerMask(layers);
			}
			/// <summary>
			/// Check mouse distance from last click and reset sorted hits and hits click order if too far away.
			/// </summary>
			/// <param name="mousePos">The current mouse position</param>
			/// <param name="squaredMaxAcceptedDistance">The max autorized distance from the last click position</param>
			public void CheckMouseDistanceFromLastClick(Vector2 mousePos, float squaredMaxAcceptedDistance)
			{
				if (_lastSortedHits != null && Vector2.SqrMagnitude(mousePos - lastClickPosition) > squaredMaxAcceptedDistance)
				{
					ClearLastClickRayCastData();
				}
			}
			/// <summary>
			/// Is this raycast similar to the last one 
			/// </summary>
			/// <param name="sortedRayCasts">The new raycast result to test</param>
			/// <returns>Is this raycast similar to the last one</returns>
			public bool SortedRayCastSimilar(RaycastHit[] sortedRayCasts)
			{
				if (sortedRayCasts == null || sortedRayCasts.Length < 1)
				{
					return (_lastSortedHits == null || _lastSortedHits.Length < 1);
				}
				else if (_lastSortedHits == null || _lastSortedHits.Length < 1)
				{
					return false;
				}
				else if (sortedRayCasts.Length != _lastSortedHits.Length)
				{
					return false;
				}
				for (int i = 0; i < sortedRayCasts.Length; ++i)
				{
					if (sortedRayCasts[i].transform != _lastSortedHits[i].transform)
					{
						RaycastedCollidersContainer  newElem = sortedRayCasts[i].transform.GetComponentInParent<RaycastedCollidersContainer>();
						RaycastedCollidersContainer  oldElem = null;
						if (_lastSortedHits[i].transform != null)
						{
							oldElem = _lastSortedHits[i].transform.GetComponentInParent<RaycastedCollidersContainer>();
						}
						if (newElem == null || newElem != oldElem)
						{
							return false;
						}
					}
				}
				return true;
			}

			/// <summary>
			/// Test the existing lastRaycast values and check for unconsistent values (null object due to their supression for exemple. will then clear these data if "clearUnconsistantList" is set to true [Default Value].
			/// </summary>
			/// <param name="clickDataValid">[OUT] Is last click raycast list valid</param>
			/// <param name="hoverDataValid">[OUT] Is last hover raycast list valid</param>
			/// <param name="clearUnconsistantList">[DEFAULT YES] Should this function destroy unconsistent values. Highly recomanded to avoid nullReference exceptions</param>
			public void TestLastRaycastsConsistancy(out bool clickDataValid, out bool hoverDataValid, bool clearUnconsistantList = true)
			{
				clickDataValid = true;
				if (_lastSortedHits != null && _lastSortedHits.Length > 0)
				{
					for (int i = 0; i < _lastSortedHits.Length; ++i)
					{
						if (_lastSortedHits[i].transform == null)
						{
							if (clearUnconsistantList)
							{
								ClearLastClickRayCastData();
							}
							clickDataValid = false;
							break;
						}
					}
				}
				hoverDataValid = true;
				if (_lastHovered != null && _lastHovered.Count > 0)
				{
					for (int i = 0; i < _lastHovered.Count; ++i)
					{
						if (_lastHovered[i] == null || _lastHovered[i].hit.transform == null)
						{
							if (clearUnconsistantList)
							{
								ClearLastHoverRayCastData();
							}
							hoverDataValid = false;
							break;
						}
					}
				}
			}
			/// <summary>
			/// Set _lastSortedHits and _hitsClickOrder to null
			/// </summary>
			private void ClearLastClickRayCastData()
			{
				_lastSortedHits = null;
				_hitsClickOrder = null;
			}
			/// <summary>
			/// Set _lastHovered to null
			/// </summary>
			private void ClearLastHoverRayCastData()
			{
				_lastHovered = null;
			}

			/// <summary>
			/// Put the first RaycastHit of hitsClickOrder to the last position
			/// </summary>
			public void HitClickRoll()
			{
				if (_hitsClickOrder != null && _hitsClickOrder.Count > 0)
				{
					RaycastHit rch = _hitsClickOrder[0];
					_hitsClickOrder.RemoveAt(0);
					_hitsClickOrder.Add(rch);
				}
			}
			/// <summary>
			/// call all the needed OnHoverEnd event for listeners, then update  the _lastHovered List with the newList;
			/// </summary>
			/// <param name="newList">Current list of Listener/Hit hovererd target</param>
			public void CallOnHoverEndAndUpdateLastHovered(List<ListenerHitPair> newList)
			{
				if (_lastHovered == null || _lastHovered.Count == 0)
				{ }
				else
				{
					if (newList == null || newList.Count < 1)
					{
						foreach (ListenerHitPair lhp in _lastHovered)
						{
							if (lhp.hit.transform != null)
							{
								lhp.listener.listener.OnPrioritizedRaycast(lhp.layers, lhp.hit, PrioritizedRayCasterEventType.mouseHoverEnded, new RaycastHit[] { lhp.hit });
							}
						}
					}
					else
					{
						for (int i = 0; i < _lastHovered.Count; ++i)
						{
							if (!newList.Contains(_lastHovered[i]))
							{

								if (_lastHovered[i].hit.transform != null)
								{
									_lastHovered[i].listener.listener.OnPrioritizedRaycast(_lastHovered[i].layers, _lastHovered[i].hit, PrioritizedRayCasterEventType.mouseHoverEnded, new RaycastHit[] { _lastHovered[i].hit });
								}
							}
						}
					}
				}
				_lastHovered = newList;
			}
		}
		public class PrioritizedListener<T>
		{
			public T listener;
			public int executeOrder;
		}
		public class ListenerHitPair
		{
			public int[] layers;
			public PrioritizedListener<IPrioritizedRaycastListener> listener;
			public RaycastHit hit;
			public ListenerHitPair(int[] layers, PrioritizedListener<IPrioritizedRaycastListener> listener, RaycastHit hit)
			{
				this.layers = new int[layers.Length];
				for (int i = 0; i < layers.Length; ++i)
				{
					this.layers[i] = layers[i];
				}
				this.listener = listener;
				this.hit = hit;
			}
			public static bool LayersEquals(int[] t1, int[] t2)
			{
				if (ReferenceEquals(t1, t2))
				{
					return true;
				}
				if (ReferenceEquals(t1, null))
				{
					return false;
				}
				if (ReferenceEquals(t1, null))
				{
					return false;
				}
				if (t1.Length != t2.Length)
				{
					return false;
				}
				for (int i = 0; i < t1.Length; i++)
				{
					if (!System.Array.Exists<int>(t2, x => x == t1[i]))
					{
						return false;
					}
				}
				return true;
			}
			public static bool operator ==(ListenerHitPair lhp1, ListenerHitPair lhp2)
			{

				if (ReferenceEquals(lhp1, lhp2))
				{
					return true;
				}
				if (ReferenceEquals(lhp1, null))
				{
					return false;
				}
				if (ReferenceEquals(lhp2, null))
				{
					return false;
				}
				return ((LayersEquals(lhp1.layers, lhp2.layers)) && (lhp1.listener == lhp2.listener) && (lhp1.hit.transform.gameObject == lhp2.hit.transform.gameObject));
			}
			public static bool operator !=(ListenerHitPair lhp1, ListenerHitPair lhp2)
			{
				return !(lhp1 == lhp2);
			}
			public bool Equals(ListenerHitPair other)
			{
				if (ReferenceEquals(null, other) || ReferenceEquals(null, other.hit.transform))
				{
					return false;
				}
				if (ReferenceEquals(this, other))
				{
					return true;
				}
				if (ReferenceEquals(this.hit.transform, null))
				{
					return false;
				}
				return ((LayersEquals(layers, other.layers)) && (listener.Equals(other.listener)) && (hit.transform.gameObject.Equals(other.hit.transform.gameObject)));
			}
			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj))
				{
					return false;
				}
				if (ReferenceEquals(this, obj))
				{
					return true;
				}

				return obj.GetType() == GetType() && Equals((ListenerHitPair)obj);
			}
			public override int GetHashCode()
			{
				return (layers, listener, hit).GetHashCode();
			}
		}
		public class ClickData
		{
			public bool isDoubleClick;
			public Vector2 position;
			public float clickTime;
			public ClickData(bool isDoubleClick, Vector2 position, float clickTime)
			{
				this.isDoubleClick = isDoubleClick;
				this.position = position;
				this.clickTime = clickTime;
			}
			public ClickData(ClickData firstClick, ClickData secondClick)
			{
				if (firstClick == null  || secondClick == null)
				{
					Debug.LogWarning("One of the given click for double click is null, this constructor  will not work as expected !");
					isDoubleClick = default;
					position = default;
					clickTime = default;
				}
				else
				{
					this.isDoubleClick = true;
					this.position = ((firstClick.position + secondClick.position) * 0.5f);
					this.clickTime = ((firstClick.clickTime + secondClick.clickTime) * 0.5f);
				}
			}
		}

		[SerializeField]
		protected bool _blockGUI = true;
		[SerializeField]
		protected List<PrioritizedLayer> _layers = new List<PrioritizedLayer>();

		public InterfaceDelegate<IBeginRaycastListener> IDOnBeginRaycast = new InterfaceDelegate<IBeginRaycastListener>();

		protected List<PrioritizedListener<IPrioritizedRaycastListener>> _listeners = new List<PrioritizedListener<IPrioritizedRaycastListener>>();
		protected List<PrioritizedListener<IPrioritizedRaycastListener>> _lastMouseovers = new List<PrioritizedListener<IPrioritizedRaycastListener>>();
		protected List<PrioritizedListener<IPrioritizedRaycastListener>> _clicksSort = new List<PrioritizedListener<IPrioritizedRaycastListener>>();

		protected Camera _camera;
		protected float _squaredMaxMouseDistanceBeforeReset = 0f;
		protected float _squaredMaxMouseDistanceForClick = 0f;
		protected float _squaredMaxMouseDistanceForDoubleClick = 0f;
		private int _lastCameraPixelWidth = 0;
		private int _lastCameraPixelHeight = 0;

		private Vector2?    _lastMouseDownPosition = null;
		private float       _lastMouseDownTime = -1f;
		private Vector2?    _lastRightMouseDownPosition = null;
		private float       _lastRightMouseDownTime = -1f;

		private List<ClickData> _bufferedClick = new List<ClickData>();

		protected virtual void Awake()
		{
			_camera = GetComponent<Camera>();
			//sort the prioritized elements
			_layers.Sort(SortLayers);
			CalculateMaxMouseDistanceBeforeReset();
		}
		protected virtual void Update()
		{
			if (HasCameraResized())
			{
				CalculateMaxMouseDistanceBeforeReset();
			}
			ClickData[] clicks = GetClickState();
			if (_layers != null && _layers.Count > 0)
			{
				foreach (PrioritizedLayer pl in _layers)
				{
					pl.TestLastRaycastsConsistancy(out bool clickDataValid, out bool hoverDataValid);
					pl.CheckMouseDistanceFromLastClick(Input.mousePosition, _squaredMaxMouseDistanceBeforeReset);
				}
			}
			if (UtilsUI.IsPointerOverUIObject())
			{
				return;
			}
			RayCastMouse(clicks[0], clicks[1], Input.GetMouseButtonDown(0),Input.GetMouseButtonUp(0));
		}

		/// <summary>
		/// Add a listener. The higher the priority, the later it gets called
		/// </summary>
		/// <param name="listener">the listener</param>
		/// <param name="executeOrder">The higher the priority, the later it gets called</param>
		public void AddListener(IPrioritizedRaycastListener listener, int executeOrder = 0)
		{
			_listeners.Add(new PrioritizedListener<IPrioritizedRaycastListener>() { listener = listener, executeOrder = executeOrder });
			_listeners.Sort(SortListeners);
		}
		public void RemoveListener(IPrioritizedRaycastListener listener)
		{
			for (int i = 0; i < _listeners.Count; ++i)
			{
				if (_listeners[i].listener == listener)
				{
					_listeners.RemoveAt(i);
					--i;
				}
			}
			//no need to sort again
		}
		private int SortLayers(PrioritizedLayer x, PrioritizedLayer y)
		{
			return x.executeOrder.CompareTo(y.executeOrder);
		}
		private int SortListeners(PrioritizedListener<IPrioritizedRaycastListener> x, PrioritizedListener<IPrioritizedRaycastListener> y)
		{
			return x.executeOrder.CompareTo(y.executeOrder);
		}
		protected bool HasCameraResized()
		{
			return ((_lastCameraPixelHeight != _camera.pixelHeight) || (_lastCameraPixelWidth != _camera.pixelWidth));
		}
		/// <summary>
		/// If the mouse moved for more than 5% of the diagonal scale of the _camera, we reset layers clickRoll relative data.
		/// </summary>
		protected void CalculateMaxMouseDistanceBeforeReset()
		{
			float percentDistanceForNextClicks = 0.05f;
			float percentDistanceForClick = 0.02f;
			float percentDistanceForDoubleClick = 0.02f;
			_squaredMaxMouseDistanceBeforeReset = ((_camera.pixelWidth * percentDistanceForNextClicks) * (_camera.pixelWidth * percentDistanceForNextClicks)) + ((_camera.pixelHeight * percentDistanceForNextClicks) * (_camera.pixelHeight * percentDistanceForNextClicks));
			_squaredMaxMouseDistanceForClick = ((_camera.pixelWidth * percentDistanceForClick) * (_camera.pixelWidth * percentDistanceForClick)) + ((_camera.pixelHeight * percentDistanceForClick) * (_camera.pixelHeight * percentDistanceForClick));
			_squaredMaxMouseDistanceForDoubleClick = ((_camera.pixelWidth * percentDistanceForDoubleClick) * (_camera.pixelWidth * percentDistanceForDoubleClick)) + ((_camera.pixelHeight * percentDistanceForDoubleClick) * (_camera.pixelHeight * percentDistanceForDoubleClick));
			_lastCameraPixelWidth = _camera.pixelWidth;
			_lastCameraPixelHeight = _camera.pixelHeight;
		}
		protected bool MouseInRange(Vector2 a, Vector2 b, ScreenDistanceType screenDistance)
		{
			float distance = 0f;
			switch (screenDistance)
			{
				case ScreenDistanceType.click:
					distance = _squaredMaxMouseDistanceForClick;
					break;
				case ScreenDistanceType.doubleClickDistance:
					distance = _squaredMaxMouseDistanceForDoubleClick;
					break;
				case ScreenDistanceType.nextClickDistance:
					distance = _squaredMaxMouseDistanceBeforeReset;
					break;
				default:
					Debug.LogWarning("Not implement case !");
					break;
			}
			return MouseInRange(a,b,distance);
		}
		protected bool MouseInRange(Vector2 a, Vector2 b, float maxSquaredScreenDistance)
		{
			return ((a - b).sqrMagnitude <= maxSquaredScreenDistance);
		}
		protected ClickData[] GetClickState()
		{
			ClickData[] ClicksToReturn = new ClickData[2] {null,null};

			float simpleClickTime = 0.18f;
#if DUAL_CLICK_ENABLED
			float doubleClickTime = 0.375f;
#endif
			float rightClickTime = 0.18f;

			//We do not check down or up mouse events if we  are over UI
			if (!UtilsUI.IsPointerOverUIObject())
			{
				//Left Clicks : left click can be buffered if 'DUAL_CLICK_ENABLED' is true
				if (Input.GetMouseButtonDown(0))
				{
					_lastMouseDownPosition = Input.mousePosition;
					_lastMouseDownTime = Time.time;
				}
				else if (Input.GetMouseButtonUp(0))
				{
					if (_lastMouseDownPosition != null && _lastMouseDownTime > 0f)
					{
						if ((Time.time <= (_lastMouseDownTime + simpleClickTime)) && MouseInRange(Input.mousePosition, _lastMouseDownPosition.Value, ScreenDistanceType.click))
						{
							//it is a click, we add  it to our buffedClick array
							_bufferedClick.Add(new ClickData(false, _lastMouseDownPosition.Value, _lastMouseDownTime));
						}
						_lastMouseDownPosition = null;
						_lastMouseDownTime = -1f;
					}
				}

				//Right Clicks : right click are always instantaneous
				if (Input.GetMouseButtonDown(1))
				{
					_lastRightMouseDownPosition = Input.mousePosition;
					_lastRightMouseDownTime = Time.time;
				}
				else if (Input.GetMouseButtonUp(1))
				{
					if (_lastRightMouseDownPosition != null && _lastRightMouseDownTime > 0f)
					{
						if ((Time.time <= (_lastRightMouseDownTime + rightClickTime)) && MouseInRange(Input.mousePosition, _lastRightMouseDownPosition.Value, ScreenDistanceType.click))
						{
							ClicksToReturn[1] = new ClickData(false, _lastRightMouseDownPosition.Value, _lastRightMouseDownTime);
						}
						_lastRightMouseDownPosition = null;
						_lastRightMouseDownTime = -1f;
					}
				}
			}
			//Look if we have left click  waiting to be sent 
			if (_bufferedClick.Count > 0)
			{
#if DUAL_CLICK_ENABLED
				if ((_bufferedClick[0].clickTime + doubleClickTime) <= Time.time)
				{
					//we return this as simple or double click
					if (_bufferedClick.Count > 1)
					{
						//we search for a double click
						if (MouseInRange(_bufferedClick[0].position, _bufferedClick[1].position, ScreenDistanceType.doubleClickDistance))
						{
							ClicksToReturn[0] = new ClickData(_bufferedClick[0], _bufferedClick[1]);
							_bufferedClick.RemoveRange(0, 2);
						}
					}
					if (ClicksToReturn[0] == null)
					{
						ClicksToReturn[0] = _bufferedClick[0];
						_bufferedClick.RemoveAt(0);
					}
				}
#else
				ClicksToReturn[0] = _bufferedClick[0];
				_bufferedClick.RemoveAt(0);
#endif
			}
			return ClicksToReturn;
		}

		protected void RayCastMouse(ClickData leftEventClick, ClickData rightEventClick, bool mouseDown, bool mouseUp)
		{
			RayCastMouse(leftEventClick, rightEventClick, mouseDown, mouseUp, Input.mousePosition);
		}
		/// <summary>
		/// Run the PrioritizedRaycaster logic for this frame managing layers and calling events.
		/// </summary>
		/// <param name="leftEventClick">Has the mouse cliked this frame.</param>
		protected void RayCastMouse(ClickData leftEventClick, ClickData rightEventClick, bool mouseDown, bool mouseUp,Vector3 mousePos)
		{
			// Trigger begin raycast to prepare some layers / colliders if needed
			if (IDOnBeginRaycast != null)
			{
				for(int i = 0; i < IDOnBeginRaycast.Count; ++i)
				{
					IDOnBeginRaycast[i].OnBeginRaycast(leftEventClick, rightEventClick, mouseDown, mouseUp, mousePos);
				}
			}

			List<PrioritizedListener<IPrioritizedRaycastListener>> _newMouseovers = new List<PrioritizedListener<IPrioritizedRaycastListener>>();

			//We are going through 3 independent layers raycasts:
			//One for Hover and Click down/up event
			//One for left clicks and left dual click
			//One for right clicks
			{
				Ray ray = _camera.ScreenPointToRay(mousePos);
				//Hover and clickDown/Ups : we are going to raycast each layer individually in order of priority.
				foreach (PrioritizedLayer layer in _layers)
				{
					//Get all Hit for this layer, then sort them, we clear object with multiple colliders
					RaycastHit[] hits = Physics.RaycastAll(ray, _camera.farClipPlane, layer.GetLayerMask());
					System.Array.Sort<RaycastHit>(hits, RayCastTools.SortRaycastHit);
					hits = ClearHitList(hits);

					if (hits != null && hits.Length > 0)
					{

						//the raycast has been sent, we are sending the information to all listeners in order, and if one returns true, 
						//then it means it prevents other scripts from getting the raycast ; it consumes it.
						//listeners of the same priority can't block each other.

						int hoverBlocker = -1; //blocker will be set to the priority of the first callback which returned true

						//We Start With OnHover
						List<ListenerHitPair> listenerHitPair  = new List<ListenerHitPair>();
						for (int i = 0; i < hits.Length; ++i)
						{
							for (int j = 0; j < _listeners.Count; ++j)
							{
								if (hoverBlocker != -1 && hoverBlocker < _listeners[j].executeOrder)
								{
									//we're blocked by the previous set of callbacks
									i = hits.Length;
									break;
								}

								if (_listeners[j].listener.OnPrioritizedRaycast(layer.layers, hits[i], PrioritizedRayCasterEventType.mouseHover, hits))
								{
									//this listener wants to block the callbacks
									hoverBlocker = _listeners[j].executeOrder;

								}
								listenerHitPair.Add(new ListenerHitPair(layer.layers, _listeners[j], hits[i]));
							}
							if (hoverBlocker != -1)
							{
								break;
							}
						}
						//Now calling OnHoverEnded
						layer.CallOnHoverEndAndUpdateLastHovered(listenerHitPair);

						if (mouseDown)
						{
							int clickDownBlocker = -1;
							for (int i = 0; i < _listeners.Count; ++i)
							{
								if (clickDownBlocker != -1 && clickDownBlocker < _listeners[i].executeOrder)
								{
									//we're blocked by the previous set of callbacks
									break;
								}

								if (_listeners[i].listener.OnPrioritizedRaycast(layer.layers, hits[0], PrioritizedRayCasterEventType.clickDown, hits))
								{
									//this listener wants to block the callbacks
									clickDownBlocker = _listeners[i].executeOrder;

								}
							}
							if(clickDownBlocker != -1)
							{
								break;
							}
						}
						else if (mouseUp)
						{
							int clickUpBlocker = -1;
							for (int i = 0; i < _listeners.Count; ++i)
							{
								if (clickUpBlocker != -1 && clickUpBlocker < _listeners[i].executeOrder)
								{
									//we're blocked by the previous set of callbacks
									break;
								}

								if (_listeners[i].listener.OnPrioritizedRaycast(layer.layers, hits[0], PrioritizedRayCasterEventType.clickUp, hits))
								{
									//this listener wants to block the callbacks
									clickUpBlocker = _listeners[i].executeOrder;

								}
							}

							if (clickUpBlocker != -1)
							{
								break;
							}
						}
					}
					else //Clean hover list
					{
						layer.CallOnHoverEndAndUpdateLastHovered(null);
					}
				}
			}
			if (leftEventClick != null)
			{
				Ray clickRay = _camera.ScreenPointToRay(leftEventClick.position);
				//Left click and DoubleClick : we are going to raycast each layer individually in order of priority.
				foreach (PrioritizedLayer layer in _layers)
				{
					RaycastHit[] clickHits = Physics.RaycastAll(clickRay, _camera.farClipPlane, layer.GetLayerMask());
					System.Array.Sort<RaycastHit>(clickHits, RayCastTools.SortRaycastHit);
					clickHits = ClearHitList(clickHits);

					//If the player has clicked this frame. Sort clickHits and roll the last ones it if the player have re-clicked on the same spot
					if (leftEventClick != null && !leftEventClick.isDoubleClick)
					{
						if (layer.layers.Length > 1)
						{
							Debug.DrawRay(clickRay.origin, clickRay.direction * 100f, Color.green, 20f);
						}
						else
						{
							Debug.DrawRay(clickRay.origin, clickRay.direction * 100f, Color.red, 2f);
						}

						if (layer.SortedRayCastSimilar(clickHits))
						{
							layer.HitClickRoll();
							layer._lastSortedHits = clickHits;
						}
						else
						{
							layer._lastSortedHits = clickHits;
							layer._hitsClickOrder = new List<RaycastHit>(clickHits);
						}
						layer.lastClickPosition = mousePos;
					}

					if (clickHits != null && clickHits.Length > 0)
					{

						//the raycast has hit, we are sending the information to all listeners in order, and if one returns true, 
						//then it means it prevents other scripts from getting the raycast ; it consumes it.
						//listeners of the same priority can't block each other.

						//If the player has made a simple click this frame. sent event to listener
						if (!leftEventClick.isDoubleClick && layer._hitsClickOrder != null && layer._hitsClickOrder.Count > 0)
						{
							int clickBlocker = -1; //blocker will be set to the priority of the first callback which returned true
												   //If an object trigger no blocker, we continue with the next one in the _hitsClickOrder list.
							for (int i = 0; i < layer._hitsClickOrder.Count; ++i)
							{

								for (int j = 0; j < _listeners.Count; ++j)
								{
									if (clickBlocker != -1 && clickBlocker < _listeners[j].executeOrder)
									{
										//we're blocked by the previous set of callbacks
										i = clickHits.Length;
										break;
									}

									if (_listeners[j].listener.OnPrioritizedRaycast(layer.layers, layer._hitsClickOrder[i], PrioritizedRayCasterEventType.click, clickHits))
									{
										//this listener wants to block the callbacks
										clickBlocker = _listeners[j].executeOrder;
									}
								}
								if (clickBlocker != -1)
								{
									break;
								}
							}

							if (clickBlocker != -1)
							{
								break;
							}
						}
						//double click
						else if (leftEventClick.isDoubleClick)
						{
							int doubleClickBlocker = -1; //blocker will be set to the priority of the first callback which returned true
							for (int i = 0; i < _listeners.Count; ++i)
							{
								if (doubleClickBlocker != -1 && doubleClickBlocker < _listeners[i].executeOrder)
								{
									//we're blocked by the previous set of callbacks
									break;
								}

								if (_listeners[i].listener.OnPrioritizedRaycast(layer.layers, clickHits[0], PrioritizedRayCasterEventType.doubleClick, clickHits))
								{
									//this listener wants to block the callbacks
									doubleClickBlocker = _listeners[i].executeOrder;
								}
							}
						}
					}
					else //Clean hover list
					{
						layer.CallOnHoverEndAndUpdateLastHovered(null);
					}
				}

			}
			if (rightEventClick != null)
			{
				Ray clickRay = _camera.ScreenPointToRay(rightEventClick.position);
				//click and DoubleClick : we are going to raycast each layer individually in order of priority.
				foreach (PrioritizedLayer layer in _layers)
				{
					RaycastHit[] clickHits = Physics.RaycastAll(clickRay, _camera.farClipPlane, layer.GetLayerMask());
					System.Array.Sort<RaycastHit>(clickHits, RayCastTools.SortRaycastHit);
					clickHits = ClearHitList(clickHits);
					
					if (clickHits != null && clickHits.Length > 0)
					{
						//Handle right clicks
						//If the player has made a right click this frame. sent event to listener
						int rightClickBlocker = -1; //blocker will be set to the priority of the first callback which returned true
						for (int i = 0; i < _listeners.Count; ++i)
						{
							if (rightClickBlocker != -1 && rightClickBlocker < _listeners[i].executeOrder)
							{
								//we're blocked by the previous set of callbacks
								break;
							}

							if (_listeners[i].listener.OnPrioritizedRaycast(layer.layers, clickHits[0], PrioritizedRayCasterEventType.rightClick, clickHits))
							{
								//this listener wants to block the callbacks
								rightClickBlocker = _listeners[i].executeOrder;

							}
						}
					}
					else //Clean hover list
					{
						layer.CallOnHoverEndAndUpdateLastHovered(null);
					}
				}

			}
		}
		protected RaycastHit[] ClearHitList(RaycastHit[] rhs)
		{
			List<RaycastHit> newHits = new List<RaycastHit>();
			List<RaycastedCollidersContainer> rch = new List<RaycastedCollidersContainer>();
			for (int i = 0; i < rhs.Length; ++i)
			{
				RaycastedCollidersContainer newElem = rhs[i].transform.GetComponentInParent<RaycastedCollidersContainer>();
				if (newElem != null)
				{
					int alreadyExistingIndex = -1;

					for (int j = 0; j < rch.Count; ++j)
					{
						if (newElem  ==  rch[j])
						{
							alreadyExistingIndex = j;
							break;
						}
					}
					if (alreadyExistingIndex >= 0)
					{
						//we should choose one RayCastHit
						//not doing anything actually mean choosing the first object touched
					}
					else
					{
						rch.Add(newElem);
						newHits.Add(rhs[i]);
					}
				}
				else
				{
					rch.Add(null);
					newHits.Add(rhs[i]);
				}
			}
			return newHits.ToArray();
		}

		/// <summary>
		/// Debug function to get the string representation of a layer
		/// </summary>
		/// <param name="layer">The layer to get the string representation from</param>
		/// <returns>the string representation. ex : [0,1,2] </returns>
		private string PrintLayers(PrioritizedLayer layer)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder("[");
			for (int i = 0; i < layer.layers.Length; ++i)
			{
				sb.Append(layer.layers[i]);
				if (i < (layer.layers.Length - 1))
				{
					sb.Append(", ");
				}
			}
			sb.Append("]");
			return sb.ToString();
		}

#if UNITY_EDITOR
		public void SimulateEventViewportPosition(Vector2 mouseViewportSpacePosition, params PrioritizedRayCasterEventType[] eventTypes)
		{
			SimulateEvent(_camera.ViewportToScreenPoint(mouseViewportSpacePosition), Time.time, eventTypes);
		}
		public void SimulateEvent(Vector2 mouseScreenSpacePosition, params PrioritizedRayCasterEventType[] eventTypes)
		{
			SimulateEvent(mouseScreenSpacePosition, Time.time, eventTypes);
		}

		public void SimulateEvent(Vector2 mouseScreenSpacePosition, float time, params PrioritizedRayCasterEventType[] eventTypes )
		{
			ClickData[] clicks = new ClickData[2] {null,null};
			bool buttonDown0 = false;
			bool buttonUp0 = false;
			for (int i = 0; i < eventTypes.Length; ++i)
			{
				switch (eventTypes[i])
				{
					case PrioritizedRayCasterEventType.None:
						break;
					case PrioritizedRayCasterEventType.mouseHoverEnded:
						break;
					case PrioritizedRayCasterEventType.mouseHover:
						break;
					case PrioritizedRayCasterEventType.clickDown:
						buttonDown0 = true;
						break;
					case PrioritizedRayCasterEventType.clickUp:
						buttonUp0 = true;
						break;
					case PrioritizedRayCasterEventType.click:
						clicks[0] = new ClickData(false, mouseScreenSpacePosition, time);
						break;
					case PrioritizedRayCasterEventType.doubleClick:
						clicks[0] = new ClickData(true, mouseScreenSpacePosition, time);
						break;
					case PrioritizedRayCasterEventType.rightClick:
						clicks[1] = new ClickData(false, mouseScreenSpacePosition, time);
						break;
					default:
						break;
				}
			}
			
			RayCastMouse(clicks[0], clicks[1], buttonDown0, buttonUp0, mouseScreenSpacePosition);
		}
#endif
	}
}