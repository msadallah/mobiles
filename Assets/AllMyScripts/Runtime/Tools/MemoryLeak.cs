#define DEBUG_LEAK
namespace AllMyScripts.Common.Tools
{
    using UnityEngine;
    using System;
    using System.Reflection;
    using System.Collections.Generic;

    public static class MemoryLeak
	{
		public static WeakReference s_reference { get; private set; }
		public static Dictionary<WeakReference,string> s_refDic = null;

		/* --------- Check object ------------*/

		public static void CheckObject( object objToCheck )
		{
#if DEBUG_LEAK
			s_reference = new WeakReference( objToCheck );
#endif
		}

		public static void IsItDead()
		{
#if DEBUG_LEAK
			if( s_reference != null )
			{
				Resources.UnloadUnusedAssets();
				GC.Collect();
				GC.WaitForPendingFinalizers();
				if( s_reference.IsAlive )
					Debug.Log( "[LEAK] Still here" );
				else
					Debug.Log( "[LEAK] I 'm dead" );
			}
#endif
		}

		/* --------- Check class ------------*/

		public static void CheckClass( object classToCheck, bool bCheckMembers = false, Type typeToCheck = null )
		{
#if DEBUG_LEAK
			//lwTools.Assert( classToCheck!=null, "classToCheck is null!" );

			if( s_refDic == null )
				s_refDic = new Dictionary<WeakReference, string>();
			//else
			//	s_refDic.Clear();
			FieldInfo[] myFieldInfo;

			// Get the type and fields of classToCheck.
			Type myType = classToCheck.GetType();

			if( typeToCheck == null )
				s_refDic.Add( new WeakReference( classToCheck ), "class " + myType );

			if( !bCheckMembers ) return;

			myFieldInfo = myType.GetFields( BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public );
			//Debug.Log( "The fields of " + classToCheck );

			// Display the field information of classToCheck.
			for( int i = 0; i < myFieldInfo.Length; ++i )
			{
				FieldInfo info = myFieldInfo[i];
				object obj = info.GetValue( classToCheck );
				//Debug.Log( "Value for " + info.Name + " : " + obj );
				if( obj != null && (typeToCheck == null || obj is Array || obj.GetType().IsSubclassOf( typeToCheck ) ) )
				{
					if( typeToCheck == null || obj.GetType().IsSubclassOf( typeToCheck ) )
						s_refDic.Add( new WeakReference( obj ), myType + "." + info.Name );
					if( obj is Array )
					{
						Array array = (Array)obj;
						int nCount = array.Length;
						for( int j = 0; j < nCount; ++j )
						{
							obj = array.GetValue( j );
							if( obj != null && (typeToCheck == null || obj.GetType().IsSubclassOf( typeToCheck ) ) )
								s_refDic.Add( new WeakReference( obj ), myType + "." + info.Name + "[" + j + "]" );
						}
					}
				}
			}
#endif
		}

		public static void DetectLeaksInClass()
		{
#if DEBUG_LEAK
			if( s_refDic != null )
			{
				Debug.Log( "[LEAK] ------------------- Leaks detection -------------------" );

				WeakReference weakRef;
				string sName;

				Resources.UnloadUnusedAssets();
				GC.Collect();
				GC.WaitForPendingFinalizers();

				foreach( var pair in s_refDic )
				{
					weakRef = pair.Key;
					sName = pair.Value;
					if( weakRef != null && weakRef.IsAlive && weakRef.Target != null )
					{
						object target = weakRef.Target;
						Type type = target.GetType();
						if( type == typeof( string ) || type == typeof( Canvas ) )
							continue;
						int nInstanceId = (target is UnityEngine.Object) ? (target as UnityEngine.Object).GetInstanceID() : -1;
						Debug.LogWarning( "[LEAK] \t- Object " + sName + " / " + target + " (" + type + ") still here! - Unity instance: " + nInstanceId );
					}
				}
			}
#endif
		}

		public static void ClearClassDic()
		{
#if DEBUG_LEAK
			Debug.Log( "[LEAK] ------------------- ClearClassDic -------------------" );

			if( s_refDic != null )
				s_refDic.Clear();
#endif
		}
	}
}
