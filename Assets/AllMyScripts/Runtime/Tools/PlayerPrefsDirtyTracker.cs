﻿namespace  AllMyScripts.Common.Tools
{
	using UnityEngine;
	using AllMyScripts.Common.Tools;
	public class PlayerPrefsDirtyTracker : SingletonMonoBehaviour<PlayerPrefsDirtyTracker>
	{
		private bool _saveRequested = false;

		/// <summary>
		/// Request a PlayerPrefs.Save() at LateUpdate() or OnDestroy()
		/// </summary>
		public void SetDirtyPlayerPrefs()
		{
			_saveRequested = true;
		}
		private void LateUpdate()
		{
			SaveIfNeeded();
		}
		private void OnDestroy()
		{
			SaveIfNeeded();
		}
		/// <summary>
		/// Inner save PlayerPrefs if dirty
		/// </summary>
		private void SaveIfNeeded()
		{
			if (_saveRequested)
			{
				PlayerPrefs.Save();
				_saveRequested = false;
			}
		}
	}
	public static class PlayerPrefsExt
	{
		/// <summary>
		/// Shortcut for PlayerPrefsDirtyTracker.instance.SetDirtyPlayerPrefs()
		/// [Request a PlayerPrefs.Save() at LateUpdate() or OnDestroy()]
		/// </summary>
		public static void SetDirty()
		{
			PlayerPrefsDirtyTracker.instance.SetDirtyPlayerPrefs();
		}
	}

}
