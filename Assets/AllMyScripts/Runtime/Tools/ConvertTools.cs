

namespace AllMyScripts.Common.Tools
{

    using System;
    using System.Globalization;

    using UnityEngine;
    /// <summary>
    /// Static helper functions to convert advanced types (such as objcts) to primary types
    /// </summary>
    public sealed class ConvertTools
	{
		public static short ToInt16(object oValue, short nDefault = 0)
		{
			if (oValue != null)
			{
				try
				{
					return Convert.ToInt16(oValue);
				}
				catch (FormatException)
				{
					Debug.Log("oValue : " + oValue);
					return Convert.ToInt16(oValue, CultureInfo.InvariantCulture);
				}
			}
			else
				return nDefault;
		}

		public static int ToInt32( object oValue, int nDefault = 0 )
		{
			if (oValue != null)
			{
				try
				{
					return Convert.ToInt32( oValue );
				}
				catch (FormatException)
				{
					Debug.Log("oValue : " + oValue);
					return Convert.ToInt32(oValue, CultureInfo.InvariantCulture);
				}
			}
			else
				return nDefault;
		}
		public static long ToLong(object oValue, long nDefault = 0)
		{
			if (oValue != null)
			{
				try
				{
					return Convert.ToInt64(oValue);
				}
				catch (FormatException)
				{
					return Convert.ToInt64(oValue, CultureInfo.InvariantCulture);
				}
			}
			else
				return nDefault;
		}

		public static uint ToUInt32( object oValue, uint nDefault = 0 )
		{
			if( oValue != null )
			{
				try
				{
					return Convert.ToUInt32(oValue);
				}
				catch (FormatException)
				{
					return Convert.ToUInt32(oValue, CultureInfo.InvariantCulture);
				}
			}
			else
				return nDefault;
		}

		public static float ToFloat( object oValue, float fDefault = 0.0f )
		{
			if (oValue != null)
			{
				try
				{
					return Convert.ToSingle(oValue);
				}
				catch (FormatException)
				{
					return Convert.ToSingle(oValue, CultureInfo.InvariantCulture);
				}
			}
			else
				return fDefault;
		}

		public static double ToDouble( object oValue, double fDefault = 0.0 )
		{
			if (oValue != null)
			{
				try
				{
					return Convert.ToDouble(oValue);
				}
				catch (FormatException)
				{
					return Convert.ToDouble(oValue, CultureInfo.InvariantCulture);
				}
			}
			return fDefault;
		}

		public static bool ToBool( object oValue, bool bDefault = false )
		{
			if (oValue != null)
			{
				try
				{
					return Convert.ToBoolean(oValue);
				}
				catch (FormatException)
				{
					return Convert.ToBoolean(oValue, CultureInfo.InvariantCulture);
				}
				catch
				{
					try
					{
						return ToInt32(oValue) != 0;
					}
					catch { }
				}
			}
			return bDefault;
		}

		public static char ToChar( object oValue, char cDefault = default ( char ) )
		{
			if( oValue != null )
			{
				try
				{
					return Convert.ToChar( oValue );
				}
				catch {}
			}
			return cDefault;
		}

		public static string ToString( object oValue, string sDefault = null )
		{
			if( oValue != null )
				return oValue.ToString();
			else
				return sDefault;
		}

		public static DateTime ToDateTime( object oValue )
		{
			try
			{
				return Convert.ToDateTime(oValue);
			}
			catch (FormatException)
			{
				return Convert.ToDateTime(oValue, CultureInfo.InvariantCulture);
			}
		}

		/// <summary>
		/// Return the day of week in legit normal french format : Monday = 0, Tuesday = 1 ... Sunday = 6
		/// </summary>
		public static int ToMondayIdx( DayOfWeek dayOfWeek )
		{
			// DayOfWeek start idx at Sunday
			return ((int)dayOfWeek + 6) % 7;
		}
	}
}
