﻿namespace AllMyScripts.Common.Tools
{
    using UnityEngine;
    using UnityEngine.Networking;

    public static class SendMailTool
    {
        public static void SendEmail(string email, string subject, string body)
        {
            string sSubject = MyEscapeURL(subject);
            string sBody = MyEscapeURL(body);
            string sURL = "mailto:" + email + "?subject=" + sSubject + "&body=" + sBody;
            if (sURL.Length > 32700) sURL = sURL.Substring(0, 32700) + "...";
            Application.OpenURL(sURL);
        }

        private static string MyEscapeURL(string url)
        {
            return UnityWebRequest.EscapeURL(url).Replace("+", "%20");
        }
    }
}