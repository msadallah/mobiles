namespace AllMyScripts.Common.Tools
{
#if UNITY_EDITOR
    using UnityEngine;
#endif
    using System.Globalization;

    using System;
    public sealed class ParseTools
	{
		public static NumberStyles floatingPointStyles = NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite | NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowExponent;
		
		// Parse a float from a string
		// Returns a default value if it failed
		public static float ParseFloatSafe( string sValue, float fDefault=0f, bool bUseCulture=false )
		{
			float fValueOut = fDefault;
			if ( !float.TryParse( sValue, floatingPointStyles, bUseCulture ? NumberFormatInfo.CurrentInfo : NumberFormatInfo.InvariantInfo, out fValueOut ) )
			{
				fValueOut = fDefault;
#if UNITY_EDITOR
				try
				{
					Debug.LogWarning("Can't parse " + sValue + " to float");
				}
				catch (ArgumentException ae) {
					Debug.LogWarning("Can't parse to float - WARNING value contains unparsable char - " + ae );
				}
	#endif
			}
			return fValueOut;
		}

		// Parse a int from a string
		// Returns a default value if it failed
		public static int ParseIntSafe( string sValue, int nDefault=0 )
		{
			int nValueOut = nDefault;
			if( !int.TryParse( sValue, NumberStyles.Integer, NumberFormatInfo.InvariantInfo, out nValueOut ) )
			{
				nValueOut = nDefault;
	#if UNITY_EDITOR
				Debug.LogWarning( "Can't parse " + sValue + " to int" );
	#endif
			}
			return nValueOut;
		}

		// Parse a uint from a string
		// Returns a default value if it failed
		public static uint ParseUIntSafe( string sValue, uint nDefault=0 )
		{
			uint nValueOut = nDefault;
			if( !uint.TryParse( sValue, NumberStyles.Integer, NumberFormatInfo.InvariantInfo, out nValueOut ) )
			{
				nValueOut = nDefault;
	#if UNITY_EDITOR
				Debug.LogWarning( "Can't parse " + sValue + " to uint" );
	#endif
			}
			return nValueOut;
		}

		// Parse a long from a string
		// Returns a default value if it failed
		public static long ParseLongSafe( string sValue, long nDefault=0 )
		{
			long nValueOut = nDefault;
			if( !long.TryParse( sValue, NumberStyles.Integer, NumberFormatInfo.InvariantInfo, out nValueOut ) )
			{
				nValueOut = nDefault;
	#if UNITY_EDITOR
				Debug.LogWarning( "Can't parse " + sValue + " to long" );
	#endif
			}
			return nValueOut;
		}

		// Parse a double from a string
		// Returns a default value if it failed
		public static double ParseDoubleSafe( string sValue, double fDefault=0 )
		{
			double fValueOut = fDefault;
			if( !double.TryParse( sValue, floatingPointStyles, NumberFormatInfo.InvariantInfo, out fValueOut ) )
			{
				fValueOut = fDefault;
	#if UNITY_EDITOR
				Debug.LogWarning( "Can't parse " + sValue + " to double" );
	#endif
			}
			return fValueOut;
		}

		// Parse a bool from a string
		// Returns a default value if it failed
		public static bool ParseBoolSafe( string sValue, bool bDefault=false )
		{
			bool bValueOut = bDefault;

			if( sValue == "0" )
				sValue = "false";
			else if( sValue == "1" )
				sValue = "true";
			
			if( !bool.TryParse( sValue, out bValueOut ) )
			{
				bValueOut = bDefault;
	#if UNITY_EDITOR
				Debug.LogWarning( "Can't parse " + sValue + " to bool using default value " + bValueOut );
	#endif
			}
			return bValueOut;
		}

		// Parse an enum from a string
		// Returns a default value if it failed
		public static T ParseEnumSafe<T>(string sEnum, T eDefault)
		{
			try
			{
				object oEnum = System.Enum.Parse(typeof(T), sEnum);
				return (T)oEnum;
			}
			catch
			{
				// ignored
			}
#if UNITY_EDITOR
			Debug.LogWarning("Can't parse " + sEnum + " to enum " + typeof(T));
#endif
			return eDefault;
		}

		// Parse an enum from a string
		// Returns a default value if it failed
		public static T ParseEnumSafeNoCase<T>(string sEnum, T eDefault)
		{
			sEnum = sEnum.ToLowerInvariant();
			try
			{
				foreach(var value in System.Enum.GetValues(typeof(T)))
				{
					if(value.ToString().ToLowerInvariant() == sEnum)
					{
						return (T)value;
					}
				}
				object oEnum = System.Enum.Parse(typeof(T), sEnum);
				return (T)oEnum;
			}
			catch
			{
				// ignored
			}
#if UNITY_EDITOR
			Debug.LogWarning("Can't parse " + sEnum + " to enum " + typeof(T));
#endif
			return eDefault;
		}

		//! @brief Try to parse an enumeration from a string value
		//!
		//! @param sEnum	value of the enumeration to parse as a string
		//! @param eResult	result value of the enumeration if the parsing succeeds, first value of the enumeration if the parsing fails
		//!
		//! @return true if the parsing succeeds, false otherwise
		public static bool TryParseEnum<T>( string sEnum, out T eResult ) where T : struct, System.IConvertible
		{
			try
			{
				object oEnum = System.Enum.Parse( typeof( T ), sEnum );
				eResult = ( T )oEnum;
				return true;
			}
			catch
			{
				// ignored
			}

			eResult = default( T );
			return false;
		}
	}
}
