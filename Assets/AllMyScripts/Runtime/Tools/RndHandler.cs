//-----------------------------------------------------------------------------
// Created: Pascal Dubois 17-01-2014
// from P:\Dev\Library\lwh\impl\Flash\src\lwSys\RndHandler.as
//-----------------------------------------------------------------------------

namespace AllMyScripts.Common.Tools
{
    using UnityEngine;

    public sealed class RndHandler
	{
		private static uint m_nSeed;

		public static void Init()
		{
			m_nSeed = (uint)( Random.value * 0x7FFFFFFF );
		}

		public static void RndSeed( uint nSeed )
		{
			m_nSeed = nSeed;
		}

		public static double Rnd()
		{
			m_nSeed = (uint)( ( (double)m_nSeed * 16807 ) % int.MaxValue );
			return ( (double)m_nSeed / (double)0x7FFFFFFF ) + 0.000000000233;
		}

		public static uint RndRange( uint nMin, uint nMax )
		{
			return (uint)( nMin + ( nMax - nMin ) * Rnd() );
		}

		public static int RndRange( int nMin, int nMax )
		{
			return (int)( nMin + ( nMax - nMin ) * Rnd() );
		}

		public static float RndRange( float fMin, float fMax )
		{
			return (float)( fMin + ( fMax - fMin ) * Rnd() );
		}

		//
	}
}
