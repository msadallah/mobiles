﻿namespace AllMyScripts.Common.Utils
{
	using UnityEngine;

	public class OnEnableOnDisableDebugLog : MonoBehaviour
	{
		private void OnEnable()
		{
			Debug.Log("[DEBUG TRIGGER] OnEnabled : " + name);
		}

		private void OnDisable()
		{
			Debug.Log("[DEBUG TRIGGER] OnDisabled : " + name);
		}
	}
}