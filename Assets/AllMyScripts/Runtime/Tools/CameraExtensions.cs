﻿namespace UnityEngine
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public static class CameraExtensions 
	{
		public static Vector3 WorldToCameraPoint(this Camera camera,Vector3 position)
		{
			position = camera.WorldToViewportPoint(position);
			position.Set(position.x * camera.scaledPixelWidth, position.y * camera.scaledPixelHeight, position.z);
			return position;
		}
	}
}