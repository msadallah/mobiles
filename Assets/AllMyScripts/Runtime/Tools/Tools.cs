namespace AllMyScripts.Common.Tools
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Text;
	using System.Text.RegularExpressions;
	using UnityEngine;
	using UnityEngine.Networking;
	public static class GlobalTools
	{
		public static readonly System.DateTime UnixEpoch = new System.DateTime( 1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc );

		public static byte[] LoadBytesFromURL(string sFullPath)
		{
			UnityWebRequest uwr = new UnityWebRequest(sFullPath);
			DownloadHandlerBuffer dH = new DownloadHandlerBuffer();
			uwr.downloadHandler = dH;

			uwr.SendWebRequest();
			while (!uwr.isDone)
				;
			if (string.IsNullOrEmpty(uwr.error))
				return uwr.downloadHandler.data;
			return null;
		}

		public static string LoadTextFromURL(string sFullPath)
		{
			UnityWebRequest uwr = new UnityWebRequest(sFullPath);
			DownloadHandlerBuffer dH = new DownloadHandlerBuffer();
			uwr.downloadHandler = dH;

			uwr.SendWebRequest();
			while (!uwr.isDone)
				;
			if (string.IsNullOrEmpty(uwr.error))
				return uwr.downloadHandler.text;
			return null;
		}

		public static byte[] LoadBinaryFileAtFullPath(string sFullPath)
		{
			if (File.Exists(sFullPath))
			{
				return File.ReadAllBytes(sFullPath);
			}
#if UNITY_ANDROID && !UNITY_EDITOR
			else
			{
				return LoadBytesFromURL( sFullPath );
			}
#else
			return null;
#endif
		}

		public static string LoadTextFileAtFullPath(string sFullPath)
		{
			string sText = null;
#if !UNITY_EDITOR && UNITY_ANDROID
			sText = LoadTextFromURL( sFullPath );
#elif UNITY_EDITOR
			if (File.Exists(sFullPath))
			{
				sText = File.ReadAllText(sFullPath);
			}
#endif
			return sText;
		}

		public static string LoadTextFile(string sFileName, string sExtension = "", string sSubPath = "")
		{
			if (string.IsNullOrEmpty(sFileName))
				return null;

			string sText = null;

			// Test if the file is present in the local directory (for testing)
			string sCompleteFileName = Application.dataPath + "/" + sSubPath + sFileName + '.' + sExtension;
			sText = LoadTextFileAtFullPath(sCompleteFileName);
			if (sText == null)
			{
				TextAsset textFile = null;
				int nStart = sFileName.LastIndexOf( '.' );
				if (nStart >= 0)
					sFileName = sFileName.Substring(0, nStart);
				textFile = Resources.Load(sFileName) as TextAsset;

				if (textFile == null)
					Debug.LogWarning("Can't open resource " + sFileName + " !");
				else
					sText = textFile.text;
			}

			if (!string.IsNullOrEmpty(sText) && sText[0].GetHashCode() == 0xFEFF)
				sText = sText.Substring(1);

			return sText;
		}

		public static string LoadAllTextFile(string sFolder, string sExtension)
		{
			if (sFolder == null || sExtension == null)
				return null;

			string sAllTxt = string.Empty;

			string sCompleteFileName = Application.dataPath 
	#if !UNITY_ANDROID
				+ "/.."
	#endif
				+ "/" + sFolder;

			if (Directory.Exists(sCompleteFileName))
			{
				string[] sAllFiles = Directory.GetFiles( sCompleteFileName, sExtension );
				if (sAllFiles.Length > 0)
				{
					foreach (string sFile in sAllFiles)
					{
						sAllTxt = sAllTxt + File.ReadAllText(sFile) + "\n";
					}
				}
			}

			if (string.IsNullOrEmpty(sAllTxt))
			{
				UnityEngine.Object[] sAllObjects = Resources.LoadAll( sFolder.TrimEnd( '*', '/' ), typeof( TextAsset ) );
				foreach (UnityEngine.Object ta in sAllObjects)
				{
					sAllTxt += (ta as TextAsset) + "\n";
				}
			}

			return sAllTxt;
		}

#if UNITY_EDITOR
		public static void SaveTextFile(string sFilePath, string sText)
		{
			File.WriteAllText(sFilePath, sText);
		}
#endif

		public static string GetCompleteFileName(string sFileName, string sExtension)
		{
#if UNITY_ANDROID && !UNITY_EDITOR
			return "jar:file://" + Application.dataPath + "!/assets/" + sFileName + sExtension;
#elif UNITY_IOS && !UNITY_EDITOR
			return Application.dataPath + "/Raw/" + sFileName + sExtension;
#elif UNITY_EDITOR
			return Application.dataPath + "/../External/" + sFileName + sExtension;
#else
			return Application.streamingAssetsPath + '/' + sFileName + sExtension;
#endif
		}

		public static byte[] LoadBinaryFile(string sFileName, string sExtension)
		{
			string sCompleteFileName = GetCompleteFileName( sFileName, sExtension );
#if UNITY_ANDROID && !UNITY_EDITOR
			byte[] bytes = LoadBytesFromURL( sCompleteFileName );
			if( bytes!=null ) return bytes;
#else
			if (File.Exists(sCompleteFileName))
			{
				return File.ReadAllBytes(sCompleteFileName);
			}
			Debug.LogWarning("Binary file not found : " + sCompleteFileName);
#endif
			return null;
		}

		public static void SaveEncryptedTextFile(string sPath, string sContent)
		{
			if (sPath == null)
				return;

			RC4 rc4 = new RC4();
			byte[] content = rc4.Encrypt( sContent );
			File.WriteAllBytes(sPath, content);
		}

		public static string LoadEncryptedTextFile(string sPath)
		{
			RC4 rc4 = new RC4();
			byte[] bBinary = LoadBinaryFile( sPath, ".enc" );
			if (bBinary == null)
			{
				Debug.LogWarning("Unable to load " + sPath + ".enc");
				return null;
			}
			string sTxt = rc4.EncryptToString( bBinary );

			if (!string.IsNullOrEmpty(sTxt) && sTxt[0].GetHashCode() == 0xFEFF)
			{
				sTxt = sTxt.Substring(1);
			}
			return sTxt;
		}

		public static bool ReadBool(string sValue, bool bDefault)
		{
			if (!string.IsNullOrEmpty(sValue))
			{
				switch (sValue.ToUpper())
				{
					case "FALSE":
					case "OFF":
					case "NO":
					case "0":
						return false;
					case "TRUE":
					case "ON":
					case "YES":
					case "1":
					case "-1":
						return true;
				}
			}
			return bDefault;
		}

		public static string TimeToString(uint nTime, bool bTrimHour = false, bool bSeconds = true)
		{
			StringBuilder sbTime = new StringBuilder();
			uint nMin = nTime / 60;
			uint nSec = nTime % 60;
			uint nHours = nMin / 60;

			nMin -= 60 * nHours;
			if (bTrimHour && nHours == 0)
			{
				if (bSeconds)
					sbTime = sbTime.AppendFormat("{0:D}:{1:D2}", nMin, nSec);
				else
					sbTime = sbTime.AppendFormat("{0:D}", nMin);
			}
			else
			{
				if (bSeconds)
					sbTime = sbTime.AppendFormat("{0:D}:{1:D2}:{2:D2}", nHours, nMin, nSec);
				else
					sbTime = sbTime.AppendFormat("{0:D}:{1:D2}", nHours, nMin);
			}
			return sbTime.ToString();
		}

		public static IEnumerable<string> GetFiles(string sPath, string sFilter = null, bool bFromResources = false)
		{
			Queue<string> queue = new Queue<string>();
			queue.Enqueue(sPath);
			while (queue.Count > 0)
			{
				sPath = queue.Dequeue();
				try
				{
					foreach (string sSubDir in Directory.GetDirectories(sPath))
					{
						queue.Enqueue(sSubDir);
					}
				}
#if UNITY_EDITOR
				catch (System.Exception ex) { Debug.Log(ex.ToString()); }
#else
				catch( System.Exception ) {}
#endif
				string[] sFiles = null;
				try
				{
					if (sFilter == null)
						sFiles = Directory.GetFiles(sPath);
					else
						sFiles = Directory.GetFiles(sPath, sFilter);
				}
#if UNITY_EDITOR
				catch (System.Exception ex) { Debug.Log(ex.ToString()); }
#else
				catch( System.Exception ) {}
#endif
				if (sFiles != null)
				{
					for (int i = 0; i < sFiles.Length; i++)
					{
						string sFile = sFiles[i];
						if (bFromResources)
						{
							if (!sFile.EndsWith(".meta"))
							{
								yield return (sFile.Replace("\\", "/").Substring(sFile.IndexOf("Resources") + 9));
							}
						}
						else
						{
							yield return sFile;
						}
					}
				}
			}

		}

		public static void DestroyObjectArray(UnityEngine.Object[] oArray)
		{
			for (int i = 0; i < oArray.Length; i++)
			{
				if (oArray[i] != null)
				{
					UnityEngine.Object.Destroy(oArray[i]);
					oArray[i] = null;
				}
			}
		}

		public static void DestroyGameObjectArray(GameObject[] goArray)
		{
			for (int i = 0; i < goArray.Length; i++)
			{
				if (goArray[i] != null)
				{
					GameObject.Destroy(goArray[i]);
					goArray[i] = null;
				}
			}
		}

		public static void DestroyGameObjectArray(MonoBehaviour[] mbArray)
		{
			for (int i = 0; i < mbArray.Length; i++)
			{
				if (mbArray[i] != null)
				{
					GameObject.Destroy(mbArray[i].gameObject);
					mbArray[i] = null;
				}
			}
		}

#if UNITY_WEBGL && !UNITY_EDITOR
		public static void Assert( bool bToEvaluate, string sMessage )
		{
			if( !bToEvaluate )
			{
				Debug.LogError( "ASSERT: "+sMessage );
			}
		}
	
		public static void AssertFormat( bool bToEvaluate, string sMessage, params object[] arguments )
		{
			if( !bToEvaluate )
			{
				Debug.LogError( "ASSERT: "+string.Format( sMessage, arguments ) );
			}
		}

		public static void Assert( bool bToEvaluate )
		{
			if( !bToEvaluate )
			{
				Debug.LogError( "ASSERT" );
			}
		}
#else
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void Assert(bool bToEvaluate, string sMessage)
		{
			if (!bToEvaluate)
			{
				throw new System.Exception("ASSERT: " + sMessage);
			}
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void AssertFormat(bool bToEvaluate, string sMessage, params object[] arguments)
		{
			if (!bToEvaluate)
			{
				throw new System.Exception("ASSERT: " + string.Format(sMessage, arguments));
			}
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void Assert(bool bToEvaluate)
		{
			Assert(bToEvaluate, string.Empty);
		}
#endif

		public static void SetLayerRecursively(GameObject rootObject, int nLayer)
		{
			if (rootObject == null)
				return;

			rootObject.layer = nLayer;

			foreach (Transform childTransform in rootObject.transform)
				SetLayerRecursively(childTransform.gameObject, nLayer);
		}

		public static void SetLossyScale(Transform tr, Vector3 vScale)
		{
			Vector3 v1 = tr.lossyScale;
			if (Mathf.Approximately(v1.x, 0) || Mathf.Approximately(v1.y, 0) || Mathf.Approximately(v1.z, 0))
			{
				tr.localScale = Vector3.zero;
			}
			else
			{
				float fX = tr.localScale.x * vScale.x / v1.x;
				float fY = tr.localScale.y * vScale.y / v1.y;
				float fZ = tr.localScale.z * vScale.z / v1.z;
				tr.localScale = new Vector3(fX, fY, fZ);
			}
		}

		/// <summary>
		/// Get real time in seconds (based on System.DateTime)
		/// </summary>
		public static uint GetTimeSinceUnixEpoch(bool bLocal = false)
		{
			System.DateTime dtNow = bLocal ? System.DateTime.Now : System.DateTime.UtcNow;
			return (uint)(dtNow - UnixEpoch).TotalSeconds;
		}

		public static DateTime GetDateTimeFromUnixTimestamp(long timestamp)
		{
			// Unix timestamp is seconds past epoch
			System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
			dtDateTime = dtDateTime.AddSeconds(timestamp).ToLocalTime();
			return dtDateTime;
		}

		/// <summary>
		/// Convert a time in seconds into days/hours/minutes/seconds
		/// </summary>
		public static void ConvertTime(uint nTimeInSeconds, out uint nDays, out uint nHours, out uint nMinutes, out uint nSeconds)
		{
			nDays = nTimeInSeconds / 86400;
			uint nHoursInSeconds = nTimeInSeconds - 86400*nDays;
			nHours = nHoursInSeconds / 3600;
			uint nMinutesInSeconds = nHoursInSeconds - 3600*nHours;
			nMinutes = nMinutesInSeconds / 60;
			nSeconds = nMinutesInSeconds - 60 * nMinutes;
		}

		public static void EnableRendererRecursively(Transform trParent, bool bEnabled)
		{
			if (trParent == null)
				return;

			Renderer rd = trParent.GetComponent<Renderer>();
			if (rd != null)
				rd.enabled = bEnabled;
			foreach (Transform trChild in trParent)
			{
				EnableRendererRecursively(trChild, bEnabled);
			}
		}

		// Get a component by its type, and add it when not found
		// Useful with Flash builds, because it doesn't automatically create required components
		public static T GetComponentSafe<T>(GameObject go) where T : Component
		{
			if (go == null)
				return null;

			T cpt = go.GetComponent<T>();
			if (cpt == null)
				cpt = go.AddComponent<T>();
			return cpt;
		}

		// Get the nearest power of two of a number
		public static int GetNearestPow2(int nNumber)
		{
			return Mathf.RoundToInt(Mathf.Pow(2f, Mathf.Ceil(Mathf.Log(nNumber) / Mathf.Log(2f))));
		}

		public static void SetParent(Transform child, Transform parent)
		{
			if (child == null || parent == null)
				return;
			child.SetParent(parent);
		}

		public static void SetParent(Transform child, Transform parent, Vector3 vPos, Vector3 vScale)
		{
			if (child == null || parent == null)
				return;
			SetParent(child, parent);
			child.localScale = vScale;
			child.localPosition = vPos;
		}


		public static void SetParent(RectTransform child, Transform parent, Vector2 vPos, Vector3 vScale)
		{
			if (child == null || parent == null)
				return;
			child.SetParent(parent);
			child.localScale = vScale;
			child.anchoredPosition = vPos;
		}

		public static void SetParent(RectTransform child, Transform parent, Vector3 vPos, Vector3 vScale)
		{
			if (child == null || parent == null)
				return;
			child.SetParent(parent);
			child.localScale = vScale;
			child.anchoredPosition3D = vPos;
		}

		public static void ReplaceGameObject(GameObject instance, Transform destination)
		{
			instance.transform.SetParent(destination.parent);
			instance.SetActive(destination.gameObject.activeSelf);
			instance.transform.localPosition = destination.localPosition;
			instance.transform.localScale = destination.localScale;
			instance.transform.localRotation = destination.localRotation;

			instance.transform.SetSiblingIndex(destination.GetSiblingIndex());

			RectTransform instanceRectTransform = instance.GetComponent<RectTransform>();
			RectTransform destinationRectTransform = destination.GetComponent<RectTransform>();
			if (instanceRectTransform != null && destinationRectTransform != null)
			{
				instanceRectTransform.anchoredPosition = destinationRectTransform.anchoredPosition;
				instanceRectTransform.sizeDelta = destinationRectTransform.sizeDelta;
				instanceRectTransform.anchorMin = destinationRectTransform.anchorMin;
				instanceRectTransform.anchorMax = destinationRectTransform.anchorMax;
				instanceRectTransform.pivot = destinationRectTransform.pivot;
			}

			GameObject.Destroy(destination.gameObject);
		}

#if UNITY_EDITOR
		public static string GetCommandLineArg(string sParam, string sDefault = "")
		{
			sParam = "-" + sParam + "=";
			foreach (string sArg in System.Environment.GetCommandLineArgs())
			{
				if (sArg.ToUpper().StartsWith(sParam))
				{
					return sArg.Substring(sParam.Length);
				}
			}
			return sDefault;
		}

		public static bool CommandLineArgExist(string sParam)
		{
			sParam = "-" + sParam;
			string[] commandLineArray = System.Environment.GetCommandLineArgs();
			for (int i = 0; i < commandLineArray.Length; ++i)
			{
				if (commandLineArray[i].Equals(sParam, System.StringComparison.OrdinalIgnoreCase))
					return true;
			}
			return false;
		}
#endif

		public static void EnableParticleSystemEmission(ParticleSystem ps, bool bEnable)
		{
			if (ps == null)
				return;

			ParticleSystem.EmissionModule emission = ps.emission;
			emission.enabled = bEnable;
		}

		public static float GetDPI()
		{
#if UNITY_ANDROID && !UNITY_EDITOR
			AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");

			AndroidJavaObject metrics = new AndroidJavaObject("android.util.DisplayMetrics");
			activity.Call<AndroidJavaObject>("getWindowManager").Call<AndroidJavaObject>("getDefaultDisplay").Call("getMetrics", metrics);

			return (metrics.Get<float>("xdpi") + metrics.Get<float>("ydpi")) * 0.5f;
#else
			return Screen.dpi;
#endif
		}


		/// <summary>
		/// Given a person's first and last name, we'll make our best guess to extract up to two initials, hopefully
		/// representing their first and last name, skipping any middle initials, Jr/Sr/III suffixes, etc. The letters 
		/// will be returned together in ALL CAPS, e.g. "TW". 
		/// </summary>
		/// <returns>One to two uppercase initials, without punctuation.</returns>
		public static string GetInitials(this string name)
		{
			// first remove all: punctuation, separator chars, control chars, and numbers (unicode style regexes)
			string initials = Regex.Replace(name, @"[\p{P}\p{S}\p{C}\p{N}]+", "");

			// Replacing all possible whitespace/separator characters (unicode style), with a single, regular ascii space.
			initials = Regex.Replace(initials, @"\p{Z}+", " ");

			// Remove all Sr, Jr, I, II, III, IV, V, VI, VII, VIII, IX at the end of names
			initials = Regex.Replace(initials.Trim(), @"\s+(?:[JS]R|I{1,3}|I[VX]|VI{0,3})$", "", RegexOptions.IgnoreCase);

			// Extract up to 2 initials from the remaining cleaned name.
			initials = Regex.Replace(initials, @"^(\p{L})[^\s]*(?:\s+(?:\p{L}+\s+(?=\p{L}))?(?:(\p{L})\p{L}*)?)?$", "$1$2").Trim();

			if (initials.Length > 2)
			{
				// Worst case scenario, everything failed, just grab the first two letters of what we have left.
				initials = initials.Substring(0, 2);
			}

			return initials.ToUpperInvariant();
		}

		/// <summary>
		/// Find approximately ratio that correspond at value in curve
		/// </summary>
		/// <param name="curve"></param>
		/// <param name="fMin"></param>
		/// <param name="fMax"></param>
		/// <param name="fValue"></param>
		/// <param name="nIterationMax"></param>
		/// <returns></returns>
		public static float ComputeRatioFromCurvedValues(AnimationCurve curve, float fMin, float fMax, float fValue, int nIterationMax = 10)
		{
			if (curve != null)
			{
				if (Mathf.Approximately(fValue, fMin) || fValue < fMin)
					return 0f;
				if (Mathf.Approximately(fValue, fMax) || fValue > fMax)
					return 1f;

				int nIter = 0;
				float fRatioMin = 0f;
				float fRatioMax = 1f;
				float fRatio = 0.5f;
				while (nIter < nIterationMax)
				{
					float fResult = Mathf.Lerp( fMin, fMax, curve.Evaluate( fRatio ) );
					float fDiff =  fResult - fValue;
					if (Mathf.Abs(fDiff) <= Mathf.Epsilon)
						return fRatio;
					if (fDiff < 0f)
						fRatioMin = fRatio;
					else
						fRatioMax = fRatio;
					fRatio = (fRatioMin + fRatioMax) * 0.5f;
					nIter++;
				}
				return fRatio;
			}
			return Mathf.InverseLerp(fMin, fMax, fValue);
		}

		/// <summary>
		/// Get the value on a curve defines between 0 and 1 and multiply it by your new interval max limit to get its value into this new interval.
		/// </summary>
		/// <param name="curve">AnimationCurve to evaluate</param>
		/// <param name="newMaxInterval">New max interval limit</param>
		/// <param name="valueOnCurve">Value to pick on the AnimationCurve</param>
		/// <returns>if your curve is defined between 0 and 1 return the value into the new interval, -1 otherwise</returns>
		public static int Convert0To1CurveValueIntoNewInterval(AnimationCurve curve, int newMaxInterval, float valueOnCurve)
		{
			if (Mathf.Approximately(0, curve.keys[0].time) && Mathf.Approximately(1, curve.keys[curve.keys.Length - 1].time))
			{
				int res = 0;
				res = Mathf.FloorToInt(curve.Evaluate(valueOnCurve) * newMaxInterval);
				return res;
			}
			else
			{
				Debug.LogWarning("Your curve isn't defined between 0 and 1");
				return -1;
			}
		}

		/// <summary>
		/// Verify if two float? are similars
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool AreSimilars(float? a, float? b)
		{
			if (a == b)
				return true;
			if (a == null || b == null)
				return false;
			return Mathf.Approximately(a.Value, b.Value);
		}

		/// <summary>
		/// Verify if two array of string are similars
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool AreSimilars(string[] a, string[] b)
		{
			if (a == b)
				return true;
			if (a == null || b == null)
				return false;
			int count = a.Length;
			if (count != b.Length)
				return false;
			for (int i = 0; i < count; ++i)
			{
				bool found = false;
				for (int j = 0; j < count; ++j)
				{
					if (b[j] == a[i])
					{
						found = true;
						break;
					}
				}
				if (!found)
					return false;

			}
			return true;
		}

		/// <summary>
		/// Convert the float in entry to a string with a certain digit number. 
		/// Above 100 it will be rounded to an integer value
		/// </summary>
		/// <param name="value">float number to convert to string</param>
		/// <param name="digitNbRoundedPart">number of digit allowed after coma</param>
		/// <returns></returns>
		public static string ConvertValueToString(float value, int digitNbRoundedPart)
		{
			if (value > 100)
				return Mathf.RoundToInt(value).ToString();
			else
				return value.ToString("F" + digitNbRoundedPart);
		}
	}
}
