﻿namespace AllMyScripts.Common.Tools
{
	using UnityEngine;
	using System.IO;
	using System.Text;

	public class ObjExporter
	{
		/// <summary>
		/// Generate an obj file formated in string from one MeshFilter
		/// </summary>
		public static string MeshToString(MeshFilter mf)
		{
			Mesh m = mf.mesh;
			Material[] mats = mf.GetComponent<MeshRenderer>().sharedMaterials;

			StringBuilder sb = new StringBuilder();

			sb.Append("g ").Append(mf.name).Append("\n");

			foreach (Vector3 v in m.vertices)
				sb.Append(string.Format("v {0} {1} {2}\n", v.x, v.y, v.z));

			sb.Append("\n");

			foreach (Vector3 v in m.normals)
				sb.Append(string.Format("vn {0} {1} {2}\n", v.x, v.y, v.z));

			sb.Append("\n");

			foreach (Vector3 v in m.uv)
				sb.Append(string.Format("vt {0} {1}\n", v.x, v.y));

			for (int material = 0; material < m.subMeshCount; material++)
			{
				sb.Append("\n");
				sb.Append("usemtl ").Append(mats[material].name).Append("\n");
				sb.Append("usemap ").Append(mats[material].name).Append("\n");

				int[] triangles = m.GetTriangles(material);
				for (int i = 0; i < triangles.Length; i += 3)
				{
					sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n",
						triangles[i] + 1, triangles[i + 1] + 1, triangles[i + 2] + 1));
				}
			}

			return sb.ToString();
		}

		/// <summary>
		/// Generate an obj file formated in string from multiple MeshFilter
		/// </summary>
		public static string MeshToString(MeshFilter[] mfs)
		{
			string s = string.Empty;

			foreach (MeshFilter mf in mfs)
				s += MeshToString(mf);

			return s;
		}

		/// <summary>
		/// Create an .obj file at path with one MeshFilter
		/// </summary>
		public static void MeshToFile(MeshFilter mf, string filePath)
		{
			using (StreamWriter sw = new StreamWriter(filePath))
			{
				sw.Write(MeshToString(mf));
			}
		}

		/// <summary>
		/// Create an .obj file at path with multiple MeshFilters
		/// </summary>
		public static void MeshToFile(MeshFilter[] mfs, string filePath)
		{
			using (StreamWriter sw = new StreamWriter(filePath))
			{
				sw.Write(MeshToString(mfs));
			}
		}

		public static byte[] MeshToFileByteArray(MeshFilter mf)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				using (StreamWriter sw = new StreamWriter(ms))
				{
					sw.Write(MeshToString(mf));
				}

				return ms.ToArray();
			}
		}

		public static byte[] MeshToFileByteArray(MeshFilter[] mfs)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				using (StreamWriter sw = new StreamWriter(ms))
				{
					sw.Write(MeshToString(mfs));
				}

				return ms.ToArray();
			}
		}
	}
}