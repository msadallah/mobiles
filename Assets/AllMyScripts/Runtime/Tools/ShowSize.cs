namespace AllMyScripts.Common.Tools
{
    using UnityEngine;

    public sealed class ShowSize : MonoBehaviour
	{
		public Rect m_rectGui = new Rect( Screen.width - 150f, 5f, 150f, 20f );
		public GUISkin m_skin;
	
		private string m_sizeLabel;
	
		void Start()
		{
			m_sizeLabel = Screen.width + "x" + Screen.height + " - DPI " + GlobalTools.GetDPI();
		}
	
		void OnGUI()
		{
			if( m_skin != null )
				GUI.skin = m_skin;
			GUIStyle style = GUI.skin.box;
			GUI.Label( m_rectGui, m_sizeLabel, style );
		}
	}
}
