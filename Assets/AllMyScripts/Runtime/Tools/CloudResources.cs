﻿//#define DEBUG_CLOUD_RESOURCES

namespace AllMyScripts.Common.Tools
{
	using System;
	using System.Collections;
	using System.IO;
	using UnityEngine;
	using UnityEngine.Networking;

	public class CloudResources : MonoBehaviour
	{
		static public IEnumerator LoadResourceFromCloud(string urlBase, string filename, Action<string> result)
		{
			string url = $"{urlBase}/{filename}.json";
			string keyDate = "fileOnCloudDate" + filename;
			string keyContent = "fileOnCloudContent" + filename;
			UnityWebRequest www = new UnityWebRequest(url, "GET");
			www.downloadHandler = new DownloadHandlerBuffer();
			yield return www.SendWebRequest();

			string json = null;
			bool hasNewCloudVersion = false;
			long netTimestamp = 0;
			long localTimestamp = 0;

			string localDate = PlayerPrefs.GetString(keyDate);
			if (!string.IsNullOrEmpty(localDate))
			{
				localTimestamp = long.Parse(localDate);
			}

			string buildDate = Common.Version.Version.GetDate();
			DateTime buildTime = DateTime.Parse(buildDate);
			long buildTimestamp = UtilsDateFormat.GetTimestampFromUnixEpoch(buildTime);

			if (www.result == UnityWebRequest.Result.Success)
			{
				string time = www.GetResponseHeader("Last-Modified");
				if (string.IsNullOrEmpty(time))
				{
					time = www.GetResponseHeader("date");
				}
				if (!string.IsNullOrEmpty(time))
				{
					DateTime netTime = DateTime.Parse(time);
					netTimestamp = UtilsDateFormat.GetTimestampFromUnixEpoch(netTime);
					hasNewCloudVersion = netTimestamp > localTimestamp && netTimestamp > buildTimestamp;
				}				
			}

			if (hasNewCloudVersion)
			{
				LogCloudResources($"[CLOUD_RESOURCES] Server have new version for {filename}.json");
				using (MemoryStream ms = new MemoryStream(www.downloadHandler.data))
				{
					try
					{
						json = new StreamReader(ms).ReadToEnd();
					}
					catch (Exception e)
					{
						Debug.LogError("Get text data from cloud failed! " + e);
						// get local data
						hasNewCloudVersion = false;
					}
					finally
					{
					}
				}
			}

			if (!hasNewCloudVersion)
			{
				if (localTimestamp > buildTimestamp)
				{
					LogCloudResources($"[CLOUD_RESOURCES] Get local version for {filename}.json");
					json = PlayerPrefs.GetString(keyContent);
				}
				else
				{
					LogCloudResources($"[CLOUD_RESOURCES] Keep build version for {filename}.json");
				}
			}

			if (!string.IsNullOrEmpty(json))
			{
				LogCloudResources($"[CLOUD_RESOURCES] LoadResourceFromCloud for {filename}.json:\n{json}");

				if (hasNewCloudVersion)
				{
					LogCloudResources($"[CLOUD_RESOURCES] Save local version for {filename}.json");
					PlayerPrefs.SetString(keyDate, netTimestamp.ToString());
					PlayerPrefs.SetString(keyContent, json);
				}
				if (result != null)
					result(json);
			}
		}

		static public IEnumerator SaveResourceOnCloud(string urlBase, string filename, string filepath)
		{
			string url = $"{urlBase}/{filename}.json";
			UnityWebRequest www = new UnityWebRequest(url, "PUT");
			www.uploadHandler = new UploadHandlerFile(filepath);
			www.SendWebRequest();
			while (www.isDone == false)
				yield return null;
			if (www.result != UnityWebRequest.Result.Success)
				Debug.LogError("SaveResourceOnCloud error " + www.error);
		}

		[System.Diagnostics.Conditional("DEBUG_CLOUD_RESOURCES")]
		private static void LogCloudResources(string log)
		{
			Debug.Log(log);
		}
	}
}
