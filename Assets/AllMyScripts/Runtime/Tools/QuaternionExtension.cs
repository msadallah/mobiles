﻿namespace UnityEngine
{
    /// <summary>
    /// Extension for the Quaternion Struct
    /// </summary>
    public static class QuaternionExtension
    {
        public static float[] ToArray(this Quaternion q)
        {
            return new float[] { q.x, q.y, q.z, q.w };
        }
    }
}