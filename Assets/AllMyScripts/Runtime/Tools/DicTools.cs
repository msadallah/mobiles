
namespace AllMyScripts.Common.Tools
{
    using System.Collections.Generic;
	using AllMyScripts.LangManager;
	using UnityEngine;

    /// <summary>
    /// Static helper functions to retrieve typed values from Dictionary<string,object> variables
    /// Useful when working with data serialized with JSON
    /// </summary>

    public static class DicTools
	{
		static public object GetValue( Dictionary<string,object> dic, string sKey )
		{
			object oValue = null;
			if( dic!=null ) dic.TryGetValue( sKey, out oValue );
			return oValue;
		}

		static public short GetValueInt16(Dictionary<string, object> dic, string sKey, short nDefault = 0)
		{
			return ConvertTools.ToInt16(GetValue(dic, sKey), nDefault);
		}
		
		static public int GetValueInt32(Dictionary<string, object> dic, string sKey, int nDefault = 0)
		{
			return ConvertTools.ToInt32(GetValue(dic, sKey), nDefault);
		}

		static public long GetValueLong(Dictionary<string, object> dic, string sKey, long nDefault = 0)
		{
			return ConvertTools.ToLong(GetValue(dic, sKey), nDefault);
		}

		static public uint GetValueUInt32( Dictionary<string,object> dic, string sKey, uint nDefault=0 )
		{
			return ConvertTools.ToUInt32( GetValue( dic, sKey ), nDefault );
		}

		static public float GetValueFloat( Dictionary<string,object> dic, string sKey, float fDefault=0.0f )
		{
			return ConvertTools.ToFloat( GetValue( dic, sKey ), fDefault );
		}

		static public double GetValueDouble( Dictionary<string,object> dic, string sKey, double fDefault=0.0 )
		{
			return ConvertTools.ToDouble( GetValue( dic, sKey ), fDefault );
		}

		static public bool GetValueBool( Dictionary<string,object> dic, string sKey, bool bDefault=false )
		{
			return ConvertTools.ToBool( GetValue( dic, sKey ), bDefault );
		}

		static public char GetValueChar( Dictionary<string,object> dic, string sKey, char cDefault = default( char ) )
		{
			return ConvertTools.ToChar( GetValue( dic, sKey ), cDefault );
		}

		static public string GetValueString( Dictionary<string,object> dic, string sKey, string sDefault=null )
		{
			return ConvertTools.ToString( GetValue( dic, sKey ), sDefault );
		}
		
		static public T GetValueStringAsEnum<T>(Dictionary<string, object> dic, string sKey, T eDefault) where T : struct, System.IConvertible
		{
			return GetValueStringAsEnum<T>(GetValueString(dic, sKey), eDefault);
		}

		static private T GetValueStringAsEnum<T>(string value, T eDefault) where T : struct, System.IConvertible
		{
			T enumValue = eDefault;
			if (!string.IsNullOrEmpty(value))
			{
				bool success = ParseTools.TryParseEnum<T>(value, out enumValue);
				if (!success)
				{
					enumValue = eDefault;
				}
			}
			return enumValue;
		}
		static public string GetValueStringAsEnum<T>(Dictionary<string, object> dic, string sKey, string sDefault = null)
		{
			return ConvertTools.ToString(GetValue(dic, sKey), sDefault);
		}

		static public List<object> GetValueList( Dictionary<string,object> dic, string sKey )
		{
			return GetValue( dic, sKey ) as List<object>;
		}
	
		static public Dictionary<string,object> GetValueDictionary( Dictionary<string,object> dic, string sKey )
		{
			return GetValue( dic, sKey ) as Dictionary<string,object>;
		}

		static public int[] GetValueListAsIntArray( Dictionary<string,object> dic, string sKey )
		{
			return GetValueListAsIntArray( GetValueList( dic, sKey ) ) ;
		}
	
		static public int[] GetValueListAsIntArray( List<object> list )
		{
			if( list==null ) return null;
			int nCount = list.Count;
			int[] nArray = new int[nCount];
			for( int i=0; i<nCount; i++ )
			{
				nArray[i] = ConvertTools.ToInt32( list[i] );
			}
			return nArray;
		}

		static public float[] GetValueListAsFloatArray( Dictionary<string,object> dic, string sKey )
		{
			return GetValueListAsFloatArray( GetValueList( dic, sKey ) ) ;
		}

		static public float[] GetValueListAsFloatArray( List<object> list )
		{
			if( list==null ) return null;
			int nCount = list.Count;
			float[] fArray = new float[nCount];
			for( int i=0; i<nCount; i++ )
			{
				fArray[i] = ConvertTools.ToFloat( list[i] );
			}
			return fArray;
		}
	
		static public string[] GetValueListAsStringArray( Dictionary<string,object> dic, string sKey )
		{
			return GetValueListAsStringArray( GetValueList( dic, sKey ) ) ;
		}

		static public string[] GetValueListAsStringArray( List<object> list )
		{
			if( list==null ) return null;
			int nCount = list.Count;
			string[] sArray = new string[nCount];
			for( int i=0; i<nCount; i++ )
			{
				sArray[i] = ConvertTools.ToString( list[i] );
			}
			return sArray;
		}

		static public bool[] GetValueListAsBoolArray( Dictionary<string,object> dic, string sKey )
		{
			return GetValueListAsBoolArray( GetValueList( dic, sKey ) ) ;
		}

		static public bool[] GetValueListAsBoolArray( List<object> list )
		{
			if( list==null ) return null;
			int nCount = list.Count;
			bool[] bArray = new bool[nCount];
			for( int i=0; i<nCount; i++ )
			{
				bArray[i] = ConvertTools.ToBool( list[i] );
			}
			return bArray;
		}

		static public T[] GetValueListAsEnumArray<T>( Dictionary<string, object> dic, string sKey ) where T : struct, System.IConvertible
		{
			return GetValueListAsEnumArray<T>( GetValueList( dic, sKey ) ) ;
		}

		static public T[] GetValueListAsEnumArray<T>( List<object> list ) where T : struct, System.IConvertible
		{
			if( list==null ) return null;
			int nCount = list.Count;
			T[] array = new T[nCount];
			for( int i=0; i<nCount; i++ )
			{
				T enumValue;
				if( ParseTools.TryParseEnum<T>((string)list[i], out enumValue ))
				{
					array[i] = enumValue;
				}
			}
			return array;
		}

		static public Vector2? GetValueListAsVector2(Dictionary<string, object> dic, string sKey)
		{
			return GetValueListAsVector2(GetValueList(dic, sKey));
		}

		static public Vector2? GetValueListAsVector2(List<object> list)
		{
			if (list == null || list.Count < 2)
				return null;
			return new Vector2(ConvertTools.ToFloat(list[0]), ConvertTools.ToFloat(list[1]));
		}

		static public Vector3? GetValueListAsVector3(Dictionary<string, object> dic, string sKey)
		{
			return GetValueListAsVector3(GetValueList(dic, sKey));
		}

		static public Vector3? GetValueListAsVector3(List<object> list)
		{
			if (list == null || list.Count < 3)
				return null;
			return new Vector3(ConvertTools.ToFloat(list[0]), ConvertTools.ToFloat(list[1]), ConvertTools.ToFloat(list[2]));
		}

		static public Quaternion? GetValueListAsQuaternion(Dictionary<string, object> dic, string sKey)
		{
			return GetValueListAsQuaternion(GetValueList(dic, sKey));
		}

		static public Quaternion? GetValueListAsQuaternion(List<object> list)
		{
			if (list == null || list.Count < 4)
				return null;
			return new Quaternion(ConvertTools.ToFloat(list[0]), ConvertTools.ToFloat(list[1]), ConvertTools.ToFloat(list[2]), ConvertTools.ToFloat(list[3]));
		}

		static public Color GetValueListAsColor(Dictionary<string, object> dic, string sKey, Color defaultColor)
		{
			Color? color = GetValueListAsColor(dic, sKey);
			if (color.HasValue)
				return color.Value;
			return defaultColor;
		}

		static public Color? GetValueListAsColor(Dictionary<string, object> dic, string sKey)
		{
			return GetValueListAsColor(GetValueList(dic, sKey));
		}

		static public Color? GetValueListAsColor(List<object> list)
		{
			if (list == null || list.Count < 4)
				return null;
			return new Color(ConvertTools.ToFloat(list[0]), ConvertTools.ToFloat(list[1]), ConvertTools.ToFloat(list[2]), ConvertTools.ToFloat(list[3]));
		}

		static public List<object> GetColorAsValueList(Color color)
		{
			List<object> list = new List<object>();
			list.Add(color.r);
			list.Add(color.g);
			list.Add(color.b);
			list.Add(color.a);
			return list;
		}

	}
}
