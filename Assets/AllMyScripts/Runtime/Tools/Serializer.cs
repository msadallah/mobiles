﻿namespace AllMyScripts.Common.Tools
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.IO;
	using System.Runtime.Serialization;

	using UnityEngine;

	public static class Serializer
	{
		/// <summary>
		/// due to Assembly issues, Serializer might not be able to access all classes projectwide.
		/// having a ProjectDeserializer in the project allows to do the deserialization process outside of the assembly.
		/// </summary>
		public abstract class ProjectDeserializer
		{
			public abstract SerializableComponent DeserializeComponentJSON(string type, Dictionary<string, object> dicComponent);
		}

		public static ProjectDeserializer projectDeserializer;
		public static GameObject result => _result;
		private static GameObject _result = null;

		public enum Format
		{
			Binary,
			Json
		}

		public interface ISerializable
		{
			SerializableComponent Serialize();
			void Deserialize(SerializableComponent obj);
		}

		static public bool ByteArrayToFile(string fileName, byte[] byteArray)
		{
			try
			{
				if (!Directory.Exists(Path.GetDirectoryName(fileName)))
					Directory.CreateDirectory(Path.GetDirectoryName(fileName));

				using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
				{
					fs.Write(byteArray, 0, byteArray.Length);
					return true;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Exception caught in process: {0}", ex);
				return false;
			}
		}

		static public void Serialize(string sFilePath, GameObject objectToSerialize, Format format = Format.Binary)
		{
			string sFileDirectoryPath = Path.GetDirectoryName(sFilePath);
			if (!Directory.Exists(sFilePath))
				Directory.CreateDirectory(sFileDirectoryPath);

			SerializableGameObject serializedGo = new SerializableGameObject(objectToSerialize);

			switch (format)
			{
				case Format.Binary:
					FileStream fs = new FileStream(sFilePath, FileMode.Create, FileAccess.ReadWrite);
					try
					{
						System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
						bf.Serialize(fs, serializedGo);
					}
					catch (SerializationException e)
					{
						Debug.LogError("Failed to serialize. Reason: " + e.Message);
						throw;
					}
					finally
					{
						fs.Close();
					}
					break;
				case Format.Json:
					string json = JSON.Serialize(serializedGo.ExportData());
					// serialize JSON to a string and then write string to a file
					File.WriteAllText(sFilePath, json);
					break;
			}
		}

		static public IEnumerator DeserializeASync(byte[] binaryContent, Format format = Format.Binary)
		{
			using (MemoryStream ms = new MemoryStream(binaryContent))
			{
				SerializableGameObject smi = null;

				switch (format)
				{
					case Format.Binary:
						System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
						bf.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
						smi = (SerializableGameObject)bf.Deserialize(ms);
						break;
					case Format.Json:
						string json = new StreamReader(ms).ReadToEnd();
						smi = new SerializableGameObject();
						yield return JSON.DeserializeASync(json);
						Dictionary<string, object> dicJSON = JSON.result as Dictionary<string, object>;
						//Dictionary<string, object> dicJSON = JSON.Deserialize(json) as Dictionary<string, object>;
						yield return smi.SerializableFromDicASync(dicJSON);
						break;
				}

				if (smi != null)
				{
					yield return smi.DeserializeGameObjectASync();
					_result = smi.result;
				}
			}
		}

		static public GameObject Deserialize(byte[] binaryContent, Format format = Format.Binary)
		{
			using (MemoryStream ms = new MemoryStream(binaryContent))
			{
				try
				{
					SerializableGameObject smi = null;
					switch (format)
					{
						case Format.Binary:
							System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
							bf.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
							smi = (SerializableGameObject)bf.Deserialize(ms);
							break;
						case Format.Json:
							string json = new StreamReader(ms).ReadToEnd();
							smi = new SerializableGameObject(JSON.Deserialize(json) as Dictionary<string, object>);
							break;
					}
					return smi.DeserializeGameObject();
				}
				catch (SerializationException e)
				{
					Debug.LogError("Failed to deserialize. Reason: " + e.Message);
					throw;
				}
			}
		}

		static public GameObject Deserialize(string sFilePath, Format format=Format.Binary)
		{
			FileStream fs = new FileStream(sFilePath, FileMode.Open);
			try
			{
				SerializableGameObject smi = null;
				switch (format)
				{
					case Format.Binary:
						System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
						smi = (SerializableGameObject)bf.Deserialize(fs);
						break;
					case Format.Json:
						string json = new StreamReader(fs).ReadToEnd();
						smi = new SerializableGameObject(JSON.Deserialize(json) as Dictionary<string, object>);
						break;
				}
				return smi.DeserializeGameObject();
			}
			catch (SerializationException e)
			{
				Debug.LogError("Failed to deserialize. Reason: " + e.Message);
				throw;
			}
			finally
			{
				fs.Close();
			}
		}
	}

	[Serializable]
	public class SerializableGameObject
	{
		public string name;
		public SerializableGameObject[] childrens;
		public SerializableComponent[] serializedComponents;

		public GameObject result => _result;
		private GameObject _result = null;

		public SerializableGameObject()
		{
		}

		public SerializableGameObject(Dictionary<string, object> dic)
		{
			name = DicTools.GetValueString(dic, "name");
			List<object> childrenList = DicTools.GetValue(dic, "children") as List<object>;
			Debug.Assert(childrenList != null, "childrenList at null in gameObject " + name);
			List<object> componentList = DicTools.GetValue(dic, "components") as List<object>;
			Debug.Assert(componentList != null, "componentList at null in gameObject " + name);
			int childrenCount = childrenList?.Count ?? 0;
			int componentCount = componentList?.Count ?? 0;
			childrens = new SerializableGameObject[childrenCount];
			for (int i = 0; i < childrenCount; ++i)
			{
				childrens[i] = new SerializableGameObject(childrenList[i] as Dictionary<string, object>);
			}
			serializedComponents = new SerializableComponent[componentCount];
			for (int i = 0; i < componentCount; ++i)
			{
				Dictionary<string,object> dicComponent = componentList[i] as Dictionary<string,object>;
				string type = DicTools.GetValueString(dicComponent, "type");
				switch (type)
				{
					case "UnityEngine.Transform":
						serializedComponents[i] = new SerializableTransform(dicComponent);
						break;
					case "UnityEngine.MeshFilter":
						serializedComponents[i] = new SerializableMeshFilter(dicComponent);
						break;
					default:
						if(Serializer.projectDeserializer != null)
							serializedComponents[i] = Serializer.projectDeserializer.DeserializeComponentJSON(type,dicComponent);
						break;
				}

			}
		}

		public IEnumerator SerializableFromDicASync(Dictionary<string, object> dic, int cycle = 100, int counter = 0)
		{
			bool isOrigin = counter == 0;

			name = DicTools.GetValueString(dic, "name");
			List<object> childrenList = DicTools.GetValue(dic, "children") as List<object>;
			Debug.Assert(childrenList != null, "childrenList at null in gameObject " + name);
			List<object> componentList = DicTools.GetValue(dic, "components") as List<object>;
			Debug.Assert(componentList != null, "componentList at null in gameObject " + name);
			int childrenCount = childrenList?.Count ?? 0;
			int componentCount = componentList?.Count ?? 0;
			childrens = new SerializableGameObject[childrenCount];
			for (int i = 0; i < childrenCount; ++i)
			{
				counter++;
				SerializableGameObject smi = new SerializableGameObject();
				IEnumerator iter = smi.SerializableFromDicASync(childrenList[i] as Dictionary<string, object>, cycle, counter);
				while (iter.MoveNext())
				{
					counter = (int)iter.Current;
					if (counter % cycle == 0)
					{
						if (isOrigin)
							yield return null;
						else
							yield return counter;
					}
				}
				childrens[i] = smi;
			}
			serializedComponents = new SerializableComponent[componentCount];
			for (int i = 0; i < componentCount; ++i)
			{
				Dictionary<string,object> dicComponent = componentList[i] as Dictionary<string,object>;
				string type = DicTools.GetValueString(dicComponent, "type");
				switch (type)
				{
					case "UnityEngine.Transform":
						serializedComponents[i] = new SerializableTransform(dicComponent);
						break;
					case "UnityEngine.MeshFilter":
						serializedComponents[i] = new SerializableMeshFilter(dicComponent);
						break;
					default:
						if (Serializer.projectDeserializer != null)
							serializedComponents[i] = Serializer.projectDeserializer.DeserializeComponentJSON(type, dicComponent);
						break;
				}
			}
			yield return counter;
		}

		public SerializableGameObject(GameObject go)
		{
			name = go.name;
			childrens = new SerializableGameObject[go.transform.childCount];

			for (int i = 0; i < go.transform.childCount; ++i)
			{
				//childrenIDs[i] = go.transform.GetChild(i).GetInstanceID();
				childrens[i] = new SerializableGameObject(go.transform.GetChild(i).gameObject);
			}

			Component[] goComponent = go.GetComponents<Component>();
			List<SerializableComponent> serializedComponents = new List<SerializableComponent>();

			for (int componentIdx = 0; componentIdx < goComponent.Length; ++componentIdx)
			{
				Component currentComponent = goComponent[componentIdx];
				if (currentComponent is Transform)
				{
					Transform currentTrans = currentComponent as Transform;
					serializedComponents.Add(new SerializableTransform(currentTrans));
				}
				else if (currentComponent is MeshFilter)
				{
					MeshFilter currentTrans = currentComponent as MeshFilter;
					serializedComponents.Add(new SerializableMeshFilter(currentTrans));
				}
				else if (currentComponent is Serializer.ISerializable)
				{
					serializedComponents.Add((currentComponent as Serializer.ISerializable).Serialize());
				}
			}
			this.serializedComponents = serializedComponents.ToArray();
		}

		public GameObject DeserializeGameObject()
		{
			GameObject go = new GameObject(name);

			foreach (SerializableGameObject serializableGameObject in childrens)
			{
				serializableGameObject.DeserializeGameObject().transform.SetParent(go.transform);
			}

			DeserializeComponents(ref go);

			return go;
		}

		public IEnumerator DeserializeGameObjectASync(int cycle = 100, int counter = 0, Transform parent = null)
		{
			_result = null;

			bool isOrigin = counter == 0;

			GameObject go = new GameObject(name);

			if (parent != null)
				go.transform.SetParent(parent);

			if (isOrigin)
				go.SetActive(false);

			parent = go.transform;

			foreach (SerializableGameObject serializableGameObject in childrens)
			{
				counter++;
				IEnumerator iter = serializableGameObject.DeserializeGameObjectASync(cycle, counter, parent);
				while (iter.MoveNext())
				{
					counter = (int)iter.Current;
					if (counter % cycle == 0)
					{
						if (isOrigin)
							yield return null;
						else
							yield return counter;
					}
				}
			}

			DeserializeComponents(ref go);

			if (isOrigin)
				go.SetActive(true);

			_result = go;
		}

		private void DeserializeComponents(ref GameObject go)
		{
			foreach (SerializableComponent serializableComponent in serializedComponents)
			{
				if (serializableComponent is SerializableTransform)
				{
					SerializableTransform serializabletransform = serializableComponent as SerializableTransform;
					serializabletransform.Deserialize(ref go);
				}
				else if (serializableComponent is SerializableMeshFilter)
				{
					SerializableMeshFilter serializableMeshFilter = serializableComponent as SerializableMeshFilter;
					Mesh mesh = new Mesh();
					serializableMeshFilter.Deserialize(ref mesh);

					MeshRenderer renderer;
					MeshCollider collider;
					MeshFilter filter = go.AddComponent<MeshFilter>();
					filter.sharedMesh = mesh;

					if (!(name.ToLower().Contains("space-")))
					{
						renderer = go.AddComponent<MeshRenderer>();
					}
					else
					{
						collider = go.AddComponent<MeshCollider>();
						collider.sharedMesh = mesh;
					}
				}
				else
				{
					//it's a Serializer.ISerializable
					Serializer.ISerializable serializable = (Serializer.ISerializable)go.AddComponent(serializableComponent.GetSerializedType());
					serializable.Deserialize(serializableComponent);
				}
			}
		}

		public object ExportData()
		{
			Dictionary<string, object> dic = new Dictionary<string, object>();
			List<object> childrenList = new List<object>();
			List<object> componentList = new List<object>();
			dic["name"] = name;
			dic["children"] = childrenList;
			dic["components"] = componentList;
			foreach (SerializableGameObject serializableGameObject in childrens)
			{
				childrenList.Add(serializableGameObject.ExportData());
			}
			foreach (SerializableComponent serializableComponent in serializedComponents)
			{
				componentList.Add(serializableComponent.GetExportedData());
			}
			return dic;
		}
	}

	[Serializable]
	public abstract class SerializableComponent
	{
		public abstract Type GetSerializedType();
		protected abstract object ExportData();

		public object GetExportedData()
		{
			object data = ExportData();

			if(data is Dictionary<string,object>)
			{
				var dataDic = ((Dictionary<string, object>)data);
				if (!dataDic.ContainsKey("type"))
					dataDic["type"] = GetSerializedType().ToString();
			}

			return data;
		}
	}

	[Serializable]
	public class SerializableTransform : SerializableComponent
	{
		[SerializeField]
		public float[] position;

		[SerializeField]
		public float[] eulerAngles;

		[SerializeField]
		public float[] scale;

		public SerializableTransform(Transform trans)
		{
			position = new float[3]; // initialize position array.
			eulerAngles = new float[3]; // initialize angles array.
			scale = new float[3]; // initialize scale array.

			position[0] = trans.localPosition.x;
			position[1] = trans.localPosition.y;
			position[2] = trans.localPosition.z;

			eulerAngles[0] = trans.localEulerAngles.x;
			eulerAngles[1] = trans.localEulerAngles.y;
			eulerAngles[2] = trans.localEulerAngles.z;

			scale[0] = trans.localScale.x;
			scale[1] = trans.localScale.y;
			scale[2] = trans.localScale.z;

		}

		public SerializableTransform(Dictionary<string, object> dic)
		{
			position = new float[3];
			position[0] = DicTools.GetValueFloat(dic, "posX");
			position[1] = DicTools.GetValueFloat(dic, "posY");
			position[2] = DicTools.GetValueFloat(dic, "posZ");
			eulerAngles = new float[3];
			eulerAngles[0] = DicTools.GetValueFloat(dic, "rotX");
			eulerAngles[1] = DicTools.GetValueFloat(dic, "rotY");
			eulerAngles[2] = DicTools.GetValueFloat(dic, "rotZ");
			scale = new float[3];
			scale[0] = DicTools.GetValueFloat(dic, "scaleX");
			scale[1] = DicTools.GetValueFloat(dic, "scaleY");
			scale[2] = DicTools.GetValueFloat(dic, "scaleZ");
		}

		public void Deserialize(ref GameObject go)
		{
			go.transform.localPosition = new Vector3(position[0], position[1], position[2]);
			go.transform.localEulerAngles = new Vector3(eulerAngles[0], eulerAngles[1], eulerAngles[2]);
			go.transform.localScale = new Vector3(scale[0], scale[1], scale[2]);
		}

		public override Type GetSerializedType()
		{
			return typeof(Transform);
		}

		protected override object ExportData()
		{
			Dictionary<string,object> dic = new Dictionary<string, object>();
			dic["type"] = GetSerializedType().ToString();
			dic["posX"] = position[0];
			dic["posY"] = position[1];
			dic["posZ"] = position[2];
			dic["rotX"] = eulerAngles[0];
			dic["rotY"] = eulerAngles[1];
			dic["rotZ"] = eulerAngles[2];
			dic["scaleX"] = scale[0];
			dic["scaleY"] = scale[1];
			dic["scaleZ"] = scale[2];
			return dic;
		}
	}

	[Serializable]
	public class SerializableMeshFilter : SerializableComponent
	{
		[SerializeField]
		public float[] vertices;

		[SerializeField]
		public int[] triangles;

		[SerializeField]
		public float[] uv;

		[SerializeField]
		public float[] uv2;

		[SerializeField]
		public float[] normals;

		[SerializeField]
		public float[] colors;

		[SerializeField]
		public string name;

		public SerializableMeshFilter(MeshFilter meshFilter) // Constructor: takes a mesh and fills out SerializableMeshInfo data structure which basically mirrors Mesh object's parts.
		{
			Mesh m = meshFilter.sharedMesh;

			name = m.name;

			vertices = new float[m.vertexCount * 3]; // initialize vertices array.
			for (int i = 0; i < m.vertexCount; i++) // Serialization: Vector3's values are stored sequentially.
			{
				vertices[i * 3] = m.vertices[i].x;
				vertices[i * 3 + 1] = m.vertices[i].y;
				vertices[i * 3 + 2] = m.vertices[i].z;
			}
			triangles = new int[m.triangles.Length]; // initialize triangles array
			for (int i = 0; i < m.triangles.Length; i++) // Mesh's triangles is an array that stores the indices, sequentially, of the vertices that form one face
			{
				triangles[i] = m.triangles[i];
			}
			uv = new float[m.uv.Length * 2]; // initialize uvs array
			for (int i = 0; i < m.uv.Length; i++) // uv's Vector2 values are serialized similarly to vertices' Vector3
			{
				uv[i * 2] = m.uv[i].x;
				uv[i * 2 + 1] = m.uv[i].y;
			}
			uv2 = new float[m.uv2.Length * 2]; // uv2
			for (int i = 0; i < m.uv2.Length; i++)
			{
				uv2[i * 2] = m.uv2[i].x;
				uv2[i * 2 + 1] = m.uv2[i].y;
			}

			normals = new float[m.normals.Length * 3]; // normals are very important
			for (int i = 0; i < m.normals.Length; i++) // Serialization
			{
				normals[i * 3] = m.normals[i].x;
				normals[i * 3 + 1] = m.normals[i].y;
				normals[i * 3 + 2] = m.normals[i].z;
			}

			colors = new float[m.colors.Length * 4];
			for (int i = 0; i < m.colors.Length; i++)
			{
				colors[i * 4] = m.colors[i].r;
				colors[i * 4 + 1] = m.colors[i].g;
				colors[i * 4 + 2] = m.colors[i].b;
				colors[i * 4 + 3] = m.colors[i].a;
			}
		}

		public SerializableMeshFilter(Dictionary<string, object> dic)
		{
			name = DicTools.GetValueString(dic, "name");
			vertices = DicTools.GetValueListAsFloatArray(DicTools.GetValue(dic, "vertices") as List<object>);
			triangles = DicTools.GetValueListAsIntArray(DicTools.GetValue(dic, "triangles") as List<object>);
			uv = DicTools.GetValueListAsFloatArray(DicTools.GetValue(dic, "uv") as List<object>);
			uv2 = DicTools.GetValueListAsFloatArray(DicTools.GetValue(dic, "uv2") as List<object>);
			normals = DicTools.GetValueListAsFloatArray(DicTools.GetValue(dic, "normals") as List<object>);
			colors = DicTools.GetValueListAsFloatArray(DicTools.GetValue(dic, "colors") as List<object>);
		}

		// GetMesh gets a Mesh object from currently set data in this SerializableMeshInfo object.
		// Sequential values are deserialized to Mesh original data types like Vector3 for vertices.
		public void Deserialize(ref Mesh m)
		{
			m.name = name;

			List<Vector3> verticesList = new List<Vector3>();
			for (int i = 0; i < vertices.Length / 3; i++)
			{
				verticesList.Add(new Vector3(
						vertices[i * 3], vertices[i * 3 + 1], vertices[i * 3 + 2]
					));
			}
			m.SetVertices(verticesList);

			m.triangles = triangles;
			List<Vector2> uvList = new List<Vector2>();
			for (int i = 0; i < uv.Length / 2; i++)
			{
				uvList.Add(new Vector2(
						uv[i * 2], uv[i * 2 + 1]
					));
			}
			m.SetUVs(0, uvList);

			List<Vector2> uv2List = new List<Vector2>();
			for (int i = 0; i < uv2.Length / 2; i++)
			{
				uv2List.Add(new Vector2(
						uv2[i * 2], uv2[i * 2 + 1]
					));
			}
			m.SetUVs(1, uv2List);

			List<Vector3> normalsList = new List<Vector3>();
			for (int i = 0; i < normals.Length / 3; i++)
			{
				normalsList.Add(new Vector3(
						normals[i * 3], normals[i * 3 + 1], normals[i * 3 + 2]
					));
			}
			m.SetNormals(normalsList);

			List<Color> colorsList = new List<Color>();
			for (int i = 0; i < colors.Length / 4; i++)
			{
				colorsList.Add(new Color(
						colors[i * 4], colors[i * 4 + 1], colors[i * 4 + 2], colors[i * 4 + 3]
					));
			}
			m.SetColors(colorsList);
		}

		public override Type GetSerializedType()
		{
			return typeof(MeshFilter);
		}

		protected override object ExportData()
		{
			Dictionary<string,object> dic = new Dictionary<string, object>();
			dic["type"] = GetSerializedType().ToString();
			dic["name"] = name;
			dic["vertices"] = ExportListData<float>(vertices);
			dic["triangles"] = ExportListData<int>(triangles);
			dic["uv"] = ExportListData<float>(uv);
			dic["uv2"] = ExportListData<float>(uv2);
			dic["normals"] = ExportListData<float>(normals);
			dic["colors"] = ExportListData<float>(colors);
			return dic;
		}

		List<object> ExportListData<T>(T[] listVal) where T : IComparable
		{
			List<object> list = new List<object>();
			foreach (T val in listVal)
				list.Add(val);
			return list;
		}


	}
}
