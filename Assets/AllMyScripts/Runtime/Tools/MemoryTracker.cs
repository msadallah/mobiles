namespace AllMyScripts.Common.Tools
{
    using UnityEngine;
    using System.Collections.Generic;
    using System.IO;
    using UnityEngine.Profiling;
    [AddComponentMenu("AllMyScripts/Tools/Memory Tracker")]
	public sealed class MemoryTracker : MonoBehaviour
	{
		public static List<int> m_IdList = new List<int>();
		public static List<string> m_TypeList = new List<string>();
		public static List<string> m_NameList = new List<string>();
		private static long m_nUsedHeapSize;
		private static long m_nMonoHeapSize;
		private static long m_nMonoUsedSize;
		private static int m_nFileNumber;
	
		public static void SnapshotReferenceMemoryState()
		{
			Resources.UnloadUnusedAssets();
			System.GC.Collect();

			Object[] Objects = Resources.FindObjectsOfTypeAll( typeof(Object) );

			m_IdList.Clear();
			m_NameList.Clear();
			m_TypeList.Clear();
			foreach( Object o in Objects )
			{
				m_IdList.Add( o.GetInstanceID() );
				m_NameList.Add( o.name );
				m_TypeList.Add( o.GetType().ToString() );
			}

			m_nUsedHeapSize = Profiler.usedHeapSizeLong;
			m_nMonoHeapSize = Profiler.GetMonoHeapSizeLong();
			m_nMonoUsedSize = Profiler.GetMonoUsedSizeLong();
		}
	
		public static void CompareMemoryStateToSnapshot( bool bFindObjectsLost, bool bFormatCSV = false )
		{
			Resources.UnloadUnusedAssets();
			System.GC.Collect();

			StreamWriter sw = File.CreateText( "MemStats_" + string.Format("{0:00}", m_nFileNumber) + ".txt" );
			m_nFileNumber++;
		
			long nUsedHeapSize = Profiler.usedHeapSizeLong;
			long nMonoHeapSize = Profiler.GetMonoHeapSizeLong();
			long nMonoUsedSize = Profiler.GetMonoUsedSizeLong();
	
			sw.WriteLine( "##################################################################" );
			sw.WriteLine( "Added " + m_IdList.Count + " objects" );
			sw.WriteLine( "##################################################################" );
			sw.WriteLine( "Profiler.usedHeapSize=" + nUsedHeapSize + ", delta=" + ( nUsedHeapSize-m_nUsedHeapSize ) );
			sw.WriteLine( "Profiler.GetMonoHeapSize=" + nMonoHeapSize + ", delta=" + ( nMonoHeapSize-m_nMonoHeapSize ) );
			sw.WriteLine( "Profiler.GetMonoUsedSize=" + nMonoUsedSize + ", delta=" + ( nMonoUsedSize-m_nMonoUsedSize ) );
			sw.WriteLine( "MemoryDump :" );

			Object[] Objects = Resources.FindObjectsOfTypeAll( typeof(Object) );
		
			int nNewObjectsId = 0;
			int nNewObjectsName = 0;
			string sNewObjectsId = string.Empty;
			string sNewObjectsName = string.Empty;

			for( int i=0; i<Objects.Length; i++ )
			{
				Object o = Objects[i];
				long nRuntimeMemorySize = Profiler.GetRuntimeMemorySizeLong( o );
				string sFormat = bFormatCSV ? "{1};{2};{3};{4};{5}" : "+ {1} [{2}] --> {3} generation : {4} mem : {5}";
				if( !m_IdList.Contains( o.GetInstanceID() ) )
				{
					nNewObjectsId++;
					sNewObjectsId = sNewObjectsId + "\n" + System.String.Format( sFormat, o.GetType().ToString(), o.GetInstanceID(), o.name, System.GC.GetGeneration( o ), nRuntimeMemorySize );
				}
				if( !m_NameList.Contains( o.name ) )
				{
					nNewObjectsName++;
					sNewObjectsName = sNewObjectsName + "\n" + System.String.Format( sFormat, o.GetType().ToString(), o.GetInstanceID(), o.name, System.GC.GetGeneration( o ), nRuntimeMemorySize );
				}
			}
		
			sw.WriteLine( "\nNew objects (by ID) : " + nNewObjectsId );
			sw.WriteLine( "------------------------------------------------------------------" );
			if( bFormatCSV ) sw.Write( "Type;ID;Name;Genration;Memory" );
			sw.WriteLine( sNewObjectsId );

			sw.WriteLine( "\nNew objects (by name) : " + nNewObjectsName );
			sw.WriteLine( "------------------------------------------------------------------" );
			if( bFormatCSV ) sw.Write( "Type;ID;Name;Genration;Memory" );
			sw.WriteLine( sNewObjectsName );
		
			int nLostObjects = 0;
		
			if( bFindObjectsLost )
			{
				string sLostObjects = string.Empty;
			
				for( int i=0; i<m_IdList.Count; i++ )
				{
					int nId = m_IdList[i];
					bool bFound = false;
		
					for( int j=0; j<Objects.Length; j++ )
					{
						if( Objects[j].GetInstanceID()==nId )
						{
							bFound = true;
							break;
						}
					}
				
					if( !bFound )
					{
						nLostObjects++;
						if( bFormatCSV )
							sLostObjects = string.Concat( sLostObjects, "\n" + m_TypeList[i] + ";" + nId + ";" + m_NameList[i] );
						else
							sLostObjects = string.Concat( sLostObjects, "\n - " + m_TypeList[i] + " [" + nId + "] --> " + m_NameList[i] );
					}
				}

				sw.WriteLine( "\nObjects lost : " + nLostObjects );
				sw.WriteLine( "------------------------------------------------------------------" );
				if( bFormatCSV ) sw.Write( "Type;ID;Name" );
				sw.WriteLine( sLostObjects );
			}

			sw.WriteLine( "\nDelta objects : " + (nNewObjectsId-nLostObjects) );
			sw.Close();
		}
	}
}
