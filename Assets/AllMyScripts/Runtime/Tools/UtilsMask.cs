﻿namespace AllMyScripts.Common.Utils
{
    using UnityEngine;

    public class UtilsMask
    {

        public static bool IsInLayerMask(GameObject obj, LayerMask mask)
        {
            return ((mask.value & (1 << obj.layer)) > 0);
        }

        public static bool IsInLayerMask(uint a, uint b)
        {
            //DLog.Log("UtilsMask.IsInLayerMask : a : "+a+", b : "+b);

            return (a & b) == b;
        }

    }
}