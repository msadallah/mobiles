#if UNITY_EDITOR
namespace AllMyScripts.Common.StateMachine
{
	using UnityEngine;

	using System.Collections.Generic;

	using AllMyScripts.Common.Tools;

	//! @class HierarchicalStateMachine
	//!
	//! @brief	State Machine where a state can be parent to another one
	public class HierarchicalStateMachine
	{
	#region State Declaration
		//! @class HierarchicalState
		//!
		//! @brief	State in a Hierarchical State Machine
		public abstract class State
		{
			//! get the owner of the state machine
			protected MonoBehaviour owner{ get{ return m_stateMachine.m_owner; } }

			//! Called after the state is created
			//!
			//! @param	oUserdataArray	data used to initialized the state
			protected virtual bool Init( params object[] oUserdataArray ) { return true; }
			//! Called before the state is destroyed
			protected virtual void Release() {}

			//! Called after the state becomes the current one
			//!
			//!	@param	stateFrom	previous state (may be the parent, a child or a sibling)
			protected virtual void Enter( State stateFrom ) {}
			//! Called before the state stops being the current one
			//!	@param	stateTo		nex state (may be the parent, a child or a sibling)
			protected virtual void Exit( State stateTo ) {}

			//! Called when the state is the current one and a message is being processed by the state machine
			//!
			//!	@params	oUserdataArray	parameters given in the initialization of the state
			protected virtual bool OnMessage( params object[] oUserdataArray ) { return false; }

			//! Change the current state (aka the caller) to another one
			//!
			//!	@tparam	t_State			type of the state to change to
			//!	@params	oUserdataArray	parameters given in the initialization of the state
			protected void ChangeState<t_State>( params object[] oUserdataArray ) where t_State : State, new() { m_stateMachine._ChangeState<t_State>( this, oUserdataArray ); }
			//! Push a state as child of the current one (aka the caller)
			//!
			//!	@tparam	t_State 		type of the state to push
			//!	@params	oUserdataArray	parameters given in the initialization of the state
			protected void PushState<t_State>( params object[] oUserdataArray ) where t_State : State, new() { m_stateMachine._PushState<t_State>( this, oUserdataArray ); }
			//! Pop the current state (aka the caller)
			protected void PopState() { m_stateMachine._PopState( this ); }

		#region State Machine Communication Methods
			internal void _SetStateMachine( HierarchicalStateMachine stateMachine ) { m_stateMachine = stateMachine; }

			internal bool _CallInit( params object[] oUserdataArray ) { return Init( oUserdataArray ); }
			internal void _CallRelease() { Release(); }

			internal void _CallEnter( State stateFrom ) { Enter(stateFrom); }
			internal void _CallExit( State stateTo ) { Exit(stateTo); }

			internal bool _CallOnMessage( params object[] oUserdataArray ) { return OnMessage( oUserdataArray ); }
		#endregion

		#region Private
			#region Attributes
			private HierarchicalStateMachine m_stateMachine;
			#endregion
		#endregion
		}
	#endregion

	#region StateOverrideDictionary Declaration
		//! @class	StateInheritanceConfig
		//!
		//! @brief	Configuration of states being overriden
		//!			This is useful if the state machine behaviour is defined by a base class and you need to override the default behaviour by replacing operations calls.
		//!			For instance, if we had a state machine for our entire game, we could override states in each specific project. State 'SplashScreen' could be overriden by 
		//!			a derived class named 'ProjectSplashScreen' who would get the specific resources.
		public sealed class StateInheritanceConfig
		{
			//! Constructor
			public StateInheritanceConfig()
			{
				m_overridenStates = new Dictionary<System.Type, System.Type>();
			}

			//!	Indicates that a state will override calls to another state
			//!
			//!	@tparam	t_State1	base state
			//!	@tparam	t_State2	inherited state
			public void OverrideState<t_State1, t_State2>() where t_State1 : State where t_State2 : t_State1
			{
				System.Type typeState1 = typeof( t_State1 );
				System.Type typeState2 = typeof( t_State2 );
				GlobalTools.Assert( typeState1!=typeState2, "OverrideState called with same types : '"+typeState1.FullName+"'." );
				GlobalTools.Assert( typeState1!=typeof( State ), "OverrideState called with base state invalid. You can not override all states." );

				m_overridenStates[typeState1] = typeState2;
			}

		#region State Machine Communication Methods
			internal State CreateState<t_State>() where t_State : State, new()
			{
				System.Type baseTypeState = typeof( State );
				System.Type typeState = typeof( t_State );

				System.Type typeOverrideState = typeof( State );
				while( typeState!=baseTypeState && m_overridenStates.TryGetValue( typeState, out typeOverrideState )==false )
				{
					typeState = typeState.BaseType;
				}

				if( typeState!=baseTypeState )
				{
					return System.Activator.CreateInstance( typeOverrideState ) as State;
				}
				else
				{
					return new t_State();
				}
			}
		#endregion

		#region Private
			#region Attributes
			private Dictionary<System.Type, System.Type> m_overridenStates;
			#endregion
		#endregion
		}
	#endregion

		//! Get the current state of the state machine
		public State CurrentState{ get{ return m_states.Count==0? null : m_states.Peek(); } }

		//! Creates a new state machine with an owner given
		//!
		//! @param	owner	creator of the state machine in order to keep the link between the states and the game object in which the state machine is
		public HierarchicalStateMachine( MonoBehaviour owner, StateInheritanceConfig stateOverrideConfig = null )
		{
			GlobalTools.Assert( owner!=null, "State Machine must have an owner." );
			m_owner = owner;
			m_states = new Stack<State>();
			m_bIsBusy = false;

			m_stateOverrideConfig = stateOverrideConfig;
		}

		//! Initialize the state machine with a state
		//!
		//! @tparam	t_State		first state
		//! @params	oUserdata	parameters given in the initialization of the state
		public void Init<t_State>( params object[] oUserdataArray ) where t_State : State, new()
		{
			GlobalTools.Assert( m_states.Count==0, "State machine already initialized." );
			PushState<t_State>( oUserdataArray );
		}

		//! Process a message to the current state
		//!
		//! @param	options		define what behaviour to adopt in case the message has not been treated
		//!	@params	oUserdata	content of the message
		public void ProcessMessage( SendMessageOptions options, params object[] oUserdataArray )
		{
			State currentState = CurrentState;
			if( currentState!=null )
			{
				m_bIsBusy = true;
				bool bHasBeenProcessed = currentState._CallOnMessage( oUserdataArray );
				if( options==SendMessageOptions.RequireReceiver && bHasBeenProcessed==false )
				{
					Debug.LogError( "State machine message has not been processed by the current state of type '"+currentState.GetType().FullName+"'." );
				}
				m_bIsBusy = false;

				ProcessEnqueuedOperations();
			}
		}

	#region State Communication Methods
		private void _ChangeState<t_State>( State state, params object[] oUserdataArray ) where t_State : State, new()
		{
			GlobalTools.Assert( state!=null );
			GlobalTools.Assert( state==CurrentState );
		
			if( m_bIsBusy )
			{
				EnqueueOperation( () => { ChangeState<t_State>( oUserdataArray ); } );
			}
			else
			{
				ChangeState<t_State>( oUserdataArray );
				ProcessEnqueuedOperations();
			}
		}

		private void _PushState<t_State>( State state, params object[] oUserdataArray ) where t_State : State, new()
		{
			GlobalTools.Assert( state!=null );
			GlobalTools.Assert( state==CurrentState );

			if( m_bIsBusy )
			{
				EnqueueOperation( () => { PushState<t_State>( oUserdataArray ); } );
			}
			else
			{
				PushState<t_State>( oUserdataArray );
				ProcessEnqueuedOperations();
			}
		}

		private void _PopState( State state )
		{
			GlobalTools.Assert( state!=null );
			GlobalTools.Assert( state==CurrentState );

			if( m_bIsBusy )
			{
				EnqueueOperation( PopState );
			}
			else
			{
				PopState();
				ProcessEnqueuedOperations();
			}
		}
	#endregion

	#region Private
		#region Methods
		private State CreateState<t_State>() where t_State : State, new()
		{		
			State newState = null;
			if( m_stateOverrideConfig==null )
			{
				newState = new t_State();
			}
			else
			{
				newState = m_stateOverrideConfig.CreateState<t_State>();
			}

			newState._SetStateMachine( this );
			return newState;
		}

		private void ChangeState<t_State>( params object[] oUserdataArray ) where t_State : State, new()
		{
			GlobalTools.Assert( m_bIsBusy==false );
			m_bIsBusy = true;

			State state = CreateState<t_State>();
			if( state._CallInit( oUserdataArray ) == false )
			{
				Debug.LogError( "State Machine failed to change to state of type '"+state.GetType().FullName+"'.\nInitialization has failed." );
				state = null;
				return;
			}

			State previousCurrentState = m_states.Count==0? null : m_states.Peek();
			if( previousCurrentState!=null )
			{
				previousCurrentState._CallExit( state );
			}
			m_states.Pop();

			m_states.Push( state );
			state._CallEnter( previousCurrentState );

			m_bIsBusy = false;

			ProcessEnqueuedOperations();
		}

		private void PushState<t_State>( params object[] oUserdataArray ) where t_State : State, new()
		{
			GlobalTools.Assert( m_bIsBusy==false );
			m_bIsBusy = true;

			State state = CreateState<t_State>();
			if( state._CallInit( oUserdataArray )==false )
			{
				Debug.LogError( "State Machine failed to push state of type '"+state.GetType().FullName+"'.\nInitialization has failed." );
				state = null;
				return;
			}

			State previousCurrentState = m_states.Count==0? null : m_states.Peek();
			if( previousCurrentState!=null )
			{
				previousCurrentState._CallExit( state );
			}

			m_states.Push( state );
			state._CallEnter( previousCurrentState );

			m_bIsBusy = false;

			ProcessEnqueuedOperations();
		}

		private void PopState()
		{
			GlobalTools.Assert( m_bIsBusy==false );
			m_bIsBusy = true;

			GlobalTools.Assert( m_states.Count>0 );

			State oldState = m_states.Pop();
			State newState = m_states.Count==0? null : m_states.Peek();
			m_states.Push( oldState );

			oldState._CallExit( newState );
			m_states.Pop();

			if( newState!=null )
			{
				newState._CallEnter( oldState );
			}

			oldState._CallRelease();
			oldState = null;

			m_bIsBusy = false;

			ProcessEnqueuedOperations();
		}

		private void EnqueueOperation( System.Action operation )
		{
			GlobalTools.Assert( m_enqueuedOperation==null, "There is already an operation enqueued.\nThis happens if multiple operations (ChangeState, PushState, PopState, OnMessage) are sent in the same operation." );
			m_enqueuedOperation = operation;
		}

		private void ProcessEnqueuedOperations()
		{
			while( m_enqueuedOperation!=null )
			{
				System.Action enqueuedOperation = m_enqueuedOperation;
				m_enqueuedOperation = null;

				enqueuedOperation();
			}
		}
		#endregion

		#region Attributes
		private readonly MonoBehaviour m_owner;
		private readonly StateInheritanceConfig m_stateOverrideConfig;

		private bool m_bIsBusy;
		private Stack<State> m_states;

		// enqueued operation
		private System.Action m_enqueuedOperation;
		#endregion
	#endregion
	}
}
#endif
