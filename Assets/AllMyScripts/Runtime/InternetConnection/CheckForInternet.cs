﻿namespace AllMyScripts.Tools
{
	using System.Collections;
	using AllMyScripts.Common.Tools;
	using UnityEngine;
    using UnityEngine.Networking;

	public static class CheckForInternet
	{
		public static bool haveInternet { get; private set; } = true;

		/// <summary>
		/// Check if the device have access to internet
		/// </summary>
		/// <returns>Check <see cref="haveInternet"/> to get the result of this enumerator</returns>
		public static IEnumerator Run(int timeout = 10)
		{
			DLog.Log($"CheckForInternet.Run (timeout = {timeout})");

			UnityWebRequest uwr = new UnityWebRequest("https://google.com");
			uwr.timeout = timeout;
			uwr.SendWebRequest();

			float startTime = Time.time;
			bool isTimeout = false;
			while (!uwr.isDone && !isTimeout)
			{
				if (Time.time - startTime > timeout)
				{
					isTimeout = true;
					Debug.LogWarning($"CheckForInternet : Timeout : {Time.time - startTime}, (timeout : {timeout}s)");
				}

				yield return null;
			}

			haveInternet = string.IsNullOrEmpty(uwr.error) && !isTimeout;

			if (!haveInternet)
				Debug.LogWarning($"Check Internet connection failed. Error : {uwr.error}");
		}
	}
}