namespace AllMyScripts.Common.States
{
    public class StateManager
	{
		private State m_currentState;
		
		public State Current
		{
			get { return m_currentState; }
		}

		public void Enter( State state )
		{
			EnterState( state );
		}
		
		public void Exit()
		{
			ExitCurrent();
		}

		public void UpdateCurrentState()
		{
			if( m_currentState != null )
				m_currentState.OnUpdate();
		}
		
		protected void EnterState( State state )
		{
			ExitCurrent();
			if( state != null )
			{
				m_currentState = state;
				m_currentState.SetStateManager(this);
				m_currentState.OnEnter();
			}
		}

		protected void ExitCurrent()
		{
			if( m_currentState != null )
			{
				try
				{
					m_currentState.OnExit();
				}
				catch(System.Exception e)
				{
					UnityEngine.Debug.LogError(e);
				}
				m_currentState.SetStateManager(null);
				m_currentState = null;
			}
		}
	}
}
