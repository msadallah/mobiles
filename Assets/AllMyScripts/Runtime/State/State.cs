namespace AllMyScripts.Common.States
{
	public abstract class State
	{
		protected StateManager m_stateManager;

		public void SetStateManager(StateManager stateManager)
		{
			m_stateManager = stateManager;
		}

		public virtual void OnEnter() { }

		public virtual void OnUpdate() { }
		public virtual void OnLateUpdate() { }

		public virtual void OnExit()
		{
			m_stateManager = null;
		}
	}
}
