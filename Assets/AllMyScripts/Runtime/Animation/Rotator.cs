﻿namespace AllMyScripts.Common.Animation
{
    using UnityEngine;
#if ODIN_INSPECTOR
	using Sirenix.OdinInspector;
#endif

	public class Rotator : MonoBehaviour
    {
		public enum Axis
		{
			Axis_X,
			Axis_Y,
			Axis_Z,
			Custom
		}

        public float fSpeed;
		public Axis axis = Axis.Axis_Z;
#if ODIN_INSPECTOR
		[ShowIf("axis", Axis.Custom)]
#endif
		public Vector3 customAxis = Vector3.zero;
		public Space spaceRotation = Space.Self;

		private Vector3 _usedAxis;

		private void Start()
		{
			switch (axis)
			{
				case Axis.Axis_X:
					_usedAxis = Vector3.right;
					break;
				case Axis.Axis_Y:
					_usedAxis = Vector3.up;
					break;
				case Axis.Axis_Z:
					_usedAxis = Vector3.forward;
					break;
				case Axis.Custom:
					_usedAxis = customAxis;
					break;
			}
			
		}

		private void Update()
        {
            transform.Rotate(_usedAxis * fSpeed * Time.deltaTime, spaceRotation);
        }
    }
}