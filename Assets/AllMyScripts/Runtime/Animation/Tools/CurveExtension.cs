namespace UnityEngine
{
    /*!
     * \author Vincent Paquin
     */
    public static class CurveExtension
    {
        /// <summary>
        /// Same as Mathf.InverseLerp, but using a curve to smooth the value
        /// </summary>
        /// <param name="curve"> the lerping curve, goes from t=0 to t=1</param>
        /// <param name="fMin">the smallest possible value</param>
        /// <param name="fMax">the biggest possible value</param>
        /// <param name="fValue">the current value</param>
        /// <returns>the animated value</returns>
        public static float InverseLerp(this AnimationCurve curve, float fMin, float fMax, float fValue)
        {
            return curve.Evaluate(Mathf.InverseLerp(fMin, fMax, fValue));
        }

        /// <summary>
        /// Animates a lerp
        /// </summary>
        /// <param name="curve">The curve to use</param>
        /// <param name="fStartValue">The smallest possible value</param>
        /// <param name="fEndValue">The biggest possible value</param>
        /// <param name="fStartTime">The time when the animation started</param>
        /// <param name="fEndTime">The time when the animation will be over</param>
        /// <param name="fCurrentTime">The current time</param>
        /// <returns>The animated current value</returns>
        public static float AnimatedLerp(this AnimationCurve curve, float fStartValue, float fEndValue, float fStartTime, float fEndTime, float fCurrentTime)
        {
            return Mathf.Lerp(fStartValue, fEndValue, curve.InverseLerp(fStartTime, fEndTime, fCurrentTime));
        }

        /// <summary>
        /// Animates a lerp
        /// </summary>
        /// <param name="curve">The curve to use</param>
        /// <param name="cStartValue">The smallest possible value</param>
        /// <param name="cEndValue">The biggest possible value</param>
        /// <param name="fStartTime">The time when the animation started</param>
        /// <param name="fEndTime">The time when the animation will be over</param>
        /// <param name="fCurrentTime">The current time</param>
        /// <returns>The animated current value</returns>
        public static Color AnimatedLerp(this AnimationCurve curve, Color cStartValue, Color cEndValue, float fStartTime, float fEndTime, float fCurrentTime)
        {
            float iLerp = curve.InverseLerp(fStartTime, fEndTime, fCurrentTime);
            return new Color(
                Mathf.Lerp(cStartValue.r, cEndValue.r, iLerp),
                Mathf.Lerp(cStartValue.g, cEndValue.g, iLerp),
                Mathf.Lerp(cStartValue.b, cEndValue.b, iLerp),
                Mathf.Lerp(cStartValue.a, cEndValue.a, iLerp));
        }

        /// <summary>
        /// Animates a lerp allowed to go below Start Value and above End Value
        /// </summary>
        /// <param name="curve">The curve to use</param>
        /// <param name="fStartValue">The smallest possible value</param>
        /// <param name="fEndValue">The biggest possible value</param>
        /// <param name="fStartTime">The time when the animation started</param>
        /// <param name="fEndTime">The time when the animation will be over</param>
        /// <param name="fCurrentTime">The current time</param>
        /// <returns>The animated current value</returns>
        public static float AnimatedLerpUnclamped(this AnimationCurve curve, float fStartValue, float fEndValue, float fStartTime, float fEndTime, float fCurrentTime)
        {
            return Mathf.LerpUnclamped(fStartValue, fEndValue, curve.InverseLerp(fStartTime, fEndTime, fCurrentTime));
        }

        /// <summary>
        /// Animates a lerp
        /// </summary>
        /// <param name="curve">The curve to use</param>
        /// <param name="fStartValue">The smallest possible value</param>
        /// <param name="fEndValue">The biggest possible value</param>
        /// <param name="fStartTime">The time when the animation started</param>
        /// <param name="fEndTime">The time when the animation will be over</param>
        /// <param name="fCurrentTime">The current time</param>
        /// <returns>The animated current value</returns>
        public static Vector3 AnimatedLerp(this AnimationCurve curve, Vector3 fStartValue, Vector3 fEndValue, float fStartTime, float fEndTime, float fCurrentTime)
        {
            return Vector3.Lerp(fStartValue, fEndValue, curve.InverseLerp(fStartTime, fEndTime, fCurrentTime));
        }

        /// <summary>
        /// Animates a lerp allowed to go below Start Value and above End Value
        /// </summary>
        /// <param name="curve">The curve to use</param>
        /// <param name="fStartValue">The smallest possible value</param>
        /// <param name="fEndValue">The biggest possible value</param>
        /// <param name="fStartTime">The time when the animation started</param>
        /// <param name="fEndTime">The time when the animation will be over</param>
        /// <param name="fCurrentTime">The current time</param>
        /// <returns>The animated current value</returns>
        public static Vector3 AnimatedLerpUnclamped(this AnimationCurve curve, Vector3 fStartValue, Vector3 fEndValue, float fStartTime, float fEndTime, float fCurrentTime)
        {
            return Vector3.LerpUnclamped(fStartValue, fEndValue, curve.InverseLerp(fStartTime, fEndTime, fCurrentTime));
        }

        /// <summary>
        /// Animates a lerp
        /// </summary>
        /// <param name="curve">The curve to use</param>
        /// <param name="fStartValue">The smallest possible value</param>
        /// <param name="fEndValue">The biggest possible value</param>
        /// <param name="fStartTime">The time when the animation started</param>
        /// <param name="fEndTime">The time when the animation will be over</param>
        /// <param name="fCurrentTime">The current time</param>
        /// <returns>The animated current value</returns>
        public static Vector2 AnimatedLerp(this AnimationCurve curve, Vector2 fStartValue, Vector2 fEndValue, float fStartTime, float fEndTime, float fCurrentTime)
        {
            return Vector2.Lerp(fStartValue, fEndValue, curve.InverseLerp(fStartTime, fEndTime, fCurrentTime));
        }

        /// <summary>
        /// Animates a lerp allowed to go below Start Value and above End Value
        /// </summary>
        /// <param name="curve">The curve to use</param>
        /// <param name="fStartValue">The smallest possible value</param>
        /// <param name="fEndValue">The biggest possible value</param>
        /// <param name="fStartTime">The time when the animation started</param>
        /// <param name="fEndTime">The time when the animation will be over</param>
        /// <param name="fCurrentTime">The current time</param>
        /// <returns>The animated current value</returns>
        public static Vector2 AnimatedLerpUnclamped(this AnimationCurve curve, Vector2 fStartValue, Vector2 fEndValue, float fStartTime, float fEndTime, float fCurrentTime)
        {
            return Vector2.LerpUnclamped(fStartValue, fEndValue, curve.InverseLerp(fStartTime, fEndTime, fCurrentTime));
        }

        /// <summary>
        /// Animates a lerp
        /// </summary>
        /// <param name="curve">The curve to use</param>
        /// <param name="fStartValue">The smallest possible value</param>
        /// <param name="fEndValue">The biggest possible value</param>
        /// <param name="fStartTime">The time when the animation started</param>
        /// <param name="fEndTime">The time when the animation will be over</param>
        /// <param name="fCurrentTime">The current time</param>
        /// <returns>The animated current value</returns>
        public static Quaternion AnimatedLerp(this AnimationCurve curve, Quaternion fStartValue, Quaternion fEndValue, float fStartTime, float fEndTime, float fCurrentTime)
        {
            return Quaternion.Lerp(fStartValue, fEndValue, curve.InverseLerp(fStartTime, fEndTime, fCurrentTime));
        }

        /// <summary>
        /// Animates a lerp allowed to go below Start Value and above End Value
        /// </summary>
        /// <param name="curve">The curve to use</param>
        /// <param name="fStartValue">The smallest possible value</param>
        /// <param name="fEndValue">The biggest possible value</param>
        /// <param name="fStartTime">The time when the animation started</param>
        /// <param name="fEndTime">The time when the animation will be over</param>
        /// <param name="fCurrentTime">The current time</param>
        /// <returns>The animated current value</returns>
        public static Quaternion AnimatedLerpUnclamped(this AnimationCurve curve, Quaternion fStartValue, Quaternion fEndValue, float fStartTime, float fEndTime, float fCurrentTime)
        {
            return Quaternion.LerpUnclamped(fStartValue, fEndValue, curve.InverseLerp(fStartTime, fEndTime, fCurrentTime));
        }

        /// <summary>
        /// returns the total duration of the animation
        /// </summary>
        /// <returns>the total duration of the animation</returns>
        public static float GetDuration(this AnimationCurve curve)
        {
            return curve.keys[curve.keys.Length - 1].time;
        }
    }
}