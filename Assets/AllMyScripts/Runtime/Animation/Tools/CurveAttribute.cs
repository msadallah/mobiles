namespace AllMyScripts.Common.Animation
{
    /*!
     * \author Vincent Paquin
     */
    using System;
    using UnityEngine;

    [AttributeUsage(AttributeTargets.Field)]
    public class CurveAttribute : PropertyAttribute
    {
    }
}