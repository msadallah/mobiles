﻿namespace AllMyScripts.Common.Animation
{
    using UnityEngine;

    public class AnimatorCallers : MonoBehaviour
    {
        [System.Serializable]
        public struct BoolCaller
        {
            public string name;
            public bool value;
            public string sParameter;
            [System.NonSerialized] public int nParameter;
        }
        [System.Serializable]
        public struct SwitchCaller
        {
            public string name;
            public string sParameter;
            [System.NonSerialized]
            public int nParameter;
        }
        [System.Serializable]
        public struct FloatCaller
        {
            public string name;
            public float value;
            public string sParameter;
            [System.NonSerialized]
            public int nParameter;
        }
        [System.Serializable]
        public struct TriggerCaller
        {
            public string name;
            public string sParameter;
            [System.NonSerialized]
            public int nParameter;
        }
        [System.Serializable]
        public struct IntCaller
        {
            public string name;
            public int value;
            public string sParameter;
            [System.NonSerialized]
            public int nParameter;
        }

        public FloatCaller[] floats;
        public BoolCaller[] booleans;
        public SwitchCaller[] switches;
        public IntCaller[] integers;
        public TriggerCaller[] triggers;
        private Animator animator;

        private void Awake()
        {
            animator = GetComponent<Animator>();

            for (int i = 0; i < floats.Length; ++i)
            {
                floats[i].nParameter = Animator.StringToHash(floats[i].sParameter);
            }

            for (int i = 0; i < booleans.Length; ++i)
            {
                booleans[i].nParameter = Animator.StringToHash(booleans[i].sParameter);
            }

            for (int i = 0; i < switches.Length; ++i)
            {
                switches[i].nParameter = Animator.StringToHash(switches[i].sParameter);
            }

            for (int i = 0; i < integers.Length; ++i)
            {
                integers[i].nParameter = Animator.StringToHash(integers[i].sParameter);
            }

            for (int i = 0; i < triggers.Length; ++i)
            {
                triggers[i].nParameter = Animator.StringToHash(triggers[i].sParameter);
            }
        }

        public void CallFloat(string sName)
        {
            for (int i = 0; i < floats.Length; ++i)
            {
                if (sName == floats[i].name)
                {
                    animator.SetFloat(floats[i].nParameter, floats[i].value);
                }
            }
        }

        public void CallInteger(string sName)
        {
            for (int i = 0; i < integers.Length; ++i)
            {
                if (sName == integers[i].name)
                {
                    animator.SetInteger(integers[i].nParameter, integers[i].value);
                }
            }
        }

        public void CallBoolean(string sName)
        {
            for (int i = 0; i < booleans.Length; ++i)
            {
                if (sName == booleans[i].name)
                {
                    animator.SetBool(booleans[i].nParameter, booleans[i].value);
                }
            }
        }

        public void CallTrigger(string sName)
        {
            for (int i = 0; i < triggers.Length; ++i)
            {
                if (sName == triggers[i].name)
                {
                    animator.SetTrigger(triggers[i].nParameter);
                }
            }
        }

        public void CallSwitch(string sName)
        {
            for (int i = 0; i < switches.Length; ++i)
            {
                if (sName == switches[i].name)
                {
                    animator.SetBool(switches[i].nParameter, !animator.GetBool(switches[i].nParameter));
                }
            }
        }
    }
}