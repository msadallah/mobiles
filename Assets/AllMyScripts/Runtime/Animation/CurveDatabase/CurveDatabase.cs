namespace AllMyScripts.Common.Animation
{
    /*!
     * \author Vincent Paquin
     */
    using System.Collections.Generic;
    using UnityEngine;

    public static class CurveDatabase
    {
        /// <summary>
        /// http://easings.net/
        /// </summary>
        public enum AnimationCurveEnum
        {
            Linear = 0,
            Swing,

            easeInQuad,
            easeOutQuad,
            easeInOutQuad,

            easeInCubic,
            easeOutCubic,
            easeInOutCubic,

            easeInQuart,
            easeOutQuart,
            easeInOutQuart,

            easeInQuint,
            easeOutQuint,
            easeInOutQuint,

            easeInSine,
            easeOutSine,
            easeInOutSine,

            easeInExpo,
            easeOutExpo,
            easeInOutExpo,

            easeInCirc,
            easeOutCirc,
            easeInOutCirc,

            easeInElastic,
            easeOutElastic,
            easeInOutElastic,

            easeInBack,
            easeOutBack,
            easeInOutBack,

            easeInBounce,
            easeOutBounce,
            easeInOutBounceCalm,
            easeInOutBounceIntense,

        }
        private static Dictionary<AnimationCurveEnum, AnimationCurve> s_AnimationsDic = new Dictionary<AnimationCurveEnum, AnimationCurve>();
#if UNITY_EDITOR
        public static readonly string[] sEditorAnimationPaths = new string[]
        {
        "Linear",
        "Swing",
        "Quad/In",
        "Quad/Out",
        "Quad/InOut",
        "Cubic/In",
        "Cubic/Out",
        "Cubic/InOut",
        "Quart/In",
        "Quart/Out",
        "Quart/InOut",
        "Quint/In",
        "Quint/Out",
        "Quint/InOut",
        "Sine/In",
        "Sine/Out",
        "Sine/InOut",
        "Expo/In",
        "Expo/Out",
        "Expo/InOut",
        "Circ/In",
        "Circ/Out",
        "Circ/InOut",
        "Elastic/In",
        "Elastic/Out",
        "Elastic/InOut",
        "Back/In",
        "Back/Out",
        "Back/InOut",
        "Bounce/In",
        "Bounce/Out",
        "Bounce/InOut Calm",
        "Bounce/InOut Intense"
        };
#endif

        /// <summary>
        /// Use this if you don't need to change the curve afterwards. Gets an animation curve shared by all the classes. 
        /// </summary>
        /// <param name="eAnimation">The animation to load</param>
        /// <returns>The animation curve</returns>
        public static AnimationCurve GetStaticAnimation(AnimationCurveEnum eAnimation)
        {
            AnimationCurve curve = null;
            if (!s_AnimationsDic.TryGetValue(eAnimation, out curve))
            {
                curve = GetNewAnimationInstance(eAnimation);
                s_AnimationsDic.Add(eAnimation, curve);
            }
            return curve;
        }

        /// <summary>
        ///  Instantiates a new curve. Use this if you need to change the curve afterwards, or need a customized duration or max value.
        /// </summary>
        /// <param name="eAnimation">The animation to load</param>
        /// <returns>The animation curve</returns>
        public static AnimationCurve GetNewAnimationInstance(AnimationCurveEnum eAnimation, bool isReversed = false, float fDuration = 1, float fMaxValue = 1)
        {
            Keyframe[] keyFrames;
            switch (eAnimation)
            {
                case AnimationCurveEnum.Linear: keyFrames = new Keyframe[] { new Keyframe(0, 0, 1f, 1f), new Keyframe(1f * fDuration, 1f * fMaxValue, 1f, 1f) }; break;
                case AnimationCurveEnum.Swing: keyFrames = new Keyframe[] { new Keyframe(0, 0, 1.9f, 1.9f), new Keyframe(0.5f * fDuration, 0.75f * fMaxValue, 1, 1), new Keyframe(1 * fDuration, 1 * fMaxValue, 0.1f, 0.1f) }; break;

                case AnimationCurveEnum.easeInQuad: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(0.5f * fDuration, 0.25f * fMaxValue, 1, 1), new Keyframe(1 * fDuration, 1 * fMaxValue, 1.946f, 1.946f) }; break;
                case AnimationCurveEnum.easeOutQuad: keyFrames = new Keyframe[] { new Keyframe(0, 0, 2f, 2f), new Keyframe(0.5f * fDuration, 0.75f * fMaxValue, 1, 1), new Keyframe(1 * fDuration, 1 * fMaxValue, 0, 0) }; break;
                case AnimationCurveEnum.easeInOutQuad: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(0.5f * fDuration, 0.5f * fMaxValue, 2f, 2f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0, 0) }; break;

                case AnimationCurveEnum.easeInCubic: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0.01f, 0.01f), new Keyframe(0.6f * fDuration, 0.216f * fMaxValue, 1.09f, 1.09f), new Keyframe(1 * fDuration, 1 * fMaxValue, 2.955f, 2.955f) }; break;
                case AnimationCurveEnum.easeOutCubic: keyFrames = new Keyframe[] { new Keyframe(0, 0, 2.936f, 2.936f), new Keyframe(0.4f * fDuration, 0.784f * fMaxValue, 1.09f, 1.09f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0.01f, 0.01f) }; break;
                case AnimationCurveEnum.easeInOutCubic: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(0.2f * fDuration, 0.032f * fMaxValue, 0.52f, 0.52f), new Keyframe(0.4f * fDuration, 0.256f * fMaxValue, 1.96f, 1.96f), new Keyframe(0.5f * fDuration, 0.5f * fMaxValue, 2.44f, 2.44f), new Keyframe(0.6f * fDuration, 0.744f * fMaxValue, 1.96f, 1.96f), new Keyframe(0.8f * fDuration, 0.968f * fMaxValue, 0.52f, 0.52f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0, 0) }; break;

                case AnimationCurveEnum.easeInQuart: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0.001f, 0.001f), new Keyframe(0.2f * fDuration, 0.002f * fMaxValue, 0.04f, 0.04f), new Keyframe(0.6f * fDuration, 0.13f * fMaxValue, 0.888f, 0.888f), new Keyframe(1 * fDuration, 1 * fMaxValue, 3.894f, 3.894f) }; break;
                case AnimationCurveEnum.easeOutQuart: keyFrames = new Keyframe[] { new Keyframe(0, 0, 3.799f, 3.799f), new Keyframe(0.3f * fDuration, 0.76f * fMaxValue, 1.4f, 1.4f), new Keyframe(0.5f * fDuration, 0.938f * fMaxValue, 0.52f, 0.52f), new Keyframe(0.8f * fDuration, 0.998f * fMaxValue, 0.04f, 0.04f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0, 0) }; break;
                case AnimationCurveEnum.easeInOutQuart: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(0.2f * fDuration, 0.013f * fMaxValue, 0.32f, 0.32f), new Keyframe(0.4f * fDuration, 0.205f * fMaxValue, 2.176f, 2.176f), new Keyframe(0.5f * fDuration, 0.5f * fMaxValue, 2.952f, 2.952f), new Keyframe(0.6f * fDuration, 0.795f * fMaxValue, 2.176f, 2.176f), new Keyframe(0.8f * fDuration, 0.987f * fMaxValue, 0.32f, 0.32f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0, 0) }; break;


                case AnimationCurveEnum.easeInQuint: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(0.3f * fDuration, 0.002f * fMaxValue, 0, 0), new Keyframe(0.4f * fDuration, 0.01f * fMaxValue, 0.144f, 0.144f), new Keyframe(0.7f * fDuration, 0.168f * fMaxValue, 1.25f, 1.25f), new Keyframe(1 * fDuration, 1 * fMaxValue, 5.129f, 5.129f) }; break;
                case AnimationCurveEnum.easeOutQuint: keyFrames = new Keyframe[] { new Keyframe(0, 0, 4.859f, 4.859f), new Keyframe(0.4f * fDuration, 0.922f * fMaxValue, 0.628f, 0.628f), new Keyframe(0.7f * fDuration, 0.998f * fMaxValue, 0, 0), new Keyframe(1 * fDuration, 1 * fMaxValue, 0, 0) }; break;
                case AnimationCurveEnum.easeInOutQuint: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(0.2f * fDuration, 0.005f * fMaxValue, 0, 0), new Keyframe(0.5f * fDuration, 0.5f * fMaxValue, 4.581f, 4.581f), new Keyframe(0.8f * fDuration, 0.995f * fMaxValue, 0, 0), new Keyframe(1 * fDuration, 1 * fMaxValue, 0, 0) }; break;

                case AnimationCurveEnum.easeInSine: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1 * fDuration, 1 * fMaxValue, 2, 2) }; break;
                case AnimationCurveEnum.easeOutSine: keyFrames = new Keyframe[] { new Keyframe(0, 0, 2, 2), new Keyframe(1 * fDuration, 1 * fMaxValue, 0, 0) }; break;
                case AnimationCurveEnum.easeInOutSine: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1 * fDuration, 1 * fMaxValue, 0, 0) }; break;

                case AnimationCurveEnum.easeInExpo: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0.032f, 0.032f), new Keyframe(0.16f * fDuration, 0.003f * fMaxValue, 0.021f, 0.021f), new Keyframe(0.4f * fDuration, 0.016f * fMaxValue, 0.11f, 0.11f), new Keyframe(0.6f * fDuration, 0.063f * fMaxValue, 0.412f, 0.412f), new Keyframe(0.8f * fDuration, 0.25f * fMaxValue, 1.755f, 1.755f), new Keyframe(1 * fDuration, 1 * fMaxValue, 6.743f, 6.743f) }; break;
                case AnimationCurveEnum.easeOutExpo: keyFrames = new Keyframe[] { new Keyframe(0, 0, 6.136f, 6.136f), new Keyframe(0.16f * fDuration, 0.67f * fMaxValue, 2.316f, 2.316f), new Keyframe(0.36f * fDuration, 0.918f * fMaxValue, 0.579f, 0.579f), new Keyframe(0.6f * fDuration, 0.984f * fMaxValue, 0.11f, 0.11f), new Keyframe(0.84f * fDuration, 0.997f * fMaxValue, 0, 0), new Keyframe(1 * fDuration, 0.999f * fMaxValue, 0, 0) }; break;
                case AnimationCurveEnum.easeInOutExpo: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(0.2f * fDuration, 0.008f * fMaxValue, 0.114f, 0.114f), new Keyframe(0.36f * fDuration, 0.072f * fMaxValue, 1.047f, 1.047f), new Keyframe(0.5f * fDuration, 0.5f * fMaxValue, 6.551f, 6.551f), new Keyframe(0.64f * fDuration, 0.928f * fMaxValue, 1.047f, 1.047f), new Keyframe(0.8f * fDuration, 0.992f * fMaxValue, 0.114f, 0.114f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0.009f, 0.009f) }; break;

                case AnimationCurveEnum.easeInCirc: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(0.302f * fDuration, 0.046f * fMaxValue, 0.298f, 0.298f), new Keyframe(0.5f * fDuration, 0.134f * fMaxValue, 0.628f, 0.628f), new Keyframe(0.9f * fDuration, 0.564f * fMaxValue, 2.01f, 2.01f), new Keyframe(0.99f * fDuration, 0.9f * fMaxValue, 7.07f, 7.07f), new Keyframe(1 * fDuration, 1 * fMaxValue, 10, 10) }; break;
                case AnimationCurveEnum.easeOutCirc: keyFrames = new Keyframe[] { new Keyframe(0, 0, 14.107f, 14.107f), new Keyframe(0.01f * fDuration, 0.141f * fMaxValue, 4.269f, 4.269f), new Keyframe(0.1f * fDuration, 0.436f * fMaxValue, 2.068f, 2.068f), new Keyframe(0.2f * fDuration, 0.6f * fMaxValue, 1.258f, 1.258f), new Keyframe(0.8f * fDuration, 0.98f * fMaxValue, 0.204f, 0.204f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0, 0) }; break;
                case AnimationCurveEnum.easeInOutCirc: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0.01f, 0.01f), new Keyframe(0.25f * fDuration, 0.067f * fMaxValue, 0.578f, 0.578f), new Keyframe(0.35f * fDuration, 0.143f * fMaxValue, 1.08f, 1.08f), new Keyframe(0.449f * fDuration, 0.282f * fMaxValue, 2.135f, 2.135f), new Keyframe(0.5f * fDuration, 0.5f * fMaxValue, 8.762f, 8.762f), new Keyframe(0.548f * fDuration, 0.718f * fMaxValue, 1.827f, 1.827f), new Keyframe(0.65f * fDuration, 0.857f * fMaxValue, 0.959f, 0.959f), new Keyframe(0.75f * fDuration, 0.933f * fMaxValue, 0.578f, 0.578f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0.01f, 0.01f) }; break;

                case AnimationCurveEnum.easeInElastic: keyFrames = new Keyframe[] { new Keyframe(0, 0, -0.032f, -0.032f), new Keyframe(0.07f * fDuration, 0.001f * fMaxValue, 0.028f, 0.028f), new Keyframe(0.15f * fDuration, 0.001f * fMaxValue, -0.04f, -0.04f), new Keyframe(0.28f * fDuration, -0.006f * fMaxValue, 0.046f, 0.046f), new Keyframe(0.42f * fDuration, 0.016f * fMaxValue, -0.041f, -0.041f), new Keyframe(0.57f * fDuration, -0.046f * fMaxValue, 0.116f, 0.116f), new Keyframe(0.72f * fDuration, 0.131f * fMaxValue, -0.327f, -0.327f), new Keyframe(0.87f * fDuration, -0.371f * fMaxValue, 0.925f, 0.925f), new Keyframe(1 * fDuration, 1 * fMaxValue, 8.736f, 8.736f) }; break;
                case AnimationCurveEnum.easeOutElastic: keyFrames = new Keyframe[] { new Keyframe(0, 0, 8.736f, 8.736f), new Keyframe(0.13f * fDuration, 1.371f * fMaxValue, 0.925f, 0.925f), new Keyframe(0.29f * fDuration, 0.869f * fMaxValue, 0.309f, 0.309f), new Keyframe(0.43f * fDuration, 1.046f * fMaxValue, 0.116f, 0.116f), new Keyframe(0.58f * fDuration, 0.984f * fMaxValue, -0.041f, -0.041f), new Keyframe(0.72f * fDuration, 1.006f * fMaxValue, 0.046f, 0.046f), new Keyframe(0.85f * fDuration, 0.999f * fMaxValue, -0.04f, -0.04f), new Keyframe(0.92f * fDuration, 0.998f * fMaxValue, 0.025f, 0.025f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0.016f, 0.016f) }; break;
                case AnimationCurveEnum.easeInOutElastic: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0.025f, 0.025f), new Keyframe(0.1f * fDuration, 0, -0.049f, -0.049f), new Keyframe(0.19f * fDuration, -0.005f * fMaxValue, 0.066f, 0.066f), new Keyframe(0.29f * fDuration, 0.025f * fMaxValue, 0.024f, 0.024f), new Keyframe(0.4f * fDuration, -0.117f * fMaxValue, -0.381f, -0.381f), new Keyframe(0.5f * fDuration, 0.5f * fMaxValue, 8.159f, 8.159f), new Keyframe(0.6f * fDuration, 1.117f * fMaxValue, -0.381f, -0.381f), new Keyframe(0.71f * fDuration, 0.975f * fMaxValue, 0.024f, 0.024f), new Keyframe(0.81f * fDuration, 1.005f * fMaxValue, 0.066f, 0.066f), new Keyframe(0.9f * fDuration, 1 * fMaxValue, -0.049f, -0.049f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0.016f, 0.016f) }; break;

                case AnimationCurveEnum.easeInBack: keyFrames = new Keyframe[] { new Keyframe(0, 0, -0.017f, -0.017f), new Keyframe(0.42f * fDuration, -0.1f * fMaxValue, 0.001f, 0.001f), new Keyframe(1 * fDuration, 1 * fMaxValue, 4.638f, 4.638f) }; break;
                case AnimationCurveEnum.easeOutBack: keyFrames = new Keyframe[] { new Keyframe(0, 0, 4.638f, 4.638f), new Keyframe(0.58f * fDuration, 1.1f * fMaxValue, 0.001f, 0.001f), new Keyframe(1 * fDuration, 1 * fMaxValue, -0.017f, -0.017f) }; break;
                case AnimationCurveEnum.easeInOutBack: keyFrames = new Keyframe[] { new Keyframe(0, 0, -0.05f, -0.05f), new Keyframe(0.24f * fDuration, -0.1f * fMaxValue, -0.005f, -0.005f), new Keyframe(0.5f * fDuration, 0.5f * fMaxValue, 5.433f, 5.433f), new Keyframe(0.76f * fDuration, 1.1f * fMaxValue, -0.005f, -0.005f), new Keyframe(1 * fDuration, 1 * fMaxValue, -0.05f, -0.05f) }; break;

                case AnimationCurveEnum.easeInBounce: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0.612f, 0.612f), new Keyframe(0.09f * fDuration, 0, -0.718f, 1.618f), new Keyframe(0.27f * fDuration, 0, -1.354f, 2.867f), new Keyframe(0.635f * fDuration, 0, -2.722f, 5.346f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0.076f, 0.076f) }; break;
                case AnimationCurveEnum.easeOutBounce: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0.038f, 0.038f), new Keyframe(0.005f * fDuration, 0, 0.076f, 0.076f), new Keyframe(0.365f * fDuration, 1 * fMaxValue, 5.65f, -2.776f), new Keyframe(0.73f * fDuration, 0.996f * fMaxValue, 2.651f, -1.285f), new Keyframe(0.91f * fDuration, 1 * fMaxValue, 1.351f, -0.636f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0.587f, 0.65f) }; break;
                case AnimationCurveEnum.easeInOutBounceCalm: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0.834f), new Keyframe(0.05f * fDuration, 0, -0.597f, 1.269f), new Keyframe(0.135f * fDuration, 0, -1.104f, 2.628f), new Keyframe(0.31f * fDuration, 0, -2.851f, 7.018f), new Keyframe(0.5f * fDuration, 0.5f * fMaxValue, 0, 0), new Keyframe(0.69f * fDuration, 1 * fMaxValue, 5.985f, -2.626f), new Keyframe(0.875f * fDuration, 1 * fMaxValue, 2.55f, -1.035f), new Keyframe(0.95f * fDuration, 1 * fMaxValue, 0.974f, -0.536f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0.528f, 0) }; break;
                case AnimationCurveEnum.easeInOutBounceIntense: keyFrames = new Keyframe[] { new Keyframe(0, 0, 0, 0.834f), new Keyframe(0.05f * fDuration, 0, -0.597f, 1.269f), new Keyframe(0.135f * fDuration, 0, -1.104f, 2.628f), new Keyframe(0.31f * fDuration, 0, -2.851f, 4.498f), new Keyframe(0.69f * fDuration, 1 * fMaxValue, 4.449f, -2.626f), new Keyframe(0.875f * fDuration, 1 * fMaxValue, 2.55f, -1.035f), new Keyframe(0.95f * fDuration, 1 * fMaxValue, 0.974f, -0.536f), new Keyframe(1 * fDuration, 1 * fMaxValue, 0.528f, 0) }; break;

                default:
                    {
                        keyFrames = new Keyframe[0];
                    }
                    break;
            }

            if (isReversed)
            {
                for (int keyframeIndex = 0; keyframeIndex < keyFrames.Length; ++keyframeIndex)
                {
                    keyFrames[keyframeIndex].time = fDuration - keyFrames[keyframeIndex].time;
                    keyFrames[keyframeIndex].inTangent = -keyFrames[keyframeIndex].inTangent;
                    keyFrames[keyframeIndex].outTangent = -keyFrames[keyframeIndex].outTangent;
                }
            }

            return new AnimationCurve(keyFrames);
        }
#if UNITY_EDITOR
        public static void DebugConstructor(AnimationCurve curve)
        {
            string sResult = "return new AnimationCurve( ";
            for (int i = 0; i < curve.keys.Length; ++i)
            {
                Keyframe keyframe = curve.keys[i];
                sResult += "new Keyframe(" + keyframe.time.ToString("F3") + "f*fDuration, " + keyframe.value.ToString("F3") + "f*fMaxValue, " + keyframe.inTangent.ToString("F3") + "f, " + keyframe.outTangent.ToString("F3") + "f)" + (i == curve.keys.Length - 1 ? "" : ",");
            }
            sResult += ");";
            GUIUtility.systemCopyBuffer = sResult;
            Debug.Log(sResult + "\nAdded to clipboard");
        }

#endif
    }
}