﻿namespace AllMyScripts.Common.Tools
{
	using UnityEngine;

	public class DirtyRunner
	{
		private System.Action _onDirty = null;
		private bool _isDirty = false;

		public DirtyRunner(System.Action onDirtyCallback, bool autoRegister = true)
		{
			_onDirty = onDirtyCallback;

			if (autoRegister)
			{
				if (DirtyRunnerManager.instance == null)
				{
					GameObject go = new GameObject("DirtyRunnerManager");
					go.AddComponent<DirtyRunnerManager>();
				}

				Register();
			}
		}

		public void Register()
		{
			DirtyRunnerManager.instance.Register(this);
		}

		public void Unregister()
		{
			DirtyRunnerManager.instance.Unregister(this);
		}

		public void CheckDirty()
		{
			if (_isDirty)
			{
				_onDirty?.Invoke();
				_isDirty = false;
			}
		}

		public void SetDirty()
		{
			_isDirty = true;
		}

	}

	/*
	public class ExempleImplementaion_DirtyRunner : UnityEngine.MonoBehaviour
	{
		// Declare your dirty runner in a variable
		// Like this, you can have multiple DirtyRunner in one MonoBehaviour class
		private DirtyRunner _dr = null;

		// Create the instance of your DirtyRunner specifying the callback to call when it's dirty
		private void Awake()
		{
			_dr = new DirtyRunner(OnDirtyCallback);
		}

		// Frequency of the runner checking : Not needed if you use autoRegister
		// Can be called in a FixedUpdate, Update, LateUpdate or what you want/need
		private void Update()
		{
			_dr.CheckDirty();
		}

		// Create a method to update something who will simply call the SetDirty method of the Runner.
		// It's more readable a call yourScript.UpdateScrollRectHeight(),
		// than yourScript.dirtyRunnerUpdateScrollRectHeight.SetDirty() from another class or whatever
		// You can use a naming convention like DR_UpdateSomething() to remind you that the update will be call in a DirtyRunner 
		public void UpdateSomething()
		{
			_dr.SetDirty();
		}

		// Create a callback method wich will be called by the DirtyRunner,
		// and do what you have to do in it.
		private void OnDirtyCallback()
		{
			UnityEngine.Debug.Log("Update what you have to update only once per frame :dab:");
		}
	}
	*/
}