﻿namespace AllMyScripts.Common.Tools
{
	using UnityEngine;
	using System.Collections.Generic;
	
	public class DirtyRunnerManager : MonoBehaviour
	{
		public static DirtyRunnerManager instance = null;

		private List<DirtyRunner> _runners = new List<DirtyRunner>();

		private void Awake()
		{
			instance = this;
		}

		private void Update()
		{
			foreach (DirtyRunner runner in _runners)
				runner.CheckDirty();
		}

		public void Register(DirtyRunner runner)
		{
			if (!_runners.Contains(runner))
				_runners.Add(runner);
		}

		public void Unregister(DirtyRunner runner)
		{
			if (_runners.Contains(runner))
				_runners.Remove(runner);
		}
	}
}