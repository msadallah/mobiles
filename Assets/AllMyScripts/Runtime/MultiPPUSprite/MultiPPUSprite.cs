﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
#endif
using UnityEngine;

[CreateAssetMenu(menuName ="Sprite Variants")]
public class MultiPPUSprite : ScriptableObject
{
	public Sprite originalSprite;
	[System.Serializable]
	public class PPUSprite
	{
		public int ppu;
		[HideInInspector] public Sprite sprite;
		public string sName = "";
	}
	public PPUSprite[] sprites;

#if UNITY_EDITOR
	public void OnValidate()
	{
		if (name == "" || sprites==null) return;
		AssetDatabase.StartAssetEditing();
		//prepare all sprites
		for (int i=0;i< sprites.Length;++i)
		{
			//force the ppu above 1
			sprites[i].ppu=Mathf.Max(sprites[i].ppu, 1);
			
			bool exists = sprites[i].sprite != null;

			//we create a new sprite. If there is already one, we copy the new sprite in the old one,
			//otherwise we save it.
			//we can't modify the currently existing sprite as some values (such as pixelsperunit) are in readonly
			Sprite newSprite = Sprite.Create(originalSprite.texture, originalSprite.textureRect, originalSprite.pivot, sprites[i].ppu*0.5f/*multiply to match spriteImporter's PPU*/, 0, SpriteMeshType.Tight, originalSprite.border);

			if (exists)
				EditorUtility.CopySerialized(newSprite, sprites[i].sprite);
			else
			{
				sprites[i].sprite = newSprite;
				sprites[i].sName = originalSprite.name + " Variant";
			}

			sprites[i].sprite.name = sprites[i].sName;

			if (!exists)
			{
				AssetDatabase.AddObjectToAsset(sprites[i].sprite, this);
			}
			else
			{
				EditorUtility.SetDirty(this);
			}
		}


		//remove the unused sprites
		Object[] objSprites = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(this));
		foreach(Object obj in objSprites)
		{
			if(obj is Sprite)
			{
				bool bUsed = false;
				foreach(var v in this.sprites)
				{
					if(v.sprite == (Sprite)obj)
					{
						bUsed = true;
						break;
					}
				}
				if(!bUsed)
				{
					AssetDatabase.RemoveObjectFromAsset(obj);
				}
			}
		}
		
		AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(this));
		AssetDatabase.StopAssetEditing();
	}
#endif
}
