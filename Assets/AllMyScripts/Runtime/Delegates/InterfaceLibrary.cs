namespace AllMyScripts.Common.Tools
{
    using UnityEngine;
    using System.Collections.Generic;

    #region Actions : Acts and doesn't return anything
    public interface IAction { void OnInterfaceCall(); }
    public interface IAction<T> { void OnInterfaceCall(T param); }
    public interface IAction<T1, T2> { void OnInterfaceCall(T1 param1, T2 param2); }
    public interface IAction<T1, T2, T3> { void OnInterfaceCall(T1 param1, T2 param2, T3 param3); }
    public interface IAction<T1, T2, T3, T4> { void OnInterfaceCall(T1 param1, T2 param2, T3 param3, T4 param4); }
    #endregion

    #region Functions : Acts and returns something
    public interface IFunc<TReturn> { TReturn OnInterfaceCall(); }
    public interface IFunc<TReturn, T> { TReturn OnInterfaceCall(T param); }
    public interface IFunc<TReturn, T1, T2> { TReturn OnInterfaceCall(T1 param1, T2 param2); }
    public interface IFunc<TReturn, T1, T2, T3> { TReturn OnInterfaceCall(T1 param1, T2 param2, T3 param3); }
    public interface IFunc<TReturn, T1, T2, T3, T4> { TReturn OnInterfaceCall(T1 param1, T2 param2, T3 param3, T4 param4); }
    #endregion

    /// <summary>
    /// Class used to manage a listener list
    /// </summary>
    /// <typeparam name="T">type of the listener</typeparam>
    public class InterfaceDelegate<T>
    {
        private List<T> delegates;
#if UNITY_EDITOR
        private List<string> m_sDebugList = null;
#endif

        public int Count
        {
            get
            {
                if (delegates == null) return 0;
                return delegates.Count;
            }
        }

        public void AddListener(T obj)
        {
			if (obj == null) return;
            if (delegates == null)
            {
                delegates = new List<T>();
#if UNITY_EDITOR
                m_sDebugList = new List<string>();
#endif
            }
            if (!delegates.Contains(obj))
            {
                delegates.Add(obj);
#if UNITY_EDITOR
                m_sDebugList.Add(obj.ToString());
#endif
            }
        }

        public void RemoveListener(T obj)
		{
			if (obj == null) return;
			if (delegates != null && delegates.Contains(obj))
            {
#if UNITY_EDITOR
                int nIdx = delegates.IndexOf(obj);
                m_sDebugList.RemoveAt(nIdx);
#endif
                delegates.Remove(obj);
            }
        }

        public void Clear()
        {
#if UNITY_EDITOR
            if (m_sDebugList != null)
                m_sDebugList.Clear();
#endif
            if (delegates != null)
                delegates.Clear();
        }

        public T this[int key]
        {
            get
            {
                return delegates[key];
            }
        }


        ~InterfaceDelegate()
        {
            int nCount = Count;
            if (nCount > 0 && Application.isPlaying)
            {
                Clear();
            }
        }
    }
}