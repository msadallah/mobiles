﻿using UnityEngine;

namespace AllMyScripts.CommandPattern
{
	public abstract class BaseCommand
	{
		public int nCommandId = -1;
		protected string commandWatcherID;
		public BaseCommand(string commandWatcherID)
		{
			this.commandWatcherID = commandWatcherID;
		}

		/// <summary>
		/// returns the name of the command, for menu displaying
		/// </summary>
		/// <returns></returns>
		public abstract string GetName();

		/// <summary>
		/// Executes the command but does NOT register it to the command watcher
		/// </summary>
		/// <returns>true if command was correctly executed, false otherwise</returns>
		public abstract bool Do(bool isRedo=false);

		/// <summary>
		/// undoes the command
		/// </summary>
		public abstract void Undo();

		/// <summary>
		/// Executes the command and registers it to the command watcher
		/// </summary>
		/// <returns>true if command was correctly executed and added to the watcher, false otherwise</returns>
		public bool Execute()
		{
			bool result = false;
			try
			{
				result = Do();
			}
			catch (System.Exception e)
			{
				//the command failed to execute
				Debug.LogError(e);
			}

			if(result)
			{
				CommandWatcher.Get(commandWatcherID)?.OnCommandExecuted(this);
				return true;
			}
			return false;
		}

		public virtual bool CanUndo()
		{
			return true;
		}
		public virtual bool CanRedo()
		{
			return true;
		}

		/// <summary>
		/// called when reference to the command is about to be dropped
		/// </summary>
		public virtual void OnRemovedFromHistory()
		{
		}

		public virtual string DebugInfos()
		{
			return GetType().Name;
		}
	}
}