﻿namespace AllMyScripts.CommandPattern
{
	using System.Collections.Generic;

	using UnityEngine;

	using AllMyScripts.Common.Tools;
#if LOG_PLANCREATOR
	using System.IO;
    using System.Reflection;
#endif
	using System;
	public class CommandWatcher
	{

		public interface ICommandExecuted { void HasDoneCommand(); }

		public InterfaceDelegate<ICommandExecuted> IDCommandExecuted = new InterfaceDelegate<ICommandExecuted>();

		//#me
		private static Dictionary<string,CommandWatcher> _Instances = new Dictionary<string, CommandWatcher>();

		/// <summary>
		/// returns an instance of a command watcher
		/// </summary>
		/// <param name="uID">the ID of the commandwatcher. Create one using Allocate</param>
		public static CommandWatcher Get(string uID)
		{
			if (!_Instances.ContainsKey(uID)) return null;
			return _Instances[uID];
		}

		/// <summary>
		/// creates a new command watcher that can be fetched using uID. Call Free to remove when not used anymore
		/// </summary>
		/// <param name="uID">the id of the new commandWatcher</param>
		public static CommandWatcher Allocate(string uID)
		{

			if (_Instances.ContainsKey(uID)) return _Instances[uID];
			_Instances.Add(uID, new CommandWatcher());
			return _Instances[uID];
		}

		/// <summary>
		/// frees the allocated commandwatcher
		/// </summary>
		public static void Free(string uID)
		{
			if (_Instances.ContainsKey(uID))
			{
				_Instances.Remove(uID);
			}
			else
			{
				Debug.LogError("CommandWatcher " + uID + " doesn't exist");
			}
		}


		private CommandWatcher() { }
		/// <summary>
		/// List of the current history of commands
		/// </summary>
		private List<BaseCommand> commands = new List<BaseCommand>();

		/// <summary>
		/// the current command in the list, we change this by using redo and undo
		/// </summary>
		private int nCurrentCommandIndex = -1;
				
		/// <summary>
		/// called when a command is executed
		/// </summary>
		/// <param name="command"></param>
		internal void OnCommandExecuted(BaseCommand command)
		{
#if LOG_PLANCREATOR
			AddToDebug(command);
#endif
			command.nCommandId = nCurrentCommandIndex;
			nCurrentCommandIndex++;

			//a command was executed, remove all the "redo" list
			while (nCurrentCommandIndex <= commands.Count - 1)
			{
				commands.RemoveAt(nCurrentCommandIndex);
			}

			//add the command
			commands.Add(command);

			//update the current
			nCurrentCommandIndex = commands.Count - 1;

			for( int i = 0; i < IDCommandExecuted.Count; i++ )
				IDCommandExecuted[i].HasDoneCommand();
		}

		public void AddListener(ICommandExecuted commandExecutedInterface)
		{
			IDCommandExecuted.AddListener(commandExecutedInterface);
		}

		public void RemoveListener(ICommandExecuted commandExecutedInterface)
		{
			IDCommandExecuted.RemoveListener(commandExecutedInterface);
		}

		public bool CanUndo()
		{
			if (nCurrentCommandIndex > -1)
			{
				return commands[nCurrentCommandIndex].CanUndo();
			}
			return false;
		}

		/// <summary>
		/// returns the name of the command to undo if Undo is called
		/// </summary>
		public string GetUndoName()
		{
			return commands[nCurrentCommandIndex].GetName();
		}

		public void Undo()
		{
			if (CanUndo())
			{
#if LOG_PLANCREATOR
				AddUndoToDebug();
#endif
				commands[nCurrentCommandIndex].Undo();
				nCurrentCommandIndex--;

				for (int i = 0; i < IDCommandExecuted.Count; i++)
					IDCommandExecuted[i].HasDoneCommand();
			}
		}
	
		public bool CanRedo()
		{
			if (nCurrentCommandIndex < commands.Count - 1)
			{
				return commands[nCurrentCommandIndex+1].CanRedo();
			}
			return false;
		}

		/// <summary>
		/// returns the name of the function to redo if Redo is called
		/// </summary>
		public string GetRedoName()
		{
			return commands[nCurrentCommandIndex + 1].GetName();
		}

		public void Redo()
		{
			if (CanRedo())
			{
#if LOG_PLANCREATOR
				AddRedoToDebug();
#endif
				nCurrentCommandIndex++;
				commands[nCurrentCommandIndex].Do(true);

				for (int i = 0; i < IDCommandExecuted.Count; i++)
					IDCommandExecuted[i].HasDoneCommand();
			}
		}

		public void ClearCommand( int nCommandId, bool triggerDelegate = true )
		{
			for (int i = 0; i < commands.Count; i++)
			{
				if(commands[i].nCommandId == nCommandId )
				{
					commands.RemoveAt(i);
					if (i <= nCurrentCommandIndex)
						nCurrentCommandIndex--;
					break;
				}
			}

			for (int i = 0; i < IDCommandExecuted.Count; i++)
				IDCommandExecuted[i].HasDoneCommand();
		}

		public void ClearCommands( int[] commandIdList )
		{
			for (int i = 0; i < commandIdList.Length; i++)
				ClearCommand(commandIdList[i], false);

			for (int i = 0; i < IDCommandExecuted.Count; i++)
				IDCommandExecuted[i].HasDoneCommand();
		}

		public void ClearHistory()
		{	
			foreach(BaseCommand command in commands)
			{
				command.OnRemovedFromHistory();
			}
			commands.Clear();
			nCurrentCommandIndex = -1;

			for (int i = 0; i < IDCommandExecuted.Count; i++)
				IDCommandExecuted[i].HasDoneCommand();
		}

		public int GetCommandsCount()
		{
			return commands.Count;
		}


#if LOG_PLANCREATOR
		private string _debugFileLocation = null;
		private readonly int MAX_LOG_FILES = 5;
		public bool writeDebugFile = false;
		private void ComputeFileLocation()
		{
			if(_debugFileLocation == null)
			{
				string directory = Application.persistentDataPath + "/CommandWatcherLogs/";

				//remove the oldest log
				if (File.Exists(directory + "log"+ MAX_LOG_FILES+".txt"))
				{
					File.Delete(directory + "log" + MAX_LOG_FILES + ".txt");
				}

				//move each log file one above
				for (int i = MAX_LOG_FILES - 1; i > 0; --i)
				{
					if (File.Exists(directory + "log"+i+".txt"))
					{
						File.Move(directory + "log" + i + ".txt", directory + "log" + (i+1) + ".txt");
					}
				}
				//move the most recent log above
				if (File.Exists(directory + "log.txt"))
				{
					File.Move(directory + "log.txt", directory + "log1.txt");
				}

				_debugFileLocation = directory + "log.txt";
			}
		}
		private void AddToDebug(BaseCommand doneCommand)
		{
			AddToDebug(Serialize(doneCommand)+"\n");
		}
		private void AddUndoToDebug()
		{
			AddToDebug("Undo\n");
		}
		private void AddRedoToDebug()
		{
			AddToDebug("Redo\n");
		}

		private void AddToDebug(string eventDescription)
		{
			if (!writeDebugFile)
				return;
			ComputeFileLocation();
			Directory.CreateDirectory(Path.GetDirectoryName(_debugFileLocation));

			if (!File.Exists(_debugFileLocation))
			{
				// Create a file to write to.
				using (StreamWriter sw = File.CreateText(_debugFileLocation))
				{
					sw.WriteLine(eventDescription);
				}
			}
			else
			{
				// This text is always added, making the file longer over time
				// if it is not deleted.
				using (StreamWriter sw = File.AppendText(_debugFileLocation))
				{
					sw.WriteLine(eventDescription);
				}
			}
		}

		private string Serialize(BaseCommand doneCommand)
		{
			return doneCommand.DebugInfos();
		}
#endif
	}
}
