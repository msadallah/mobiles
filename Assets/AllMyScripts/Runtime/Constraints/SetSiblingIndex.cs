﻿namespace AllMyScripts.Common.Tools
{
    using UnityEngine;

    [ExecuteAlways]
    /// <summary>
    /// If you need documentation on this object then you're likely a beginner in unity
    /// </summary>
    public class SetSiblingIndex : MonoBehaviour
    {
        /// <summary>
        /// Oh, right. The index. The index for the object. The index chosen especially to set to the object. The object's index. That index?
        /// </summary>
        public bool alwaysFirst;

        public bool alwaysLast;

        public int index;

#if UNITY_EDITOR
        private bool m_bStarted = false;
#endif

        private bool UseIndex()
        {
            return !alwaysFirst && !alwaysLast;
        }

#if UNITY_EDITOR

        private void Start()
        {
            m_bStarted = true;
            SetIndex();
        }

        void Update()
        {
            if (!Application.isPlaying)
            {
                SetIndex();
            }
        }
#endif

        private void OnValidate()
        {
            SetIndex();
        }

        private void OnEnable()
        {
            SetIndex();
        }

        public void SetIndex()
        {
#if UNITY_EDITOR
            if (!m_bStarted) return;
#endif

            if (UseIndex())
            {
                transform.SetSiblingIndex(index);
            }
            else
            {
                if (alwaysFirst) transform.SetAsFirstSibling();
                else transform.SetAsLastSibling();
            }
        }
    }
}