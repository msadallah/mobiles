﻿namespace AllMyScripts.Common.Tools
{
    using UnityEngine;

    [DefaultExecutionOrder(5000)]
    public class CopyRotation : MonoBehaviour
    {
        public Transform target;
        public bool copyX;
        public bool copyY;
        public bool copyZ;
        public Vector3 offset;

        // Update is called once per frame
        void LateUpdate()
        {
            if (target != null)
            {
                transform.rotation = Quaternion.Euler(new Vector3
                ((copyX ? (target.rotation.eulerAngles.x + offset.x) : transform.rotation.eulerAngles.x),
                (copyY ? (target.rotation.eulerAngles.y + offset.y) : transform.rotation.eulerAngles.y),
                (copyZ ? (target.rotation.eulerAngles.z + offset.z) : transform.rotation.eulerAngles.z)
                ));
            }
        }
    }
}