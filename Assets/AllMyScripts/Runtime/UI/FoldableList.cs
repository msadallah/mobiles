﻿namespace AllMyScripts.Common.UI
{
	using System.Collections;
	using System.Collections.Generic;
    using AllMyScripts.Common.Animation;
    using AllMyScripts.Common.Tools;
    using UnityEngine;
	using UnityEngine.UI;
#if ODIN_INSPECTOR
	using Sirenix.OdinInspector;
#endif

	/// <summary>
	/// class that allows a LayoutElement to own a list and extend/reduce its size based on that list
	/// </summary>
	[RequireComponent(typeof(LayoutElement))]
	public class FoldableList : MonoBehaviour
	{
		public const float FOLD_ANIMATION_SPEED_OPEN = 0.50f;
		public const float FOLD_ANIMATION_SPEED_CLOSE = 0.40f;

		public interface IListener
		{
			void OnPreFoldAnimation(FoldableList list, bool opening);
			void OnPostFoldAnimation(FoldableList list, bool open);
		}

		private float _foldedHeight;
		private LayoutElement _layoutElement;
		private bool _folded = true;
		private bool _fullyOpen = false;
		private Coroutine _foldingCoroutine;

		public InterfaceDelegate<IListener> listeners = new InterfaceDelegate<IListener>();

		public bool disableContainerWhenClosed = true;

		/// <summary>
		/// the container that will be resized
		/// </summary>
		[Header("References")]
		[Tooltip("the container that will be resized")]
#if ODIN_INSPECTOR
		[Required]
#endif
		public RectTransform listContainer;
		/// <summary>
		/// the mask that will be resized
		/// </summary>
		[Tooltip("the mask that will be resized")]
		public RectTransform listMask;

		/// <summary>
		/// is the list open/opening?
		/// </summary>
		public bool isOpen { get { return !_folded; } }

		/// <summary>
		/// should we add some additional space when unfolding?
		/// </summary>
		[Tooltip("should we add some additional space when unfolding?")]
		public float unfoldedAdditionalHeight=0;

		/// <summary>
		/// the background to recolor
		/// </summary>
		[Tooltip("the background to recolor")]
		public Image bkg;

		/// <summary>
		/// The multiplicator of the animation speed
		/// </summary>
		[Tooltip("The multiplicator of the animation speed")]
		public float normalizedSpeed=1;
		public RectTransform foldArrow;

#if ODIN_INSPECTOR
		[ShowIf("foldArrow",null)]
#endif
		public float foldArrowClosedRotation=90;

#if ODIN_INSPECTOR
		[ShowIf("foldArrow",null)]
#endif
		public float foldArrowOpenRotation=0;

		private void Awake()
		{
			_layoutElement = GetComponent<LayoutElement>();
			_foldedHeight = _layoutElement.preferredHeight;
			if(disableContainerWhenClosed)
			{
				listContainer.gameObject.SetActive(isOpen);
			}
		}

		public void SetOpen(bool open, bool animated)
		{
			if (_foldingCoroutine != null)
			{
				StopCoroutine(_foldingCoroutine);
			}
			if (!open != _folded && gameObject.activeInHierarchy)
			{
				_foldingCoroutine = StartCoroutine(SetFoldedCoroutine(!open, animated));
			}
			if(!animated)
			{
				_fullyOpen = open;
			}
		}
		public void SetOpen(bool open)
		{
			SetOpen(open, true);
		}

		public void SwitchOpen()
		{
			SetOpen(!isOpen);
		}

		private void Update()
		{
			if (_fullyOpen) //if we're fully open, we keep resizing in case the contents change
			{
				//listContainer is automatically sized, use this as a reference
				float height = listContainer.sizeDelta.y;
				_layoutElement.preferredHeight = height + _foldedHeight + unfoldedAdditionalHeight * 2; //in total, we are the height of the view PLUS our own height

				listMask.sizeDelta = new Vector2(listMask.sizeDelta.x, height + unfoldedAdditionalHeight * 2); //when the animation is over, the mask is the same height as the view

				//rotate the arrow
				if (foldArrow)
					foldArrow.transform.localRotation = Quaternion.Euler(new Vector3(foldArrow.transform.localRotation.eulerAngles.x,
																				foldArrow.transform.localRotation.eulerAngles.y,
																				foldArrowOpenRotation));
			}
		}
		/// <summary>
		///Opens/Closes the view's hemis List
		///</summary>
		private IEnumerator SetFoldedCoroutine(bool folded,bool animated)
		{
			_fullyOpen = false;
			_folded = folded;

			if(!folded && disableContainerWhenClosed)
			{
				listContainer.gameObject.SetActive(true);
			}

			for(int i=0;i<listeners.Count;++i)
			{
				listeners[i].OnPreFoldAnimation(this, !folded);
			}
			List<ComplexLayout> disabledLayouts = new List<ComplexLayout>();
			var cl = gameObject.GetComponentInParent<ComplexLayout>();
			if (cl)
			{
				cl.enabled = false;
				disabledLayouts.Add(cl);
			}
			foreach (var cl2 in gameObject.GetComponentsInChildren<ComplexLayout>())
			{
				cl2.enabled = false;
				disabledLayouts.Add(cl2);
			}
			yield return null;
			//animate the opening/Closing
			float layoutStartHeight = _layoutElement.preferredHeight;
			float layoutEndHeight = _folded ? _foldedHeight : _foldedHeight + listContainer.sizeDelta.y + unfoldedAdditionalHeight*2;

			//fold arrow values
			float foldStartRotation=0,foldEndRotation=0;
			if (foldArrow)
			{
				foldStartRotation = foldArrow.transform.localRotation.eulerAngles.z;
				foldEndRotation = _folded ? foldArrowClosedRotation : foldArrowOpenRotation;
			}

			//mask values
			float maskStartHeight=0,maskEndHeight=0;
			if (listMask != null)
			{
				maskStartHeight = listMask.sizeDelta.y;
				maskEndHeight = _folded ? 0 : listContainer.sizeDelta.y + unfoldedAdditionalHeight * 2;
			}

			//time values
			float startTime = Time.time;
			float endTime = animated? Time.time + ((_folded ? FOLD_ANIMATION_SPEED_CLOSE:FOLD_ANIMATION_SPEED_OPEN)/normalizedSpeed) : 0;

			//bkg color values
			Color startColor=Color.white,endColor=Color.white;
			if (bkg != null)
			{
				startColor = bkg.color;
				endColor = _folded ? UISkinManager.currentModel.GetColor(UISkinDefinition.SkinColorDefinitions.White) : UISkinManager.currentModel.GetColor(UISkinDefinition.SkinColorDefinitions.NeutralBackgroundImage);
			}


			//animation variables
			AnimationCurve curve = CurveDatabase.GetStaticAnimation(CurveDatabase.AnimationCurveEnum.easeInOutSine);
			float lerp=0;

			//start animating
			while (Time.time < endTime)
			{
				lerp = curve.Evaluate(Mathf.InverseLerp(startTime, endTime, Time.time));
				//size the layout
				_layoutElement.preferredHeight = Mathf.Lerp(layoutStartHeight, layoutEndHeight, lerp);

				//rotate the arrow
				if (foldArrow)
				{
					foldArrow.transform.localRotation = Quaternion.Euler(new Vector3(foldArrow.transform.localRotation.eulerAngles.x,
																				foldArrow.transform.localRotation.eulerAngles.y,
																				Mathf.Lerp(foldStartRotation, foldEndRotation, lerp)));
				}

				//size the mask
				if (listMask)
				{
					listMask.sizeDelta = new Vector2(listMask.sizeDelta.x, Mathf.Lerp(maskStartHeight, maskEndHeight, lerp));
				}

				//color the bkg
				if (bkg)
				{
					bkg.color = Color.Lerp(startColor, endColor, lerp);
				}

				yield return null;
			}
			//size the layout
			_layoutElement.preferredHeight = layoutEndHeight;

			//rotate the arrow
			if (foldArrow)
			{
				foldArrow.transform.localRotation = Quaternion.Euler(new Vector3(foldArrow.transform.localRotation.eulerAngles.x,
																			foldArrow.transform.localRotation.eulerAngles.y,
																			foldEndRotation));
			}

			//size the mask
			if (listMask)
			{
				listMask.sizeDelta = new Vector2(listMask.sizeDelta.x, maskEndHeight);
			}

			//color the bkg
			if (bkg)
			{
				bkg.color = endColor;
			}





			for (int i = 0; i < listeners.Count; ++i)
			{
				listeners[i].OnPostFoldAnimation(this, !folded);
			}

			if (!_folded)
			{
				//if it's open, the children might change size as well
				_fullyOpen = true;
			}
			else
			{
				listContainer.gameObject.SetActive(!disableContainerWhenClosed);
			}
			foreach (var cl2 in disabledLayouts)
			{
				cl2.enabled = true;
			}
		}
	}
}