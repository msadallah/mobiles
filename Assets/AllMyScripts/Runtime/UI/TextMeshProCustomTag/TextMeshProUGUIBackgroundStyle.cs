﻿namespace TMPro
{
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.EventSystems;

	using UnityEngine.Events;
	using System.Globalization;

	using AllMyScripts.Common.UI;
	using AllMyScripts.Common.Pooling;
    using AllMyScripts.Common.Utils;

    public class TextMeshProUGUIBackgroundStyle : UIBehaviour
    {

		public const int PADDING = 6;


		public const string BACKGROUND_REGEX = "<[bB][aA][cC][kK][gG][rR][oO][uU][nN][dD]=[ -., 0-9A-z]*>";

		public static string GenerateTag(Color c)
		{
			return "<background=" + c.r.ToString(CultureInfo.InvariantCulture) + "," 
								   +c.g.ToString(CultureInfo.InvariantCulture) + "," 
								   +c.b.ToString(CultureInfo.InvariantCulture) + "," 
									+c.a.ToString(CultureInfo.InvariantCulture) + ">";
		}
		public static string GenerateTag(string parameter)
		{
			return "<background="+ parameter+">";
		}
		public static string GenerateTag(Color c, string parameter)
		{

			return "<background=" + c.r.ToString(CultureInfo.InvariantCulture) + ","
								   + c.g.ToString(CultureInfo.InvariantCulture) + ","
								   + c.b.ToString(CultureInfo.InvariantCulture) + ","
									+ c.a.ToString(CultureInfo.InvariantCulture) + ","
									+parameter+">";
		}

        public static string EndTag {
            get {
                return "</background>";
            }
        }

		// Warning ! Take a deep breath before going too deep into this class.

		#region Properties
		[System.Serializable]public class BackgroundEvent : UnityEvent<BackgroundTagPanel, string> { };

		[SerializeField] private bool m_forceOverflow = true;
		[Header("<background=R,G,B,A,stringParameter>")]
		[Header("<background=R,G,B,A>")]
		[Header("<background=stringParameter>")]
		[Header("Tag can be :")]
		public BackgroundEvent OnBackgroundClicked = new BackgroundEvent();

		private TextMeshProUGUI _textMeshPro;
        private TextMeshProUGUI textMeshPro
		{
			get
			{
				if(!_textMeshPro)
				{
					_textMeshPro = GetComponentInChildren<TextMeshProUGUI>();
				}
				return _textMeshPro;
			}
		}
        protected override void Start()
        {
            base.Start();
        }

		private ObjectPool BackgroundObjectPool;

        private string TextToInput;

		///list of the character indexes that are influenced by a backgroundtag
        private Dictionary<BackgroundTagPanel, List<int>> dicBackgroundChars = new Dictionary<BackgroundTagPanel, List<int>>();
        
        private RectTransform rt;
        public RectTransform RT {
            get {
                if (this.rt == null) {
                    this.rt = this.GetComponent<RectTransform>();
                }
                return this.rt;
            }
        }

        #endregion

		/// <summary>
		/// sets the text using the given background pool. If none is set, TMPBackgroundManager is used
		/// </summary>
        public void SetText(string text, ObjectPool BackgroundObjectPool = null)
        {
			if(BackgroundObjectPool==null)
			{
				if(TMPBackgroundManager.backgroundPool==null)
				{
					Debug.LogWarning("No background pool set, and the default backgroundmanager can not be found", gameObject);
				}
				BackgroundObjectPool = TMPBackgroundManager.backgroundPool;
			}
            this.BackgroundObjectPool = BackgroundObjectPool;

            //we clear the previous text
            if (string.IsNullOrEmpty(text))
            {
                TextToInput = " ";
                textMeshPro.textInfo.ClearAllMeshInfo();
                return;
            }
            // Be sure that we use overflow mode, otherwise it could modify the string to parse
			if (m_forceOverflow)
				textMeshPro.overflowMode = TextOverflowModes.Overflow;

            //and we force the text to refresh
            textMeshPro.ForceMeshUpdate();
            
            TMP_TextInfo textInfos = textMeshPro.GetTextInfo(text);
			textMeshPro.text = "";
            string sTextToParse = "";

            for (int i = 0; i < textInfos.characterCount; ++i)
            {
                sTextToParse += textInfos.characterInfo[i].character;

            }
            TextToInput = text;            
			
            this.ClearDictionnary();
            //parse the Background and add in dicBackgroundChar
            List<BackgroundTagPanel> currentTags = new List<BackgroundTagPanel>();
            for (int i = 0; i < sTextToParse.Length; ++i)
            {
                if (sTextToParse[i] == '<'
                   &&
                   i + 12 < sTextToParse.Length)
                {
                    //we might've encountered a tag.
                    if (sTextToParse[i + 1] == '/'
                        //is it a closing tag?
                        && (sTextToParse[i + 2] == 'B' || sTextToParse[i + 2] == 'b')
                        && (sTextToParse[i + 3] == 'A' || sTextToParse[i + 3] == 'a')
                        && (sTextToParse[i + 4] == 'C' || sTextToParse[i + 4] == 'c')
                        && (sTextToParse[i + 5] == 'K' || sTextToParse[i + 5] == 'k')
                        && (sTextToParse[i + 6] == 'G' || sTextToParse[i + 6] == 'g')
                        && (sTextToParse[i + 7] == 'R' || sTextToParse[i + 7] == 'r')
                        && (sTextToParse[i + 8] == 'O' || sTextToParse[i + 8] == 'o')
                        && (sTextToParse[i + 9] == 'U' || sTextToParse[i + 9] == 'u')
                        && (sTextToParse[i + 10] == 'N' || sTextToParse[i + 10] == 'n')
                        && (sTextToParse[i + 11] == 'D' || sTextToParse[i + 11] == 'd')
                        && sTextToParse[i + 12] == '>')
                    {

						//It is a closing tag! remove the last tag
						sTextToParse = sTextToParse.Remove(i, 13);
						currentTags.RemoveAt(currentTags.Count - 1);
						

                        i--;
                        continue;
                    }
                    else if (
                        (sTextToParse[i + 1] == 'B' || sTextToParse[i + 1] == 'b')
                        && (sTextToParse[i + 2] == 'A' || sTextToParse[i + 2] == 'a')
                        && (sTextToParse[i + 3] == 'C' || sTextToParse[i + 3] == 'c')
                        && (sTextToParse[i + 4] == 'K' || sTextToParse[i + 4] == 'k')
                        && (sTextToParse[i + 5] == 'G' || sTextToParse[i + 5] == 'g')
                        && (sTextToParse[i + 6] == 'R' || sTextToParse[i + 6] == 'r')
                        && (sTextToParse[i + 7] == 'O' || sTextToParse[i + 7] == 'o')
                        && (sTextToParse[i + 8] == 'U' || sTextToParse[i + 8] == 'u')
                        && (sTextToParse[i + 9] == 'N' || sTextToParse[i + 9] == 'n')
                        && (sTextToParse[i + 10] == 'D' || sTextToParse[i + 10] == 'd')
                        && sTextToParse[i + 11] == '=')
                    {
                        //it's an opening tag! Hopefully

                        //first, we fetch the parameters
                        string sTagAndParameters = sTextToParse.Substring(i + 12, sTextToParse.IndexOf('>') - (i + 12));

                        List<string> paramsString = new List<string>();

                        if (sTagAndParameters.Contains(","))
                        {
                            paramsString.AddRange(sTagAndParameters.Split(','));
                        }
                        else {
                            paramsString.Add(sTagAndParameters);
                        }
						//remove the tag from the text to parse
						sTextToParse = sTextToParse.Remove(i, 13 + sTagAndParameters.Length);

						BackgroundTagPanel btp = this.BackgroundObjectPool.GetInstance().GetComponent<BackgroundTagPanel>();

                        btp.SetInitialTag(sTagAndParameters);
                        btp.SetParameters(paramsString);
						btp.SetOwner(this);
						btp.transform.SetParent(transform);
						btp.transform.localScale = Vector3.one;

                        // Setup 
                        currentTags.Add(btp);
                        i--;

                        continue;
                    }
                }
                foreach (BackgroundTagPanel backgroundTextPanel in currentTags)
                {
                    if (!dicBackgroundChars.ContainsKey(backgroundTextPanel))
                    {
                        dicBackgroundChars.Add(backgroundTextPanel, new List<int>());
                    }
                    dicBackgroundChars[backgroundTextPanel].Add(i);

                }
                

            }// End for (sTextToParse)
            

            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(BACKGROUND_REGEX);

			TextToInput = regex.Replace(TextToInput, "<space="+ PADDING + "px><nobr>");
			TextToInput = TextToInput.Replace("</background>", "</nobr><space="+ PADDING + "px>");
			
			textMeshPro.text = TextToInput;
            this.textMeshPro.transform.SetAsLastSibling();

            UpdateBTP();
			textMeshPro.Rebuild(CanvasUpdate.PreRender);

        }

		[ContextMenu("Update")]
		private void UpdateBTP()
		{
			textMeshPro.ForceMeshUpdate();
			var textInfo = textMeshPro.textInfo;
			foreach (KeyValuePair<BackgroundTagPanel, List<int>> kvp in this.dicBackgroundChars)
			{
				RectTransform rt = kvp.Key.GetRT();

				Vector3 topLeft = textInfo.characterInfo[kvp.Value[0]].topLeft - Vector3.right*PADDING;
				Vector3 bottomRight = textInfo.characterInfo[kvp.Value[kvp.Value.Count-1]].bottomRight + Vector3.right*PADDING;
				//find the highest and lowest y
				float top, bottom;
				for (int i=0;i<kvp.Value.Count;++i)
				{
					top = textInfo.characterInfo[kvp.Value[i]].topLeft.y;
					bottom = textInfo.characterInfo[kvp.Value[i]].bottomRight.y;
					if (topLeft.y < top)
					{
						topLeft.y = top;
					}
					if (bottomRight.y > bottom)
					{
						bottomRight.y = bottom;
					}
				}


				Vector3 position = new Vector3(topLeft.x, (topLeft.y + bottomRight.y) * 0.5f);
				kvp.Key.transform.position = ((RectTransform)textMeshPro.transform).TransformPoint(position);
				((RectTransform)kvp.Key.transform).sizeDelta = new Vector2(bottomRight.x - topLeft.x, textInfo.lineInfo[0].lineHeight);
			}
		}
		
			   

		public void OnClickBackground(BackgroundTagPanel backgroundTagPanel)
		{
			// Send the event to the receiving scripts
			this.OnBackgroundClicked.Invoke(backgroundTagPanel, backgroundTagPanel.ClickParameter);
        }

        private float ReadParameter(float parameter, float defaultValue)
        {
            if (parameter == 0)
            {
                return defaultValue;
            }
            return parameter;
        }

		protected override void OnDestroy()
		{
			base.OnDestroy();
			ClearDictionnary();
		}

		private void ClearDictionnary() {

            List<BackgroundTagPanel> backgroundTagPanels = new List<BackgroundTagPanel>();

            foreach (KeyValuePair<BackgroundTagPanel, List<int>> kvp in this.dicBackgroundChars) {
                this.BackgroundObjectPool.PoolObject(kvp.Key.gameObject);
            }

            this.dicBackgroundChars.Clear();
        }

		protected override void OnRectTransformDimensionsChange()
		{
			base.OnRectTransformDimensionsChange();
			UpdateBTP();
			Invoke("UpdateBTP", 0.05f);
		}
		protected override void OnTransformParentChanged()
		{
			base.OnTransformParentChanged();
			UpdateBTP();
			Invoke("UpdateBTP", 0.05f);
		}
	}
}