﻿namespace TMPro
{
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;

	using System.Globalization;
    using AllMyScripts.Common.Utils;

    /// <summary>
    /// <see cref="TextMeshProUGUIBackgroundStyle"/>
    /// </summary>
    public class BackgroundTagPanel : MonoBehaviour
	{
		#region Properties

		[Header("BackgroundTagPanel")]
		[SerializeField]
		private Image backgroundImage = default;

		private RectTransform rt;
		public RectTransform RT
		{
			get
			{
				if (this.rt == null)
				{
					this.rt = this.GetComponent<RectTransform>();
				}
				return this.rt;
			}
		}


		protected List<string> parameters;

		private float totalWidth = -1f;
		public float TotalWidth
		{
			get { return this.totalWidth; }
		}

		public int TotalPxWidth
		{
			get
			{

				return Mathf.FloorToInt(this.RT.rect.width);

				// return Mathf.FloorToInt((this.Text.Length * this.textTag.fontSize) + this.LeftSpacing + this.RightSpacing);
			}
		}


		private Button buttonTag = null;
		public Button ButtonTag
		{
			get
			{
				if (this.buttonTag == null)
				{
					this.buttonTag = this.GetComponentInChildren<Button>();
				}

				return this.buttonTag;
			}
		}

		private string clickParameter;
		public string ClickParameter
		{
			get { return this.clickParameter; }
		}

		private TextMeshProUGUIBackgroundStyle parentTMProBackgroundStyle;

		private string initialTag;
		public string InitialTag
		{
			get { return this.initialTag; }
		}

		#endregion

		public void Start()
		{
			// this.ButtonTag.onClick.AddListener(this.OnClickTagButton);
		}

		public void OnDestroy()
		{

			this.parentTMProBackgroundStyle = null;

			if (this.buttonTag != null)
			{
				//   this.buttonTag.onClick.RemoveListener(this.OnClickTagButton);
			}
		}

		public void OnClickTagButton()
		{
			if (this.parentTMProBackgroundStyle != null)
			{
				this.parentTMProBackgroundStyle.OnClickBackground(this);
			}
		}

		public void SetInitialTag(string initialTag)
		{
			this.initialTag = initialTag;
		}

		private void SetColorFromParam()
		{
			if (this.parameters != null && this.parameters.Count > 2)
			{
				// Take the 3 first parameters : assume that they are color float rgb value
				List<float> colorParameters = new List<float>();

				CultureInfo ciUS = System.Globalization.CultureInfo.GetCultureInfo("EN-us");

				// Parse the parameters for float
				foreach (string param in this.parameters)
				{
					float colorVal = 0f;
					if (float.TryParse(param, System.Globalization.NumberStyles.Any, ciUS, out colorVal))
					{
						colorParameters.Add(colorVal);
					}
				}

				if (colorParameters.Count > 3)
				{
					Color colorBackground = new Color(Mathf.Clamp01(colorParameters[0]), Mathf.Clamp01(colorParameters[1]), Mathf.Clamp01(colorParameters[2]), Mathf.Clamp01(colorParameters[3]));
					this.backgroundImage.color = colorBackground;
				}
				else
				{
					// Debug.Log("BackgroundTagPanel.SetColorFromParam - not enough params to make a rgb color. paramCount : " + colorParameters.Count);
				}

			}
		}
		public virtual void SetOwner(TextMeshProUGUIBackgroundStyle owner)
		{
			parentTMProBackgroundStyle = owner;
		}
		public virtual void SetParameters(List<string> parameters)
		{
			this.parameters = parameters;
			this.clickParameter = "";

			if (this.parameters != null)
			{
				this.SetColorFromParam();

				// TODO Improve param testing
				// Simple test : if there is 1 param, there is only the onClick param, if there is 4, params are : r, g, b, onClick
				if (this.parameters.Count == 1)
				{
					//only the click parameter
					this.clickParameter = this.parameters[0];
				}
				else if (this.parameters.Count == 4)
				{
					//only the colors
					this.clickParameter = null;
				}
				else if (this.parameters.Count == 5)
				{
					//both the colors and the click param
					this.clickParameter = this.parameters[4];
				}
				else
				{
					Debug.LogError("Error : TMPBackgroundStyle needs to have either 1, 4 or 5 parameters ; color, alpha, parameter");
				}
				this.ButtonTag.interactable = !string.IsNullOrEmpty(this.clickParameter);

			}

		}

		public void SetPosition(RectTransform parentRT, Vector3 bottomLeft, Vector3 topRight)
		{

			//Debug.Log("BackgroundTopPanel.SetPosition - bottomLeft : "+bottomLeft.ToString()+", topRight : "+ topRight.ToString());

			this.RT.SetParent(parentRT);

			this.RT.SetHeight(topRight.y - bottomLeft.y);
			this.RT.SetWidth(this.totalWidth);

			this.RT.position = new Vector3(this.RT.position.x, this.RT.position.y, 0f);

			this.RT.anchoredPosition = bottomLeft;

		}

	}
}