﻿//Code originally for Six Legends 
//Author : Vincent Paquin 
//Owner : Vincent Paquin 
//Only AllMyScripts and AllMyScripts related projects are allowed to reuse this code

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMPro
{
    public class TextMeshProUGUIAnimatedStyle : UIBehaviour
    {
        public const string ANIM_REGEX = "<[aA][nN][iI][mM]=[ ,A-z]*[-., 0-9]*>";
        private class TextAnimation
        {
            public AnimationEnum eAnimation;
            public List<float> lParameters;
            public TextAnimation(AnimationEnum animation, List<float> parameters = null)
            {
                eAnimation = animation;
                lParameters = parameters;
            }
        }
        public enum AnimationEnum
        {
            Wave,
            Shake,
            Scale,
            Dance
        }
        [System.NonSerialized]
        public TMPro.TextMeshProUGUI textMeshPro;
        protected override void Awake()
        {
            base.Awake();
            textMeshPro = GetComponent<TextMeshProUGUI>();

        }


        private string TextToInput;
        private Dictionary<TextAnimation, List<int>> dicAnimatedChars = new Dictionary<TextAnimation, List<int>>();

        public void SetText(string text)
        {
            //we need to recalculate the vertex operations to run
            StopAllCoroutines();
            if (string.IsNullOrEmpty(text))
            {
                TextToInput = " ";
                textMeshPro.textInfo.ClearAllMeshInfo();
                return;
            }
            textMeshPro.ForceMeshUpdate();
            TMP_TextInfo textInfos = textMeshPro.GetTextInfo(text);
            string sTextToParse = "";

            for (int i = 0; i < textInfos.characterCount; ++i)
            {
                sTextToParse += textInfos.characterInfo[i].character;

            }

            TextToInput = text;
            //cleanup texttoinput from the anim tags
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(ANIM_REGEX);
            TextToInput = regex.Replace(TextToInput, "");
            TextToInput = TextToInput.Replace("</anim>", "");

            //stext contains the text without the animations
            dicAnimatedChars.Clear();
            //parse the animation and add in dicAnimatedChar
            List<TextAnimation> currentTags = new List<TextAnimation>();
            for (int i = 0; i < sTextToParse.Length; ++i)
            {

                if (sTextToParse[i] == '<'
                   &&
                   i + 6 < sTextToParse.Length)
                {
                    //we might've encountered a tag.
                    if (sTextToParse[i + 1] == '/'
                        //is it a closing tag?
                        && (sTextToParse[i + 2] == 'A' || sTextToParse[i + 2] == 'a')
                        && (sTextToParse[i + 3] == 'N' || sTextToParse[i + 3] == 'n')
                        && (sTextToParse[i + 4] == 'I' || sTextToParse[i + 4] == 'i')
                        && (sTextToParse[i + 5] == 'M' || sTextToParse[i + 5] == 'm')
                        && sTextToParse[i + 6] == '>')
                    {
                        //It is a closing tag! remove the last tag
                        currentTags.RemoveAt(currentTags.Count - 1);
                        sTextToParse = sTextToParse.Remove(i, 7);
                        i--;
                        continue;
                    }
                    else if (
                        (sTextToParse[i + 1] == 'A' || sTextToParse[i + 1] == 'a')
                        && (sTextToParse[i + 2] == 'N' || sTextToParse[i + 2] == 'n')
                        && (sTextToParse[i + 3] == 'I' || sTextToParse[i + 3] == 'i')
                        && (sTextToParse[i + 4] == 'M' || sTextToParse[i + 4] == 'm')
                        && sTextToParse[i + 5] == '=')
                    {
                        //it's an opening tag! Hopefully
                        string sTagAndParameters = sTextToParse.Substring(i + 6, sTextToParse.IndexOf('>') - (i + 6));
                        string sTag = sTagAndParameters;
                        List<float> parameters = null;
                        if (sTagAndParameters.Contains(","))
                        {
                            //There are parameters
                            parameters = new List<float>();
                            string[] sParameters = sTagAndParameters.Split(',');
                            sTag = sParameters[0];
                            if (sParameters.Length > 2)
                            {
                                for (int p = 1; p < sParameters.Length - 1; ++p)
                                {
                                    parameters.Add(float.Parse(sParameters[p], System.Globalization.CultureInfo.InvariantCulture));
                                }
                            }
                            string sLastParameter = sParameters[sParameters.Length - 1];
                            parameters.Add(float.Parse(sLastParameter, System.Globalization.CultureInfo.InvariantCulture));

                        }

                        currentTags.Add(new TextAnimation((AnimationEnum)System.Enum.Parse(typeof(AnimationEnum), sTag, true), parameters));
                        sTextToParse = sTextToParse.Remove(i, sTagAndParameters.Length + 7);
                        i--;
                        continue;
                    }
                }
                foreach (TextAnimation animation in currentTags)
                {

                    if (!dicAnimatedChars.ContainsKey(animation))
                    {
                        dicAnimatedChars.Add(animation, new List<int>());
                    }
                    dicAnimatedChars[animation].Add(i);
                }
            }
            textMeshPro.text = TextToInput;
        }
        private void LateUpdate()
        {
            if (dicAnimatedChars.Count > 0)
            {
                textMeshPro.text = TextToInput;
                textMeshPro.ForceMeshUpdate();
                TMP_TextInfo textInfo = textMeshPro.textInfo;
                foreach (KeyValuePair<TextAnimation, List<int>> kvp in dicAnimatedChars)
                {
                    switch (kvp.Key.eAnimation)
                    {
                        case AnimationEnum.Wave:
                            {
                                Wave(kvp.Key, kvp.Value, textInfo);
                                break;
                            }
                        case AnimationEnum.Shake:
                            {
                                Shake(kvp.Key, kvp.Value, textInfo);
                                break;
                            }
                        case AnimationEnum.Scale:
                            {
                                Scale(kvp.Key, kvp.Value, textInfo);
                                break;
                            }
                        case AnimationEnum.Dance:
                            {
                                Dance(kvp.Key, kvp.Value, textInfo);
                                break;
                            }
                    }
                }
                textMeshPro.UpdateGeometry(textMeshPro.mesh, 0);
            }
        }

        private float ReadParameter(float parameter, float defaultValue)
        {
            if (parameter == 0)
            {
                return defaultValue;
            }
            return parameter;
        }

        #region Wave
        private const float WAVE_AMPLITUDE = 3;
        private const float WAVE_SPEED = 10;
        private const float WAVE_OFFSET = 2;
        [Header("Wave")]
        private float waveAmplitude = WAVE_AMPLITUDE;
        private float waveSpeed = WAVE_SPEED;
        private float waveOffset = WAVE_OFFSET;
        private void Wave(TextAnimation animation, List<int> indices, TMP_TextInfo textInfo)
        {
            waveAmplitude = WAVE_AMPLITUDE;
            waveSpeed = WAVE_SPEED;
            waveOffset = WAVE_OFFSET;
            //read (potential) parameters
            if (animation.lParameters != null && animation.lParameters.Count > 0)
            {
                waveAmplitude = ReadParameter(animation.lParameters[0], WAVE_AMPLITUDE);
                if (animation.lParameters.Count > 1)
                {
                    waveSpeed = ReadParameter(animation.lParameters[1], WAVE_SPEED);
                    if (animation.lParameters.Count > 2)
                    {
                        waveOffset = ReadParameter(animation.lParameters[2], WAVE_OFFSET);
                    }
                }
            }
            Vector3 up = Vector3.up;

            for (int i = 0; i < textInfo.characterCount; i++)
            {
                if (!indices.Contains(i))
                    continue;

                TMP_CharacterInfo charInfo = textInfo.characterInfo[i];

                // Skip characters that are not visible and thus have no geometry to manipulate.
                if (!charInfo.isVisible)
                    continue;

                // Get the index of the material used by the current character.
                int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;

                // Get the index of the first vertex used by this text element.
                int vertexIndex = textInfo.characterInfo[i].vertexIndex;
                // Get the cached vertices of the mesh used by this text element (character or sprite).

                Vector3[] destinationVertices = textInfo.meshInfo[materialIndex].vertices;

                float fTransform = waveAmplitude * Mathf.Sin(Time.time * waveSpeed + (i * waveOffset));
                destinationVertices[vertexIndex] += up * fTransform;
                destinationVertices[vertexIndex + 1] += up * fTransform;
                destinationVertices[vertexIndex + 2] += up * fTransform;
                destinationVertices[vertexIndex + 3] += up * fTransform;
            }
            for (int i = 0; i < textInfo.meshInfo.Length; i++)
            {
                textInfo.meshInfo[i].mesh.vertices = textInfo.meshInfo[i].vertices;
                textMeshPro.UpdateGeometry(textInfo.meshInfo[i].mesh, i);
            }
        }
        #endregion

        #region Shake
        private const float SHAKE_AMPLITUDE = 3;
        [Header("Shake")]
        private float shakeAmplitude = SHAKE_AMPLITUDE;
        private void Shake(TextAnimation animation, List<int> indices, TMP_TextInfo textInfo)
        {
            shakeAmplitude = SHAKE_AMPLITUDE;
            if (animation.lParameters != null && animation.lParameters.Count > 0)
            {
                shakeAmplitude = ReadParameter(animation.lParameters[0], SHAKE_AMPLITUDE);
            }

            for (int i = 0; i < textInfo.characterCount; i++)
            {
                if (!indices.Contains(i))
                    continue;

                TMP_CharacterInfo charInfo = textInfo.characterInfo[i];

                // Skip characters that are not visible and thus have no geometry to manipulate.
                if (!charInfo.isVisible)
                    continue;

                // Get the index of the material used by the current character.
                int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;

                // Get the index of the first vertex used by this text element.
                int vertexIndex = textInfo.characterInfo[i].vertexIndex;
                // Get the cached vertices of the mesh used by this text element (character or sprite).

                Vector3[] destinationVertices = textInfo.meshInfo[materialIndex].vertices;
                Vector3 direction = new Vector3(Random.Range(-shakeAmplitude, shakeAmplitude), Random.Range(-shakeAmplitude, shakeAmplitude), 0);
                destinationVertices[vertexIndex] += direction;
                destinationVertices[vertexIndex + 1] += direction;
                destinationVertices[vertexIndex + 2] += direction;
                destinationVertices[vertexIndex + 3] += direction;
            }
            for (int i = 0; i < textInfo.meshInfo.Length; i++)
            {
                textInfo.meshInfo[i].mesh.vertices = textInfo.meshInfo[i].vertices;
                textMeshPro.UpdateGeometry(textInfo.meshInfo[i].mesh, i);
            }
        }
        #endregion

        #region Scale
        private const float SCALE_AMPLITUDE = 3;
        private const float SCALE_SPEED = 10;
        private const float SCALE_OFFSET = -1.2f;
        [Header("Scale")]
        private float ScaleAmplitude = SCALE_AMPLITUDE;
        private float ScaleSpeed = SCALE_SPEED;
        private float ScaleOffset = SCALE_OFFSET;
        private void Scale(TextAnimation animation, List<int> indices, TMP_TextInfo textInfo)
        {
            ScaleAmplitude = SCALE_AMPLITUDE;
            ScaleSpeed = SCALE_SPEED;
            ScaleOffset = SCALE_OFFSET;
            //read (potential) parameters
            if (animation.lParameters != null && animation.lParameters.Count > 0)
            {
                ScaleAmplitude = ReadParameter(animation.lParameters[0], SCALE_AMPLITUDE);
                if (animation.lParameters.Count > 1)
                {
                    ScaleSpeed = ReadParameter(animation.lParameters[1], SCALE_SPEED);
                    if (animation.lParameters.Count > 2)
                    {
                        ScaleOffset = ReadParameter(animation.lParameters[2], SCALE_OFFSET);
                    }
                }
            }

            for (int i = 0; i < textInfo.characterCount; i++)
            {
                if (!indices.Contains(i))
                    continue;

                TMP_CharacterInfo charInfo = textInfo.characterInfo[i];

                // Skip characters that are not visible and thus have no geometry to manipulate.
                if (!charInfo.isVisible)
                    continue;

                // Get the index of the material used by the current character.
                int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;

                // Get the index of the first vertex used by this text element.
                int vertexIndex = textInfo.characterInfo[i].vertexIndex;
                // Get the cached vertices of the mesh used by this text element (character or sprite).

                Vector3[] destinationVertices = textInfo.meshInfo[materialIndex].vertices;
                Vector3 v3Center = (destinationVertices[vertexIndex] + destinationVertices[vertexIndex + 2]) * 0.5f;
                float fScale = ScaleAmplitude * (0.5f * (1 + Mathf.Sin(Time.time * ScaleSpeed + (i * ScaleOffset))));
                destinationVertices[vertexIndex] += (destinationVertices[vertexIndex] - v3Center).normalized * fScale;
                destinationVertices[vertexIndex + 1] += (destinationVertices[vertexIndex + 1] - v3Center).normalized * fScale;
                destinationVertices[vertexIndex + 2] += (destinationVertices[vertexIndex + 2] - v3Center).normalized * fScale;
                destinationVertices[vertexIndex + 3] += (destinationVertices[vertexIndex + 3] - v3Center).normalized * fScale;
            }
            for (int i = 0; i < textInfo.meshInfo.Length; i++)
            {
                textInfo.meshInfo[i].mesh.vertices = textInfo.meshInfo[i].vertices;
                textMeshPro.UpdateGeometry(textInfo.meshInfo[i].mesh, i);
            }
        }
        #endregion

        #region Dance
        private const float DANCE_SPEED = 10;
        private const float DANCE_OFFSET = 0.02f;
        [Header("Dance")]
        private float DanceSpeed = DANCE_SPEED;
        private float DanceOffset = DANCE_OFFSET;
        private void Dance(TextAnimation animation, List<int> indices, TMP_TextInfo textInfo)
        {
            DanceSpeed = DANCE_SPEED;
            DanceOffset = DANCE_OFFSET;
            //read (potential) parameters
            if (animation.lParameters != null && animation.lParameters.Count > 0)
            {
                DanceSpeed = ReadParameter(animation.lParameters[0], DANCE_SPEED);
                if (animation.lParameters.Count > 1)
                {
                    DanceOffset = ReadParameter(animation.lParameters[1], DANCE_OFFSET);
                }
            }

            float sinTime = Mathf.Sin(Time.time * DanceSpeed) * DanceOffset;
            float cosTime = Mathf.Cos(Time.time * DanceSpeed) * DanceOffset;
            for (int i = 0; i < textInfo.characterCount; i++)
            {
                if (!indices.Contains(i))
                    continue;

                TMP_CharacterInfo charInfo = textInfo.characterInfo[i];

                // Skip characters that are not visible and thus have no geometry to manipulate.
                if (!charInfo.isVisible)
                    continue;

                // Get the index of the material used by the current character.
                int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;

                // Get the index of the first vertex used by this text element.
                int vertexIndex = textInfo.characterInfo[i].vertexIndex;
                // Get the cached vertices of the mesh used by this text element (character or sprite).

                Vector3[] destinationVertices = textInfo.meshInfo[materialIndex].vertices;
                destinationVertices[vertexIndex] += Vector3.right * sinTime;
                destinationVertices[vertexIndex + 1] += Vector3.right * cosTime;
                destinationVertices[vertexIndex + 2] += Vector3.right * cosTime;
                destinationVertices[vertexIndex + 3] += Vector3.right * sinTime;
            }
            for (int i = 0; i < textInfo.meshInfo.Length; i++)
            {
                textInfo.meshInfo[i].mesh.vertices = textInfo.meshInfo[i].vertices;
                textMeshPro.UpdateGeometry(textInfo.meshInfo[i].mesh, i);
            }
        }
        #endregion
    }
}
