﻿namespace TMPro
{
	using AllMyScripts.Common.Pooling;
    using UnityEngine;

    public class TMPBackgroundManager : MonoBehaviour
    {
        public static ObjectPool backgroundPool;
        public GameObject backgroundPrefab;

        private void Awake()
        {
            backgroundPool = GetComponent<ObjectPool>();
            backgroundPool.Init(backgroundPrefab, 8, transform);
        }
    }
}