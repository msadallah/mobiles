﻿namespace AllMyScripts.Common.UI
{
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.EventSystems;

	using AllMyScripts.Common.Tools;

	[DisallowMultipleComponent]
	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	public class RenderArea : UIBehaviour
	{
		public interface ICameraViewportChanged { void OnCameraViewportChanged(); }
		public Camera[] m_camera;
		/// <summary>
		/// When a camera's render rect goes beyond [0..1], it is resized to fit in the bounds.
		/// Force constant size reveerses the process ; it forces the render rect to always have
		/// the right size, even if it doesn't fit the rect anymore
		/// </summary>
		public bool forceConstantSize=false;

		public bool UpdateRect { get; set; } = true;

		public InterfaceDelegate<ICameraViewportChanged> IDCameraViewportChanged = new InterfaceDelegate<ICameraViewportChanged>();
		private RectTransform _rectTransform;

		protected override void Awake()
		{
			base.Awake();
			_rectTransform = GetComponent<RectTransform>();
		}

		protected void Update()
		{
			if(UpdateRect)
				Recalculate();
		}

		public void Recalculate()
		{
			if (m_camera != null && m_camera.Length > 0)
			{
				Rect result = GetRect();

				foreach (Camera cam in m_camera)
				{
					if (cam != null)
						cam.rect = result;
				}

				for (int i = 0; i < IDCameraViewportChanged.Count; i++)
					IDCameraViewportChanged[i].OnCameraViewportChanged();
			}
		}

		public Rect GetRect()
		{
			Rect rect = _rectTransform.GetRectTransformToScreenSpace();
			Rect result = new Rect(rect.x / (float)Screen.width, rect.y / (float)Screen.height, rect.width / (float)Screen.width, rect.height / (float)Screen.height);

			if (forceConstantSize)
			{
				if (result.xMin < 0)
					result.x += 0 - result.xMin;

				if (result.xMax > 1)
					result.x += 1 - result.xMax;

				if (result.yMin < 0)
					result.y += 0 - result.yMin;

				if (result.yMax > 1)
					result.y += 1 - result.yMax;
			}

			return result;
		}
	}
}