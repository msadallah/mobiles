﻿namespace AllMyScripts.Common.UI
{
	using UnityEngine;
	using UnityEngine.UI;

	/// <summary>
	/// carves a hole in a UI Graphic through shaders
	/// </summary>
	[RequireComponent(typeof(Graphic))]
	[ExecuteInEditMode]
	public class CarvedUIImage : MonoBehaviour
	{
		/// <summary>
		/// the recttransform that will be carving the image
		/// </summary>
		[Header("The graphic/image needs to use the material : CarvedUIImage")]
		public RectTransform carver = null;
		private Graphic _targetGraphic = null;

		/// <summary>
		/// should the hole update itself?
		/// </summary>
		public bool autoUpdate = true;

		//used in update
		/// <summary>
		/// result contains the UV where the image will be carved
		/// </summary>
		private Vector4 _result;
		private Vector3[] _myWorldCorners = new Vector3[4];
		private Vector3[] _carverWorldCorners= new Vector3[4];
		private void Start()
		{
			_targetGraphic = GetComponent<Graphic>();
			//we will want to use an instance of the material instead of the actual material
			_targetGraphic.material = new Material(Shader.Find("UI/Carved-Default"));
		}
		private void LateUpdate()
		{
			if (autoUpdate)
				UpdateHole();
		}
		/// <summary>
		/// recalculates the hole. note that setting autoUpdate to true will do this automatically
		/// </summary>
		public void UpdateHole()
		{
			//find the UVs
			if (carver != null && carver.gameObject.activeInHierarchy)
			{
				///get the rects positions
				carver.GetWorldCorners(_carverWorldCorners);
				((RectTransform)transform).GetWorldCorners(_myWorldCorners);

				//we write in result the UV rect in which the image will NOT be drawn
				_result.Set(Mathf.InverseLerp(_myWorldCorners[0].x, _myWorldCorners[2].x, _carverWorldCorners[0].x) //xmin
				, Mathf.InverseLerp(_myWorldCorners[0].y, _myWorldCorners[2].y, _carverWorldCorners[0].y)           //ymin
				, Mathf.InverseLerp(_myWorldCorners[0].x, _myWorldCorners[2].x, _carverWorldCorners[2].x)           //xmax
				, Mathf.InverseLerp(_myWorldCorners[0].y, _myWorldCorners[2].y, _carverWorldCorners[2].y));         //ymax

				_targetGraphic.material.SetVector("_CarvedUV", _result);
			}
		}
	}
}