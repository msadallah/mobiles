﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

public class UIRedirectInput : UIBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, IScrollHandler
{
	[FormerlySerializedAs("redirectTo"), SerializeField] private GameObject _redirectTo;
	public GameObject redirectTo
	{
		get
		{
			return _redirectTo;
		}
		set
		{
			_redirectTo = value;
			if (_redirectTo)
				Prepare();
		}
	}

	[SerializeField] private bool _onClick = true;
	[SerializeField] private bool _onEnter = true;
	[SerializeField] private bool _onExit = true;
	[SerializeField] private bool _onDown = true;
	[SerializeField] private bool _onUp = true;
	[SerializeField] private bool _onScroll = true;

	private IPointerClickHandler _onClickHandler;
	private IPointerEnterHandler _onEnterHandler;
	private IPointerExitHandler _onExitHandler;
	private IPointerDownHandler _onDownHandler;
	private IPointerUpHandler _onUpHandler;
	private IScrollHandler _onScrollHandler;

	protected override void Awake()
	{
		base.Awake();

		if (_redirectTo)
			Prepare();
	}

	private void Prepare()
	{
		_onClickHandler = _redirectTo.GetComponent<IPointerClickHandler>();
		_onEnterHandler = _redirectTo.GetComponent<IPointerEnterHandler>();
		_onExitHandler = _redirectTo.GetComponent<IPointerExitHandler>();
		_onDownHandler = _redirectTo.GetComponent<IPointerDownHandler>();
		_onUpHandler = _redirectTo.GetComponent<IPointerUpHandler>();
		_onScrollHandler = _redirectTo.GetComponent<IScrollHandler>();
	}

#if UNITY_EDITOR
	protected override void OnValidate()
	{
		base.OnValidate();
		Awake();
	}
#endif

	public void OnPointerClick(PointerEventData eventData)
	{
		if (_onClick)
			_onClickHandler?.OnPointerClick(eventData);
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (_onDown)
			_onDownHandler?.OnPointerDown(eventData);
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (_onEnter)
			_onEnterHandler?.OnPointerEnter(eventData);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if (_onExit)
			_onExitHandler?.OnPointerExit(eventData);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (_onUp)
			_onUpHandler?.OnPointerUp(eventData);
	}

	public void OnScroll(PointerEventData eventData)
	{
		if (_onScroll)
			_onScrollHandler?.OnScroll(eventData);
	}
}