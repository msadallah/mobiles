using UnityEngine.UI;
using UnityEngine;
using System.Collections;

/// <summary>
/// Init this monobehaviour with a scrollrect, the corresponding gameobject
/// will disable it
/// when out of the scrollrect
/// </summary>
[RequireComponent(typeof(RectTransform))]
public class RectTransformDisablerOnScroll : MonoBehaviour
{
	/// <summary>
	/// The container to check. If set, it is started on start
	/// </summary>
	public ScrollRect scrollContainer
	{
		get
		{
			return m_scrollContainer;
		}
		set
		{
			m_scrollContainer = value;
			Init();
		}
	}
	/// <summary>
	/// The container to check. If set, it is started on start
	/// </summary>
	public ScrollRect m_scrollContainer;
	RectTransform m_rtMe;
	RectTransform m_rtScroll;
	private Canvas canvas;

	public void Start()
	{
		canvas = GetComponent<Canvas>();
		if (canvas == null)
		{
			gameObject.AddComponent<GraphicRaycaster>();
			canvas = GetComponent<Canvas>();
		}
		if (m_scrollContainer != null)
		{
			Init();
		}
		else
		{
			scrollContainer = GetComponentInParent<ScrollRect>();
		}
	}
	void Init()
	{
		m_scrollContainer.onValueChanged.AddListener(OnScroll);
		m_rtScroll = m_scrollContainer.GetComponent<RectTransform>();
		m_rtMe = GetComponent<RectTransform>();
	}

	private void OnScroll(Vector2 newPosition)
	{
		Apply();
	}
	private void Apply()
	{
		if (transform.parent != null && transform.parent.gameObject.activeInHierarchy && m_rtScroll != null && m_rtMe != null)
		{
			canvas.enabled = Overlap(m_rtScroll, m_rtMe);
		}
	}

	private void OnAnimatorMove()
	{
		Apply();
	}


	private void OnDestroy()
	{
		if (m_scrollContainer != null)
		{
			m_scrollContainer.onValueChanged.RemoveListener(OnScroll);
		}
	}

	public bool Overlap(RectTransform rtContainer, RectTransform rtContent, float fOffset = 0f)
	{
		Rect rectContainer = GetWorldRect(rtContainer);
		Rect rectContent = GetWorldRect(rtContent);

		Vector2 vOffset = Vector2.one * fOffset;
		rectContent.size += vOffset;
		rectContent.position -= vOffset * 0.5f;

		return rectContainer.Overlaps(rectContent);
	}

	private static Vector3[] vCorners = new Vector3[4];

	public Rect GetWorldRect(RectTransform rt)
	{
		rt.GetWorldCorners(vCorners);

		return new Rect(vCorners[0], new Vector2(
			vCorners[3].x - vCorners[0].x, vCorners[1].y - vCorners[0].y));
	}
}
