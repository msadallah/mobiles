﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TMPro.TMP_InputField))]
[DisallowMultipleComponent]
public class TMProInputFieldDelay : MonoBehaviour
{
	//how long without editing before sending the callback?
	public float fDelay = 1f;

	[Tooltip("Called after the user hasn't changed the value for fDelay seconds")]
	public TMPro.TMP_InputField.OnChangeEvent onStoppedEditing;

	private TMPro.TMP_InputField inputField;
	public string textOnSelection;
	private void Awake()
	{
		inputField = GetComponent<TMPro.TMP_InputField>();
		inputField.onSelect.AddListener(OnSelected);
		inputField.onValueChanged.AddListener(ValueChanged);
		inputField.onEndEdit.AddListener(ValueChangedImmediate);
		inputField.onSubmit.AddListener(ValueChangedImmediate);
	}

	private void OnSelected(string arg0)
	{
		textOnSelection = inputField.text;
	}

	private void ValueChanged(string sValue)
	{
		CancelInvoke();
		Invoke("ValueChangedDelayed", fDelay);
	}
	private void ValueChangedImmediate(string sValue)
	{
		CancelInvoke();
		ValueChangedDelayed();
	}
	private void ValueChangedDelayed()
	{
		if (textOnSelection != inputField.text)
		{
			textOnSelection = inputField.text;
			onStoppedEditing.Invoke(inputField.text);
		}
	}
}
