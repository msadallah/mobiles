﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteAlways]
public class CopyColorGraphic : MonoBehaviour
{
    public Graphic copyFrom;
    public Graphic[] copyTo;
    private void Awake()
    {
        if (copyFrom == null)
        {
            copyFrom = GetComponent<Graphic>();
        }
    }
    // Update is called once per frame
    void Update()
    {
        foreach (Graphic g in copyTo)
        {
            g.color = copyFrom.color;
        }
    }
}
