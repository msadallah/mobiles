﻿namespace AllMyScripts.Common.UI.BoxSizing
{
	using System;
	using System.Collections.Generic;

	using UnityEngine;

	public class WidthBoundBoxSizingGrid : BoxSizingGrid
	{
		[System.Serializable]
		public class Step : IComparable<Step>
		{
			public float fPixelsStartSize;
			public Vector2Int dimensions;

			public int CompareTo(Step obj)
			{
				return fPixelsStartSize.CompareTo(obj.fPixelsStartSize);
			}
		}
		[ReadOnly] public float currentWidth;
		public List<Step> steps;
		public override void Recalculate()
		{
			if (Application.isPlaying)
			{
				steps.Sort();
				steps.Reverse();

				currentWidth = ((RectTransform)transform).rect.width;


				Vector2Int target = new Vector2Int(0, 0);
				for (int i = 0; i < steps.Count; ++i)
				{
					if (currentWidth > steps[i].fPixelsStartSize)
					{
						target = steps[i].dimensions;
						break;
					}
				}
				if (!(target.x == target.y && target.x == 0))
				{
					var children = GetComponentsInChildren<BoxSizingTransform>();
					for (int i = 0; i < children.Length; ++i) children[i].horizontalFit = target;
				}

			}

			base.Recalculate();
		}
	}
}