﻿namespace AllMyScripts.Common.UI.BoxSizing
{
    using UnityEngine;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    public class BoxSizingParameters : ScriptableObject
    {

        #region Default const

        public const int horizontalPadding = -8;
        public const int spacing = 16;
        public const int halfSpacing = spacing / 2;

        #endregion

        #region Properties

        [Header("BoxSizingParametersSettings")]

        public int HorizontalPaddingSetting = -8;
        public int spacingSetting = 16;

        public int HalfSpacingSetting
        {
            get { return this.spacingSetting / 2; }
        }

        #endregion

#if UNITY_EDITOR
        [MenuItem("ScriptableObjects/Create a BoxSizingParametersSettings")]
        public static void CreateAsset()
        {
            var asset = ScriptableObject.CreateInstance<BoxSizingParameters>();
            AssetDatabase.CreateAsset(asset, "Assets/BoxSizingParametersSettings.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
#endif

    }
}