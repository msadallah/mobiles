﻿namespace AllMyScripts.Common.UI.BoxSizing
{
	using UnityEngine;
	using UnityEngine.EventSystems;
	using UnityEngine.UI;
	using UnityEngine.Serialization;

	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	public class BoxSizingTransform : UIBehaviour
	{
		public enum AutoVerticalAlign { None, Top, Bottom };

		public int Width { get { return horizontalFit.y - horizontalFit.x; } }

		public int Offset { get { return horizontalFit.x; } }

		//in inspector, if the maxWidth is below 0, it is disabled. However, it can range from any value from -infinity to -1,  in order
		//to keep the previously entered value
		public int maxWidth = -488;

		/// <summary>
		/// the size and offset of the recttransform in the box
		/// </summary>
		[FormerlySerializedAs("_horizontalFit")] [FormerlySerializedAs("horizontalFit")] public Vector2Int horizontalFit = new Vector2Int(0, 12);

		[Tooltip("Parameters settings for BoxSizingTransform, if null will use default value")]
		public BoxSizingParameters boxSizingParametersSetting = default;

		// Used to block the variables in the rectTransform
		private DrivenRectTransformTracker m_drtt = new DrivenRectTransformTracker();

		private RectTransform rt;

		public RectTransform RectTransform
		{
			get
			{
				if (this.rt == null)
				{
					this.rt = GetComponent<RectTransform>();
				}

				return this.rt;
			}
		}

		protected override void Awake()
		{
			rt = GetComponent<RectTransform>();
			base.Awake();
		}

		bool bRecalculating = false;
#if UNITY_EDITOR
		bool bNeedToRecalculate = false;
#endif
		/// <summary>
		/// recalculates the size of the element
		/// </summary>
		private void Recalculate()
		{
			// m_drtt.Clear();
			m_drtt.Add(this, RectTransform, DrivenTransformProperties.AnchorMinX | DrivenTransformProperties.AnchorMaxX | DrivenTransformProperties.SizeDeltaX | DrivenTransformProperties.AnchoredPositionX);

			if (rt == null)
				rt = GetComponent<RectTransform>();


			bRecalculating = true;
			base.OnCanvasGroupChanged();

			//get the variables we'll be using from the parameters



			int horizontalPadding = BoxSizingParameters.horizontalPadding;
			float halfSpacing = BoxSizingParameters.halfSpacing;
			if (this.boxSizingParametersSetting != null)
			{
				horizontalPadding = this.boxSizingParametersSetting.HorizontalPaddingSetting;
				halfSpacing = this.boxSizingParametersSetting.HalfSpacingSetting;
			}


			//set the horizontal offset based on padding and spacing
			rt.offsetMin = new Vector2((horizontalPadding * Mathf.LerpUnclamped(1, 0, (horizontalFit.x) / 6f)) + halfSpacing, rt.offsetMin.y);
			rt.offsetMax = new Vector2(-horizontalPadding * Mathf.LerpUnclamped(1, 0, ((12 - horizontalFit.y) / 6f)) - halfSpacing, rt.offsetMax.y);

			//set the horizontal anchors based on the horizontal Fit
			rt.anchorMin = new Vector2(horizontalFit.x / 12f, rt.anchorMin.y);
			rt.anchorMax = new Vector2(horizontalFit.y / 12f, rt.anchorMax.y);


			//limit to a maximum width
			if (maxWidth > 0 && rt.rect.width > maxWidth)
			{
				//we're above the maxWidth, so we need to set to a fixed anchor instead
				float anchor = (rt.anchorMin.x + rt.anchorMax.x) * 0.5f;
				rt.anchorMin = new Vector2(anchor, rt.anchorMin.y);
				rt.anchorMax = new Vector2(anchor, rt.anchorMax.y);
				rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, maxWidth);
			}

			bRecalculating = false;
		}


		protected override void OnDisable()
		{
			m_drtt.Clear();
			LayoutRebuilder.MarkLayoutForRebuild((RectTransform)transform);
			base.OnDisable();
		}

		#region UIBehaviourOverrides

#if UNITY_EDITOR
		protected override void OnValidate()
		{
			base.OnValidate();
			if (horizontalFit.x < 0) horizontalFit.x = 0;
			if (horizontalFit.x > 12) horizontalFit.x = 12;
			if (horizontalFit.y < 0) horizontalFit.y = 0;
			if (horizontalFit.y > 12) horizontalFit.y = 12;
			if (horizontalFit.y < horizontalFit.x) horizontalFit.y = horizontalFit.x;
			if (!bRecalculating)
				bNeedToRecalculate = true; // Avoid to change rect transform during OnValidate callback
		}
#endif

		protected override void OnBeforeTransformParentChanged()
		{
			base.OnBeforeTransformParentChanged();
			if (!bRecalculating)
				Recalculate();
		}
		protected override void OnCanvasGroupChanged()
		{
			base.OnCanvasGroupChanged();
			if (!bRecalculating)
				Recalculate();
		}
		protected override void OnCanvasHierarchyChanged()
		{
			base.OnCanvasHierarchyChanged();
			if (!bRecalculating)
				Recalculate();
		}
		protected override void OnDidApplyAnimationProperties()
		{
			base.OnDidApplyAnimationProperties();
			if (!bRecalculating)
				Recalculate();
		}
		protected override void OnRectTransformDimensionsChange()
		{
			base.OnRectTransformDimensionsChange();
			if (!bRecalculating)
				Recalculate();
		}
		protected override void OnTransformParentChanged()
		{
			base.OnTransformParentChanged();
			if (!bRecalculating)
				Recalculate();
		}
		protected override void Start()
		{
			base.Start();
			if (!bRecalculating)
				Recalculate();
		}
#if UNITY_EDITOR
		public void Update()
		{
			if (bNeedToRecalculate)
			{
				Recalculate();
				bNeedToRecalculate = false;
			}
		}
#endif
		#endregion
	}
}