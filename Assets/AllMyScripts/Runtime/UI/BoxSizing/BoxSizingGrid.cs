﻿namespace AllMyScripts.Common.UI.BoxSizing
{
	using System.Collections.Generic;
    using AllMyScripts.Common.Utils;
    using UnityEngine;
	using UnityEngine.EventSystems;
	using UnityEngine.UI;

	/// <summary>
	/// A grid Layout of BoxSizingTransform
	/// </summary>
	[DisallowMultipleComponent]
	[ExecuteAlways]
	[RequireComponent(typeof(RectTransform))]
	public class BoxSizingGrid : UIBehaviour, ILayoutGroup
	{
		#region Properties

		public enum BoxSizingGridMode { FixedHeight, ChildExpandToHeight, FirstElementInRowHeight }
		public enum BoxSizingGridSortMode { None, LongerFirst };

		[Header("BoxSizingGrid")]

		[Tooltip("FixedHeight : All element will have the defined height" +
			"\n\nChildExpandToHeight : All line will have the same height, and they will be calculated from the rect height of this game object." +
			"\n\nFirstElementInRowHeight : All element in a line will have the height of the first element in the line")]
		[SerializeField]
		public BoxSizingGridMode ElementHeightMode = BoxSizingGridMode.FixedHeight;

		[Tooltip("None : Element are not sorted" +
			"\n\nLongerFirst : Element are sorted by how long there BoxSizingTransform is, putting the longest one first.")]
		[SerializeField]
		public BoxSizingGridSortMode ElementSortMode = BoxSizingGridSortMode.None;

		[SerializeField]
		[Range(0f, 100f)]
		private float LineSpacing = 0f;

		/// <summary>
		/// Serialized in <see cref="BoxSizingGridEditor"/>
		/// </summary>
		[HideInInspector]
		[SerializeField]
		private bool UpdateLayoutElementPreferedHeight = true;

		private List<BoxSizingTransform> boxSizingTransforms = new List<BoxSizingTransform>();

		private RectTransform rt;
		public RectTransform RectTransform
		{
			get
			{
				if (this.rt == null)
				{
					this.rt = GetComponent<RectTransform>();
				}

				return this.rt;
			}
		}

		/// <summary>
		/// Serialized in <see cref="BoxSizingGridEditor"/>
		/// </summary>
		[SerializeField]
		private LayoutElement layoutElement = default;
		public LayoutElement LayoutElement
		{
			get
			{
				return this.layoutElement;
			}
		}

		/// <summary>
		/// Serialized in <see cref="BoxSizingGridEditor"/>
		/// </summary>
		[HideInInspector]
		[SerializeField]
		private float fixedHeight = 50f;

		private bool bRecalculating = false;

		// Used to block the variables in the rectTransform
		private DrivenRectTransformTracker m_drtt = new DrivenRectTransformTracker();


		#endregion


		protected override void Awake()
		{
			rt = GetComponent<RectTransform>();
			base.Awake();

#if UNITY_EDITOR
			if (!Application.isPlaying && this.layoutElement == null)
			{
				this.layoutElement = this.GetComponent<LayoutElement>();
			}
#endif

		}


		public virtual void Recalculate()
		{

			this.bRecalculating = true;

			this.m_drtt.Add(this, this.RectTransform, DrivenTransformProperties.PivotY);

			this.boxSizingTransforms.Clear();
			List<GameObject> gameObjects = new List<GameObject>();

			// Search BoxSizingTransform in children
			foreach (BoxSizingTransform bst in this.GetComponentsInChildren<BoxSizingTransform>(false))
			{
				if (!gameObjects.Contains(bst.gameObject))
				{
					gameObjects.Add(bst.gameObject);
					this.boxSizingTransforms.Add(bst);
				}
			}

			// Sort BoxSizingTransform children
			if (this.ElementSortMode == BoxSizingGridSortMode.LongerFirst)
			{

				// Init a dictionnary(int size, List<BST> elements) with a list of box per size 
				Dictionary<int, List<BoxSizingTransform>> listOfBSTPerSize = new Dictionary<int, List<BoxSizingTransform>>();
				foreach (BoxSizingTransform bst in this.boxSizingTransforms)
				{
					if (listOfBSTPerSize.ContainsKey(bst.Width))
					{
						listOfBSTPerSize[bst.Width].Add(bst);
					}
					else
					{
						List<BoxSizingTransform> listBST = new List<BoxSizingTransform>();
						listBST.Add(bst);
						listOfBSTPerSize.Add(bst.Width, listBST);
					}
				}

				// For every width possible (start with 12)
				for (int i = 12; i > 0; --i)
				{
					if (listOfBSTPerSize.ContainsKey(i))
					{
						foreach (BoxSizingTransform bst in listOfBSTPerSize[i])
						{
							// Set as being the last sibling -> Since we do it for every BST, but on the longer first then on smaller bst, we sort bst in the hierarchy by how long they are.
							bst.RectTransform.SetAsLastSibling();
						}
					}
				}
			}

			int boxWidth = 0;
			float lineYStart = 0f;
			int lineCount = 1;

			float childExpandLineHeight = 0f;
			float lineHeight = -1f;

			// If we use the child expand layout option, we need to count the number of line beforehand
			if (this.ElementHeightMode == BoxSizingGridMode.ChildExpandToHeight)
			{
				foreach (BoxSizingTransform bst in this.boxSizingTransforms)
				{
					boxWidth += (bst.horizontalFit.y - bst.horizontalFit.x);
					// If this box sized goes out, start a new line
					if (boxWidth > 12)
					{
						++lineCount;
						boxWidth = (bst.horizontalFit.y - bst.horizontalFit.x);
					}
				}

				// Impossible, but i hate 0 division
				if (lineCount > 0)
				{
					float totalLineSpacingHeight = (lineCount - 1) * this.LineSpacing;
					childExpandLineHeight = (this.RectTransform.rect.height - totalLineSpacingHeight) / (float)lineCount;
				}

				// Reset the box counter
				boxWidth = 0;
			}

			// For each registered box
			for (int i = 0; i < this.boxSizingTransforms.Count; ++i)
			{
				BoxSizingTransform bst = this.boxSizingTransforms[i];

				// If the lineheight was not set
				if (lineHeight == -1f)
				{
					if (this.ElementHeightMode == BoxSizingGridMode.FirstElementInRowHeight)
					{
						lineHeight = bst.RectTransform.rect.height;
					}
					else if (this.ElementHeightMode == BoxSizingGridMode.FixedHeight)
					{
						lineHeight = this.fixedHeight;
					}
				}

				// Add the box width for this layout
				int boxWStart = boxWidth;
				boxWidth += (bst.horizontalFit.y - bst.horizontalFit.x);

				// If this box sized goes out, start a new line
				if (boxWidth > 12)
				{

					switch (this.ElementHeightMode)
					{
						case BoxSizingGridMode.ChildExpandToHeight:
							lineYStart += childExpandLineHeight + this.LineSpacing;
							break;
						case BoxSizingGridMode.FirstElementInRowHeight:
							++lineCount;
							lineYStart += lineHeight + this.LineSpacing;
							lineHeight = bst.RectTransform.rect.height;
							break;
						case BoxSizingGridMode.FixedHeight:
							++lineCount;
							lineYStart += lineHeight + this.LineSpacing;
							lineHeight = this.fixedHeight;
							break;
					}

					boxWStart = 0;
					boxWidth = (bst.horizontalFit.y - bst.horizontalFit.x);
				}

				// Set the horizontal fit according to the layout
				bst.horizontalFit = new Vector2Int(boxWStart, boxWidth);

				// Set the height of this child
				if (this.ElementHeightMode == BoxSizingGridMode.ChildExpandToHeight)
				{
					bst.RectTransform.SetHeight(childExpandLineHeight);
				}
				else
				{
					bst.RectTransform.SetHeight(lineHeight);
				}

				// Fix element pivot y to 1
				bst.RectTransform.anchorMin = new Vector2(bst.RectTransform.offsetMin.x, 1f);
				bst.RectTransform.anchorMax = new Vector2(bst.RectTransform.offsetMax.x, 1f);
				bst.RectTransform.pivot = new Vector2(bst.RectTransform.pivot.x, 1f);

				this.m_drtt.Add(bst.gameObject, bst.RectTransform, DrivenTransformProperties.PivotY | DrivenTransformProperties.AnchorMaxY | DrivenTransformProperties.AnchorMinY);

				this.RectTransform.pivot = new Vector2(this.RectTransform.pivot.x, 1f);


				// Set the y position, ignore grid pivot
				// float yPos = -(lineYStart);
				bst.RectTransform.anchoredPosition = new Vector2(bst.RectTransform.anchoredPosition.x, -lineYStart);
			}

			// Change the prefered height of the LayoutElement
			if (this.UpdateLayoutElementPreferedHeight && this.ElementHeightMode != BoxSizingGridMode.ChildExpandToHeight && this.layoutElement != null)
			{
				this.layoutElement.preferredHeight = lineYStart + lineHeight;
			}

			this.bRecalculating = false;
		}


		protected override void OnDisable()
		{
			m_drtt.Clear();
			LayoutRebuilder.MarkLayoutForRebuild((RectTransform)transform);
			base.OnDisable();
		}

		#region UIBehaviourOverrides

#if UNITY_EDITOR
		protected override void OnValidate()
		{
			base.OnValidate();
			if (!bRecalculating)
				LayoutRebuilder.MarkLayoutForRebuild((RectTransform)transform);
		}
#endif

		protected override void OnCanvasGroupChanged()
		{
			base.OnCanvasGroupChanged();
			if (!bRecalculating)
				Recalculate();
		}
		protected override void OnCanvasHierarchyChanged()
		{
			base.OnCanvasHierarchyChanged();
			if (!bRecalculating)
				Recalculate();
		}
		protected override void OnDidApplyAnimationProperties()
		{
			base.OnDidApplyAnimationProperties();
			if (!bRecalculating)
				Recalculate();
		}
		protected override void OnRectTransformDimensionsChange()
		{
			base.OnRectTransformDimensionsChange();
			if (!bRecalculating)
				Recalculate();
		}
		protected override void OnTransformParentChanged()
		{
			base.OnTransformParentChanged();
			if (!bRecalculating)
				Recalculate();
		}
		protected override void Start()
		{
			base.Start();
			if (!bRecalculating)
				Recalculate();
		}

		public void SetLayoutHorizontal()
		{
		}

		public void SetLayoutVertical()
		{
			Recalculate();
		}

		#endregion
	}
}