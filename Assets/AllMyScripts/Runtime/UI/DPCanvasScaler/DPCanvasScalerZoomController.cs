﻿namespace AllMyScripts.Common.UI.CanvasTools
{
    using UnityEngine;

    /// <summary>
    /// allows to zoom/Dezoom DPCanvasScaler using mousewheel or keyboard
    /// </summary>
    public class DPCanvasScalerZoomController : MonoBehaviour
    {
        void Update()
        {
            if(
#if !UNITY_STANDALONE_OSX||!UNITY_EDITOR_OSX
				Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)
#else
				Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.RightCommand)
#endif
				)
			{
                if(Input.GetAxis("Mouse ScrollWheel") != 0)
                {
                    DPCanvasScaler.userZoom += .1f * Mathf.Sign(Input.GetAxis("Mouse ScrollWheel"));
                }
                if (Input.GetKeyDown(KeyCode.KeypadPlus) || Input.GetKeyDown(KeyCode.Plus))
                {
                    DPCanvasScaler.userZoom += 0.1f;
                }
                if (Input.GetKeyDown(KeyCode.Minus) || Input.GetKeyDown(KeyCode.KeypadMinus))
                {
                    DPCanvasScaler.userZoom -= 0.1f;
                }

                if(Input.GetKeyDown(KeyCode.Alpha0) || Input.GetKeyDown(KeyCode.Keypad0))
                {
                    DPCanvasScaler.userZoom = 1;
                }
            }
        }
    }
}