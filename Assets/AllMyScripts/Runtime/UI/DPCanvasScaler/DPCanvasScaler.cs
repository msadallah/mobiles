﻿/*
    This is a modified version of Unity's CanvasScaler, as found here:
    https://bitbucket.org/Unity-Technologies/ui/src/fadfa14d2a5c?at=4.6
    
    I share it under the same MIT/X11 license.
    
    Modder: Tess Snider, Hidden Achievement
*/
namespace AllMyScripts.Common.UI.CanvasTools
{
	using System.Collections.Generic;
	using AllMyScripts.Common.Tools;
	using UnityEngine;
	using UnityEngine.EventSystems;
	using UnityEngine.UI;

	[RequireComponent(typeof(Canvas))]
	[ExecuteInEditMode]
	[AddComponentMenu("Layout/DP Canvas Scaler")]
	public class DPCanvasScaler : UIBehaviour
	{
		private const string ZOOM_PLAYERPREFS_KEY = "Canvas_Scaler_UserZoom";

		private static List<DPCanvasScaler> s_instances = new List<DPCanvasScaler>();

		#region Zoom
		/// <summary>
		/// the minimum and maximum values for the DPCanvasScaler Zoom
		/// </summary>
		public static Vector2 s_zoomMinMax = new Vector2(0.4f,2.5f);

		/// <summary>
		/// the zoom on the scale set by the user
		/// </summary>
		private static float s_userZoom = 1;

		/// <summary>
		/// the zoom on the scale set by the user
		/// </summary>
		public static float userZoom
		{
			get
			{
				return s_userZoom;
			}
			set
			{
				value = Mathf.Clamp(value, s_zoomMinMax.x, s_zoomMinMax.y);

				s_userZoom = value;
				PlayerPrefs.SetFloat(ZOOM_PLAYERPREFS_KEY, s_userZoom);
				PlayerPrefsExt.SetDirty();
				foreach (var canvas in s_instances)
				{
					canvas.Handle();
				}
				for (int i = 0; i < onZoomChanged.Count; ++i)
					onZoomChanged[i].OnZoomChanged(s_userZoom);


			}
		}

		public interface IZoomListener { void OnZoomChanged(float zoomLevel); }

		public static InterfaceDelegate<IZoomListener> onZoomChanged = new InterfaceDelegate<IZoomListener>();
		#endregion

#if UNITY_EDITOR
		public const string PLAYER_PREF_DP_CANVAS_SCALER_SIMUL_DPI = "DPCanvasScalerDpi";
		public const string PLAYER_PREF_DP_CANVAS_SCALER_SIMUL_SIZE_X = "DPCanvasScalerSizeX";
		public const string PLAYER_PREF_DP_CANVAS_SCALER_SIMUL_SIZE_Y = "DPCanvasScalerSizeY";
#endif

		public interface IScaleFactor { void OnScaleFactorChanged(DPCanvasScaler dpCanvasScalor); }
		private InterfaceDelegate<IScaleFactor> m_scaleFactorDelegate = new InterfaceDelegate<IScaleFactor>();
		public InterfaceDelegate<IScaleFactor> scaleFactorDelegate { get { return m_scaleFactorDelegate; } }

		/// <summary>
		/// The log base doesn't have any influence on the results whatsoever, as long as the same base is used everywhere.
		/// </summary>
		private const float kLogBase = 2f;

		/// <summary>
		/// If this is set, the following parameters will be set according to the copy
		/// </summary>
		[Tooltip("If this is set, the following parameters will be set according to the copy")]
		[SerializeField]
		private DPCanvasScaler m_copyParametersFrom = default;


		[Header("Canvas Scaler")]
		[Tooltip("If a sprite has this 'Pixels Per Unit' setting, then one pixel in the sprite will cover one unit in the UI.")]
		[SerializeField]
		protected float m_ReferencePixelsPerUnit = 100f;
		public float referencePixelsPerUnit { get { return m_ReferencePixelsPerUnit; } set { m_ReferencePixelsPerUnit = value; } }


		[Header("DP Canvas Scaler")]
		[Tooltip("The DPI to assume if the screen DPI is not known.")]
		[SerializeField]
		protected float m_FallbackScreenDPI = 96f;
		public float fallbackScreenDPI { get { return m_FallbackScreenDPI; } set { m_FallbackScreenDPI = value; } }

		// World Canvas settings
		[Tooltip("The amount of pixels per unit to use for dynamically created bitmaps in the UI, such as Text.")]
		[SerializeField]
		protected float m_DynamicPixelsPerUnit = 1f;
		public float dynamicPixelsPerUnit { get { return m_DynamicPixelsPerUnit; } set { m_DynamicPixelsPerUnit = value; } }

		/// <summary>
		/// should we recalculate the DPI based on the camera's width (not recommended if the canvas is fullscreen)
		/// </summary>
		[Tooltip("should we recalculate the DPI based on the camera's width (not recommended if the canvas is fullscreen)")]
		[SerializeField]
		protected bool m_bDPICameraWidthBound = false;
		/// <summary>
		/// should we recalculate the DPI based on the camera's width (not recommended if the canvas is fullscreen)
		/// </summary>
		public bool bDPICameraWidthBound { get { return m_bDPICameraWidthBound; } set { m_bDPICameraWidthBound = value; } }

		[Header("Mobile only Parameters")]
		[SerializeField]
		protected float m_fMinSizeCoef = 0.6f;
		[SerializeField]
		protected float m_fMaxSizeCoef = 1.5f;
		[SerializeField]
		protected float m_fMinPhysicWidth = 2.3f;
		[SerializeField]
		protected float m_fMaxPhysicWidth = 8f;
		[SerializeField]
		protected float _minimumVirtualWidth = 340f;

		[Header("- Desktop/WebGL only Parameters")]
		[SerializeField]
		[Tooltip("Minimal physical size in inches. If the screen is smaller, the DPI will be scaled accordingly")]
		protected Vector2 m_fMinPhysicalSizeInches;

		[SerializeField]
		[Tooltip("Maximum physical size in inches. If the screen is bigger, the DPI will be scaled accordingly")]
		protected Vector2 m_fMaxPhysicalSizeInches;


		[Header("- Debugging Tools")]

		[Tooltip("is simulation like on mobile, or desktop?")]
		[SerializeField]
		protected bool m_bMobileSimulate = true;
		public bool bMobileSimulate { get { return m_bMobileSimulate; } set { m_bMobileSimulate = value; } }

		[SerializeField]
		protected DPCanvasScalerSettings m_settings;
		public DPCanvasScalerSettings settings { get { return m_settings; } set { m_settings = value; } }

		[Tooltip("Scale Factor Custom Coef")]
		[SerializeField]
		protected float m_fScaleFactorCustomCoef = 1f;
		public float scaleFactorCustomCoef { get { return m_fScaleFactorCustomCoef; } set { m_fScaleFactorCustomCoef = value; } }

		[Tooltip("Simulation to a specific Resolution")]
		[SerializeField]
		protected Vector2 m_v2SimulateScreenSize = Vector2.zero;
#if UNITY_EDITOR
		public delegate void OnScreenSizeChanged(Vector2 v2Size);
		public static OnScreenSizeChanged s_onScreenSizeChanged;

		public Vector2 simulateScreenSize
		{
			get { return m_v2SimulateScreenSize; }
			set
			{
				m_v2SimulateScreenSize = value;
				if(s_onScreenSizeChanged != null)
					s_onScreenSizeChanged(value);
			}
		}
#else
		[System.NonSerialized]public Vector2 simulateScreenSize = Vector2.zero;
#endif

		[Tooltip("Simulation to a specific DPI")]
		[SerializeField]
		protected float m_SimulateScreenDPI = 0f;
#if UNITY_EDITOR
		public float simulateScreenDPI { get { return m_SimulateScreenDPI; } set { m_SimulateScreenDPI = value; } }
#else
		[System.NonSerialized]public const float simulateScreenDPI = 0;
#endif
		// General variables
		[System.NonSerialized]
		protected Canvas m_Canvas;
		[System.NonSerialized]
		protected float m_PrevScaleFactor = 1;
		[System.NonSerialized]
		protected float m_PrevReferencePixelsPerUnit = 100;
		[System.NonSerialized]
		protected float m_fSizeCoefOnScaleFactor = 1f;
		public float sizeCoefOnScaleFactor { get { return m_fSizeCoefOnScaleFactor; } }
		[System.NonSerialized]
		protected float m_fCurrentDpi = 500f;
		public float currentDpi { get { return m_fCurrentDpi; } }
		public float currentScaleFactor { get { return m_Canvas.scaleFactor; } }

		public float currentWidth
		{
			get
			{
				if (simulateScreenSize.x > 0f)
					return simulateScreenSize.x;

				if (m_Canvas == null)
					return Screen.safeArea.width;

				if (m_Canvas.renderMode == RenderMode.ScreenSpaceCamera && m_Canvas.worldCamera != null)
					return m_Canvas.worldCamera.scaledPixelWidth;

				return Screen.safeArea.width;
			}
		}
		public float currentHeight
		{
			get
			{
				if (simulateScreenSize.y > 0f)
					return simulateScreenSize.y;

				if (m_Canvas == null)
					return Screen.safeArea.height;

				if (m_Canvas.renderMode == RenderMode.ScreenSpaceCamera && m_Canvas.worldCamera != null)
					return m_Canvas.worldCamera.scaledPixelHeight;

				return Screen.safeArea.height;
			}
		}

		[System.NonSerialized] public float gameWidth;
		[System.NonSerialized] public float gameHeight;
		/// <summary>
		/// returns the camera of the canvas if there is one, null otherwise
		/// </summary>
		private Camera GetCameraCanvas()
		{
			if (m_Canvas.renderMode == RenderMode.ScreenSpaceCamera)
			{
				return m_Canvas.worldCamera;
			}
			return null;
		}

		protected DPCanvasScaler() { }

		public float CanvasWidth { get { return currentWidth / currentScaleFactor; } }
		public float CanvasHeight { get { return currentHeight / currentScaleFactor; } }

		protected override void Awake()
		{
			base.Awake();
			s_instances.Add(this);
#if UNITY_EDITOR
			m_SimulateScreenDPI = PlayerPrefs.GetFloat(PLAYER_PREF_DP_CANVAS_SCALER_SIMUL_DPI + "_" + Application.productName, m_SimulateScreenDPI);
			m_v2SimulateScreenSize.x = PlayerPrefs.GetFloat(PLAYER_PREF_DP_CANVAS_SCALER_SIMUL_SIZE_X + "_" + Application.productName, m_v2SimulateScreenSize.x);
			m_v2SimulateScreenSize.y = PlayerPrefs.GetFloat(PLAYER_PREF_DP_CANVAS_SCALER_SIMUL_SIZE_Y + "_" + Application.productName, m_v2SimulateScreenSize.y);
#endif
			//load the user's zoom
#if !UNITY_EDITOR //we "forget" the userzoom for the editor to avoid programmers accidentally working with a different zoom than x1
            s_userZoom = PlayerPrefs.GetFloat(ZOOM_PLAYERPREFS_KEY, s_userZoom);
#else
			s_userZoom = 1;
#endif
		}

		protected override void OnEnable()
		{
			base.OnEnable();

#if (UNITY_ANDROID || UNITY_IOS) //editor and deskttop/webgl
			bMobileSimulate = true;
#endif


			m_Canvas = GetComponent<Canvas>();
			Handle();
		}

		protected override void OnDisable()
		{
			SetScaleFactor(1);
			SetReferencePixelsPerUnit(100);
			base.OnDisable();
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
			s_instances.Remove(this);
		}

		protected virtual void Update()
		{
#if UNITY_EDITOR
			CanvasScaler cs= gameObject.GetComponent<CanvasScaler>();
			if (cs != null)
			{
				DestroyImmediate(cs);
			}
			Handle();
#endif
		}

		protected override void OnRectTransformDimensionsChange()
		{
			base.OnRectTransformDimensionsChange();
			Handle();
		}
		protected virtual void Handle()
		{
			GetCopiedParameters();
			//we require these variables for the inspector, and for later
			gameWidth = currentWidth;
			gameHeight = currentHeight;

			//if we're in a mobile application in landscape, switch the behaviour
			if(gameWidth>gameHeight && bMobileSimulate)
			{
				var tmp = gameWidth;
				gameWidth = gameHeight;
				gameHeight = tmp;
			}

			if (m_Canvas == null || !m_Canvas.isRootCanvas)
				return;

			if (m_Canvas.renderMode == RenderMode.WorldSpace)
			{
				HandleWorldCanvas();
				return;
			}

			HandleConstantPhysicalSize();
		}

		protected virtual void HandleWorldCanvas()
		{
			SetScaleFactor(m_DynamicPixelsPerUnit);
			SetReferencePixelsPerUnit(m_ReferencePixelsPerUnit);
		}

		protected virtual void HandleConstantPhysicalSize()
		{
			float currentDpi = simulateScreenDPI > 0f ? simulateScreenDPI : GlobalTools.GetDPI();
			m_fCurrentDpi = (currentDpi == 0 ? m_FallbackScreenDPI : currentDpi);
			//Debug.Log( "HandleConstantPhysicalSize dpi " + dpi );

			//The targetDPI is used as reference. The smaller the value, the bigger the content.
			//on mobile, HTML changes it to 160. however, for some reason, we had to fiddle
			float targetDPI = 96f;

			bool bUseSizeCoef = true;

#if UNITY_EDITOR || !(UNITY_ANDROID || UNITY_IOS) //editor and deskttop/webgl
			if (simulateScreenDPI == 0f)
				bUseSizeCoef = false;
#else//mobile
			bMobileSimulate = true;
#endif
			if (m_bDPICameraWidthBound)
			{
				Camera camera = GetCameraCanvas();
				if (camera != null)
				{
					//we are cameraRendering, multiply the dpi by its width
					m_fCurrentDpi *= camera.rect.width;
				}
			}

			if (bUseSizeCoef && bMobileSimulate)
			{
				float fScreenWidth = gameWidth;
				float fPhysicScreenWidth = fScreenWidth / m_fCurrentDpi;
				m_fSizeCoefOnScaleFactor = Mathf.Lerp(m_fMinSizeCoef, m_fMaxSizeCoef, Mathf.InverseLerp(m_fMinPhysicWidth, m_fMaxPhysicWidth, fPhysicScreenWidth));
				float potentialScaleFactor = m_fCurrentDpi / targetDPI * m_fSizeCoefOnScaleFactor;
				if (fScreenWidth / potentialScaleFactor < _minimumVirtualWidth)
					m_fSizeCoefOnScaleFactor = fScreenWidth / _minimumVirtualWidth * targetDPI / m_fCurrentDpi;
			}
			else
			{
				m_fSizeCoefOnScaleFactor = 1f;
				if (m_fMinPhysicalSizeInches.magnitude > 0 || m_fMaxPhysicalSizeInches.magnitude > 0)
				{
					//we need to check if the physical size of the screen is enough for our DPI
					float fPhysicW = gameWidth / currentDpi;
					float fPhysicH = gameHeight / currentDpi;

					int worstCaseScenario = 0; //will be 1, 2, 3 or 4 depending on what is the most problematic
					float worstDelta = 0;


					//is it too thin?
					if (m_fMinPhysicalSizeInches.magnitude > 0 && fPhysicW < m_fMinPhysicalSizeInches.x)
					{
						worstCaseScenario = 1;
						worstDelta = m_fMinPhysicalSizeInches.x - fPhysicW;

					}
					//is it too large?
					else if (m_fMaxPhysicalSizeInches.magnitude > 0 && fPhysicW > m_fMaxPhysicalSizeInches.x)
					{
						worstCaseScenario = 2;
						worstDelta = fPhysicW - m_fMaxPhysicalSizeInches.x;
					}

					//is it too small?
					if (m_fMinPhysicalSizeInches.magnitude > 0 && fPhysicH < m_fMinPhysicalSizeInches.y)
					{
						float delta = m_fMinPhysicalSizeInches.y - fPhysicH;
						if (worstCaseScenario == 0 || worstDelta < delta)
						{
							worstCaseScenario = 3;
							worstDelta = delta;
						}

					}
					//is it too tall?
					else if (m_fMaxPhysicalSizeInches.magnitude > 0 && fPhysicH > m_fMaxPhysicalSizeInches.y)
					{
						float delta = fPhysicH - m_fMaxPhysicalSizeInches.y;
						if (worstCaseScenario == 0 || worstDelta < delta)
						{
							worstCaseScenario = 4;
							worstDelta = delta;
						}
					}

					//we now know what is the worst "out of limits" we've reached, and set the size coef accordingly
					switch (worstCaseScenario)
					{
						case 1:
						case 2:
							//the width isn't good
							m_fSizeCoefOnScaleFactor = fPhysicW / (worstCaseScenario == 1 ? m_fMinPhysicalSizeInches : m_fMaxPhysicalSizeInches).x;
							break;
						case 3:
						case 4:
							//the height isn't good
							m_fSizeCoefOnScaleFactor = fPhysicH / (worstCaseScenario == 3 ? m_fMinPhysicalSizeInches : m_fMaxPhysicalSizeInches).y;
							break;
					}

					//this shouldn't happen but after such a division I'd rather be safe
					if (float.IsInfinity(m_fSizeCoefOnScaleFactor))
					{
						m_fSizeCoefOnScaleFactor = 1;
					}
				}
			}

#if UNITY_EDITOR
			if (m_fScaleFactorCustomCoef <= 0f)
				m_fScaleFactorCustomCoef = 0.001f;
#endif

			SetScaleFactor(s_userZoom * (m_fCurrentDpi / targetDPI * m_fScaleFactorCustomCoef * m_fSizeCoefOnScaleFactor));

			SetReferencePixelsPerUnit(m_ReferencePixelsPerUnit);
		}

		protected void SetScaleFactor(float scaleFactor)
		{
			//Debug.Log( "SetScaleFactor " + scaleFactor );

			if (scaleFactor == m_PrevScaleFactor)
				return;
			m_Canvas.scaleFactor = scaleFactor;
			m_PrevScaleFactor = scaleFactor;

			int nCount = scaleFactorDelegate.Count;
			for (int i = 0; i < nCount; ++i)
				scaleFactorDelegate[i].OnScaleFactorChanged(this);
		}

		protected void SetReferencePixelsPerUnit(float referencePixelsPerUnit)
		{
			if (referencePixelsPerUnit == m_PrevReferencePixelsPerUnit)
				return;

			m_Canvas.referencePixelsPerUnit = referencePixelsPerUnit;
			m_PrevReferencePixelsPerUnit = referencePixelsPerUnit;
		}

#if UNITY_EDITOR
		protected override void OnValidate()
		{
			GetCopiedParameters();
		}
#endif

		public void GetCopiedParameters()
		{
			if (m_copyParametersFrom == null)
				return;


			m_ReferencePixelsPerUnit = m_copyParametersFrom.m_ReferencePixelsPerUnit;

			m_FallbackScreenDPI = m_copyParametersFrom.m_FallbackScreenDPI;
			m_DynamicPixelsPerUnit = m_copyParametersFrom.m_DynamicPixelsPerUnit;
			m_bDPICameraWidthBound = m_copyParametersFrom.m_bDPICameraWidthBound;

			m_fMaxPhysicWidth = m_copyParametersFrom.m_fMaxPhysicWidth;
			m_fMinPhysicWidth = m_copyParametersFrom.m_fMinPhysicWidth;
			m_fMaxSizeCoef = m_copyParametersFrom.m_fMaxSizeCoef;
			m_fMinSizeCoef = m_copyParametersFrom.m_fMinSizeCoef;

			m_fMaxPhysicalSizeInches = m_copyParametersFrom.m_fMaxPhysicalSizeInches;
			m_fMinPhysicalSizeInches = m_copyParametersFrom.m_fMinPhysicalSizeInches;

			m_bMobileSimulate = m_copyParametersFrom.m_bMobileSimulate;
			m_settings = m_copyParametersFrom.m_settings;
			m_fScaleFactorCustomCoef = m_copyParametersFrom.m_fScaleFactorCustomCoef;
			m_SimulateScreenDPI = m_copyParametersFrom.m_SimulateScreenDPI;
			m_v2SimulateScreenSize = m_copyParametersFrom.m_v2SimulateScreenSize;
		}
	}
}