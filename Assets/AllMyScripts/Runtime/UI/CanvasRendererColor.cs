﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// CanvasRenderer has the ability to change color. However, on disable, it loses the color it was attributed.
/// CanvasRendererColor is a "memory" for the CanvasRenderer's color.
/// </summary>
[ExecuteAlways] //ExecuteAlways in order to be WYSIWYG
[DisallowMultipleComponent] //We can't have more than one CanvasRendereColor or else we'll have conflicts
public class CanvasRendererColor : MonoBehaviour
{
	[Header("Colors is visible as Read Only, it must be modified by script.")]
	[ReadOnly][SerializeField] Color _color;

	CanvasRenderer canvasRenderer
	{
		get
		{
			if(_canvasRenderer==null)
			{
				_canvasRenderer = GetComponent<CanvasRenderer>();
			}
			return _canvasRenderer;
		}
	}
	CanvasRenderer _canvasRenderer;


	public static CanvasRendererColor SetColor(GameObject go,Color color)
	{
#if UNITY_EDITOR
		if(go.GetComponents<CanvasRendererColor>().Length>1)
		{
			Debug.LogWarning("Multiple Canvas Renderer Color in " + go + "\nplease delete ALL CanvasRendererColors or this may lead to unexpected behaviours.", go);
		}
#endif
		var result = go.GetComponent<CanvasRendererColor>();
		if(result==null)
		{
#if UNITY_EDITOR
			if((go.hideFlags & HideFlags.NotEditable) == 0)
#endif
			result = go.AddComponent<CanvasRendererColor>();
		}
		if(result!=null) result.SetColor(color);
		return result;
	}

	public void SetColor(Color color)
	{
		_color = color;
		canvasRenderer.SetColor(_color);
	}

	public Color GetColor()
	{
		return _color;
	}

	private void OnEnable()
	{
		canvasRenderer.SetColor(_color);
	}
}
public static class CanvasRendererExtension
{
	public static void SetConstantColor(this CanvasRenderer cr, Color color)
	{
		if(cr!=null&&cr.gameObject!=null)
			CanvasRendererColor.SetColor(cr.gameObject, color);
	}
}