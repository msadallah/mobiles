﻿namespace AllMyScripts.Common.UI.CanvasTools
{
	using UnityEngine;
	using System.Collections;
#if UNITY_EDITOR
	using UnityEditor;
#endif

	[CreateAssetMenu (menuName = "DPCanvasScalerSettings")]
	public class DPCanvasScalerSettings : ScriptableObject
	{
		#region Struct

		[System.Serializable]
		public struct DeviceSimulationData
		{
			public string sName;
			public float fDpi;
			public int nResX;
			public int nResY;
		}

		#endregion

		[SerializeField]
		protected DeviceSimulationData[] m_deviceSimulationArray = null;
		public DeviceSimulationData[] deviceSimulationArray { get { return m_deviceSimulationArray; } }

#if UNITY_EDITOR
		[MenuItem ("ScriptableObjects/Create an DPCanvasScalerSettings")]
		public static void CreateAsset ()
		{
			var asset = ScriptableObject.CreateInstance<DPCanvasScalerSettings>();
			AssetDatabase.CreateAsset (asset, "Assets/DPCanvasScalerSettings.asset");
			AssetDatabase.SaveAssets ();

			EditorUtility.FocusProjectWindow ();
			Selection.activeObject = asset;
		}

		[ContextMenu ("Reorder by DPI")]
		public void ReorderByDPI ()
		{
			System.Array.Sort (m_deviceSimulationArray, new DeviceComparer ());
		}
		public class DeviceComparer : IComparer
		{
			public int Compare (object x, object y)
			{
				return ((DeviceSimulationData) x).fDpi.CompareTo (((DeviceSimulationData) y).fDpi);
			}
		}
#endif
	}
}