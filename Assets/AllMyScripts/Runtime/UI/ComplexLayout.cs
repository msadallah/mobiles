﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// when a Layout has a complex autobuilding hierarchy (horizontal layout inside of a layout with auto sized texts etc...)
/// </summary>
[ExecuteAlways]
public class ComplexLayout : UIBehaviour
{
	[Tooltip("The complexity, meaning how many times we'll rebuild the layout")]
	public int complexity = 2;
	private int rebuildCount = 0;
	protected override void OnRectTransformDimensionsChange()
	{
		base.OnRectTransformDimensionsChange();
		Rebuild();
	}

	[ContextMenu("Rebuild")]
	public void Rebuild(bool immediate = false)
	{
		rebuildCount = 0;
		if (immediate) LateUpdate();
	}
	private void LateUpdate()
	{
		while (rebuildCount < complexity)
		{
			foreach(RectTransform rt in GetComponentsInChildren<RectTransform>())
			{
				//the buttons are autosized in an autosized layout. We have to rebuild it multiple times
				LayoutRebuilder.ForceRebuildLayoutImmediate(rt);
			}
			rebuildCount++;
		}
	}
	protected override void Start()
	{
		base.Start();
		Rebuild();
	}
}
