﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UISelectOnAwake : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
		EventSystem.current.SetSelectedGameObject(gameObject);
    }
}
