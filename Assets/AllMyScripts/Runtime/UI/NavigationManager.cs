﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NavigationManager : MonoBehaviour {
	private EventSystem system;

	private void Awake()
	{
		system = GetComponent<EventSystem>();
	}

	private float fLastAutoTab = -2;
	private float fLastTabPress = -2;
	private const float TIME_BETWEEN_TABS = 0.05f;
	private const float TIME_BEFORE_AUTOTABS = .75f;
	public void Update()
	{
		DeselectButton();

		bool bTabInput = false;
		//manage tab key being held
		if(Input.GetKeyDown(KeyCode.Tab))
		{
			bTabInput = true;
			fLastTabPress = Time.time;
		}
		if (Input.GetKey(KeyCode.Tab))
		{
			if(fLastTabPress < Time.time-TIME_BEFORE_AUTOTABS && fLastAutoTab<Time.time - TIME_BETWEEN_TABS)
			{
				bTabInput = true;
				fLastAutoTab = Time.time;
			}
		}

		//Manage down on tab and up in shift+tab
		if (bTabInput)
		{ 
			Selectable next;
			bool bDown =Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
			if (system.currentSelectedGameObject==null)
			{
				var selectables = new List<TMPro.TMP_InputField>(GameObject.FindObjectsOfType<TMPro.TMP_InputField>());
				//select the first one
				if (!bDown)
				{
					next = selectables[0];
				}
				else
				{
					next = selectables[selectables.Count - 1];
				}
			}

			else
			{
				var current = system.currentSelectedGameObject.GetComponent<TMPro.TMP_InputField>();

				//we only loop between elements of the same root canvas
				var selectables = new List<TMPro.TMP_InputField>(current.GetRT().GetRootCanvas().GetComponentsInChildren<TMPro.TMP_InputField>());
				selectables.Reverse();
				int nCurrent = selectables.IndexOf(system.currentSelectedGameObject.GetComponent<TMPro.TMP_InputField>());


				if (!bDown)
				{
					//going up
					if(nCurrent>0)
						next = selectables[nCurrent-1];
					else
						next = selectables[selectables.Count - 1];
				}

				else
				{
					//going down
					if(nCurrent<selectables.Count-1)
						next = selectables[nCurrent + 1];
					else
						next = selectables[0];
				}

			}
			if (next != null)
			{

				InputField inputfield = next.GetComponent<InputField>();
				if (inputfield != null) inputfield.OnPointerClick(new PointerEventData(system));  //if it's an input field, also set the text caret

				system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
			}
			//else Debug.Log("next nagivation element not found");
		}

		//manage enter key
		if((Input.GetKeyDown(KeyCode.KeypadEnter)|| Input.GetKeyDown(KeyCode.Return))
			&&
			system.currentSelectedGameObject!=null
			)
		{
			SubmitOnEnter sor = system.currentSelectedGameObject.GetComponent<SubmitOnEnter>();
			if (sor!=null && sor.buttonToPress.interactable)
			{
				ExecuteEvents.Execute(sor.buttonToPress.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);
				if (sor.toSelectOnPress != null)
				{
					system.SetSelectedGameObject(sor.toSelectOnPress.GetComponentInChildren<Selectable>().gameObject, new BaseEventData(system));
				}
			}
		}
	}

	private void DeselectButton()
	{
		if(system.currentSelectedGameObject != null && system.currentSelectedGameObject.GetComponent<Button>())
		{
			system.SetSelectedGameObject(null);
		}
	}
}
