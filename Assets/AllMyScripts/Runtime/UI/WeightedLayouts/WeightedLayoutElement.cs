using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif

[AddComponentMenu("Layout/lw/Layout Weighted Element", 140)]
    [RequireComponent(typeof(RectTransform))]
    [ExecuteAlways]
public class WeightedLayoutElement : UIBehaviour
{
	public bool useWeight = true;
	[SerializeField]
#if ODIN_INSPECTOR
	[ShowIf("useWeight")]
#endif
	private uint m_nWeight = 1;

	public bool useAdditionalPixels = false;
	[SerializeField]
#if ODIN_INSPECTOR
	[ShowIf("useAdditionalPixels")]
#endif
	private float m_AdditionalPixels = 0;

	public uint nWeight
	{
		get
		{
			return m_nWeight;
		}
		set
		{
			if (m_nWeight != value)
			{
				m_nWeight = value;
				SetDirty();
			}
		}
	}
	public float nAdditionalPixels
	{
		get
		{
			return m_AdditionalPixels;
		}
		set
		{
			if (m_AdditionalPixels != value)
			{
				m_AdditionalPixels = value;
				SetDirty();
			}
		}
	}

	protected void SetDirty()
    {
        if (!IsActive())
            return;
        LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
    }
    #if UNITY_EDITOR
        protected override void OnValidate()
        {
            SetDirty();
        }

    #endif
}
