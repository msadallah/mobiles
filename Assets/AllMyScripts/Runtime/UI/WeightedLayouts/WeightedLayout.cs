using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

/// <summary>
/// Apply this UIBehaviour to a Panel in order to distribute its children using weights
/// </summary>
[ExecuteAlways]
[AddComponentMenu("Layout/lw/Weighted Layout", 140)]
public class WeightedLayout : UIBehaviour, ILayoutGroup
{
	#region bool bChildForceExpand
	[SerializeField]
	[Tooltip("If layout is vertical, forces the object to take all the horizontal space, and vice-versa")]
	protected bool m_bChildForceExpand = true;

	public bool bChildForceExpand
	{
		get { return m_bChildForceExpand; }
		set { m_bChildForceExpand = value; SetDirty(); }
	}
	#endregion
	#region LayoutType eLayout
	[SerializeField]
	protected LayoutType m_eLayout = default;
	public LayoutType eLayout
	{
		get { return m_eLayout; }
		set { m_eLayout = value; SetDirty(); }
	}
	#endregion
	#region float fSpacing
	[SerializeField]
	[Tooltip("Distance in pixels separating elements")]
	protected float m_fSpacing = default;

	public float fSpacing
	{
		get { return m_fSpacing; }
		set { m_fSpacing = value; SetDirty(); }
	}

	
	#endregion



	private bool isRootLayoutGroup
	{
		get
		{
			Transform parent = transform.parent;
			if (parent == null)
				return true;
			return transform.parent.GetComponent(typeof(ILayoutGroup)) == null;
		}
	}

	public enum LayoutType
	{
		Vertical,
		Horizontal
	}

	// Used to block the variables in the rectTransform
	private DrivenRectTransformTracker m_drtt = new DrivenRectTransformTracker();



	#region Unity Lifetime calls

	protected override void OnEnable()
	{
		base.OnEnable();
		SetDirty();
	}

	protected override void OnDisable()
	{
		m_drtt.Clear();
		LayoutRebuilder.MarkLayoutForRebuild((RectTransform)transform);
		base.OnDisable();
	}

	protected override void OnDidApplyAnimationProperties()
	{
		SetDirty();
	}


	protected override void OnRectTransformDimensionsChange()
	{
		base.OnRectTransformDimensionsChange();
		if (isRootLayoutGroup)
			SetDirty();
	}

	protected void OnTransformChildrenChanged()
	{
		SetDirty();
	}
	#endregion

	override protected void Awake()
	{
		base.Awake();
		SetDirty();
	}
	/// <summary>
	/// Implemented by ILayoutGroup
	/// </summary>
	public void SetLayoutHorizontal()
	{
		if (m_eLayout == LayoutType.Horizontal)
		{
			//get all the children and their weights
			RectTransform[] rtChildren;
			uint[] nWeights;
			float[] nPixels;
			bool[] bIgnore;
			uint totalWeight = CalculateChildrenWeights(out rtChildren, out nWeights, out bIgnore, out nPixels);


			float width = ((RectTransform)transform).rect.width;
			
			//remove the pixels from the available width
			foreach (var pixels in nPixels) width -= pixels;

			//remove the spacing from the available width
			width -= (nWeights.Length - 1) * m_fSpacing;


			float fCurrentPosition = 0; 
			for (int i = 0; i < rtChildren.Length; ++i)
			{
				if (bIgnore[i]) continue;
				rtChildren[i].anchorMin = new Vector2(0, m_bChildForceExpand ? 0 : rtChildren[i].anchorMin.y);
				rtChildren[i].anchorMax = new Vector2(0, m_bChildForceExpand ? 1 : rtChildren[i].anchorMax.y);

				rtChildren[i].offsetMin = new Vector2(fCurrentPosition, m_bChildForceExpand ? 0 : rtChildren[i].offsetMin.y);

				fCurrentPosition += nPixels[i];
				if(totalWeight!=0)
					fCurrentPosition += (nWeights[i]/(float)totalWeight) * width;

				rtChildren[i].offsetMax = new Vector2(fCurrentPosition, m_bChildForceExpand ? 0 : rtChildren[i].offsetMax.y);
				fCurrentPosition += m_fSpacing;
			}
		}
	}

	/// <summary>
	/// Implemented by ILayoutGroup
	/// </summary>
	public void SetLayoutVertical()
	{
		//see SetLayoutHorizontal for details
		if (m_eLayout == LayoutType.Vertical)
		{

			//get all the children and their weights
			RectTransform[] rtChildren;
			uint[] nWeights;
			float[] nPixels;
			bool[] bIgnore;
			uint totalWeight = CalculateChildrenWeights(out rtChildren, out nWeights, out bIgnore, out nPixels);


			float height = ((RectTransform)transform).rect.height;

			//remove the pixels from the available width
			foreach (var pixels in nPixels) height -= pixels;

			//remove the spacing from the available width
			height -= (nWeights.Length - 1) * m_fSpacing;


			float fCurrentPosition = 0;
			for (int i = rtChildren.Length-1; i >=0 ; --i)
			{
				if (bIgnore[i]) continue;
				rtChildren[i].anchorMin = new Vector2(m_bChildForceExpand ? 0 : rtChildren[i].anchorMin.x,0);
				rtChildren[i].anchorMax = new Vector2(m_bChildForceExpand ? 1 : rtChildren[i].anchorMax.x,0);

				rtChildren[i].offsetMin = new Vector2(m_bChildForceExpand ? 0 : rtChildren[i].offsetMin.x, fCurrentPosition);

				fCurrentPosition += nPixels[i];
				if (totalWeight != 0)
					fCurrentPosition += (nWeights[i] / (float)totalWeight) * height;

				rtChildren[i].offsetMax = new Vector2(m_bChildForceExpand ? 0 : rtChildren[i].offsetMax.x, fCurrentPosition);
				fCurrentPosition += m_fSpacing;
			}
		}
	}

	/// <summary>
	/// returns the informations required about each children and their weights
	/// </summary>
	/// <param name="rtChildren">the array of rectTransforms that need to be placed</param>
	/// <param name="nWeights">the array of weights corresponding</param>
	/// <returns>the total weight</returns>
	private uint CalculateChildrenWeights(out RectTransform[] rtChildren, out uint[] nWeights, out bool[] bIgnore, out float[] nPixels)
	{
		//we release all the children that were locked
		m_drtt.Clear();

		uint nTotalWeight = 0;
		rtChildren = new RectTransform[transform.childCount];
		nWeights = new uint[transform.childCount];
		nPixels = new float[transform.childCount];
		bIgnore = new bool[transform.childCount];
		int nIndex = 0;
		for (int i = 0; i < transform.childCount; ++i)
		{
			//vertical layout goes top to bottom, so 1 to 0. We read the children in the inverse order
			if (m_eLayout == LayoutType.Vertical)
			{
				nIndex = transform.childCount - 1 - i;
			}
			else
			{
				nIndex = i;
			}
			rtChildren[nIndex] = transform.GetChild(nIndex) as RectTransform;
			if(rtChildren[nIndex] != null)
			{ 
				ILayoutIgnorer ignore = rtChildren[nIndex].GetComponent<ILayoutIgnorer>();
				if (ignore != null )
				{
					bIgnore[nIndex] = ignore.ignoreLayout;
				}
				else
					bIgnore[nIndex] = !rtChildren[nIndex].gameObject.activeInHierarchy;

				WeightedLayoutElement element = rtChildren[nIndex].GetComponent<WeightedLayoutElement>();
				if(!rtChildren[nIndex].gameObject.activeInHierarchy)
				{
					nWeights[nIndex] = 0;
					nPixels[nIndex] = 0;
				}
				else if (element != null)
				{
					nWeights[nIndex] = element.useWeight ? element.nWeight : 0;
					nPixels[nIndex] = element.useAdditionalPixels ? element.nAdditionalPixels : 0;
				}
				else if (!bIgnore[nIndex])
				{
					nWeights[nIndex] = 1;
					nPixels[nIndex] = 0;
				}
				else
				{
					nWeights[nIndex] = 0;
					nPixels[nIndex] = 0;
				}

				if (!bIgnore[nIndex])
				{
					//we use drtt to block the values in the inspector
					switch (m_eLayout)
					{
						case LayoutType.Vertical:
							m_drtt.Add(this, rtChildren[nIndex], DrivenTransformProperties.AnchorMinY | DrivenTransformProperties.AnchorMaxY | DrivenTransformProperties.SizeDeltaY | DrivenTransformProperties.AnchoredPositionY);
							if (m_bChildForceExpand)
							{
								m_drtt.Add(this, rtChildren[nIndex], DrivenTransformProperties.AnchorMinX | DrivenTransformProperties.AnchorMaxX | DrivenTransformProperties.SizeDeltaX | DrivenTransformProperties.AnchoredPositionX);
							}
							break;
						case LayoutType.Horizontal:
							m_drtt.Add(this, rtChildren[nIndex], DrivenTransformProperties.AnchorMinX | DrivenTransformProperties.AnchorMaxX | DrivenTransformProperties.SizeDeltaX | DrivenTransformProperties.AnchoredPositionX);
							if (m_bChildForceExpand)
							{
								m_drtt.Add(this, rtChildren[nIndex], DrivenTransformProperties.AnchorMinY | DrivenTransformProperties.AnchorMaxY | DrivenTransformProperties.SizeDeltaY | DrivenTransformProperties.AnchoredPositionY);
							}
							break;
					}
					nTotalWeight += nWeights[nIndex];
				}
			}
		}
		return nTotalWeight;
	}
#if UNITY_EDITOR
	protected override void OnValidate()
	{
		SetDirty();
	}
#endif
	protected override void OnDestroy()
	{
		m_drtt.Clear();
		base.OnDestroy();
		SetLayoutHorizontal();
		SetLayoutVertical();
	}

	protected void SetDirty()
	{
		if (!IsActive())
			return;
		LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
	}
}
