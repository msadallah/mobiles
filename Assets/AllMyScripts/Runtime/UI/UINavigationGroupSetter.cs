﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteAlways]
public class UINavigationGroupSetter : MonoBehaviour
{

    public enum NavigationOrderMode {
        Simple,
        Hierarchy,
        Vertical,
        Horizontal
    }

    #region Properties

    //[Header("UINavigationGroupSetter")]
    //[SerializeField]
    //private bool reorderDone = false; // TODO make a Editor script with a nice button
    //[SerializeField]
    //public NavigationOrderMode OrderMode;

    #endregion

    //// Update is called once per frame
    //void Update()
    //{
    //    if (!Application.isPlaying) {

    //        //
    //        if (this.reorderDone) {

    //            switch (this.OrderMode) {
    //                case NavigationOrderMode.Simple:
    //                    this.ReorderSelectableNavigationSimple();
    //                    break;
    //            }

    //            this.reorderDone = true;

    //        }

    //    }
    //}

    [ContextMenu("Reorder selectable navigation - Simple")]
    public void ReorderSelectableNavigationSimple() {

        Debug.Log("UINavigationGroupSetter - Reorder simple start");

        List<Selectable> selectables = new List<Selectable>();

        // Get all selectable that are not explicitly not navigable
        foreach (Selectable s in this.GetComponentsInChildren<Selectable>()) {
            if (s.navigation.mode != Navigation.Mode.None) {
                selectables.Add(s);
            }
        }

        Debug.Log("UINavigationGroupSetter - Selectables to reorder : "+selectables.Count);

        // Re assign navigation explicitly
        for (int i = 0; i < selectables.Count; ++i) {

            Navigation n = selectables[i].navigation;

            n.mode = Navigation.Mode.Explicit;
            // Up selectable
            if (i == 0)
            {
                n.selectOnUp = selectables[selectables.Count - 1];                
            }
            else {
                n.selectOnUp = selectables[i - 1];
            }
            // Down selectable
            if (i == selectables.Count - 1)
            {
                n.selectOnDown = selectables[0];
            }
            else {
                n.selectOnDown = selectables[i + 1];
            }

            // Copy left & right
            n.selectOnLeft = n.selectOnUp;
            n.selectOnRight = n.selectOnDown;

            selectables[i].navigation = n;

        }

        Debug.Log("UINavigationGroupSetter - Reorder simple end");

    }

    public void ReorderSelectableNavigationHierarchyOrder() {
        // TODO Reorder elements by how first they are in the hierachy order
    }

}
