﻿using UnityEngine.UI;

public static class UIEventSyncExtensions
{
    static Toggle.ToggleEvent emptyToggleEvent = new Toggle.ToggleEvent();
    // allow to change the value of a toggle without triggering the OnValueChangedDelegate

    /// <summary>
    /// allow to change the value of a toggle without triggering the OnValueChangedDelegate
    /// </summary>
    public static void SetValue(this Toggle instance, bool value)
    {
        var originalEvent = instance.onValueChanged;
        instance.onValueChanged = emptyToggleEvent;
        instance.isOn = value;
        instance.onValueChanged = originalEvent;
    }
}