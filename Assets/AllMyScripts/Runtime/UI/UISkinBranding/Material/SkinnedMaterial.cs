﻿namespace AllMyScripts.Common.UI
{
    using UnityEngine;

    /// <summary>
    /// Extended class from image allowing for skinning
    /// </summary>
    [ExecuteAlways]
    public class SkinnedMaterial : MonoBehaviour
    {
        private MeshRenderer render;
        public bool instantiateMaterial = false;
        [Header("Skinning")]
        public UISkinDefinition.SkinColorDefinitions skin;

        protected void Awake()
        {
            render = GetComponent<MeshRenderer>();
            UpdateSkin();
        }

        public void UpdateSkin()
        {
            if (instantiateMaterial)
            {
                render.material.color = UISkinManager.currentModel.GetColor(skin);
            }
            else
            {
                render.sharedMaterial.color = UISkinManager.currentModel.GetColor(skin);
            }
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying)
            {
                UpdateSkin();
            }
        }
#endif
    }
}