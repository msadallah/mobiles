﻿namespace AllMyScripts.Common.UI
{
    using UnityEngine;

    public class CameraSkinnedClearColor : MonoBehaviour
    {
        [SerializeField] private UISkinDefinition.SkinColorDefinitions _skin;
        public UISkinDefinition.SkinColorDefinitions skin
        {
            get
            {
                return _skin;
            }
            set
            {
                _skin = value;
                RefreshColor();
            }
        }
        private void OnValidate()
        {
            RefreshColor();
        }
        private void Awake()
        {
            RefreshColor();
        }

        private void RefreshColor()
        {
            GetComponent<Camera>().backgroundColor = UISkinManager.currentModel.GetColor(skin);
        }
    }
}