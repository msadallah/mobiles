﻿namespace AllMyScripts.Common.UI
{
    using UnityEngine;
    using System.Collections;
    using AllMyScripts.Common.Animation;

    /// <summary>
    /// Extended class from image allowing for skinning
    /// </summary>
    [ExecuteAlways]
    public class SkinnedImageInvertMask : ImageInvertMask
    {


        [Header("Skinning")]
        public UISkinDefinition.SkinColorDefinitions colorSkin;
        public UISkinDefinition.SkinSpriteDefinitions imageSkin;
        public bool bSkinnedSprite = false;

        private UISkinDefinition.SkinColorDefinitions previousColorSkin;
        private UISkinDefinition.SkinSpriteDefinitions previousImageSkin;
        protected override void Awake()
        {
            base.Awake();
            UpdateSkin();
        }

        public void UpdateSkin()
        {
            color = UISkinManager.currentModel.GetColor(colorSkin);
            if (bSkinnedSprite)
            {
                sprite = UISkinManager.currentModel.GetSprite(imageSkin);
            }
            previousColorSkin = colorSkin;
            previousImageSkin = imageSkin;
        }

        private void Update()
        {
            if (!Application.isPlaying
                || colorSkin != previousColorSkin
                || imageSkin != previousImageSkin)
            {
                UpdateSkin();
            }
        }

        Coroutine colorCoroutine;

        public Coroutine AnimateColor(CurveDatabase.AnimationCurveEnum curve, float fDuration, UISkinDefinition.SkinColorDefinitions target)
        {
            if (!gameObject.activeInHierarchy)
            {
                previousColorSkin = colorSkin;
                colorSkin = target;
                color = UISkinManager.currentModel.GetColor(target);
                return null;
            }
            if (colorCoroutine != null)
                StopCoroutine(colorCoroutine);
            AnimationCurve newCurve = CurveDatabase.GetNewAnimationInstance(curve, fDuration: fDuration);
            colorCoroutine = StartCoroutine(CoroutineAnimateColor(newCurve, target));
            return colorCoroutine;
        }

        public Coroutine AnimateColor(AnimationCurve animCurve, UISkinDefinition.SkinColorDefinitions target)
        {
            if (colorCoroutine != null)
                StopCoroutine(colorCoroutine);
            colorCoroutine = StartCoroutine(CoroutineAnimateColor(animCurve, target));
            return colorCoroutine;
        }

        private IEnumerator CoroutineAnimateColor(AnimationCurve animCurve, UISkinDefinition.SkinColorDefinitions target)
        {
            float fStartTime = Time.time;
            float fEndTime = Time.time + animCurve.GetDuration();

            //get the colors to animate
            Color cEndColor = UISkinManager.currentModel.GetColor(target);

            Color cStartColor = color;
            while (Time.time < fEndTime)
            {
                color = animCurve.AnimatedLerp(cStartColor, cEndColor, fStartTime, fEndTime, Time.time);
                yield return null;
            }
            color = cEndColor;
            previousColorSkin = colorSkin;
            colorSkin = target;
        }
    }
}