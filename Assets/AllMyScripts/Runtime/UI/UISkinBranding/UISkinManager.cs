﻿namespace AllMyScripts.Common.UI
{
    using UnityEngine;

    public static class UISkinManager
    {
        private static UISkinModel _currentModel;
#if UNITY_EDITOR
        private static bool currentIsInvalid = false;
#endif
        public static UISkinModel currentModel
        {
            get
            {
                if (_currentModel == null
#if UNITY_EDITOR
                || currentIsInvalid
#endif
                )
                {
                    //Set Default Model
                    _currentModel = Resources.Load<UISkinModel>("UISkins/UISkin");
                }
#if UNITY_EDITOR
                if (_currentModel == null)
                {
                    _currentModel = ScriptableObject.CreateInstance<UISkinModel>();
                    currentIsInvalid = true;
                }
                else
                {
                    currentIsInvalid = false;
                }
#endif
                return _currentModel;
            }
            set
            {
                _currentModel = value;
            }
        }
    }
}