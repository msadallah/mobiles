﻿namespace AllMyScripts.Common.UI
{
	///<summary>
	/// UISkinDefinition contains all the definitions for skins
	/// as well as their matching name in editor
	/// if a name is string.empty, it won't be shown in editor
	/// </summary>
	public static class UISkinDefinition
	{
		//the enum used to fetch a skin
		public enum SkinColorDefinitions
		{
			Main = 0,
			MainUltraLight = 1,
			MainLight = 2,
			MainDark = 3,

			Alert = 4,
			AlertUltraLight = 5,
			AlertLight = 6,
			AlertDark = 7,

			Neutral = 8,
			NeutralUltraLight = 9,
			NeutralLight = 10,
			NeutralDark = 11,
			NeutralBackgroundImage = 12,

			Warning = 13,

			Success = 14,

			TextureOverlay = 15,

			White = 16,
			Black = 17,

			TransparentSeparator = 18,

			NeutralGhost = 19,
			TextureDivider = 20,
			TextureDividerWhite = 21,
			White50 = 22,

			NeutralLightGrey = 23,
			Shadow = 24,
			MainDarkLight = 25,
			ShadowLight = 26,
			ShadowUltraLight = 27,
			NeutralEnergy = 28,
			White25 = 29,
			White75 = 30
		}

#if UNITY_EDITOR
		//the name and path in the editor
		public static readonly string[] skinColorDefinitionNames = new string[]
		{
			"Main/Main",
			"Main/Ultra Light",
			"Main/Light",
			"Main/Dark",

			"Alert/Main",
			"Alert/Ultra Light",
			"Alert/Light",
			"Alert/Dark",

			"Neutral/Main",
			"Neutral/Ultra Light",
			"Neutral/Light",
			"Neutral/Dark",
			"Neutral/Background Image",

			"Warning",

			"Success",

			"Texture/overlay",

			"Fixed/White",
			"Fixed/Black",

			"Separator/Transparent",

			"Neutral/Ghost",
			"Texture/Divider",
			"Texture/Divider White",
			"Fixed/White 50%",

			"Neutral/Light Grey",
			"Shadow/Main",
			"Main/DarkLight",
			"Shadow/Light",
			"Shadow/UltraLight",
			"Neutral/NeutralEnergy",
			"Fixed/White 25%",
			"Fixed/White 75%"
		};
#endif



		//the enum used to fetch a skin
		public enum SkinSpriteDefinitions
		{
			TitleLogo,
			SplashScreen,
		}

#if UNITY_EDITOR
		//the name and path in the editor
		public static readonly string[] skinSpriteDefinitionNames = new string[]
		{
			"Logo"
		};
#endif
	}
}