﻿namespace AllMyScripts.Common.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    using System.Collections;
    using AllMyScripts.Common.Animation;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    /// <summary>
    /// Extended class from image allowing for skinning
    /// </summary>
    [ExecuteInEditMode]
    [AddComponentMenu("UI/Skinned Image")]
    public class SkinnedImage : Image
    {
        [SerializeField]
        private Color m_Tint = Color.white;
        public virtual Color tint { get { return m_Tint; } set { if (SetColorTest(ref m_Tint, value)) UpdateSkin(); } }

#if UNITY_EDITOR
        /// <summary>
        /// Adds a quick create menu in the hierarchy context menu
        /// </summary>
        [MenuItem("GameObject/Skinned UI/Image", false, 1)]
        private static void CreateGameObject()
        {
            GameObject child = new GameObject("Skinned Image");
            RectTransform rectTransform = child.AddComponent<RectTransform>();
            child.AddComponent<SkinnedImage>();
            child.transform.SetParent(Selection.activeGameObject.transform);
            child.transform.localScale = Vector3.one;
            rectTransform.localPosition = Vector3.zero;
            rectTransform.sizeDelta = new Vector2(100, 100);
            rectTransform.anchorMin = Vector3.zero;
            rectTransform.anchorMax = Vector3.one;
            rectTransform.offsetMin = Vector3.zero;
            rectTransform.offsetMax = Vector3.zero;
            Selection.activeGameObject = child;
        }
#endif


        [Header("Skinning")]
        [SerializeField]
        private UISkinDefinition.SkinColorDefinitions colorSkin = default;
        public UISkinDefinition.SkinColorDefinitions skin
        {
            get
            {
                return colorSkin;
            }
            set
            {
                colorSkin = value;
                UpdateSkin();
            }
        }
        [SerializeField]
        private UISkinDefinition.SkinSpriteDefinitions imageSkin = default;
        public UISkinDefinition.SkinSpriteDefinitions imageskin
        {
            get
            {
                return imageSkin;
            }
            set
            {
                imageSkin = value;
                UpdateSkin();
            }
        }

        public bool bSkinnedSprite = false;

        public bool bUseSkinColor = true;

        private UISkinDefinition.SkinColorDefinitions previousColorSkin;
        private UISkinDefinition.SkinSpriteDefinitions previousImageSkin;
        protected override void Awake()
        {
            base.Awake();
        }
        protected override void OnEnable()
        {
            base.OnEnable();
            UpdateSkin();
        }

        [ContextMenu("Update Skin")]
        public void UpdateSkin()
        {
            if (bUseSkinColor)
            {
                color = UISkinManager.currentModel.GetColor(colorSkin) * tint;
                if (bSkinnedSprite)
                {
                    sprite = UISkinManager.currentModel.GetSprite(imageSkin);
                }
            }
            previousColorSkin = colorSkin;
            previousImageSkin = imageSkin;
        }

        private void Update()
        {
            if (!Application.isPlaying
                || colorSkin != previousColorSkin
                || imageSkin != previousImageSkin)
            {
                UpdateSkin();
            }
        }

        Coroutine colorCoroutine;
        Coroutine tintCoroutine;

        public Coroutine AnimateColor(CurveDatabase.AnimationCurveEnum curve, float fDuration, UISkinDefinition.SkinColorDefinitions target)
        {
            if (colorCoroutine != null)
                StopCoroutine(colorCoroutine);
            AnimationCurve newCurve = CurveDatabase.GetNewAnimationInstance(curve, fDuration: fDuration);
            colorCoroutine = StartCoroutine(CoroutineAnimateColor(newCurve, target));
            return colorCoroutine;
        }

        public Coroutine AnimateColor(AnimationCurve animCurve, UISkinDefinition.SkinColorDefinitions target)
        {
            if (colorCoroutine != null)
                StopCoroutine(colorCoroutine);
            colorCoroutine = StartCoroutine(CoroutineAnimateColor(animCurve, target));
            return colorCoroutine;
        }


        public Coroutine AnimateTint(CurveDatabase.AnimationCurveEnum curve, float fDuration, Color target)
        {
            if (tintCoroutine != null)
                StopCoroutine(tintCoroutine);
            AnimationCurve newCurve = CurveDatabase.GetNewAnimationInstance(curve, fDuration: fDuration);
            tintCoroutine = StartCoroutine(CoroutineAnimateTint(newCurve, target));
            return tintCoroutine;
        }

        public Coroutine AnimateTint(AnimationCurve animCurve, Color target)
        {
            if (colorCoroutine != null)
                StopCoroutine(tintCoroutine);
            tintCoroutine = StartCoroutine(CoroutineAnimateTint(animCurve, target));
            return tintCoroutine;
        }

        private IEnumerator CoroutineAnimateColor(AnimationCurve animCurve, UISkinDefinition.SkinColorDefinitions target)
        {
            float fStartTime = Time.time;
            float fEndTime = Time.time + animCurve.GetDuration();

            //get the colors to animate
            Color cEndColor = UISkinManager.currentModel.GetColor(target) * tint;

            Color cStartColor = color * tint;
            while (Time.time < fEndTime)
            {
                color = animCurve.AnimatedLerp(cStartColor, cEndColor, fStartTime, fEndTime, Time.time);
                yield return null;
            }
            color = cEndColor;
            previousColorSkin = colorSkin;
            colorSkin = target;
        }

        private IEnumerator CoroutineAnimateTint(AnimationCurve animCurve, Color target)
        {
            float fStartTime = Time.time;
            float fEndTime = Time.time + animCurve.GetDuration();

            //get the colors to animate
            Color cEndColor = tint;

            Color cStartColor = tint;
            while (Time.time < fEndTime)
            {
                tint = animCurve.AnimatedLerp(cStartColor, cEndColor, fStartTime, fEndTime, Time.time);
                yield return null;
            }
            tint = cEndColor;
        }

        public static bool SetColorTest(ref Color currentValue, Color newValue)
        {
            if (currentValue.r == newValue.r && currentValue.g == newValue.g && currentValue.b == newValue.b && currentValue.a == newValue.a)
                return false;

            currentValue = newValue;
            return true;
        }
        protected override void OnDidApplyAnimationProperties()
        {
            base.OnDidApplyAnimationProperties();
            UpdateSkin();
        }
    }
}