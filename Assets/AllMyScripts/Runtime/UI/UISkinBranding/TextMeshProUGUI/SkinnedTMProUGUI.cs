﻿namespace TMPro
{
    using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;
#endif

	using AllMyScripts.Common.UI;

	/// <summary>
	/// Extended class from image allowing for skinning
	/// </summary>
	[ExecuteAlways]
    [AddComponentMenu("UI/Skinned Text Mesh Pro UGUI")]
    public class SkinnedTMProUGUI : TextMeshProUGUI
    {
#if UNITY_EDITOR
        /// <summary>
        /// Adds a quick create menu in the hierarchy context menu
        /// </summary>
        [MenuItem("GameObject/Skinned UI/TextMeshPro UGUI", false, 1)]
        private static void CreateGameObject()
        {
            GameObject child = new GameObject("Skinned TextMeshProUGUI");
            RectTransform rectTransform = child.AddComponent<RectTransform>();
            var tmpro = child.AddComponent<SkinnedTMProUGUI>();
            child.transform.SetParent(Selection.activeGameObject.transform);
            child.transform.localScale = Vector3.one;
            rectTransform.localPosition = Vector3.zero;
            rectTransform.sizeDelta = new Vector2(100, 100);
            rectTransform.anchorMin = Vector3.zero;
            rectTransform.anchorMax = Vector3.one;
            rectTransform.offsetMin = Vector3.zero;
            rectTransform.offsetMax = Vector3.zero;
            Selection.activeGameObject = child;

            //default values
            tmpro.extraPadding = true;
            tmpro.skin = UISkinDefinition.SkinColorDefinitions.Neutral;

        }

        [ContextMenu("Add Localisation")]
        private void AddLocalisation()
        {
            if (gameObject.GetComponent<TextLocalisation>() == null)
                gameObject.AddComponent<TextLocalisation>();
        }

        protected override void Reset()
        {
            base.Reset();
            lineSpacing = UISkinManager.currentModel.defaultLineHeight;
        }
#endif


        [Header("Skinning")]
        public bool bUseSkinColor = true;
        public bool bUseSkinTags = false;
        public UISkinDefinition.SkinColorDefinitions skin;
        private UISkinDefinition.SkinColorDefinitions previousSkin;
        [Header("- Smart Behaviour")]
        [Tooltip("checks on update if the text requires to have skin tags replace. Do not use if unrequired")]
        public bool bRefreshTagsOnUpdate = false;

        protected override void Awake()
        {
            base.Awake();
            extraPadding = true;
            UpdateSkin();
        }

        public void UpdateSkin()
        {
            if (bUseSkinColor)
            {
                color = UISkinManager.currentModel.GetColor(skin);
                previousSkin = skin;
            }
            if (bUseSkinTags && !string.IsNullOrEmpty(text))
            {
                text = UISkinManager.currentModel.ApplyTags(text);
            }
        }

        private void Update()
        {
            if (bRefreshTagsOnUpdate)
            {
                //we apply the tags, if something changed, then it means we need to update
                var originalText = text;
                var modifiedText = UISkinManager.currentModel?.ApplyTags(originalText) ?? "";

                if (!originalText.Equals(modifiedText))
                {
                    text = modifiedText;
                }
            }

            if (skin != previousSkin && bUseSkinColor)
                UpdateSkin();
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();
            UpdateSkin();
        }
#endif


    }
}