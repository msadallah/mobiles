﻿namespace AllMyScripts.Common.UI
{
	using UnityEngine;

	/// <summary>
	///This file contains the variables and interactivity with the skinned elements
	///It is the "database" of the skin definitions, linking a definition to its real values
	///this is where you add the color, string, or anything that matches a skin definition
	/// </summary>
	[CreateAssetMenu(fileName = "UISkin.asset", menuName = "AllMyScripts/UISkin")]
	public class UISkinModel : ScriptableObject
	{
		[System.Serializable]
		public class RichTagData
		{
			public string originalTag = "tag";
			public string replacementOpeningTag = "<color=red>";
			public string replacementClosingTag = "</color>";
		}

		[Header("Colors")]
		[Header("Main")]
		public Color[] main;
		public Color[] mainUltraLight;
		public Color[] mainLight;
		public Color[] mainDark;
		public Color[] mainDarkLight;
		[Header("Alert")]
		public Color[] alert;
		public Color[] alertUltraLight;
		public Color[] alertLight;
		public Color[] alertDark;
		[Header("Warning")]
		public Color[] warning;
		[Header("Neutral")]
		public Color[] neutral;
		public Color[] neutralUltraLight;
		public Color[] neutralLight;
		public Color[] neutralDark;
		public Color[] neutralBackgroundImage;
		public Color[] neutralGhost;
		public Color[] neutralLightGrey;
		public Color[] neutralEnergy;
		[Header("Others")]
		public Color[] success;
		public Color[] shadow = new Color[]{ new Color(0, 0, 0, 0.4f) };
		public Color[] shadowLight = new Color[]{ new Color(0, 0, 0, 0.21f)};
		public Color[] shadowUltraLight = new Color[]{ new Color(0, 0, 0, 0.16f)};
		public Color[] textureDivider = new Color[]{ new Color(0, 0, 0, 0.1f)};
		public Color[] textureDividerWhite = new Color[]{ new Color(1, 1, 1, 0.15f)};
		public Color[] textureOverlay = new Color[]{ new Color(0, 0, 0, 0.2f)};
		public Color[] transparentSeparator = new Color[]{ new Color(0, 0, 0, 0.1f)};


		private Color _white50 = new Color(1, 1, 1, 0.5f);
		private Color _white25 = new Color(1, 1, 1, 0.25f);
		private Color _white75 = new Color(1, 1, 1, 0.75f);

		[Header("Text Tag replacements")]
		public RichTagData[] tags;

		[Header("Line Heights")]

		public float defaultLineHeight = 1.25f;

		[Header("Introduction")]
		public Sprite splashScreen = null;
		public bool displayPoweredBy = true;

		[Header("Logo")]
		public Sprite titleLogo;

		/// <summary>
		/// get the definition's color
		/// </summary>
		/// <param name="definition">the definition to fetch</param>
		/// <returns>the corresponding color. White if nothing is returned</returns>
		public Color GetColor(UISkinDefinition.SkinColorDefinitions definition, bool useSkinOverride = false)
		{
			switch (definition)
			{
				case UISkinDefinition.SkinColorDefinitions.Main:
					return GetOverridedColor(main, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.MainDark:
					return GetOverridedColor(mainDark, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.MainLight:
					return GetOverridedColor(mainLight, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.MainUltraLight:
					return GetOverridedColor(mainUltraLight, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.MainDarkLight:
					return GetOverridedColor(mainDarkLight, useSkinOverride);



				case UISkinDefinition.SkinColorDefinitions.Alert:
					return GetOverridedColor(alert, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.AlertDark:
					return GetOverridedColor(alertDark, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.AlertLight:
					return GetOverridedColor(alertLight, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.AlertUltraLight:
					return GetOverridedColor(alertUltraLight, useSkinOverride);



				case UISkinDefinition.SkinColorDefinitions.Warning:
					return GetOverridedColor(warning, useSkinOverride);



				case UISkinDefinition.SkinColorDefinitions.Neutral:
					return GetOverridedColor(neutral, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.NeutralDark:
					return GetOverridedColor(neutralDark, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.NeutralLight:
					return GetOverridedColor(neutralLight, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.NeutralUltraLight:
					return GetOverridedColor(neutralUltraLight, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.NeutralBackgroundImage:
					return GetOverridedColor(neutralBackgroundImage, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.NeutralGhost:
					return GetOverridedColor(neutralGhost, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.NeutralLightGrey:
					return GetOverridedColor(neutralLightGrey, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.NeutralEnergy:
					return GetOverridedColor(neutralEnergy, useSkinOverride);


				case UISkinDefinition.SkinColorDefinitions.Success:
					return GetOverridedColor(success, useSkinOverride);



				case UISkinDefinition.SkinColorDefinitions.TextureOverlay:
					return GetOverridedColor(textureOverlay, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.TextureDivider:
					return GetOverridedColor(textureDivider, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.TextureDividerWhite:
					return GetOverridedColor(textureDividerWhite, useSkinOverride);



				case UISkinDefinition.SkinColorDefinitions.White:
					return Color.white;

				case UISkinDefinition.SkinColorDefinitions.Black:
					return Color.black;

				case UISkinDefinition.SkinColorDefinitions.White25:
					return _white25;

				case UISkinDefinition.SkinColorDefinitions.White50:
					return _white50;

				case UISkinDefinition.SkinColorDefinitions.White75:
					return _white75;



				case UISkinDefinition.SkinColorDefinitions.TransparentSeparator:
					return GetOverridedColor(transparentSeparator, useSkinOverride);



				case UISkinDefinition.SkinColorDefinitions.Shadow:
					return GetOverridedColor(shadow, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.ShadowLight:
					return GetOverridedColor(shadowLight, useSkinOverride);

				case UISkinDefinition.SkinColorDefinitions.ShadowUltraLight:
					return GetOverridedColor(shadowUltraLight, useSkinOverride);

			}
			return Color.white;
		}

		private Color GetOverridedColor(Color[] colorArray, bool useOverride)
		{
			if (colorArray != null && colorArray.Length != 0)
			{
				if (useOverride && colorArray.Length > 1)
					return colorArray[1];
				else
					return colorArray[0];
			}
			else
			{
				return Color.magenta;
			}
		}

		/// <summary>
		/// get the definition's sprite
		/// </summary>
		/// <param name="definition">the definition to fetch</param>
		/// <returns>the corresponding sprite. null if nothing is returned</returns>
		public Sprite GetSprite(UISkinDefinition.SkinSpriteDefinitions definition)
		{
			switch (definition)
			{
				case UISkinDefinition.SkinSpriteDefinitions.SplashScreen:
					return splashScreen;

				case UISkinDefinition.SkinSpriteDefinitions.TitleLogo:
					return titleLogo;
			}
			return null;
		}

		/// <summary>
		/// changes the tags in the original text to the ones in the skin 
		/// </summary>
		/// <param name="sToReplace">the string to replace</param>
		/// <returns>the result</returns>
		public string ApplyTags(string sToReplace)
		{
			bool hasReplaced = false;

			if (string.IsNullOrEmpty(sToReplace) || !sToReplace.Contains("</"))
				return sToReplace;
			if (tags != null)
			{
				foreach (RichTagData rtd in tags)
				{
					if (sToReplace.Contains("<" + rtd.originalTag + ">"))
					{
						hasReplaced = true;
						sToReplace = sToReplace.Replace("<" + rtd.originalTag + ">", rtd.replacementOpeningTag);
						sToReplace = sToReplace.Replace("</" + rtd.originalTag + ">", rtd.replacementClosingTag);
					}

				}
			}
			//we now replace <color=Main> by <color=#xxxxxx>

			if (hasReplaced)
			{
				foreach (UISkinDefinition.SkinColorDefinitions skinColor in System.Enum.GetValues(typeof(UISkinDefinition.SkinColorDefinitions)))
				{
					//sToReplace = Regex.Replace(sToReplace, "<color=" + skinColor.ToString() + ">", "<color=#" + ColorUtility.ToHtmlStringRGBA(GetColor(skinColor)) + ">", RegexOptions.IgnoreCase);
					sToReplace = sToReplace.Replace("<color=" + skinColor.ToString() + ">", "<color=#" + ColorUtility.ToHtmlStringRGBA(GetColor(skinColor)) + ">");
				}
			}

			return sToReplace;
		}
	}
}