﻿namespace TMPro
{
    using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;
#endif

    using AllMyScripts.Common.UI;

    /// <summary>
    /// Extended class from image allowing for skinning
    /// </summary>
    [ExecuteAlways]
    [AddComponentMenu("UI/Skinned TextMesh")]
    public class SkinnedTMPro : TextMeshPro
    {
#if UNITY_EDITOR
        /// <summary>
        /// Adds a quick create menu in the hierarchy context menu
        /// </summary>
        [MenuItem("GameObject/Skinned UI/TextMeshPro", false, 1)]
        private static void CreateGameObject()
        {
            GameObject child = new GameObject("Skinned TextMeshPro");
            RectTransform rectTransform = child.AddComponent<RectTransform>();
            child.AddComponent<SkinnedTMPro>().extraPadding = true;
            child.transform.SetParent(Selection.activeGameObject.transform);
            child.transform.localScale = Vector3.one;
            rectTransform.localPosition = Vector3.zero;
            rectTransform.sizeDelta = new Vector2(100, 100);
            rectTransform.anchorMin = Vector3.zero;
            rectTransform.anchorMax = Vector3.one;
            rectTransform.offsetMin = Vector3.zero;
            rectTransform.offsetMax = Vector3.zero;
            Selection.activeGameObject = child;

        }


        protected override void Reset()
        {
            base.Reset();

            lineSpacing = UISkinManager.currentModel.defaultLineHeight;
        }
#endif


        [Header("Skinning")]
        public bool bUseSkinColor = true;
        public bool bUseSkinTags = false;
        public UISkinDefinition.SkinColorDefinitions skin;
        private UISkinDefinition.SkinColorDefinitions previousSkin;

        protected override void Awake()
        {
            base.Awake();
            extraPadding = true;
            UpdateSkin();
        }

        public void UpdateSkin()
        {
            if (bUseSkinColor)
                color = UISkinManager.currentModel.GetColor(skin);
            if (bUseSkinTags && !string.IsNullOrEmpty(text))
            {
                text = UISkinManager.currentModel.ApplyTags(text);
            }
            previousSkin = skin;
        }

#if UNITY_EDITOR
        private void Update()
        {/*
		if(!Application.isPlaying
			|| skin != previousSkin)
		*/
            {
                UpdateSkin();
            }
        }
#endif


    }
}