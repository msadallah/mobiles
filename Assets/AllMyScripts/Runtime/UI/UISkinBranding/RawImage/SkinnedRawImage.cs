﻿namespace AllMyScripts.Common.UI
{
    using UnityEngine;
    using UnityEngine.UI;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    /// <summary>
    /// Extended class from image allowing for skinning
    /// </summary>
    [ExecuteAlways]
    [AddComponentMenu("UI/Skinned RawImage")]
    public class SkinnedRawImage : RawImage
    {

#if UNITY_EDITOR
        /// <summary>
        /// Adds a quick create menu in the hierarchy context menu
        /// </summary>
        [MenuItem("GameObject/Skinned UI/RawImage", false, 1)]
        private static void CreateGameObject()
        {
            GameObject child = new GameObject("Skinned RawImage");
            RectTransform rectTransform = child.AddComponent<RectTransform>();
            child.AddComponent<SkinnedRawImage>();
            child.transform.SetParent(Selection.activeGameObject.transform);
            child.transform.localScale = Vector3.one;
            rectTransform.localPosition = Vector3.zero;
            rectTransform.sizeDelta = new Vector2(100, 100);
            rectTransform.anchorMin = Vector3.zero;
            rectTransform.anchorMax = Vector3.one;
            rectTransform.offsetMin = Vector3.zero;
            rectTransform.offsetMax = Vector3.zero;
            Selection.activeGameObject = child;
        }
#endif


        [Header("Skinning")]
        public UISkinDefinition.SkinColorDefinitions skin;

        protected override void Awake()
        {
            base.Awake();
            UpdateSkin();
        }

        [ContextMenu("Update Skin")]
        public void UpdateSkin()
        {
            color = UISkinManager.currentModel.GetColor(skin);
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying)
            {
                UpdateSkin();
            }
        }
#endif
    }
}