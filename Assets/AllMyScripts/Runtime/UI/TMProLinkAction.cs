﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Class added to a textMeshPro to link a <link> tag to an UnityEvent
/// </summary>
[RequireComponent(typeof(TextMeshProUGUI))]
public class TMProLinkAction : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
{
	[System.Serializable]
	public struct LinkAction
	{
		public string sLink;
		public UnityEvent Action;

        public void OpenURL() {
            Application.OpenURL(sLink);
        }

	}
	public LinkAction[] m_actions;
	private TextMeshProUGUI m_TextMeshPro;

    private bool wantToAddUrl = false;
    private bool addURLDone = false;
    public bool AddURLDone { get { return this.addURLDone; } }

	private void Start()
	{
		m_TextMeshPro = GetComponent<TextMeshProUGUI>();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		int linkIndex = TMP_TextUtilities.FindIntersectingLink(m_TextMeshPro, Input.mousePosition,m_TextMeshPro.GetComponentInParent<GraphicRaycaster>().GetComponent<Canvas>().worldCamera);

        // If no action is found, at least try to search if some link are url
        if (this.wantToAddUrl && !this.AddURLDone) {
            this.ComputeOpenURLToLink();
        }

        if (this.m_actions != null && linkIndex != -1)
        {
            TMP_LinkInfo linkInfo = m_TextMeshPro.textInfo.linkInfo[linkIndex];
            for (int i = 0; i < m_actions.Length; ++i)
            {
                if (m_actions[i].sLink == linkInfo.GetLinkID())
                {
                    m_actions[i].Action.Invoke();
                    return;
                }
            }
        }
	}

	public void OnPointerDown(PointerEventData eventData)
	{
	}

	public void OnPointerUp(PointerEventData eventData)
	{
	}


    /// <summary>
    /// Parse the link, if they have an url as link, add an action that open the url. Done the first time on pointer click is trigerred
    /// </summary>
    public void AddOpenURLToLink() {
        this.wantToAddUrl = true;
    }

    private void ComputeOpenURLToLink() {

        if (m_TextMeshPro != null)
        {
            List<LinkAction> linkActionToAdd = new List<LinkAction>();
            List<string> linkAdded = new List<string>();

            // Foreach link, check if it is an Http scheme, if true, add a link action to an open url format
            foreach (TMP_LinkInfo li in m_TextMeshPro.textInfo.linkInfo)
            {
                // Debug.Log("TMProLinkAction.AddOpenURLToLink - LinkID : " + li.GetLinkID());

                // Try to check if this is a valid http url
                Uri uriResult;
                bool result = Uri.TryCreate(li.GetLinkID(), UriKind.Absolute, out uriResult)
                    && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

                if (result && !linkAdded.Contains(li.GetLinkID()))
                {
                    linkAdded.Add(li.GetLinkID());
                    LinkAction la = new LinkAction();
                    la.sLink = li.GetLinkID();

                    UnityEvent ue = new UnityEvent();
                    ue.AddListener(la.OpenURL);
                    la.Action = ue;
                    linkActionToAdd.Add(la);
                }
            }

            // Add OpenURL actions to existing actions
            if (this.m_actions != null)
            {
                linkActionToAdd.AddRange(this.m_actions);
            }
            this.m_actions = linkActionToAdd.ToArray();

            this.addURLDone = true;
        }
    }    

}
