﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteAlways]
public class CopyColor : MonoBehaviour
{
	public CanvasRenderer copyFrom;
	public CanvasRenderer [] copyTo;
	private void Awake()
	{
		if(copyFrom==null)
		{
			copyFrom = GetComponent<CanvasRenderer>();
		}
	}
	// Update is called once per frame
	void Update()
    {
        foreach(CanvasRenderer g in copyTo)
		{
			g.SetColor(copyFrom.GetColor());
		}
    }
}
