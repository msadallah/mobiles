namespace UnityEngine.UI
{
    using UnityEngine;

    /*
     * \author Raphael Benedetto
     */
    public class CustomImage : Image
	{
#if UNITY_EDITOR
		protected override void OnValidate()
		{
			base.OnValidate();
			if (overrideSprite != null)
			{
				m_bForceSliced = true;
				type = Type.Filled;
			}
		}
#endif
		public bool m_bForceSliced = false;

		/// <summary>
		/// Update the UI renderer mesh.
		/// </summary>
		protected override void OnPopulateMesh( VertexHelper toFill )
		{
			if( type==Type.Filled && m_bForceSliced )
			{
				GenerateSlicedSprite( toFill );
				return;
			}
			base.OnPopulateMesh( toFill );
		}

		/// <summary>
		/// Generate vertices for a simple Image.
		/// </summary>
		private void GenerateSimpleSprite( VertexHelper vh, bool lPreserveAspect )
		{
			Vector4 v = GetDrawingDimensions( lPreserveAspect );
			var uv = ( overrideSprite!=null ) ? UnityEngine.Sprites.DataUtility.GetOuterUV( overrideSprite ) : Vector4.zero;

			var color32 = color;
			vh.Clear();
			vh.AddVert( new Vector3( v.x, v.y ), color32, new Vector2( uv.x, uv.y ) );
			vh.AddVert( new Vector3( v.x, v.w ), color32, new Vector2( uv.x, uv.w ) );
			vh.AddVert( new Vector3( v.z, v.w ), color32, new Vector2( uv.z, uv.w ) );
			vh.AddVert( new Vector3( v.z, v.y ), color32, new Vector2( uv.z, uv.y ) );

			vh.AddTriangle( 0, 1, 2 );
			vh.AddTriangle( 2, 3, 0 );
		}

		/// Image's dimensions used for drawing. X = left, Y = bottom, Z = right, W = top.
		private Vector4 GetDrawingDimensions( bool shouldPreserveAspect )
		{
			var padding = Vector4.zero;
			var size = overrideSprite == null ? Vector2.zero : new Vector2( overrideSprite.rect.width, overrideSprite.rect.height );

			Rect r = GetPixelAdjustedRect();
			// Debug.Log(string.Format("r:{2}, size:{0}, padding:{1}", size, padding, r));

			int spriteW = Mathf.RoundToInt( size.x );
			int spriteH = Mathf.RoundToInt( size.y );

			var v = new Vector4(
				padding.x / spriteW,
				padding.y / spriteH,
				( spriteW - padding.z ) / spriteW,
				( spriteH - padding.w ) / spriteH );

			if( shouldPreserveAspect && size.sqrMagnitude>0.0f )
			{
				var spriteRatio = size.x / size.y;
				var rectRatio = r.width / r.height;

				if( spriteRatio>rectRatio )
				{
					var oldHeight = r.height;
					r.height = r.width * ( 1.0f / spriteRatio );
					r.y += ( oldHeight - r.height ) * rectTransform.pivot.y;
				}
				else
				{
					var oldWidth = r.width;
					r.width = r.height * spriteRatio;
					r.x += ( oldWidth - r.width ) * rectTransform.pivot.x;
				}
			}

			v = new Vector4(
				r.x + r.width * v.x,
				r.y + r.height * v.y,
				r.x + r.width * v.z,
				r.y + r.height * v.w
				);

			return v;
		}

		private static readonly Vector2[] s_VertScratch = new Vector2[4];
		private static readonly Vector2[] s_UVScratch = new Vector2[4];

		private void GenerateSlicedSprite( VertexHelper toFill )
		{
			if( !hasBorder )
			{
				GenerateFilledSprite( toFill, false );
				return;
			}

			Vector4 outer, inner, padding, border;

			if( overrideSprite!=null )
			{
				outer = UnityEngine.Sprites.DataUtility.GetOuterUV( overrideSprite );
				inner = UnityEngine.Sprites.DataUtility.GetInnerUV( overrideSprite );
				padding = Vector4.zero;
				border = overrideSprite.border;
			}
			else
			{
				outer = Vector4.zero;
				inner = Vector4.zero;
				padding = Vector4.zero;
				border = Vector4.zero;
			}

			Rect rect = GetPixelAdjustedRect();
			border = GetAdjustedBorders( border / pixelsPerUnit, rect );
			padding = padding / pixelsPerUnit;

			s_VertScratch[0] = new Vector2( padding.x, padding.y );
			s_VertScratch[3] = new Vector2( rect.width - padding.z, rect.height - padding.w );

			s_VertScratch[1].x = border.x;
			s_VertScratch[1].y = border.y;
			s_VertScratch[2].x = rect.width - border.z;
			s_VertScratch[2].y = rect.height - border.w;

			for( int i = 0; i<4; ++i )
			{
				s_VertScratch[i].x += rect.x;
				s_VertScratch[i].y += rect.y;
			}

			s_UVScratch[0] = new Vector2( outer.x, outer.y );
			s_UVScratch[1] = new Vector2( inner.x, inner.y );
			s_UVScratch[2] = new Vector2( inner.z, inner.w );
			s_UVScratch[3] = new Vector2( outer.z, outer.w );

			toFill.Clear();

			for( int x = 0; x<3; ++x )
			{
				int x2 = x + 1;

				for( int y = 0; y<3; ++y )
				{
					if( !fillCenter && x==1 && y==1 )
						continue;

					int y2 = y + 1;

					GenerateFilledSprite( toFill, false,
										new Vector2( s_VertScratch[x].x, s_VertScratch[y].y ),
										new Vector2( s_VertScratch[x2].x, s_VertScratch[y2].y ),
										new Vector2( s_UVScratch[x].x, s_UVScratch[y].y ),
										new Vector2( s_UVScratch[x2].x, s_UVScratch[y2].y ) );
				}
			}
		}

		private static readonly Vector3[] s_Xy = new Vector3[4];
		private static readonly Vector3[] s_Uv = new Vector3[4];

		private void GenerateFilledSprite( VertexHelper toFill, bool preserveAspect, Vector2? v2PosMin = null, Vector2? v2PosMax = null, Vector2? v2UvMin = null, Vector2? v2UvMax = null )
		{
			if( v2PosMin==null )
				toFill.Clear();

			if( fillAmount<0.001f )
				return;

			Vector4 v = GetDrawingDimensions( preserveAspect );
			Vector4 outer = overrideSprite!=null ? UnityEngine.Sprites.DataUtility.GetOuterUV( overrideSprite ) : Vector4.zero;
			UIVertex uiv = UIVertex.simpleVert;
			uiv.color = color;

			float tx0 = outer.x;
			float ty0 = outer.y;
			float tx1 = outer.z;
			float ty1 = outer.w;

			// Horizontal and vertical filled sprites are simple -- just end the Image prematurely
			if( fillMethod==FillMethod.Horizontal || fillMethod==FillMethod.Vertical )
			{
				if( fillMethod==FillMethod.Horizontal )
				{
					float fill = ( tx1 - tx0 ) * fillAmount;

					if( fillOrigin==1 )
					{
						v.x = v.z - ( v.z - v.x ) * fillAmount;
						tx0 = tx1 - fill;
					}
					else
					{
						v.z = v.x + ( v.z - v.x ) * fillAmount;
						tx1 = tx0 + fill;
					}
				}
				else if( fillMethod==FillMethod.Vertical )
				{
					float fill = ( ty1 - ty0 ) * fillAmount;

					if( fillOrigin==1 )
					{
						v.y = v.w - ( v.w - v.y ) * fillAmount;
						ty0 = ty1 - fill;
					}
					else
					{
						v.w = v.y + ( v.w - v.y ) * fillAmount;
						ty1 = ty0 + fill;
					}
				}
			}

			if( v2PosMin!=null && v2PosMax!=null && v2UvMin!=null && v2UvMax!=null )
			{
				Vector4 v4Pos, v4UV;

				v4Pos.x = v2PosMin.Value.x;
				v4Pos.y = v2PosMin.Value.y;
				v4Pos.z = v2PosMax.Value.x;
				v4Pos.w = v2PosMax.Value.y;
				v4UV.x = v2UvMin.Value.x;
				v4UV.y = v2UvMin.Value.y;
				v4UV.z = v2UvMax.Value.x;
				v4UV.w = v2UvMax.Value.y;

				if( v.x>v4Pos.z || v.z<v4Pos.x || v.y>v4Pos.w || v.w<v4Pos.y ) return;

				tx0 = Mathf.Lerp( v4UV.x, v4UV.z, Mathf.InverseLerp( v4Pos.x, v4Pos.z, v.x ) );
				tx1 = Mathf.Lerp( v4UV.x, v4UV.z, Mathf.InverseLerp( v4Pos.x, v4Pos.z, v.z ) );
				ty0 = Mathf.Lerp( v4UV.y, v4UV.w, Mathf.InverseLerp( v4Pos.y, v4Pos.w, v.y ) );
				ty1 = Mathf.Lerp( v4UV.y, v4UV.w, Mathf.InverseLerp( v4Pos.y, v4Pos.w, v.w ) );

				v.x = Mathf.Max( v.x, v4Pos.x );
				v.y = Mathf.Max( v.y, v4Pos.y );
				v.z = Mathf.Min( v.z, v4Pos.z );
				v.w = Mathf.Min( v.w, v4Pos.w );
			}

			s_Xy[0] = new Vector2( v.x, v.y );
			s_Xy[1] = new Vector2( v.x, v.w );
			s_Xy[2] = new Vector2( v.z, v.w );
			s_Xy[3] = new Vector2( v.z, v.y );

			s_Uv[0] = new Vector2( tx0, ty0 );
			s_Uv[1] = new Vector2( tx0, ty1 );
			s_Uv[2] = new Vector2( tx1, ty1 );
			s_Uv[3] = new Vector2( tx1, ty0 );

			{
				if( fillAmount<1f && fillMethod!=FillMethod.Horizontal && fillMethod!=FillMethod.Vertical )
				{
					if( fillMethod==FillMethod.Radial90 )
					{
						if( RadialCut( s_Xy, s_Uv, fillAmount, fillClockwise, fillOrigin ) )
							AddQuad( toFill, s_Xy, color, s_Uv );
					}
					else if( fillMethod==FillMethod.Radial180 )
					{
						for( int side = 0; side<2; ++side )
						{
							float fx0, fx1, fy0, fy1;
							int even = fillOrigin>1 ? 1 : 0;

							if( fillOrigin==0 || fillOrigin==2 )
							{
								fy0 = 0f;
								fy1 = 1f;
								if( side==even )
								{
									fx0 = 0f;
									fx1 = 0.5f;
								}
								else
								{
									fx0 = 0.5f;
									fx1 = 1f;
								}
							}
							else
							{
								fx0 = 0f;
								fx1 = 1f;
								if( side==even )
								{
									fy0 = 0.5f;
									fy1 = 1f;
								}
								else
								{
									fy0 = 0f;
									fy1 = 0.5f;
								}
							}

							s_Xy[0].x = Mathf.Lerp( v.x, v.z, fx0 );
							s_Xy[1].x = s_Xy[0].x;
							s_Xy[2].x = Mathf.Lerp( v.x, v.z, fx1 );
							s_Xy[3].x = s_Xy[2].x;

							s_Xy[0].y = Mathf.Lerp( v.y, v.w, fy0 );
							s_Xy[1].y = Mathf.Lerp( v.y, v.w, fy1 );
							s_Xy[2].y = s_Xy[1].y;
							s_Xy[3].y = s_Xy[0].y;

							s_Uv[0].x = Mathf.Lerp( tx0, tx1, fx0 );
							s_Uv[1].x = s_Uv[0].x;
							s_Uv[2].x = Mathf.Lerp( tx0, tx1, fx1 );
							s_Uv[3].x = s_Uv[2].x;

							s_Uv[0].y = Mathf.Lerp( ty0, ty1, fy0 );
							s_Uv[1].y = Mathf.Lerp( ty0, ty1, fy1 );
							s_Uv[2].y = s_Uv[1].y;
							s_Uv[3].y = s_Uv[0].y;

							float val = fillClockwise ? fillAmount * 2f - side : fillAmount * 2f - ( 1 - side );

							if( RadialCut( s_Xy, s_Uv, Mathf.Clamp01( val ), fillClockwise, ( ( side + fillOrigin + 3 ) % 4 ) ) )
							{
								AddQuad( toFill, s_Xy, color, s_Uv );
							}
						}
					}
					else if( fillMethod==FillMethod.Radial360 )
					{
						for( int corner = 0; corner<4; ++corner )
						{
							float fx0, fx1, fy0, fy1;

							if( corner<2 )
							{
								fx0 = 0f;
								fx1 = 0.5f;
							}
							else
							{
								fx0 = 0.5f;
								fx1 = 1f;
							}

							if( corner==0 || corner==3 )
							{
								fy0 = 0f;
								fy1 = 0.5f;
							}
							else
							{
								fy0 = 0.5f;
								fy1 = 1f;
							}

							s_Xy[0].x = Mathf.Lerp( v.x, v.z, fx0 );
							s_Xy[1].x = s_Xy[0].x;
							s_Xy[2].x = Mathf.Lerp( v.x, v.z, fx1 );
							s_Xy[3].x = s_Xy[2].x;

							s_Xy[0].y = Mathf.Lerp( v.y, v.w, fy0 );
							s_Xy[1].y = Mathf.Lerp( v.y, v.w, fy1 );
							s_Xy[2].y = s_Xy[1].y;
							s_Xy[3].y = s_Xy[0].y;

							s_Uv[0].x = Mathf.Lerp( tx0, tx1, fx0 );
							s_Uv[1].x = s_Uv[0].x;
							s_Uv[2].x = Mathf.Lerp( tx0, tx1, fx1 );
							s_Uv[3].x = s_Uv[2].x;

							s_Uv[0].y = Mathf.Lerp( ty0, ty1, fy0 );
							s_Uv[1].y = Mathf.Lerp( ty0, ty1, fy1 );
							s_Uv[2].y = s_Uv[1].y;
							s_Uv[3].y = s_Uv[0].y;

							float val = fillClockwise ?
								fillAmount * 4f - ( ( corner + fillAmount ) % 4 ) :
								fillAmount * 4f - ( 3 - ( ( corner + fillOrigin ) % 4 ) );

							if( RadialCut( s_Xy, s_Uv, Mathf.Clamp01( val ), fillClockwise, ( ( corner + 2 ) % 4 ) ) )
								AddQuad( toFill, s_Xy, color, s_Uv );
						}
					}
				}
				else
				{
					AddQuad( toFill, s_Xy, color, s_Uv );
				}
			}
		}

		private static void AddQuad( VertexHelper vertexHelper, Vector3[] quadPositions, Color32 color, Vector3[] quadUVs )
		{
			int startIndex = vertexHelper.currentVertCount;

			for( int i = 0; i<4; ++i )
				vertexHelper.AddVert( quadPositions[i], color, quadUVs[i] );

			vertexHelper.AddTriangle( startIndex, startIndex + 1, startIndex + 2 );
			vertexHelper.AddTriangle( startIndex + 2, startIndex + 3, startIndex );
		}

		private static void AddQuad( VertexHelper vertexHelper, Vector2 posMin, Vector2 posMax, Color32 color, Vector2 uvMin, Vector2 uvMax )
		{
			int startIndex = vertexHelper.currentVertCount;

			vertexHelper.AddVert( new Vector3( posMin.x, posMin.y, 0 ), color, new Vector2( uvMin.x, uvMin.y ) );
			vertexHelper.AddVert( new Vector3( posMin.x, posMax.y, 0 ), color, new Vector2( uvMin.x, uvMax.y ) );
			vertexHelper.AddVert( new Vector3( posMax.x, posMax.y, 0 ), color, new Vector2( uvMax.x, uvMax.y ) );
			vertexHelper.AddVert( new Vector3( posMax.x, posMin.y, 0 ), color, new Vector2( uvMax.x, uvMin.y ) );

			vertexHelper.AddTriangle( startIndex, startIndex + 1, startIndex + 2 );
			vertexHelper.AddTriangle( startIndex + 2, startIndex + 3, startIndex );
		}

		private Vector4 GetAdjustedBorders( Vector4 border, Rect rect )
		{
			for( int axis = 0; axis<=1; axis++ )
			{
				// If the rect is smaller than the combined borders, then there's not room for the borders at their normal size.
				// In order to avoid artefacts with overlapping borders, we scale the borders down to fit.
				float combinedBorders = border[axis] + border[axis + 2];
				if( rect.size[axis]<combinedBorders && combinedBorders!=0 )
				{
					float borderScaleRatio = rect.size[axis] / combinedBorders;
					border[axis] *= borderScaleRatio;
					border[axis + 2] *= borderScaleRatio;
				}
			}
			return border;
		}

		/// <summary>
		/// Adjust the specified quad, making it be radially filled instead.
		/// </summary>

		private static bool RadialCut( Vector3[] xy, Vector3[] uv, float fill, bool invert, int corner )
		{
			// Nothing to fill
			if( fill<0.001f ) return false;

			// Even corners invert the fill direction
			if( ( corner & 1 )==1 ) invert = !invert;

			// Nothing to adjust
			if( !invert && fill>0.999f ) return true;

			// Convert 0-1 value into 0 to 90 degrees angle in radians
			float angle = Mathf.Clamp01( fill );
			if( invert ) angle = 1f - angle;
			angle *= 90f * Mathf.Deg2Rad;

			// Calculate the effective X and Y factors
			float cos = Mathf.Cos( angle );
			float sin = Mathf.Sin( angle );

			RadialCut( xy, cos, sin, invert, corner );
			RadialCut( uv, cos, sin, invert, corner );
			return true;
		}

		/// <summary>
		/// Adjust the specified quad, making it be radially filled instead.
		/// </summary>

		private static void RadialCut( Vector3[] xy, float cos, float sin, bool invert, int corner )
		{
			int i0 = corner;
			int i1 = ( ( corner + 1 ) % 4 );
			int i2 = ( ( corner + 2 ) % 4 );
			int i3 = ( ( corner + 3 ) % 4 );

			if( ( corner & 1 )==1 )
			{
				if( sin>cos )
				{
					cos /= sin;
					sin = 1f;

					if( invert )
					{
						xy[i1].x = Mathf.Lerp( xy[i0].x, xy[i2].x, cos );
						xy[i2].x = xy[i1].x;
					}
				}
				else if( cos>sin )
				{
					sin /= cos;
					cos = 1f;

					if( !invert )
					{
						xy[i2].y = Mathf.Lerp( xy[i0].y, xy[i2].y, sin );
						xy[i3].y = xy[i2].y;
					}
				}
				else
				{
					cos = 1f;
					sin = 1f;
				}

				if( !invert ) xy[i3].x = Mathf.Lerp( xy[i0].x, xy[i2].x, cos );
				else xy[i1].y = Mathf.Lerp( xy[i0].y, xy[i2].y, sin );
			}
			else
			{
				if( cos>sin )
				{
					sin /= cos;
					cos = 1f;

					if( !invert )
					{
						xy[i1].y = Mathf.Lerp( xy[i0].y, xy[i2].y, sin );
						xy[i2].y = xy[i1].y;
					}
				}
				else if( sin>cos )
				{
					cos /= sin;
					sin = 1f;

					if( invert )
					{
						xy[i2].x = Mathf.Lerp( xy[i0].x, xy[i2].x, cos );
						xy[i3].x = xy[i2].x;
					}
				}
				else
				{
					cos = 1f;
					sin = 1f;
				}

				if( invert ) xy[i3].y = Mathf.Lerp( xy[i0].y, xy[i2].y, sin );
				else xy[i1].x = Mathf.Lerp( xy[i0].x, xy[i2].x, cos );
			}
		}
	}
}
