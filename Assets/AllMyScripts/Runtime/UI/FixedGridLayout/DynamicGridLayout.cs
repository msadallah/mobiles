﻿namespace AllMyScripts.Common.UI
{
    using System;
    using System.Collections.Generic;

    using UnityEngine;

    public class DynamicGridLayout : FixedGridLayout
    {

        /// <summary>
        /// one parameter for the size of the grid
        /// </summary>
        [System.Serializable]
        public class GridParameter : IComparable<GridParameter>
        {
            public int sizeInPixels;
            public int nCount;

            public int CompareTo(GridParameter other)
            {
                return sizeInPixels.CompareTo(other.sizeInPixels);
            }
        }

        /// <summary>
        /// list of all the parameters. Set the count matching the size pixels and voila
        /// </summary>
        public GridParameter[] gridParameters;


        public override void SetLayoutHorizontal()
        {
            UpdateCount();
            base.SetLayoutHorizontal();
        }

        public override void SetLayoutVertical()
        {
            UpdateCount();
            base.SetLayoutVertical();
        }

        private void UpdateCount()
        {
            List<GridParameter> parameters = new List<GridParameter>(gridParameters);
            parameters.Sort();
            m_Count = 0;
            float size = order == Direction.Horizontal ? ((RectTransform)transform).rect.width : ((RectTransform)transform).rect.height;
            for (int i = 0; i < parameters.Count; ++i)
            {
                if (size > parameters[i].sizeInPixels)
                {
                    //this is the right one
                    m_Count = Mathf.Clamp(parameters[i].nCount, 1, int.MaxValue);
                }
            }
            if (m_Count == 0)
                m_Count = parameters[0].nCount;
        }
    }
}