namespace AllMyScripts.Common.UI
{
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    using System.Collections.Generic;

    /// <summary>
    /// Apply this UIBehaviour to a Panel in order to distribute its children using weights
    /// </summary>
    [ExecuteInEditMode]
    [AddComponentMenu("Layout/lw/Fixed Grid Layout", 140)]
    public class FixedGridLayout : UIBehaviour, ILayoutGroup, ILayoutElement
    {
        public enum Direction { Horizontal, Vertical }
        #region Order order
        [SerializeField]
        protected Direction m_Order = default;

        public Direction order
        {
            get { return m_Order; }
            set { m_Order = value; SetDirty(); }
        }
        #endregion

        #region float fSpacing
        [SerializeField]
        [Tooltip("Distance in pixels separating elements")]
        protected float m_Spacing = default;

        public float spacing
        {
            get { return m_Spacing; }
            set { m_Spacing = value; SetDirty(); }
        }
        #endregion
        #region float fOffset
        [SerializeField]
        [Tooltip("Distance in pixels separating elements")]
        protected float m_Offset = default;

        public float offset
        {
            get { return m_Offset; }
            set { m_Offset = value; SetDirty(); }
        }
        #endregion

        #region int count
        [SerializeField]
        protected int m_Count = 1;

        public int nCount
        {
            get { return m_Count; }
            set { m_Count = Mathf.Clamp(value, 1, int.MaxValue); SetDirty(); }
        }
        #endregion

        #region int size
        [SerializeField]
        protected float m_Size = 1;

        public float size
        {
            get { return m_Size; }
            set { m_Size = value; SetDirty(); }
        }
        #endregion

        #region float itemSpacing
        [SerializeField]
        protected float m_itemSpacing = 0;

        public float itemSpacing
        {
            get { return m_itemSpacing; }
            set { m_itemSpacing = value; SetDirty(); }
        }
        #endregion


        // Used to block the variables in the rectTransform
        private DrivenRectTransformTracker m_drtt = new DrivenRectTransformTracker();

        private bool isRootLayoutGroup
        {
            get
            {
                Transform parent = transform.parent;
                if (parent == null)
                    return true;
                return transform.parent.GetComponent(typeof(ILayoutGroup)) == null;
            }
        }
        #region ILayoutElement
        public float minWidth
        {
            get
            {
                return preferredWidth;
            }
        }

        public float preferredWidth
        {
            get
            {
                RectTransform[] children = GetAffectedChildren();
                float nColumns = Mathf.Ceil((float)children.Length / (float)nCount);
                return size * nColumns + (nColumns + 1) * spacing;
            }
        }

        public float flexibleWidth
        {
            get
            {
                return 0;
            }
        }

        public float minHeight
        {
            get
            {
                return preferredHeight;
            }
        }

        public float preferredHeight
        {
            get
            {
                RectTransform[] children = GetAffectedChildren();
                float nRows = Mathf.Ceil((float)children.Length / (float)nCount);
                return size * nRows + (nRows + 1) * spacing;
            }
        }

        public float flexibleHeight
        {
            get
            {
                return 0;
            }
        }

        public int layoutPriority
        {
            get
            {
                return 0;
            }
        }
        #endregion



        #region Unity Lifetime calls

        protected override void OnEnable()
        {
            base.OnEnable();
            SetDirty();
        }

        protected override void OnDisable()
        {
            m_drtt.Clear();
            LayoutRebuilder.MarkLayoutForRebuild((RectTransform)transform);
            base.OnDisable();
        }

        protected override void OnDidApplyAnimationProperties()
        {
            SetDirty();
        }


        protected override void OnRectTransformDimensionsChange()
        {
            base.OnRectTransformDimensionsChange();
            if (isRootLayoutGroup)
                SetDirty();
        }

        protected void OnTransformChildrenChanged()
        {
            SetDirty();
        }
        #endregion

        override protected void Awake()
        {
            base.Awake();
            SetDirty();
        }

        /// <summary>
        /// Implemented by ILayoutGroup
        /// </summary>
        public virtual void SetLayoutHorizontal()
        {
            RectTransform[] children = GetAffectedChildren();
            m_drtt.Clear();
            //counts the first loop's values
            int nCounterX = 0;
            //counts the second loop's values
            int nCounterY = 0;
            //counts the current child
            int iterator = 0;

            float inverseCount = (1f / (float)nCount);

            while (iterator < children.Length)
            {
                m_drtt.Add(this, children[iterator],
                    DrivenTransformProperties.AnchoredPosition |
                    DrivenTransformProperties.Anchors |
                    (m_Order == Direction.Horizontal ? DrivenTransformProperties.PivotY : DrivenTransformProperties.PivotX) |
                    DrivenTransformProperties.SizeDelta);


                if (m_Order == Direction.Horizontal)
                {
                    children[iterator].pivot = new Vector2(children[iterator].pivot.x, 1);
                    //calculate the child's RectTransform
                    children[iterator].anchorMin = new Vector2((float)nCounterX * inverseCount, children[iterator].anchorMin.y);
                    children[iterator].anchorMax = new Vector2(((float)nCounterX + 1f) * inverseCount, children[iterator].anchorMin.y);

                    children[iterator].offsetMin = new Vector2(m_itemSpacing * 0.5f, 0);
                    children[iterator].offsetMax = new Vector2(-m_itemSpacing * 0.5f, 0);
                    if (nCounterX != 0)
                    {
                        children[iterator].offsetMin = new Vector2(m_itemSpacing, 0);
                    }
                    if (nCounterX != nCount - 1)
                    {
                        children[iterator].offsetMax = new Vector2(-m_itemSpacing, 0);
                    }

                    children[iterator].sizeDelta = new Vector2(children[iterator].sizeDelta.x - offset, size);



                    nCounterX++;
                    if (nCounterX == nCount)
                    {
                        nCounterX = 0;
                        nCounterY++;
                    }
                }
                else
                {
                    //calculate the child's RectTransform
                    children[iterator].pivot = new Vector2(0, children[iterator].pivot.y);

                    children[iterator].anchorMin = new Vector2(0, children[iterator].anchorMin.y);
                    children[iterator].anchorMax = new Vector2(0, children[iterator].anchorMax.y);

                    children[iterator].anchoredPosition = new Vector2(nCounterY * size + (nCounterY + 1) * spacing, children[iterator].anchoredPosition.y);
                    children[iterator].localPosition = new Vector3(children[iterator].localPosition.x, children[iterator].localPosition.y);

                    children[iterator].sizeDelta = new Vector2(size, children[iterator].sizeDelta.y - offset);


                    nCounterX++;
                    if (nCounterX == nCount)
                    {
                        nCounterX = 0;
                        nCounterY++;
                    }
                }
                iterator++;
            }
        }

        /// <summary>
        /// Implemented by ILayoutGroup
        /// </summary>
        public virtual void SetLayoutVertical()
        {
            RectTransform[] children = GetAffectedChildren();
            m_drtt.Clear();

            //counts the first loop's values
            int nCounterX = 0;
            //counts the second loop's values
            int nCounterY = 0;
            //counts the current child
            int iterator = 0;

            float inverseCount = (1f / (float)nCount);

            while (iterator < children.Length)
            {

                m_drtt.Add(this, children[iterator],
                    DrivenTransformProperties.AnchoredPosition |
                    DrivenTransformProperties.Anchors |
                    (m_Order == Direction.Horizontal ? DrivenTransformProperties.PivotY : DrivenTransformProperties.PivotX) |
                    DrivenTransformProperties.SizeDelta);


                if (m_Order == Direction.Horizontal)
                {
                    //calculate the child's RectTransform
                    children[iterator].pivot = new Vector2(children[iterator].pivot.x, 1);

                    children[iterator].anchorMin = new Vector2(children[iterator].anchorMin.x, 1);
                    children[iterator].anchorMax = new Vector2(children[iterator].anchorMax.x, 1);

                    children[iterator].offsetMin = new Vector2(m_itemSpacing, 0);
                    children[iterator].offsetMax = new Vector2(-m_itemSpacing, 0);
                    if (nCounterX != 0)
                    {
                        children[iterator].offsetMin = new Vector2(m_itemSpacing * .5f, 0);
                    }
                    if (nCounterX != nCount - 1)
                    {
                        children[iterator].offsetMax = new Vector2(-m_itemSpacing * .5f, 0);
                    }

                    children[iterator].anchoredPosition = new Vector2(children[iterator].anchoredPosition.x, -nCounterY * size + (-nCounterY - 1) * spacing);
                    children[iterator].localPosition = new Vector3(children[iterator].localPosition.x, children[iterator].localPosition.y);

                    children[iterator].sizeDelta = new Vector2(children[iterator].sizeDelta.x - offset, size);


                    nCounterX++;
                    if (nCounterX == nCount)
                    {
                        nCounterX = 0;
                        nCounterY++;
                    }
                }
                else
                {
                    children[iterator].pivot = new Vector2(0, children[iterator].pivot.y);
                    //calculate the child's RectTransform
                    children[iterator].anchorMin = new Vector2(children[iterator].anchorMin.x, 1f - ((float)(nCounterX + 1) * inverseCount));
                    children[iterator].anchorMax = new Vector2(children[iterator].anchorMin.x, 1f - (((float)nCounterX) * inverseCount));

                    children[iterator].offsetMin = new Vector2(0, m_itemSpacing);
                    children[iterator].offsetMax = new Vector2(0, -m_itemSpacing);
                    if (nCounterX != 0)
                    {
                        children[iterator].offsetMin = new Vector2(0, m_itemSpacing * .5f);
                    }
                    if (nCounterX != nCount - 1)
                    {
                        children[iterator].offsetMax = new Vector2(0, -m_itemSpacing * .5f);
                    }

                    children[iterator].anchoredPosition = new Vector2(nCounterY * size + (nCounterY + 1) * spacing, children[iterator].anchoredPosition.y);
                    children[iterator].localPosition = new Vector3(children[iterator].localPosition.x, children[iterator].localPosition.y);

                    children[iterator].sizeDelta = new Vector2(size, children[iterator].sizeDelta.y - offset);


                    nCounterX++;
                    if (nCounterX == nCount)
                    {
                        nCounterX = 0;
                        nCounterY++;
                    }
                }
                iterator++;
            }
        }
        /// <summary>
        /// returns all children that don't ignore layout and are active
        /// </summary>
        /// <returns></returns>
        private RectTransform[] GetAffectedChildren()
        {
            List<RectTransform> children = new List<RectTransform>();
            for (int i = 0; i < transform.childCount; ++i)
            {
                if (!transform.GetChild(i).gameObject.activeInHierarchy) continue;
                ILayoutIgnorer layoutIgnorer = transform.GetChild(i).gameObject.GetComponent<ILayoutIgnorer>();
                if (layoutIgnorer != null)
                {
                    if (layoutIgnorer.ignoreLayout) continue;
                }
                children.Add((RectTransform)transform.GetChild(i));
            }
            return children.ToArray();
        }


#if UNITY_EDITOR
        protected override void OnValidate()
        {
            SetDirty();
        }
#endif
        protected override void OnDestroy()
        {
            m_drtt.Clear();
            base.OnDestroy();
            SetLayoutHorizontal();
            SetLayoutVertical();
        }

        protected void SetDirty()
        {
            if (!IsActive())
                return;
            m_Count = Mathf.Clamp(m_Count, 1, int.MaxValue);
            LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
        }

        public void CalculateLayoutInputHorizontal()
        {
        }

        public void CalculateLayoutInputVertical()
        {
        }
    }
}