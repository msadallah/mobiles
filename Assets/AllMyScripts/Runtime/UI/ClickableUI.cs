﻿namespace AllMyScripts.Common.UI
{
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.EventSystems;

	public class ClickableUI : MonoBehaviour, IPointerClickHandler
	{
		public Button.ButtonClickedEvent leftClick = new Button.ButtonClickedEvent();
		public Button.ButtonClickedEvent middleClick = new Button.ButtonClickedEvent();
		public Button.ButtonClickedEvent rightClick = new Button.ButtonClickedEvent();
		public void OnPointerClick(PointerEventData eventData)
		{
			if (eventData.button == PointerEventData.InputButton.Left)
				leftClick.Invoke();
			else if (eventData.button == PointerEventData.InputButton.Middle)
				middleClick.Invoke();
			else if (eventData.button == PointerEventData.InputButton.Right)
				rightClick.Invoke();
			;
		}
	}
}