﻿namespace AllMyScripts.Common.UI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.EventSystems;

	public class ScaleOnHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		[Tooltip("The RectTransform target you want to scale on hover")]
		[SerializeField] private RectTransform _target = null;
		[SerializeField] private float _scaleAmountOnEnter = 1.5f;
		[SerializeField] private float _scaleAmountOnExit = 1f;

		[Header("Animation")]
		[SerializeField] private bool _animated = true;
		[SerializeField] private float _animDuration = 0.2f;

		private Coroutine _scalingCo = null;

		void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
		{
			SetScale(_scaleAmountOnEnter);
		}

		void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
		{
			SetScale(_scaleAmountOnExit);
		}

		private void SetScale(float value)
		{
			if (_animated)
			{
				if (_scalingCo != null)
					StopCoroutine(_scalingCo);
				_scalingCo = StartCoroutine(SetScaleEnum(value));
			}
			else
				_target.localScale = Vector3.one * value;
		}

		private IEnumerator SetScaleEnum(float value)
		{
			float startScale = _target.localScale.x;
			float delta = value - startScale;

			float startTime = Time.time;
			while (Time.time - startTime < _animDuration)
			{
				float ratio01 = (Time.time - startTime) / _animDuration;
				_target.localScale = Vector3.one * (startScale + delta * ratio01);
				yield return null;
			}

			_target.localScale = Vector3.one * value;
		}
	}
}