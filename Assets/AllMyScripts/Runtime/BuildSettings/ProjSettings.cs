namespace AllMyScripts.Builder
{
	using UnityEngine;
#if UNITY_EDITOR
	using UnityEditor;
#endif

	using System;
	using System.Collections.Generic;

	using AllMyScripts.Common.Tools;

    public enum ProjTarget
	{
		Runtime,
		Editor,
		Both,
		None
	}

	[Serializable]
	public sealed class BuildVariable
	{
		public BuildVariable(string sName, string sValue, ProjTarget eTarget)
		{
			name = sName;
			value = sValue;
			target = eTarget;
		}

		public string name;
		public string value;
		public ProjTarget target;

		public void GenerateAsXml(Common.Tools.Xml.XmlWriter xw)
		{
			if ( string.IsNullOrEmpty( name ) || string.IsNullOrEmpty( value ) )
				return;

			xw.WriteStartElement("VARIABLE");
			xw.WriteAttribute("name", name);
			xw.WriteAttribute("value", value);
			xw.WriteAttribute("target", target.ToString());
			xw.WriteEndElement();
		}
	}

	public sealed class ProjSettings
	{
		public const string PLAYER_PREFS_PATH = "Builder.buildSettingsPath";

		private static Dictionary<string, BuildVariable> s_variables;

		public static Dictionary<string, BuildVariable> Variables
		{
			get
			{
				if (s_variables == null)
				{
					s_variables = new Dictionary<string, BuildVariable>();
					Load();
				}
				return s_variables;
			}
		}

		public static bool ReadXml(string sXml)
		{
			Common.Tools.Xml.XmlReader xr = new Common.Tools.Xml.XmlReader();
			if (xr.Init(sXml))
			{
				while (xr.Read())
				{
					if (xr.IsNodeElement())
					{
#if UNITY_EDITOR
						GlobalTools.Assert(xr.Name.ToUpper() == xr.Name, "Element is not in uppercase !");
#endif
						switch (xr.Name)
						{
							case "VARIABLE":
								string sVarName = xr.GetAttribute( "name" );
								string sVarValue = xr.GetAttribute( "value" );
								ProjTarget target = xr.GetAttributeEnum( "target", ProjTarget.Editor );
								BuildVariable newVar = new BuildVariable( sVarName, sVarValue, target );
								Variables[sVarName] = newVar;
								break;
						}
					}
				}
				xr.Close();
				return true;
			}
			return false;
		}

		public static void LoadInitSettings(string sFileContent)
		{
			if (Variables != null)
			{
				if (!string.IsNullOrEmpty(sFileContent))
				{
					ReadXml(sFileContent);
				}
			}
		}

		public static bool Load()
		{
#if UNITY_EDITOR
			string sPath = EditorPrefs.GetString( PLAYER_PREFS_PATH, null );
			if (!string.IsNullOrEmpty(sPath) && Load(sPath, true))
				return true;
#endif
			return Load("BuildSettings", false);
		}

		public static bool Load(string sFilePath, bool bFullPath)
		{
			Variables.Clear();
			string sXml = string.Empty;
			if (bFullPath)
				sXml = GlobalTools.LoadTextFileAtFullPath(sFilePath);
			else
				sXml = GlobalTools.LoadTextFile(sFilePath);
			return ReadXml(sXml);
		}

#if UNITY_EDITOR
		public static void Save(List<BuildVariable> buildVariables)
		{
			string sFilePath = Application.dataPath + "/Resources/BuildSettings.txt";

			Common.Tools.Xml.XmlWriter xw = new Common.Tools.Xml.XmlWriter();

			xw.WriteStartElement("PROJSETTINGS");
			foreach (BuildVariable buildVar in buildVariables)
			{
				if (buildVar.target == ProjTarget.Both || buildVar.target == ProjTarget.Runtime)
				{
					xw.WriteStartElement("VARIABLE");
					xw.WriteAttribute("name", buildVar.name);
					xw.WriteAttribute("value", buildVar.value);
					xw.WriteAttribute("target", buildVar.target.ToString());
					xw.WriteEndElement();
				}
			}
			xw.WriteEndElement();

			Variables.Clear();
			foreach ( BuildVariable buildVar in buildVariables )
				Variables.Add(buildVar.name, buildVar);

			GlobalTools.SaveTextFile(sFilePath, xw.GetString());
		}
#endif

		public static string GetVariableValue(string sVariableName)
		{
			Variables.TryGetValue(sVariableName, out BuildVariable buildVar);
			return buildVar != null ? buildVar.value : string.Empty;
		}

		public static void SetVariableValue(string sVariableName, string sValue)
		{
			BuildVariable buildVar = Variables[ sVariableName ];
			buildVar.value = sValue;
		}

		public static bool GetBoolVariableValue(string sVariableName)
		{
			return GlobalTools.ReadBool(GetVariableValue(sVariableName), false);
		}

		public static void SetBoolVariableValue(string sVariableName, bool bValue)
		{
			SetVariableValue(sVariableName, bValue ? "true" : "false");
		}

		// ----------------------------------------------------------------------------------
		// ----------------------------------------------------------------------------------
		// Static functions enabling direct access to some common used variables
		// ----------------------------------------------------------------------------------
		// ----------------------------------------------------------------------------------
		public static bool IsReleaseBuild()
		{
			return !GetBoolVariableValue( "DEVELOPMENT_BUILD" );
		}

		public static string GetOwnerName()
		{
			return GetVariableValue("OWNERNAME");
		}

		public static string GetAppName()
		{
			return GetVariableValue("APPNAME");
		}

		public static string GetSecretKey()
		{
			return GetVariableValue("SECRETKEY");
		}

		public static string GetiOSCameraDescription()
		{
			return GetVariableValue("CAMERA_DESCRIPTION");
		}
		public static string GetiOSLibraryDescription()
		{
			return GetVariableValue("LIBRARY_DESCRIPTION");
		}
		public static string GetiOSLocationDescription()
		{
			return GetVariableValue("LOCATION_DESCRIPTION");
		}
		public static string GetiOSBluetoothDescription()
		{
			return GetVariableValue("BLUETOOTH_DESCRIPTION");
		}

		public static string GetRoomId()
		{
			return GetVariableValue("ROOM_ID");
		}
	}
}
