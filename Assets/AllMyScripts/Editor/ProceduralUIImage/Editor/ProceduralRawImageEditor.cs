using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.ProceduralImage;
using System;
using UnityEngine.UI;
using UnityEditor.AnimatedValues;

namespace UnityEditor.UI
{
    [CustomEditor(typeof(ProceduralRawImage), true)]
    [CanEditMultipleObjects]
    public class ProceduralRawImageEditor : RawImageEditor
    {
        static List<ModifierID> attrList;

		// No Border
        //SerializedProperty m_borderWidth;
        SerializedProperty m_falloffDist;
		// CUSTOM ADD - Property for adaptFalloffCorners
		SerializedProperty m_adaptFalloffCorner;
		// CUSTOM ADD - Property for adaptFalloffSize
		SerializedProperty m_adaptFalloffSize;

		SerializedProperty m_Texture;
		SerializedProperty m_UVRect;
		SerializedProperty m_IgnoreMask;

		GUIContent m_newUVRectContent;

		int selectedId;

        protected override void OnEnable()
        {
            base.OnEnable();

			// Note we have precedence for calling rectangle for just rect, even in the Inspector.
			// For example in the Camera component's Viewport Rect.
			// Hence sticking with Rect here to be consistent with corresponding property in the API.
			m_newUVRectContent = EditorGUIUtility.TrTextContent( "UV Rect" );

			m_Texture = serializedObject.FindProperty("m_Texture");
			m_UVRect = serializedObject.FindProperty( "m_UVRect" );

			m_IgnoreMask = serializedObject.FindProperty("ignoreMask");

            attrList = ModifierUtility.GetAttributeList();

			// No border
            //m_borderWidth = serializedObject.FindProperty("borderWidth");
            m_falloffDist = serializedObject.FindProperty("falloffDistance");
			// CUSTOM ADD - Find adaptFalloffSize property
			m_adaptFalloffSize = serializedObject.FindProperty( "adaptFalloffSize" );
			// CUSTOM ADD - Find adaptFalloffCorners property
			m_adaptFalloffCorner = serializedObject.FindProperty( "adaptFalloffCorners" );

			if ((target as ProceduralRawImage).GetComponent<ProceduralImageModifier>() != null)
            {
                selectedId = attrList.IndexOf(((ModifierID[])(target as ProceduralRawImage).GetComponent<ProceduralImageModifier>().GetType().GetCustomAttributes(typeof(ModifierID), false))[0]);
            }
            selectedId = Mathf.Max(selectedId, 0);
            EditorApplication.update -= UpdateProceduralImage;
            EditorApplication.update += UpdateProceduralImage;
        }

        /// <summary>
        /// Updates the procedural image in Edit mode. This will prevent issues when working with layout components.
        /// </summary>
        public void UpdateProceduralImage()
        {
            if (target != null)
            {
                (target as ProceduralRawImage).Update();
            }
            else
            {
                EditorApplication.update -= UpdateProceduralImage;
            }
        }

        public override void OnInspectorGUI()
        {

            CheckForShaderChannelsGUI();

            serializedObject.Update();

            ProceduralRawImageTypeGUI();
            EditorGUILayout.Space();
            ModifierGUI();
			// No Border
            //EditorGUILayout.PropertyField(m_borderWidth);
            EditorGUILayout.PropertyField(m_falloffDist);
			// CUSTOM ADD - Show adaptFalloffSize property
			EditorGUILayout.PropertyField( m_adaptFalloffSize );
			// CUSTOM ADD - Show adaptFalloffCorners property
			EditorGUILayout.PropertyField( m_adaptFalloffCorner );
			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(m_IgnoreMask);
			if(serializedObject.ApplyModifiedProperties())
			{
				foreach (var t in targets) ((ProceduralRawImage)t).UpdateNoMask();
			}
        }

        /// <summary>
        /// Sprites's custom properties based on the type.
        /// </summary>
		protected void ProceduralRawImageTypeGUI()
		{
			EditorGUILayout.PropertyField( m_Texture );
			AppearanceControlsGUI();
			RaycastControlsGUI();
			EditorGUILayout.PropertyField( m_UVRect, m_newUVRectContent );
			SetShowNativeSize( false );
			NativeSizeButtonGUI();
		}

		void SetShowNativeSize( bool instant )
		{
			base.SetShowNativeSize( m_Texture.objectReferenceValue != null, instant );
		}

		void CheckForShaderChannelsGUI()
        {
            Canvas c = (target as Component).GetComponentInParent<Canvas>();
            if (c != null && (c.additionalShaderChannels | AdditionalCanvasShaderChannels.TexCoord1 | AdditionalCanvasShaderChannels.TexCoord2 | AdditionalCanvasShaderChannels.TexCoord3) != c.additionalShaderChannels)
            {
                //Texcoord1 not enabled;
                EditorGUILayout.HelpBox("TexCoord1,2,3 are not enabled as an additional shader channel in parent canvas. Procedural Image will not work properly", MessageType.Error);
                if (GUILayout.Button("Fix: Enable TexCoord1,2,3 in Canvas: " + c.name))
                {
                    Undo.RecordObject(c, "enable TexCoord1,2,3 as additional shader channels");
                    c.additionalShaderChannels |= AdditionalCanvasShaderChannels.TexCoord1 | AdditionalCanvasShaderChannels.TexCoord2 | AdditionalCanvasShaderChannels.TexCoord3;
                }
            }
        }

        protected void ModifierGUI()
        {
            GUIContent[] con = new GUIContent[attrList.Count];
            for (int i = 0; i < con.Length; i++)
            {
                con[i] = new GUIContent(attrList[i].Name);
            }


            bool hasMultipleValues = false;
            if (targets.Length > 1)
            {
                Type t = (targets[0] as ProceduralRawImage).GetComponent<ProceduralImageModifier>().GetType();
                foreach (var item in targets)
                {
                    if ((item as ProceduralRawImage).GetComponent<ProceduralImageModifier>().GetType() != t)
                    {
                        hasMultipleValues = true;
                        break;
                    }
                }
            }

            if(!hasMultipleValues)
            {
                int index = EditorGUILayout.Popup(new GUIContent("Modifier Type"), selectedId, con);
                if (selectedId != index)
                {
                    selectedId = index;
                    foreach (var item in targets)
                    {
                        (item as ProceduralRawImage).ModifierType = ModifierUtility.GetTypeWithId(attrList[selectedId].Name);

                        MoveComponentBehind((item as ProceduralRawImage), (item as ProceduralRawImage).GetComponent<ProceduralImageModifier>());
                    }
                    //Exit GUI prevents Unity from trying to draw destroyed components editor;
                    EditorGUIUtility.ExitGUI();
                }
            }
            else{
                int index = EditorGUILayout.Popup(new GUIContent("Modifier Type"), -1, con);
                if (index != -1)
                {
                    selectedId = index;
                    foreach (var item in targets)
                    {
                        (item as ProceduralRawImage).ModifierType = ModifierUtility.GetTypeWithId(attrList[selectedId].Name);

                        MoveComponentBehind((item as ProceduralRawImage), (item as ProceduralRawImage).GetComponent<ProceduralImageModifier>());
                    }
                    //Exit GUI prevents Unity from trying to draw destroyed components editor;
                    EditorGUIUtility.ExitGUI();
                }
            }
        }

        public override string GetInfoString()
        {
            ProceduralRawImage image = target as ProceduralRawImage;
            return string.Format("Modifier: {0}, Line-Weight: {1}", attrList[selectedId].Name, image.BorderWidth);
        }
        /// <summary>
        /// Moves a component behind a reference component.
        /// </summary>
        /// <param name="reference">Reference component.</param>
        /// <param name="componentToMove">Component to move.</param>
        static void MoveComponentBehind(Component reference, Component componentToMove)
        {
            if (reference == null || componentToMove == null || reference.gameObject != componentToMove.gameObject)
            {
                return;
            }
            Component[] comps = reference.GetComponents<Component>();
            List<Component> list = new List<Component>();
            list.AddRange(comps);
            int i = list.IndexOf(componentToMove) - list.IndexOf(reference);
            while (i != 1)
            {
                if (i < 1)
                {
                    UnityEditorInternal.ComponentUtility.MoveComponentDown(componentToMove);
                    i++;
                }
                else if (i > 1)
                {
                    UnityEditorInternal.ComponentUtility.MoveComponentUp(componentToMove);
                    i--;
                }
            }
        }

        protected enum ProceduralImageType
        {
            Simple = 0,
            Filled = 3
        }
    }
}