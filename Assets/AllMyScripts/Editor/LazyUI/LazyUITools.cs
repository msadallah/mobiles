using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class LazyUITools
{
	[MenuItem( "AllMyScripts/Tools/Lazy UI/Make Selection Lazy UI" )]
	//adds the lazy UI Material to the object and its children, but not on masks
	public static void MakeSelectionLazy()
	{
		if( EditorUtility.DisplayDialog( "Lazy UI", "Apply Lazy UI on " + Selection.gameObjects.Length + " objects ?\nWARNING ! If the prefab is loaded in a mask at runtime, the mask won't be able to apply itself.", "Yes", "No" ) )
		{
			Material mat = ( Material )AssetDatabase.LoadAssetAtPath( "Assets/LazyUI/Materials/Mat_UI_Lazy.mat", typeof( Material ) );
			int nCount = 0;
			int nMaskCount = 0;
			if( mat != null )
			{
				foreach( GameObject go in Selection.gameObjects )
				{
					foreach( UnityEngine.UI.Image image in go.GetComponentsInChildren<UnityEngine.UI.Image>() )
					{
						if( image.gameObject.GetComponent<UnityEngine.UI.Mask>() != null || image.gameObject.GetComponentInParent<RectMask2D>() != null)
						{
							nMaskCount++;
							continue;
						}
						//funfact : getcomponentinparent doesn't work on prefabs
						Transform tParent = image.transform;
                        bool bParentIsMask = false;
						while( tParent.parent != null )
						{
							tParent = tParent.parent;
							if( tParent.gameObject.GetComponent<UnityEngine.UI.Mask>() != null || image.gameObject.GetComponentInParent<RectMask2D>() != null )
							{
								nMaskCount++;
                                bParentIsMask = true;
                                break;
							}
						}
                        if (bParentIsMask)
                        {
                            continue;
                        }

                        if( image.material.ToString().Contains( "Default UI Material" ) )
						{
							if( Mathf.Approximately( image.color.r, 1) && Mathf.Approximately( image.color.g, 1 ) && Mathf.Approximately( image.color.b, 1 ))
							{
								image.material = mat;
							}
							else
							{
								image.material = mat;
							}
							nCount++;
							EditorUtility.SetDirty( go );
						}
					}
				}
				EditorUtility.DisplayDialog( "Lazy UI", nCount + " normal images changed and " + nMaskCount + " images NOT changed because they were children of a mask.", "Ok cool thanks." );
			}
			else
			{
				Debug.LogError( "ERROR - Could not load lazy material : " + "Assets/LazyUI/Materials/Mat_UI_Lazy.mat" );
			}
		}
	}


	[MenuItem("AllMyScripts/Tools/Lazy UI/Make Selection Non-Lazy")]
	//adds the lazy UI Material to the object and its children, but not on masks
	public static void MakeSelectionNonLazy()
	{
		if( EditorUtility.DisplayDialog( "Lazy UI", "Disable Lazy UI on " + Selection.gameObjects.Length + " objects ?", "Yes", "No" ) )
		{
			Material mat = ( Material )AssetDatabase.LoadAssetAtPath( "Assets/LazyUI/Materials/Mat_UI_Lazy.mat", typeof( Material ) );
			int nCount = 0;
			if( mat != null )
			{
				foreach( GameObject go in Selection.gameObjects )
				{
					foreach( UnityEngine.UI.Image image in go.GetComponentsInChildren<UnityEngine.UI.Image>() )
					{
						if( image.material == mat )
						{
							image.material = null;
							nCount++;
						}
					}
					EditorUtility.SetDirty( go );
				}
				EditorUtility.DisplayDialog( "Lazy UI", nCount + " images changed.", "Ok cool thanks." );
			}
			else
			{
				Debug.LogError( "ERROR - Could not load lazy material : " + "Assets/Art_safari/Materials/Mat_UI_Lazy.mat" );
			}
		}
	}


	[MenuItem("AllMyScripts/Tools/Lazy UI/Find Lazy UI in scene with Mask parent")]
	//adds the lazy UI Material to the object and its children, but not on masks
	public static void FindLazy()
	{
        Debug.Log("Finding bad Lazies");
		Image[] images = Object.FindObjectsOfType<Image>();
        int nLaziesCount=0;
        int nBadLaziesCount = 0;
		foreach(Image image in images)
		{
            if (image.material.shader.name.Contains("UI/Lazy"))
            {
                nLaziesCount++;
                if (image.gameObject.GetComponentInParent<Mask>() != null || image.gameObject.GetComponentInParent<RectMask2D>() != null)
                {
                    Debug.Log("LAZY : " + image.gameObject, image.gameObject);
                    nBadLaziesCount++;
                }
            }
		}
        Debug.Log(nLaziesCount + " lazies, including " + nBadLaziesCount + " bad ones");
	}
}