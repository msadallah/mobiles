namespace AllMyScripts.Common.Animation
{
    /*!
     * \author Vincent Paquin
     */
    using UnityEngine;
    using UnityEditor;
    [CustomPropertyDrawer(typeof(CurveAttribute))]
    public class AnimationDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.type != "AnimationCurve")
            {
                base.OnGUI(position, property, label);
            }
            else
            {
                //draw the curve
                Rect curvePosition = new Rect(position);
                curvePosition.height = m_bUnfolded ? 64 : 16;
                property.animationCurveValue = EditorGUI.CurveField(curvePosition, property.displayName, property.animationCurveValue);

                //draw the foldout
                Rect foldoutPosition = new Rect(position);
                foldoutPosition.width = curvePosition.height;
                m_bUnfolded = EditorGUI.Foldout(foldoutPosition, m_bUnfolded, "");

                if (m_bUnfolded)
                {
                    //draw the unfolded content

                    //constructor builder
                    Rect debugPosition = new Rect(position);
                    debugPosition.width = 100;
                    debugPosition.height = 16;
                    debugPosition.y += 64;

                    /*ADDS A BUTTON THAT WILL WRITE A CONSTRUCTOR FOR THE CURRENT CURVE
                     * 
                    if(GUI.Button(debugPosition,"Debug current"))
                    {
                        CurveDatabase.DebugConstructor( property.animationCurveValue );
                    }*/

                    //curve selector
                    Rect popupPosition = new Rect(position);
                    popupPosition.height = 16;
                    popupPosition.y += 64;

                    bool isReversed;
                    int currentSelection = FindCurrentSelection(property.animationCurveValue, out isReversed);

                    string[] allPresetEntries = System.Array.ConvertAll(CurveDatabase.sEditorAnimationPaths, (string item) =>
                        {
                            if (isReversed)
                            {
                                return string.Format("{0} (Reversed)", item);
                            }
                            else
                            {
                                return item;
                            }
                        });

                    EditorGUI.BeginChangeCheck();
                    currentSelection = EditorGUI.Popup(popupPosition, "Preset curve", currentSelection, allPresetEntries);
                    if (EditorGUI.EndChangeCheck())
                    {
                        property.animationCurveValue = CurveDatabase.GetNewAnimationInstance((CurveDatabase.AnimationCurveEnum)currentSelection, isReversed);
                    }

                    Rect reverseCheckboxRect = new Rect(position.x, popupPosition.yMax, position.width, EditorGUIUtility.singleLineHeight);
                    EditorGUI.BeginChangeCheck();
                    isReversed = EditorGUI.Toggle(reverseCheckboxRect, "Reverse", isReversed);
                    if (EditorGUI.EndChangeCheck())
                    {
                        property.animationCurveValue = CurveDatabase.GetNewAnimationInstance((CurveDatabase.AnimationCurveEnum)currentSelection, isReversed);
                    }
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (property.type == "AnimationCurve")
            {
                return m_bUnfolded ? 64 + 16 + EditorGUIUtility.singleLineHeight : 16;
            }
            return base.GetPropertyHeight(property, label);
        }

        #region Private
        private int FindCurrentSelection(AnimationCurve animationCurve, out bool isReversed)
        {
            CurveDatabase.AnimationCurveEnum[] presets = (CurveDatabase.AnimationCurveEnum[])System.Enum.GetValues(typeof(CurveDatabase.AnimationCurveEnum));

            isReversed = false;
            int index = presets.Length - 1;
            while (index >= 0 && AreSameCurves(animationCurve, CurveDatabase.GetNewAnimationInstance(presets[index], false)) == false)
            {
                --index;
            }

            if (index == -1)
            {
                isReversed = true;
                index = presets.Length - 1;
                while (index >= 0 && AreSameCurves(animationCurve, CurveDatabase.GetNewAnimationInstance(presets[index], true)) == false)
                {
                    --index;
                }
            }
            return index;
        }

        private bool AreSameCurves(AnimationCurve curveRef, AnimationCurve curveToTest)
        {
            bool sameCurve = curveRef.keys.Length == curveToTest.keys.Length;
            int keyIndex = 0;
            while (sameCurve && keyIndex < curveRef.keys.Length)
            {
                sameCurve &= curveRef.keys[keyIndex].time == curveToTest.keys[keyIndex].time;
                sameCurve &= curveRef.keys[keyIndex].value == curveToTest.keys[keyIndex].value;
                sameCurve &= AnimationUtility.GetKeyLeftTangentMode(curveRef, keyIndex) == AnimationUtility.GetKeyLeftTangentMode(curveToTest, keyIndex) &&
                             AnimationUtility.GetKeyRightTangentMode(curveRef, keyIndex) == AnimationUtility.GetKeyRightTangentMode(curveToTest, keyIndex);
                sameCurve &= curveRef.keys[keyIndex].inTangent == curveToTest.keys[keyIndex].inTangent;
                sameCurve &= curveRef.keys[keyIndex].outTangent == curveToTest.keys[keyIndex].outTangent;

                ++keyIndex;
            }

            return sameCurve;
        }

        private bool m_bUnfolded = false;
        #endregion
    }
}