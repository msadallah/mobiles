﻿namespace AllMyScripts.Editor.Tools.DebugHelper
{
	using System;
	using System.Collections.Generic;
    using AllMyScripts.Common.Tools.DebugHelper;
    using UnityEditor;

	public abstract class ADebugViewerData
	{
		private DateTime _nextEditorUpdate = new DateTime(1970, 1, 1);
		protected static ADebugViewerData _instance;

		public abstract List<string> visuals
		{
			get;
		}

		public abstract List<string> logs
		{
			get;
		}

		protected ADebugViewerData()
		{

		}

		public void UpdateData()
		{
			DebugKeys.visualDebugsShowState = new Dictionary<string, bool>(visuals.Count);
			for (int i = 0; i < visuals.Count; ++i)
			{
				if (EditorPrefs.HasKey(visuals[i]))
				{
					DebugKeys.visualDebugsShowState.Add(visuals[i], EditorPrefs.GetBool(visuals[i]));
				}
				else
				{
					DebugKeys.visualDebugsShowState.Add(visuals[i], visuals[i].Contains("_D1"));
				}

			}

			DebugKeys.logDebugsShowState = new Dictionary<string, bool>(logs.Count);
			for (int i = 0; i < logs.Count; ++i)
			{
				if (EditorPrefs.HasKey(logs[i]))
				{
					DebugKeys.logDebugsShowState.Add(logs[i], EditorPrefs.GetBool(logs[i]));
				}
				else
				{
					DebugKeys.logDebugsShowState.Add(logs[i], logs[i].Contains("_D1"));
				}

			}

			_nextEditorUpdate = DateTime.Now.AddSeconds(5);
		}

	}
}