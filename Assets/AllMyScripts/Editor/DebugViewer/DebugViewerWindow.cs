﻿namespace AllMyScripts.Editor.Tools.DebugHelper
{
	using System.Collections;
	using System.Collections.Generic;
    using AllMyScripts.Common.Tools.DebugHelper;
    using UnityEditor;
	using UnityEngine;

	public class DebugViewerWindow : EditorWindow
	{
		// GUI
		private GUIStyle _titleStyle;
		private GUIStyle _debugLabelStyle;

		public static float s_windowBorderSpace = 10f;
		public static float s_verticalVisualElementSpace = 7f;
		public static float s_verticalCategorySpace = 15f;

		[MenuItem("AllMyScripts/Tools/DebugViewer", false, 10)]
		static void Init()
		{
			// Get existing open window or if none, make a new one:
			DebugViewerWindow window = (DebugViewerWindow)EditorWindow.GetWindow(typeof(DebugViewerWindow), false,"Debug Viewer");
			window.Show();
		}
		protected virtual void OnGUI()
		{

			SetupStyle();

			GUILayout.BeginVertical();

				GUILayout.Space(s_windowBorderSpace);
				//Builder window top part
				GUILayout.BeginHorizontal();
					GUILayout.Space(s_windowBorderSpace);
					TitleGUI();
					GUILayout.Space(s_windowBorderSpace);
				GUILayout.EndHorizontal();

				GUILayout.Space(s_verticalCategorySpace);

				GUILayout.BeginHorizontal();
					GUILayout.Space(s_windowBorderSpace);
					GUILayout.BeginVertical(GUILayout.MaxWidth(position.width * 0.5f));
						DebugGUI("Visual Elements", DebugKeys.visualDebugsShowState, "VisualHelper_");
					GUILayout.EndVertical();

					GUILayout.Space(s_windowBorderSpace);
					GUILayout.BeginVertical(GUILayout.MaxWidth(position.width * 0.5f));
						DebugGUI("Log Elements", DebugKeys.logDebugsShowState, "LogHelper_");
					GUILayout.EndVertical();
					GUILayout.Space(s_windowBorderSpace);
				GUILayout.EndHorizontal();

			GUILayout.EndVertical();
		}
		
		protected virtual void TitleGUI()
		{
			GUILayout.BeginVertical();
			GUILayout.Label("Debugger", _titleStyle);
			GUILayout.EndVertical();
		}
		protected virtual void DebugGUI(string title, Dictionary<string, bool> element, string textToEarease)
		{
			GUILayout.BeginVertical();
			GUILayout.Label(title, _titleStyle);

			if (element == null)
			{
				GUILayout.Label
					(
						"The " + textToEarease.Replace("Helper_", "") + " dictionary is empty, please override the 'ADebugViewerData' class in this project with the corresponding values to Use this panel !",
						_debugLabelStyle
					);
			}
			else
			{
				List<KeyValuePair<string, bool>> changes  = new List<KeyValuePair<string, bool>>();
				foreach (KeyValuePair<string, bool> pair in element)
				{
					GUILayout.BeginHorizontal();

					string labelText = pair.Key.Replace(textToEarease,"");

					GUILayout.Label(labelText, _debugLabelStyle);
					bool bNewEnable = EditorGUILayout.Toggle(pair.Value, GUILayout.Width(14f));

					if (bNewEnable != pair.Value)
					{
						changes.Add(new KeyValuePair<string, bool>(pair.Key, bNewEnable));
					}
					GUILayout.EndHorizontal();
					GUILayout.Space(s_windowBorderSpace);
				}
				for (int i = 0; i < changes.Count; ++i)
				{
					EditorPrefs.SetBool(changes[i].Key, changes[i].Value);
					element[changes[i].Key] = changes[i].Value;
				}
			}
			GUILayout.EndVertical();
		}
		protected virtual void SetupStyle()
		{
			if (_titleStyle == null)
			{
				_titleStyle = new GUIStyle();
				_titleStyle.font = EditorStyles.whiteLargeLabel.font;
				_titleStyle.fontSize = EditorStyles.whiteLargeLabel.fontSize;
				_titleStyle.fontStyle = EditorStyles.whiteLargeLabel.fontStyle;
				_titleStyle.fixedHeight = 20;
				_titleStyle.stretchWidth = true;
				_titleStyle.alignment = TextAnchor.MiddleCenter;
				_titleStyle.normal.textColor = EditorStyles.whiteLargeLabel.normal.textColor;
				_titleStyle.normal.background = EditorStyles.toolbar.normal.background;
			}
			if (_debugLabelStyle == null)
			{
				_debugLabelStyle = new GUIStyle(EditorStyles.label);
				_debugLabelStyle.fontStyle = FontStyle.Bold;
				_debugLabelStyle.richText = true;
				_debugLabelStyle.wordWrap = true;
				_debugLabelStyle.stretchWidth = true;
				_debugLabelStyle.stretchHeight = false;
				_debugLabelStyle.clipping = TextClipping.Clip;
			}
		}
	}
}