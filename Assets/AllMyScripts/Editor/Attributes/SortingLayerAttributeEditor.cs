namespace AllMyScripts.Common.Attributes.Editor
{
    using UnityEngine;
    using UnityEditor;
    /// <summary>
    /// Custom property drawer for attribute lwSortingLayer
    /// </summary>
    [CustomPropertyDrawer( typeof( SortingLayerAttribute ) )]
	public class SortingLayerAttributeEditor : PropertyDrawer
	{
		public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			EditorGUI.BeginProperty( position, label, property );

			if( property.propertyType == SerializedPropertyType.String )
			{
				int nSelected = -1;
				string[] sLayerNames = new string[SortingLayer.layers.Length];
				for( int nLayerIndex = 0; nLayerIndex < sLayerNames.Length; ++nLayerIndex )
				{
					sLayerNames[nLayerIndex] = SortingLayer.layers[nLayerIndex].name;
					if( property.stringValue == sLayerNames[nLayerIndex] )
					{
						nSelected = nLayerIndex;
					}
				}

				position = EditorGUI.PrefixLabel( position, GUIUtility.GetControlID( FocusType.Passive ), label );
				EditorGUI.BeginChangeCheck();
				nSelected = EditorGUI.Popup( position, nSelected, sLayerNames );
				if( EditorGUI.EndChangeCheck() )
				{
					property.stringValue = sLayerNames[nSelected];
				}
			}
			else
			{
				EditorGUI.LabelField( position, label, new GUIContent( "Sorting layer attribute is only available for strings." ) );
			}

			EditorGUI.EndProperty();
		}
	}
}
