namespace AllMyScripts.Common.Attributes.Editor
{
    using UnityEngine;
    using UnityEditor;

    /// <summary>
    /// Custom property drawer for attribute lwSortingLayer
    /// </summary>
    [CustomPropertyDrawer( typeof( MaskFieldAttribute ) )]
	public class lwMaskFieldAttributeEditor : PropertyDrawer
	{
		public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			EditorGUI.BeginProperty( position, label, property );
			property.intValue = EditorGUI.MaskField( position, label, property.intValue, property.enumNames );
			EditorGUI.EndProperty();
		}
	}
}
