namespace AllMyScripts.Common.Tools.Editor
{
    using UnityEditor;
    using UnityEngine;

    using System.IO;

    /// <summary>
    /// Used to search a component in a gameObject or a prefab
    /// </summary>
    public class TextureTools : EditorWindow
    {
        [MenuItem("AllMyScripts/Tools/Texture Tools")]
        static void Init()
        {
            TextureTools window = (TextureTools)EditorWindow.GetWindow(typeof(TextureTools));
            window.Show();
        }


        private void OnGUI()
        {
            EditorGUILayout.LabelField(Selection.objects.Length + " objects selected");
            if (Selection.objects.Length == 0)
            {
                return;
            }
            foreach (UnityEngine.Object tex in Selection.objects)
            {
                AssetImporter imp = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(tex));
                if (!imp.GetType().IsAssignableFrom(typeof(TextureImporter)))
                {
                    EditorGUILayout.LabelField(tex.name + " isn't a texture");
                    return;
                }
            }
            if (GUILayout.Button("Make White"))
            {
                foreach (object obj in Selection.objects)
                {
                    Texture2D texture = new Texture2D(4, 4);
                    string path = Application.dataPath.Replace("Assets", "") + "/" + AssetDatabase.GetAssetPath((Texture2D)obj);
                    texture.LoadImage(File.ReadAllBytes(path));
                    Color[] pixels = texture.GetPixels();
                    for (int i = 0; i < pixels.Length; ++i)
                    {
                        pixels[i] = new Color(1, 1, 1, pixels[i].a);
                    }
                    texture.SetPixels(pixels);
                    texture.Apply();
                    File.WriteAllBytes(path, texture.EncodeToPNG());
                }
            }
            if (GUILayout.Button("Vertical Center"))
            {
                VerticalCenter();
            }
        }
        private void VerticalCenter()
        {
            foreach (object obj in Selection.objects)
            {
                Texture2D tex = new Texture2D(4, 4);
                string path = Application.dataPath.Replace("Assets", "") + "/" + AssetDatabase.GetAssetPath((Texture2D)obj);
                tex.LoadImage(File.ReadAllBytes(path));
                Color[][] pixels = new Color[tex.height][];
                for (int y = 0; y < tex.height; ++y)
                {
                    pixels[y] = tex.GetPixels(0, y, tex.width, 1);
                }

                int yStart = -1;
                int yEnd = -1;
                for (int y = 0; y < tex.height; ++y)
                {
                    bool rowIsTransparent = true;

                    for (int x = 0; x < tex.width; ++x)
                    {
                        if (yStart == -1)
                        {
                            if (!Mathf.Approximately(pixels[y][x].a, 0))
                            {
                                yStart = y;
                                rowIsTransparent = false;
                                break;
                            }
                        }
                        else
                        {
                            if (yEnd == -1)
                            {
                                if (!Mathf.Approximately(pixels[y][x].a, 0))
                                {
                                    rowIsTransparent = false;
                                    break;
                                }
                            }
                            else
                            {
                                if (!Mathf.Approximately(pixels[y][x].a, 0))
                                {
                                    rowIsTransparent = false;
                                    yEnd = -1;
                                    break;
                                }
                            }
                        }
                    }
                    if (rowIsTransparent && yEnd == -1)
                    {
                        yEnd = y + 1;
                    }
                }

                if (yEnd == -1)
                {
                    //the end is the edge
                    yEnd = tex.height;
                }

                Color[][] result = new Color[tex.height][];
                for (int i = 0; i < result.Length; ++i)
                {
                    result[i] = new Color[tex.width];
                }

                int startPoint = (int)(tex.height * 0.5f - (yEnd - yStart) * 0.5f);
                int currentY = 0;
                for (int y = 0; y < tex.height; ++y)
                {
                    for (int x = 0; x < tex.width; ++x)
                    {
                        if (y > startPoint && y < startPoint + (yEnd - yStart))
                        {
                            result[y][x] = pixels[yStart + currentY][x];
                        }
                        else
                        {
                            result[y][x] = new Color(1, 1, 1, 0);
                        }
                    }
                    if (y > startPoint && y < startPoint + (yEnd - yStart))
                    {
                        currentY++;
                    }
                }


                for (int y = 0; y < tex.height; ++y)
                {
                    tex.SetPixels(0, y, tex.width, 1, result[y]);
                }
                tex.Apply();
                File.WriteAllBytes(path, tex.EncodeToPNG());
            }
        }
    }
}