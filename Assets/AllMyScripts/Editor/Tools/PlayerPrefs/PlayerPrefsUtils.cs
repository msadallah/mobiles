﻿namespace AllMyScripts.Common.Tools.Editor
{
    using UnityEngine;
    using UnityEditor;

    public class PlayerPrefsUtils : EditorWindow
	{
		private string _keyToCreate = string.Empty;
		private string _stringValueToCreate = string.Empty;

		private string _keyToGet = string.Empty;
		private string _keyToDelete = string.Empty;

		[MenuItem("AllMyScripts/Tools/PlayerPrefs")]
		static void Init()
		{
			PlayerPrefsUtils window = (PlayerPrefsUtils)EditorWindow.GetWindow(typeof(PlayerPrefsUtils), false, "PlayerPrefsUtils");
			window.Show();
		}

		void OnGUI()
		{
			GUILayout.Label("Create Player Pref", EditorStyles.boldLabel);
			_keyToCreate = EditorGUILayout.TextField("Key", _keyToCreate);
			_stringValueToCreate = EditorGUILayout.TextField("String value", _stringValueToCreate);

			if (GUILayout.Button("Create Player Pref Key"))
				CreatePlayerPrefKey(_keyToCreate, _stringValueToCreate);

			GUILayout.Space(15f);

			GUILayout.Label("Get Player Pref", EditorStyles.boldLabel);
			_keyToGet = EditorGUILayout.TextField("Key", _keyToGet);

			if (GUILayout.Button("Get Player Pref Key"))
				GetPlayerPrefKey(_keyToGet);

			GUILayout.Space(15f);

			GUILayout.Label("Delete Player Pref", EditorStyles.boldLabel);
			_keyToDelete = EditorGUILayout.TextField("Key", _keyToDelete);

			if(GUILayout.Button("Delete Player Pref Key"))
				DeletePlayerPrefKey(_keyToDelete);

			GUILayout.Space(15f);

			if(GUILayout.Button("Delete All Players Prefs"))
			{
				PlayerPrefs.DeleteAll();
				Debug.LogWarning("[PLAYERPREFSUTILS] All keys has been deleted from player prefs");
			}
		}

		private void CreatePlayerPrefKey(string key, string value)
		{
			PlayerPrefs.SetString(key, value);

			if (PlayerPrefs.HasKey(key))
				Debug.LogWarning($"[PLAYERPREFSUTILS] Key : {key} : was replaced by new value : {value}");
			else
				Debug.LogWarning($"[PLAYERPREFSUTILS] Key : {key} : was created with value : {value}");
		}

		private void GetPlayerPrefKey(string key)
		{
			if (PlayerPrefs.HasKey(key))
				Debug.LogWarning($"[PLAYERPREFSUTILS] Key : {key} contains value : {PlayerPrefs.GetString(key)}");
			else
				Debug.LogWarning($"[PLAYERPREFSUTILS] Key : {key} not found");
		}

		private void DeletePlayerPrefKey(string key)
		{
			if(PlayerPrefs.HasKey(key))
			{
				PlayerPrefs.DeleteKey(key);
				Debug.LogWarning("[PLAYERPREFSUTILS] Key : " + key + " : has been deleted from player prefs");
			}
			else
			{
				Debug.LogWarning("[PLAYERPREFSUTILS] Key : " + key + " : not found in player prefs");
			}
		}
	}
}