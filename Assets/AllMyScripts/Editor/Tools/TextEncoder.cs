namespace AllMyScripts.Common.Tools.Editor
{
    using UnityEngine;
    using UnityEditor;
    using System.IO;
    using AllMyScripts.Common.Tools;

    public class TextEncoder : EditorWindow
	{
		private static TextEncoder m_Window;

		private RC4 m_RC4;
		private string m_sPassword;

		private string m_sInputFilePath = string.Empty;
		private string m_sOutputFilePath = string.Empty;

		private string m_sInfo = string.Empty;
		private GUIStyle m_StyleInfo;

		private int m_nTime = 0;

		[MenuItem( "AllMyScripts/Tools/Text Encoder" )]
		private static void Init()
		{
			Reload();

			m_Window.minSize = new Vector2( 700f, 140f );
			m_Window.m_sPassword = m_Window.m_RC4.GetKey();

			if( File.Exists( Application.dataPath + "/Resources/Texts/Langs.txt" ) )
			{
				m_Window.m_sInputFilePath = Application.dataPath + "/Resources/Texts/Langs.txt";
				m_Window.m_sOutputFilePath = Application.dataPath + "/Resources/Texts/Langs.enc";
			}
		}

		private static void Reload()
		{
			m_Window = EditorWindow.GetWindow<TextEncoder>( typeof( TextEncoder ) );

			m_Window.m_RC4 = new RC4();

			m_Window.m_sInfo = string.Empty;

			m_Window.m_StyleInfo = new GUIStyle();
			m_Window.m_StyleInfo.alignment = TextAnchor.MiddleRight;
			m_Window.m_StyleInfo.normal.textColor = Color.white;
			m_Window.m_StyleInfo.fontSize = 16;
		}

		private void OnInspectorUpdate()
		{
			if( m_Window == null )
				Reload();

			m_nTime++;
			if( m_nTime >= 24 )
			{
				m_sInfo = string.Empty;
				Repaint();
			}
		}

		private void OnGUI()
		{
			EditorGUILayout.LabelField( "Password: " );
			m_sPassword = EditorGUILayout.TextField( m_sPassword );

			// FILES
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.BeginVertical();
			EditorGUILayout.LabelField( "Input file: " );
			EditorGUILayout.BeginHorizontal();
			m_sInputFilePath = EditorGUILayout.TextField( m_sInputFilePath );
			if( GUILayout.Button( "...", GUILayout.Width( 26f ) ) )
			{
				string sPath = EditorUtility.OpenFilePanel( "Select an input file", m_sInputFilePath, "" );
				if( !string.IsNullOrEmpty( sPath ) )
					m_sInputFilePath = sPath;
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.LabelField( "Output file: " );
			EditorGUILayout.BeginHorizontal();
			m_sOutputFilePath = EditorGUILayout.TextField( m_sOutputFilePath );
			if( GUILayout.Button( "...", GUILayout.Width( 26f ) ) )
			{
				string sPath = EditorUtility.SaveFilePanel( "Select an output file", m_sOutputFilePath, GetFullFileName( m_sOutputFilePath ), "" );
				if( !string.IsNullOrEmpty( sPath ) )
					m_sOutputFilePath = sPath;
			}
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.EndVertical();


			EditorGUILayout.BeginVertical( GUILayout.Width( 10f ) );
			GUILayout.Space( 1f );
			EditorGUILayout.EndVertical();


			EditorGUILayout.BeginVertical( GUILayout.Width( 100f ) );
			GUILayout.Space( 19f );
			if( GUILayout.Button( "Encrypt", GUILayout.Width( 100f ), GUILayout.Height( 60f ) ) )
				Encrypt();
			EditorGUILayout.EndVertical();
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.LabelField( m_sInfo, m_StyleInfo );
		}

		private void Encrypt()
		{
			if( m_sPassword.Length < 8 )
			{
				UpdateInfo( "Password must be at least 8 characters long." );
				return;
			}

			byte[] bytesToEncode = ReadFile( m_sInputFilePath );
			if( bytesToEncode == null )
			{
				UpdateInfo( "Wrong Input file." );
				return;
			}

			m_RC4.SetKey( m_sPassword );

			CreateFile( m_sOutputFilePath, m_RC4.Encrypt( bytesToEncode ) );

			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();

			UpdateInfo( GetFullFileName( m_sOutputFilePath ) + " wrote." );
		}

		private void CreateFile( string filePath, byte[] text )
		{
			using( BinaryWriter binaryWriter = new BinaryWriter( File.Open( @filePath, FileMode.Create ) ) )
			{
				binaryWriter.Write( text );
			}
		}

		private byte[] ReadFile( string filePath )
		{
			if( !File.Exists( filePath ) )
				return null;

			byte[] fileContent = null;

			using( BinaryReader binaryReader = new BinaryReader( File.Open( @filePath, FileMode.Open, FileAccess.Read )))
			{
				fileContent = binaryReader.ReadBytes( (System.Int32)new FileInfo( filePath ).Length );
			}

			return fileContent;
		}

		private void UpdateInfo( string sInfo )
		{
			m_sInfo = sInfo;

			m_nTime = 0;
		}

		private string GetFullFileName( string sPath )
		{
			if( string.IsNullOrEmpty( sPath ) )
				return string.Empty;

			sPath = sPath.Replace( "\\", "/" );

			return sPath.Substring( sPath.LastIndexOf( "/" ) + 1 );
		}
	}
}
