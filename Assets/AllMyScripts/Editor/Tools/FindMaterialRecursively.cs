namespace AllMyScripts.Common.Utils.Editor
{
    using UnityEngine;
    using UnityEditor;

    using System.Collections;
    using System.IO;

    public class FindMaterialRecursively : EditorWindow
	{	
		private static int go_count;
		private static int material_count;
		private static string sMaterialName = string.Empty;
		private static bool bFindShaderToo;
 
		[MenuItem("AllMyScripts/Tools/Find/MaterialRecusively %&g")]
		public static void ShowWindow()
		{
			EditorWindow.GetWindow(typeof(FindMaterialRecursively));
		}
 
		public void OnGUI()
		{
			sMaterialName = EditorGUILayout.TextField( "Material Name : ", sMaterialName );

			bFindShaderToo = EditorGUILayout.Toggle( "Find Shader too : ", bFindShaderToo );
		
			if (GUILayout.Button("Find Material With This Name"))
			{
				FindInSelected();
			}
		
			if (GUILayout.Button("Find Material in all prefabs"))
			{
				FindInPrefabs();
			}
		}
		private static void FindInSelected()
		{
			GameObject[] go = Selection.gameObjects;
			go_count = 0;
			material_count = 0;
			foreach (GameObject g in go)
			{
   				FindInGO(g);
			}
			Debug.Log(string.Format("Searched {0} GameObjects, {1} material instances", go_count, material_count ));
		}
	
		void FindInPrefabs()
		{
			string[] files;
			GameObject obj;
			
			go_count = 0;
			material_count = 0;

			// Stack of folders:
			Stack stack = new Stack();

			// Add root directory:
			stack.Push(Application.dataPath);

			// Continue while there are folders to process
			while (stack.Count > 0)
			{
				// Get top folder:
				string dir = (string)stack.Pop();

				try
				{
					// Get a list of all prefabs in this folder:
					files = Directory.GetFiles(dir, "*.prefab");

					// Process all prefabs:
					for (int i = 0; i < files.Length; ++i)
					{
						// Make the file path relative to the assets folder:
						files[i] = files[i].Substring(Application.dataPath.Length - 6);

						obj = (GameObject)AssetDatabase.LoadAssetAtPath(files[i], typeof(GameObject));

						if (obj != null)
						{
							FindInGO(obj);
						}
					}

					// Add all subfolders in this folder:
					foreach (string dn in Directory.GetDirectories(dir))
					{
						stack.Push(dn);
					}
				}
				catch
				{
					// Error
					Debug.LogError("Could not access folder: \"" + dir + "\"");
				}
			}
			Debug.Log(string.Format("Searched {0} GameObjects, {1} material instances", go_count, material_count ));
		}
 
		private static void FindInGO(GameObject g)
		{
			go_count++;
			Renderer rd = g.GetComponent<Renderer>();
			if( rd != null )
			{
				Material[] mats = rd.sharedMaterials;
				for (int i = 0; i < mats.Length; i++)
				{
					Material mat = mats[i];
					if( mat != null )
					{
						if( mat.name.Contains( sMaterialName ) || (bFindShaderToo && mat.shader.name.Contains( sMaterialName )) )
						{
			        		material_count++;
							Debug.Log( mat.name + " has been found in " + g.name + " root " + g.transform.root.name + " (shader : " + mat.shader.name + ")" );
						}
					}
				}
			}
	#if UNITY_4_6 || UNITY_5
			Image img = g.GetComponent<Image>();
			if( img!=null )
			{
				Material mat = img.material;
				if( mat != null )
				{
					if( mat.name.Contains( sMaterialName ) || (bFindShaderToo && mat.shader.name.Contains( sMaterialName )) )
					{
						material_count++;
						Debug.Log( mat.name + " has been found in " + g.name + " root " + g.transform.root.name + " (shader : " + mat.shader.name + ")" );
					}
				}
			}
			RawImage rimg = g.GetComponent<RawImage>();
			if( rimg!=null )
			{
				Material mat = rimg.material;
				if( mat != null )
				{
					if( mat.name.Contains( sMaterialName ) || (bFindShaderToo && mat.shader.name.Contains( sMaterialName )) )
					{
						material_count++;
						Debug.Log( mat.name + " has been found in " + g.name + " root " + g.transform.root.name + " (shader : " + mat.shader.name + ")" );
					}
				}
			}
	#endif

			// Now recurse through each child GO (if there are any):
			foreach (Transform childT in g.transform)
			{
				FindInGO(childT.gameObject);
			}
		}
	}
}
