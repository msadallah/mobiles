using UnityEditor;
using UnityEngine;

using System.Collections.Generic;

public class ReferencesTracker : EditorWindow
{
	private Object target;
	private List<Object> references = new List<Object>();

	[MenuItem("AllMyScripts/Tools/References Tracker")]
	public static void Init()
	{
		GetWindow<ReferencesTracker>("References Tracker");
	}

	public void OnGUI()
	{
		target = EditorGUILayout.ObjectField("Referenced Object : ", target, typeof (Object), true) as Object;

		if (target == null) { return; }

		if (GUILayout.Button("Find Objects Referencing it"))
		{
			references.Clear();
			FindObjectsReferencing(target);
		}

		if ((references != null) && (references.Count > 0))
		{
			GUILayout.Label("References", EditorStyles.boldLabel);

			EditorGUI.BeginDisabledGroup(true);
			foreach (Object reference in references)
			{
				EditorGUILayout.ObjectField(reference, typeof (Object), true);
			}
			EditorGUI.EndDisabledGroup();
		}
	}

	private void FindObjectsReferencing(Object target)
	{
		bool isAsset = !string.IsNullOrEmpty(AssetDatabase.GetAssetPath(target));

		int[] targetInstanceIds = null;

		if (target is GameObject)
		{
			Component[] components = ((GameObject)target).GetComponents<Component>();
			int count = components.Length;

			targetInstanceIds = new int[count + 1];
			for (int i = count - 1; i >= 0; --i)
			{
				targetInstanceIds[i] = components[i].GetInstanceID();
			}
			targetInstanceIds[count] = target.GetInstanceID();
		}
		else
		{
			targetInstanceIds = new int[] {target.GetInstanceID()};
		}

		FindObjectsReferencing(targetInstanceIds, Resources.FindObjectsOfTypeAll<Component>(), isAsset);
		if (isAsset)
		{
			FindObjectsReferencing(targetInstanceIds, Resources.FindObjectsOfTypeAll<ScriptableObject>(), true);
		}
	}

	private void FindObjectsReferencing<T>(int[] targetInstanceIds, T[] candidates, bool isAsset) where T : Object
	{
		foreach (T candidate in candidates)
		{
			if (candidate is Transform) { continue; }
			if ((!isAsset) && (!string.IsNullOrEmpty(AssetDatabase.GetAssetPath(candidate)))) { continue; }

			bool found = false;
			SerializedObject so;
			try
			{
				so = new SerializedObject(candidate);
			}
			catch (System.Exception e)
			{
				Debug.LogErrorFormat(this, "[{0}] Cannot create SerializedObject for '{1}'", GetType().Name, candidate);
				Debug.LogException(e, candidate);
				continue;
			}
			SerializedProperty prop = so.GetIterator();
			while (prop.Next(true))
			{
				if( (prop.propertyType == SerializedPropertyType.ObjectReference) &&
					targetInstanceIds.Contains(prop.objectReferenceInstanceIDValue) && 
					(!((candidate is Component) && (prop.propertyPath == "m_GameObject"))))
				{
					found = true;
					Debug.Log("[ReferencesTracker] found reference in property '" + prop.propertyPath + "' of '" + candidate.name + " (" + candidate.GetType().Name + ")'", candidate);
				}
			}

			if (found)
			{
				references.Add(candidate);
			}
		}
	}
}

public static class ArrayExtension
{
	public static bool Contains( this int[] array, int reference )
	{
		for (int i = array.Length - 1; i >= 0; --i)
		{
			if (array[i] == reference)
			{
				return true;
			}
		}

		return false;
	}
}