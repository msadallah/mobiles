namespace AllMyScripts.Common.Utils.Editor
{
    using UnityEngine;
    using System.IO;
    using UnityEditor;

    public class AssetImporter : AssetPostprocessor
	{
		private bool MetaExist
		{
			get
			{
				return File.Exists( AssetDatabase.GetTextMetaFilePathFromAssetPath( assetPath ) ); 
			}
		}

		private void OnPreprocessAudio()
		{
			string sPathName = assetPath.ToUpper();
			AudioImporter audioImporter = (AudioImporter)assetImporter;

			if( sPathName.Contains( "SFX_" ) ||
				sPathName.Contains( "_SFX" ) )
			{
				if( !MetaExist )
				{
					audioImporter.forceToMono = true;

				}
			}
			else if( sPathName.Contains( "AMB_" ) || sPathName.Contains( "_AMB" ) )
			{
				if( !MetaExist )
				{
					audioImporter.forceToMono = true;
				}
			}
		}

		private void OnPreprocessTexture()
		{
			string sPathName = assetPath.ToUpper();
			TextureImporter textureImporter = (TextureImporter)assetImporter;

			if( sPathName.Contains( "_BUMP" ) || sPathName.Contains( "BUMP_" ) || sPathName.Contains( "_NORMAL" ) || sPathName.Contains( "NORMAL_" ) )
			{
				if( !MetaExist )
				{
					textureImporter.convertToNormalmap = true;
					textureImporter.normalmapFilter = TextureImporterNormalFilter.Standard;
					textureImporter.filterMode = FilterMode.Bilinear;
				}
			}
			else if( sPathName.Contains( "GUI_" ) || sPathName.Contains( "_GUI" ) || sPathName.Contains( "GUI/" ) || sPathName.Contains( "SPRITE ATLASES/" ) 
					|| sPathName.Contains( "FONTS/" ) || sPathName.Contains( "APP_" ) || sPathName.Contains( "_APP" ) )
			{
				if( !MetaExist )
				{
					textureImporter.textureType = TextureImporterType.GUI;
					textureImporter.maxTextureSize = 2048;
					textureImporter.textureCompression	= TextureImporterCompression.Uncompressed;
					textureImporter.filterMode = FilterMode.Bilinear;
				}
			}
			else if( sPathName.Contains( "_CURSOR" ) || sPathName.Contains( "CURSOR_" ) )
			{
				if( !MetaExist )
					textureImporter.textureType = TextureImporterType.Cursor;
			}
		}

#if ENABLED_ASSETPREPROCESS_MODEL
		void OnPreprocessModel()
		{
			ModelImporter modelImporter = ( ModelImporter ) assetImporter;
			string sPath = AssetDatabase.GetAssetPath( modelImporter );
			string sFileName = Path.GetFileNameWithoutExtension( sPath );
		
			modelImporter.normalImportMode 			= ModelImporterTangentSpaceMode.Calculate;
			modelImporter.isReadable 				= true;
			modelImporter.meshCompression 			= ModelImporterMeshCompression.Off;
			modelImporter.optimizeMesh				= true;
		}
#endif
	}
}
