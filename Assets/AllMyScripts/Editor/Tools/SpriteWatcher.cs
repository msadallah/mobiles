namespace AllMyScripts.Common.UI.Editor
{
    using UnityEngine;
    using UnityEditor;

    using System.Collections.Generic;

    //! @class SpriteWatcher
    //!
    //!	@brief	Editor window that build a report of sprites used at a certain instant of the game
    public class SpriteWatcher : EditorWindow
	{
		[MenuItem( "AllMyScripts/Tools/Find/Sprites watcher" )]
		private static void CreateWindow()
		{
			SpriteWatcher window = EditorWindow.GetWindow<SpriteWatcher>();
			window.ShowUtility();
		}

	#region Unity callbacks
		private void OnEnable()
		{
			m_spriteInfoList = new List<SpriteInfo>();
			m_v2ScrollPosition = Vector2.zero;
			m_selectedNode = null;

			m_spriteInfoComparer = new SpriteInfoComparer();
			m_spriteInfoComparer.m_nSortingColumnIndex = -1;
			m_spriteInfoComparer.m_bIsAscending = true;

			m_sFilterText = "";
			EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
		}

		private void OnDisable()
		{
			EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
			m_selectedNode = null;
			m_spriteInfoList = null;
		}

		private void OnGUI()
		{
			if(Application.isPlaying)
			{
				OnReportGUI();
			
				if(GUILayout.Button("Pause and build report"))
				{
					EditorApplication.isPaused = true;
					BuildReportRoutine();
				}
			}
			else
			{
				EditorGUILayout.HelpBox("The application should be playing in order to use this tool.", MessageType.Error);
			}
		}
	#endregion

	#region Private
	#region Declaration
		private class SpriteInfo
		{
			internal Sprite m_sprSprite;
		
			internal string m_sSpritePackingTag;
			internal List<GameObject> m_goReferences = new List<GameObject>();
		}

		private class SpriteInfoComparer : IComparer<SpriteInfo>
		{
			public int m_nSortingColumnIndex;
			public bool m_bIsAscending;

			int IComparer<SpriteInfo>.Compare( SpriteInfo lhs, SpriteInfo rhs )
			{
				if( m_nSortingColumnIndex==-1 )
				{
					return -1;
				}
				else
				{
					int nComparison = 0;
					switch( m_nSortingColumnIndex )
					{
						case ( int )Column.AssetName: nComparison = string.CompareOrdinal( lhs.m_sprSprite.name, rhs.m_sprSprite.name ); break;
						case ( int )Column.AtlasName: nComparison = string.CompareOrdinal( lhs.m_sSpritePackingTag, rhs.m_sSpritePackingTag ); break;
						case ( int )Column.References: nComparison = Comparer<int>.Default.Compare( lhs.m_goReferences.Count, rhs.m_goReferences.Count ); break;
					}

					return m_bIsAscending? nComparison : -nComparison;
				}
			}
		}

		private enum Column
		{
			AssetName,
			AtlasName,
			References,
		}
		private static readonly string[] COLUMN_NAMES = new string[]
		{
			"Sprite name", "Name of the sprite",
			"Atlas name", "Name of the atlas",
			"References", "Number of objects that uses this asset"
		};
		private static readonly int[] COLUMN_WIDTHS = new int[]
		{
			-1, 100,
			200, 100,
			100, 100,
		};
		private static GUILayoutOption[] ColumnWidth( int nColumnIndex )
		{
			return new GUILayoutOption[]
			{
				( COLUMN_WIDTHS[nColumnIndex*2]<0? GUILayout.ExpandWidth( true ) : GUILayout.Width( COLUMN_WIDTHS[nColumnIndex*2] ) ),
				GUILayout.MinWidth( COLUMN_WIDTHS[nColumnIndex*2+1] )
			};
		}
		#endregion

		#region Methods
		private void OnPlayModeStateChanged( PlayModeStateChange state )
		{
			// clear report if we stop playing or stops the pause
			if (state == PlayModeStateChange.ExitingEditMode || EditorApplication.isPaused == false)
			{
				m_selectedNode = null;
				m_spriteInfoList.Clear();
				m_v2ScrollPosition = Vector2.zero;
			}
		}

		private void OnReportGUI()
		{
			EditorGUILayout.Space();
			EditorGUILayout.BeginHorizontal();
			{
				m_sFilterText = EditorGUILayout.TextField( m_sFilterText, ( GUIStyle )"SearchTextField" );
				UnityEngine.GUI.SetNextControlName( "SpriteWatcher_SearchClearButton" );
				if( GUILayout.Button( GUIContent.none, ( GUIStyle )"SearchCancelButton" ) )
				{
					m_sFilterText = "";
					UnityEngine.GUI.FocusControl( "SpriteWatcher_SearchClearButton" );
				}
			}
			EditorGUILayout.EndHorizontal();

			int nColumnCount = System.Enum.GetValues( typeof( Column ) ).Length;

			// headers
			EditorGUILayout.BeginHorizontal( EditorStyles.toolbar );
			for( int nColumnIndex = 0; nColumnIndex<nColumnCount; ++nColumnIndex )
			{
				GUIContent headerContent = new GUIContent( COLUMN_NAMES[nColumnIndex*2], COLUMN_NAMES[nColumnIndex*2+1] );
				if( GUILayout.Button( headerContent, EditorStyles.toolbarButton, ColumnWidth( nColumnIndex ) ) )
				{
					if( m_spriteInfoComparer.m_nSortingColumnIndex==nColumnIndex )
					{
						m_spriteInfoComparer.m_bIsAscending = m_spriteInfoComparer.m_bIsAscending==false;
					}
					else
					{
						m_spriteInfoComparer.m_nSortingColumnIndex = nColumnIndex;
						m_spriteInfoComparer.m_bIsAscending = true;
					}

					m_spriteInfoList.Sort( m_spriteInfoComparer );
				}
			}
			float fMinWidth, fMaxWidth;
			UnityEngine.GUI.skin.verticalScrollbar.CalcMinMaxWidth( GUIContent.none, out fMinWidth, out fMaxWidth );
			GUILayout.Space( fMaxWidth );
			EditorGUILayout.EndHorizontal();

			m_v2ScrollPosition = EditorGUILayout.BeginScrollView(m_v2ScrollPosition, false, false, GUILayout.ExpandWidth(true));
			{
				for( int nIndex = 0; nIndex<m_spriteInfoList.Count; ++nIndex )
				{
					if( string.IsNullOrEmpty( m_sFilterText ) || m_spriteInfoList[nIndex].m_sprSprite.name.Contains( m_sFilterText ) )
					{
						EditorGUILayout.BeginVertical( UnityEngine.GUI.skin.box );
						EditorGUILayout.BeginHorizontal( GUILayout.ExpandWidth( true ) );

						GUIContent spriteNameContent = new GUIContent( m_spriteInfoList[nIndex].m_sprSprite.name, COLUMN_NAMES[( int )Column.AssetName*2+1] );
						EditorGUILayout.LabelField( spriteNameContent, ColumnWidth( ( int )Column.AssetName ) );
						GUIContent atlasNameContent = new GUIContent( m_spriteInfoList[nIndex].m_sSpritePackingTag, COLUMN_NAMES[( int )Column.AtlasName*2+1] );
						EditorGUILayout.LabelField( atlasNameContent, ColumnWidth( ( int )Column.AtlasName ) );
						GUIContent referencesContent = new GUIContent( m_spriteInfoList[nIndex].m_goReferences.Count.ToString(), COLUMN_NAMES[( int )Column.References*2+1] );
						EditorGUILayout.LabelField( referencesContent, ColumnWidth( ( int )Column.References ) );

						EditorGUILayout.EndHorizontal();

						Rect spriteInfoLineRect = GUILayoutUtility.GetLastRect();
						if( m_spriteInfoList[nIndex]==m_selectedNode )
						{
							OnSelectedNodeGUI();
						}

						if( Event.current.isMouse )
						{
							if( spriteInfoLineRect.Contains( Event.current.mousePosition ) && Event.current.button==0 && Event.current.type==EventType.MouseUp )
							{
								if( m_selectedNode==m_spriteInfoList[nIndex] )
								{
									m_selectedNode = null;
								}
								else
								{
									m_selectedNode = m_spriteInfoList[nIndex];
								}

								Repaint();
							}
						}
						EditorGUILayout.EndVertical();
					}
				}
			}
			EditorGUILayout.EndScrollView();
		}

		private void OnSelectedNodeGUI()
		{
			SpriteInfo nextSelectedSpriteInfo = null;
		
			++EditorGUI.indentLevel;
			for( int nReferenceIndex = 0; nReferenceIndex<m_selectedNode.m_goReferences.Count; ++nReferenceIndex )
			{
				EditorGUILayout.BeginHorizontal();
				if( GUILayout.Button( "Select", GUILayout.ExpandWidth( false ) ) )
				{
					EditorGUIUtility.PingObject( m_selectedNode.m_goReferences[nReferenceIndex] );
					Selection.activeObject = m_selectedNode.m_goReferences[nReferenceIndex];
				}
				EditorGUILayout.LabelField( "- "+m_selectedNode.m_goReferences[nReferenceIndex].name );
				EditorGUILayout.EndHorizontal();
			}
			--EditorGUI.indentLevel;

			if( nextSelectedSpriteInfo!=null )
			{
				m_selectedNode = nextSelectedSpriteInfo;
			}
		}

		private void BuildReportRoutine()
		{
			m_spriteInfoList.Clear();

			try
			{
				EditorUtility.DisplayProgressBar( "Build report on sprites use", "Gathering all objects", 0.0f );

				Dictionary<Sprite, SpriteInfo> sprSpriteDictionary = new Dictionary<Sprite, SpriteInfo>();
				GameObject[] goRootGameObjects = new List<GameObject>( GetSceneRoots() ).ToArray();
				Queue<GameObject> goQueue = new Queue<GameObject>( goRootGameObjects );
				while( goQueue.Count>0 )
				{
					GameObject goCurrent = goQueue.Dequeue();
					EditorUtility.DisplayProgressBar( "Build report on sprites use", "Gathering all objects : " + goCurrent.name, 0.0f );
					GatherInformation( goCurrent, ref sprSpriteDictionary );

					foreach( Transform trChild in goCurrent.transform )
					{
						goQueue.Enqueue( trChild.gameObject );
					}
				}

				for( int nReportIndex = 0; nReportIndex<m_spriteInfoList.Count; ++nReportIndex )
				{
					EditorUtility.DisplayProgressBar( "Build report on sprites use", "Cleaning up...", nReportIndex/( float )m_spriteInfoList.Count );

					SpriteInfo spriteInfo = m_spriteInfoList[nReportIndex];

					int nReferenceIndex = 0;
					while( nReferenceIndex<spriteInfo.m_goReferences.Count )
					{
						int nOtherIndex = nReferenceIndex+1;
						while( nOtherIndex<spriteInfo.m_goReferences.Count && spriteInfo.m_goReferences[nOtherIndex].transform.parent!=spriteInfo.m_goReferences[nReferenceIndex].transform )
						{
							++nOtherIndex;
						}

						if( nOtherIndex<spriteInfo.m_goReferences.Count )
						{
							spriteInfo.m_goReferences.RemoveAt( nReferenceIndex );
						}
						else
						{
							++nReferenceIndex;
						}
					}
				}

				EditorUtility.ClearProgressBar();
			}
			catch( System.Exception exception )
			{
				EditorUtility.ClearProgressBar();
				throw exception;
			}
		}

		private static IEnumerable<GameObject> GetSceneRoots()
		{
			HierarchyProperty hierarchyProperty = new HierarchyProperty( HierarchyType.GameObjects );
			int[] nDummyArray = new int[0];
			while( hierarchyProperty.Next( nDummyArray ) )
			{
				yield return hierarchyProperty.pptrValue as GameObject;
			}
		}

		private void GatherInformation( GameObject goGo, ref Dictionary<Sprite, SpriteInfo> sprSpriteDictionary )
		{
			//Component[] allComponents = go.GetComponents<Component>();
			//for(int componentIndex = 0; componentIndex < allComponents.Length; ++componentIndex)
			{
				//Component current = allComponents[componentIndex];

				List<Sprite> sprSprites = new List<Sprite>();

				Object[] dependencies = EditorUtility.CollectDependencies( new Object[]{ goGo/*current*/ } );
				for( int nDependencyIndex = 0; nDependencyIndex<dependencies.Length; ++nDependencyIndex )
				{
					Sprite sprSprite = dependencies[nDependencyIndex] as Sprite;
					if( sprSprite!=null )
					{
						sprSprites.Add( sprSprite );
					}
				}

				for( int nSpriteIndex = 0; nSpriteIndex<sprSprites.Count; ++nSpriteIndex )
				{
					Sprite sprSprite = sprSprites[nSpriteIndex];

					if( sprSprite!=null )
					{
						bool bShouldBeAdded = true;

						if( sprSpriteDictionary.ContainsKey( sprSprite )==false )
						{
							string sTexturePath = AssetDatabase.GetAssetPath( sprSprite );
							UnityEditor.AssetImporter assetImpoter = UnityEditor.AssetImporter.GetAtPath( sTexturePath );
							TextureImporter textureImporter = assetImpoter as TextureImporter;

							if( textureImporter==null )
							{
								bShouldBeAdded = false;
							}
							else
							{
								SpriteInfo spriteInfo = new SpriteInfo();
								spriteInfo.m_sprSprite = sprSprite;
								spriteInfo.m_sSpritePackingTag = textureImporter.spritePackingTag;

								sprSpriteDictionary.Add(sprSprite, spriteInfo);
								m_spriteInfoList.Add(spriteInfo);
							}
						}

						if(bShouldBeAdded)
						{
							sprSpriteDictionary[sprSprite].m_goReferences.Add( goGo/*current*/ );
						}
					}
				}
			}
		}
		#endregion

	#region Attributes
		private List<SpriteInfo> m_spriteInfoList;
		private Vector2 m_v2ScrollPosition;
		private SpriteInfoComparer m_spriteInfoComparer;
		private string m_sFilterText;

		private SpriteInfo m_selectedNode;
		#endregion
	#endregion
	}
}