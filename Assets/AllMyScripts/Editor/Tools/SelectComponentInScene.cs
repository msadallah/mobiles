﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;


using System.Reflection;

public class SelectComponentInScene : EditorWindow
{
	private List<SceneAsset> scenes = new List<SceneAsset>();
	//AllMyScripts.Common.UI.CanvasTools
	private string _typeToLookFor = "AllMyScripts.Common.UI.CanvasTools.DPCanvasScaler";
	[MenuItem("Window/Select Component In Scene")]
	static void Init()
	{


		SelectComponentInScene window = (SelectComponentInScene)EditorWindow.GetWindow(typeof(SelectComponentInScene));
		window.minSize = new Vector2(200, 50);
		window.Show();
	}
	void OnEnable()
	{
		/*
		string[] sScenes = EditorPrefs.GetString("SceneLoaderScenes").Split('\n');
		foreach (string s in sScenes)
		{
			SceneAsset asset = AssetDatabase.LoadAssetAtPath<SceneAsset>(s);
			if (asset != null && !scenes.Contains(asset))
			{
				scenes.Add(asset);
			}
		}*/
	}

	private void OnGUI()
	{
		EditorGUILayout.BeginVertical();

		GUILayout.Space(20);

		_typeToLookFor = GUI.TextField(new Rect(10, 10, 250, 20), _typeToLookFor, 200);

		GUILayout.Space(20);

		if (GUILayout.Button("Select first type"))
		{
			SelectObject();
		}
		EditorGUILayout.EndHorizontal();
	}

	private void SelectObject()
	{
		Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
		List<Assembly> myAssemblies = new List<Assembly>();
		System.Type  t = null;

		foreach (Assembly assembly in assemblies)
		{
			t = System.Type.GetType(_typeToLookFor + ", " + assembly.FullName);
			if (t != null)
			{
				break;
			}
		}

		if (t != null)
		{
			Object o = GameObject.FindObjectOfType(t);

			if (o is MonoBehaviour)
			{

				Selection.objects = new Object[1] { ((MonoBehaviour)o).gameObject };
				ActiveEditorTracker.sharedTracker.isLocked = true;
			}
			else
			{
				if (o == null)
				{
					Debug.LogWarning("No Object of type '" + t.Name + "' in current scenes");
				}
				else
				{
					Debug.LogWarning("" + o.ToString() + " is not of type '" + typeof(MonoBehaviour).Name + "'");
				}
			}

		}
		else
		{
			Debug.LogWarning("No type found for string '" + _typeToLookFor + "'");
		}
	}
}
