namespace AllMyScripts.Common.Utils.Editor
{
    using UnityEngine;
    using UnityEditor;

    using System.Collections;
    using System.IO;

    public class FindScriptRecursively : EditorWindow
	{	
		private static int go_count;
		private static int script_count;
		private static string sScriptName = string.Empty;
 
		[MenuItem("AllMyScripts/Tools/Find/ScriptRecursively %&s")]
		public static void ShowWindow()
		{
			EditorWindow.GetWindow(typeof(FindScriptRecursively));
		}
 
		public void OnGUI()
		{
			sScriptName = EditorGUILayout.TextField( "Script Name : ", sScriptName );

			if (GUILayout.Button("Find Script With This Name"))
			{
				FindInSelected();
			}
		
			if (GUILayout.Button("Find Script in all prefabs"))
			{
				FindInPrefabs();
			}
		}
		private static void FindInSelected()
		{
			GameObject[] go = Selection.gameObjects;
			go_count = 0;
			script_count = 0;
			foreach (GameObject g in go)
			{
   				FindInGO(g);
			}
			Debug.Log(string.Format("Searched {0} GameObjects, {1} script instances", go_count, script_count ));
		}
	
		void FindInPrefabs()
		{
			string[] files;
			GameObject obj;
			
			go_count = 0;
			script_count = 0;

			// Stack of folders:
			Stack stack = new Stack();

			// Add root directory:
			stack.Push(Application.dataPath);

			// Continue while there are folders to process
			while (stack.Count > 0)
			{
				// Get top folder:
				string dir = (string)stack.Pop();

				try
				{
					// Get a list of all prefabs in this folder:
					files = Directory.GetFiles(dir, "*.prefab");

					// Process all prefabs:
					for (int i = 0; i < files.Length; ++i)
					{
						// Make the file path relative to the assets folder:
						files[i] = files[i].Substring(Application.dataPath.Length - 6);

						obj = (GameObject)AssetDatabase.LoadAssetAtPath(files[i], typeof(GameObject));

						if (obj != null)
						{
							FindInGO(obj);
						}
					}

					// Add all subfolders in this folder:
					foreach (string dn in Directory.GetDirectories(dir))
					{
						stack.Push(dn);
					}
				}
				catch
				{
					// Error
					Debug.LogError("Could not access folder: \"" + dir + "\"");
				}
			}
			Debug.Log(string.Format("Searched {0} GameObjects, {1} script instances", go_count, script_count ));
		}
 
		private static void FindInGO(GameObject g)
		{
			go_count++;
			Component script = g.GetComponent( sScriptName );
			if( script!=null )
			{
				script_count++;
				Debug.Log( sScriptName + " has been found in " + g.name + " root " + g.transform.root.name + " (script : " + script.name + ")", g.transform.root );
			}

			// Now recurse through each child GO (if there are any):
			foreach (Transform childT in g.transform)
			{
				FindInGO(childT.gameObject);
			}
		}
	}
}