namespace AllMyScripts.Common.Utils.Editor
{
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEditor;
    using System.Collections;
    using System.IO;

    public class FindTextureRecursively : EditorWindow
	{	
		private static int go_count;
		private static int texture_count;
		private static string sTextureName = string.Empty;
 
		[MenuItem("AllMyScripts/Tools/Find/TextureRecursively %&g")]
		public static void ShowWindow()
		{
			EditorWindow.GetWindow(typeof(FindTextureRecursively));
		}
 
		public void OnGUI()
		{
			sTextureName = EditorGUILayout.TextField( "Material Name : ", sTextureName );

			if (GUILayout.Button("Find Texture With This Name"))
			{
				FindInSelected();
			}
		
			if (GUILayout.Button("Find Texture in all prefabs"))
			{
				FindInPrefabs();
			}
		}
		private static void FindInSelected()
		{
			GameObject[] go = Selection.gameObjects;
			go_count = 0;
			texture_count = 0;
			foreach (GameObject g in go)
			{
   				FindInGO(g);
			}
			Debug.Log(string.Format("Searched {0} GameObjects, {1} material instances", go_count, texture_count ));
		}
	
		void FindInPrefabs()
		{
			string[] files;
			GameObject obj;
			
			go_count = 0;
			texture_count = 0;

			// Stack of folders:
			Stack stack = new Stack();

			// Add root directory:
			stack.Push(Application.dataPath);

			// Continue while there are folders to process
			while (stack.Count > 0)
			{
				// Get top folder:
				string dir = (string)stack.Pop();

				try
				{
					// Get a list of all prefabs in this folder:
					files = Directory.GetFiles(dir, "*.prefab");

					// Process all prefabs:
					for (int i = 0; i < files.Length; ++i)
					{
						// Make the file path relative to the assets folder:
						files[i] = files[i].Substring(Application.dataPath.Length - 6);

						obj = (GameObject)AssetDatabase.LoadAssetAtPath(files[i], typeof(GameObject));

						if (obj != null)
						{
							FindInGO(obj);
						}
					}

					// Add all subfolders in this folder:
					foreach (string dn in Directory.GetDirectories(dir))
					{
						stack.Push(dn);
					}
				}
				catch
				{
					// Error
					Debug.LogError("Could not access folder: \"" + dir + "\"");
				}
			}
			Debug.Log(string.Format("Searched {0} GameObjects, {1} texture instances", go_count, texture_count ));
		}
 
		private static void FindInGO(GameObject g)
		{
			go_count++;
			Renderer rd = g.GetComponent<Renderer>();
			if( rd != null )
			{
				Material[] mats = rd.sharedMaterials;
				for (int i = 0; i < mats.Length; i++)
				{
					Material mat = mats[i];
					if( mat != null )
					{
						if( mat.mainTexture!=null && mat.mainTexture.name.Contains( sTextureName ) )
						{
							texture_count++;
							Debug.Log( mat.mainTexture.name + " has been found in " + g.name + " root " + g.transform.root.name + " (mat : " + mat.name + ")" );
						}
					}
				}
			}
			Image img = g.GetComponent<Image>();
			if( img!=null && img.mainTexture!=null )
			{
				if( img.mainTexture.name.Contains( sTextureName ) )
				{
					texture_count++;
					Debug.Log( img.mainTexture.name + " has been found in " + g.name + " root " + g.transform.root.name + " (img : " + img.name + ")" );
				}
			}
			RawImage rawImg = g.GetComponent<RawImage>();
			if( rawImg!=null && rawImg.mainTexture!=null )
			{
				if( rawImg.mainTexture.name.Contains( sTextureName ) )
				{
					texture_count++;
					Debug.Log( rawImg.mainTexture.name + " has been found in " + g.name + " root " + g.transform.root.name + " (img : " + rawImg.name + ")" );
				}
			}

			// Now recurse through each child GO (if there are any):
			foreach (Transform childT in g.transform)
			{
				FindInGO(childT.gameObject);
			}
		}
	}
}