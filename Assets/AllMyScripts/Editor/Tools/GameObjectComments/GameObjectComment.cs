﻿namespace AllMyScripts.Common.Tools.Editor
{
	using System;
	using UnityEditor;
	using UnityEngine;

	/// <summary>
	/// This class is executed with the editor, and will add to the top of gameobjects a comment editor
	/// </summary>
	[InitializeOnLoad]
	static class GameObjectComment
	{
		private static bool _foldout = false;
		private static string _text = "";
		private static UnityEngine.Object _lastTarget;
		private static GameObjectCommentHolder _currentHolder;
		private static GUIStyle _greenTextStyle;
		static GameObjectComment()
		{
			UnityEditor.Editor.finishedDefaultHeaderGUI += DisplayComment;
		}

		static void DisplayComment(UnityEditor.Editor editor)
		{
			try
			{
				//we surround all this in a big try catch, why? Because if WE crash, we don't want to destroy the user's ENTIRE inspector window.

				if (_greenTextStyle == null)
				{
					_greenTextStyle = new GUIStyle(EditorStyles.foldout);
					_greenTextStyle.normal.textColor = new Color(0, 0.5f, 0);
					_greenTextStyle.onNormal.textColor = new Color(0, 0.5f, 0);
					_greenTextStyle.focused.textColor = new Color(0, 0.5f, 0);
					_greenTextStyle.onFocused.textColor = new Color(0, 0.5f, 0);
					_greenTextStyle.active.textColor = new Color(0, 0.5f, 0);
					_greenTextStyle.onActive.textColor = new Color(0, 0.5f, 0);
					_greenTextStyle.fontSize = 20;
					_greenTextStyle.wordWrap = false;
					_greenTextStyle.fontStyle = FontStyle.Bold;
				}

				if (!(editor.target is GameObject))
					return;

				if (_lastTarget != editor.target)
				{
					//we selected something else!
					_lastTarget = editor.target;
					_text = "";
					_foldout = false;
					//get its holder
					_currentHolder = GetHolder((GameObject)editor.target, false);
					if (_currentHolder != null)
					{
						_text = _currentHolder.text;
					}
				}

				bool hasComment = _currentHolder != null;
				Rect totalRect;

				//we display the text "comment" in green if there's a comment

				if (hasComment)
				{
					GUIContent text = new GUIContent("Comments");
					totalRect = EditorGUILayout.GetControlRect(GUILayout.Height(_greenTextStyle.CalcSize(text).y));
					_foldout = EditorGUI.Foldout(totalRect, _foldout, text, _greenTextStyle);
				}
				else
				{
					totalRect = EditorGUILayout.GetControlRect();
					_foldout = EditorGUI.Foldout(totalRect, _foldout, "Comments");
				}


				if (_foldout)
				{
					//text edition if the foldout is open
					var rect = EditorGUILayout.GetControlRect(GUILayout.Height(100));
					EditorGUI.BeginChangeCheck();
					_text = EditorGUI.TextArea(rect, _text);

					if (EditorGUI.EndChangeCheck())
					{//text was edited!
					 //remove it if unneeded
						if (string.IsNullOrWhiteSpace(_text) || string.IsNullOrEmpty(_text))
						{
							if (_currentHolder != null)
							{
								try
								{
									GameObject.DestroyImmediate(_currentHolder.gameObject);
									_currentHolder = null;
								}
								catch (InvalidOperationException)
								{
									//this is illegal because we're in a prefab. Just remove the text
									_currentHolder.text = "";
								}
							}
							return;
						}

						if (_currentHolder == null) //there is no holder currently, add one
							_currentHolder = GetHolder((GameObject)editor.target, true);
						//we hide the holder
						//we have a commentholder and a text
						Undo.RecordObject(_currentHolder, "Add comment");
						_currentHolder.text = _text;
					}
				}
			}
			catch
			{
			}
		}

		/// <summary>
		/// returns the gameobject's comment holder, can add it if needed
		/// </summary>
		/// <param name="addIfUnavailable"></param>
		static GameObjectCommentHolder GetHolder(GameObject obj, bool addIfUnavailable)
		{
			Transform commentHolder = obj.transform.Find("GameObject Comments");
			if (commentHolder == null)
			{
				if (!addIfUnavailable)
					return null;
				commentHolder = new GameObject("GameObject Comments").transform;
				commentHolder.gameObject.AddComponent<GameObjectCommentHolder>();
				commentHolder.gameObject.tag = "EditorOnly"; //we make sure this isn't in a build
				commentHolder.SetParent(obj.transform);
				Undo.RegisterCreatedObjectUndo(commentHolder.gameObject, "Add comment");
			}
			return commentHolder.gameObject.GetComponent<GameObjectCommentHolder>();
		}
	}
}