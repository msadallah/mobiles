namespace AllMyScripts.Common.Utils.Editor
{
    using UnityEngine;
    using UnityEditor;

    using AllMyScripts.Common.Tools;
    //! @class EnumFlagPropertyDrawer
    //!
    //! @brief Property drawer for a lwEnumFlag class
    [CustomPropertyDrawer( typeof( EnumFlagBaseClass ), true )]
	public class EnumFlagPropertyDrawer : PropertyDrawer
	{
		public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			EditorGUI.BeginProperty( position, label, property );

			SerializedProperty valuesArrayProperty = property.FindPropertyRelative( "m_nValues" );
			SerializedProperty namesArrayProperty = property.FindPropertyRelative( "m_enumNames" );

			System.Text.StringBuilder sStringizedFlagValues = new System.Text.StringBuilder();
			int nFlagSetCounter = 0;
			for( int nIndex = 0; nIndex<namesArrayProperty.arraySize; ++nIndex )
			{
				SerializedProperty nameProperty = namesArrayProperty.GetArrayElementAtIndex( nIndex );
				bool bIsChecked = EnumFlagEditorUtils.IsValueChecked( valuesArrayProperty, nIndex );
				if( bIsChecked )
				{
					if( nFlagSetCounter==0 )
					{
						sStringizedFlagValues.Append( '|' );
					}
					sStringizedFlagValues.Append( nameProperty.stringValue );
					++nFlagSetCounter;
				}
			}

			string sPopupLabel;
			if( nFlagSetCounter==0 )
			{
				sPopupLabel = "None";
			}
			else if( nFlagSetCounter==namesArrayProperty.arraySize )
			{
				sPopupLabel = "Everything";
			}
			else if( nFlagSetCounter > 1 )
			{
				sPopupLabel = "Mixed...";
			}
			else
			{
				sPopupLabel = sStringizedFlagValues.ToString();
			}

			Rect controlRect = EditorGUI.PrefixLabel( position, label );
			if( UnityEngine.GUI.Button( controlRect, sPopupLabel, "MiniPopup" ) )
			{
				GenericMenu contextualMenu = new GenericMenu();
				contextualMenu.AddItem( new GUIContent( "None" ), false, new GenericMenu.MenuFunction( Curry.Bind( EnumFlagEditorUtils.SetAsNone, valuesArrayProperty ) ) ); 
				contextualMenu.AddItem( new GUIContent( "Everything" ), false, new GenericMenu.MenuFunction( Curry.Bind( EnumFlagEditorUtils.SetAsEverything, namesArrayProperty.arraySize, valuesArrayProperty ) ) );
				contextualMenu.AddSeparator( "" );

				for( int nIndex = 0; nIndex<namesArrayProperty.arraySize; ++nIndex )
				{
					SerializedProperty nameProperty = namesArrayProperty.GetArrayElementAtIndex( nIndex );
					bool bIsChecked = EnumFlagEditorUtils.IsValueChecked( valuesArrayProperty, nIndex );
					contextualMenu.AddItem( new GUIContent( nameProperty.stringValue ), bIsChecked, new GenericMenu.MenuFunction( Curry.Bind( EnumFlagEditorUtils.FlipFlag, valuesArrayProperty, nIndex ) ) );
				}
				contextualMenu.ShowAsContext();
			}

			EditorGUI.EndProperty();
		}
	}

	//! @class EnumFlagEditorUtils
	//!
	//!	@brief Utility class for editor methods
	public static class EnumFlagEditorUtils
	{
		//!	Set the property with the value given
		public static void Serialize<t_Enum>( ref SerializedProperty property, EnumFlag<t_Enum> value ) where t_Enum : struct, System.IConvertible
		{
			GlobalTools.Assert( property!=null );
			GlobalTools.Assert( value!=null );

			SerializedProperty internalArrayProperty = property.FindPropertyRelative( "m_nValues" );
			GlobalTools.Assert( internalArrayProperty!=null );
			if( internalArrayProperty.arraySize==0 )
			{
				internalArrayProperty.arraySize = EnumFlag<t_Enum>.ARRAY_LENGTH;

				SerializedProperty internalEnumNamesArray = property.FindPropertyRelative( "m_enumNames" );
				GlobalTools.Assert( internalEnumNamesArray.arraySize==0 ); // if the array of values have no element, this one should not have any too
				string[] enumNames = System.Enum.GetNames( typeof( t_Enum ) );
				internalEnumNamesArray.arraySize = enumNames.Length;
				for( int enumIndex = 0; enumIndex<enumNames.Length; ++enumIndex )
				{
					SerializedProperty enumNameProperty = internalEnumNamesArray.GetArrayElementAtIndex( enumIndex );
					enumNameProperty.stringValue = enumNames[enumIndex];
				}
			}
			GlobalTools.Assert( internalArrayProperty.arraySize == EnumFlag<t_Enum>.ARRAY_LENGTH );

			for( int nElementIndex = 0; nElementIndex<internalArrayProperty.arraySize; ++nElementIndex )
			{
				byte nValue = 0;

				int nFirstBitIndex = nElementIndex*sizeof( byte )*8;
				int nLastBitIndex = Mathf.Min( ( nElementIndex+1 )*sizeof( byte )*8, EnumFlag<t_Enum>.COUNT-1 );
				for( int nBitIndex = nFirstBitIndex; nBitIndex<=nLastBitIndex; ++nBitIndex )
				{
					if( value[nBitIndex] )
					{
						nValue |= ( byte )( 1 << ( nBitIndex-nFirstBitIndex ) );
					}
				}

				SerializedProperty elementProperty = internalArrayProperty.GetArrayElementAtIndex( nElementIndex );
				elementProperty.intValue = nValue;
			}
		}

		//! Get the value inside the property
		public static t_Result Deserialize<t_Result, t_Enum>( SerializedProperty property ) where t_Result : EnumFlag<t_Enum>, new() where t_Enum : struct, System.IConvertible
		{
			GlobalTools.Assert( property!=null );

			SerializedProperty internalArrayProperty = property.FindPropertyRelative( "m_nValues" );
			GlobalTools.Assert( internalArrayProperty!=null );
			GlobalTools.Assert( internalArrayProperty.arraySize == EnumFlag<t_Enum>.ARRAY_LENGTH );
			t_Result result = new t_Result();
			for( int nElementIndex = 0; nElementIndex<internalArrayProperty.arraySize; ++nElementIndex )
			{
				SerializedProperty elementProperty =internalArrayProperty.GetArrayElementAtIndex( nElementIndex );
				byte nValue = ( byte )elementProperty.intValue;

				int nFirstBitIndex = nElementIndex*sizeof( byte )*8;
				int nLastBitIndex = Mathf.Min( ( nElementIndex+1 )*sizeof( byte )*8, EnumFlag<t_Enum>.COUNT-1 );
				for( int nBitIndex = nFirstBitIndex; nBitIndex<=nLastBitIndex; ++nBitIndex )
				{
					result[nBitIndex] = ( nValue&( 1<<( nBitIndex-nFirstBitIndex ) ) ) != 0;
				}
			}
			return result;
		}

		public static bool IsValueChecked( SerializedProperty valuesArrayProperty, int nIndex )
		{
			int nArrayIndex = nIndex/( sizeof( byte ) * 8 );
			SerializedProperty elementProperty = valuesArrayProperty.GetArrayElementAtIndex( nArrayIndex );
			int nElementIndex = nIndex%( sizeof( byte ) * 8 );
			return ( elementProperty.intValue&( 1<<nElementIndex ) )!=0;
		}

		public static void SetAsNone( SerializedProperty valuesArrayProperty )
		{
			for( int nElementIndex = 0; nElementIndex<valuesArrayProperty.arraySize; ++nElementIndex )
			{
				SerializedProperty elementProperty = valuesArrayProperty.GetArrayElementAtIndex( nElementIndex );
				elementProperty.intValue = 0;
			}

			valuesArrayProperty.serializedObject.ApplyModifiedProperties();
		}

		public static void SetAsEverything( int nValueCount, SerializedProperty valuesArrayProperty )
		{
			for( int nElementIndex = 0; nElementIndex<valuesArrayProperty.arraySize; ++nElementIndex )
			{
				SerializedProperty elementProperty = valuesArrayProperty.GetArrayElementAtIndex( nElementIndex );

				byte value = byte.MaxValue;
				if( nElementIndex==valuesArrayProperty.arraySize-1 )
				{
					int nBitCount = nValueCount-( ( nValueCount/( sizeof( byte )*8 ) )*( sizeof( byte )*8 ) );
					value = ( byte )( ( 1<<nBitCount )-1 );
				}

				elementProperty.intValue = value;
			}

			valuesArrayProperty.serializedObject.ApplyModifiedProperties();
		}

		public static void FlipFlag( SerializedProperty valuesArrayProperty, int nIndex )
		{
			int nArrayIndex = nIndex/( sizeof( byte ) * 8 );
			SerializedProperty elementProperty = valuesArrayProperty.GetArrayElementAtIndex( nArrayIndex );
			int nElementIndex = nIndex%( sizeof( byte ) * 8 );

			if( ( elementProperty.intValue&( 1<<nElementIndex ) )!=0 )
			{
				elementProperty.intValue &= ~( 1<<nElementIndex );
			}
			else
			{
				elementProperty.intValue |= 1<<nElementIndex;
			}

			valuesArrayProperty.serializedObject.ApplyModifiedProperties();
		}
	}
}
