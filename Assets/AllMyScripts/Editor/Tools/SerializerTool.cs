namespace AllMyScripts.Common.Tools.Editor
{
	using UnityEngine;
	using UnityEditor;
    using System.IO;

    /// <summary>
    /// Tools to serialize or deserialize GameObject
    /// </summary>
    public class SerializerTool
	{
		[MenuItem("Assets/Serialize this prefab", false, 20)]
		public static void SerializeActiveObject()
		{
			Object cur = Selection.activeObject;
			if (cur == null)
				return;
			if (cur is GameObject)
			{
				string sPath = AssetDatabase.GetAssetPath(cur);
				Serializer.Serialize(sPath + ".zed", cur as GameObject);
			}
			else
			{
				Debug.LogError("Not a prefab!");
			}
		}

		[MenuItem("Assets/Deserialize this prefab", false, 20)]
		public static void DeserializeActiveObject()
		{
			Object cur = Selection.activeObject;
			if (cur == null)
				return;
			if (cur.name.EndsWith(".prefab"))
			{
				string sPath = AssetDatabase.GetAssetPath(cur);
				byte[] data = File.ReadAllBytes(sPath);
				Serializer.Deserialize(data);
			}
			else
			{
				Debug.LogError("Not a serialized prefab!");
			}
		}
	}
}
