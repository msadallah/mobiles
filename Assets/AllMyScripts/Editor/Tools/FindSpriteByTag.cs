namespace AllMyScripts.Common.Utils.Editor
{
    using UnityEngine;
    using UnityEditor;
    using System.Collections.Generic;

    public class FindSpriteByTag : EditorWindow
	{
		public struct SpriteInfo
		{
			public Sprite sprite;
			public string sName;
			public string sMaxSize;
			public string sFormat;
		}

		private static string s_sTagName = string.Empty;
		private static List<SpriteInfo> s_spriteList = new List<SpriteInfo>();
		private Vector2 s_v2Scroll;
 
		[MenuItem("AllMyScripts/Tools/Find/Sprite By Tag")]
		public static void ShowWindow()
		{
			EditorWindow.GetWindow<FindSpriteByTag>();
		}
 
		public void OnGUI()
		{
			s_sTagName = EditorGUILayout.TextField( "Tag Name : ", s_sTagName );
		
			if (GUILayout.Button("Find Sprites"))
			{
				FindInProject();
			}

			EditorGUILayout.BeginVertical();

			s_v2Scroll = GUILayout.BeginScrollView( s_v2Scroll, GUILayout.Height (800) );

			if( s_spriteList!=null )
			{
				if(GUILayout.Button("Select All"))
				{
					List<UnityEngine.Object> sprites = new List<UnityEngine.Object>();
					foreach (SpriteInfo info in s_spriteList)
					{
						sprites.Add(info.sprite.texture);
					}
					Selection.objects = sprites.ToArray();
				}
				foreach( SpriteInfo info in s_spriteList )
				{
					EditorGUILayout.BeginHorizontal();

					EditorGUILayout.ObjectField( info.sName, info.sprite.texture, typeof(Texture2D), false );
					GUILayout.Label( info.sMaxSize );
					GUILayout.Label( info.sFormat );

					EditorGUILayout.EndHorizontal();
				}
			}

			GUILayout.EndScrollView();

			EditorGUILayout.EndVertical();
		}
	
		void FindInProject()
		{
			string[] guids = AssetDatabase.FindAssets ("t:Sprite" );
			string sPath = string.Empty;

			s_spriteList.Clear();

			foreach (string guid in guids)
			{
				sPath = AssetDatabase.GUIDToAssetPath( guid );
				TextureImporter textureImporter = (TextureImporter)UnityEditor.AssetImporter.GetAtPath( sPath );
				SpriteInfo info;
				if( textureImporter.spritePackingTag==s_sTagName )
				{
					info.sprite = AssetDatabase.LoadAssetAtPath( sPath, typeof( Sprite ) ) as Sprite;
					info.sName = info.sprite.name;
					info.sMaxSize = textureImporter.maxTextureSize.ToString();
					info.sFormat = textureImporter.textureCompression.ToString();
					s_spriteList.Add( info );
				}
			}
		}
   
	}
}
