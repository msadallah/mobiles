namespace AllMyScripts.Common.Tools.Editor
{
    using UnityEngine;
    using UnityEditor;

    using System.IO;
    using System.Collections.Generic;

    using OfficeOpenXml;

    using AllMyScripts.Common.Tools;
	using AllMyScripts.Common.Utils.Editor;

	/// <summary>
	/// Lw localization checker tool.
	/// </summary>
	public class LocalizationCheckerTool : EditorWindow
	{
		[MenuItem( "AllMyScripts/LangManager/Localization Checker (Used and unused keys)", false, 1 )]
		public static LocalizationCheckerTool CreateOrGetWindow()
		{
			LocalizationCheckerTool window = EditorWindow.GetWindow<LocalizationCheckerTool>();
			window.minSize = new Vector2(600.0f, 300.0f);
			window.ShowUtility();

			return window;
		}

	#region Unity callbacks
		private void OnEnable()
		{
			titleContent = new GUIContent( "Localization Checker" );
			m_bIsExcludePathsFoldoutOpened = false;

			m_resultFilePath = null;
			m_resultLogPath = null;

			LoadPrefs();
		}

		private void OnDisable()
		{
			SavePrefs();
		}

		private void OnGUI()
		{
			EditorGUILayout.BeginVertical( UnityEngine.GUI.skin.box, GUILayout.ExpandWidth( true ) );
			{
				OnExcludePathsGUI( ref m_bIsExcludePathsFoldoutOpened );
			}
			EditorGUILayout.EndVertical();
			EditorGUILayout.Space();
			m_sExcelFilePath = Common.GUI.Editor.EditorGUILayout.FileSelection( "Excel source", m_sExcelFilePath, new string[]{ "Excel files", "xls,xlsx" }, PathUtil.PathType.Any, "Select source file" );
			EditorGUILayout.HelpBox("To use the tool, the file must be closed but locked into SVN for write acces.", MessageType.Info);
			EditorGUILayout.Space();

			EditorGUI.BeginChangeCheck();
			m_includeScriptFiles = EditorGUILayout.Toggle("Include script files", m_includeScriptFiles);
			if(EditorGUI.EndChangeCheck())
			{
				SavePrefs();
			}

			EditorGUI.BeginChangeCheck();
			m_shouldLogDetails = EditorGUILayout.Toggle("Create log details file", m_shouldLogDetails);
			if(EditorGUI.EndChangeCheck())
			{
				SavePrefs();
			}

			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.FlexibleSpace();
				Color oldBackgroundColor = UnityEngine.GUI.backgroundColor;
				UnityEngine.GUI.backgroundColor = Color.green;
				EditorGUI.BeginDisabledGroup(string.IsNullOrEmpty(m_sExcelFilePath)  ||  File.Exists(m_sExcelFilePath) == false);
				if( GUILayout.Button( "Check keys" ) )
				{
					CheckLocalizationKeys();
				}
				EditorGUI.EndDisabledGroup();
				UnityEngine.GUI.backgroundColor = oldBackgroundColor;
				GUILayout.FlexibleSpace();
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.Space();

			if(string.IsNullOrEmpty(m_resultFilePath) == false)
			{
				Color oldBackgroundColor = UnityEngine.GUI.backgroundColor;
				UnityEngine.GUI.backgroundColor = Color.cyan;
				EditorGUILayout.BeginVertical( UnityEngine.GUI.skin.box);
				{
					EditorGUILayout.LabelField("Result files", EditorStyles.boldLabel);

					EditorGUILayout.BeginHorizontal();
					{
						EditorGUILayout.LabelField("Output file", m_resultFilePath, GUILayout.ExpandWidth(true));
						if(GUILayout.Button("Open", GUILayout.ExpandWidth(false)))
						{
							EditorUtility.OpenWithDefaultApp(m_resultFilePath);
						}
					}
					EditorGUILayout.EndHorizontal();

					if(string.IsNullOrEmpty(m_resultLogPath) == false)
					{
						EditorGUILayout.BeginHorizontal();
						{
							EditorGUILayout.LabelField("Log file", m_resultLogPath, GUILayout.ExpandWidth(true));
							if(GUILayout.Button("Open", GUILayout.ExpandWidth(false)))
							{
								EditorUtility.OpenWithDefaultApp(m_resultLogPath);
							}
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndVertical();
				UnityEngine.GUI.backgroundColor = oldBackgroundColor;
			}
		}
	#endregion

	#region Private
		#region Declarations
		private static readonly string s_windowPreferencesPrefix = "AllMyScripts/LocalizationCheckerTool/";
		private static readonly string s_excelFileCheckerToolColumnTitle = "CHECKER_TOOL";

		/// <summary>
		/// Localization key.
		/// </summary>
		private class LocalizationKey
		{
			public readonly int worksheetIndex;
			public readonly int rowIndex;
			public readonly string value;

			public enum Status
			{
				NotUsed,
				Used
			}

			public LocalizationKey(int a_worksheetIndex, int a_rowIndex, string a_value)
			{
				worksheetIndex = a_worksheetIndex;
				rowIndex = a_rowIndex;
				value = a_value;

				m_duplicates = new HashSet<LocalizationKey>();
				m_assetPathsContainingReference = new List<string>();
			}

			public Status status
			{
				get{ return m_assetPathsContainingReference.Count == 0? Status.NotUsed : Status.Used; }
			}
			public int referenceCount
			{
				get{ return m_assetPathsContainingReference.Count; }
			}
			public string GetReference(int index)
			{
				return m_assetPathsContainingReference[index];
			}
			public void AddReference(string assetPathContainingReference)
			{
				m_assetPathsContainingReference.Add(assetPathContainingReference);
			}

			public bool hasDuplicates{ get{ return m_duplicates.Count > 0; } }
			public void AddDuplicate(LocalizationKey localizationKey)
			{
				GlobalTools.Assert(localizationKey != null);
				_AddDuplicate(localizationKey);
				localizationKey._AddDuplicate(this);
			}

			private void _AddDuplicate(LocalizationKey localizationKey)
			{
				GlobalTools.Assert(m_duplicates.Contains(localizationKey));
				m_duplicates.Add(localizationKey);
			}

			private HashSet<LocalizationKey> m_duplicates;
			private List<string> m_assetPathsContainingReference;
		}
		#endregion

		#region Methods
		private void LoadPrefs()
		{
			string sExcludePaths = EditorPrefs.GetString( s_windowPreferencesPrefix+"m_sExcludePaths" );
			m_sExcludePaths = sExcludePaths.Split( new char[]{ ';' }, System.StringSplitOptions.RemoveEmptyEntries );

			m_sExcelFilePath = EditorPrefs.GetString( s_windowPreferencesPrefix+"m_excelFilePath", "" );

			m_includeScriptFiles = EditorPrefs.GetBool(s_windowPreferencesPrefix + "m_includeScriptFiles", false);
			m_shouldLogDetails = EditorPrefs.GetBool(s_windowPreferencesPrefix + "m_shouldLogDetails", false);
		}

		private void SavePrefs()
		{
			string sExcludePaths = string.Join( ";", m_sExcludePaths );
			EditorPrefs.SetString( s_windowPreferencesPrefix+"m_sExcludePaths", sExcludePaths );

			EditorPrefs.SetString( s_windowPreferencesPrefix+"m_excelFilePath", m_sExcelFilePath );

			EditorPrefs.SetBool(s_windowPreferencesPrefix + "m_includeScriptFiles", m_includeScriptFiles);
			EditorPrefs.SetBool(s_windowPreferencesPrefix + "m_shouldLogDetails", m_shouldLogDetails);
		}

		private bool ShouldRetrieveAllReferences()
		{
			return m_shouldLogDetails;
		}

		private void CheckLocalizationKeys()
		{
			m_resultFilePath = null;
			m_resultLogPath = null;

			FileInfo fileInfo = new FileInfo(m_sExcelFilePath);

			bool shouldRetry = true;
			while(shouldRetry)
			{
				try
				{
					using(ExcelPackage excelFile = new ExcelPackage(fileInfo))
					{
						shouldRetry = false;
						string[] assetPaths;
						LocalizationKey[] localizationKeys;
						if(RetrievingAssetPaths(out assetPaths)  &&  ScanExcelFileForLocalizationKeys(excelFile, out localizationKeys)  &&  CheckAssetsForKeys(localizationKeys, assetPaths))
						{
							ApplyResults(excelFile, localizationKeys);

							try
							{
								excelFile.Save();
								m_resultFilePath = m_sExcelFilePath;
								EditorUtility.DisplayDialog("Localization Checker", "Verification has been done.\nThe excel file has been modified.", "Ok");
							}
							catch(System.Exception)
							{
								string fileDirectory = Path.GetDirectoryName(m_sExcelFilePath);
								string filename = Path.GetFileNameWithoutExtension(m_sExcelFilePath);
								string extension = Path.GetExtension(m_sExcelFilePath);
							
								int index = 2;
								string savedFilename;
								do
								{
									savedFilename = string.Format("{0}/{1}_{2}.{3}", fileDirectory, filename, index, extension);
									++index;
								}
								while(File.Exists(savedFilename));

								Stream stream = File.Create(savedFilename);
								excelFile.SaveAs(stream);
								stream.Close();
								m_resultFilePath = savedFilename;
								EditorUtility.DisplayDialog("Localization Checker", string.Format("Verification has been done.\nThe excel file seems to be locked, so the result has been saved into '{0}'.", savedFilename), "Oh...  Ok");
							}

							if(m_shouldLogDetails)
							{
								LogDetails(localizationKeys);
							}
						}
					}
				}
				catch(System.Exception)
				{
					shouldRetry = EditorUtility.DisplayDialog("Localization Checker", "The excel file may be opened by another program, please close it or cancel the procedure.", "I have closed it", "Cancel");
				}
			}
		}

		private bool RetrievingAssetPaths(out string[] assetPaths)
		{
			string[] assetGuids = AssetDatabase.FindAssets("t:TextAsset t:ScriptableObject t:GameObject");

			string progressBarTitle = "Scanning excel file for localization keys";
			string progressBarMessage = "{0}/" + assetGuids.Length + " : {1}";

			bool hasBeenCanceled = false;
			int assetGuidIndex = 0;
			List<string> assetPathList = new List<string>();
			while(hasBeenCanceled == false  &&  assetGuidIndex < assetGuids.Length)
			{
				string assetPath = AssetDatabase.GUIDToAssetPath(assetGuids[assetGuidIndex]);

				hasBeenCanceled = EditorUtility.DisplayCancelableProgressBar(progressBarTitle, string.Format(progressBarMessage, assetGuidIndex + 1, assetPath), assetGuidIndex / (float)assetGuids.Length);
				if(hasBeenCanceled == false)
				{
					// check exclude paths
					int excludePathIndex = 0;
					while(excludePathIndex < m_sExcludePaths.Length  &&  assetPath.StartsWith(m_sExcludePaths[excludePathIndex]) == false)
					{
						++excludePathIndex;
					}

					if(m_includeScriptFiles == false)
					{
						Object asset = AssetDatabase.LoadAssetAtPath<Object>(assetPath);
						if(asset is MonoScript)
						{
							// don't add the asset
							excludePathIndex = m_sExcludePaths.Length;
						}
					}

					if(excludePathIndex == m_sExcludePaths.Length)
					{
						assetPathList.Add(assetPath);
					}

					++assetGuidIndex;
				}
			}

			EditorUtility.ClearProgressBar();

			assetPaths = assetPathList.ToArray();
			return hasBeenCanceled == false;
		}

		private bool ScanExcelFileForLocalizationKeys(ExcelPackage excelPackage, out LocalizationKey[] localizationKeys)
		{
			Dictionary<string, List<LocalizationKey>> localizationKeyUnicityChecker = new Dictionary<string, List<LocalizationKey>>();
			List<LocalizationKey> localizationKeyList = new List<LocalizationKey>();

			string progressBarTitle = "Scanning excel file for localization keys";
			string progressBarMessage = "Worksheet {0}/" + excelPackage.Workbook.Worksheets.Count + " - row : {1}/{2}";
			float worksheetPercent = 1.0f / (float)excelPackage.Workbook.Worksheets.Count;

			int worksheetIndex = 1;
			bool hasBeenCanceled = false;
			while(hasBeenCanceled == false  &&  worksheetIndex <= excelPackage.Workbook.Worksheets.Count)
			{
				ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[worksheetIndex];
				int rowIndex = 2;
				while(hasBeenCanceled == false  &&  rowIndex <= worksheet.Dimension.Rows)
				{
					hasBeenCanceled = EditorUtility.DisplayCancelableProgressBar(progressBarTitle, string.Format(progressBarMessage, worksheetIndex, rowIndex - 1, worksheet.Dimension.Rows - 1), worksheetPercent * (worksheetIndex - 1 + (rowIndex - 2) / (float)(worksheet.Dimension.Rows - 2)));
					if(hasBeenCanceled == false)
					{
						string localizationKeyText = worksheet.GetValue<string>(rowIndex, 1);
						if(string.IsNullOrEmpty(localizationKeyText) == false)
						{
							LocalizationKey localizationKey = new LocalizationKey(worksheetIndex, rowIndex, localizationKeyText);
							localizationKeyList.Add(localizationKey);

							List<LocalizationKey> duplicateList;
							if(localizationKeyUnicityChecker.TryGetValue(localizationKeyText, out duplicateList))
							{
								duplicateList.Add(localizationKey);
							}
							else
							{
								duplicateList = new List<LocalizationKey>();
								duplicateList.Add(localizationKey);
								localizationKeyUnicityChecker.Add(localizationKeyText, duplicateList);
							}
						}
					}
					++rowIndex;
				}
				++worksheetIndex;
			}

			// fill the duplicates into the localization key
			Dictionary<string, List<LocalizationKey>>.Enumerator duplicateDictionaryEnumerator = localizationKeyUnicityChecker.GetEnumerator();
			while(duplicateDictionaryEnumerator.MoveNext())
			{
				if(duplicateDictionaryEnumerator.Current.Value.Count > 1)
				{
					for(int keyIndex = 0; keyIndex < duplicateDictionaryEnumerator.Current.Value.Count - 1; ++keyIndex)
					{
						for(int duplicateIndex = keyIndex + 1; duplicateIndex < duplicateDictionaryEnumerator.Current.Value.Count; ++duplicateIndex)
						{
							duplicateDictionaryEnumerator.Current.Value[keyIndex].AddDuplicate(duplicateDictionaryEnumerator.Current.Value[duplicateIndex]);
						}
					}
				}
			}

			EditorUtility.ClearProgressBar();

			localizationKeys = localizationKeyList.ToArray();
			return hasBeenCanceled == false;
		}

		private bool CheckAssetsForKeys(LocalizationKey[] localizationKeys, string[] assetPaths)
		{
			string progressBarTitle = "Checking keys in all files";
			string progressBarMessage = "{0}/" + assetPaths.Length + " - {1}";

			int assetIndex = 0;
			bool hasBeenCanceled = false;
			while(hasBeenCanceled == false  &&  assetIndex < assetPaths.Length)
			{
				hasBeenCanceled = EditorUtility.DisplayCancelableProgressBar(progressBarTitle, string.Format(progressBarMessage, assetIndex + 1, assetPaths[assetIndex]), assetIndex / (float)assetPaths.Length);
				if(hasBeenCanceled == false)
				{                                          
					string assetTextContent = File.ReadAllText(assetPaths[assetIndex]);

					int localizationKeyIndex = 0;
					while(localizationKeyIndex < localizationKeys.Length)
					{
						if(ShouldRetrieveAllReferences()  ||  localizationKeys[localizationKeyIndex].status == LocalizationKey.Status.NotUsed)
						{
							bool hasKeyInFile = assetTextContent.Contains(localizationKeys[localizationKeyIndex].value);
							if(hasKeyInFile)
							{
								localizationKeys[localizationKeyIndex].AddReference(assetPaths[assetIndex]);
							}
						}
					
						++localizationKeyIndex;
					}
				}

				++assetIndex;
			}

			EditorUtility.ClearProgressBar();

			return hasBeenCanceled == false;
		}

		private void ApplyResults(ExcelPackage excelPackage, LocalizationKey[] localizationKeys)
		{
			string progressBarTitle = "Writing result into excel file";
			string progressBarMessage = "writing result for key {0}/" + localizationKeys.Length + " - {1}";

			int[] worksheetCheckerToolColumnIndexes = new int[excelPackage.Workbook.Worksheets.Count];

			for(int localizationKeyIndex = 0; localizationKeyIndex < localizationKeys.Length; ++localizationKeyIndex)
			{
				EditorUtility.DisplayProgressBar(progressBarTitle, string.Format(progressBarMessage, localizationKeyIndex + 1, localizationKeys[localizationKeyIndex].value), localizationKeyIndex / (float)localizationKeys.Length);

				int worksheetIndex = localizationKeys[localizationKeyIndex].worksheetIndex;
				ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[worksheetIndex];
				if(worksheetCheckerToolColumnIndexes[worksheetIndex - 1] <= 0)
				{
					worksheetCheckerToolColumnIndexes[worksheetIndex - 1] = ExcelUtils.FindColumn(worksheet, 1, s_excelFileCheckerToolColumnTitle);
					if(worksheetCheckerToolColumnIndexes[worksheetIndex - 1] <= 0)
					{
						// the column does not exist, add it
						worksheet.InsertColumn(2, 1);
						worksheet.SetValue(1, 2, s_excelFileCheckerToolColumnTitle);
						worksheetCheckerToolColumnIndexes[worksheetIndex - 1] = 2;
					}
				}

				string resultText;
				switch(localizationKeys[localizationKeyIndex].status)
				{
					case LocalizationKey.Status.NotUsed: resultText = "X"; break;
					case LocalizationKey.Status.Used:
					{
						if(localizationKeys[localizationKeyIndex].hasDuplicates)
						{
							resultText = "D";
						}
						else
						{
							resultText = "V";
						}
					}
					break;
					default:
					{
						GlobalTools.AssertFormat(false, "Unhandled localization key status '{0}'.", localizationKeys[localizationKeyIndex].status);
						resultText = "?";
					}
					break;
				}
				worksheet.SetValue(localizationKeys[localizationKeyIndex].rowIndex, worksheetCheckerToolColumnIndexes[worksheetIndex - 1], resultText);
			}

			EditorUtility.ClearProgressBar();
		}

		private void LogDetails(LocalizationKey[] localizationKeys)
		{
			System.DateTime date = System.DateTime.Now;

			string fileDirectory = Path.GetDirectoryName(m_sExcelFilePath);
			string filename = string.Format("{0}_{1}_{2}_{3}", date.Year, date.Month.ToString("00"), date.Day.ToString("00"), Path.GetFileNameWithoutExtension(m_sExcelFilePath));

			int index = 2;
			string savedFilename;
			do
			{
				savedFilename = string.Format("{0}/{1}_{2}.log", fileDirectory, filename, index);
				++index;
			}
			while(File.Exists(savedFilename));

			StreamWriter stream = File.CreateText(savedFilename);
			stream.WriteLine("=========================================================================================");
			stream.WriteLine("This file has been generated using the Localization Checker tool on " + date.ToString("G"));
			stream.WriteLine("=========================================================================================");

			for(int localizationKeyIndex = 0; localizationKeyIndex < localizationKeys.Length; ++localizationKeyIndex)
			{
				stream.WriteLine(string.Format("{0} : {1}{2}", localizationKeys[localizationKeyIndex].value, localizationKeys[localizationKeyIndex].hasDuplicates? "[Duplicate]" : "", localizationKeys[localizationKeyIndex].status == LocalizationKey.Status.NotUsed? "[UNUSED]" : ""));
				for(int referenceIndex = 0; referenceIndex < localizationKeys[localizationKeyIndex].referenceCount; ++referenceIndex)
				{
					stream.WriteLine("\t- " + localizationKeys[localizationKeyIndex].GetReference(referenceIndex));
				}
			}

			stream.Close();

			m_resultLogPath = savedFilename;
		}

		private void OnExcludePathsGUI( ref bool bIsFoldoutOpened )
		{
			bIsFoldoutOpened = EditorGUILayout.Foldout( bIsFoldoutOpened, string.Format("Exclude paths ({0})", m_sExcludePaths.Length) );
			if( bIsFoldoutOpened )
			{
				EditorGUILayout.HelpBox( "Assets in those paths will not be processed in the search of localization keys.", MessageType.Warning );

				int nRemovedIndex = -1;
				for( int nIndex = 0; nIndex<m_sExcludePaths.Length; ++nIndex )
				{
					EditorGUILayout.BeginHorizontal();
					{
						if( GUILayout.Button( "X", GUILayout.ExpandWidth( false ) ) )
						{
							nRemovedIndex = nIndex;
						}
						EditorGUILayout.LabelField( m_sExcludePaths[nIndex], GUILayout.ExpandWidth( true ) );
					}
					EditorGUILayout.EndHorizontal();
				}

				if( nRemovedIndex>=0 )
				{
					for( int nIndex = nRemovedIndex; nIndex<m_sExcludePaths.Length-2; ++nIndex )
					{
						m_sExcludePaths[nIndex] = m_sExcludePaths[nIndex+1];
					}
					System.Array.Resize( ref m_sExcludePaths, m_sExcludePaths.Length-1 );
				}

				if( GUILayout.Button( "Add folder to exclude", GUILayout.ExpandWidth( false ) ) )
				{
					string sSelectedFolderPath = EditorUtility.SaveFolderPanel( "Select new exclude folder", Application.dataPath, "" );
					if( string.IsNullOrEmpty( sSelectedFolderPath )==false && sSelectedFolderPath.StartsWith( Application.dataPath, System.StringComparison.Ordinal ) )
					{
						string sRelativePath = sSelectedFolderPath.Substring( Application.dataPath.Length-"Assets".Length );
						if( System.Array.IndexOf( m_sExcludePaths, sRelativePath )==-1 )
						{
							System.Array.Resize( ref m_sExcludePaths, m_sExcludePaths.Length+1 );
							m_sExcludePaths[m_sExcludePaths.Length-1] = sRelativePath;

							SavePrefs();
						}
					}
				}
			}
		}
		#endregion

		#region Attributes
		private bool m_bIsExcludePathsFoldoutOpened;
		private static string[] m_sExcludePaths;

		private string m_sExcelFilePath;

		private bool m_includeScriptFiles;
		private bool m_shouldLogDetails;

		private string m_resultFilePath;
		private string m_resultLogPath;
		#endregion
	#endregion
	}
}
