namespace AllMyScripts.Common.Tools.Editor
{
    using UnityEngine;
    using UnityEditor;

    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Reflection;

    using Object = UnityEngine.Object;

    //! @class MemoryWatcher
    //! \author Raphael Benedetto
    //!	@brief	Editor window that build a report of memory used at a certain instant of the game
    public class MemoryWatcher : EditorWindow
	{
		public enum ReportType
		{
			FindObjectInScene,
			FindObjectInMemory,
			FindTypeDepInScene,
			FindTypeDepInMemory,
			FindRefInComponents,
			CollectTypeDep,
			DiffSceneMemory,
			Count
		};

		public enum DepFilter
		{
			None,
			All,
			Direct,
			Root,
			Component,
			DirectComponent,
			Count
		}

		[MenuItem( "AllMyScripts/Tools/Find/Memory watcher" )]
		private static void CreateWindow()
		{
			MemoryWatcher window = EditorWindow.GetWindow<MemoryWatcher>();
			window.ShowUtility();
		}

		#region Unity callbacks
		private void OnEnable()
		{
			m_report = new List<ObjectInfo>();
			m_reportScrollPosition = Vector2.zero;
			m_selectedNode = null;

			m_objectInfoComparer = new ObjectInfoComparer();
			m_objectInfoComparer.sortingColumn = -1;
			m_objectInfoComparer.isAscending = true;

			m_currentType = StringToType( m_sTypeName );

			EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
		}

		private void OnDisable()
		{
			EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
			m_selectedNode = null;
			m_report = null;
		}

		private void OnGUI()
		{
			if( Application.isPlaying )
			{
				OnReportGUI();

				EditorGUILayout.BeginHorizontal();
				string sName = EditorGUILayout.TextField( "Type name", m_sTypeName );
				if( m_sTypeName != sName )
				{
					m_sTypeName = sName;
					if( string.IsNullOrEmpty( m_sTypeName ) )
						m_currentType = null;
					else
						m_currentType = StringToType( m_sTypeName );
				}
				m_sObjectName = EditorGUILayout.TextField( "Object name", m_sObjectName );
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				m_nCurrentID = EditorGUILayout.IntField( "ID", m_nCurrentID );
				m_depFilter = (DepFilter)EditorGUILayout.EnumPopup( "Dep Filter", m_depFilter );
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				m_sAtlasName = EditorGUILayout.TextField( "Atlas name", m_sAtlasName );
				EditorGUILayout.EndHorizontal();

				GUI.enabled = m_currentType != null || string.IsNullOrEmpty( m_sTypeName );

				// FindObjectInScene
				if( GUILayout.Button( "Pause and FindObjectInScene" ) )
				{
					EditorApplication.isPaused = true;
					BuildReportRoutine( ReportType.FindObjectInScene, m_sObjectName, m_currentType, m_nCurrentID, m_sAtlasName );
				}
				// FindObjectInMemory
				if( GUILayout.Button( "Pause and FindObjectInMemory" ) )
				{
					EditorApplication.isPaused = true;
					BuildReportRoutine( ReportType.FindObjectInMemory, m_sObjectName, m_currentType, m_nCurrentID, m_sAtlasName );
				}
				// FindTypeDepInScene
				if( GUILayout.Button( "Pause and FindTypeDepInScene" ) )
				{
					EditorApplication.isPaused = true;
					BuildReportRoutine( ReportType.FindTypeDepInScene, m_sObjectName, m_currentType, m_nCurrentID, m_sAtlasName );
				}
				// FindTypeDepInMemory
				if( GUILayout.Button( "Pause and FindTypeDepInMemory" ) )
				{
					EditorApplication.isPaused = true;
					BuildReportRoutine( ReportType.FindTypeDepInMemory, m_sObjectName, m_currentType, m_nCurrentID, m_sAtlasName );
				}
				// FindRefInComponents
				if( GUILayout.Button( "Pause and FindRefInComponents" ) )
				{
					EditorApplication.isPaused = true;
					BuildReportRoutine( ReportType.FindRefInComponents, m_sObjectName, m_currentType, m_nCurrentID, m_sAtlasName );
				}
				// CollectTypeDep
				if( GUILayout.Button( "Pause and CollectTypeDep" ) )
				{
					EditorApplication.isPaused = true;
					BuildReportRoutine( ReportType.CollectTypeDep, m_sObjectName, m_currentType, m_nCurrentID, m_sAtlasName );
				}
				// DiffSceneMemory
				if( GUILayout.Button( "Pause and DiffSceneMemory" ) )
				{
					EditorApplication.isPaused = true;
					BuildReportRoutine( ReportType.DiffSceneMemory, m_sObjectName, m_currentType, m_nCurrentID, m_sAtlasName );
				}

				GUI.enabled = true;

				// Grabbing
				EditorGUILayout.BeginHorizontal();
				// Grab memory
				if( GUILayout.Button( "Grab memory" ) )
				{
					GrabMemory();
				}
				// Compare memory
				if( GUILayout.Button( "Compare memory" ) )
				{
					CompareMemory();
				}
				// Reset memory
				if( GUILayout.Button( "Reset memory" ) )
				{
					ResetMemory();
				}
				EditorGUILayout.EndHorizontal();
				// Clean
				if( GUILayout.Button( "GC.Collect & Resources.UnloadUnusedAssets" ) )
				{
					EditorApplication.isPaused = false;
					GC.Collect();
					Resources.UnloadUnusedAssets();
				}
			}
			else
			{
				EditorGUILayout.HelpBox( "The application should be playing in order to use this tool.", MessageType.Error );
			}
		}
		#endregion

		#region Private
		#region Declaration
		private class RefInfo
		{
			internal string m_sName;
			internal string m_sType;
			internal string m_sTexPackingTag;
			internal int m_nInstanceID;

			public RefInfo( Object oRef )
			{
				m_sName = oRef.name;
				m_sType = oRef.GetType().ToString();
				m_sTexPackingTag = null;
				if( oRef is Texture )
				{
					string texturePath = AssetDatabase.GetAssetPath( oRef );
					AssetImporter assetImporter = AssetImporter.GetAtPath( texturePath );
					TextureImporter textureImporter = assetImporter as TextureImporter;
					m_sTexPackingTag = textureImporter != null ? textureImporter.spritePackingTag : null;
				}
				m_nInstanceID = oRef.GetInstanceID();
			}

			public RefInfo( string sName )
			{
				m_sName = sName;
				m_sType = null;
				m_sTexPackingTag = null;
				m_nInstanceID = 0;
			}

			public string GetInfo( bool bWithPackingTag = false )
			{
				if( m_nInstanceID == 0 ) return m_sName;
				if( bWithPackingTag )
					return m_sName + "(" + m_sType + "/" + m_nInstanceID + "/" + m_sTexPackingTag + ")";
				else
					return m_sName + "(" + m_sType + "/" + m_nInstanceID + ")";
			}
		}

		private class ObjectInfo
		{
			internal RefInfo m_ref;
			internal List<RefInfo> references = new List<RefInfo>();

			public ObjectInfo( Object o )
			{
				m_ref = new RefInfo( o );
			}

			public ObjectInfo( String sName )
			{
				m_ref = new RefInfo( sName );
			}
		}

		private class ObjectInfoComparer : IComparer<ObjectInfo>
		{
			public int sortingColumn;
			public bool isAscending;

			int IComparer<ObjectInfo>.Compare( ObjectInfo lhs, ObjectInfo rhs )
			{
				if( sortingColumn == -1 )
				{
					return -1;
				}
				else
				{
					int comparison = 0;
					switch( sortingColumn )
					{
						case (int)Column.AssetName: comparison = string.CompareOrdinal( lhs.m_ref.m_sName, rhs.m_ref.m_sName ); break;
						case (int)Column.AtlasName: comparison = string.CompareOrdinal( lhs.m_ref.m_sTexPackingTag, rhs.m_ref.m_sTexPackingTag ); break;
						case (int)Column.References: comparison = Comparer<int>.Default.Compare( lhs.references.Count, rhs.references.Count ); break;
					}

					return isAscending ? comparison : -comparison;
				}
			}
		}

		private enum Column
		{
			AssetName,
			AtlasName,
			References,
		}
		private static string[] ms_columnNames = new string[]
		{
			"Object name", "Name of the Object",
			"Atlas name", "Name of the atlas",
			"References", "Number of objects that uses this asset"
		};
		private static int[] ms_columnWidths = new int[]
		{
			-1, 100,
			200, 100,
			100, 100,
		};
		private static GUILayoutOption[] ColumnWidth( int columnIndex )
		{
			return new GUILayoutOption[]
			{
				(ms_columnWidths[columnIndex * 2] < 0? GUILayout.ExpandWidth(true) : GUILayout.Width(ms_columnWidths[columnIndex * 2])),
				GUILayout.MinWidth(ms_columnWidths[columnIndex * 2 + 1])
			};
		}
		#endregion

		#region Methods

		public static Type StringToType( string TypeName )
		{
			if( string.IsNullOrEmpty( TypeName ) )
				return null;

			// Try Type.GetType() first. This will work with types defined
			// by the Mono runtime, in the same assembly as the caller, etc.
			var type = Type.GetType( TypeName );

			// If it worked, then we're done here
			if( type != null )
				return type;

			// If the TypeName is a full name, then we can try loading the defining assembly directly
			if( TypeName.Contains( "." ) )
			{

				// Get the name of the assembly (Assumption is that we are using 
				// fully-qualified type names)
				var assemblyName = TypeName.Substring( 0, TypeName.IndexOf( '.' ) );

				// Attempt to load the indicated Assembly
				var assembly = Assembly.Load( assemblyName );
				if( assembly == null )
					return null;

				// Ask that assembly to return the proper Type
				type = assembly.GetType( TypeName );
				if( type != null )
					return type;

			}

			// If we still haven't found the proper type, we can enumerate all of the 
			// loaded assemblies and see if any of them define the type
			var currentAssembly = Assembly.GetExecutingAssembly();
			var referencedAssemblies = currentAssembly.GetReferencedAssemblies();
			foreach( var assemblyName in referencedAssemblies )
			{

				// Load the referenced assembly
				var assembly = Assembly.Load( assemblyName );
				if( assembly != null )
				{
					// See if that assembly defines the named type
					type = assembly.GetType( TypeName );
					if( type != null )
						return type;
				}
			}

			Debug.LogWarning( "Type not found!" );

			// The type just couldn't be found...
			return null;
		}

		private void OnPlayModeStateChanged( PlayModeStateChange state )
		{
			// clear report if we stop playing or stops the pause
			if(  state == PlayModeStateChange.ExitingEditMode || EditorApplication.isPaused == false )
			{
				m_selectedNode = null;
				m_report.Clear();
				m_reportScrollPosition = Vector2.zero;
			}
		}

		private void OnReportGUI()
		{
			if( m_report == null )
			{
				return;
			}

			int columnCount = Enum.GetValues(typeof(Column)).Length;

			// headers
			EditorGUILayout.BeginHorizontal( EditorStyles.toolbar );
			for( int columnIndex = 0; columnIndex < columnCount; ++columnIndex )
			{
				GUIContent headerContent = new GUIContent(ms_columnNames[columnIndex * 2], ms_columnNames[columnIndex * 2 + 1]);
				if( GUILayout.Button( headerContent, EditorStyles.toolbarButton, ColumnWidth( columnIndex ) ) )
				{
					if( m_objectInfoComparer.sortingColumn == columnIndex )
					{
						m_objectInfoComparer.isAscending = m_objectInfoComparer.isAscending == false;
					}
					else
					{
						m_objectInfoComparer.sortingColumn = columnIndex;
						m_objectInfoComparer.isAscending = true;
					}

					m_report.Sort( m_objectInfoComparer );
				}
			}
			float minWidth, maxWidth;
			GUI.skin.verticalScrollbar.CalcMinMaxWidth( GUIContent.none, out minWidth, out maxWidth );
			GUILayout.Space( maxWidth );
			EditorGUILayout.EndHorizontal();

			m_reportScrollPosition = EditorGUILayout.BeginScrollView( m_reportScrollPosition, false, false, GUILayout.ExpandWidth( true ) );
			{
				for( int index = 0; index < m_report.Count; ++index )
				{
					EditorGUILayout.BeginVertical( GUI.skin.box );
					EditorGUILayout.BeginHorizontal( GUILayout.ExpandWidth( true ) );

					GUIContent ObjectNameContent = new GUIContent(m_report[index].m_ref.GetInfo(), ms_columnNames[(int)Column.AssetName * 2 + 1]);
					EditorGUILayout.LabelField( ObjectNameContent, ColumnWidth( (int)Column.AssetName ) );
					GUIContent atlasNameContent = new GUIContent(m_report[index].m_ref.m_sTexPackingTag, ms_columnNames[(int)Column.AtlasName * 2 + 1]);
					EditorGUILayout.LabelField( atlasNameContent, ColumnWidth( (int)Column.AtlasName ) );
					GUIContent referencesContent = new GUIContent(m_report[index].references.Count.ToString(), ms_columnNames[(int)Column.References * 2 + 1]);
					EditorGUILayout.LabelField( referencesContent, ColumnWidth( (int)Column.References ) );

					EditorGUILayout.EndHorizontal();

					Rect lineRect = GUILayoutUtility.GetLastRect();
					if( m_report[index] == m_selectedNode )
					{
						OnSelectedNodeGUI();
					}

					if( Event.current.isMouse )
					{
						if( lineRect.Contains( Event.current.mousePosition ) && Event.current.button == 0 && Event.current.type == EventType.MouseUp )
						{
							if( m_selectedNode == m_report[index] )
							{
								m_selectedNode = null;
							}
							else
							{
								m_selectedNode = m_report[index];
							}

							Repaint();
						}
					}
					EditorGUILayout.EndVertical();
				}
			}
			EditorGUILayout.EndScrollView();
		}

		private void OnSelectedNodeGUI()
		{
			++EditorGUI.indentLevel;
			for( int referenceIndex = 0; referenceIndex < m_selectedNode.references.Count; ++referenceIndex )
			{
				RefInfo info = m_selectedNode.references[referenceIndex];
				EditorGUILayout.BeginHorizontal();
				if( GUILayout.Button( "Select", GUILayout.ExpandWidth( false ) ) )
				{
					EditorGUIUtility.PingObject( info.m_nInstanceID );
				}
				EditorGUILayout.LabelField( "- " + info.GetInfo( true ) );
				if( GUILayout.Button( "Watch", GUILayout.ExpandWidth( false ) ) )
				{
					Object o = EditorUtility.InstanceIDToObject( info.m_nInstanceID );
					Debug.Log( "Watch " + info.m_sName + " " + info.m_nInstanceID + " o " + o );
					if( o != null )
					{
						m_sObjectName = o.name;
						m_currentType = o.GetType();
						m_sTypeName = m_currentType.ToString();
						m_nCurrentID = o.GetInstanceID();
					}
				}
				EditorGUILayout.EndHorizontal();
			}
			--EditorGUI.indentLevel;
		}

		private void BuildReportRoutine( ReportType reportType, string sAssetName = null, Type type = null, int nID = 0, string sAtlasName = null )
		{
			m_report.Clear();

			try
			{
				EditorUtility.DisplayProgressBar( "Build report on objects use", "Gathering all objects", 0.0f );

				if( reportType == ReportType.FindObjectInScene )
				{
					Object[] sceneObjects = EditorUtility.CollectDependencies( GetObjectsInScene() );
					SearchObjects( sceneObjects, "All objects in scene of type ", sAssetName, nID, type, sAtlasName );
				}
				else if( reportType == ReportType.FindObjectInMemory )
				{
					Object[] allObjects = Resources.FindObjectsOfTypeAll( typeof( Object ) ) as Object[];
					SearchObjects( allObjects, "All objects in memory of type ", sAssetName, nID, type, sAtlasName );
				}
				else if( reportType == ReportType.FindTypeDepInScene && type != null )
				{
					Dictionary<Object, ObjectInfo> oDictionary = new Dictionary<Object, ObjectInfo>();
					Object[] allObjects = Resources.FindObjectsOfTypeAll( typeof(Object) ) as Object[];
					Object o = null;
					int nObjectCount = allObjects.Length;
					for( int i = 0; i < nObjectCount; ++i )
					{
						o = allObjects[i];
						EditorUtility.DisplayProgressBar( "Build report on Object use", "Parse all objects : " + o.name, 0.0f );
						if( MatchObject( o, sAssetName, type, nID, sAtlasName ) )
							GatherInformation<Object>( o, ref oDictionary, GetObjectsInScene() );
					}
					ClearReports();
					oDictionary.Clear();
					oDictionary = null;
				}
				else if( reportType == ReportType.FindTypeDepInMemory && type != null )
				{
					Dictionary<Object, ObjectInfo> oDictionary = new Dictionary<Object, ObjectInfo>();
					Object[] allObjects = Resources.FindObjectsOfTypeAll( typeof(Object) ) as Object[];
					Object o = null;
					int nObjectCount = allObjects.Length;
					for( int i = 0; i < nObjectCount; ++i )
					{
						o = allObjects[i];
						EditorUtility.DisplayProgressBar( "Build report on Object use", "Parse all objects : " + o.name, 0.0f );
						if( MatchObject( o, sAssetName, type, nID, sAtlasName ) )
							GatherInformation<Object>( o, ref oDictionary, allObjects );
					}
					ClearReports();
					oDictionary.Clear();
					oDictionary = null;
				}
				else if( reportType == ReportType.FindRefInComponents && type != null )
				{
					Dictionary<Object, ObjectInfo> oDictionary = new Dictionary<Object, ObjectInfo>();
					Object[] allObjects = Resources.FindObjectsOfTypeAll( typeof(Object) ) as Object[];
					Object o = null;
					int nObjectCount = allObjects.Length;
					for( int i = 0; i < nObjectCount; ++i )
					{
						o = allObjects[i];
						EditorUtility.DisplayProgressBar( "Build report on Object use", "Parse all objects : " + o.name, 0.0f );
						if( MatchObject( o, sAssetName, type, nID, sAtlasName ) )
						{
							foreach( Object obj in allObjects )
							{
								FieldInfo[] fields = obj.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
								foreach( FieldInfo fieldInfo in fields )
								{
									if( o is MonoBehaviour && FieldReferencesComponent<MonoBehaviour>( obj, fieldInfo, o as MonoBehaviour ) )
									{
										GatherComponentInformation<Object>( obj, ref oDictionary, fieldInfo.Name );
									}
									else if( o is MonoScript && FieldReferencesComponent<MonoScript>( obj, fieldInfo, o as MonoScript ) )
									{
										GatherComponentInformation<Object>( obj, ref oDictionary, fieldInfo.Name );
									}
								}
							}
						}
					}
					ClearReports();
					oDictionary.Clear();
					oDictionary = null;
				}
				else if( reportType == ReportType.CollectTypeDep && type != null )
				{
					Object[] allObjects = Resources.FindObjectsOfTypeAll( typeof( Object ) ) as Object[];
					Object[] oArray = new Object[1];
					int nObjectCount = allObjects.Length;
					for( int i = 0; i < nObjectCount; ++i )
					{
						Object o = allObjects[i];
						if( MatchObject( o, sAssetName, type, nID, sAtlasName ) )
						{
							ObjectInfo objectInfo = new ObjectInfo( o );
							m_report.Add( objectInfo );
							oArray[0] = o;
							Object[] oDepArray = EditorUtility.CollectDependencies( oArray );
							int nDepCount = oDepArray.Length;
							for( int j = 0; j < nDepCount; ++j )
							{
								if( oDepArray[j] != null )
								{
									Object oRef = oDepArray[j];
									if( oRef != o )
									{
										RefInfo refInfo = new RefInfo( oRef );
										objectInfo.references.Add( refInfo );
									}
								}
							}
						}
					}
					//ClearReports();
				}
				else if( reportType == ReportType.DiffSceneMemory && type != null )
				{
					Object[] oDepArray = EditorUtility.CollectDependencies( GetObjectsInScene() );
					Object[] allObjects = Resources.FindObjectsOfTypeAll( typeof( Object ) ) as Object[];
					int nObjectCount = allObjects.Length;
					for( int i = 0; i < nObjectCount; ++i )
					{
						Object o = allObjects[i];
						if( MatchObject( o, sAssetName, type, nID, sAtlasName ) )
						{
							if( IsInsideArray<Object>( o, oDepArray ) )
								continue;

							ObjectInfo objectInfo = new ObjectInfo( o );
							m_report.Add( objectInfo );
						}
					}
					ClearReports();
				}

				EditorUtility.ClearProgressBar();
			}
			catch( Exception exception )
			{
				EditorUtility.ClearProgressBar();
				throw exception;
			}
		}

		private void GatherInformation<T>( T asset, ref Dictionary<T, ObjectInfo> infoDictionary, Object[] allObjects ) where T : Object
		{
			if( asset != null )
			{
				if( infoDictionary.ContainsKey( asset ) == false )
				{
					ObjectInfo objectInfo = null;
					Object currentObj = null;
					int nObjectCount = allObjects.Length;
					for( int i = 0; i < nObjectCount; ++i )
					{
						currentObj = allObjects[i];
						if( currentObj == null || currentObj == asset ) continue;
						EditorUtility.DisplayProgressBar( "Build report on " + typeof( T ) + " use", "Parse all " + typeof( T ) + " : " + asset.name, (float)i / (float)nObjectCount );
						if( IsDependent( currentObj, asset ) )
						{
							if( objectInfo == null )
								objectInfo = new ObjectInfo( asset );
							RefInfo info = new RefInfo( currentObj );
							objectInfo.references.Add( info );
						}
					}

					if( objectInfo != null )
					{
						infoDictionary.Add( asset, objectInfo );
						m_report.Add( objectInfo );
					}
				}
			}
		}

		private void GatherComponentInformation<T>( T asset, ref Dictionary<T, ObjectInfo> infoDictionary, string sMember ) where T : Object
		{
			if( asset != null )
			{
				ObjectInfo objectInfo = null;
				infoDictionary.TryGetValue( asset, out objectInfo );
				if( objectInfo == null )
					objectInfo = new ObjectInfo( asset );
				RefInfo info = new RefInfo( sMember );
				objectInfo.references.Add( info );
				if( infoDictionary.ContainsKey( asset ) == false )
				{
					infoDictionary.Add( asset, objectInfo );
					m_report.Add( objectInfo );
				}
			}
		}

		private void SearchObjects( Object[] objects, string sTitle, string sAssetName = null, int nID = 0, Type type = null, string sAtlasName = null )
		{
			if( type != null )
			{
				SearchObjectsOfType( objects, sTitle, type, sAssetName, nID, sAtlasName );
				return;
			}
			Dictionary<Type,ObjectInfo> typeDic = new Dictionary<Type, ObjectInfo>();
			int nObjectCount = objects.Length;
			for( int i = 0; i < nObjectCount; ++i )
			{
				Object o = objects[i];
				EditorUtility.DisplayProgressBar( "SearchObjects", "Parse all object " + o.name, (float)i / (float)nObjectCount );
				if( MatchObject( o, sAssetName, null, nID, sAtlasName ) )
				{
					type = o.GetType();
					ObjectInfo objectInfo = null;
					if( !typeDic.TryGetValue( type, out objectInfo ) )
					{
						objectInfo = new ObjectInfo( sTitle + type );
						m_report.Add( objectInfo );
						typeDic.Add( type, objectInfo );
					}
					RefInfo info = new RefInfo( o );
					objectInfo.references.Add( info );
				}
			}
			typeDic.Clear();
			typeDic = null;
		}

		private void SearchObjectsOfType( Object[] objects, string sTitle, Type type, string sAssetName = null, int nID = 0, string sAtlasName = null )
		{
			int nObjectCount = objects.Length;
			ObjectInfo objectInfo = new ObjectInfo( sTitle + type );
			m_report.Add( objectInfo );
			for( int i = 0; i < nObjectCount; ++i )
			{
				Object o = objects[i];
				if( MatchObject( o, sAssetName, type, nID, sAtlasName ) )
				{
					RefInfo info = new RefInfo( o );
					objectInfo.references.Add( info );
				}
			}
		}

		private Object[] GetObjectsInScene()
		{
			//List<Object> objectInSceneList = new List<Object>();
			//GameObject[] goRootArray = new List<GameObject>(GetSceneRoots()).ToArray();
			//Queue<GameObject> queue = new Queue<GameObject>(goRootArray);
			//while( queue.Count > 0 )
			//{
			//	GameObject current = queue.Dequeue();
			//	if( current != null )
			//	{
			//		objectInSceneList.Add( current );
			//		foreach( Transform child in current.transform )
			//			queue.Enqueue( child.gameObject );
			//	}
			//}
			//Object[] objArray = objectInSceneList.ToArray();
			//objectInSceneList = null;
			//return objArray;
			GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>() ;
			return allObjects;
		}

		private void ClearReports()
		{
			Object oRef = null;
			List<RefInfo> refInfoToRemoveList = new List<RefInfo>();
			Debug.Log( "DepFilter " + m_depFilter );
			foreach( ObjectInfo info in m_report )
			{
				refInfoToRemoveList.Clear();
				// Check dependencies
				int nRefInfoCount = info.references.Count;
				Debug.Log( "info " + info.m_ref.m_sName + " dep count " + nRefInfoCount );
				for( int i = 0; i < nRefInfoCount; ++i )
				{
					RefInfo refInfo = info.references[i];
					oRef = EditorUtility.InstanceIDToObject( refInfo.m_nInstanceID );
					switch( m_depFilter )
					{
						case DepFilter.Direct:
							for( int j = 0; j < nRefInfoCount; ++j )
							{
								if( i != j )
								{
									RefInfo refInfoDep = info.references[j];
									Object oDep = EditorUtility.InstanceIDToObject( refInfoDep.m_nInstanceID );
									if( IsDependent( oRef, oDep ) && !IsDependent( oDep, oRef ) )
									{
										refInfoToRemoveList.Add( refInfo );
										break;
									}
								}
							}
							break;
						case DepFilter.Component:
							if( !IsObjectOfType( oRef, typeof( Component ) ) )
								refInfoToRemoveList.Add( refInfo );
							break;
						case DepFilter.DirectComponent:
							if( !IsObjectOfType( oRef, typeof( Component ) ) )
							{
								Debug.Log( "Remove parent component " + refInfo.m_sName );
								refInfoToRemoveList.Add( refInfo );
								continue;
							}
							for( int j = 0; j < nRefInfoCount; ++j )
							{
								if( i != j )
								{
									RefInfo refInfoDep = info.references[j];
									Object oDep = EditorUtility.InstanceIDToObject( refInfoDep.m_nInstanceID );
									if( IsObjectOfType( oDep, typeof( Component ) ) && IsDependent( oRef, oDep ) && !IsDependent( oDep, oRef ) )
									{
										Debug.Log( "Remove dep component " + refInfo.m_sName );
										refInfoToRemoveList.Add( refInfo );
										break;
									}
								}
							}
							break;
					}
				}
				Debug.Log( "info " + info.m_ref.m_sName + " remove dep count " + refInfoToRemoveList.Count );
				// Remove parent references
				foreach( RefInfo refInfo in refInfoToRemoveList )
					info.references.Remove( refInfo );
			}
			refInfoToRemoveList = null;
		}

		private void GrabMemory()
		{
			m_nObjSceneArray = ObjectArrayToIDs( EditorUtility.CollectDependencies( GetObjectsInScene() ) );
			m_nObjMemoryArray = ObjectArrayToIDs( Resources.FindObjectsOfTypeAll( typeof( Object ) ) as Object[] );
		}

		private void ResetMemory()
		{
			m_nObjSceneArray = null;
			m_nObjMemoryArray = null;
		}

		private void CompareMemory()
		{
			if( m_nObjSceneArray == null ) return;

			m_report.Clear();

			int[] nObjSceneArray = ObjectArrayToIDs( EditorUtility.CollectDependencies( GetObjectsInScene() ) );
			int[] nObjMemoryArray = ObjectArrayToIDs( Resources.FindObjectsOfTypeAll( typeof( Object ) ) as Object[] );

			int nObjCount = 0;

			// Leak ?
			ObjectInfo leakInfo = new ObjectInfo( "Possible Leak" );
			m_report.Add( leakInfo );

			nObjCount = m_nObjSceneArray.Length;
			for( int i = 0; i < nObjCount; ++i )
			{
				int nID = m_nObjSceneArray[i];
				if( IsInsideArray<int>( nID, nObjMemoryArray ) && !IsInsideArray<int>( nID, nObjSceneArray ) )
				{
					Object o = EditorUtility.InstanceIDToObject( nID );
					if( o is MonoScript ) continue;
					leakInfo.references.Add( new RefInfo( o ) );
				}
			}

			// New ?
			ObjectInfo newInfo = new ObjectInfo( "New objects" );
			m_report.Add( newInfo );

			nObjCount = nObjMemoryArray.Length;
			for( int i = 0; i < nObjCount; ++i )
			{
				int nID = nObjMemoryArray[i];
				if( !IsInsideArray<int>( nID, m_nObjMemoryArray ) )
				{
					newInfo.references.Add( new RefInfo( EditorUtility.InstanceIDToObject( nID ) ) );
				}
			}

			// Removed ?
			//ObjectInfo removedInfo = new ObjectInfo( "Removed objects" );
			//m_report.Add( removedInfo );

			//nObjCount = m_nObjMemoryArray.Length;
			//for( int i = 0; i<nObjCount; ++i )
			//{
			//	int nID = m_nObjMemoryArray[i];
			//	if( !IsInsideArray<int>( nID, nObjMemoryArray ) )
			//	{
			//		Object o = EditorUtility.InstanceIDToObject( nID );
			//		if( o!=null )
			//			removedInfo.references.Add( new RefInfo( o ) );
			//		else
			//			removedInfo.references.Add( new RefInfo( "ID " + nID ) );
			//	}
			//}

			nObjSceneArray = null;
			nObjMemoryArray = null;
		}

		private bool IsDependent( Object oParent, Object oDep )
		{
			Object[] oDepArray = EditorUtility.CollectDependencies( new Object[] { oParent } );
			foreach( Object o in oDepArray )
			{
				if( o == oDep )
					return true;
			}
			return false;
		}

		private bool IsInsideArray<T>( T tAsset, T[] tArray )
		{
			int nCount = tArray.Length;
			for( int i = 0; i < nCount; ++i )
			{
				if( EqualityComparer<T>.Default.Equals( tAsset, tArray[i] ) )
					return true;
			}
			return false;
		}

		private static bool FieldReferencesComponent<T>( Object obj, FieldInfo fieldInfo, T mb ) where T : Object
		{
			if( fieldInfo.FieldType.IsClass && fieldInfo.FieldType.IsGenericType && fieldInfo.FieldType.IsAutoLayout && !fieldInfo.FieldType.IsArray )
			{
				object val = fieldInfo.GetValue(obj);
				if( val != null )
				{
					if( val is IList )
					{
						var arr = val as IList;
						if( arr != null )
						{
							foreach( object elem in arr )
							{
								if( elem != null && elem.GetType() == mb.GetType() )
								{
									var o = elem as T;
									if( o == mb )
										return true;
								}
							}
						}
					}
					else if( val is ICollection )
					{
						var arr = val as ICollection;
						if( arr != null )
						{
							foreach( object elem in arr )
							{
								if( elem != null && elem.GetType() == mb.GetType() )
								{
									var o = elem as T;
									if( o == mb )
										return true;
								}
							}
						}
					}
					else if( val is IDictionary )
					{
						var arr = val as IDictionary;
						if( arr != null )
						{
							foreach( object elem in arr.Keys )
							{
								if( elem != null && elem.GetType() == mb.GetType() )
								{
									var o = elem as T;
									if( o == mb )
										return true;
								}
							}
							foreach( object elem in arr.Values )
							{
								if( elem != null && elem.GetType() == mb.GetType() )
								{
									var o = elem as T;
									if( o == mb )
										return true;
								}
							}
						}
					}
				}
			}
			else if( fieldInfo.FieldType.IsArray )
			{
				var arr = fieldInfo.GetValue(obj) as Array;
				if( arr != null )
				{
					foreach( object elem in arr )
					{
						if( elem != null && elem.GetType() == mb.GetType() )
						{
							var o = elem as T;
							if( o == mb )
								return true;
						}
					}
				}
			}
			else
			{
				if( fieldInfo.FieldType == mb.GetType() )
				{
					var o = fieldInfo.GetValue(obj) as T;
					if( o == mb )
						return true;
				}
			}
			return false;
		}

		private bool IsObjectOfType( Object o, Type type )
		{
			return o.GetType() == type || o.GetType().IsSubclassOf( type );
		}

		private bool MatchObject( Object o, string sAssetName, Type type, int nID, string sAtlasName = null )
		{
			return (string.IsNullOrEmpty( sAssetName ) || o.name.Contains( sAssetName ))
				&& (type == null || IsObjectOfType( o, type ))
				&& (nID == 0 || nID == o.GetInstanceID())
				&& (string.IsNullOrEmpty( sAtlasName ) || CheckAtlas( o, sAtlasName ) || CheckGameObjectAtlas( o, sAtlasName ));
		}

		private bool CheckAtlas( Object o, string sAtlasName )
		{
			if( o != null && o is Texture )
			{
				string texturePath = AssetDatabase.GetAssetPath( o.GetInstanceID() );
				AssetImporter assetImpoter = AssetImporter.GetAtPath( texturePath );
				TextureImporter textureImporter = assetImpoter as TextureImporter;
				if( textureImporter != null )
					return textureImporter.spritePackingTag.Contains( sAtlasName );
			}
			return false;
		}

		private bool CheckGameObjectAtlas( Object o, string sAtlasName )
		{
			if( o is GameObject )
			{
				GameObject go = (GameObject)o;
				bool bActive = go.activeSelf;
				if( !bActive ) go.SetActive( true );
				CanvasRenderer canvasRenderer = go.GetComponent<CanvasRenderer>();
				if( canvasRenderer != null )
				{
					Material mat = canvasRenderer.GetMaterial();
					if( mat != null )
					{
						if( CheckAtlas( mat.mainTexture, sAtlasName ) )
							return true;
					}
				}
				Renderer[] array = go.GetComponents<Renderer>();
				if( array != null )
				{
					int nCount = array.Length;
					for( int i = 0; i < nCount; ++i )
					{
						Material[] matArray = array[i].sharedMaterials;
						for( int j = 0; j < matArray.Length; ++j )
						{
							if( matArray[j] != null && CheckAtlas( matArray[j].mainTexture, sAtlasName ) )
								return true;
						}
					}
				}
				if( !bActive ) go.SetActive( false );
			}
			return false;
		}

		private int[] ObjectArrayToIDs( Object[] oArray )
		{
			int nCount = oArray.Length;
			int[] nArray = new int[nCount];
			for( int i = 0; i < nCount; ++i )
			{
				nArray[i] = oArray[i].GetInstanceID();
			}
			return nArray;
		}

		private static IEnumerable<GameObject> GetSceneRoots()
		{
			HierarchyProperty hierarchyProperty = new HierarchyProperty(HierarchyType.GameObjects);
			int[] dummy = new int[0];
			while( hierarchyProperty.Next( dummy ) )
			{
				yield return hierarchyProperty.pptrValue as GameObject;
			}
		}
#endregion

#region Attributes
		private List<ObjectInfo> m_report;
		private Vector2 m_reportScrollPosition;
		private ObjectInfoComparer m_objectInfoComparer;
		private string m_sTypeName = string.Empty;
		private string m_sObjectName = string.Empty;
		private string m_sAtlasName = string.Empty;
		private ObjectInfo m_selectedNode;
		private int[] m_nObjSceneArray;
		private int[] m_nObjMemoryArray;
		private Type m_currentType;
		private int m_nCurrentID;
		private DepFilter m_depFilter = DepFilter.All;
#endregion

#endregion
	}
}
