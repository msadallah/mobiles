﻿namespace AllMyScripts.Common.Utils.Editor
{
    using UnityEngine;
	using UnityEngine.UI;
	using UnityEditor;
    using TMPro;
    using System.Reflection;

    public class CheckAllPrefabs : EditorWindow
    {
        [MenuItem("AllMyScripts/Tools/Check All Prefabs Tool")]
        static void Init()
        {
            CheckAllPrefabs window = (CheckAllPrefabs)EditorWindow.GetWindow(typeof(CheckAllPrefabs));
            window.Show();
        }

        private string m_sCheckTypeName = string.Empty;
        private string m_sCheckMemberName = string.Empty;

        private void OnGUI()
        {
            if (GUILayout.Button("Check Scale On All Prefabs"))
            {
                CheckScaleOnAllPrefabsAtPath(new string[] { "Assets" }, false);
            }

            if (GUILayout.Button("Patch Scale On All Prefabs"))
            {
                CheckScaleOnAllPrefabsAtPath(new string[] { "Assets" }, true);
            }

            if (GUILayout.Button("Check Scale On Selection"))
            {
                CheckScaleOnAllPrefabsInSelection(false);
            }

            if (GUILayout.Button("Patch Scale On Selection"))
            {
                CheckScaleOnAllPrefabsInSelection(true);
            }

            GUILayout.Label("-----------------------------------------");

            if (GUILayout.Button("Check Texts On All Prefabs"))
            {
                CheckTextsOnAllPrefabsAtPath(new string[] { "Assets" }, false);
            }

            if (GUILayout.Button("Check Texts On Selection"))
            {
                CheckTextsOnAllPrefabsInSelection(false);
            }

            GUILayout.Label("-----------------------------------------");

            GUILayout.BeginHorizontal();
            GUILayout.Label("Type name", GUILayout.Width(100f));
            m_sCheckTypeName = GUILayout.TextField(m_sCheckTypeName);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Member name", GUILayout.Width(100f));
            m_sCheckMemberName = GUILayout.TextField(m_sCheckMemberName);
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Check null ref On All Prefabs"))
            {
                CheckNullRefOnAllPrefabsAtPath(new string[] { "Assets" });
            }

            if (GUILayout.Button("Check null ref On Selection"))
            {
                CheckNullRefOnAllPrefabsInSelection();
            }

            GUILayout.Label("-----------------------------------------");

            if (GUILayout.Button("Check missing ref On All Prefabs"))
            {
                CheckMissingRefOnAllPrefabsAtPath(new string[] { "Assets" });
            }

            if (GUILayout.Button("Check missing ref On Selection"))
            {
                CheckMissingRefOnAllPrefabsInSelection();
            }

            GUILayout.Label("-----------------------------------------");

            if (GUILayout.Button("Check canvas On All Prefabs"))
            {
                CheckCanvasOnAllPrefabsAtPath(new string[] { "Assets" }, false);
            }

            if (GUILayout.Button("Patch canvas On All Prefabs"))
            {
                CheckCanvasOnAllPrefabsAtPath(new string[] { "Assets" }, true);
            }

            if (GUILayout.Button("Check canvas On selection"))
            {
                CheckCanvasOnAllPrefabsInSelection(false);
            }

            if (GUILayout.Button("Patch canvas On selection"))
            {
                CheckCanvasOnAllPrefabsInSelection(true);
            }

			GUILayout.Label( "-----------------------------------------" );

			if( GUILayout.Button( "Check Mask ref On All Prefabs" ) )
			{
				CheckMaskOnAllPrefabsAtPath( new string[] { "Assets" } );
			}

			if( GUILayout.Button( "Check Mask ref On Selection" ) )
			{
				CheckMaskOnAllPrefabsInSelection();
			}

			GUILayout.Label( "-----------------------------------------" );

			if( GUILayout.Button( "Re-Apply All Prefabs" ) )
			{
				ReApplyAllPrefabsAtPath( new string[] { "Assets" } );
			}

			if( GUILayout.Button( "Re-Apply Prefabs On Selection" ) )
			{
				ReApplyAllPrefabsInSelection();
			}

			GUILayout.Label( "-----------------------------------------" );
		}

        private void CheckScaleOnAllPrefabsAtPath(string[] sPathArray, bool bReplace)
        {
            if (bReplace)
            {
                AssetDatabase.StartAssetEditing();
            }

            var guids = AssetDatabase.FindAssets(" t:GameObject", sPathArray);
            foreach (var g in guids)
            {
                CheckScaleOnPrefabWithGUID(g, bReplace);
            }

            if (bReplace)
            {
                AssetDatabase.StopAssetEditing();
                AssetDatabase.SaveAssets();
            }
        }

        private void CheckScaleOnAllPrefabsInSelection(bool bReplace)
        {
            if (bReplace)
            {
                AssetDatabase.StartAssetEditing();
            }

            foreach (var g in Selection.assetGUIDs)
            {
                CheckScaleOnPrefabWithGUID(g, bReplace);
            }

            if (bReplace)
            {
                AssetDatabase.StopAssetEditing();
                AssetDatabase.SaveAssets();
            }
        }

        private void CheckScaleOnPrefabWithGUID(string sGUID, bool bReplace)
        {
            string path = AssetDatabase.GUIDToAssetPath(sGUID);
            GameObject prefab = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
            CheckScaleOnPrefab(prefab, bReplace);
        }

        private bool CheckScaleOnPrefab(GameObject go, bool bReplace)
        {
            bool bReplaced = false;

            if (go != null)
            {
                Transform[] trArray = go.GetComponentsInChildren<Transform>();
                foreach (Transform tr in trArray)
                {
                    if ((tr.localScale.x != 1f && Mathf.Approximately(tr.localScale.x, 1f)) ||
                        (tr.localScale.y != 1f && Mathf.Approximately(tr.localScale.y, 1f)) ||
                        (tr.localScale.z != 1f && Mathf.Approximately(tr.localScale.z, 1f)))
                    {
                        Debug.Log("Duplicate object in " + go.name + " at " + tr.name + " child! (" + tr.localScale.x + "," + tr.localScale.y + "," + tr.localScale.z + ")");
                        if (bReplace)
                        {
                            tr.localScale = Vector3.one;
                            bReplaced = true;
                        }
                    }
                    if (tr.localScale.z == 0f && (tr.localScale.x + tr.localScale.y + tr.localScale.z >= 2f))
                    {
                        Debug.LogWarning("Bad scale in " + go.name + " at " + tr.name + " child! (" + tr.localScale.x + "," + tr.localScale.y + "," + tr.localScale.z + ")");
                        if (bReplace)
                        {
                            tr.localScale = Vector3.one;
                            bReplaced = true;
                        }
                    }
                }
            }

            return bReplaced;
        }


        private void CheckTextsOnAllPrefabsAtPath(string[] sPathArray, bool bReplace)
        {
            if (bReplace)
            {
                AssetDatabase.StartAssetEditing();
            }

            var guids = AssetDatabase.FindAssets(" t:GameObject", sPathArray);
            foreach (var g in guids)
            {
                CheckTextsOnPrefabWithGUID(g, bReplace);
            }

            if (bReplace)
            {
                AssetDatabase.StopAssetEditing();
                AssetDatabase.SaveAssets();
            }
        }

        private void CheckTextsOnAllPrefabsInSelection(bool bReplace)
        {
            if (bReplace)
            {
                AssetDatabase.StartAssetEditing();
            }

            foreach (var g in Selection.assetGUIDs)
            {
                CheckTextsOnPrefabWithGUID(g, bReplace);
            }

            if (bReplace)
            {
                AssetDatabase.StopAssetEditing();
                AssetDatabase.SaveAssets();
            }
        }

        private void CheckTextsOnPrefabWithGUID(string sGUID, bool bReplace)
        {
            string path = AssetDatabase.GUIDToAssetPath(sGUID);
            GameObject prefab = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
            CheckTextsOnPrefab(prefab, bReplace);
        }

        private bool CheckTextsOnPrefab(GameObject go, bool bReplace)
        {
            bool bReplaced = false;

            if (go != null)
            {
                TextMeshProUGUI[] textsArray = go.GetComponentsInChildren<TextMeshProUGUI>();
                foreach (TextMeshProUGUI tm in textsArray)
                {
                    if (tm.fontSize % 1f != 0f)
                    {
                        Debug.Log("Text Font Size not valid in " + go.name + " at " + tm.name + " size " + tm.fontSize, go);
                        if (bReplace)
                        {
                            bReplaced = true;
                        }
                    }
                    //if( tm.fontStyle == FontStyles.Bold )
                    //{
                    //	Debug.Log( "Text Font Type is in BOLD in " + go.name + " at " + tm.name + " use Roboto Bold instead!", go );
                    //	if( bReplace )
                    //	{
                    //		bReplaced = true;
                    //	}
                    //}
                }
            }

            return bReplaced;
        }


        private void CheckNullRefOnAllPrefabsAtPath(string[] sPathArray)
        {
            CheckNullRefOnAllPrefabsInList(AssetDatabase.FindAssets(" t:GameObject", sPathArray));
        }

        private void CheckNullRefOnAllPrefabsInSelection()
        {
            CheckNullRefOnAllPrefabsInList(Selection.assetGUIDs);
        }

        private void CheckNullRefOnAllPrefabsInList(string[] list)
        {
            if (list != null)
            {
                int nTotal = list.Length;
                for (int i = 0; i < nTotal; ++i)
                {
                    string sGUID = list[i];
                    EditorUtility.DisplayProgressBar("CheckNullRef", "GUID " + sGUID, (float)i / (float)nTotal);
                    CheckNullRefOnPrefabWithGUID(sGUID);
                }
                EditorUtility.ClearProgressBar();
            }
        }

        private void CheckNullRefOnPrefabWithGUID(string sGUID)
        {
            string path = AssetDatabase.GUIDToAssetPath(sGUID);
            GameObject prefab = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
            CheckNullRefOnPrefab(prefab);
        }

        private void CheckNullRefOnPrefab(GameObject go)
        {
            if (go != null)
            {
                MonoBehaviour[] scripts = go.GetComponentsInChildren<MonoBehaviour>(true);
                foreach (var script in scripts)
                {
                    if (script != null)
                    {
                        FieldInfo[] myFieldInfo = script.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                        for (int i = 0; i < myFieldInfo.Length; ++i)
                        {
                            FieldInfo info = myFieldInfo[i];
                            if (string.IsNullOrEmpty(m_sCheckTypeName) || info.FieldType.FullName.Contains(m_sCheckTypeName))
                            {
                                if (string.IsNullOrEmpty(m_sCheckMemberName) || info.Name.Contains(m_sCheckMemberName))
                                {
                                    if ((info.IsPublic && !info.IsNotSerialized) || (info.IsPrivate && info.GetCustomAttribute(typeof(SerializeField)) != null))
                                    {
                                        object obj = info.GetValue(script);
                                        if (obj == null)
                                            Debug.Log("Null ref on " + go.name + " - script " + script + " - info " + info);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Debug.Log("Missing ref on game object " + go.name);
                    }
                }
            }
        }

        private void CheckMissingRefOnAllPrefabsAtPath(string[] sPathArray)
        {
            CheckMissingRefOnAllPrefabsInList(AssetDatabase.FindAssets(" t:GameObject", sPathArray));
        }

        private void CheckMissingRefOnAllPrefabsInSelection()
        {
            CheckMissingRefOnAllPrefabsInList(Selection.assetGUIDs);
        }

        private void CheckMissingRefOnAllPrefabsInList(string[] list)
        {
            if (list != null)
            {
                int nTotal = list.Length;
                for (int i = 0; i < nTotal; ++i)
                {
                    string sGUID = list[i];
                    EditorUtility.DisplayProgressBar("CheckMissingRef", "GUID " + sGUID, (float)i / (float)nTotal);
                    CheckMissingRefOnPrefabWithGUID(sGUID);
                }
                EditorUtility.ClearProgressBar();
            }
        }

        private void CheckMissingRefOnPrefabWithGUID(string sGUID)
        {
            string path = AssetDatabase.GUIDToAssetPath(sGUID);
            GameObject prefab = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
            CheckMissingRefOnPrefab(prefab);
        }

        private void CheckMissingRefOnPrefab(GameObject go)
        {
            if (go != null)
            {
                Component[] components = go.GetComponentsInChildren<Component>(true);
                foreach (var component in components)
                {
                    if (component != null)
                    {
                        FieldInfo[] myFieldInfo = component.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                        for (int i = 0; i < myFieldInfo.Length; ++i)
                        {
                            FieldInfo info = myFieldInfo[i];

                            if ((info.IsPublic && !info.IsNotSerialized) || (info.IsPrivate && info.GetCustomAttribute(typeof(SerializeField)) != null))
                            {
                                if (typeof(Object).IsAssignableFrom(info.FieldType))
                                {
                                    Object obj = (Object)info.GetValue(component);
                                    if (obj == null)
                                    {
                                        SerializedObject so = new SerializedObject(component);
                                        SerializedProperty sp = so.GetIterator();
                                        while (sp.NextVisible(true))
                                        {
                                            if (sp.name == info.Name)
                                            {
                                                var prop = typeof(SerializedProperty).GetProperty("objectReferenceStringValue", BindingFlags.NonPublic | BindingFlags.Instance);
                                                var result = (string)prop.GetValue(sp, null);
                                                if (result.Contains("Missing"))
                                                    Debug.Log("Missing ref on " + go.name + " - Component " + component + " - info " + info);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Debug.Log("Missing ref on game object " + go.name);
                    }
                }
            }
        }




        private void CheckCanvasOnAllPrefabsAtPath(string[] sPathArray, bool bReplace)
        {
            if (bReplace)
            {
                AssetDatabase.StartAssetEditing();
            }

            CheckCanvasOnAllPrefabsInList(AssetDatabase.FindAssets(" t:GameObject", sPathArray), bReplace);

            if (bReplace)
            {
                AssetDatabase.StopAssetEditing();
                AssetDatabase.SaveAssets();
            }
        }

        private void CheckCanvasOnAllPrefabsInSelection(bool bReplace)
        {
            if (bReplace)
            {
                AssetDatabase.StartAssetEditing();
            }

            CheckCanvasOnAllPrefabsInList(Selection.assetGUIDs, bReplace);

            if (bReplace)
            {
                AssetDatabase.StopAssetEditing();
                AssetDatabase.SaveAssets();
            }
        }

        private void CheckCanvasOnAllPrefabsInList(string[] list, bool bReplace)
        {
            if (list != null)
            {
                int nTotal = list.Length;
                for (int i = 0; i < nTotal; ++i)
                {
                    string sGUID = list[i];
                    EditorUtility.DisplayProgressBar("CheckCanvas", "GUID " + sGUID, (float)i / (float)nTotal);
                    CheckCanvasOnPrefabWithGUID(sGUID, bReplace);
                }
                EditorUtility.ClearProgressBar();
            }
        }

        private void CheckCanvasOnPrefabWithGUID(string sGUID, bool bReplace)
        {
            string path = AssetDatabase.GUIDToAssetPath(sGUID);
            GameObject prefab = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
            CheckCanvasOnPrefab(prefab, bReplace);
        }

        private void CheckCanvasOnPrefab(GameObject go, bool bReplace)
        {
            if (go != null)
            {
                AdditionalCanvasShaderChannels shaderChannels = AdditionalCanvasShaderChannels.TexCoord1 | AdditionalCanvasShaderChannels.TexCoord2 | AdditionalCanvasShaderChannels.TexCoord3;
                Canvas[] components = go.GetComponentsInChildren<Canvas>(true);
                foreach (var component in components)
                {
                    if (component != null)
                    {
                        if ((component.additionalShaderChannels & shaderChannels) != shaderChannels && component.additionalShaderChannels != AdditionalCanvasShaderChannels.None)
                        {
                            Debug.Log("Canvas shader channels not valid in " + go.name + " at " + component.name + " channels " + component.additionalShaderChannels, go);
                            if (bReplace)
                                component.additionalShaderChannels = shaderChannels;
                        }
                    }
                }
            }
        }


		private void CheckMaskOnAllPrefabsAtPath(string[] sPathArray)
        {
            CheckMaskOnAllPrefabsInList(AssetDatabase.FindAssets(" t:GameObject", sPathArray));
        }

        private void CheckMaskOnAllPrefabsInSelection()
        {
            CheckMaskOnAllPrefabsInList(Selection.assetGUIDs);
        }

        private void CheckMaskOnAllPrefabsInList(string[] list)
        {
            if (list != null)
            {
                int nTotal = list.Length;
                for (int i = 0; i < nTotal; ++i)
                {
                    string sGUID = list[i];
                    EditorUtility.DisplayProgressBar("CheckMask", "GUID " + sGUID, (float)i / (float)nTotal);
                    CheckMaskOnPrefabWithGUID(sGUID);
                }
                EditorUtility.ClearProgressBar();
            }
        }

        private void CheckMaskOnPrefabWithGUID(string sGUID)
        {
            string path = AssetDatabase.GUIDToAssetPath(sGUID);
            GameObject prefab = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
            CheckMaskOnPrefab(prefab);
        }

        private void CheckMaskOnPrefab(GameObject go)
        {
            if (go != null)
            {
                Mask[] components = go.GetComponentsInChildren<Mask>(true);
                foreach (var component in components)
                {   
					Debug.Log("Mask ref on " + go.name + " - Component " + component + " - go " + go );
                }
            }
        }

		private void ReApplyAllPrefabsAtPath( string[] sPathArray )
		{
			AssetDatabase.StartAssetEditing();

			var guids = AssetDatabase.FindAssets(" t:GameObject", sPathArray);
			foreach( var g in guids )
			{
				ReApplyPrefabWithGUID( g );
			}

			AssetDatabase.StopAssetEditing();
			AssetDatabase.SaveAssets();
		}

		private void ReApplyAllPrefabsInSelection()
		{
			AssetDatabase.StartAssetEditing();

			foreach( var g in Selection.assetGUIDs )
			{
				ReApplyPrefabWithGUID( g );
			}

			AssetDatabase.StopAssetEditing();
			AssetDatabase.SaveAssets();
		}

		private void ReApplyPrefabWithGUID( string sGUID )
		{
			string path = AssetDatabase.GUIDToAssetPath(sGUID);
			GameObject go = AssetDatabase.LoadAssetAtPath( path, typeof( GameObject ) ) as GameObject;
			go.name = go.name + "";
			Debug.Log( "Re-Apply on prefab " + go.name );
		}
	}
}