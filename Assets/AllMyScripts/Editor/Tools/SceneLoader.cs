﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

// ensure class initializer is called whenever scripts recompile
[InitializeOnLoadAttribute]
public static class PlayModeStateLisener
{
	// register an event handler when the class is initialized
	static PlayModeStateLisener()
	{
		EditorApplication.playModeStateChanged += OnPlayModeState;
	}

	private static void OnPlayModeState(PlayModeStateChange state)
	{
		if (state == PlayModeStateChange.EnteredEditMode)
		{
			if (EditorPrefs.HasKey("SCENE_TO_RELOAD"))
			{
				EditorSceneManager.OpenScene(EditorPrefs.GetString("SCENE_TO_RELOAD"), OpenSceneMode.Single);
				EditorPrefs.DeleteKey("SCENE_TO_RELOAD");
			}
		}
	}
}

public class SceneLoader : EditorWindow
{
	private List<SceneAsset> scenes = new List<SceneAsset>();
	[MenuItem("Window/Scene Accessor")]
	static void Init()
	{


		SceneLoader window = (SceneLoader)EditorWindow.GetWindow(typeof(SceneLoader));
		window.minSize = new Vector2(500, 20);
		window.Show();
	}

	void OnEnable()
	{
		string[] sScenes = EditorPrefs.GetString("SceneLoaderScenes").Split('\n');
		foreach (string s in sScenes)
		{
			SceneAsset asset = AssetDatabase.LoadAssetAtPath<SceneAsset>(s);
			if (asset != null && !scenes.Contains(asset))
			{
				scenes.Add(asset);
			}
		}
	}

	private void OnGUI()
	{
		EditorGUILayout.BeginHorizontal();
		foreach (SceneAsset scene in scenes)
		{
			if (GUILayout.Button("x", GUILayout.Width(20)))
			{
				scenes.Remove(scene);
				UpdateEditorPrefs();
				return;
			}
			if (GUILayout.Button(scene.name))
			{
				EditorSceneManager.OpenScene(AssetDatabase.GetAssetOrScenePath(scene), OpenSceneMode.Single);
			}

			if (GUILayout.Button(">", GUILayout.Width(20)))
			{
				PlaySelectedScene(scene);
				return;
			}
			GUILayout.Space(10);
		}
		SceneAsset asset = (SceneAsset)EditorGUILayout.ObjectField(null, typeof(SceneAsset), false);
		if (asset != null)
		{
			scenes.Add(asset);
			UpdateEditorPrefs();
		}
		EditorGUILayout.EndHorizontal();
	}

	private void PlaySelectedScene(SceneAsset toPlay)
	{
		if (EditorApplication.isPlaying)
		{
			Debug.LogError("This function can not be used in play Mode !");
			return;
		}
		SceneAsset _active = AssetDatabase.LoadAssetAtPath<SceneAsset>(EditorSceneManager.GetActiveScene().path);
		if (_active != toPlay)
		{
			EditorPrefs.SetString("SCENE_TO_RELOAD", EditorSceneManager.GetActiveScene().path);
			EditorSceneManager.OpenScene(AssetDatabase.GetAssetOrScenePath(toPlay), OpenSceneMode.Single);
		}
		EditorApplication.isPlaying = true;
	}
	private void UpdateEditorPrefs()
	{
		string sResult = "";
		foreach (SceneAsset s in scenes)
		{
			sResult += AssetDatabase.GetAssetOrScenePath(s) + "\n";
		}
		sResult = sResult.Remove(sResult.Length - 1);
		EditorPrefs.SetString("SceneLoaderScenes", sResult);
	}
}
