namespace AllMyScripts.Common.Utils.Editor
{
    using UnityEngine;
    using UnityEditor;

    using AllMyScripts.Common.Tools;

    //! @class EnumArrayPropertyDrawer
    //!
    //! @brief Property drawer for a EnumArray class
    [CustomPropertyDrawer( typeof( EnumArrayBaseClass ), true )]
	public class EnumArrayPropertyDrawer : PropertyDrawer
	{
		public override void OnGUI( Rect position, SerializedProperty property, GUIContent label )
		{
			EditorGUI.BeginProperty( position, label, property );

			property.isExpanded = EditorGUI.Foldout( new Rect( position.x, position.y, position.width, 16.0f ), property.isExpanded, label );
			if( property.isExpanded )
			{
				++EditorGUI.indentLevel;

				SerializedProperty valuesArrayProperty = property.FindPropertyRelative( "m_internalArray" );
				SerializedProperty namesArrayProperty = property.FindPropertyRelative( "m_enumNames" );

				float fTop = position.y+16.0f;
				for( int nIndex = 0; nIndex<namesArrayProperty.arraySize; ++nIndex )
				{
					GUIContent elementLabel = new GUIContent( namesArrayProperty.GetArrayElementAtIndex( nIndex ).stringValue );
					SerializedProperty elementProperty = valuesArrayProperty.GetArrayElementAtIndex( nIndex );

					float fPropertyHeight = EditorGUI.GetPropertyHeight( elementProperty, elementLabel );
					Rect elementRect = new Rect( position.x, fTop, position.width, fPropertyHeight ); 
					EditorGUI.PropertyField( elementRect, elementProperty, elementLabel, true );

					fTop += fPropertyHeight;
				}

				--EditorGUI.indentLevel;
			}

			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight( SerializedProperty property, GUIContent label )
		{
			float fHeight = 16.0f;
			if( property.isExpanded )
			{
				SerializedProperty valuesArrayProperty = property.FindPropertyRelative( "m_internalArray" );
				SerializedProperty namesArrayProperty = property.FindPropertyRelative( "m_enumNames" );

				for( int nIndex = 0; nIndex<namesArrayProperty.arraySize; ++nIndex )
				{
					fHeight += EditorGUI.GetPropertyHeight( valuesArrayProperty.GetArrayElementAtIndex( nIndex ), new GUIContent( namesArrayProperty.GetArrayElementAtIndex( nIndex ).stringValue ) );
				}
			}

			return fHeight;
		}
	}
}
