namespace AllMyScripts.Common.UI
{
	using UnityEngine.UI;
	using UnityEditor.UI;
	using UnityEditor;

	/// <summary>
	/// Editor class used to edit UI modified Image.
	/// </summary>
	[CustomEditor( typeof( CustomImage ), true )]
	[CanEditMultipleObjects]
	public class CustomImageEditor : ImageEditor
	{
		SerializedProperty m_baseType;

		protected override void OnEnable()
		{
			base.OnEnable();

            m_baseType = serializedObject.FindProperty("m_Type");
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			CustomImage myImage = (CustomImage)target;
            Image.Type typeEnum = (Image.Type)m_baseType.enumValueIndex;

			if( typeEnum==Image.Type.Filled )
			{
				serializedObject.Update();

				myImage.m_bForceSliced = EditorGUILayout.Toggle( "Force sliced", myImage.m_bForceSliced );

				serializedObject.ApplyModifiedProperties();
			}
		}
	}
}
