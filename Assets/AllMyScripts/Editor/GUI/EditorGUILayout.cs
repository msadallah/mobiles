﻿namespace AllMyScripts.Common.GUI.Editor
{
    using UnityEngine;
    using UnityEditor;

    using AllMyScripts.Common.Tools;
    using AllMyScripts.Common.Tools.Editor;

    //! @class GUILayout
    //!
    //! @brief	static class that holds custom controls using GUI Layouts
    public static class EditorGUILayout
	{
		//! Create a size handle that allow you to resize an element on horizontal or vertical axis
		//!
		//!	@param	fValue			current value of the control (the width or the height) that need to be resized
		//!	@param	bIsHorizontal	whether the control is an horizontal resizer or a vertical one
		//!	@param	fHandleSize		size of the handle for the width or the height depending on bIsHorizontal
		//!
		//!	@return the new value of the control that need to be resized
		public static float SizeHandle( float fValue, bool bIsHorizontal, float fHandleSize = 10.0f )
		{
			GUILayoutOption sizeOption = bIsHorizontal ? GUILayout.Width( fHandleSize ) : GUILayout.Height( fHandleSize );
			GUILayoutOption expandOption = bIsHorizontal ? GUILayout.ExpandHeight( true ) : GUILayout.ExpandWidth( true );
			Rect resizingRect = GUILayoutUtility.GetRect( GUIContent.none, UnityEngine.GUI.skin.box, sizeOption, expandOption );
			return EditorGUI.SizeHandle( resizingRect, fValue, bIsHorizontal );
		}

		//! Create a control that allow you to choose a folder and return its path
		//!
		//!	@param	sLabel				label of the control
		//!	@param	sDirectoryPath		value of the control
		//!	@param	folderType			location type of the folder
		//!	@param	sBrowseWindowTitle	title of the folder selection window
		//!	@param	sBrowseDefaultName	default name of the folder in the folder selection window
		//!
		//!	@return	the path to the directory selected
		public static string FolderSelection( string sLabel, string sDirectoryPath, PathUtil.PathType folderType = PathUtil.PathType.Any, string sBrowseWindowTitle = "Choose a folder", string sBrowseDefaultName = "Folder" )
		{
			UnityEditor.EditorGUILayout.BeginHorizontal( GUILayout.ExpandWidth( true ) );
			Vector2 labelSize = UnityEngine.GUI.skin.label.CalcSize( new GUIContent( sDirectoryPath ) );
			Vector2 buttonSize = UnityEngine.GUI.skin.button.CalcSize( EditorGUI.FOLDER_SELECTION_BUTTON_LABEL );
			float fControlHeight = Mathf.Max( buttonSize.y, labelSize.y );
			Rect controlRect = GUILayoutUtility.GetRect( EditorGUIUtility.labelWidth + buttonSize.x + labelSize.x, EditorGUIUtility.currentViewWidth, fControlHeight, fControlHeight, GUILayout.ExpandWidth( true ), GUILayout.ExpandHeight( false ) );
			UnityEditor.EditorGUILayout.EndHorizontal();

			return EditorGUI.FolderSelection( controlRect, sLabel, sDirectoryPath, folderType, sBrowseWindowTitle, sBrowseDefaultName );
		}

		//! Create a control that allow you to choose a file and return its path
		//!
		//!	@param	sLabel				label of the control
		//!	@param	sFilePath			value of the control
		//!	@param	sFilters				array of filters to populate OpenFilePanelWithFilters method
		//!	@param	fileType			location type of the file
		//!	@param	sBrowseWindowTitle	title of the folder selection window
		//!
		//!	@return	the path to the file selected
		public static string FileSelection( string sLabel, string sFilePath, string[] sFilters, PathUtil.PathType fileType = PathUtil.PathType.Any, string sBrowseWindowTitle = "Choose a file" )
		{
			UnityEditor.EditorGUILayout.BeginHorizontal( GUILayout.ExpandWidth( true ) );
			Vector2 labelSize = UnityEngine.GUI.skin.label.CalcSize( new GUIContent( sFilePath ) );
			Vector2 buttonSize = UnityEngine.GUI.skin.button.CalcSize( EditorGUI.FILE_SELECTION_BUTTON_LABEL );
			float fControlHeight = Mathf.Max( buttonSize.y, labelSize.y );
			Rect controlRect = GUILayoutUtility.GetRect( EditorGUIUtility.labelWidth + buttonSize.x + labelSize.x, EditorGUIUtility.currentViewWidth, fControlHeight, fControlHeight, GUILayout.ExpandWidth( true ), GUILayout.ExpandHeight( false ) );
			UnityEditor.EditorGUILayout.EndHorizontal();

			return EditorGUI.FileSelection( controlRect, sLabel, sFilePath, sFilters, fileType, sBrowseWindowTitle );
		}

		public static EnumFlag<t_Enum> EnumFlag<t_Enum>( string sLabel, EnumFlag<t_Enum> value, params GUILayoutOption[] layoutOptions ) where t_Enum : struct, System.IConvertible
		{
			Rect position = GUILayoutUtility.GetRect( new GUIContent( sLabel ), "MiniPopup", layoutOptions );
			return EditorGUI.EnumFlag<t_Enum>( position, sLabel, value );
		}

		public static int ButtonedPopup(string label, int selectedIndex, string[] displayedOptions, params GUILayoutOption[]  options)
		{

			UnityEditor.EditorGUILayout.BeginHorizontal();
			UnityEditor.EditorGUILayout.LabelField(label);
			if (GUILayout.Button("<", EditorStyles.miniButton, GUILayout.Width(20)))
			{
				selectedIndex--;
				if (selectedIndex < 0) selectedIndex = displayedOptions.Length - 1;
			}
			int nResult = UnityEditor.EditorGUILayout.Popup(selectedIndex, displayedOptions,options);
			if (GUILayout.Button(">", EditorStyles.miniButton, GUILayout.Width(20)))
			{
				nResult++;
				if (nResult >= displayedOptions.Length) nResult = 0;
			}
			UnityEditor.EditorGUILayout.EndHorizontal();
			return nResult;
		}

		public static int ButtonedPopupRect( Rect position, string label, int selectedIndex, string[] displayedOptions )
		{
			Rect labelRect = new Rect( position.x, position.y, 150f, position.height - 2 );
			Rect previousRect = new Rect( labelRect.xMax, position.y, 20f, position.height - 2 );
			Rect popupRect = new Rect( previousRect.xMax - 5f, position.y + 2f, 200f, position.height - 2 );
			Rect nextRect = new Rect( popupRect.xMax + 10f, position.y, 20f, position.height - 2 );

			UnityEditor.EditorGUI.LabelField( labelRect, label );
			if( GUI.Button( previousRect, "<", EditorStyles.miniButton ) )
			{
				selectedIndex--;
				if( selectedIndex < 0 ) selectedIndex = displayedOptions.Length - 1;
			}
			int nResult = UnityEditor.EditorGUI.Popup(popupRect, selectedIndex, displayedOptions );
			if( GUI.Button( nextRect, ">", EditorStyles.miniButton ) )
			{
				nResult++;
				if( nResult >= displayedOptions.Length ) nResult = 0;
			}
			return nResult;
		}
	}
}