﻿namespace AllMyScripts.Common.GUI.Editor
{
    using UnityEngine;
    using UnityEditor;

    using AllMyScripts.Common.Tools;
    using AllMyScripts.Common.Tools.Editor;

    //! @class EditorGUI
    //!
    //! @brief	static class that holds custom controls
    public static class EditorGUI
	{
		//! Create a size handle that allow you to resize an element on horizontal or vertical axis
		//!
		//!	@param	rect			position and size of the control
		//!	@param	fValue			current value of the control (the width or the height) that need to be resized
		//!	@param	bIsHorizontal	whether the control is an horizontal resizer or a vertical one
		//!
		//!	@return the new value of the control that need to be resized
		public static float SizeHandle( Rect rect, float fValue, bool bIsHorizontal )
		{
			int nControlID = GUIUtility.GetControlID( FocusType.Passive );

			MouseCursor mouseCursor = bIsHorizontal ? MouseCursor.ResizeHorizontal : MouseCursor.ResizeVertical;
			EditorGUIUtility.AddCursorRect( rect, mouseCursor );

			switch ( Event.current.GetTypeForControl( nControlID ) )
			{
				case EventType.MouseUp:
					if ( GUIUtility.hotControl == nControlID )
					{
						GUIUtility.hotControl = 0;
					}
					break;
				case EventType.MouseDown:
					if ( rect.Contains( Event.current.mousePosition ) && Event.current.button == 0 )
					{
						GUIUtility.hotControl = nControlID;
					}
					break;
			}

			if ( Event.current.isMouse && GUIUtility.hotControl == nControlID && Event.current.type == EventType.MouseDrag )
			{
				fValue += bIsHorizontal ? Event.current.delta.x : Event.current.delta.y;
				Event.current.Use();
				UnityEngine.GUI.changed = true;
			}

			return fValue;
		}

		//! Create a control that allow you to choose a folder and return its path
		//!
		//!	@param	rect				position and size of the control
		//!	@param	sLabel				label of the control
		//!	@param	sDirectoryPath		value of the control
		//!	@param	eFolderType			location type of the folder
		//!	@param	sBrowseWindowTitle	title of the folder selection window
		//!	@param	sBrowseDefaultName	default name of the folder in the folder selection window
		//!
		//!	@return	the path to the directory selected
		public static string FolderSelection( Rect rect, string sLabel, string sDirectoryPath, PathUtil.PathType eFolderType = PathUtil.PathType.Any, string sBrowseWindowTitle = "Choose a folder", string sBrowseDefaultName = "Folder" )
		{
			Vector2 v2ButtonSize = UnityEngine.GUI.skin.button.CalcSize( FOLDER_SELECTION_BUTTON_LABEL );
			Rect labelRect = new Rect( rect.x, rect.y, rect.width - v2ButtonSize.x, rect.height );
			Rect buttonRect = new Rect( labelRect.x + labelRect.width, rect.y, v2ButtonSize.x, v2ButtonSize.y );
			UnityEditor.EditorGUI.LabelField( labelRect, sLabel, sDirectoryPath );
			if ( UnityEngine.GUI.Button( buttonRect, FOLDER_SELECTION_BUTTON_LABEL ) )
			{
				string sAbsoluteSourceDirectoryPath;
				if ( PathUtil.ToAbsolute( eFolderType, sDirectoryPath, out sAbsoluteSourceDirectoryPath ) == false )
				{
					sAbsoluteSourceDirectoryPath = Application.dataPath;
				}

				string sSelectedAbsoluteDirectoryPath = EditorUtility.OpenFolderPanel( sBrowseWindowTitle, sAbsoluteSourceDirectoryPath, sBrowseDefaultName );
				// if the path is null, the user has cancelled, so we don't do anything
				if ( sSelectedAbsoluteDirectoryPath != null )
				{
					string sResultPath;
					if ( PathUtil.ToRelative( eFolderType, sSelectedAbsoluteDirectoryPath, out sResultPath ) )
					{
						sDirectoryPath = sResultPath;
					}
				}
			}

			return sDirectoryPath;
		}

		//! Create a control that allow you to choose a file and return its path
		//!
		//!	@param	rect				position and size of the control
		//!	@param	sLabel				label of the control
		//!	@param	sFilePath			value of the control
		//!	@param	sFilters			array of filters to populate OpenFilePanelWithFilters method
		//!	@param	eFolderType			location type of the folder
		//!	@param	sBrowseWindowTitle	title of the folder selection window
		//!
		//!	@return	the path to the directory selected
		public static string FileSelection( Rect rect, string sLabel, string sFilePath, string[] sFilters, PathUtil.PathType eFileType = PathUtil.PathType.Any, string sBrowseWindowTitle = "Choose a file" )
		{
			GlobalTools.Assert( sFilters != null && sFilters.Length > 0 && ( sFilters.Length % 2 ) == 0 );

			Vector2 v2ButtonSize = UnityEngine.GUI.skin.button.CalcSize( FOLDER_SELECTION_BUTTON_LABEL );
			Rect labelRect = new Rect( rect.x, rect.y, rect.width - v2ButtonSize.x, rect.height );
			Rect buttonRect = new Rect( labelRect.x + labelRect.width, rect.y, v2ButtonSize.x, v2ButtonSize.y );
			UnityEditor.EditorGUI.LabelField( labelRect, sLabel, sFilePath );
			if ( UnityEngine.GUI.Button( buttonRect, FILE_SELECTION_BUTTON_LABEL ) )
			{
				string sAbsoluteSourceFilePath;
				string sAbsoluteSourceDirectoryPath;
				if ( !string.IsNullOrEmpty( sFilePath ) && PathUtil.ToAbsolute( eFileType, sFilePath, out sAbsoluteSourceFilePath ) )
				{
					sAbsoluteSourceDirectoryPath = System.IO.Path.GetDirectoryName( sAbsoluteSourceFilePath );
				}
				else
				{
					sAbsoluteSourceDirectoryPath = Application.dataPath;
				}

				string sSelectedAbsoluteFilePath = EditorUtility.OpenFilePanelWithFilters( sBrowseWindowTitle, sAbsoluteSourceDirectoryPath, sFilters );
				// if the path is null, the user has cancelled, so we don't do anything
				if ( sSelectedAbsoluteFilePath != null )
				{
					string sResultPath;
					if ( PathUtil.ToRelative( eFileType, sSelectedAbsoluteFilePath, out sResultPath ) )
					{
						sFilePath = sResultPath;
					}
				}
			}

			return sFilePath;
		}

		public static EnumFlag<t_Enum> EnumFlag<t_Enum>( Rect position, string sLabel, EnumFlag<t_Enum> value ) where t_Enum : struct, System.IConvertible
		{
			string[] sNamesArray = System.Enum.GetNames( typeof( t_Enum ) );

			System.Text.StringBuilder sStringizedFlagValues = new System.Text.StringBuilder();
			int nFlagSetCounter = 0;
			for ( int nIndex = 0 ; nIndex < sNamesArray.Length ; nIndex++ )
			{
				if ( value[ nIndex ] )
				{
					if ( nFlagSetCounter == 0 )
					{
						sStringizedFlagValues.Append( '|' );
					}
					sStringizedFlagValues.Append( sNamesArray[ nIndex ] );
					nFlagSetCounter++;
				}
			}

			string sPopupLabel;
			if ( nFlagSetCounter == 0 )
			{
				sPopupLabel = "None";
			}
			else if ( nFlagSetCounter == sNamesArray.Length )
			{
				sPopupLabel = "Everything";
			}
			else if ( nFlagSetCounter > 1 )
			{
				sPopupLabel = "Mixed...";
			}
			else
			{
				sPopupLabel = sStringizedFlagValues.ToString();
			}

			Rect controlRect = UnityEditor.EditorGUI.PrefixLabel( position, new GUIContent( sLabel ) );
			if ( UnityEngine.GUI.Button( controlRect, sPopupLabel, "MiniPopup" ) )
			{
				GenericMenu contextualMenu = new GenericMenu();
				contextualMenu.AddItem( new GUIContent( "None" ), false, new GenericMenu.MenuFunction( () => value.SetAll( false ) ) );
				contextualMenu.AddItem( new GUIContent( "Everything" ), false, new GenericMenu.MenuFunction( () => value.SetAll( true ) ) );
				contextualMenu.AddSeparator( "" );

				for ( int nIndex = 0 ; nIndex < sNamesArray.Length ; nIndex++ )
				{
					int nId = nIndex;
					contextualMenu.AddItem( new GUIContent( sNamesArray[ nIndex ] ), value[ nIndex ], new GenericMenu.MenuFunction( () => value[ nId ] = !value[ nId ] ) );
				}
				contextualMenu.ShowAsContext();
			}

			return value;
		}

#region Private
#region Declarations
		internal static readonly GUIContent FILE_SELECTION_BUTTON_LABEL = new GUIContent( "Browse" );
		internal static readonly GUIContent FOLDER_SELECTION_BUTTON_LABEL = new GUIContent( "Browse" );
#endregion
#endregion
	}
}