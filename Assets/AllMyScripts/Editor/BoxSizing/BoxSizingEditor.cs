﻿namespace AllMyScripts.Common.UI.BoxSizing.Editor
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(BoxSizingTransform))]
    [CanEditMultipleObjects]
    public class BoxSizingEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            RectTransform rt = ((BoxSizingTransform)target).GetComponent<RectTransform>();

            EditorGUILayout.LabelField("Horizontal Fit", EditorStyles.boldLabel);
            //horizontal fit
            EditorGUILayout.PropertyField(serializedObject.FindProperty("horizontalFit"));


            EditorGUILayout.LabelField("Max Width", EditorStyles.boldLabel);
            var maxWidth = serializedObject.FindProperty("maxWidth");
            bool bMaxWidth = maxWidth.intValue >= 0;

            //if bMaxWidth is false, maxWidth is inferior to 0 (in order to keep the value in store)
            bMaxWidth = EditorGUILayout.Toggle("Has max width", bMaxWidth);
            if (!bMaxWidth)
            {
                if (maxWidth.intValue > 0)
                    maxWidth.intValue = -maxWidth.intValue;
            }
            else
            {
                EditorGUI.indentLevel++;
                if (maxWidth.intValue < 0)
                    maxWidth.intValue = -maxWidth.intValue;
                EditorGUILayout.PropertyField(maxWidth);
                EditorGUI.indentLevel--;
            }

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("boxSizingParametersSetting"));


            serializedObject.ApplyModifiedProperties();

        }
    }
}