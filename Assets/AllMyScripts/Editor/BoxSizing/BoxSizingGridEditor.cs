﻿namespace AllMyScripts.Common.UI.BoxSizing.Editor
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(BoxSizingGrid))]
    [CanEditMultipleObjects]
    public class BoxSizingGridEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            BoxSizingGrid bsg = (BoxSizingGrid)target;

            SerializedProperty propUpdateLayoutElement = this.serializedObject.FindProperty("UpdateLayoutElementPreferedHeight");
            SerializedProperty propLayoutElement = this.serializedObject.FindProperty("layoutElement");
            SerializedProperty propFixedHeight = this.serializedObject.FindProperty("fixedHeight");

            this.serializedObject.GetIterator().Next(true);
            SerializedProperty serializedProp = this.serializedObject.GetIterator();

            EditorGUILayout.PropertyField(this.serializedObject.FindProperty("ElementHeightMode"));
            EditorGUILayout.PropertyField(this.serializedObject.FindProperty("ElementSortMode"));
            EditorGUILayout.PropertyField(this.serializedObject.FindProperty("LineSpacing"));

            EditorGUILayout.Space();

            switch (bsg.ElementHeightMode)
            {
                case BoxSizingGrid.BoxSizingGridMode.ChildExpandToHeight:
                    break;
                case BoxSizingGrid.BoxSizingGridMode.FirstElementInRowHeight:

                    EditorGUILayout.PropertyField(propLayoutElement);

                    if (bsg.LayoutElement != null)
                    {
                        EditorGUILayout.PropertyField(propUpdateLayoutElement);
                    }
                    break;
                case BoxSizingGrid.BoxSizingGridMode.FixedHeight:
                    EditorGUILayout.PropertyField(propFixedHeight);

                    EditorGUILayout.Space();

                    EditorGUILayout.PropertyField(propLayoutElement);
                    if (bsg.LayoutElement != null)
                    {
                        EditorGUILayout.PropertyField(propUpdateLayoutElement);
                    }
                    break;
            }

            serializedObject.ApplyModifiedProperties();

        }
    }
}