using UnityEngine;
using System.Collections;
using UnityEditor;

namespace AllMyScripts.Editor
{
	//! @class EditorCoroutine
	//!
	//! @brief	Class to execute coroutine in editor mode
	public class EditorCoroutine
	{
		readonly IEnumerator m_routine;

		public static EditorCoroutine Start( IEnumerator routine )
		{
			EditorCoroutine coroutine = new EditorCoroutine( routine );
			coroutine.Start();
			return coroutine;
		}

		EditorCoroutine( IEnumerator routine )
		{
			m_routine = routine;
		}

		void Start()
		{
			//Debug.Log("start");
			EditorApplication.update += Update;
		}

		public void Stop()
		{
			//Debug.Log("stop");
			EditorApplication.update -= Update;
		}

		void Update()
		{
			/* NOTE: no need to try/catch MoveNext,
				* if an IEnumerator throws its next iteration returns false.
				* Also, Unity probably catches when calling EditorApplication.update.
				*/

			//Debug.Log("update");
			if ( !m_routine.MoveNext() )
			{
				Stop();
			}
		}
	}
}