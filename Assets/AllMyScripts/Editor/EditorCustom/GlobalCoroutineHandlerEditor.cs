﻿namespace AllMyScripts.Common.Tools.Editor
{
	using UnityEditor;
	using UnityEngine;
	using AllMyScripts.Common.Tools;

	[CustomEditor(typeof(CoroutineTools.GlobalCoroutineHandler))]
	public class GlobalCoroutineHandlerInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			EditorGUILayout.LabelField("LOGS:");
			foreach (var log in CoroutineTools.logs)
			{
				EditorGUILayout.LabelField(log);
			}
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("CURRENTS:" + CoroutineTools.routines.Count.ToString());
			foreach( var keyval in CoroutineTools.routines )
			{
				EditorGUILayout.LabelField(keyval.Value.routineKey);
			}			
			EditorUtility.SetDirty(target);
		}
	}
}