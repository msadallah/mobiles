﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace AllMyScripts.Editor
{
	//! @class BuiltInResourcesWindow
	//!
	//! @brief Editor window displaying all editor built-in styles and icons
	//! This code has been adapted from http://wiki.unity3d.com/index.php/Show_Built_In_Resources
	//! @TODO : improve visual and allow code generation into the clipboard for easy use.
	public class lwBuiltInResourcesWindow : EditorWindow
	{
		#region Unity callbacks
		private void OnEnable()
		{
			m_tabSelectionIndex = 0;
			m_filterText = "";
			m_contentScrollPosition = Vector2.zero;

			m_activeContent = new GUIContent( "active" );
			m_inactiveContent = new GUIContent( "inactive" );
		}

		private void OnGUI()
		{
			EditorGUILayout.HelpBox( "Click on the button above the style or the icon you want to copy the way to get it into the clipboard.\nPaste it into your code editor to get it.", MessageType.Info );

			// search filter
			EditorGUILayout.BeginHorizontal();
			{
				m_filterText = EditorGUILayout.TextField( m_filterText, (GUIStyle)"SearchTextField" );
				UnityEngine.GUI.SetNextControlName( "BuiltinResourcesWindow_SearchClearButton" );
				if ( GUILayout.Button( GUIContent.none, (GUIStyle)"SearchCancelButton" ) )
				{
					m_filterText = "";
					UnityEngine.GUI.FocusControl( "BuiltinResourcesWindow_SearchClearButton" );
				}
			}
			EditorGUILayout.EndHorizontal();

			// tab selection
			string[] tabNames = System.Enum.GetNames( typeof( Tab ) );
			int newTabIndex = GUILayout.Toolbar( m_tabSelectionIndex, tabNames );
			if ( newTabIndex != m_tabSelectionIndex )
			{
				m_tabSelectionIndex = newTabIndex;
			}

			m_contentScrollPosition = EditorGUILayout.BeginScrollView( m_contentScrollPosition, false, false, GUILayout.ExpandWidth( true ), GUILayout.ExpandHeight( true ) );
			switch ( m_tabSelectionIndex )
			{
				case (int)Tab.Styles: OnStylesGUI(); break;
				case (int)Tab.Icons: OnIconsGUI(); break;
				default: EditorGUILayout.HelpBox( "Invalid tab index : " + m_tabSelectionIndex, MessageType.Error ); break;
			}
			EditorGUILayout.EndScrollView();
		}

		private void OnStylesGUI()
		{
			int elementCountPerRow = Mathf.FloorToInt( position.width / 250.0f );
			int indexInRow = 0;
			foreach ( GUIStyle style in UnityEngine.GUI.skin.customStyles )
			{
				if ( string.IsNullOrEmpty( m_filterText ) || style.name.Contains( m_filterText ) )
				{
					if ( indexInRow == 0 )
					{
						EditorGUILayout.BeginHorizontal( GUILayout.ExpandWidth( true ) );
					}

					EditorGUILayout.BeginVertical( UnityEngine.GUI.skin.box, GUILayout.Width( 250.0f ) );
					{
						if ( GUILayout.Button( style.name ) )
						{
							CopyText( "(GUIStyle)\"" + style.name + "\"" );
						}
						GUILayout.BeginHorizontal();
						{
							GUILayout.FlexibleSpace();
							GUILayout.Toggle( false, m_inactiveContent, style, GUILayout.Width( 100.0f ) );
							GUILayout.FlexibleSpace();
							GUILayout.Toggle( true, m_activeContent, style, GUILayout.Width( 100.0f ) );
							GUILayout.FlexibleSpace();
						}
						GUILayout.EndHorizontal();
					}
					EditorGUILayout.EndVertical();

					++indexInRow;
					if ( indexInRow >= elementCountPerRow )
					{
						EditorGUILayout.EndHorizontal();
						indexInRow = 0;
					}
				}
			}
			if ( indexInRow > 0 )
			{
				EditorGUILayout.EndHorizontal();
			}
		}

		private void OnIconsGUI()
		{
			if ( m_textureIcons == null )
			{
				EditorUtility.DisplayProgressBar( "Gathering textures", "", 0.0f );
				m_textureIcons = new List<Texture2D>( Resources.FindObjectsOfTypeAll<Texture2D>() );
				int nTextureIndex = 0;
				while ( nTextureIndex < m_textureIcons.Count )
				{
					bool bShouldBeRemoved = string.IsNullOrEmpty( m_textureIcons[ nTextureIndex ].name );
					bShouldBeRemoved |= EditorGUIUtility.FindTexture( m_textureIcons[ nTextureIndex ].name ) == null;

					if ( bShouldBeRemoved )
					{
						m_textureIcons.RemoveAt( nTextureIndex );
					}
					else
					{
						EditorUtility.DisplayProgressBar( "Gathering textures", m_textureIcons[ nTextureIndex ].name, nTextureIndex / (float)m_textureIcons.Count );
						++nTextureIndex;
					}
				}
				EditorUtility.DisplayProgressBar( "Gathering textures", "sorting textures by name", 1.0f );
				m_textureIcons.Sort( ( pA, pB ) => System.String.Compare( pA.name, pB.name, System.StringComparison.OrdinalIgnoreCase ) );
				EditorUtility.ClearProgressBar();
			}

			int elementCountPerRow = Mathf.FloorToInt( position.width / 250.0f );
			int indexInRow = 0;
			for ( int nTextureIndex = 0 ; nTextureIndex < m_textureIcons.Count ; ++nTextureIndex )
			{
				if ( string.IsNullOrEmpty( m_filterText ) || m_textureIcons[ nTextureIndex ].name.Contains( m_filterText ) )
				{
					if ( indexInRow == 0 )
					{
						EditorGUILayout.BeginHorizontal( GUILayout.ExpandWidth( true ) );
					}

					EditorGUILayout.BeginVertical( UnityEngine.GUI.skin.box, GUILayout.Width( 250.0f ) );
					{
						if ( GUILayout.Button( m_textureIcons[ nTextureIndex ].name ) )
						{
							CopyText( "EditorGUIUtility.FindTexture(\"" + m_textureIcons[ nTextureIndex ].name + "\")" );
						}
						GUILayout.BeginHorizontal();
						{
							GUILayout.FlexibleSpace();
							GUILayout.Label( m_textureIcons[ nTextureIndex ], GUILayout.MaxWidth( 200.0f ) );
							GUILayout.FlexibleSpace();
						}
						GUILayout.EndHorizontal();
					}
					EditorGUILayout.EndVertical();

					++indexInRow;
					if ( indexInRow >= elementCountPerRow )
					{
						EditorGUILayout.EndHorizontal();
						indexInRow = 0;
					}
				}
			}

			if ( indexInRow > 0 )
			{
				EditorGUILayout.EndHorizontal();
			}
		}
		#endregion

		#region Private
		#region Declarations
		private enum Tab
		{
			Styles,
			Icons,
		}
		#endregion

		#region Methods
		[MenuItem( "AllMyScripts/Tools/Built-in styles and icons" )]
		private static void ShowWindow()
		{
			EditorWindow.GetWindow<lwBuiltInResourcesWindow>();
		}

		void CopyText( string sText )
		{
			TextEditor editor = new TextEditor();
			editor.text = sText;
			editor.SelectAll();
			editor.Copy();
		}
		#endregion

		#region Attributes
		private string m_filterText;
		private int m_tabSelectionIndex;
		private Vector2 m_contentScrollPosition;

		private GUIContent m_activeContent;
		private GUIContent m_inactiveContent;

		private List<Texture2D> m_textureIcons;
		#endregion
		#endregion
	}
}