namespace AllMyScripts.Editor.Builder.Android
{
	using System;
	using System.IO;
	using System.Linq;
	using System.Xml;
	using UnityEditor;
	using UnityEngine;

	[Serializable]
	public class ManifestOptions
	{
		public enum AndroidPlugin
		{
			//HockeyApp,
			AppCenter,
			AllMyScripts,
			QRCode,
			Firebase,
			Icons,
			NativeCameraGallery,
			BLE,
			Count
		}

		// Plugins
		[SerializeField] private bool[] m_bPlugins = new bool[(int)AndroidPlugin.Count];

		private const string ANDROID = "android";
		private const string ANDROID_NAME = "name";
		private const string ANDROID_ENABLED = "enabled";
		private const string ANDROID_CONFIG = "configChanges";
		private const string ANDROID_VALUE = "value";
		private const string ANDROID_THEME = "theme";
		private const string ANDROID_LABEL = "label";
		private const string ANDROID_REQUIRED = "required";
		private const string ANDROID_AUTHORITIES = "authorities";
		private const string ANDROID_GRANTURI = "grantUriPermissions";
		private const string ANDROID_RESOURCES = "resource";
		private const string ANDROID_INITORDER = "initOrder";
		private const string ANDROID_PROTECTORDER = "protectionLevel";
		private const string ANDROID_EXPORTED = "exported";
		private const string ANDROID_PERMISSIONS = "permission";
		private const string ANDROID_LAUNCHMODE = "launchMode";

		private const string META_DATA = "meta-data";
		private const string INTENT_FILTER = "intent-filter";
		private const string USES_PERMISSION = "uses-permission";
		private const string USES_FEATURE = "uses-feature";
		private const string ACTIVITY_NAME = "activity";
		private const string SERVICE_NAME = "service";
		private const string RECEIVER_NAME = "receiver";
		//private const string ACTION_NAME = "action";
		//private const string CATEGORY_NAME = "category";
		//private const string DATA_NAME = "data";
		//private const string PERMISSION_NAME = "permission";

		//private const string ANDROID_CONFIG_COMMON = "fontScale|keyboard|keyboardHidden|locale|mnc|mcc|navigation|orientation|screenLayout|screenSize|smallestScreenSize|uiMode|touchscreen";

		//private const string ANDROID_THEME_COMMON = "@android:style/Theme.NoTitleBar.Fullscreen";

		private const string UNITY_ACTIVTY = "com.unity3d.player.UnityPlayerActivity";
		private const string OVERRIDE_FIREBASE_ACTIVTY = "com.google.firebase.MessagingUnityPlayerActivity";

		#region ManifestModRegion

		//private void WriteXmlElement( XmlWriter xw, string sElement, Dictionary<string, string> sAttributeDictionary, bool bIsExported = false )
		//{
		//	xw.WriteStartElement( sElement );
		//	foreach( KeyValuePair<string, string> entry in sAttributeDictionary )
		//	{
		//		xw.WriteAttributeString( entry.Key, entry.Value );
		//	}
		//	if( bIsExported )
		//		xw.WriteAttributeString( ANDROID, ANDROID_EXPORTED, null, "true" );
		//	xw.WriteEndElement();
		//}

		private void WriteXmlMetaData(XmlWriter xw, string sName, string sValue)
		{
			xw.WriteStartElement(META_DATA);
			xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, sName);
			xw.WriteAttributeString(ANDROID, ANDROID_VALUE, null, sValue);
			xw.WriteEndElement();
		}

		private void WriteXmlAction(XmlWriter xw, string sName)
		{
			xw.WriteStartElement("action");
			xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, sName);
			xw.WriteEndElement();
		}

		private void WriteXmlCategory(XmlWriter xw, string sName)
		{
			xw.WriteStartElement("category");
			xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, sName);
			xw.WriteEndElement();
		}

		private void WriteXmlActivity(XmlWriter xw, string sName, string sConfig = null, string sTheme = null)
		{
			xw.WriteStartElement(ACTIVITY_NAME);
			xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, sName);
			if (!string.IsNullOrEmpty(sConfig))
				xw.WriteAttributeString(ANDROID, ANDROID_CONFIG, null, sConfig);
			if (!string.IsNullOrEmpty(sTheme))
				xw.WriteAttributeString(ANDROID, ANDROID_THEME, null, sTheme);
			xw.WriteEndElement();
		}

		private void WriteXmlFeature(XmlWriter xw, string sName, string sRequired = "false")
		{
			xw.WriteStartElement(USES_FEATURE);
			xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, sName);
			xw.WriteAttributeString(ANDROID, ANDROID_REQUIRED, null, sRequired);
			xw.WriteEndElement();
		}

		private void WriteXmlPermission(XmlWriter xw, string sName)
		{
			xw.WriteStartElement(USES_PERMISSION);
			xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, sName);
			xw.WriteEndElement();
		}

		//private void WriteXmlService( XmlWriter xw, string sName, bool bEnabled = false )
		//{
		//	xw.WriteStartElement( SERVICE_NAME );
		//	xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, sName);
		//	if( bEnabled ) xw.WriteAttributeString(ANDROID, ANDROID_ENABLED, null, "true" );
		//	xw.WriteEndElement();
		//}

		//private void WriteXmlReceiver( XmlWriter xw, string sName )
		//{
		//	xw.WriteStartElement( RECEIVER_NAME );
		//	xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, sName);
		//	xw.WriteEndElement();
		//}

		//private void WriteXmlReceiverInstall( XmlWriter xw, string sName, bool bForward )
		//{
		//	xw.WriteStartElement( RECEIVER_NAME );
		//	xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, sName);
		//	xw.WriteAttributeString(ANDROID, ANDROID_ENABLED, null, "true");
		//	xw.WriteAttributeString(ANDROID, ANDROID_EXPORTED, null, "true");
		//	WriteXmlIntentFilter(xw, "com.android.vending.INSTALL_REFERRER");
		//	xw.WriteEndElement();
		//}

		private void WriteXmlIntentFilter(XmlWriter xw, string sAction, string sCategory = "", string sPriority = "")
		{
			xw.WriteStartElement(INTENT_FILTER);
			if (!string.IsNullOrEmpty(sPriority))
				xw.WriteAttributeString(ANDROID, "priority", null, sPriority);
			WriteXmlAction(xw, sAction);
			if (!string.IsNullOrEmpty(sCategory))
				WriteXmlCategory(xw, sCategory);
			xw.WriteEndElement();
		}

		#endregion

		public bool Save(string sPath = "Plugins/Android/AndroidManifest.xml")
		{
			string sBundleId = PlayerSettings.applicationIdentifier;
			sPath = Application.dataPath + '/' + sPath;

			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = ("\t");
			settings.Encoding = new System.Text.UTF8Encoding(true);
			settings.ConformanceLevel = ConformanceLevel.Document;
			settings.NewLineOnAttributes = false;

			if (!Directory.Exists(Application.dataPath + "/Plugins/Android/"))
			{
				if (!Directory.Exists(Application.dataPath + "/Plugins/"))
					AssetDatabase.CreateFolder("Assets", "Plugins");

				AssetDatabase.CreateFolder("Assets/Plugins", "Android");
			}

			XmlWriter xw = XmlWriter.Create( sPath, settings );
			xw.WriteStartDocument();
			// manifest
			xw.WriteStartElement("manifest");
			xw.WriteAttributeString("xmlns", "android", null, "http://schemas.android.com/apk/res/android");
			xw.WriteAttributeString("xmlns", "tools", null, "http://schemas.android.com/tools");

			xw.WriteAttributeString("package", "com.unity3d.player");
			xw.WriteAttributeString(ANDROID, "installLocation", null, "preferExternal");
			xw.WriteAttributeString(ANDROID, "versionCode", null, "1");
			xw.WriteAttributeString(ANDROID, "versionName", null, "1.0");

			//sdk requirements
			xw.WriteStartElement("uses-sdk");
			xw.WriteAttributeString(ANDROID, "minSdkVersion", null, ((int)PlayerSettings.Android.minSdkVersion).ToString());
			int nAndroidTargetSdkVersion = (int)PlayerSettings.Android.targetSdkVersion;
			if (nAndroidTargetSdkVersion == 0)
				nAndroidTargetSdkVersion = Enum.GetValues(typeof(AndroidSdkVersions)).Cast<int>().Max();
			xw.WriteAttributeString(ANDROID, "targetSdkVersion", null, nAndroidTargetSdkVersion.ToString());
			xw.WriteEndElement(); // sdk requirements

			// supports-screens
			xw.WriteStartElement("supports-screens");
			xw.WriteAttributeString(ANDROID, "smallScreens", null, "true");
			xw.WriteAttributeString(ANDROID, "normalScreens", null, "true");
			xw.WriteAttributeString(ANDROID, "largeScreens", null, "true");
			xw.WriteAttributeString(ANDROID, "xlargeScreens", null, "true");
			xw.WriteAttributeString(ANDROID, "anyDensity", null, "true");
			xw.WriteEndElement(); // supports-screens

			// application
			xw.WriteStartElement("application");

			//Special case : we override the icons with our .aar
			if (GetPlugin(AndroidPlugin.Icons))
			{
#if UNITY_2019_3_OR_NEWER
				//xw.WriteAttributeString(ANDROID, "icon", null, "@mipmap/ic_launcher");
#else
				xw.WriteAttributeString(ANDROID, "icon", null, "@mipmap/ic_launcher");
#endif
				xw.WriteAttributeString(ANDROID, "roundIcon", null, "@mipmap/ic_launcher_round");
			}
			else
			{
				xw.WriteAttributeString(ANDROID, "icon", null, "@drawable/app_icon");
			}

			xw.WriteAttributeString(ANDROID, ANDROID_LABEL, null, "@string/app_name");
			xw.WriteAttributeString(ANDROID, "theme", null, "@style/UnityTransparentStatusBarTheme");

			// MAIN ACTIVITY
			// activity : UnityPlayerActivity
			if (GetPlugin(AndroidPlugin.Firebase))
				xw.WriteComment(AndroidPlugin.Firebase + " Override MainActivity");

			xw.WriteStartElement(ACTIVITY_NAME);
			xw.WriteAttributeString(ANDROID, ANDROID_LABEL, null, "@string/app_name");

			if (GetPlugin(AndroidPlugin.Firebase))
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, OVERRIDE_FIREBASE_ACTIVTY);
			else
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, UNITY_ACTIVTY);
			// intent-filter
			string sCategory = PlayerSettings.Android.androidTVCompatibility ? "android.intent.category.LEANBACK_LAUNCHER" : "android.intent.category.LAUNCHER";
			WriteXmlIntentFilter(xw, "android.intent.action.MAIN", sCategory);

			if (GetPlugin(AndroidPlugin.AllMyScripts))
			{
				xw.WriteComment(AndroidPlugin.AllMyScripts.ToString());
				// WriteXmlIntentFilter(xw, "android.nfc.action.TECH_DISCOVERED");
				// WriteXmlIntentFilter(xw, "android.nfc.action.NDEF_DISCOVERED"); //, "android.intent.category.DEFAULT");
				// WriteXmlIntentFilter(xw, "android.nfc.action.TAG_DISCOVERED"); //, "android.intent.category.DEFAULT");
				WriteXmlMetaData(xw, "unityplayer.SkipPermissionsDialog", "true");

				xw.WriteComment("Most Android device must support the layoutInDisplayCutoutMode setup in activity theme");
				//xw.WriteComment("Huawei notch support");
				//WriteXmlMetaData(xw, "android.notch_support","true");

				xw.WriteComment("Xiaomi notch support");
				WriteXmlMetaData(xw, "notch.config", "portrait|landscape");

				// meta data used for NFC triggering
				//xw.WriteStartElement(META_DATA);
				//xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "android.nfc.action.TECH_DISCOVERED");
				//xw.WriteAttributeString(ANDROID, ANDROID_RESOURCES, null, "@xml/filter_nfc");
				//xw.WriteEndElement(); // meta-data
			}

			WriteXmlMetaData(xw, "unityplayer.UnityActivity", "true");

			xw.WriteEndElement(); // activity : UnityPlayerActivity

			// ACTIVITY / SERVICE PART			
			//if (GetPlugin(AndroidPlugin.HockeyApp))
			//{
			//	xw.WriteComment(AndroidPlugin.HockeyApp + " Activities");
			//	WriteXmlActivity(xw, "net.hockeyapp.unity.UpdateActivity");
			//	WriteXmlActivity(xw, "net.hockeyapp.unity.FeedbackActivity");
			//	WriteXmlActivity(xw, "net.hockeyapp.unity.PaintActivity");
			//	WriteXmlActivity(xw, "net.hockeyapp.unity.LoginActivity");
			//	WriteXmlActivity(xw, "net.hockeyapp.unity.ExpiryInfoActivity");
			//}

			if (GetPlugin(AndroidPlugin.NativeCameraGallery))
			{
				// provider
				xw.WriteStartElement("provider");
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "com.yasirkula.unity.NativeCameraContentProvider");
				xw.WriteAttributeString(ANDROID, ANDROID_AUTHORITIES, null, sBundleId + ".nativecameraprovider");
				xw.WriteAttributeString(ANDROID, ANDROID_EXPORTED, null, "false");
				xw.WriteAttributeString(ANDROID, ANDROID_GRANTURI, null, "true");
				xw.WriteEndElement(); // provider
			}

			if (GetPlugin(AndroidPlugin.Firebase))
			{
				xw.WriteComment(AndroidPlugin.Firebase + " Services/Receivers");

				// service
				xw.WriteStartElement(SERVICE_NAME);
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "com.google.firebase.messaging.MessageForwardingService");
				xw.WriteAttributeString(ANDROID, ANDROID_EXPORTED, null, "false");
				xw.WriteEndElement(); // service

				// meta-data
				xw.WriteStartElement(META_DATA);
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "com.google.firebase.messaging.default_notification_icon");
				xw.WriteAttributeString(ANDROID, ANDROID_RESOURCES, null, "@drawable/ic_stat_ic_notification");
				xw.WriteEndElement(); // meta-data
				xw.WriteStartElement(META_DATA);
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "com.google.firebase.messaging.default_notification_color");
#if !USE_FLEXOM
				xw.WriteAttributeString(ANDROID, ANDROID_RESOURCES, null, "@color/colorOthers");
#else
				xw.WriteAttributeString(ANDROID, ANDROID_RESOURCES, null, "@color/colorFlexom");
#endif
				xw.WriteEndElement(); // meta-data

				// provider
				xw.WriteStartElement("provider");
				xw.WriteAttributeString(ANDROID, ANDROID_AUTHORITIES, null, sBundleId + ".firebaseinitprovider");
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "com.google.firebase.provider.FirebaseInitProvider");
				xw.WriteAttributeString(ANDROID, ANDROID_EXPORTED, null, "false");
				xw.WriteAttributeString(ANDROID, ANDROID_INITORDER, null, "100");
				xw.WriteEndElement(); // provider

				// receiver
				xw.WriteStartElement(RECEIVER_NAME);
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "com.google.firebase.iid.FirebaseInstanceIdReceiver");
				xw.WriteAttributeString(ANDROID, ANDROID_EXPORTED, null, "true");
				xw.WriteAttributeString(ANDROID, ANDROID_PERMISSIONS, null, "com.google.android.c2dm.permission.SEND");
				// intentfilter
				WriteXmlIntentFilter(xw, "com.google.android.c2dm.intent.RECEIVE", sBundleId);
				xw.WriteEndElement(); // receiver

				// receiver
				xw.WriteStartElement(RECEIVER_NAME);
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "com.google.firebase.iid.FirebaseInstanceIdInternalReceiver");
				xw.WriteAttributeString(ANDROID, ANDROID_EXPORTED, null, "false");
				xw.WriteEndElement(); // receiver

				// service
				xw.WriteStartElement(SERVICE_NAME);
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "com.google.firebase.iid.FirebaseInstanceIdService");
				xw.WriteAttributeString(ANDROID, ANDROID_EXPORTED, null, "true");
				// intentfilter
				WriteXmlIntentFilter(xw, "com.google.firebase.INSTANCE_ID_EVENT", string.Empty, "-500");
				xw.WriteEndElement(); // service

				// service
				xw.WriteStartElement(SERVICE_NAME);
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "com.google.firebase.messaging.cpp.ListenerService");
				xw.WriteAttributeString(ANDROID, ANDROID_EXPORTED, null, "false");
				// intentfilter
				WriteXmlIntentFilter(xw, "com.google.firebase.MESSAGING_EVENT");
				xw.WriteEndElement(); // service

				// service
				xw.WriteStartElement(SERVICE_NAME);
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "com.google.firebase.messaging.cpp.FcmInstanceIDListenerService");
				xw.WriteAttributeString(ANDROID, ANDROID_EXPORTED, null, "false");
				// intentfilter
				WriteXmlIntentFilter(xw, "com.google.firebase.INSTANCE_ID_EVENT");
				xw.WriteEndElement(); // service

				// service
				xw.WriteStartElement(SERVICE_NAME);
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, "com.google.firebase.messaging.cpp.RegistrationIntentService");
				xw.WriteAttributeString(ANDROID, ANDROID_EXPORTED, null, "false");
				xw.WriteEndElement(); // service

			}

			xw.WriteEndElement(); // application

			// PERMISSIONS PART

			if (GetPlugin(AndroidPlugin.Firebase))
			{
				xw.WriteComment(AndroidPlugin.Firebase + " Permissions");
				WriteXmlPermission(xw, "com.google.android.c2dm.permission.RECEIVE");
				WriteXmlPermission(xw, sBundleId + ".permission.C2D_MESSAGE");
				// permission
				xw.WriteStartElement("permission");
				xw.WriteAttributeString(ANDROID, ANDROID_NAME, null, sBundleId + ".permission.C2D_MESSAGE");
				xw.WriteAttributeString(ANDROID, ANDROID_PROTECTORDER, null, "signature");
				xw.WriteEndElement(); // permission
			}

			if (GetPlugin(AndroidPlugin.QRCode))
			{
				xw.WriteComment(AndroidPlugin.QRCode + " Permissions");
				WriteXmlPermission(xw, "android.permission.INTERNET");
				WriteXmlPermission(xw, "android.permission.ACCESS_NETWORK_STATE");
			}


			if (GetPlugin(AndroidPlugin.NativeCameraGallery))
			{
				xw.WriteComment(AndroidPlugin.NativeCameraGallery + " Permissions");
				WriteXmlPermission(xw, "android.permission.READ_EXTERNAL_STORAGE");
				WriteXmlPermission(xw, "android.permission.WRITE_EXTERNAL_STORAGE");
			}

			if (GetPlugin(AndroidPlugin.AllMyScripts))
			{
				xw.WriteComment(AndroidPlugin.AllMyScripts + " Permissions");
				WriteXmlPermission(xw, "android.permission.NFC");
				WriteXmlFeature(xw, "android.hardware.nfc");
			}

			if (GetPlugin(AndroidPlugin.NativeCameraGallery) || GetPlugin(AndroidPlugin.QRCode))
			{
				xw.WriteComment(AndroidPlugin.NativeCameraGallery + " / " + AndroidPlugin.QRCode + " Permissions");
				WriteXmlPermission(xw, "android.permission.CAMERA");
				WriteXmlFeature(xw, "android.hardware.camera");
				WriteXmlFeature(xw, "android.hardware.camera.autofocus");
			}

			if (GetPlugin(AndroidPlugin.BLE))
			{
				xw.WriteComment(AndroidPlugin.BLE + " Permissions");
				WriteXmlPermission(xw, "android.permission.ACCESS_FINE_LOCATION");
				WriteXmlPermission(xw, "android.permission.ACCESS_COARSE_LOCATION");
				WriteXmlPermission(xw, "android.permission.INTERNET");
				WriteXmlPermission(xw, "android.permission.BLUETOOTH_ADMIN");
				WriteXmlPermission(xw, "android.permission.BLUETOOTH");
				//WriteXmlFeature(xw, "android.hardware.bluetooth");
				//WriteXmlFeature(xw, "android.hardware.bluetooth_le");
			}

			xw.WriteEndElement(); // manifest
			xw.WriteEndDocument();
			xw.Flush();
			xw.Close();

			return true;
		}

		public void SetPlugin(AndroidPlugin ePlugin, bool bEnable = true)
		{
			m_bPlugins[(int)ePlugin] = bEnable;
		}

		public bool GetPlugin(AndroidPlugin ePlugin)
		{
			//Debug.Log( "ePlugin : " + ePlugin + " value associated : " + m_bPlugins[ (int)ePlugin ] );
			if (ePlugin == AndroidPlugin.Count)
				return false;

			return m_bPlugins[(int)ePlugin];
		}

		public void ResetPlugins()
		{
			m_bPlugins = new bool[(int)AndroidPlugin.Count];
			for (int i = 0; i < m_bPlugins.Length; i++)
			{
				m_bPlugins[i] = false;
			}
		}

		public static AndroidPlugin GetAndroidPlugin(string sPluginName)
		{
			sPluginName = sPluginName.ToLower()
							.Replace(" ", "")
							.Replace("-", "")
							.Replace(".", "")
							.Replace(",", "");

			for (int i = 0; i < (int)AndroidPlugin.Count; i++)
			{
				AndroidPlugin androidPlugin = (AndroidPlugin)i;
				if (sPluginName == androidPlugin.ToString().ToLower())
					return androidPlugin;
			}

			return AndroidPlugin.Count;
		}

	}
}