#if UNITY_IOS
namespace AllMyScripts.Editor.Builder.IOS
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using UnityEditor;
    //Unity using part
    using UnityEngine;
    using UnityEditor.iOS.Xcode;

    using AllMyScripts.Common.Tools;
	public static class IOSUtils
	{
		private static readonly string[] IOS_VERSION = { "IOS5,6", "IOS7-9" };

		private static int GetUnityVersionNumber()
		{
			string version = Application.unityVersion;
			string[] versionComponents = version.Split('.');

			int majorVersion = 0;
			int minorVersion = 0;

			try
			{
				if ( versionComponents.Length > 0 && versionComponents[0] != null)
				{
					majorVersion = Convert.ToInt32( versionComponents[0] );
				}

				if ( versionComponents.Length > 1 && versionComponents[1] != null)
				{
					minorVersion = Convert.ToInt32( versionComponents[1] );
				}
			}
			catch ( Exception e )
			{
				Debug.LogError("Error parsing Unity version number: " + e);
			}

			return ( majorVersion * 100 ) + ( minorVersion * 10 );
		}

		public static void PatchRegisterMonoModules( string path )
		{
			int versionNumber = GetUnityVersionNumber();

			string fullPath = IOUtility.CombineMultiplePaths( path, "Libraries", "RegisterMonoModules.h" );
			FileInfo projectFileInfo = new FileInfo( fullPath );
			StreamReader fs = projectFileInfo.OpenText();
			string data = fs.ReadToEnd();
			fs.Close();

			if ( versionNumber >= 430 )
			{
				data += "\n#define HAS_UNITY_VERSION_DEF 1\n";
			}
			else
			{
				data += "\n#define UNITY_VERSION ";
				data += versionNumber;
				data += "\n";
			}

			StreamWriter writer = new StreamWriter( fullPath, false );
			writer.Write(data);
			writer.Close();
		}

		/// <summary>
		/// Return splash position in splash array. NOT IN USE ANYMORE
		/// </summary>
		/// <param name="sFileName">Splash name</param>
		/// <returns>splash position in splash array</returns>
		public static int GetSplashIndex( string sFileName )
		{
			for ( int i = 0; i < Constants.IOS_SPLASH_SIZES.Length; i++ )
			{
				if ( sFileName.Contains( Constants.IOS_SPLASH_SIZES[i] ) )
				{
					return i;
				}
			}
			return -1;
		}

		/// <summary>
		/// Test if the splash exists in splashes array
		/// </summary>
		/// <param name="sFileName">Splash name</param>
		/// <returns>Return true if exists. False otherwise</returns>
		public static bool HasSplashSize( ref string sFileName )
		{
			for ( int i = 0; i < Constants.IOS_SPLASH_SIZES.Length; i++ )
			{
				if ( sFileName.Contains( Constants.IOS_SPLASH_SIZES[i] ) )
				{
					sFileName = Constants.IOS_SPLASH_SIZES[i];
					return true;
				}
			}
			return false;
		}

		public static string GetIconName(int nIconSize)
		{
			return "Icon-" + nIconSize + ".png";
		}

		public static string GetXCodeProjectName()
		{
#if !UNITY_2019_3_OR_NEWER
			return PBXProject.GetUnityTargetName(); //Unity drop this support and provide not even one method to replace this...
#else
			return "Unity-iPhone"; //HACK : Verify if this impact any step during the build process
#endif
		}
		
		public static void XCodePatcher( Profile buildProfile, string sPath, string sPlugins )
		{
			string sProjPath = PBXProject.GetPBXProjectPath(sPath);
			PBXProject xcodeProj = new PBXProject();

			xcodeProj.ReadFromString(File.ReadAllText(sProjPath));
#if UNITY_2019_3_OR_NEWER
			string sTargetGUID = xcodeProj.GetUnityMainTargetGuid();
#else
			string sTargetGUID = xcodeProj.TargetGuidByName(PBXProject.GetUnityTargetName());
#endif

			//Particular build settings properties
			xcodeProj.SetBuildProperty( sTargetGUID, "GCC_ENABLE_OBJC_EXCEPTIONS", "yes" );
			xcodeProj.SetBuildProperty( sTargetGUID, "ENABLE_BITCODE", "false" );

			string[] sBundleArray = PlayerSettings.applicationIdentifier.Split ('.');
			string sLastPart = sBundleArray[sBundleArray.Length-1];
			string sEntitlementsName = sLastPart + ".entitlements";
			string sCompleteEntitlementPath = IOUtility.CombinePaths ( sPath, sEntitlementsName );

			if (File.Exists (sCompleteEntitlementPath))
				File.Delete (sCompleteEntitlementPath);

			PlistDocument emptyDoc = new PlistDocument ();
			emptyDoc.Create ();
			emptyDoc.WriteToFile (sCompleteEntitlementPath);

			xcodeProj.AddFile (sCompleteEntitlementPath, sEntitlementsName);
#if !UNITY_2019_3_OR_NEWER
			ProjectCapabilityManager capabilityMgr = new ProjectCapabilityManager(sProjPath, sEntitlementsName, PBXProject.GetUnityTargetName());
#else
			ProjectCapabilityManager capabilityMgr = new ProjectCapabilityManager(sProjPath, sEntitlementsName, targetGuid : xcodeProj.GetUnityMainTargetGuid());
#endif
			Dictionary<string, string[]> dicPlugins = new Dictionary<string, string[]>();
			dicPlugins.Add("FIREBASE", new string[] { "CoreMotion_AddressBook_StoreKit_SystemConfiguration", "1_0_0_0", "libz_libsqlite3_libc++", "PushNotifications_BackgroundModes" });
			dicPlugins.Add("HOCKEYAPP", new string[] { "AssetsLibrary_CoreText_MobileCoreServices_QuickLook_Security_Photos", "0_0_0_0_0_0	", "libz", "" });
			dicPlugins.Add("NATIVECAMERAGALLERY", new string[] { "ImageIO_MobileCoreServices_Photos", "0_0_0", "", "" });
			dicPlugins.Add("BLE", new string[] { "CoreBluetooth", "1", "", "BackgroundModes" });

			Dictionary<string, string> sDicParameters = new Dictionary<string, string>();
   			string[] sArraySplit = sPlugins.Split( new char[] { '%', '=' } );
            for (int nParamIndex = 0; nParamIndex < sArraySplit.Length; nParamIndex += 2)
                sDicParameters.Add(sArraySplit[nParamIndex], sArraySplit[nParamIndex + 1]);

			BackgroundModesOptions flags = BackgroundModesOptions.None;

			foreach (var value in dicPlugins) 
			{
				if (sDicParameters.ContainsKey (value.Key)) 
				{
					for (int i = 0; i < value.Value.Length; ++i) 
					{
						if (!string.IsNullOrEmpty (value.Value [i])) 
						{ 
							if (i == 0) { //Frameworks
								string[] sFrameworks = value.Value [i].Split ('_');
								string[] sFrameworkAreWeak = value.Value [i + 1].Split ('_');
								for (int j = 0; j < sFrameworks.Length; ++j)
									xcodeProj.AddFrameworkToProject (sTargetGUID, sFrameworks [j] + ".framework", ParseTools.ParseBoolSafe (sFrameworkAreWeak [j]));
								i++;
							} 
							else if (i == 2) 
							{
								string[] sLibraries = value.Value [i].Split ('_');
								for (int j = 0; j < sLibraries.Length; ++j)
									xcodeProj.AddFileToBuild (sTargetGUID, xcodeProj.AddFile ("usr/lib/" + sLibraries [j] + ".tbd", "Frameworks/" + sLibraries [j] + ".tbd", PBXSourceTree.Sdk));

							} 
							else if (i == 3) 
							{
								string[] sCapabilities = value.Value [i].Split ('_');
								for (int j = 0; j < sCapabilities.Length; ++j) 
								{
									if (sCapabilities[j].ToUpper() == "PUSHNOTIFICATIONS")
									{
										capabilityMgr.AddPushNotifications(EditorUserBuildSettings.development);
										xcodeProj.AddCapability(sTargetGUID, PBXCapabilityType.PushNotifications, sCompleteEntitlementPath);
									}
									else if (sCapabilities[j].ToUpper() == "BACKGROUNDMODES")
									{
										switch (value.Key)
										{
											case "FIREBASE":
												flags |= BackgroundModesOptions.RemoteNotifications;
												break;
											case "BLE":
												flags |= BackgroundModesOptions.UsesBluetoothLEAccessory;
												flags |= BackgroundModesOptions.ActsAsABluetoothLEAccessory;
												break;
										}
									}
								}
							}
						}
					}

					if (value.Key == "HOCKEYAPP")
						InsertCodeIntoControllerClass (sPath);

					if (value.Key == "FIREBASE")
					{
						string sFilePath = string.Empty;
						if (sDicParameters.TryGetValue("FIREBASE", out sFilePath))
						{
                            string sXCodePathFile = IOUtility.CombinePaths( sPath, "GoogleService-Info.plist" );
                            File.Copy(sFilePath, sXCodePathFile);
							xcodeProj.AddFileToBuild(sTargetGUID, xcodeProj.AddFile(sXCodePathFile, "GoogleService-Info.plist" ));
							xcodeProj.AddBuildProperty(sTargetGUID, "OTHER_LDFLAGS", "-ObjC");
						}
					}

					if ( value.Key == "BLE" )
					{
						string filePath = Path.Combine( sPath, "Info.plist" );
						PlistDocument plist = new PlistDocument();
						plist.ReadFromString( File.ReadAllText( filePath ) );
						PlistElementDict rootDict = plist.root;

						// Get or create array to manage device capabilities
						const string capsKey = "UIRequiredDeviceCapabilities";
						PlistElementArray capsArray;
						PlistElement element;
						capsArray = rootDict.values.TryGetValue( capsKey, out element ) ? element.AsArray() : rootDict.CreateArray( capsKey );
						// Add the bluetooth-le
						capsArray.AddString( "bluetooth-le" );

						// Get or create array to manage background modes
						const string backgroundModesKey = "UIBackgroundModes";
						PlistElementArray backgroundModesArray;
						backgroundModesArray = rootDict.values.TryGetValue( backgroundModesKey, out element ) ? element.AsArray() : rootDict.CreateArray( backgroundModesKey );
						backgroundModesArray.AddString( "bluetooth-central" );
						
						File.WriteAllText( filePath, plist.WriteToString() );
					}
				}
			}

			if ( flags != BackgroundModesOptions.None )
				capabilityMgr.AddBackgroundModes( flags );

			capabilityMgr.WriteToFile();
			File.WriteAllText( sProjPath, xcodeProj.WriteToString() );
		}

		enum Position { Begin, End };

		private static string rn = "\n";

		private static string PATH_AUTH = "/Classes/UnityAppController.mm";
		private static string SIGNATURE_AUTH = "- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation";
		private static string CODE_AUTH = rn + "if([HockeyAppUnity handleOpenURL:url sourceApplication:sourceApplication annotation:annotation]){" + rn + "return YES;" + rn + "}" + rn;
		private static string CODE_LIB_IMPORT = "#import \"HockeyAppUnity.h\"" + rn;

		private static void InsertCodeIntoControllerClass(string projectPath)
		{
			string filepath = projectPath + PATH_AUTH;
			string[] methodSignatures = { SIGNATURE_AUTH };
			string[] valuesToAppend = { CODE_AUTH };
			Position[] positionsInMethod = new Position[] { Position.Begin };

			InsertCodeIntoClass(filepath, methodSignatures, valuesToAppend, positionsInMethod);
		}

		private static void InsertCodeIntoClass(string filepath, string[] methodSignatures, string[] valuesToAppend, Position[] positionsInMethod)
		{
			if (!File.Exists(filepath))
			{
				return;
			}

			string fileContent = File.ReadAllText(filepath);
			List<int> ignoredIndices = new List<int>();

			for (int i = 0; i < valuesToAppend.Length; i++)
			{
				string val = valuesToAppend[i];

				if (fileContent.Contains(val))
				{
					ignoredIndices.Add(i);
				}
			}

			string[] fileLines = File.ReadAllLines(filepath);
			List<string> newContents = new List<string>();
			bool found = false;
			int foundIndex = -1;

			newContents.Add(CODE_LIB_IMPORT);
			foreach (string line in fileLines)
			{
				if (line.Trim().Contains(CODE_LIB_IMPORT.Trim()))
				{
					continue;
				}

				newContents.Add(line + rn);
				for (int j = 0; j < methodSignatures.Length; j++)
				{
					if ((line.Trim().Equals(methodSignatures[j])) && !ignoredIndices.Contains(j))
					{
						foundIndex = j;
						found = true;
					}
				}

				if (found)
				{
					if ((positionsInMethod[foundIndex] == Position.Begin) && line.Trim().Equals("{"))
					{
						newContents.Add(valuesToAppend[foundIndex] + rn);
						found = false;
					}
					else if ((positionsInMethod[foundIndex] == Position.End) && line.Trim().Equals("}"))
					{
						newContents = newContents.GetRange(0, newContents.Count - 1);
						newContents.Add(valuesToAppend[foundIndex] + rn + "}" + rn);
						found = false;
					}
				}
			}
			string output = string.Join("", newContents.ToArray());
			File.WriteAllText(filepath, output);
		}
	}
}
#endif
