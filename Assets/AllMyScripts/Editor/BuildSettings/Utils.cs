namespace AllMyScripts.Editor.Builder
{
    using System;
    using System.IO;
    using System.Text;
    using System.Threading;
    using UnityEditor;
    using UnityEngine;
    using System.Collections.Generic;

    using AllMyScripts.Common.Tools;
    using AllMyScripts.Common.Tools.Xml;

    public static class Utils
    {
        public static List<Thread> m_ThreadsRunning = new List<Thread>();
        public static List<Thread> m_ThreadsToRun = new List<Thread>();
        public static string m_sLastProcessMessage = string.Empty;


        #region Threaded
        public static Thread ThreadProcess(BuilderModel buildModel, string sExecutable, string sArguments, string sSubPath, bool bStartThread = true, bool bPoolThread = false, bool bStandardExecution = false, bool bJoin = true)
        {
            string sAssetPath = Constants.GetAssetsPath();
            if (Application.platform == RuntimePlatform.WindowsEditor)
                sExecutable = '"' + sExecutable + '"';
            Thread t = new Thread(() => RunProcess(buildModel, sExecutable, sArguments, sSubPath, bStandardExecution, true, sAssetPath));

            if (bStartThread)
            {
                t.Start();
                while (t.ThreadState == ThreadState.Unstarted) ;
                if (bPoolThread)
                    m_ThreadsRunning.Add(t);
                else if (bJoin)
                    t.Join();
            }
            else if (bPoolThread)
                m_ThreadsToRun.Add(t);

            return t;
        }

        public static void RunProcess(BuilderModel buildModel, string executable, string arguments, string subPath, bool bStandardExecution = false, bool bWaitForExit = false, string sAssetPath = "", bool bRunAsAdmin = false)
        {
            string sDebugInfo = "Running Process '" + executable + " " + arguments + "'";
            if (!string.IsNullOrEmpty(subPath))
                sDebugInfo += " in '" + subPath + "'";
            sDebugInfo += ".";
            if (bWaitForExit)
                sDebugInfo += "..";

            buildModel.UpdateLogs(sDebugInfo);

            System.Diagnostics.Process p = StartProcess(executable, arguments, subPath, bStandardExecution, sAssetPath, bRunAsAdmin);

            if (!bWaitForExit)
                return;

            if (!bStandardExecution)
            {
                StringBuilder output = new StringBuilder();
                string sOutput = String.Empty;
                while (!p.HasExited)
                {
                    output.Append(p.StandardOutput.ReadToEnd());
                    sOutput = output.ToString();
#if UNITY_IOS
                    m_sLastProcessMessage = sOutput;
#endif
                    buildModel.UpdateLogs(sOutput);
                    buildModel.UpdateLogs(p.StandardError.ReadToEnd(), MessageType.Warning, false, true);
                }
            }

            p.Close();
            buildModel.UpdateLogs(executable + " process complete.");
        }

        public static System.Diagnostics.Process StartProcess(string executable, string arguments, string subPath, bool bStandardExecution, string sAssetPath = "", bool bRunAsAdmin = false)
        {
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = executable;
            p.StartInfo.Arguments = arguments;
            p.StartInfo.RedirectStandardError = !bStandardExecution;
            p.StartInfo.RedirectStandardOutput = !bStandardExecution;
            p.StartInfo.CreateNoWindow = true;
            if (bRunAsAdmin)
                p.StartInfo.Verb = "runas";
            p.StartInfo.WorkingDirectory = Path.IsPathRooted(subPath) ? subPath : IOUtility.CombineMultiplePaths(sAssetPath, "..", subPath);
            p.StartInfo.UseShellExecute = bStandardExecution;
            p.Start();
            return p;
        }

        private static void DeleteFile(string sPath)
        {
            File.SetAttributes(sPath, FileAttributes.Normal);

            string[] files = Directory.GetFiles(sPath);
            string[] dirs = Directory.GetDirectories(sPath);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in dirs)
            {
                DeleteFile(dir);
            }

            Directory.Delete(sPath, false);
        }

        public static Thread ThreadDelete(string sPath, bool bStartThread = true, bool bPoolThread = false)
        {
            Thread t = new Thread(() => DeleteFile(sPath));

            if (bStartThread)
            {
                t.Start();
                while (t.ThreadState == ThreadState.Unstarted) ;

                if (bPoolThread)
                    m_ThreadsRunning.Add(t);
                else
                    t.Join();
            }
            else if (bPoolThread)
                m_ThreadsToRun.Add(t);

            return t;
        }

        public static Thread ThreadMove(BuilderModel buildModel, string sSource, string sDestination, bool bDirectory, bool bStartThread = true, bool bPoolThread = false)
        {
            Thread t = new Thread(() => MoveFile(buildModel, sSource, sDestination, bDirectory));

            if (bStartThread)
            {
                t.Start();
                while (t.ThreadState == ThreadState.Unstarted) ;

                if (bPoolThread)
                    m_ThreadsRunning.Add(t);
                else
                    t.Join();
            }
            else if (bPoolThread)
                m_ThreadsToRun.Add(t);

            return t;
        }

        private static void MoveFile(BuilderModel model, string sSource, string sDestination, bool bDirectory)
        {
            if (bDirectory)
                Directory.Move(sSource, sDestination);
            else
                File.Move(sSource, sDestination);

            model.UpdateLogs("Moved '" + sSource + "' to '" + sDestination + "'.");
        }



        #endregion
        #region ProjectUtils
        public static bool IsPluginExist(List<string> sPluginsPath, string sPlugin)
        {
            sPlugin = sPlugin.ToLower();
            for (int i = 0; i < sPluginsPath.Count; i++)
                if (sPluginsPath[i].ToLower().Contains(sPlugin))
                    return true;
            return false;
        }

        public static string GetAndroidSdkToolsFolder(bool bWantBuildToolsFolder = false)
        {
            string sRoot = EditorPrefs.GetString("AndroidSdkRoot");
            if (!bWantBuildToolsFolder)
            {
                string sTools = IOUtility.ReplacePathSeparator(Path.Combine(sRoot, "build-tools"));
                if (Directory.Exists(sTools))
                {
                    string[] sDirs = Directory.GetDirectories(sTools);
                    if (sDirs.Length > 0)
                        return sDirs[0] + Path.DirectorySeparatorChar;
                }
            }
            else
            {
                string sTools = Path.Combine(sRoot, "tools");
                if (Directory.Exists(sTools))
                    return sTools;
            }
            Debug.LogError("Couldn't locate Android SDK tools folder !");
            return null;
        }

        public static string GetProjectFolder()
        {
            string sProjectFolder = IOUtility.ReplacePathSeparator(Constants.GetAssetsPath());

            sProjectFolder = sProjectFolder.Substring(0, sProjectFolder.LastIndexOf(Path.DirectorySeparatorChar));
            return sProjectFolder.Substring(0, sProjectFolder.LastIndexOf(Path.DirectorySeparatorChar));
        }

        public static string GetBuildFolder()
        {
            return IOUtility.CombinePaths(GetProjectFolder(), "Build");
        }

        public static void DirectoryRecursiveCreation(string sDir, bool bEditorAsset)
        {
            sDir = Path.GetDirectoryName(sDir);
            if (sDir == null ||
                Directory.Exists(sDir))
                return;

            if (!bEditorAsset)
            {
                Directory.CreateDirectory(sDir);
                return;
            }

            DirectoryRecursiveCreation(sDir, true);

            sDir = IOUtility.ReplacePathSeparator(sDir);
            int nStartIndex = sDir.IndexOf(Path.DirectorySeparatorChar + "Assets" + Path.DirectorySeparatorChar, StringComparison.Ordinal) + 1;
            if (nStartIndex <= 0)
                return;

            int nMiddleIndex = sDir.LastIndexOf(Path.DirectorySeparatorChar);
            if (sDir.Length - 2 < nMiddleIndex)
                return;

            AssetDatabase.CreateFolder(sDir.Substring(nStartIndex, nMiddleIndex - nStartIndex), sDir.Substring(nMiddleIndex + 1));
        }

        public static List<string> GetPluginsPath(string sCurrentPath, bool bFirstIteration = true)
        {
            List<string> sFoldersPath = new List<string>();
            string sFullPath = IOUtility.CombineMultiplePaths(Constants.GetAssetsPath(), sCurrentPath);
            bool bPlugin = false;

            if (Directory.Exists(sFullPath))
            {
                string sFolderName;
                foreach (string sFolderPath in Directory.GetDirectories(sFullPath))
                {
                    sFolderName = IOUtility.GetFolderName(sFolderPath);
                    if (sFolderName != ".svn")
                    {
                        if (sCurrentPath.ToLower().Contains("android"))
                        {
                            if (sFolderName == "bin" || sFolderName == "libs" || sFolderName == "res" || sFolderName == "src")
                            {
                                if (bFirstIteration || bPlugin || sFolderName == "src")
                                    continue;
                                sFoldersPath.Add(sCurrentPath);
                                bPlugin = true;
                            }
                            else
                            {
                                string sPluginPath = IOUtility.CombineMultiplePaths(sCurrentPath, sFolderName);
                                sFoldersPath.AddRange(GetPluginsPath(sPluginPath, false));
                            }
                        }
                        else if (sCurrentPath.ToLower().Contains("ios"))
                        {
                            string sPluginPath = IOUtility.CombineMultiplePaths(sCurrentPath, sFolderName);
                            sFoldersPath.Add(sPluginPath);
                        }
                    }
                }
            }
            return sFoldersPath;
        }

		public static void AddMaskItem(string sPlatform, string sValue, BitMaskList maskList)
		{
			int nMask = 0;
			if (string.IsNullOrEmpty(sPlatform))
			{
				nMask = (1 << ((int)Constants.BUILDER_TARGETS_LIST.Length)) - 1;
			}
			else
			{
				sPlatform = sPlatform.ToUpper();
				for (int i = 0; i < Constants.BUILDER_TARGETS_LIST.Length; i++)
				{
					if (sPlatform.Contains(Constants.BUILDER_TARGETS_LIST[i].m_sTargetName))
						nMask |= 1 << i;
				}
			}

			BitMaskItem item = new BitMaskItem(sValue, nMask);
			maskList.Add(item);
		}

		public static void WriteMaskPlatformList(ref XmlWriter xw, BitMaskList MaskListToWrite, string sHeader)
		{
			int nFull = (1 << Constants.BUILDER_TARGETS_LIST.Length) - 1;

			foreach (BitMaskItem item in MaskListToWrite)
			{
				if (item.nMask == nFull)
				{
					TestAndWriteAttribute(ref xw, sHeader, item.sValue);
				}
				else
				{
					string sPlatform = "";
					for (int i = 0; i < Constants.BUILDER_TARGETS_LIST.Length; i++)
					{
						if ((item.nMask & (1 << i)) != 0)
						{
							if (!sPlatform.Contains(Constants.BUILDER_TARGETS_LIST[i].m_sTargetName))
							{
								if (sPlatform != "")
									sPlatform += "|";
								sPlatform += Constants.BUILDER_TARGETS_LIST[i].m_sTargetName;
							}
						}
					}
					WritePlatformElement(ref xw, sHeader, item.sValue, sPlatform);
				}
			}
		}

		public static void WritePlatformElement(ref XmlWriter xw, string sName, string sValue, string sPlatform)
		{
			if (!string.IsNullOrEmpty(sValue))
			{
				xw.WriteStartElement(sName);
				xw.WriteAttribute("value", sValue);
				if (sPlatform != "") xw.WriteAttribute("platform", sPlatform);
				xw.WriteEndElement();
			}
		}

		public static void TestAndWriteAttribute(ref XmlWriter xw, string sName, string sValue)
		{
			if (!string.IsNullOrEmpty(sValue))
			{
				xw.WriteStartElement(sName);
				xw.WriteAttribute("value", sValue);
				xw.WriteEndElement();
			}
		}
		#endregion
	}
}
