namespace AllMyScripts.Editor.Builder.PList
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using System.Xml;
	using System.Xml.Linq;

	public class PListParser
	{
		//private const string LSApplicationQueriesSchemesKey = "LSApplicationQueriesSchemes";
		//private const string NSAppTransportSecurityKey = "NSAppTransportSecurity";
		//private const string NSExceptionDomainsKey = "NSExceptionDomains";
		//private const string NSIncludesSubdomainsKey = "NSIncludesSubdomains";
		//private const string NSExceptionRequiresForwardSecrecyKey = "NSExceptionRequiresForwardSecrecy";
		private const string CFBundleURLTypesKey = "CFBundleURLTypes";
		//private const string CFBundleURLSchemesKey = "CFBundleURLSchemes";
		//private const string CFBundleURLName = "CFBundleURLName";

		private readonly string m_sFilePath;

		public PListParser(string fullPath)
		{
			m_sFilePath = fullPath;
			XmlReaderSettings settings = new XmlReaderSettings();
#if NET_4_6
			settings.DtdProcessing = DtdProcessing.Parse;
#else
			settings.ProhibitDtd = false;
#endif
			XmlReader plistReader = XmlReader.Create(m_sFilePath, settings);

			XDocument doc = XDocument.Load(plistReader);
			XElement plist = doc.Element("plist");
			XElement dict = plist.Element("dict");
			this.XMLDict = new PListDict(dict);
			plistReader.Close();
		}

		public PListDict XMLDict { get; set; }

		public void WriteToFile()
		{
			// Corrected header of the plist
			string publicId = "-//Apple//DTD PLIST 1.0//EN";
			string stringId = "http://www.apple.com/DTDs/PropertyList-1.0.dtd";
			string internalSubset = null;
			XDeclaration declaration = new XDeclaration("1.0", Encoding.UTF8.EncodingName, null);
			XDocumentType docType = new XDocumentType("plist", publicId, stringId, internalSubset);

			XMLDict.Save(m_sFilePath, declaration, docType);
		}

		//private static void SetCFBundleURLSchemes(PListDict plistDict,string appID)
		//{
		//	IList<object> currentSchemas;
		//	if ( ContainsKeyWithValueType( plistDict, PListParser.CFBundleURLTypesKey, typeof( IList<object> ) ) )
		//	{
		//		currentSchemas = (IList<object>)plistDict[ PListParser.CFBundleURLTypesKey ];
		//	}
		//	else
		//	{
		//		// Didn't find any CFBundleURLTypes, let's create one
		//		currentSchemas = new List<object>();
		//		plistDict[ PListParser.CFBundleURLTypesKey ] = currentSchemas;
		//	}
		//}

		public void SetOneKeyOneValue(string sKey, object oValue)
		{
			GetKeyEntrySingleValue(sKey);
			XMLDict[sKey] = oValue;
		}

		public void SetOneKeyMultipleValue(string sKey, List<object> oValue)
		{
			GetKeyEntryMultipleValue(sKey);
			XMLDict[sKey] = oValue;
		}

		public bool DeleteKeyEntry(string sKey)
		{
			if (!XMLDict.ContainsKey(sKey))
				return false;
			XMLDict.Remove(sKey);
			return true;
		}

		private object GetKeyEntrySingleValue(string sKey)
		{
			object currentSchemas = null;
			if (ContainsKeyWithValueType(this.XMLDict, sKey, typeof(object)))
			{
				currentSchemas = this.XMLDict[sKey];
			}
			else
			{
				currentSchemas = new object();
				this.XMLDict.Add(sKey, currentSchemas);
			}
			return currentSchemas;
		}

		private IList<object> GetKeyEntryMultipleValue(string sKey)
		{
			IList<object> currentSchemas = null;
			if (ContainsKeyWithValueType(this.XMLDict, sKey, typeof(IList<object>)))
			{
				currentSchemas = (IList<object>)this.XMLDict[sKey];
			}
			else
			{
				// Didn't find any CFBundleURLTypes, let's create one
				currentSchemas = new List<object>();
				this.XMLDict.Add(sKey, currentSchemas);
			}
			return currentSchemas;
		}

		//private static void AddAppID(ICollection<object> schemesCollection, string appID, string sPrefix = "")
		//{
		//	string modifiedID = sPrefix + appID;
		//	schemesCollection.Add((object)modifiedID);
		//}

		//private static void AddAppLinkSchemes(ICollection<object> schemesCollection, ICollection<string> appLinkSchemes)
		//{
		//	foreach (var appLinkScheme in appLinkSchemes)
		//	{
		//		schemesCollection.Add(appLinkScheme);
		//	}
		//}

		private static bool ContainsKeyWithValueType(IDictionary<string, object> dictionary, string key, Type type)
		{
			if (dictionary.ContainsKey(key) &&
				type.IsInstanceOfType(dictionary[key]))
			{
				return true;
			}
			return false;
		}
	}
}
