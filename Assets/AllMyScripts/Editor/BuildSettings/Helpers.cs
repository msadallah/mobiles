using UnityEditor;

namespace AllMyScripts.Editor.Builder
{
	public static class Helpers
	{
		public static string GetApplicationIdentifier()
		{
			return PlayerSettings.GetApplicationIdentifier( EditorUserBuildSettings.selectedBuildTargetGroup );
		}

		public static void SetApplicationIdentifier( string sId )
		{
			PlayerSettings.SetApplicationIdentifier( EditorUserBuildSettings.selectedBuildTargetGroup, sId );
		}
	}
}
