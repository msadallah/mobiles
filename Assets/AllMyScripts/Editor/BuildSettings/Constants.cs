namespace AllMyScripts.Editor.Builder
{
	using UnityEditor;
	using UnityEngine;

	using AllMyScripts.Common.Tools;
	[System.Serializable]
	public sealed class Target
	{
		public BuildTarget m_eBuildTarget { get; private set; } = BuildTarget.NoTarget;
		public BuildTargetGroup m_eBuildTargetGroup { get; private set; } = BuildTargetGroup.Unknown;
		public string m_sTargetName { get; private set; } = string.Empty;

		public Target(Target targetToCopy)
		{
			m_eBuildTarget = targetToCopy.m_eBuildTarget;
			m_eBuildTargetGroup = targetToCopy.m_eBuildTargetGroup;
			m_sTargetName = targetToCopy.m_sTargetName;
		}

		public Target(BuildTarget eBuildTarget, BuildTargetGroup eBuildTargetGroup, string sTargetName)
		{
			m_eBuildTarget = eBuildTarget;
			m_eBuildTargetGroup = eBuildTargetGroup;
			m_sTargetName = sTargetName;
		}
	}

	public static class Constants
	{
		public static readonly string DEFAULT_BUILD_SETTINGS = "Default";

		// RegEx version string : accepts A.BB.CCC or A.BB.CCC.DDD (only numbers)
		public const string REGEX_VERSION_STRING = @"^\d{1}\.\d{1,2}\.\d{1,3}?(\.\d{1,3})?$";

		// RegEx hidden folder : match when there is a folder starting with a . in the path
		public static readonly string REGEX_HIDDEN_FOLDER = @"\" + System.IO.Path.DirectorySeparatorChar + @"\..*\" + System.IO.Path.DirectorySeparatorChar;

		// Assets path for thread purpose
		public static string GetAssetsPath()
		{
			return Application.dataPath;
		}

		// Profiles path
		public static string GetProfilesPath()
		{
			string sProjectFolder = IOUtility.ReplacePathSeparator(Constants.GetAssetsPath());
			return IOUtility.ReplacePathSeparator(sProjectFolder + "/Build/Config/Profiles/");
		}

		public static string GetPluginConfigFilesPath()
		{
			string sProjectFolder = IOUtility.ReplacePathSeparator(Constants.GetAssetsPath());
			return IOUtility.ReplacePathSeparator(sProjectFolder + "/Build/Config/PluginConfigs/");
		}

		public static readonly Target[] BUILDER_TARGETS_LIST =
		{
			//Windows Target
			new Target( BuildTarget.StandaloneWindows, BuildTargetGroup.Standalone, "STANDALONE" ),
			new Target( BuildTarget.StandaloneWindows64, BuildTargetGroup.Standalone, "STANDALONE" ),
			//OSX Target
			new Target( BuildTarget.StandaloneOSX, BuildTargetGroup.Standalone, "STANDALONE" ),
			//Linux Targets
#if !UNITY_2019_2_OR_NEWER
			new Target( BuildTarget.StandaloneLinux, BuildTargetGroup.Standalone, "STANDALONE" ),
			new Target( BuildTarget.StandaloneLinuxUniversal, BuildTargetGroup.Standalone, "STANDALONE" ),
#endif
			new Target( BuildTarget.StandaloneLinux64, BuildTargetGroup.Standalone, "STANDALONE" ),
			//Mobile Targets
			new Target( BuildTarget.iOS, BuildTargetGroup.iOS, "IOS" ),
			new Target( BuildTarget.Android, BuildTargetGroup.Android, "ANDROID" ),
			//Web Target
			new Target( BuildTarget.WebGL, BuildTargetGroup.WebGL, "WEBGL" )
		};

		public static Target GetBuilderTarget(BuildTarget buildTarget)
		{
			for (int i = 0; i < BUILDER_TARGETS_LIST.Length; i++)
			{
				if (BUILDER_TARGETS_LIST[i].m_eBuildTarget == buildTarget)
					return BUILDER_TARGETS_LIST[i];
			}
			return null;
		}

		//Company name
		public const string COMPANY_NAME = "Liris";
		public const string XCASSETS_ICONS_PATH = "Images.xcassets/AppIcon.appiconset";
		public const string XCASSETS_SPLASHS_PATH = "Images.xcassets/LaunchImage.launchimage";

		// iOS Splash screens
		public static readonly string[] IOS_SPLASH_SIZES = { "320x480", "640x960", "640x1136", "768x1024", "1024x768", "750x1334", "1242x2208", "2208x1242", "1536x2048", "2048x1536" };

		//XCode version management
		public static readonly string[] m_XCodeVersionArray = { "Default", "XCode 7", "XCode 8", "XCode 9" };

		// Android manifests path
		public const string DEFAULT_MANIFEST_PATH = "/Plugins/Android/AndroidManifest.xml";
		public const string MANIFEST_TO_RESTORE_PATH = "/Plugins/Android/AndroidManifestToRestore.xml";

		public const string SEVENZIP_PATH = "/SignTools/7z.exe";

		// Langs path
		public const string LANGS_PATH = "Assets/Resources/Texts/Langs.txt";
	}
}