﻿namespace AllMyScripts.Editor.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public class StandaloneWindows64Profile : StandaloneProfile
    {
        public override BuildTarget m_ePlatform => BuildTarget.StandaloneWindows64;
        public StandaloneWindows64Profile(string filePath) : base(filePath)
        {
        }

        public StandaloneWindows64Profile(Profile builderProfile) : base(builderProfile)
        {
        }
    }
}