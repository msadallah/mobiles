﻿namespace AllMyScripts.Editor.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public class StandaloneOSXProfile : StandaloneProfile
    {
        public override BuildTarget m_ePlatform => BuildTarget.StandaloneOSX;
        public StandaloneOSXProfile(string filePath) : base(filePath)
        {
        }

        public StandaloneOSXProfile(Profile builderProfile) : base(builderProfile)
        {
        }
    }
}