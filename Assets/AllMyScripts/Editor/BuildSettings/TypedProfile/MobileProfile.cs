﻿namespace AllMyScripts.Editor.Builder
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	[System.Serializable]
	public abstract class MobileProfile : Profile
	{
		public MobileProfile(string filePath) : base(filePath)
		{
		}

		public MobileProfile(Profile builderProfile) : base(builderProfile)
		{
		}
	}
}