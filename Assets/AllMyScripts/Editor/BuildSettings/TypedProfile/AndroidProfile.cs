﻿namespace AllMyScripts.Editor.Builder
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using AllMyScripts.Common.Tools.Xml;
    using AllMyScripts.Editor.Builder.Android;
    using UnityEditor;
    using UnityEngine;

    public class AndroidProfile : MobileProfile
    {
        public override BuildTargetGroup typedBuildingTargetGroup => BuildTargetGroup.Android;
        public override BuildTarget m_ePlatform => BuildTarget.Android;
        //Android specific parameters
        // the android key idx used to sign the apk ( in lwAndroidKeys.GetKeyNames() )
        public int m_nAndroidKeyIndex;
        public AndroidArchitecture m_architecture = AndroidArchitecture.All;
        public bool m_bSplitArchitectureApk = false;
        public bool m_bUseAABSystem = false;
        public AndroidProfile(string filePath) : base(filePath)
        {
        }

        public AndroidProfile(Profile builderProfile) : base(builderProfile)
        {
            if (builderProfile is AndroidProfile)
            {
                AndroidProfile androidProfile = (AndroidProfile)builderProfile;
                //Specific android part
                m_nAndroidKeyIndex = androidProfile.m_nAndroidKeyIndex;
                m_architecture = androidProfile.m_architecture;
                m_bSplitArchitectureApk = androidProfile.m_bSplitArchitectureApk;
                m_bUseAABSystem = androidProfile.m_bUseAABSystem;
            }
            else
            {
                Debug.LogWarning("Base profile is not android type");
            }
        }

        protected override void WriteTypedScriptingSettings(XmlWriter xw, BuildTarget buildTarget)
        {
            if (buildTarget == BuildTarget.Android)
            {
                xw.WriteAttribute("architecture", m_architecture.ToString());
                xw.WriteAttributeBool("splitApkTarget", m_bSplitArchitectureApk);
                xw.WriteAttributeBool("useAABSystem", m_bUseAABSystem);
            }
        }
        protected override void WriteTypedSettings(XmlWriter xw, BuildTarget buildTarget)
        {
			//Android scripting Settings
			xw.WriteCommentLine( "Android specific part" );
			if (buildTarget == BuildTarget.Android)
            {
                xw.WriteStartElement("ANDROID_KEY");
                xw.WriteAttribute("value", Keys.GetKey(m_nAndroidKeyIndex).sName);
                xw.WriteEndElement();
            }
        }
        protected override void XmlOnReadNodeElement(XmlReader xr, string node)
        {
            base.XmlOnReadNodeElement(xr, node);
            switch (node)
            {
                case "ANDROID_KEY":
                    if (typedBuildingTargetGroup == BuildTargetGroup.Android)
                    {
                        int nKeyIndex = Array.IndexOf(Keys.GetKeyNames(), xr.GetAttribute("value"));
                        if (nKeyIndex >= 0)
                        {
                            m_nAndroidKeyIndex = nKeyIndex;
                        }
                    }
                    break;

                case "SCRIPTING":
                    m_bUseAABSystem = xr.GetAttributeBool("useAABSystem", false);
                    if (m_scriptingBackend == ScriptingImplementation.IL2CPP)
                    {
                        m_architecture = xr.GetAttributeEnum("architecture", AndroidArchitecture.All);
#if !UNITY_2019_3_OR_NEWER
                        m_architecture &= ~AndroidArchitecture.X86;
#endif
                        m_bSplitArchitectureApk = xr.GetAttributeBool("splitApkTarget", false);
                    }
                    else
                    {
                        m_architecture = xr.GetAttributeEnum("architecture", AndroidArchitecture.ARMv7
#if !UNITY_2019_3_OR_NEWER
                                        | AndroidArchitecture.X86
#endif
                                        );
                        if (m_architecture.HasFlag(AndroidArchitecture.ARM64) || m_architecture.HasFlag(AndroidArchitecture.All))
                            m_architecture = AndroidArchitecture.ARMv7
#if !UNITY_2019_3_OR_NEWER
                                            | AndroidArchitecture.X86
#endif
                                            ;
                    }
                    break;
            }
        }
    }
}