﻿namespace AllMyScripts.Editor.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public class StandaloneLinux64Profile : StandaloneProfile
    {
        public override BuildTarget m_ePlatform => BuildTarget.StandaloneLinux64;
        public StandaloneLinux64Profile(string filePath) : base(filePath)
        {
        }

        public StandaloneLinux64Profile(Profile builderProfile) : base(builderProfile)
        {
        }
    }
}