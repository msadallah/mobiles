﻿namespace AllMyScripts.Editor.Builder
{
    using AllMyScripts.Common.Tools.Xml;
    using UnityEditor;
    using UnityEngine;

    public abstract class StandaloneProfile : Profile
	{
		//Desktop profile only
		public FullScreenMode _fullscreenMode = FullScreenMode.Windowed;
        public int _defaultScreenWidth = 720;
        public int _defaultScreenHeight = 480;

        public override BuildTargetGroup typedBuildingTargetGroup => BuildTargetGroup.Standalone;
		public StandaloneProfile(string filePath) : base(filePath)
		{
		}

		public StandaloneProfile(Profile builderProfile) : base(builderProfile)
        {
            if (builderProfile is StandaloneProfile)
            {
                StandaloneProfile standaloneProfile = (StandaloneProfile)builderProfile;
                //Specific Desktop part
                _fullscreenMode = standaloneProfile._fullscreenMode;
                _defaultScreenWidth = standaloneProfile._defaultScreenWidth;
                _defaultScreenHeight = standaloneProfile._defaultScreenHeight;
            }
            else
            {
                Debug.LogWarning("Base profile is not a Standalone type");
            }
		}
        protected override void XmlOnReadNodeElement(XmlReader xr, string node)
        {
            base.XmlOnReadNodeElement(xr, node);
            switch (node)
            {
                case "FULLSCREEN_MODE":
                    _fullscreenMode = xr.GetAttributeEnum("value", FullScreenMode.Windowed);
                    break;
                case "DEFAULT_SCREEN_WIDTH":
                    _defaultScreenWidth = xr.GetAttributeInt("value", _defaultScreenWidth);
                    break;
                case "DEFAULT_SCREEN_HEIGHT":
                    _defaultScreenHeight = xr.GetAttributeInt("value", _defaultScreenHeight);
                    break;
            }
        }

        protected override void WriteTypedSettings( XmlWriter xw, BuildTarget buildTarget)
        {
			xw.WriteCommentLine( "Standalone specific part" );
			xw.WriteStartElement("FULLSCREEN_MODE");
            xw.WriteAttribute("value", _fullscreenMode.ToString());
            xw.WriteEndElement();
            xw.WriteStartElement("DEFAULT_SCREEN_WIDTH");
            xw.WriteAttribute("value", _defaultScreenWidth.ToString());
            xw.WriteEndElement();
            xw.WriteStartElement("DEFAULT_SCREEN_HEIGHT");
            xw.WriteAttribute("value", _defaultScreenHeight.ToString());
            xw.WriteEndElement();
        }
    }
}