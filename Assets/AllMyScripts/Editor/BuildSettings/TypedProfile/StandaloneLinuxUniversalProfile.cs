﻿namespace AllMyScripts.Editor.Builder
{
    using UnityEditor;

#if UNITY_2019_2_OR_NEWER
	[System.Obsolete("Deprecated by Unity since 2019.2, use only Linux64 now. Profile class can be removed after this Unity release")]
#endif
	public class StandaloneLinuxUniversalProfile : StandaloneProfile
	{
#if UNITY_2019_2_OR_NEWER
		public override BuildTarget m_ePlatform => BuildTarget.StandaloneLinux64;
#else
        public override BuildTarget m_ePlatform => BuildTarget.StandaloneLinuxUniversal;
#endif
        public StandaloneLinuxUniversalProfile(string filePath) : base(filePath)
        {
        }

        public StandaloneLinuxUniversalProfile(Profile builderProfile) : base(builderProfile)
        {
        }
    }
}