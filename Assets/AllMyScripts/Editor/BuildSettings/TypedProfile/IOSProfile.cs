﻿namespace AllMyScripts.Editor.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using AllMyScripts.Common.Tools;
    using AllMyScripts.Common.Tools.Xml;
    using UnityEditor;
    using UnityEngine;

    public class IOSProfile : MobileProfile
    {
        public enum XCodeMethod
        {
            appStore = 0,
            validation = 1,
            adHoc = 2,
            package = 3,
            entreprise = 4,
            development = 5,
            developerId = 6,
            macApplication = 7,
        }

        // iOS profile only (Provisioning profiles)
        // these fields are not read in lwBuilder but are read in the batches that autobuild an iOS version
        public string m_sTeamIds = string.Empty;
        public string m_targetOS = "9.0";
        public string m_sUseStatusBar = "false";
        public XCodeMethod m_XCodeMethod = XCodeMethod.development;

        public override BuildTargetGroup typedBuildingTargetGroup => BuildTargetGroup.iOS;
        public override BuildTarget m_ePlatform => BuildTarget.iOS;
        public IOSProfile(string filePath) : base(filePath)
        {
        }

        public IOSProfile(Profile builderProfile) : base(builderProfile)
        {
            if (builderProfile is IOSProfile)
            {
                IOSProfile IOSProfile = (IOSProfile)builderProfile;
                //Specific iOS part
                m_sTeamIds = IOSProfile.m_sTeamIds;
                m_targetOS = IOSProfile.m_targetOS;
                m_sUseStatusBar = IOSProfile.m_sUseStatusBar;
                m_XCodeMethod = IOSProfile.m_XCodeMethod;
            }
            else
            {
                Debug.LogWarning("Base profile is not IOS type");
            }
        }
        protected override void XmlOnReadNodeElement(XmlReader xr, string node)
        {
            base.XmlOnReadNodeElement(xr, node);
            switch (node)
            {
                case "TEAM_ID":
                    m_sTeamIds = xr.GetAttribute("value", string.Empty);
                    break;
                case "TARGET_OS":
                    m_targetOS = xr.GetAttribute("value", string.Empty);
                    break;
                case "USE_STATUS_BAR":
                    m_sUseStatusBar = xr.GetAttribute("value", string.Empty);
                    break;
                case "SCRIPTING":
                    if (m_ePlatform == BuildTarget.iOS)
                    {
                        m_XCodeMethod = ConvertFromXCodeMethodName(xr.GetAttribute("method", "development"));
                    }
                    break;
            }
        }
        protected override void WriteTypedScriptingSettings(XmlWriter xw, BuildTarget buildTarget)
        {
            if (buildTarget == BuildTarget.iOS)
            {
                xw.WriteAttribute("method", ConvertToXCodeMethodName(m_XCodeMethod));
            }
        }
        protected override void WriteTypedSettings(XmlWriter xw, BuildTarget buildTarget)
        {
			xw.WriteCommentLine( "iOS specific part" );
			// iOS Settings
			if (buildTarget == BuildTarget.iOS)
            {
                xw.WriteStartElement("TEAM_ID");
                xw.WriteAttribute("value", m_sTeamIds);
                xw.WriteEndElement();

                xw.WriteStartElement("TARGET_OS");
                xw.WriteAttribute("value", m_targetOS);
                xw.WriteEndElement();

                xw.WriteStartElement("USE_STATUS_BAR");
                xw.WriteAttribute("value", m_sUseStatusBar);
                xw.WriteEndElement();
            }
        }

        public static string ConvertToXCodeMethodName(XCodeMethod method)
        {
            if (method == XCodeMethod.adHoc)
                return "ad-hoc";
            else if (method == XCodeMethod.appStore)
                return "app-store";
            else if (method == XCodeMethod.developerId)
                return "developer-id";
            else if (method == XCodeMethod.macApplication)
                return "mac-application";
            else
                return method.ToString();
        }

        public static XCodeMethod ConvertFromXCodeMethodName(string method)
        {
            if (method == "ad-hoc")
                return XCodeMethod.adHoc;
            else if (method == "app-store")
                return XCodeMethod.appStore;
            else if (method == "developer-id")
                return XCodeMethod.developerId;
            else if (method == "mac-application")
                return XCodeMethod.macApplication;
            else
                return ParseTools.ParseEnumSafe(method, XCodeMethod.development);
        }
    }
}