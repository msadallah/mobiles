﻿namespace AllMyScripts.Editor.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public class StandaloneWindowsProfile : StandaloneProfile
    {
        public override BuildTarget m_ePlatform => BuildTarget.StandaloneWindows;
        public StandaloneWindowsProfile(string filePath) : base(filePath)
        {
        }

        public StandaloneWindowsProfile(Profile builderProfile) : base(builderProfile)
        {
        }
    }
}