﻿namespace AllMyScripts.Editor.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using AllMyScripts.Common.Tools.Xml;
    using UnityEditor;
    using UnityEngine;

    public class WebGLprofile : Profile
    {
        //WebGL specific parameters
        public WebGLExceptionSupport webGLExceptionSupport;
        public string webGLTemplate = "Default";
        public string webGLAwsPath = string.Empty;

        public override BuildTargetGroup typedBuildingTargetGroup => BuildTargetGroup.WebGL;
        public override BuildTarget m_ePlatform => BuildTarget.WebGL;

        public WebGLprofile(string filePath) : base(filePath) {}

        public WebGLprofile(Profile builderProfile) : base(builderProfile)
        {
            if (builderProfile is WebGLprofile)
            {
                WebGLprofile webGLprofile = (WebGLprofile)builderProfile;
                //Specific WebGL part
                webGLTemplate = webGLprofile.webGLTemplate;
                webGLExceptionSupport = webGLprofile.webGLExceptionSupport;
            }
            else
            {
                Debug.LogWarning("Base profile is not WebGL type");
            }
        }

        protected override void XmlOnReadNodeElement(XmlReader xr, string node)
        {
            base.XmlOnReadNodeElement(xr, node);
            if (m_ePlatform == BuildTarget.WebGL)
            {
                switch (node)
                {
                    case "SCRIPTING":
                        webGLExceptionSupport = xr.GetAttributeEnum("webGLExceptionSupport", WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly);
                        break;
                    case "TEMPLATE":
                        webGLTemplate = xr.GetAttribute("name", string.Empty);
                        break;
                    case "AWSPATH":
                        webGLAwsPath = xr.GetAttribute("path", string.Empty);
                        break;
                }
            }
        }

        protected override void WriteTypedScriptingSettings(XmlWriter xw, BuildTarget buildTarget)
        {
            if (buildTarget == BuildTarget.WebGL)
            {
                xw.WriteAttribute("webGLExceptionSupport", webGLExceptionSupport.ToString());
            }
        }

        protected override void WriteTypedSettings(XmlWriter xw, BuildTarget buildTarget)
        {
			xw.WriteCommentLine( "WebGL specific part" );
			//WebGL Settings
			if (buildTarget == BuildTarget.WebGL)
            {
                xw.WriteStartElement("TEMPLATE");
                xw.WriteAttribute("name", webGLTemplate);
                xw.WriteEndElement();

                xw.WriteStartElement("AWSPATH");
                xw.WriteAttribute("path", webGLAwsPath);
                xw.WriteEndElement();
            }
        }
    }
}