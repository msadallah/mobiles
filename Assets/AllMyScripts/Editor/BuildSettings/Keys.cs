namespace AllMyScripts.Editor.Builder.Android
{
	public static class Keys
	{
		public class Key
		{
			public string sName
			{
				get { return m_sName; }
			}

			public string sStoreName
			{
				get { return m_sStoreName; }
			}

			public string sStorePass
			{
				get { return m_sStorePass; }
			}

			public string sAliasName
			{
				get { return m_sAliasName; }
			}

			public string sAliasPass
			{
				get { return m_sAliasPass; }
			}

			public Key( string sName, string sStoreName, string sStorePass, string sAliasName, string sAliasPass )
			{
				m_sName = sName;
				m_sStoreName = sStoreName;
				m_sStorePass = sStorePass;
				m_sAliasName = sAliasName;
				m_sAliasPass = sAliasPass;
			}

			private string m_sName;
			private string m_sStoreName;
			private string m_sStorePass;
			private string m_sAliasName;
			private string m_sAliasPass;
		}

		public static string[] GetKeyNames()
		{
			string[] sKeyNames = new string[s_keys.Length];
			for( int i = 0; i < s_keys.Length; i++ )
			{
				sKeyNames[i] = s_keys[i].sName;
			}
			return sKeyNames;
		}

		public static Key GetKey( int nIndex )
		{
			if( nIndex >= 0 &&
				nIndex < s_keys.Length )
				return s_keys[nIndex];
			else
				return null;
		}

		private static readonly Key[] s_keys = new Key[] {	new Key( "liris", "Liris", "Liris", "LirisKey", "Liris") };
	}
}
