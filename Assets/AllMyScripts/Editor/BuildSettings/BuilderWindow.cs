#if UNITY_EDITOR
namespace AllMyScripts.Editor.Builder
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Text.RegularExpressions;
	using System.Threading;

	using UnityEditor;
	using UnityEngine;

	using AllMyScripts.Common.Tools;
	using AllMyScripts.LangManager;
	using AllMyScripts.Builder;
	using AllMyScripts.Editor.Builder.Android;

	public class BuilderWindow : EditorWindow
	{
		#region Attributes
		// Threads management
		private readonly Thread _mainThread = Thread.CurrentThread;

		public static float s_verticalSpaceBtwCategories = 5f;
		public static float s_horizontalSpaceBtwColmuns = 5f;
		public static float s_windowBorderSpace = 10f;

		// Window management
		private static BuilderWindow s_Window;

		private string _sBuildInfos = string.Empty;

		// Build Langs.txt
		private bool _bBuildLanguages;

		// GUI
		private GUIStyle _titleStyle;
		private GUIStyle _buildStyle;
		private GUIStyle _buildErrorStyle;
		private GUIStyle _buildWarningStyle;
		private GUIStyle _buildInfosStyle;
		private GUIStyle _globalDefinesStyle;

		private const string USED_GLOBAL_DEFINE_TAG = " ---> ";
		private const string MODIFIED_GLOBAL_DEFINE_TAG = " (*)";
		private static Color _freeSkinGlobalDefineSavedTextColor = new Color(0f,0.6f,0f);
		private static Color _proSkinGlobalDefineSavedTextColor = new Color(0f,0.95f,0f);
		private static Color _freeSkinGlobalDefineModifiedBkgColor = new Color(0.6f,0f,0f);
		private static Color _proSkinGlobalDefineModifiedBkgColor = new Color(0.95f,0f,0f);

		private Vector2 _vBuildInfosPosition = Vector2.zero;

		private float _fRefreshTime;

		private Vector2 _scrollPosGlobalDefines;
		private Vector2 _scrollPosProjSettings;

		public BuilderModel model;
		#endregion

		#region Inits

		//TODO : Debug the neverending recompiling do 

		[MenuItem("AllMyScripts/Tools/Builder... &b", false, 0)]
		public static void Init()
		{
			s_Window = GetWindow(typeof(BuilderWindow), false, "AllMyScripts Builder") as BuilderWindow;
			if (s_Window != null && s_Window.model == null)
				s_Window.InitAll();
		}

		private void InitAll()
		{
			if (s_Window == null)
				s_Window = GetWindow(typeof(BuilderWindow), false, "AllMyScripts Builder") as BuilderWindow;
			
			if (model == null)
				model = new BuilderModel();

			model.m_logs = UpdateBuildInfos;
			model.Init();
			model.m_update = StartBuildingGame;
		}


		//Split with model and window
		public void Building()
		{
			if (s_Window == null)
				InitAll();

			bool bRefresh = false;
			_fRefreshTime += 0.005f;
			if (_fRefreshTime >= 1f)
			{
				_fRefreshTime -= 1f;
				bRefresh = true;
			}

			if (bRefresh)
			{
				if (model.m_sSimpleBuildInfos.Length > 2 && model.m_sSimpleBuildInfos.Substring(model.m_sSimpleBuildInfos.Length - 3) == "...")
					UpdateBuildInfos(".", MessageType.Info, false, false, false);
				Repaint();
			}

			if (EditorApplication.isCompiling || (!model.IsBuilding() && !model.IsSigning()))
				return;

			for (int i = 0; i < Utils.m_ThreadsRunning.Count; i++)
				if (Utils.m_ThreadsRunning[i] == null || !Utils.m_ThreadsRunning[i].IsAlive)
					Utils.m_ThreadsRunning.RemoveAt(i--);

			if (Utils.m_ThreadsRunning.Count <= 0 && Utils.m_ThreadsToRun.Count > 0)
			{
				Utils.m_ThreadsToRun[0].Start();
				Utils.m_ThreadsRunning.Add(Utils.m_ThreadsToRun[0]);
				Utils.m_ThreadsToRun.RemoveAt(0);
				while (Utils.m_ThreadsRunning[0].ThreadState == ThreadState.Unstarted)
					;
			}

			if (model.GetCurrentBuildMethods() == null)
				model.InitBuildMethods();

			Repaint();

			if (model.IsSigning() || model.m_bBuildPaused || Utils.m_ThreadsRunning.Count > 0 || model.CurrentBuildMethod >= model.GetCurrentBuildMethods().Length)
				return;

			_fRefreshTime = 0f;

			model.GetCurrentBuildMethods()[model.CurrentBuildMethod]();

			// Phase pause
			model.UpdatePause();
		}

		private void InstallApk(BuilderModel.BuilderState currentSBuilderState, string sApkPath = "")
		{
			if (string.IsNullOrEmpty(sApkPath))
			{
				if (currentSBuilderState != BuilderModel.BuilderState.None)
				{
					string sExecutableName = model.GetExecutableName();
					sApkPath = model.GetSelectedTargetDestinationFolder() + '/' + sExecutableName;
				}
				else
				{
					sApkPath = EditorUtility.OpenFilePanel("APK location", model.GetSelectedTargetDestinationFolder(), "apk");
					if (string.IsNullOrEmpty(sApkPath))
						return;

					ChangeBuilderWindow(BuilderModel.BuilderState.SigningAPK);
					EditorApplication.update = Building;
				}
			}

			sApkPath = IOUtility.ReplacePathSeparator(sApkPath);
			int nIndex = sApkPath.LastIndexOf(".apk", StringComparison.Ordinal);
			if (nIndex > 0)
				sApkPath = sApkPath.Substring(0, nIndex);

			string sAdbPath = EditorPrefs.GetString("AndroidSdkRoot") + "/platform-tools/adb.exe";
			Utils.ThreadProcess(model, sAdbPath, "install -r \"" + sApkPath + ".apk\"", "", true, true, true);
			model.UpdateLogs("Installing the APK. Please wait...");
		}

		public bool StartBuildingGame()
		{
			if (model.BuilderTarget.m_eBuildTarget != EditorUserBuildSettings.activeBuildTarget)
			{
				if (!EditorUtility.DisplayDialog("Wrong active target.", "The active target is not the same as the build target. Do you want to continue?", "Build", "Cancel"))
					return false;
			}
			_sBuildInfos = string.Empty;

			ChangeBuilderWindow(BuilderModel.BuilderState.Building);
			_fRefreshTime = 0f;

			EditorApplication.update += Building;

			return true;
		}

		public void ChangeBuilderWindow(BuilderModel.BuilderState state)
		{
			model.ResetInfosOnBuilderChanges(state);

			if (model.BMBuilderState == BuilderModel.BuilderState.None)
				EditorApplication.update -= Building;
			_vBuildInfosPosition = Vector2.zero;
		}

		private void SetupStyles()
		{
			if ( _titleStyle == null )
			{
				_titleStyle = new GUIStyle();
				_titleStyle.font = EditorStyles.whiteBoldLabel.font;
				_titleStyle.fontSize = EditorStyles.whiteLargeLabel.fontSize;
				_titleStyle.fontStyle = EditorStyles.whiteBoldLabel.fontStyle;
				_titleStyle.fixedHeight = 20;
				_titleStyle.alignment = TextAnchor.MiddleCenter;
				_titleStyle.normal.textColor = EditorStyles.whiteLargeLabel.normal.textColor;
				_titleStyle.normal.background = EditorStyles.toolbar.normal.background;
			}

			if ( _buildStyle == null )
			{
				_buildStyle = new GUIStyle();
				_buildStyle.font = EditorStyles.whiteLargeLabel.font;
				_buildStyle.fontSize = 12;
				_buildStyle.fixedHeight = 18;
				_buildStyle.normal.textColor = EditorStyles.whiteLargeLabel.normal.textColor;
			}

			if ( _buildErrorStyle == null )
			{
				_buildErrorStyle = new GUIStyle();
				_buildErrorStyle.font = EditorStyles.whiteLargeLabel.font;
				_buildErrorStyle.fontSize = 12;
				_buildErrorStyle.fixedHeight = 18;
				_buildErrorStyle.normal.textColor = Color.red;
			}

			if (_buildWarningStyle == null )
			{
				_buildWarningStyle = new GUIStyle();
				_buildWarningStyle.font = EditorStyles.whiteLargeLabel.font;
				_buildWarningStyle.fontSize = 12;
				_buildWarningStyle.fixedHeight = 18;
				_buildWarningStyle.normal.textColor = Color.yellow;
			}

			if (_buildInfosStyle == null )
			{
				_buildInfosStyle = new GUIStyle( EditorStyles.textField );
				_buildInfosStyle.richText = true;
				_buildInfosStyle.wordWrap = true;
				_buildInfosStyle.stretchWidth = true;
				_buildInfosStyle.stretchHeight = true;
			}
		}


		#endregion

		private void Update()
		{
			if (s_Window == null)
				InitAll();

			if (model.BMBuilderState != BuilderModel.BuilderState.None)
				return;

			if (_bBuildLanguages)
			{
				model.BuildLanguages();
				_bBuildLanguages = false;
			}
		}

		#region GUI

		private void OnGUI()
		{
            if (model == null || !model.InitDone)
				return;

			SetupStyles();

			if (OnGUIBuild())
				return;


			GUILayout.BeginVertical();

			GUILayout.Space( s_windowBorderSpace );

			//Builder window top part
			GUILayout.BeginHorizontal();
			GUILayout.Space( s_windowBorderSpace );
			BuilderProfileGUI();
			GUILayout.Space( s_windowBorderSpace );
			GUILayout.EndHorizontal();

			//Builder window middle part
			GUILayout.BeginHorizontal();

			GUILayout.Space( s_windowBorderSpace );

			//Left column
			GUILayout.BeginVertical(GUILayout.MaxWidth(position.width * 0.33f), GUILayout.ExpandHeight(false));
			ProjSettingsGUI();
			GUILayout.Space(s_verticalSpaceBtwCategories);
			GlobalDefinesGUI();
			GUILayout.EndVertical();

			GUILayout.Space( s_horizontalSpaceBtwColmuns );
			
			//Middle column
			GUILayout.BeginVertical(GUILayout.MaxWidth(position.width * 0.33f));
			LanguagesGUI();
			GUILayout.Space( s_verticalSpaceBtwCategories );
			IconAndSplashGUI();
			GUILayout.Space(s_verticalSpaceBtwCategories);
			ScenesGUI();
			GUILayout.EndVertical();

			GUILayout.Space( s_horizontalSpaceBtwColmuns );

			//Right column
			GUILayout.BeginVertical(GUILayout.MaxWidth(position.width * 0.33f));
			BuildTargetOtionsGUI();
			GUILayout.Space( s_verticalSpaceBtwCategories );
			ScriptingSettingGUI();
			GUILayout.Space( s_verticalSpaceBtwCategories );
			BuildOptionsGUI();
			if ( model.BuilderTarget.m_eBuildTargetGroup == BuildTargetGroup.Android )
			{
				GUILayout.Space( s_verticalSpaceBtwCategories );
				BuildingToolsGUI();
			}
			GUILayout.EndVertical();

			GUILayout.Space( s_windowBorderSpace );

			GUILayout.EndHorizontal();

			GUILayout.Space( s_verticalSpaceBtwCategories );

			//Builder window bottom part
			GUILayout.BeginHorizontal();
			GUILayout.Space( s_windowBorderSpace );
			StartBuildGUI();
			GUILayout.Space( s_windowBorderSpace );
			GUILayout.EndHorizontal();

			GUILayout.EndVertical();
		}

		private void BuilderProfileGUI()
		{
			GUILayout.BeginVertical();
			GUILayout.Label("Profile", _titleStyle);
			GUILayout.BeginHorizontal();
			int nNewProfile = EditorGUILayout.Popup(model.CurrentProfile, model.BuilderProfilerNames.ToArray());
			if (nNewProfile != model.CurrentProfile)
				model.SetCurrentProfile(nNewProfile);
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Open"))
				model.OpenProfile();
			if (GUILayout.Button("Save"))
				model.SaveProfile();
			if (GUILayout.Button("Delete"))
				model.DeleteProfile();
			if (GUILayout.Button("Reload all profiles"))
			{
				GUIUtility.keyboardControl = 0;	
				model.LoadProfiles(model.BuilderProfilerNames[model.CurrentProfile]);
			}
			GUILayout.EndHorizontal();
			EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
			GUILayout.EndVertical();
		}

		private void ScriptingSettingGUI()
		{
			GUILayout.Label("Scripting Settings", _titleStyle);

			GUILayout.BeginHorizontal();
			model.CurrentBuilderProfile.m_apiCompatibilityLevel = (ApiCompatibilityLevel)EditorGUILayout.EnumPopup("ACL", model.CurrentBuilderProfile.m_apiCompatibilityLevel);
			GUILayout.EndHorizontal();

#if !UNITY_2019_3_OR_NEWER
			GUILayout.BeginHorizontal();
			model.CurrentBuilderProfile.m_scriptingRuntimeVersion = (ScriptingRuntimeVersion)EditorGUILayout.EnumPopup("SRV", model.CurrentBuilderProfile.m_scriptingRuntimeVersion);
			GUILayout.EndHorizontal();
#endif
			GUILayout.BeginHorizontal();
			using (new EditorGUI.DisabledScope(model.CurrentBuilderProfile.m_ePlatform == BuildTarget.iOS))
			{
				model.CurrentBuilderProfile.m_scriptingBackend = (ScriptingImplementation)EditorGUILayout.EnumPopup("Backend", model.CurrentBuilderProfile.m_scriptingBackend);
			}
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			using (new EditorGUI.DisabledScope(model.CurrentBuilderProfile.m_scriptingBackend != ScriptingImplementation.IL2CPP || model.CurrentBuilderProfile.m_ePlatform == BuildTarget.iOS))
			{
				model.CurrentBuilderProfile.m_compilerConfiguration = (Il2CppCompilerConfiguration)EditorGUILayout.EnumPopup("compiler conf.", model.CurrentBuilderProfile.m_compilerConfiguration);
			}
			if (model.CurrentBuilderProfile.m_scriptingBackend == ScriptingImplementation.IL2CPP)
			{
				GUILayout.EndHorizontal();
				model.CurrentBuilderProfile.stripEngineCode = EditorGUILayout.Toggle("Strip Engine Code", model.CurrentBuilderProfile.stripEngineCode);
				GUILayout.BeginHorizontal();
			}
			else
			{
				model.CurrentBuilderProfile.stripEngineCode = false;
			}
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			ManagedStrippingLevel newManagedStrippingLevel;
			if (model.CurrentBuilderProfile.m_scriptingBackend == ScriptingImplementation.IL2CPP)
			{
				int managedStrippingLevelAsInt =  (int)model.CurrentBuilderProfile.managedStrippingLevel;

				int selected = Mathf.Clamp(managedStrippingLevelAsInt - 1, 0, 2);
				string[] options = new string[]
				{
					"Low", "Medium", "High",
				};
				selected = EditorGUILayout.Popup("Managed Stripping Level", selected, options);
				newManagedStrippingLevel = (ManagedStrippingLevel)(++selected);
			}
			else
			{
				newManagedStrippingLevel = (ManagedStrippingLevel)EditorGUILayout.EnumPopup("Managed Stripping Level", model.CurrentBuilderProfile.managedStrippingLevel);
			}

			if (newManagedStrippingLevel != model.CurrentBuilderProfile.managedStrippingLevel)
			{
				model.CurrentBuilderProfile.managedStrippingLevel = newManagedStrippingLevel;
			}
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			Profile.VR_SDK vrSdkSelected = model.CurrentBuilderProfile.vrSdk;
			vrSdkSelected = (Profile.VR_SDK)EditorGUILayout.EnumPopup("Prefered VR SDK", vrSdkSelected);
			if (vrSdkSelected != model.CurrentBuilderProfile.vrSdk)
			{
				model.CurrentBuilderProfile.vrSdk = vrSdkSelected;
			}
			GUILayout.EndHorizontal();

			if (model.CurrentBuilderProfile.m_ePlatform == BuildTarget.Android)
			{
				AndroidProfile androidProfile  = (AndroidProfile)model.CurrentBuilderProfile;
				using (new EditorGUI.DisabledScope(androidProfile.m_bUseAABSystem))
				{
					GUILayout.BeginHorizontal();
					androidProfile.m_bSplitArchitectureApk = EditorGUILayout.Toggle("Split apk by architecture", androidProfile.m_bSplitArchitectureApk);
					GUILayout.EndHorizontal();
				}

				EditorGUILayout.LabelField("Target Architectures");
				bool bSelectARM64 = androidProfile.m_architecture.HasFlag(AndroidArchitecture.ARM64);
				bool bSelectARMv7 = androidProfile.m_architecture.HasFlag(AndroidArchitecture.ARMv7);

				GUILayout.BeginVertical();

				bSelectARMv7 = EditorGUILayout.Toggle("ARMv7", androidProfile.m_architecture.HasFlag(AndroidArchitecture.ARMv7));
				if (bSelectARMv7)
					androidProfile.m_architecture |= AndroidArchitecture.ARMv7;
				else
				{
					androidProfile.m_architecture &= ~AndroidArchitecture.ARMv7;
				}

				using (new EditorGUI.DisabledScope(model.CurrentBuilderProfile.m_scriptingBackend != ScriptingImplementation.IL2CPP))
				{
					if (model.CurrentBuilderProfile.m_scriptingBackend != ScriptingImplementation.IL2CPP)
						androidProfile.m_architecture &= ~AndroidArchitecture.ARM64;
					bSelectARM64 = EditorGUILayout.Toggle("ARM64", androidProfile.m_architecture.HasFlag(AndroidArchitecture.ARM64));
					if (bSelectARM64)
						androidProfile.m_architecture |= AndroidArchitecture.ARM64;
					else
					{
						androidProfile.m_architecture &= ~AndroidArchitecture.ARM64;
					}
				}

				GUILayout.EndVertical();
			}
			else if (model.CurrentBuilderProfile.m_ePlatform == BuildTarget.iOS)
			{
				GUILayout.BeginHorizontal();
				((IOSProfile)model.CurrentBuilderProfile).m_XCodeMethod = (IOSProfile.XCodeMethod)EditorGUILayout.EnumPopup("Method", (((IOSProfile)model.CurrentBuilderProfile).m_XCodeMethod));
				GUILayout.EndHorizontal();
			}
			else if(model.CurrentBuilderProfile.m_ePlatform ==  BuildTarget.WebGL)
			{
				GUILayout.BeginVertical();
				GUILayout.BeginHorizontal();
				WebGLprofile webGlProfile = (WebGLprofile)model.CurrentBuilderProfile;
				webGlProfile.webGLExceptionSupport = (WebGLExceptionSupport)EditorGUILayout.EnumPopup("Enable WebGL Exceptions", webGlProfile.webGLExceptionSupport);
				GUILayout.EndHorizontal();

				if (webGlProfile.webGLExceptionSupport == WebGLExceptionSupport.FullWithStacktrace)
				{
					EditorGUILayout.HelpBox
						(
							"'Full With StackTrace' exception support will decrease performance and increase browser memory usage. Only use this for debugging purposes, and make sure to test in a 64-bit browser."
							, MessageType.Warning
						);
				}
				GUILayout.EndVertical();
			}
		}

		private void ProjSettingsGUI()
		{
			GUILayout.Label("Project Settings", _titleStyle);
			GUILayout.BeginVertical();
			BuildVariable varToRemove = null;

			bool areBuildVariables = (model?.CurrentBuilderProfile?.buildVariables.Count ?? 0) > 0;

			if (areBuildVariables)
			{
				GUILayout.BeginHorizontal();
				GUILayout.Label("key");
				GUILayout.Label("value");
				GUILayout.Label("target");
				GUILayout.EndHorizontal();
			}
			_scrollPosProjSettings = GUILayout.BeginScrollView( _scrollPosProjSettings, GUILayout.ExpandHeight(false));

			if (areBuildVariables)
			{
				foreach (BuildVariable buildVar in model?.CurrentBuilderProfile?.buildVariables)
				{
					GUILayout.BeginHorizontal();

					buildVar.name = EditorGUILayout.TextField(buildVar.name);
					buildVar.value = EditorGUILayout.TextField(buildVar.value);
					buildVar.target = (ProjTarget)EditorGUILayout.EnumPopup(buildVar.target);

					GUI.enabled = buildVar.name != "DEVELOPMENT_BUILD";

					if (GUILayout.Button("X", GUILayout.Width(20f)))
						varToRemove = buildVar;

					GUI.enabled = true;

					GUILayout.EndHorizontal();
				}
			}

			GUILayout.EndScrollView();

			if (varToRemove != null)
				model.CurrentBuilderProfile.buildVariables.Remove(varToRemove);

			if (GUILayout.Button("Add"))
				model.CurrentBuilderProfile.buildVariables.Add(new BuildVariable("","",ProjTarget.None));
			
			GUILayout.EndVertical();
		}

		private void GlobalDefinesGUI()
		{
			GUILayout.Label($"Global Defines", _titleStyle);
			if (model.GlobalDefineManager.GlobalDefines.Count > 0)
			{
				bool isAlreadyExists = false;
				_scrollPosGlobalDefines = GUILayout.BeginScrollView(_scrollPosGlobalDefines, GUILayout.ExpandHeight(false));
				for (int i = 0; i < model.GlobalDefineManager.GlobalDefines.Count; i++)
				{
					GUILayout.BeginHorizontal();

					Color guiContentColor = GUI.contentColor;
					Color guiBkgColor = GUI.backgroundColor;
					bool bEnabled = model.GlobalDefineManager.GlobalDefines[i].bEnabled;
					bool bSavedEnabled = model.GlobalDefineManager.GlobalDefines[i].bSavedEnabled;
					bool bDiffFromProfile = bEnabled != bSavedEnabled;
					bool isCurrentlyUsed = model.GlobalDefineManager.GlobalDefines[i].isCurrentlyUsed;
					bool hasProSkin = EditorGUIUtility.isProSkin;
					bool bNewEnable = EditorGUILayout.Toggle(bEnabled, GUILayout.Width(14f));
  					if (bNewEnable != bEnabled)
					{
						model.GlobalDefineManager.GlobalDefines[i].bEnabled = bNewEnable;
						if (bNewEnable)
							model.CurrentBuilderProfile.m_globalDefines.Add(model.GlobalDefineManager.GlobalDefines[i].sName);
						else
							model.CurrentBuilderProfile.m_globalDefines.Remove(model.GlobalDefineManager.GlobalDefines[i].sName);
					}

					 

					Color textColor = EditorStyles.textField.normal.textColor;
					if (bSavedEnabled)
						textColor = hasProSkin ? _proSkinGlobalDefineSavedTextColor : _freeSkinGlobalDefineSavedTextColor;

					Color bkgColor = guiBkgColor;
					if (bDiffFromProfile)
						bkgColor = hasProSkin ? _proSkinGlobalDefineModifiedBkgColor : _freeSkinGlobalDefineModifiedBkgColor;

					if (_globalDefinesStyle == null)
						_globalDefinesStyle = new GUIStyle(EditorStyles.textField);
					_globalDefinesStyle.fontStyle = isCurrentlyUsed ? FontStyle.Bold : FontStyle.Normal;
					_globalDefinesStyle.normal.textColor = textColor;
					GUI.backgroundColor = bkgColor;

					string sName = model.GlobalDefineManager.GlobalDefines[i].sName;
					if (isCurrentlyUsed)
						sName = USED_GLOBAL_DEFINE_TAG + sName;
					if (bDiffFromProfile)
						sName += MODIFIED_GLOBAL_DEFINE_TAG;
					sName = EditorGUILayout.TextField(sName, _globalDefinesStyle);
					model.GlobalDefineManager.GlobalDefines[i].sName = sName.Replace(USED_GLOBAL_DEFINE_TAG, "").Replace(MODIFIED_GLOBAL_DEFINE_TAG, "");

					GUI.contentColor = guiContentColor;
					GUI.backgroundColor = guiBkgColor;

					if(model.GlobalDefineManager.AlreadyContainsGDAtId(i))
						isAlreadyExists = true;

					if (GUILayout.Button("X", GUILayout.Width(20f)))
					{
						model.CurrentBuilderProfile.m_globalDefines.Remove(model.GlobalDefineManager.GlobalDefines[i].sName);
						model.GlobalDefineManager.GlobalDefines.RemoveAt(i);
					}

					GUILayout.EndHorizontal();
				}

				GUILayout.Label($"{USED_GLOBAL_DEFINE_TAG}= currently used by unity in {EditorUserBuildSettings.selectedBuildTargetGroup} group" );
				GUILayout.Label($" {MODIFIED_GLOBAL_DEFINE_TAG} = modification from saved XML file");

				GUILayout.EndScrollView();

				if (isAlreadyExists)
					EditorGUILayout.HelpBox("You have duplicated defines. It's forbidden and ignored", MessageType.Warning);
			}

			GUILayout.Space(10f);


			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Add"))
			{
				model.AddGlobalDefine("NEW_DEFINE", false, false);
			}

			if (GUILayout.Button("Save"))
			{
                model.GlobalDefineManager.SaveInXmlFile();
			}

			if(GUILayout.Button("Reload"))
			{
				GUIUtility.keyboardControl = 0;
				model.ReloadGlobalDefinesFromProfiles();
			}

            GUILayout.EndHorizontal();
            
            GUILayout.BeginHorizontal();
            if (GUILayout.Button($"Apply for current {EditorUserBuildSettings.selectedBuildTargetGroup} group"))
                model.GlobalDefineManager.Save(EditorUserBuildSettings.selectedBuildTargetGroup);
            GUILayout.EndHorizontal();
        }

		private void LanguagesGUI()
		{
			GUILayout.Label( "Languages", _titleStyle );
			List<string> allValues = new List<string>(); 
			allValues.Add( "Select all" );
			allValues.Add( "EFIGS" );
			for ( int i = 0 ; i < model.Countries?.Length ; i++ )
				allValues.Add( model.Countries[ i ].m_sEnglishLanguageName );
			GUILayout.BeginHorizontal();
			model.m_nSelectedIndexLanguage = EditorGUILayout.Popup( model.m_nSelectedIndexLanguage, allValues.ToArray() );
			if ( GUILayout.Button( "Add" ) )
			{
				if ( model.m_nSelectedIndexLanguage <= 1 )
				{
					for ( int i = 0 ; i < model.Countries?.Length ; i++ )
					{
						// idx 0 : all languages to add
						bool bAddLanguage = model.m_nSelectedIndexLanguage == 0;
						// idx 1 : EFIGS only
						if ( model.m_nSelectedIndexLanguage == 1 )
						{
							switch ( model.Countries[ i ].m_eLang )
							{
								case SystemLanguage.English:
								case SystemLanguage.French:
								case SystemLanguage.Italian:
								case SystemLanguage.German:
								case SystemLanguage.Spanish:
									bAddLanguage = true;
									break;
							}
						}

						if ( bAddLanguage && !model.CurrentBuilderProfile.m_sLanguageCultures.Contains( model.Countries[ i ].m_sLanguageCulture ) )
							model.CurrentBuilderProfile.m_sLanguageCultures.Add( model.Countries[ i ].m_sLanguageCulture );
					}
				}
				else
				{
					// -2 because of Select All and EFIGS options
					int nIdxLanguage = model.m_nSelectedIndexLanguage - 2;
					if ( !model.CurrentBuilderProfile.m_sLanguageCultures.Contains( model.Countries[ nIdxLanguage ].m_sLanguageCulture ) )
						model.CurrentBuilderProfile.m_sLanguageCultures.Add( model.Countries[ nIdxLanguage ].m_sLanguageCulture );
				}
			}
			GUILayout.EndHorizontal();

			for ( int i = 0 ; i < model.CurrentBuilderProfile.m_sLanguageCultures.Count ; i++ )
			{
				Country country = null;
				for ( int j = 0 ; j < model.Countries?.Length ; j++ )
				{
					if ( model.Countries[ j ].m_sLanguageCulture == model.CurrentBuilderProfile.m_sLanguageCultures[ i ] )
					{
						country = model.Countries[ j ];
						break;
					}
				}

				if ( country == null )
					continue;

				GUILayout.BeginHorizontal();

				GUILayout.Label( country.m_sEnglishLanguageName );

				if ( i > 0 && GUILayout.Button( "^", GUILayout.Width( 30f ) ) )
				{
					string tmp = model.CurrentBuilderProfile.m_sLanguageCultures[ i ];
					model.CurrentBuilderProfile.m_sLanguageCultures[ i ] = model.CurrentBuilderProfile.m_sLanguageCultures[ i - 1 ];
					model.CurrentBuilderProfile.m_sLanguageCultures[ i - 1 ] = tmp;
					break;
				}

				if ( i < model.CurrentBuilderProfile.m_sLanguageCultures.Count - 1 )
				{
					if ( GUILayout.Button( "v", GUILayout.Width( 30f ) ) )
					{
						string tmp = model.CurrentBuilderProfile.m_sLanguageCultures[ i ];
						model.CurrentBuilderProfile.m_sLanguageCultures[ i ] = model.CurrentBuilderProfile.m_sLanguageCultures[ i + 1 ];
						model.CurrentBuilderProfile.m_sLanguageCultures[ i + 1 ] = tmp;
						break;
					}
				}
				else
				{
					GUILayout.Space( 34f );
				}

				if (GUILayout.Button("X", GUILayout.Width(20f)))
				{
					model.CurrentBuilderProfile.m_sLanguageCultures.RemoveAt( i );
					break;
				}
				GUILayout.EndHorizontal();
			}

			GUILayout.Space( 5f );

			if ( GUILayout.Button( "Remove All" ) )
			{
				model.CurrentBuilderProfile.m_sLanguageCultures.Clear();
			}

			GUILayout.Space( 5f );

			if ( GUILayout.Button( "Build Languages" ) )
				_bBuildLanguages = true;

			string sUnhandledLanguage = CountryCode.CheckUnhandledLanguage( model.CurrentBuilderProfile.m_sLanguageCultures );
			if ( !string.IsNullOrEmpty( sUnhandledLanguage ) )
				EditorGUILayout.HelpBox( sUnhandledLanguage + " is not a language handled in Country", MessageType.Error );
		}

		private void IconAndSplashGUI()
		{
			GUILayout.Label( "Icon & splash parameters", _titleStyle );

			if (model.BuilderTarget.m_eBuildTargetGroup != BuildTargetGroup.WebGL &&
				model.BuilderTarget.m_eBuildTargetGroup != BuildTargetGroup.Android)
			{
				model.CurrentBuilderProfile.iconTexture = (Texture2D)EditorGUILayout.ObjectField(model.CurrentBuilderProfile.iconTexture, typeof(Texture2D), false);

				if ( !model.ValidIcons() )
					EditorGUILayout.HelpBox( "Application icon is missing", MessageType.Warning );

				EditorGUILayout.LabelField( "", GUI.skin.horizontalSlider );
			}

			model.CurrentBuilderProfile.m_bUseSplashSystem = GUILayout.Toggle(model.CurrentBuilderProfile.m_bUseSplashSystem, "Use Splash System");
			if (model.CurrentBuilderProfile.m_bUseSplashSystem)
			{
				Color cBackgroundColor;
				if (!ColorUtility.TryParseHtmlString(model.CurrentBuilderProfile.m_sSplashColor, out cBackgroundColor))
					cBackgroundColor = new Color(.1333f, .1725f, .2117f);

				model.CurrentBuilderProfile.m_sSplashColor = "#" + ColorUtility.ToHtmlStringRGB(EditorGUILayout.ColorField(cBackgroundColor));
			}
		}

		private void ScenesGUI()
		{
			GUILayout.Label("Scenes", _titleStyle);
			int sceneIdxToRemove = -1;
			for (int i = 0; i < model.CurrentBuilderProfile.scenes?.Count; ++i)
			{
				GUILayout.BeginHorizontal();
				model.CurrentBuilderProfile.scenes[i] = (SceneAsset)EditorGUILayout.ObjectField(model.CurrentBuilderProfile.scenes[i], typeof(SceneAsset), false);
				if (GUILayout.Button("X", GUILayout.Width(20f)))
					sceneIdxToRemove = i;
				GUILayout.EndHorizontal();
			}

			if (sceneIdxToRemove >= 0)
				model.CurrentBuilderProfile.scenes?.RemoveAt(sceneIdxToRemove);

			if (GUILayout.Button("Add"))
			{
				if (model.CurrentBuilderProfile.scenes == null)
					model.CurrentBuilderProfile.scenes = new List<SceneAsset>();
				model.CurrentBuilderProfile.scenes.Add(null);
			}

			GUI.enabled = model.CurrentBuilderProfile.scenes?.Count > 0;
			if (GUILayout.Button("Apply To Build Settings"))
				model.SetEditorBuildSettingsScenes();
			GUI.enabled = true;

			bool atLeastOneSceneNotNull = false;
			for (int i = 0; i < model.CurrentBuilderProfile.scenes?.Count; i++)
			{
				if (!(model.CurrentBuilderProfile.scenes[i] is null))
					atLeastOneSceneNotNull = true;
			}

			if (model.CurrentBuilderProfile.scenes?.Count == 0 || !atLeastOneSceneNotNull)
				EditorGUILayout.HelpBox("You need at least 1 scene in your project", MessageType.Error);
		}

		private void BuildTargetOtionsGUI()
		{
			GUILayout.Label("Target", _titleStyle);

			GUILayout.BeginHorizontal();

			int nNewTarget = EditorGUILayout.Popup(model.SelectedTarget, model.TargetsName, GUILayout.Width(144f));
			model.NeedToChangeTarget(nNewTarget);

			// On disable le bouton switch platform si on est déjà sur la bonne plateforme
			GUI.enabled = (model.BuilderTarget.m_eBuildTarget != EditorUserBuildSettings.activeBuildTarget);
			if (GUILayout.Button("Switch Platform", GUILayout.Height(15f)))
				model.SwitchPlatform(true);
			GUI.enabled = true;

			GUILayout.EndHorizontal();

			GUILayout.BeginVertical();
			BundleVersion();

			model.CurrentBuilderProfile.m_sProductName = EditorGUILayout.TextField("Product Name", model.CurrentBuilderProfile.m_sProductName);

			if (string.IsNullOrEmpty(model.CurrentBuilderProfile.m_sProductName))
				EditorGUILayout.HelpBox("Product Name can't be empty", MessageType.Error);

			GUILayout.EndVertical();

			TargetOptionsGUI();
		}

		private void TargetOptionsGUI()
		{
			switch (model.BuilderTarget.m_eBuildTarget)
			{
				case BuildTarget.iOS:
					IOSOptionsGUI();
					break;
				case BuildTarget.Android:
					AndroidOptionsGUI();
					break;
			}
		}

		private void IOSOptionsGUI()
		{
			model.CurrentBuilderProfile.m_sBundleIdentifier = EditorGUILayout.TextField( "Bundle Identifier", model.CurrentBuilderProfile.m_sBundleIdentifier);
			Helpers.SetApplicationIdentifier(model.CurrentBuilderProfile.m_sBundleIdentifier);

			GUILayout.Space(5f);
			GUILayout.Label("iOS Options", _titleStyle);
			((IOSProfile)model.CurrentBuilderProfile).m_sTeamIds = EditorGUILayout.TextField("Team ID", ((IOSProfile)model.CurrentBuilderProfile).m_sTeamIds);
			GUILayout.Space(5f);

			PluginsGUI();
		}

		private void AndroidOptionsGUI()
		{
			// Key
			GUILayout.BeginHorizontal();
			AndroidProfile androidProfile = (AndroidProfile) model.CurrentBuilderProfile;
			int nPrevIndex = androidProfile.m_nAndroidKeyIndex;
			androidProfile.m_nAndroidKeyIndex = EditorGUILayout.Popup("Key", androidProfile.m_nAndroidKeyIndex, Keys.GetKeyNames());
			GUILayout.EndHorizontal();

			if (androidProfile.m_nAndroidKeyIndex != nPrevIndex)
				model.SetAndroidKey(true);

			// Game Name
			string sNewBuilderIdentifier = EditorGUILayout.TextField("Bundle Identifier", model.CurrentBuilderProfile.m_sBundleIdentifier);
			if (model.CurrentBuilderProfile.m_sBundleIdentifier != sNewBuilderIdentifier)
			{
				model.CurrentBuilderProfile.m_sBundleIdentifier = sNewBuilderIdentifier;
				Helpers.SetApplicationIdentifier(model.CurrentBuilderProfile.m_sBundleIdentifier);
				model.SetAndroidInfo(false);
			}

			GUILayout.Space(5f);

			PluginsGUI();
		}

		private void PluginsGUI()
		{
			if (model.BuilderTarget.m_eBuildTarget == BuildTarget.Android)
			{
				GUILayout.Label("Android Plugins & Permissions", _titleStyle);
			}
			else if (model.BuilderTarget.m_eBuildTarget == BuildTarget.iOS)
			{
				GUILayout.Label("iOS Plugins", _titleStyle);
			}

			for (int i = 0; i < model.PluginsPath.Count; i++)
			{
				string sPluginName = IOUtility.GetFileName(model.PluginsPath[i]);
				GUILayout.BeginHorizontal();

				bool bIsPluginSelected = model.CurrentBuilderProfile.m_selectedPlugins.Contains(model.PluginsPath[i]);
				bool bNewValuePluginSelected = EditorGUILayout.Toggle(sPluginName, bIsPluginSelected);
				if (bIsPluginSelected != bNewValuePluginSelected)
				{
					if (bNewValuePluginSelected)
					{
						model.CurrentBuilderProfile.m_selectedPlugins.Add(model.PluginsPath[i]);
					}
					else
					{
						model.CurrentBuilderProfile.m_selectedPlugins.Remove(model.PluginsPath[i]);
					}
				}

				GUILayout.EndHorizontal();
			}

			if (GUILayout.Button("Copy Google Services"))
			{
				if (model.CurrentBuilderProfile.TryGetPluginConfig(ManifestOptions.AndroidPlugin.Firebase.ToString(), out string sPluginFileName))
				{
					string sPluginPath = IOUtility.CombinePaths(Constants.GetAssetsPath() + "/../../Data/Firebase", sPluginFileName);
					if (File.Exists(sPluginPath))
					{
						string sDestPath = Constants.GetAssetsPath() + "/";
						string sDestFile = string.Empty;
						if (model.BuilderTarget.m_eBuildTarget == BuildTarget.Android)
						{
							sDestFile = "google-services.json";
						}
						else if (model.BuilderTarget.m_eBuildTarget == BuildTarget.iOS)
						{
							sDestFile = "GoogleService-Info.plist";
						}

						File.Copy(sPluginPath, sDestPath + sDestFile, true);
						AssetDatabase.ImportAsset("Assets/" + sDestFile);
					}
				}				
			}

			GUILayout.BeginHorizontal();

			string sMissingPlugin = model.CheckMissingPlugins();
			if ( !string.IsNullOrEmpty( sMissingPlugin ) )
				EditorGUILayout.HelpBox( sMissingPlugin + " is set in the current profile but doesn't not exist in the project.", MessageType.Error );

			if (model.BuilderTarget.m_eBuildTarget == BuildTarget.Android)
			{
				if (GUILayout.Button("Open Manifest"))
					AndroidUtils.OpenManifest(model);
				if (GUILayout.Button("Save Manifest"))
					model.SetAndroidManifest();
			}

			if (GUILayout.Button("Reload Plugins"))
				model.LoadPlugins();
			GUILayout.EndHorizontal();
		}

		private void BuildOptionsGUI()
		{
			GUILayout.Label("Build Options", _titleStyle);

			GUILayout.BeginHorizontal();
			model.m_eBuildControl = (BuilderModel.BuildControl)EditorGUILayout.EnumPopup("Control", model.m_eBuildControl);
			GUILayout.EndHorizontal();

			if( model.BuilderTarget.m_eBuildTargetGroup == BuildTargetGroup.Standalone )
			{
				GUILayout.BeginHorizontal();
				((StandaloneProfile)model.CurrentBuilderProfile)._fullscreenMode = (FullScreenMode)EditorGUILayout.EnumPopup("Fullscreen Mode", ((StandaloneProfile)model.CurrentBuilderProfile)._fullscreenMode);
				GUILayout.EndHorizontal();
				if (((StandaloneProfile)model.CurrentBuilderProfile)._fullscreenMode == FullScreenMode.Windowed)
				{
					GUILayout.BeginHorizontal();
					((StandaloneProfile)model.CurrentBuilderProfile)._defaultScreenWidth = EditorGUILayout.IntField("Default Screen Width", ((StandaloneProfile)model.CurrentBuilderProfile)._defaultScreenWidth);
					GUILayout.EndHorizontal();
					GUILayout.BeginHorizontal();
					((StandaloneProfile)model.CurrentBuilderProfile)._defaultScreenHeight = EditorGUILayout.IntField("Default Screen Height", ((StandaloneProfile)model.CurrentBuilderProfile)._defaultScreenHeight);
					GUILayout.EndHorizontal();
				}
			}

			if (model.BuilderTarget.m_eBuildTarget == BuildTarget.Android)
			{
				GUILayout.BeginHorizontal(GUILayout.Width(280f));
				// Android SDK Warning
				if (string.IsNullOrEmpty(EditorPrefs.GetString("AndroidSdkRoot")))
				{
					EditorGUILayout.HelpBox("You didn't set the Android SDK Location", MessageType.Warning);
					if (GUILayout.Button("Set Android SDK Location"))
					{
						string SDKInstallFolder = EditorUtility.OpenFolderPanel("Android SDK install location", string.Empty, string.Empty);
						if (!string.IsNullOrEmpty(SDKInstallFolder))
							EditorPrefs.SetString("AndroidSdkRoot", SDKInstallFolder);
					}
					GUI.enabled = false;
				}
				
				if (model.CurrentBuilderProfile.m_scriptingBackend == ScriptingImplementation.IL2CPP && string.IsNullOrEmpty(EditorPrefs.GetString("AndroidNdkRoot")))
				{
					EditorGUILayout.HelpBox("You didn't set the Android NDK Location", MessageType.Warning);
					if (GUILayout.Button("Set Android NDK Location"))
					{
						string NDKInstallFolder = EditorUtility.OpenFolderPanel("Android NDK install location", string.Empty, string.Empty);
						if (!string.IsNullOrEmpty(NDKInstallFolder))
							EditorPrefs.SetString("AndroidNdkRoot", NDKInstallFolder);
					}
					GUI.enabled = false;
				}
				GUILayout.EndHorizontal();
			}

			if (model.BuilderTarget.m_eBuildTarget == BuildTarget.WebGL)
			{
				WebGLprofile webGLProfile = (WebGLprofile)model.CurrentBuilderProfile;
				model.templateIdx = EditorGUILayout.Popup("WebGL Template", model.templateIdx, model.templates.ToArray());
				if (model.templateIdx < model.templates.Count && webGLProfile.webGLTemplate != model.templates[model.templateIdx])
					webGLProfile.webGLTemplate = model.templates[model.templateIdx];

				webGLProfile.webGLAwsPath = EditorGUILayout.TextField("AWS Path", webGLProfile.webGLAwsPath);
			}

			BuildingPostParamGUI();
		}

		private void BuildingToolsGUI()
		{
			GUILayout.Label("Build Tools", _titleStyle);
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Launch\nMonitor", GUILayout.Height(35f)))
				AndroidUtils.LaunchMonitor(model);
			GUI.enabled = true;
			if (GUILayout.Button("Install\nAPK", GUILayout.Height(35f)))
				InstallApk(model.BMBuilderState);
			GUILayout.EndHorizontal();
		}

		private void StartBuildGUI()
		{
			GUILayout.BeginVertical();
			EditorGUILayout.LabelField( "", GUI.skin.horizontalSlider );
			GUILayout.BeginHorizontal();
			string sButtonText = "Start Build";
			if (model.m_bGoogleProject)
				sButtonText = "Start Export";
			if (GUILayout.Button(sButtonText, GUILayout.Height(50f)))
				model.StartBuildingCheck();
			GUILayout.EndHorizontal();
			GUILayout.EndVertical();
		}

		private void BuildingPostParamGUI()
		{
			GUILayout.BeginHorizontal();
			GUILayout.BeginVertical();

			if (model.BuilderTarget.m_eBuildTarget == BuildTarget.Android)
			{
				model.m_bGoogleProject = GUILayout.Toggle(model.m_bGoogleProject, " Generate Android Studio Project");
				using (new EditorGUI.DisabledScope(model.m_bGoogleProject))
				{
					((AndroidProfile)model.CurrentBuilderProfile).m_bUseAABSystem = GUILayout.Toggle(((AndroidProfile)model.CurrentBuilderProfile).m_bUseAABSystem, " Use AAB System file");
				}
			}

			BuildTargetGroup targetGroup = BuildPipeline.GetBuildTargetGroup(model.BuilderTarget.m_eBuildTarget);

			if (targetGroup == BuildTargetGroup.iOS)
			{
				model.m_bArchiveXCode = GUILayout.Toggle(model.m_bArchiveXCode, " Archive");
			}
			else if (targetGroup == BuildTargetGroup.Android)
			{
				model.m_bAndroidInstallAfterBuild = GUILayout.Toggle(model.m_bAndroidInstallAfterBuild, " Installation");
			}
			else if (targetGroup == BuildTargetGroup.Standalone)
			{
				model.m_bAutoRun = GUILayout.Toggle(model.m_bAutoRun, " Auto Run");
			}

			GUILayout.EndVertical();
			GUILayout.EndHorizontal();
		}

		private bool OnGUIBuild()
		{
			if (model.BMBuilderState == BuilderModel.BuilderState.None)
				return false;

			if(s_Window != null)
			{ 
				GUILayout.BeginArea(new Rect(s_Window.position.width * 0.025f, 10f, s_Window.position.width * 0.95f, s_Window.position.height * 0.1f), "");
				GUILayout.BeginHorizontal();

				GUILayout.BeginVertical();
					if (model.IsBuilding())
					{
						if (model.GetCurrentBuildMethods() == null)
							model.InitBuildMethods();

						GUILayout.Label((model.CurrentProfile < 1 ? "Building game" : "Building '" +
							model.BuilderProfilerNames[model.CurrentProfile] + "' profile") + ". Please wait...", _buildStyle);
						GUILayout.Label((model.m_bBuildPaused ? "Next" : "Current") + " method: " + model.GetCurrentBuildMethods()[model.CurrentBuildMethod].Method.Name, _buildStyle);
						GUILayout.Label("Number of warnings detected: " + model.BuildWarning, model.BuildWarning > 0 ? _buildWarningStyle : _buildStyle);
						GUILayout.Label("Number of errors detected: " + model.BuildErrors, model.BuildErrors > 0 ? _buildErrorStyle : _buildStyle);

						if (model.CurrentBuildMethod == model.GetCurrentBuildMethods().Length - 1)
							GUILayout.Label("Some threads are still running... Please wait.", _buildWarningStyle);
					}
					else if (model.IsSigning())
						GUILayout.Label("Signing...", _buildStyle);
					else if (model.m_bBuildAbort)
					{
						if (model.m_bBuildAbortByUser)
							GUILayout.Label("Build abort by user.", _buildErrorStyle);
						else
							GUILayout.Label("Build abort. Check Console for info.", _buildErrorStyle);
					}
					else
					{
						if (model.BuildErrors > 0)
							GUILayout.Label("Build complete. " + model.BuildErrors + " errors detected.", _buildErrorStyle);
						else if (model.BuildWarning > 0)
							GUILayout.Label("Build complete. " + model.BuildWarning + " warnings detected.", _buildWarningStyle);
						else
							GUILayout.Label("Build successfully complete!", _buildStyle);
					}
				GUILayout.EndVertical();

				if (model.BMBuilderState == BuilderModel.BuilderState.BuildComplete)
				{
					if (!model.m_bBuildAbort && model.BuilderTarget.m_eBuildTarget == BuildTarget.Android && GUILayout.Button("Install", GUILayout.Width(90f), GUILayout.Height(60f)))
						InstallApk(model.BMBuilderState);

					if (GUILayout.Button("OK"))
						ChangeBuilderWindow(BuilderModel.BuilderState.None);
				}
				else if (model.m_bBuildPaused)
				{
					GUILayout.BeginHorizontal(GUILayout.Width(model.m_bBuildErrorPause ? 270f : 180f));

					model.m_bBuildAbort = GUILayout.Button("Abort", GUILayout.Width(90f), GUILayout.Height(60f));
					bool bRetry = false;

					if (model.m_bBuildErrorPause && GUILayout.Button("Retry", GUILayout.Width(90f), GUILayout.Height(60f)))
						bRetry = model.RetryBuildMethod();

					if (!bRetry)
					{
						model.m_bBuildPaused = !GUILayout.Button(model.m_bBuildErrorPause ? "Ignore" : "Continue", GUILayout.Width(90f), GUILayout.Height(60f));

						if (!model.m_bBuildPaused)
							model.m_bBuildErrorPause = false;
					}

					if (model.m_bBuildAbort)
						model.AbortBuild();

					GUILayout.EndHorizontal();
				}
				else if (model.IsSigning())
				{
					GUILayout.BeginHorizontal(GUILayout.Width(180f));

					if (Utils.m_ThreadsRunning.Count < 1 && Utils.m_ThreadsToRun.Count < 1)
					{
						if (GUILayout.Button("Install", GUILayout.Width(90f), GUILayout.Height(60f)))
							InstallApk(model.BMBuilderState);

						if (GUILayout.Button("OK", GUILayout.Width(90f), GUILayout.Height(60f)))
							ChangeBuilderWindow(BuilderModel.BuilderState.None);
					}
					else
					{
						if (GUILayout.Button("Cancel", GUILayout.Width(90f), GUILayout.Height(60f)))
							ChangeBuilderWindow(BuilderModel.BuilderState.None);
					}
					GUILayout.EndHorizontal();
				}

				GUILayout.EndHorizontal();
				GUILayout.EndArea();

				GUILayout.BeginArea(new Rect( s_Window.position.width * 0.025f, s_Window.position.height * 0.15f, s_Window.position.width * 0.95f, s_Window.position.height * 0.8f ), "");
					_vBuildInfosPosition = GUILayout.BeginScrollView(_vBuildInfosPosition);
					GUILayout.TextArea(_sBuildInfos, _buildInfosStyle);
					GUILayout.EndScrollView();
				GUILayout.EndArea();
			}
			return true;
		} // GUI

#endregion

#region Log

		private void UpdateBuildInfos(string sInfo, MessageType type = MessageType.Info, bool bHelpBox = true, bool bIncrementErrors = false, bool bNewLine = true)
		{
			if (string.IsNullOrEmpty(sInfo))
				return;

			if (model.BMBuilderState != BuilderModel.BuilderState.None)
			{
				if (bNewLine && sInfo.Substring(sInfo.Length - 3) == "...")
					bNewLine = false;
				if (sInfo.Length > 1 && _sBuildInfos.Length > 0 && _sBuildInfos[_sBuildInfos.Length - 1] != '\n')
					_sBuildInfos += '\n';

				if (type == MessageType.Warning)
				{
					_sBuildInfos += "<color=#ffd700>";
				}
				else if (type == MessageType.Error)
				{
					_sBuildInfos += "<color=#cc0000>";
				}
				else
					_sBuildInfos += "<color=#008000>";

				_sBuildInfos += sInfo;
				_sBuildInfos += "</color>";
				if (bNewLine)
					_sBuildInfos += '\n';

				CheckTextAreaMaxLength(ref _sBuildInfos);
				if (bNewLine)
					_vBuildInfosPosition.y = 13337f;

				if (Thread.CurrentThread == _mainThread)
					Repaint();

				return;
			}

			try
			{
				if (bHelpBox && Thread.CurrentThread == _mainThread)
					EditorGUILayout.HelpBox(sInfo, type);
				else
					DefaultLog(sInfo, type);
			}
			catch (Exception)
			{
				DefaultLog(sInfo, type);
			}
		}

		private void CheckTextAreaMaxLength(ref string sText)
		{
			if (sText.Length <= 16384)
				return;

			sText = sText.Substring(sText.Length - 16383);
		}

		private void DefaultLog(string sInfo, MessageType type = MessageType.Info)
		{
			switch (type)
			{
				case MessageType.Info:
					Debug.Log(sInfo);
					break;
				case MessageType.Warning:
					Debug.LogWarning(sInfo);
					break;
				case MessageType.Error:
					Debug.LogError(sInfo);
					break;
			}
		}

#endregion

		private void BundleVersion()
		{
			if (string.IsNullOrEmpty(model.CurrentBuilderProfile.m_sVersion))
				model.CurrentBuilderProfile.m_sVersion = PlayerSettings.bundleVersion;

			if (TextFieldPattern(ref model.CurrentBuilderProfile.m_sVersion, "Project Version", Constants.REGEX_VERSION_STRING))
				model.SetBundleVersion(model.CurrentBuilderProfile.m_sVersion);
			else
				UpdateBuildInfos("The Version number is wrong. Please set a correct one!", MessageType.Error);
		}

		public bool TextFieldPattern(ref string sValueLabel, string sLabel, string sPattern, bool bRemoveContent = false)
		{
			Color editorColor = GUI.color;
			bool bSucceed = true;

			if (sValueLabel == null)
				sValueLabel = string.Empty;

			if (sValueLabel == string.Empty || !Regex.Match(sValueLabel, sPattern).Success)
			{
				GUI.color = Color.red;
				bSucceed = false;
			}

			sValueLabel = EditorGUILayout.TextField(sLabel, sValueLabel);
			if (!Regex.Match(sValueLabel, sPattern).Success && bRemoveContent)
			{
				sValueLabel = string.Empty;
				bSucceed = false;
			}

			GUI.color = editorColor;
			return bSucceed;
		}

		void OnDestroy()
		{
			EditorApplication.update -= Building;
		}

		void OnDisable()
		{
			if (focusedWindow)
				EditorApplication.update -= Building;
		}

		public void OnEnable()
		{
			if (focusedWindow)
			{
				EditorApplication.update += Building;
				hideFlags = HideFlags.HideAndDontSave;
			}

			if (model != null && model.Countries == null)
				model.InitCountryLanguages();
		}
	}
}
#endif