namespace AllMyScripts.Editor.Builder.Android
{
    using System.Collections.Generic;
    using System.IO;
    using UnityEditor;
    using UnityEngine;

    using AllMyScripts.Common.Tools;
    public static class AndroidUtils
	{
		// Assets path for thread purpose
		private static readonly string ASSETS_PATH = Application.dataPath;

		private static Dictionary<string, List<string>> m_androidPluginAssetsDic =
			new Dictionary<string, List<string>>();

		public static bool CheckAndroidSDKAndJDK()
		{
			bool bOkBuild = true;
			if (string.IsNullOrEmpty(EditorPrefs.GetString("AndroidSdkRoot")))
			{
				bOkBuild = false;
				EditorUtility.DisplayDialog("Android SDK install location not set", "You didn't set the Android SDK Location", "SET");
				string sSDKInstallFolder = EditorUtility.OpenFolderPanel("Android SDK install location", string.Empty, string.Empty);
				if (!string.IsNullOrEmpty(sSDKInstallFolder))
				{
					EditorPrefs.SetString("AndroidSdkRoot", sSDKInstallFolder);
					bOkBuild = true;
				}
			}

			if (EditorPrefs.GetBool("JdkUseEmbedded", false))
			{
				bOkBuild = true;
			}
			else
			{
				if (string.IsNullOrEmpty(EditorPrefs.GetString("JdkPath")))
				{
					bOkBuild = false;
					EditorUtility.DisplayDialog("JDK location not set", "You didn't set the JDK Location", "SET");
					string sJDKInstallFolder = EditorUtility.OpenFolderPanel("JDK install location", string.Empty, string.Empty);
					if (!string.IsNullOrEmpty(sJDKInstallFolder))
					{
						EditorPrefs.SetString("JdkPath", sJDKInstallFolder);
						bOkBuild = true;
					}
				}
			}

			return bOkBuild;
		}

		public static void VerifyAndroidLibsFolders()
		{
			string sPluginsDir = Path.Combine( ASSETS_PATH, "Plugins" );
			string sAndroidDir = Path.Combine( sPluginsDir, "Android" );
			string sTmpDir = Path.Combine( sPluginsDir, "libs" );
			if( Directory.Exists( sAndroidDir ) )
			{
				DirectoryInfo root = new DirectoryInfo( sAndroidDir );
				DirectoryInfo[] dirs = root.GetDirectories();
				foreach( DirectoryInfo dir in dirs )
				{
					string sNameDir = Path.Combine( sAndroidDir, dir.Name );
					PluginImporter androidPluginImporter = AssetImporter.GetAtPath( sNameDir ) as PluginImporter;
					if ( androidPluginImporter != null )
						androidPluginImporter.SetCompatibleWithPlatform( BuildTarget.Android, true );
					string sManifestFile = Path.Combine( sNameDir, "AndroidManifest.xml" );
					string sLibDir = Path.Combine( sNameDir, "libs" );
					if( Directory.Exists( sLibDir ) && !File.Exists( sManifestFile ) )
					{
						sLibDir = sLibDir.Replace( ASSETS_PATH, "Assets" );
						sTmpDir = sTmpDir.Replace( ASSETS_PATH, "Assets" );
						AssetDatabase.MoveAsset( sLibDir, sTmpDir );
						AssetDatabase.ImportAsset( sTmpDir, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ImportRecursive );
						string[] sPathSO = { "/x86", "/armeabi-v7a" };
						string sKeyCPU = "CPU";
						string[] sValue = { "x86", "ARMv7" };

						for ( int i = 0 ; i < sPathSO.Length ; ++i )
						{
							string sCurrentPath = sTmpDir + sPathSO[ i ];
							if ( AssetDatabase.IsValidFolder( sCurrentPath ) )
							{
								string[] sFileSoExtArray = Directory.GetFiles( sCurrentPath, "*.so", SearchOption.AllDirectories );
								foreach ( string sPathFile in sFileSoExtArray )
								{
									PluginImporter p = AssetImporter.GetAtPath( sPathFile ) as PluginImporter;
									if( p != null )
									{
										p.SetCompatibleWithPlatform( BuildTarget.Android, true );
										p.SetPlatformData( BuildTarget.Android, sKeyCPU, sValue[ i ] );
									}
								}
							}
						}
						AssetDatabase.MoveAsset( sTmpDir, sLibDir );
					}
				}
			}
		}

		public static void MergeAndroidAssetFolders()
		{
			m_androidPluginAssetsDic.Clear();
			string sPluginsDir = Path.Combine( ASSETS_PATH, "Plugins" );
			string sAndroidDir = Path.Combine( sPluginsDir, "Android" );
			string sTmpAssetsDir = Path.Combine( sAndroidDir, "assets" );

			if( Directory.Exists( sTmpAssetsDir ) )
			{
				foreach( string sFile in Directory.GetFiles( sTmpAssetsDir ) )
					File.Delete( sFile );
				Directory.Delete( sTmpAssetsDir );
			}

			if( Directory.Exists( sAndroidDir ) )
			{
				DirectoryInfo root = new DirectoryInfo( sAndroidDir );
				DirectoryInfo[] dirs = root.GetDirectories();
				foreach( DirectoryInfo dir in dirs )
				{
					List<string> assetNames = new List<string>();
					string sAssetDir = IOUtility.CombineMultiplePaths( sAndroidDir, dir.Name, "assets" );

					if( Directory.Exists( sAssetDir ) )
					{
						if( !Directory.Exists( sTmpAssetsDir ) )
							AssetDatabase.CreateFolder( "Assets/Plugins/Android", "assets" );

						foreach( string sFile in Directory.GetFiles( sAssetDir ) )
						{
							string sFileName = Path.GetFileName( sFile );
							if( !sFile.EndsWith( "meta" ) )
							{
								assetNames.Add( sFileName );
								AssetDatabase.MoveAsset( "Assets/Plugins/Android/" + dir.Name + "/assets/" + sFileName, "Assets/Plugins/Android/assets/" + sFileName );
							}
						}
					}
					m_androidPluginAssetsDic.Add( dir.Name, assetNames );
				}
			}
		}

		public static void RestoreAndroidAssetFolders()
		{
			string sAssetDir = ASSETS_PATH + "/Plugins/Android/assets";
			if( Directory.Exists( sAssetDir ) )
			{
				foreach(
					KeyValuePair<string, List<string>> element in m_androidPluginAssetsDic )
				{
					if( element.Value.Count > 0 )
					{
						for( int i = 0; i < element.Value.Count; ++i )
							AssetDatabase.MoveAsset( "Assets/Plugins/Android/assets/" + element.Value[i], "Assets/Plugins/Android/" + element.Key + "/assets/" + element.Value[i] );
					}
				}
				AssetDatabase.DeleteAsset( "Assets/Plugins/Android/assets" );
			}
		}

		#region AndroidUtils

		public static void ApkAdd( BuilderModel buildModel, string sRoot, string sApkPath, string sAddPath, string sDestFolder, bool bStarted )
		{
			if( Application.platform == RuntimePlatform.WindowsEditor )
                Utils.ThreadProcess( buildModel, sRoot + Constants.SEVENZIP_PATH, "a -tzip \"" + sApkPath + "\" " + sAddPath + " -xr!.svn -mx0", sDestFolder, !bStarted, true );
			else
                Utils.ThreadProcess( buildModel, "zip", "a \"" + sApkPath + "\" " + sAddPath + " -x.svn -0", sDestFolder, !bStarted, true );
		}

        public static void ApkUnsign( BuilderModel buildModel, string sRoot, string sApkPath, string sDestFolder, bool bStarted )
		{
			if( Application.platform == RuntimePlatform.WindowsEditor )
                Utils.ThreadProcess( buildModel, sRoot + Constants.SEVENZIP_PATH, "d \"" + sApkPath + "\" META-INF", sDestFolder, !bStarted, true );
			else
                Utils.ThreadProcess( buildModel, "zip", "-d \"" + sApkPath + "\" META-INF/\\*", sDestFolder, !bStarted, true );
		}

		public static void ApkZipAlign( BuilderModel buildModel, string sApkPathTemp, string sApkPathFinal,
										string sDestFolder, bool bStarted )
		{
			string sZipAlignPath = Utils.GetAndroidSdkToolsFolder();
			if( Application.platform == RuntimePlatform.WindowsEditor )
				sZipAlignPath = Path.Combine( Utils.GetAndroidSdkToolsFolder(), "zipalign.exe" );
			else
				sZipAlignPath = Path.Combine( Utils.GetAndroidSdkToolsFolder(), "zipalign" );

            Utils.ThreadProcess( buildModel, sZipAlignPath, "-f 4 \"" + sApkPathTemp + "\" \"" + sApkPathFinal + '"', sDestFolder, !bStarted, true );
		}

        public static void ApkSign( BuilderModel buildModel, string sApkPath, string sDestFolder, bool bStarted )
		{
			string sJarSignerPath = string.Empty;
			sJarSignerPath = EditorPrefs.GetString( "JdkPath" );

			if ( Application.platform == RuntimePlatform.WindowsEditor )
				sJarSignerPath = sJarSignerPath + Path.DirectorySeparatorChar + "bin" + Path.DirectorySeparatorChar + "jarsigner.exe";
			else
				sJarSignerPath = "jarsigner";

            Utils.ThreadProcess( buildModel, sJarSignerPath,
										"-keystore \"" + PlayerSettings.Android.keystoreName +
										"\" -storepass " + PlayerSettings.Android.keystorePass +
										" -keypass " + PlayerSettings.Android.keyaliasPass +
										" -digestalg SHA1 -sigalg MD5withRSA \"" + sApkPath + "\" " +
										PlayerSettings.Android.keyaliasName, sDestFolder, !bStarted, true );
		}

		public static void LaunchMonitor(BuilderModel buildModel)
		{
			if( string.IsNullOrEmpty( EditorPrefs.GetString( "AndroidSdkRoot" ) ) )
			{
				Debug.LogError( "Android SDK install location not set." );
				return;
			}

			string sDdmsPath = Path.Combine(Utils.GetAndroidSdkToolsFolder(true), "monitor.bat");
			if ( !File.Exists(sDdmsPath))
				sDdmsPath = Path.Combine(Utils.GetAndroidSdkToolsFolder(true), "ddms.bat");

			sDdmsPath = "\"" + sDdmsPath + "\"";

            Utils.RunProcess( buildModel, sDdmsPath, "", "", false, false, Constants.GetAssetsPath(), true );
		}

        public static void OpenManifest( BuilderModel buildModel, string sPath = "Plugins/Android/AndroidManifest.xml" )
		{
			if ( Application.platform == RuntimePlatform.WindowsEditor )
                Utils.RunProcess( buildModel, Path.Combine( Constants.GetAssetsPath(), sPath ), "", "", true, false, Constants.GetAssetsPath() );
			else if( Application.platform == RuntimePlatform.OSXEditor )
                Utils.RunProcess( buildModel, "open", sPath, "", true, false, Constants.GetAssetsPath() );
		}
		#endregion
	}
}
