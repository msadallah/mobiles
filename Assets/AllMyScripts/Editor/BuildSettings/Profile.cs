namespace AllMyScripts.Editor.Builder
{
    using System;
    using UnityEditor;
    using System.Collections.Generic;
    using System.IO;

    using AllMyScripts.Common.Tools;
    using AllMyScripts.LangManager;

    using AllMyScripts.Editor.Builder.Android;
    using AllMyScripts.Common.Tools.Xml;
    using UnityEngine;
    using AllMyScripts.Builder;

    [System.Serializable]
    public abstract class Profile
    {
		public enum VR_SDK
		{
			None,
			OpenVR
		}

		public abstract BuildTargetGroup typedBuildingTargetGroup

		{
			get;
		}
		// Index of lwBuilder.TARGETS_LIST in order to set the BuildTarget
		public abstract BuildTarget m_ePlatform
		{
			get;
		}

		private string _profilePath = string.Empty;

		public string m_sProductName = string.Empty;


        // it's the preprocess directive list
        public List<string> m_globalDefines = new List<string>();

        // the bundle id used for iOS, Android and MAC OS
        public string m_sBundleIdentifier = string.Empty;

        // The version of the application. It must be in X.XX.XXX format ( except the WebGL one it can be X.XX.XXX.XXX )
        public string m_sVersion = string.Empty;

        public List<string> m_selectedPlugins = new List<string>();

		public ScriptingImplementation m_scriptingBackend = ScriptingImplementation.Mono2x;
#if !UNITY_2019_3_OR_NEWER
		public ScriptingRuntimeVersion m_scriptingRuntimeVersion = ScriptingRuntimeVersion.Latest;
#endif
		public ApiCompatibilityLevel m_apiCompatibilityLevel = ApiCompatibilityLevel.NET_Standard_2_0;
		public Il2CppCompilerConfiguration m_compilerConfiguration = Il2CppCompilerConfiguration.Release;

		// The language culture for this build
		public List<string> m_sLanguageCultures = new List<string>();
		
        // Specific splash folder for a profile
        public string m_sSplashProfilePath = string.Empty;
        public string m_sSplashColor;
		public bool m_bUseSplashSystem;
		public bool m_bUseUnitySplash;
		public bool m_SplashDefinesInProfile = false;

		public List<string> m_pluginConfigName = new List<string>();
        public List<string> m_pluginConfigFilesName = new List<string>();

		public bool stripEngineCode;
		public ManagedStrippingLevel managedStrippingLevel;

		public List<SceneAsset> scenes;

		public List<BuildVariable> buildVariables = new List<BuildVariable>();

		public Texture2D iconTexture;

		public VR_SDK vrSdk;

		public static Profile GetTypedProfile(string filePath)
		{
			BuildTarget buildTarget = GetXmlType(GlobalTools.LoadTextFileAtFullPath(filePath));
			switch (buildTarget)
			{
				case BuildTarget.StandaloneWindows:
					return new StandaloneWindowsProfile(filePath);
				case BuildTarget.StandaloneWindows64:
					return new StandaloneWindows64Profile(filePath);
				case BuildTarget.StandaloneOSX:
					return new StandaloneOSXProfile(filePath);
#if !UNITY_2019_2_OR_NEWER
				case BuildTarget.StandaloneLinux:
					return new StandaloneLinuxProfile(filePath);
				case BuildTarget.StandaloneLinuxUniversal:
					return new StandaloneLinuxUniversalProfile(filePath);
#endif
				case BuildTarget.StandaloneLinux64:
					return new StandaloneLinux64Profile(filePath);
				case BuildTarget.iOS:
					return new IOSProfile(filePath);
				case BuildTarget.Android:
					return new AndroidProfile(filePath);
				case BuildTarget.WebGL:
					return new WebGLprofile(filePath);
			}
			return null;
		}

		public static Profile GetTypedProfile(Profile builderProfile)
		{
			BuildTarget buildTarget = builderProfile.m_ePlatform;
			switch (buildTarget)
			{
				case BuildTarget.StandaloneWindows:
					return new StandaloneWindowsProfile(builderProfile);
				case BuildTarget.StandaloneWindows64:
					return new StandaloneWindows64Profile(builderProfile);
				case BuildTarget.StandaloneOSX:
					return new StandaloneOSXProfile(builderProfile);
#if !UNITY_2019_2_OR_NEWER
				case BuildTarget.StandaloneLinux:
					return new StandaloneLinuxProfile(builderProfile);
				case BuildTarget.StandaloneLinuxUniversal:
					return new StandaloneLinuxUniversalProfile(builderProfile);
#endif
				case BuildTarget.StandaloneLinux64:
					return new StandaloneLinux64Profile(builderProfile);
				case BuildTarget.iOS:
					return new IOSProfile(builderProfile);
				case BuildTarget.Android:
					return new AndroidProfile(builderProfile);
				case BuildTarget.WebGL:
					return new WebGLprofile(builderProfile);
			}
			return null;
		}

		public static Profile GetTypedProfile(Profile builderProfile, Target newTarget)
		{
			BuildTarget buildTarget = builderProfile.m_ePlatform;
			Profile p = null;
			switch (newTarget.m_eBuildTarget)
			{
				case BuildTarget.StandaloneWindows:
					p = new StandaloneWindowsProfile(builderProfile);
					break;
				case BuildTarget.StandaloneWindows64:
					p = new StandaloneWindows64Profile(builderProfile);
					break;
				case BuildTarget.StandaloneOSX:
					p = new StandaloneOSXProfile(builderProfile);
					break;
#if !UNITY_2019_2_OR_NEWER
				case BuildTarget.StandaloneLinux:
					p = new StandaloneLinuxProfile(builderProfile);
					break;
				case BuildTarget.StandaloneLinuxUniversal:
					p = new StandaloneLinuxUniversalProfile(builderProfile);
					break;
#endif
				case BuildTarget.StandaloneLinux64:
					p = new StandaloneLinux64Profile(builderProfile);
					break;
				case BuildTarget.iOS:
					p = new IOSProfile(builderProfile);
					break;
				case BuildTarget.Android:
					p = new AndroidProfile(builderProfile);
					break;
				case BuildTarget.WebGL:
					p = new WebGLprofile(builderProfile);
					break;
			}
			return p;
		}

		public Profile(string filePath)
		{
			_profilePath = filePath;
			scenes = new List<SceneAsset>();
			ReadXml(GlobalTools.LoadTextFileAtFullPath(_profilePath));
			if (buildVariables.Count <= 0)
				buildVariables.Add(new BuildVariable("DEVELOPMENT_BUILD", "true", ProjTarget.Both));
		}

        protected Profile(Profile builderProfile)
        {
            if (builderProfile == null) 
				return;
			if (builderProfile.m_ePlatform  != m_ePlatform)
			{
				Debug.LogError("The profile to copy is of a diffrent Type !");
			}

            m_globalDefines = new List<string>(builderProfile.m_globalDefines);
            m_sBundleIdentifier = builderProfile.m_sBundleIdentifier;

            m_sVersion = builderProfile.m_sVersion;

            m_selectedPlugins = new List<string>(builderProfile.m_selectedPlugins);

			m_sLanguageCultures = new List<string>(builderProfile.m_sLanguageCultures);

			//Plugin part
			m_pluginConfigName = builderProfile.m_pluginConfigName;
			m_pluginConfigFilesName = builderProfile.m_pluginConfigFilesName;

			//Specific scripting element
			m_scriptingBackend = builderProfile.m_scriptingBackend;
#if !UNITY_2019_3_OR_NEWER
			m_scriptingRuntimeVersion = builderProfile.m_scriptingRuntimeVersion;
#endif
			m_apiCompatibilityLevel = builderProfile.m_apiCompatibilityLevel;
			m_compilerConfiguration = builderProfile.m_compilerConfiguration;

			buildVariables = builderProfile.buildVariables;
			if (buildVariables.Count <= 0)
				buildVariables.Add(new BuildVariable("DEVELOPMENT_BUILD", "true", ProjTarget.Both));

			stripEngineCode = builderProfile.stripEngineCode;
			managedStrippingLevel = builderProfile.managedStrippingLevel;

			vrSdk = VR_SDK.None;

			//Icons & splash & scenes part
			iconTexture = builderProfile.iconTexture;
			m_SplashDefinesInProfile = builderProfile.m_SplashDefinesInProfile;
			m_sSplashProfilePath = builderProfile.m_sSplashProfilePath;
			m_bUseSplashSystem = builderProfile.m_bUseSplashSystem;
			m_bUseUnitySplash = builderProfile.m_bUseUnitySplash;
			m_sSplashColor = builderProfile.m_sSplashColor;
			scenes = builderProfile.scenes;
		}

		private static BuildTarget GetXmlType(string sXml)
		{
			if (string.IsNullOrEmpty(sXml))
				return BuildTarget.StandaloneWindows;

			XmlReader xr = new XmlReader();
			if (xr.Init(sXml))
			{
				while (xr.Read())
				{
					if (!xr.IsNodeElement())
						continue;

					switch (xr.Name.ToUpper())
					{
						case "PLATFORM":
							return xr.GetAttributeEnum("value", BuildTarget.StandaloneWindows);
					}
				}
				xr.Close();
			}
			return BuildTarget.StandaloneWindows;
		}

        protected virtual void ReadXml(string sXml)
        {
            // special case for an empty profile
            if (string.IsNullOrEmpty(sXml)) return;

            XmlReader xr = new XmlReader();
			scenes.Clear();

			if (xr.Init(sXml))
            {
                while (xr.Read())
                {
                    if (!xr.IsNodeElement()) continue;
					XmlOnReadNodeElement(xr, xr.Name.ToUpper());
                }
                xr.Close();
            }
        }

		protected virtual void XmlOnReadNodeElement(XmlReader xr, string node)
		{
			switch (node)
			{
				case "PLATFORM":
					if (xr.GetAttributeEnum("value", BuildTarget.StandaloneWindows) != m_ePlatform)
					{
						Debug.LogError("The profile to copy is of a diffrent Type !");
					}
					break;
				case "PRODUCT_NAME":
					m_sProductName = xr.GetAttribute("value");
					break;
				case "GLOBALDEFINES":
					XmlReader globalDefinesReader = xr.ReadSubtree();
					if (globalDefinesReader != null)
					{
						while (globalDefinesReader.Read())
						{
							switch (globalDefinesReader.Name.ToUpper())
							{
								case "GLOBALDEFINE":
									m_globalDefines.Add(globalDefinesReader.GetAttribute("value"));
									break;
							}
						}
					}
					break;
				case "BUNDLE_IDENTIFIER":
					m_sBundleIdentifier = xr.GetAttribute("value");
					break;
				case "PROJECT_VERSION":
				case "IOS_BUNDLE_VERSION":
				case "ANDROID_BUNDLE_VERSION":
					m_sVersion = xr.GetAttribute("value", "0.0.0");
					break;
				case "PLUGINS":
					XmlReader pluginsReader = xr.ReadSubtree();
					if (pluginsReader != null)
					{
						while (pluginsReader.Read())
						{
							switch (pluginsReader.Name.ToUpper())
							{
								case "PLUGIN":
									string sPath = pluginsReader.GetAttribute("value");
									if (sPath.Contains(Path.DirectorySeparatorChar + "Assets" + Path.DirectorySeparatorChar))
										sPath = sPath.Substring(sPath.IndexOf(Path.DirectorySeparatorChar + "Assets" + Path.DirectorySeparatorChar, StringComparison.Ordinal) + 7);
									sPath = IOUtility.ReplacePathSeparator(sPath);
									m_selectedPlugins.Add(sPath);
									break;
							}
						}
					}
					break;
				case "LANGUAGES":
					XmlReader langsReader = xr.ReadSubtree();
					if (langsReader != null)
					{
						while (langsReader.Read())
						{
							switch (langsReader.Name.ToUpper())
							{
								case "LANGUAGE":
									m_sLanguageCultures.Add(langsReader.GetAttribute("value"));
									break;
							}
						}
					}
					break;
				case "ICON_PATH":
					string iconTexturePath = IOUtility.ReplacePathSeparator(xr.GetAttribute("value", string.Empty));
					iconTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(iconTexturePath, typeof(Texture2D));
					break;
				case "SPLASHS_FOLDER":
					m_SplashDefinesInProfile = true;
					m_bUseSplashSystem = xr.GetAttributeBool("useSplashSystem", true);
					m_bUseUnitySplash = xr.GetAttributeBool("useUnitySplash", false);
					m_sSplashProfilePath = xr.GetAttribute("value", string.Empty);
					m_sSplashProfilePath = IOUtility.ReplacePathSeparator(m_sSplashProfilePath);
					m_sSplashColor = xr.GetAttribute("backgroundcolor", string.Empty);
					break;
				case "PLUGIN_CONFIG_FILE":
					m_pluginConfigName.Add(xr.GetAttribute("name", string.Empty));
					m_pluginConfigFilesName.Add(xr.GetAttribute("file", string.Empty));
					break;
				case "SCRIPTING":
					m_scriptingBackend = xr.GetAttributeEnum("backend", ScriptingImplementation.IL2CPP); //Mono2x OR IL2CPP OR WinRTDotNET
#if !UNITY_2019_3_OR_NEWER
					m_scriptingRuntimeVersion = xr.GetAttributeEnum("runtimeVersion", ScriptingRuntimeVersion.Latest); //Legacy OR Latest
#endif
					m_apiCompatibilityLevel = xr.GetAttributeEnum("apiCompatibilityLevel", ApiCompatibilityLevel.NET_Standard_2_0); //See documentation : https://docs.unity3d.com/ScriptReference/ApiCompatibilityLevel.html
					m_compilerConfiguration = xr.GetAttributeEnum("compilerConfig", Il2CppCompilerConfiguration.Release); //See documentation : https://docs.unity3d.com/ScriptReference/Il2CppCompilerConfiguration.html
					stripEngineCode = xr.GetAttributeBool("stripEngineCode", true);
					managedStrippingLevel = xr.GetAttributeEnum<ManagedStrippingLevel>("managedStrippingLevel", ManagedStrippingLevel.Low);
					vrSdk = xr.GetAttributeEnum("vrSdk", VR_SDK.None);
					break;
				case "SCENES":
					XmlReader scenesReader = xr.ReadSubtree();
					if (scenesReader != null)
					{
						while (scenesReader.Read())
						{
							switch (scenesReader.Name.ToUpper())
							{
								case "SCENE":
									string path = scenesReader.GetAttribute("path");
									SceneAsset sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(path);
									scenes.Add(sceneAsset);
									break;
							}
						}
					}
					break;

				case "PROJSETTINGS":
					XmlReader projSettings = xr.ReadSubtree();
					ProjSettings.Variables.Clear();
					if (projSettings != null)
					{
						while (projSettings.Read())
						{
							switch (projSettings.Name.ToUpper())
							{
								case "VARIABLE":
									string name = projSettings.GetAttribute("name");
									string value = projSettings.GetAttribute("value");
									ProjTarget target = projSettings.GetAttributeEnum("target", ProjTarget.Editor);
									BuildVariable projVarBuild = new BuildVariable(name, value, target);
									ProjSettings.Variables.Add(name, projVarBuild);
									buildVariables.Add(projVarBuild);
									break;
								default:
									break;
							}
						}
					}
					break;
			}
		}


		public virtual void WriteXml(string sProfilePath, BuildTarget buildTarget, List<string> sPluginsPath, XmlWriter xw = null)
		{
			if (xw == null)
			{
				xw = new XmlWriter();
			}

            xw.WriteStartElement("BUILDERPROFILE");
			xw.WriteCommentLine( "Platform & Product & Version" );
			xw.WriteStartElement("PLATFORM");
            xw.WriteAttribute("value", m_ePlatform.ToString());
            xw.WriteEndElement();

            xw.WriteStartElement("PRODUCT_NAME");
            xw.WriteAttribute("value", m_sProductName);
            xw.WriteEndElement();

            xw.WriteStartElement("PROJECT_VERSION");
            xw.WriteAttribute("value", m_sVersion);
            xw.WriteEndElement();

			if ( buildTarget == BuildTarget.Android || buildTarget == BuildTarget.iOS )
			{
				xw.WriteStartElement( "BUNDLE_IDENTIFIER" );
				xw.WriteAttribute( "value", m_sBundleIdentifier );
				xw.WriteEndElement();
			}

			xw.WriteCommentLine("Global Defines");
			if (m_globalDefines.Count > 0)
			{
				xw.WriteStartElement("GLOBALDEFINES");
				for (int i = 0; i < m_globalDefines.Count; i++)
				{
					xw.WriteStartElement("GLOBALDEFINE");
					xw.WriteAttribute("value", m_globalDefines[i]);
					xw.WriteEndElement();
				}
				xw.WriteEndElement();
			}

			xw.WriteCommentLine("Plugins");
			if (m_selectedPlugins.Count > 0)
			{
				xw.WriteStartElement("PLUGINS");
				for (int i = 0; i < m_selectedPlugins.Count; i++)
				{
					if (sPluginsPath.Contains(m_selectedPlugins[i]))
					{
						xw.WriteStartElement("PLUGIN");
						xw.WriteAttribute("value", m_selectedPlugins[i].Replace("\\", "/"));
						xw.WriteEndElement();
					}
					else
					{
						m_selectedPlugins.RemoveAt(i);
						i--;
					}
				}
				xw.WriteEndElement();
			}

			for ( int i = 0 ; i < m_pluginConfigName.Count ; i++ )
			{
				xw.WriteStartElement( "PLUGIN_CONFIG_FILE" );
				xw.WriteAttribute( "name", m_pluginConfigName[ i ] );
				xw.WriteAttribute( "file", m_pluginConfigFilesName[ i ] );
				xw.WriteEndElement();
			}

			WriteTypedPluginsSettings(xw, buildTarget);

			//Scripting settings
			xw.WriteCommentLine( "Scripting & Stripping" );
			xw.WriteStartElement("SCRIPTING");
			xw.WriteAttribute("backend", buildTarget == BuildTarget.iOS ? "IL2CPP" : m_scriptingBackend.ToString());
#if !UNITY_2019_3_OR_NEWER
			xw.WriteAttribute("runtimeVersion", m_scriptingRuntimeVersion.ToString());
#endif
			xw.WriteAttribute("apiCompatibilityLevel", m_apiCompatibilityLevel.ToString());
			xw.WriteAttribute("compilerConfig", buildTarget == BuildTarget.iOS ? "Release" : m_compilerConfiguration.ToString());
			xw.WriteAttributeBool("stripEngineCode", stripEngineCode);
			xw.WriteAttribute("managedStrippingLevel", managedStrippingLevel.ToString());
			xw.WriteAttribute("vrSdk", vrSdk.ToString());

			WriteTypedScriptingSettings(xw, buildTarget);

			xw.WriteEndElement();

			WriteTypedSettings(xw, buildTarget);

			// Langs
			xw.WriteCommentLine("Languages");
			if (m_sLanguageCultures.Count > 0)
			{
				xw.WriteStartElement("LANGUAGES");
				for (int i = 0; i < m_sLanguageCultures.Count; i++)
				{
					//we check if the language culture to save exist in our countries handled
					Country country = CountryCode.GetCountry( m_sLanguageCultures[i] );
					if (country != null)
					{
						xw.WriteStartElement("LANGUAGE");
						xw.WriteAttribute("value", m_sLanguageCultures[i]);
						xw.WriteEndElement();
					}
					else
					{
						m_sLanguageCultures.RemoveAt(i);
						i--;
					}
				}
				xw.WriteEndElement();
			}

			xw.WriteCommentLine( "Icons & Splashscreens" );
			xw.WriteStartElement("ICON_PATH");
			xw.WriteAttribute( "value", AssetDatabase.GetAssetPath(iconTexture));
			xw.WriteEndElement();

			xw.WriteStartElement( "SPLASHS_FOLDER" );
			xw.WriteAttributeBool("useSplashSystem", m_bUseSplashSystem);
			if (m_bUseSplashSystem)
			{
				xw.WriteAttributeBool("useUnitySplash", m_bUseUnitySplash);
				xw.WriteAttribute( "splashPath", m_sSplashProfilePath.Replace("\\", "/"));
				xw.WriteAttribute("backgroundcolor", m_sSplashColor);
			}
			xw.WriteEndElement();


			xw.WriteCommentLine("Scenes");
			if(scenes.Count > 0)
			{
				xw.WriteStartElement("SCENES");
				foreach( SceneAsset scene in scenes )
				{
					xw.WriteStartElement("SCENE");
					xw.WriteAttribute("path", AssetDatabase.GetAssetPath(scene));
					xw.WriteEndElement();
				}
				xw.WriteEndElement();
			}

			xw.WriteCommentLine("ProjSettings");
			if (buildVariables.Count > 0)
			{
				xw.WriteStartElement("PROJSETTINGS");
				foreach (BuildVariable variable in buildVariables)
					variable.GenerateAsXml(xw);
				xw.WriteEndElement();
			}
			xw.WriteEndElement();

			File.WriteAllText( sProfilePath, xw.GetString() );
		}

		protected virtual void WriteTypedPluginsSettings(XmlWriter xw, BuildTarget buildTarget) { }

		protected virtual void WriteTypedScriptingSettings(XmlWriter xw, BuildTarget buildTarget) { }

		protected virtual void WriteTypedSettings(XmlWriter xw, BuildTarget buildTarget) { }

		public bool TryGetPluginConfig( string sName, out string sFile )
        {
            sFile = null;
            int nIdx = m_pluginConfigName.IndexOf(sName);
            if( nIdx >= 0 )
            {
                sFile = m_pluginConfigFilesName[nIdx];
                return true;
            }
            return false;
        }

		public void EditGlobalDefines(string sProfilePath)
		{
			string sXml = GlobalTools.LoadTextFileAtFullPath(sProfilePath);
			//TODO : Not implemented yet
		}

		public void ReloadGlobalDefines()
		{
			XmlReader xr = new XmlReader();
			m_globalDefines.Clear();
			if (xr.Init(GlobalTools.LoadTextFileAtFullPath(_profilePath)))
			{
				while (xr.Read())
				{
					if (!xr.IsNodeElement())
						continue;

					switch (xr.Name.ToUpper())
					{
						case "GLOBALDEFINES":
							XmlReader globalDefinesReader = xr.ReadSubtree();
							if (globalDefinesReader != null)
							{
								while (globalDefinesReader.Read())
								{
									switch (globalDefinesReader.Name.ToUpper())
									{
										case "GLOBALDEFINE":
											m_globalDefines.Add(globalDefinesReader.GetAttribute("value"));
											break;
									}
								}
							}
							break;
					}
				}
			}
		}
	}
}
