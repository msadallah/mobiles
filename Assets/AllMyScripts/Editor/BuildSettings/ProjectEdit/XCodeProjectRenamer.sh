#!/bin/sh

################################################################################
# usage:
#
#   renameXcodeProject.sh <OldProjectName> <NewProjectName>
#
# examples:
#
#   ./renameXcodeProject.sh OldName NewName
#   ./renameXcodeProject.sh "Old Name" "New Name"
#
################################################################################

OLDNAME=$1
NEWNAME=$2
PATHTOTEST=$3

# remove bad characters
OLDNAME=`echo "${OLDNAME}" | sed -e "s/[^a-zA-Z0-9_ -]//g"`
NEWNAME=`echo "${NEWNAME}" | sed -e "s/[^a-zA-Z0-9_ -]//g"`

TMPFILE=/tmp/xcodeRename.$$

if [ "$OLDNAME" = "" -o "$NEWNAME" = "" ]; then
  echo "usage: $0 <OldProjectName> <NewProjectName>"
  exit
fi

echo "${NEWNAME}" | grep "${OLDNAME}" > /dev/null
if [ $? -eq 0 ]; then
  echo "Error: New project name cannot contain old project name. Use a tmp name first. Terminating."
  exit
fi

if [ ! -d "${PATHTOTEST}/${OLDNAME}" ]; then
  echo "ERROR: \"${PATHTOTEST}/${OLDNAME}\" directory not found. Terminating"
  exit
fi 

# set new project directory
if [ -d "${NEWNAME}" ]; then
  echo "ERROR: project directory \"${NEWNAME}\" exists. Terminating."
  exit
fi

# be sure tmp file is writable
cp /dev/null ${TMPFILE}
if [ $? -ne 0 ]; then
  echo "tmp file ${TMPFILE} is not writable. Terminating."
  exit
fi

# create project name with unscores for spaces
OLDNAMEUSCORE=`echo "${OLDNAME}" | sed -e "s/ /_/g"`
NEWNAMEUSCORE=`echo "${NEWNAME}" | sed -e "s/ /_/g"`

# copy project directory
#echo copying project directory from "${OLDNAME}" to "${NEWNAME}"
#cp -rp "${OLDNAME}" "${NEWNAME}"

# remove build directory
#echo removing build directory from "${NEWNAME}"
#rm -rf "${NEWNAME}/build"

#find text files, replace text
find "${PATHTOTEST}" | while read currFile
do
  # find files that are of type text
  file "${currFile}" | grep "text" > /dev/null
  if [ $? -eq 0 ]; then
    # see if old proj name with underscores is in the text
    grep "${OLDNAMEUSCORE}" "${currFile}" > /dev/null
    if [ $? -eq 0 ]; then
       # replace the text with new proj name
       echo found "${OLDNAMEUSCORE}" in "${currFile}", replacing...
       sed -e "s/${OLDNAMEUSCORE}/${NEWNAMEUSCORE}/g" "${currFile}" > ${TMPFILE}
       mv ${TMPFILE} "${currFile}"              
       cp /dev/null ${TMPFILE}
    fi
    # see if old proj name is in the text
    grep "${OLDNAME}" "${currFile}" > /dev/null
    if [ $? -eq 0 ]; then
       # replace the text with new proj name
       echo found "${OLDNAME}" in "${currFile}", replacing...
       sed -e "s/${OLDNAME}/${NEWNAME}/g" "${currFile}" > ${TMPFILE}
       mv ${TMPFILE} "${currFile}"              
       cp /dev/null ${TMPFILE}
    fi
  fi
done

# rename directories with underscores
find "${PATHTOTEST}" -path "${PATHTOTEST}"/build -prune -o -type dir | while read currFile
do
  echo "${currFile}" | grep "${OLDNAMEUSCORE}" > /dev/null
  if [ $? -eq 0 ]; then
    MOVETO=`echo "${currFile}" | sed -e "s/${OLDNAMEUSCORE}/${NEWNAMEUSCORE}/g"`
    echo renaming "${currFile}" to "${MOVETO}"
    mv "${currFile}" "${MOVETO}"
  fi
done

# rename directories with spaces
find "${PATHTOTEST}" -path "${PATHTOTEST}"/build -prune -o -type dir | while read currFile
do
  echo "${currFile}" | grep "${OLDNAME}" > /dev/null
  if [ $? -eq 0 ]; then
    MOVETO=`echo "${currFile}" | sed -e "s/${OLDNAME}/${NEWNAME}/g"`
    echo renaming "${currFile}" to "${MOVETO}"
    mv "${currFile}" "${MOVETO}"
  fi
done

# rename files with underscores
find "${PATHTOTEST}" -type file | while read currFile
do
  echo "${currFile}" | grep "${OLDNAMEUSCORE}" > /dev/null
  if [ $? -eq 0 ]; then
    MOVETO=`echo "${currFile}" | sed -e "s/${OLDNAMEUSCORE}/${NEWNAMEUSCORE}/g"`
    echo renaming "${currFile}" to "${MOVETO}"
    mv "${currFile}" "${MOVETO}"
  fi
done

# rename files with spaces
find "${PATHTOTEST}" -type file | while read currFile
do
  echo "${currFile}" | grep "${OLDNAME}" > /dev/null
  if [ $? -eq 0 ]; then
    MOVETO=`echo "${currFile}" | sed -e "s/${OLDNAME}/${NEWNAME}/g"`
    echo renaming "${currFile}" to "${MOVETO}"
    mv "${currFile}" "${MOVETO}"
  fi
done
#rm -f ${TMPFILE}
echo finished.