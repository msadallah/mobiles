#!/bin/bash

OWNERNAME="$1"
APPNAME="$2"
RELEASENOTES="$3"
TEAMSNAME="$4"
BUILDPATH="$5"
DSYMPATH="$6"
BUILD_TARGET="$7"
BUILD_VERSION="$8"


#**********************************************************************************************
#********************************* MIXED API/CLI PART *****************************************
#**********************************************************************************************

echo "--------------------------- Upload AppCenter ---------------------------"

BUILD_TARGET_LOWER="$(tr '[:upper:]' '[:lower:]' <<< "$BUILD_TARGET")"

echo "Testing Path: ${BUILDPATH}"

# For Windows target, we need to create the zip file
# In order to upload it on AppCenter
if [[ "$BUILD_TARGET_LOWER" = "windows" ]]; then
    if [ -d "$BUILDPATH" ]; then
		# Setting the current context on the parent folder Path
        cd `dirname "$BUILDPATH"` || exit 
        FOLDERNAME=`basename "$BUILDPATH"`
        zip -q -r "${FOLDERNAME}.zip" "./${FOLDERNAME}";
        BUILDPATH="./${FOLDERNAME}.zip"
    fi
fi

echo "Uploading from Path: ${BUILDPATH}"
echo "Uploading version : ${BUILD_VERSION}"

#Upload the build to the AppCenter
appcenter distribute release --silent --token ${APPCENTER_TOKEN} -a "${OWNERNAME}/${APPNAME}" -g "Unity-Dev" -r "${RELEASENOTES}" -f "${BUILDPATH}" -b "${BUILD_VERSION}"

echo "Value => ${OWNERNAME} & ${APPNAME} / ${RELEASENOTES} / ${BUILDPATH} / ${DSYMPATH} / ${TEAMSNAME} / ${BUILD_VERSION}"

if [[ "$BUILD_TARGET_LOWER" = "android" ]]; then
	echo "TODO specific case for breakpad android stuff"
	#TODO Add maybe specific case for Android but for this we will need breakpad integration to generate symbols for an apk.
	#appcenter crashes -b "${DSYMPATH}" -a "${OWNERNAME}/${APPNAME}" --token ${APPCENTER_TOKEN}
elif [[ "$BUILD_TARGET_LOWER" = "ios" ]]; then
	echo "Upload symbols on iOS platform"
	#Add specific iOS case where you have to add dSym on appcenter too
	appcenter crashes upload-symbols -s "${DSYMPATH}" -a "${OWNERNAME}/${APPNAME}" --token ${APPCENTER_TOKEN}
fi

#Extract release-id from the last release for an app 
RELEASE_ID=$(appcenter distribute releases list -a "$OWNERNAME/$APPNAME" | grep '^ID' | sort -rV | head -n 1 | sed 's/[^0-9]*//g')

#Iterate on the team ids to generate each JSON part to generate the final command line 
TEAM_ARRAY=($TEAMSNAME)
for TEAM in "${TEAM_ARRAY[@]}"; do
	echo "Add the team $TEAM to the current release ${APPNAME}"
	appcenter distribute releases add-destination -s -r "$RELEASE_ID" -d "$TEAM" -t "group"  -a "$OWNERNAME/$APPNAME"
done

#**********************************************************************************************
#**********************************************************************************************
#**********************************************************************************************



#**********************************************************************************************
#********************************** APPCENTER-CLI PART ****************************************
#**********************************************************************************************

#First part is using appcenter-cli but due to some limitation with their CLI currentl we don't gonna use it.
#appcenter distribute release --silent --token ${APPCENTER_TOKEN} -a "${OWNERNAME}/${APPNAME}" -r ${RELEASENOTES} -f "${BUILDPATH}" -g "${TEAMSNAME}"

#BUILD_TARGET_LOWER="$(tr '[:upper:]' '[:lower:]' <<< "$BUILD_TARGET")"
#if [[ BUILD_TARGET_LOWER = *"android"* ]]; then 
#	echo "TODO specific case for breakpad android stuff"
#	#TODO Add maybe specific case for Android but for this we will need breakpad integration to generate symbols for an apk.
#	#appcenter crashes -b "${DSYMPATH}" -a "${OWNERNAME}/${APPNAME}" --token ${APPCENTER_TOKEN}
#elif [BUILD_TARGET_LOWER = *"ios"*]; then 
#	#Add specific iOS case where you have to add dSym on appcenter too
#	appcenter crashes upload-symbols -s "${DSYMPATH}" -a "${OWNERNAME}/${APPNAME}" --token ${APPCENTER_TOKEN}
#fi

#**********************************************************************************************
#**********************************************************************************************
#**********************************************************************************************





#**********************************************************************************************
#************************************** CURL PART *********************************************
#**********************************************************************************************

#Generate the UPLOAD_URL to allow us to upload our release on app center application space
#UPLOAD_URL_APP=$(curl -X POST --header "Content-Type:application/json" --header "Accept:application/json" --header "X-API-Token:${APPCENTER_TOKEN}" "https://api.appcenter.ms/v0.1/apps/${OWNERNAME}/${APPNAME}/release_uploads" | jq '.upload_url')
#upload the application iOS or Android it's the same term even if you use ipa (Apple term)
#curl -F "ipa=@${BUILDPATH}" $UPLOAD_URL_APP
#Path the application to trigger their system with the submission.
#curl -X PATCH --header "Content-Type:application/json" --header "Accept:application/json" --header "X-API-Token:${APPCENTER_TOKEN}" -d '{ "status": "committed"  }' "${UPLOAD_URL}"

#Retrieve the lastest release id for an application. This will allow us to patch some application field just after.
#LASTEST_RELEASEID=$(curl -X GET --header "Content-Type:application/json" --header "Accept:application/json" --header "X-API-Token:${APPCENTER_TOKEN}" "https://api.appcenter.ms/v0.1/apps/${OWNERNAME}/${APPNAME}/releases" | jq .[0] | jq .id)

#Iterate on the team ids to generate each JSON part to generate the final command line 
#TEAM_NUMBER=0
#DISTRIBUTIONS_JSON="\"destinations\": ["
#TEAM_ARRAY=($TEAMSNAME)
#for TEAM in "${TEAM_ARRAY[@]}" do
#	if [$TEAM_NUMBER > 0]; then
#		DISTRIBUTIONS_JSON="${DISTRIBUTIONS_JSON},"
#	fi
#	DISTRIBUTIONS_JSON="${DISTRIBUTIONS_JSON}$(curl -X GET --header "Content-Type:application/json" --header "Accept:application/json" --header "X-API-Token:${APPCENTER_TOKEN}" "https://api.appcenter.ms/v0.1/orgs/${OWNERNAME}/distribution_groups" | jq '.[] | if (.name == "$TEAM") then {name: "$TEAM", id:.id} else empty end')"
#	TEAM_NUMBER=$((TEAM_NUMBER+1))
#done
#DISTRIBUTIONS_JSON= "${DISTRIBUTIONS_JSON} ]"

#The line that will patch the previous release to add each authorized group to download it.
#curl -X PATCH "https://api.appcenter.ms/v0.1/apps/${OWNERNAME}/${APPNAME}/releases/${LASTEST_RELEASEID}" -H "accept: application/json" -H "X-API-Token:${APPCENTER_TOKEN}" -H "Content-Type: application/json" -d '{ "release_notes": "${RELEASENOTES}", "mandatory_update": false, $DISTRIBUTIONS_JSON, "notify_testers": false}'

#Now we can upload the symbols to unsymbolicate the code to retrieve the good lines in case of bugs / crashes / etc...
#UPLOAD_URL_DSYM=$(curl -X POST --header "Content-Type:application/json" --header "Accept:application/json" --header "X-API-Token:${APPCENTER_TOKEN}" "https://api.appcenter.ms/v0.1/apps/${OWNERNAME}/${APPNAME}/symbol_uploads" -d "{\"symbol_type\": \"${BUILD_TARGET}\"}")
#upload the dSym for the current platform / app, need to implement the blob page system if we wanna use this.
#curl -X PUT --header "x-ms-blob-type: BlockBlob" ${DSYMPATH} ${UPLOAD_URL_DSYM}

#**********************************************************************************************
#**********************************************************************************************
#**********************************************************************************************
