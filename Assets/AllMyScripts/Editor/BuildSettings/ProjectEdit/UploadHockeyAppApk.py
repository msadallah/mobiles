import os
import stat
import subprocess
from sys import argv


def set_user_exec_file(file_path):
    mode = os.stat(file_path).st_mode
    mode |= stat.S_IXUSR
    os.chmod(file_path, mode)


apkPath = argv[1]
ownername = argv[2]
appname = argv[3]
teamsName = argv[4]
releasenotes = "Build with gitlab-ci"

print('--------------------------- Upload HockeyApp ----------------------------------')
shell_script_associated = os.path.dirname(os.path.realpath(argv[0])) + "/UploadAppCenter.sh"
set_user_exec_file(shell_script_associated)

commandHockeyApp = [shell_script_associated,
                    ownername,
                    appname,
                    releasenotes,
                    teamsName,
                    apkPath,
                    "", #Symbols need to manage for Android with IL2CPP
                    "android"
                    ]

subprocess.call(commandHockeyApp)