#!/bin/bash
echo "$1"
filename=$(basename "$1")
echo "$2"
filename="$3"
echo $filename " !!!!"
DSYM_PATH="$1/dSYMs/$filename.app.dSYM"
DSYM_ZIP="$2/$filename.dSYM.zip"

echo "$DSYM_PATH"
echo "$DSYM_ZIP"

if [ ! -e "$DSYM_PATH" ] ; then
    echo "NO DSYM TO ZIP"
else
    echo "DSYM TO ZIP"
    zip -r -9 "$DSYM_ZIP" "$DSYM_PATH"
fi