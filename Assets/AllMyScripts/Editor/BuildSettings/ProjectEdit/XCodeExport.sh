#!/bin/sh

xcodebuild -project "$1" -scheme "$2" clean archive -archivePath "$3" -allowProvisioningUpdates
xcodebuild -exportArchive -archivePath "$3" -exportOptionsPlist "$4" -exportPath "$5" -allowProvisioningUpdates