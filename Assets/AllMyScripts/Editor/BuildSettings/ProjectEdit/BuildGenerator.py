import os
import shutil
import time 
import subprocess
from sys import argv

def runProcess(exe):
    p = subprocess.Popen(exe, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    while(True):
        retcode = p.poll() #returns None while subprocess is running
        line = p.stdout.readline()
        yield line
        if(retcode is not None):
            break
            
argv[1] = argv[1]
print( argv[1] )
choice = argv[1].split('%')
buildPath= argv[2]
sPathToXCodeProj= argv[3]
sOwnerName = argv[4]
sAppName = argv[5]
sTeamNames = argv[6]
sProductName = argv[7]
sPlistPath = argv[8]
sReleaseNotes = "Build with gitlab-ci"
sLogPath = argv[9]

#date = time.strftime("%d_%m_%Y") + "_" + time.strftime("%H.%M.%S")

sPathToVersion = buildPath+"/build/"#+date+"/"
sSchemeName = ""
sBuildName = ""

if not os.path.exists(sPathToVersion):
    os.makedirs(sPathToVersion)

commandListSchemes = ["xcodebuild","-list","-project",sPathToXCodeProj]

print('--------------------------- Build options : ' + argv[1] + ' ----------------------------------' )

#Build and archive
if "archive" in choice or "hockeyapp" in choice:
    
    commandePermissions = ["chmod", "+x", os.path.dirname(os.path.realpath(argv[0])) + "/XCodeExport.sh"]
    subprocess.call(commandePermissions)
    commandePermissions = ["chmod", "+x", os.path.dirname(os.path.realpath(argv[0])) + "/ExtractDSYM.sh"]
    subprocess.call(commandePermissions)
    
    if "appcenter" in choice:
        commandePermissions = ["chmod", "+x", os.path.dirname(os.path.realpath(argv[0])) + "/UploadAppCenter.sh"]
        subprocess.call(commandePermissions)
        
    file = open(sLogPath + '/XCodeLog.log', 'a+')
    print(sPathToVersion + "XCodeLog.log")
    print( '--------------------------- Permission ok ! ----------------------------------' )

    for line in runProcess(commandListSchemes):
        print( line )
        if "sch-" in line:
            sSchemeName = line.strip(' \n')
            sArchivePath = sPathToVersion + sSchemeName + ".xcarchive"
            print("\n#################### SCHEME : " + sSchemeName + " // " + sArchivePath + " ###################\n")

            if "archive" in choice:
                print( '--------------------------- IPA ----------------------------------' )
                if os.path.exists(str(sPathToVersion + sSchemeName+".ipa")):
                    os.remove(str(sPathToVersion + sSchemeName+".ipa"))

                commandeIPA = [os.path.dirname(os.path.realpath(argv[0])) + "/XCodeExport.sh",
                               str(sPathToXCodeProj),
                               str(sSchemeName),
                               str(sPathToVersion + sSchemeName + ".xcarchive"),
                               str(sPlistPath),
                               str(sPathToVersion)]
                out = subprocess.check_output(commandeIPA)
                file.write( out )
                file.write( "\n########### Archive done ###########\n" )
                
                print('sPathToVersion : ' + sPathToVersion)
                
                file.write( "\n########### Find dSYM file ###########\n" )
                commandeDSYM = [os.path.dirname(os.path.realpath(argv[0])) + "/ExtractDSYM.sh",
                                str( sPathToVersion + sSchemeName + ".xcarchive" ),
                                str( sPathToVersion ),
                                str( sProductName )]
                file.write( os.path.dirname(os.path.realpath(argv[0])) + "/ExtractDSYM.sh\n" )
                file.write( str( sPathToVersion + sSchemeName + ".xcarchive\n" ))
                file.write( sPathToVersion )
                file.write( sProductName )
                file.write( "########### Start DSYM script on ###########\n" )
                out = subprocess.check_output(commandeDSYM)
                file.write( out )
                file.write( "########### DSYM extracted and zipped ###########\n" )
            if "appcenter" in choice:
                print( '----------------------- Upload AppCenter ------------------------------' )
                file.write("\n##### APPCENTER UPLOAD STARTED #####\n")
                commandAppCenter = [os.path.dirname(os.path.realpath(argv[0])) + "/UploadAppCenter.sh",
                                      sOwnerName,
                                      sAppName,
                                      sReleaseNotes,
                                      sTeamNames,
                                      str(sPathToVersion + sSchemeName +".ipa"),
                                      str(sPathToVersion + sProductName +".dSYM.zip"),
                                      "iOS"]
                out = subprocess.check_output(commandAppCenter)
                file.write(out)
                file.write("\n##### APPCENTER UPLOAD FINISHED #####\n")
