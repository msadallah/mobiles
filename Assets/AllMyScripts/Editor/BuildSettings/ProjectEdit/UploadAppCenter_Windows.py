import os
import stat
import subprocess
from sys import argv

ownername = argv[1]
appname = argv[2]
teamsName = argv[3]
buildPath = argv[4]
buildVersion = argv[5]
releasenotes = "Build with gitlab-ci"

print('--------------------------- Upload AppCenter Windows ---------------------------')
print('OWNERNAME: ' + ownername)
print('APPNAME: ' + appname)
print('TEAMSNAME: ' + teamsName)
print('BUILDPATH: ' + buildPath)
print('BUILDVERSION: ' + buildVersion)

def set_user_exec_file(file_path):
    mode = os.stat(file_path).st_mode
    mode |= stat.S_IXUSR
    os.chmod(file_path, mode)

shell_script_associated = os.path.dirname(os.path.realpath(argv[0])) + "/UploadAppCenter.sh"
set_user_exec_file(shell_script_associated)

commandAppCenter = [shell_script_associated,
                    ownername,
                    appname,
                    releasenotes,
                    teamsName,
                    buildPath,
                    "", #Symbols need to manage for Android with IL2CPP
                    "windows",
					buildVersion
                    ]

subprocess.call(commandAppCenter)