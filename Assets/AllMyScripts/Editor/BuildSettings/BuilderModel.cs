﻿namespace AllMyScripts.Editor.Builder
{
	//C# pure libraries
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Reflection;
	using System.Threading;

	//Unity libraries
	using UnityEditor;
	using UnityEngine;
#if UNITY_IOS
	using UnityEditor.iOS.Xcode;
#endif

#if USE_ADDRESSABLES
	using UnityEditor.AddressableAssets;
	using UnityEditor.AddressableAssets.Build;
	using UnityEditor.AddressableAssets.Settings;
#endif

	//Our libraries
	using AllMyScripts.LangManager;
	using AllMyScripts.Editor.GlobalDefines;
	using AllMyScripts.Common.Tools;
	using AllMyScripts.Builder;
	using AllMyScripts.Editor.Builder.Android;
	
#if UNITY_IOS
	using AllMyScripts.Editor.Builder.IOS;
	using AllMyScripts.Editor.Builder.PList;
#endif

	[Serializable]
	public class BuilderModel
	{
		public delegate void OnUpdateLogs(string sInfo, MessageType type = MessageType.Info, bool bHelpBox = true, bool bIncrementErrors = false, bool bNewLine = true);
		public OnUpdateLogs m_logs;

		public delegate bool OnBuildingUpdate();
		public OnBuildingUpdate m_update;

		// Builder state
		public enum BuilderState
		{
			None,
			Building,
			BuildComplete,
			SigningAPK
		}

		public enum BuildControl
		{
			AbortOnCriticalError,
			PauseOnWarning,
			PhaseByPhase,
			StepByStep
		}

		private static bool m_bInitDone = default;
		public bool InitDone { get { return m_bInitDone; } }

		//Misc variables
		[SerializeField]
		private bool m_bVerifySavingAssets = default;

#if UNITY_IOS || UNITY_ANDROID || UNITY_STANDALONE_WIN
		public string _AppCenterTeamsName = "Unity-Dev";
#endif
		public static string m_sTempLocation = string.Empty;
		public static string m_sAndroidSdkLocation = string.Empty;
		public static string m_sAndroidNdkLocation = string.Empty;

		// Main Builder Config
		[SerializeField]
		private int m_nSelectedTarget = default;
		public int SelectedTarget { get { return m_nSelectedTarget; } }
		public BuildControl m_eBuildControl = BuildControl.AbortOnCriticalError;

		[SerializeField]
		private string[] m_sTargetsName = default;
		public string[] TargetsName { get { return m_sTargetsName; } }
		[SerializeField]
		private string[] m_sDestinationFolders = default;

		[SerializeField]
		private string m_sLastProfile = string.Empty;

		[SerializeField] private BuilderState m_BuilderState = BuilderState.None;
		public BuilderState BMBuilderState { get { return m_BuilderState; } }

		[SerializeField]
		private Target m_BuilderTarget = new Target(Constants.BUILDER_TARGETS_LIST[0]);
		public Target BuilderTarget { get { return m_BuilderTarget; } }

		// Profiles
		[SerializeField]
		private int m_nCurrentProfile = default;
		public int CurrentProfile { get { return m_nCurrentProfile; } }

		private Profile m_currentBuilderProfile = default;
		public Profile CurrentBuilderProfile { get { return m_currentBuilderProfile; } }
		[SerializeField]
		private List<string> m_builderProfilerNames = new List<string>();
		public List<string> BuilderProfilerNames { get { return m_builderProfilerNames; } }
		[SerializeField]
		private List<Profile> m_builderProfiles = new List<Profile>();
		[SerializeField]
		private List<string> m_buildSettingsNames = new List<string>();
		public List<string> BuildSettingsNames { get { return m_buildSettingsNames; } }

		//Building process variables
		public bool m_bBuildAbortByUser;
		public bool m_bBuildAbort;
		public bool m_bBuildPaused;

		public bool m_bBuildErrorPause;
		public string m_sSimpleBuildInfos = string.Empty;

		// Languages
		[SerializeField]
		private Country[] m_countries = default;
		public Country[] Countries { get { return m_countries; } }
		public int m_nSelectedIndexLanguage = 0;

		// Build methods
		private Func<bool>[] m_BuildMethods;
		public Func<bool>[] BuildEndPhaseMethods { get; private set; }

		[SerializeField]
		private int m_nCurrentBuildMethod = default;
		public int CurrentBuildMethod { get { return m_nCurrentBuildMethod; } }

		[SerializeField]
		private int m_nCurrentBuildMethodPhase = -1;
		public int nCurrentBuildMethodPhase { get { return m_nCurrentBuildMethodPhase; } }

		private int m_nAbortBuildMethod = -1;

		//Post parameters
		public bool m_bBuildXCode;
		public bool m_bArchiveXCode;
		public bool m_bUploadAppCenter = false;
		public string m_sAppleID = "";
		public bool m_bAutoRun = false;

		// Plugins settings
		[SerializeField] 
		private List<string> m_sPluginsPath = new List<string>();
		public List<string> PluginsPath { get { return m_sPluginsPath; } }

		// current global defines
		[SerializeField]
		private GlobalDefineManager m_globalDefineManager = default;
		public GlobalDefineManager GlobalDefineManager { get { return m_globalDefineManager; } }

		public bool m_bGoogleProject = false;

		[SerializeField]
		private bool m_bRecompileOnly = false;

		// Android profile settings
		[SerializeField]
		private ManifestOptions m_ManifestOptions = new ManifestOptions();

		// Android settings
		public bool m_bAndroidInstallAfterBuild = false;
		
		// Build control
		[SerializeField]
		private int m_nBuildErrors = default;
		public int BuildErrors { get { return m_nBuildErrors; } }

		[SerializeField]
		private int m_nBuildWarning = default;
		public int BuildWarning { get { return m_nBuildWarning; } }

		private List<string> _templates = new List<string>();
		public List<string> templates { get { return _templates; }}
		public int templateIdx = 0;

		public static BuilderModel s_model;

		public delegate void OnUpdateBuildInfos(string sInfo, MessageType type = MessageType.Info, bool bHelpBox = true, bool bIncrementErrors = false, bool bNewLine = true);
		public OnUpdateBuildInfos m_updateBuildInfos;


#region AssemblyReload
		/// <summary>
		/// Function called after recompilation, used to  differenciate first Unity compilation and others
		/// </summary>
		[InitializeOnLoadMethod]
		private static void OnInitialized()
		{
			if (EditorPrefs.GetBool("RecompileProcess", false))
			{
				m_bInitDone = EditorPrefs.GetBool("m_bInitDoneBeforeRecompile", false);
			}
			else
			{
				//Unity compiled for the first time, reset m_bInitDone value to false
				m_bInitDone = false;
			}
			AssemblyReloadEvents.beforeAssemblyReload += OnBeforeAssemblyReload;
			AssemblyReloadEvents.afterAssemblyReload += OnAfterAssemblyReload;
		}
		~BuilderModel()
		{
			AssemblyReloadEvents.beforeAssemblyReload -= OnBeforeAssemblyReload;
			AssemblyReloadEvents.afterAssemblyReload -= OnAfterAssemblyReload;
		}

		/// <summary>
		/// Called before a Unity compilation, not called on first compilation
		/// </summary>
		private static void OnBeforeAssemblyReload()
		{
			EditorPrefs.SetBool("RecompileProcess", true);
			EditorPrefs.SetBool("m_bInitDoneBeforeRecompile", m_bInitDone);
		}

		private static void OnAfterAssemblyReload()
		{
			EditorPrefs.SetBool("RecompileProcess", false);
		}
#endregion

		public void IncrementErrors()
		{
			m_nBuildErrors++;
		}

		public void IncrementWarnings()
		{
			m_nBuildWarning++;
		}

		public void Init()
		{
			InitBuildMethods();
			InitCountryLanguages();
			InitWebGLTemplates();

			if (m_bInitDone)
			{
				if (m_globalDefineManager != null)
					m_globalDefineManager.UpdateCurrentlyUsedGlobalDefines(EditorUserBuildSettings.selectedBuildTargetGroup);
				if (CurrentBuilderProfile != null)
				{	
					return;
				}
			}

			EditorPrefs.DeleteKey(ProjSettings.PLAYER_PREFS_PATH);

			InitTargets();

			if (m_globalDefineManager == null)
			{
				m_globalDefineManager = new GlobalDefineManager();
				m_globalDefineManager.Init();
			}

			LoadProfiles();
			LoadDestinationFolders();
			LoadPlugins();
		
			m_bInitDone = true;
}

		public void InitCountryLanguages()
		{
			m_countries = CountryCode.GetAllCountries();
		}

		public void InitWebGLTemplates()
		{
			_templates.Clear();
			_templates.Add("Default");
			_templates.Add("Minimal");

			string[] templateFolders = AssetDatabase.GetSubFolders("Assets/WebGLTemplates");
			for (int i = 0; i < templateFolders.Length; ++i)
				_templates.Add(templateFolders[i].Substring(templateFolders[i].LastIndexOf('/') + 1));
		}

#region BuildMethods
		public void InitBuildMethods()
		{
			if (m_bRecompileOnly)
			{
				m_BuildMethods = new Func<bool>[] {
					//Pre-Recompile
					SetEditorPrefs, //All target
					UpdateProjSettings, //All target
					SwitchPlatform, //All target
					SaveGlobalDefines, //All target
					SetPlugins, //Android & iOS

					//Post-recompile
					RestoreBuildSettings, //All target
					RestoreGlobalDefines, //All target
					RestoreEditorPrefs, //All target
					EndBuildGame //All target
				};
			}
			else
			{
				m_BuildMethods = new Func<bool>[] {
						// PreBuild
						SetEditorPrefs, //All target
						UpdateProjSettings, //All target
						SwitchPlatform, //All target
						SetAndroidInfo, //Android Only
						SetAndroidParameters, //Android Only
						SetWebGLParameters, //WebGL only
						CheckIOSFolders, //IOS Only
						SaveGlobalDefines, //All target
						SetPlugins, //Android & iOS
						PreBuildCustomMethod, //All target
						WriteDateVersion,  //All target
						BuildLanguages, //All target
						BuildAddressables, //All target
						SetGameName, //Standalone & Android & iOS
						SetIcons, //Standalone & Android & iOS
						SetSplashs, //All targets
						SetAppSettings, //Standalone & Android & iOS & WebGL
						ChangeEnabledScenes, //All target

						// Build
						BuildPlayer, //All targets

						//PostBuild
#if UNITY_IOS
						PatchXCodeProject, //iOS Only
						UpdateIOSPlistFiles, //IOS Only
						UpdateXCodeProj, //iOS Only
#endif
						BuildRunArchiveUpload, //Android & iOS
						
						RestorePlugins, //All target
						PostBuildCustomMethod, //All target
						RestoreBuildSettings, //All target
						RestoreGlobalDefines, //All target
						RestoreEditorPrefs, //All target
						AndroidInstall, //Android Only
						BuildLog, //All target

						// End Build
						FinalStepBuild, //All target
						EndBuildGame //All target
					};
			}

			BuildEndPhaseMethods = new Func<bool>[]
			{
					ChangeEnabledScenes,
					BuildPlayer
			};
		}
#endregion

#region GlobalDefinesMethods
		
		public void AddGlobalDefine(string sName, bool bEnabled, bool isCurrentlyUsed)
		{
			m_globalDefineManager.GlobalDefines.Add(new GlobalDefine(sName, bEnabled, isCurrentlyUsed));
		}

		private bool RemoveGlobalDefine(string sName)
		{
			for (int i = 0; i < m_globalDefineManager.GlobalDefines.Count; i++)
			{
				if (m_globalDefineManager.GlobalDefines[i].sName == sName)
				{
					m_globalDefineManager.GlobalDefines.RemoveAt(i);
					return true;
				}
			}
			return false;
		}

		public void SetSavedGlobalDefines()
		{
			foreach (GlobalDefine globalDefine in m_globalDefineManager.GlobalDefines)
				globalDefine.bSavedEnabled = globalDefine.bEnabled;
		}

		public void ResetGlobalDefines()
		{
			foreach (GlobalDefine globalDefine in m_globalDefineManager.GlobalDefines)
				globalDefine.Reset();
		}

		private void EnableGlobalDefine(string sName)
		{
			foreach (GlobalDefine globalDefine in m_globalDefineManager.GlobalDefines)
			{
				if (globalDefine.sName == sName)
				{
					globalDefine.bEnabled = true;
					return;
				}
			}

			//This avoid to build something with missing global define values
			GlobalDefine newMissingDefine = new GlobalDefine(sName, true, false);
			m_globalDefineManager.GlobalDefines.Add(newMissingDefine);
		}

#endregion

		private void LoadDestinationFolders()
		{
			string sBuildFolder = Utils.GetBuildFolder();
			int nProfileNumber = 0;
			if (m_builderProfiles != null && m_builderProfiles.Count > 0)
				nProfileNumber = m_builderProfiles.Count;

			m_sDestinationFolders = new string[nProfileNumber];
			for (int i = 0; i < m_sDestinationFolders.Length; i++)
				m_sDestinationFolders[i] = EditorPrefs.GetString("Builder.destinationFolders." + m_currentBuilderProfile.m_sBundleIdentifier + "." + i.ToString("00"), sBuildFolder);
		}

#region BuildConfigAndProfiles
		private void InitTargets()
		{
			m_sTargetsName = new string[Constants.BUILDER_TARGETS_LIST.Length];
			m_nSelectedTarget = 0;
			for (int i = 0; i < Constants.BUILDER_TARGETS_LIST.Length; i++)
			{
				m_sTargetsName[i] = Constants.BUILDER_TARGETS_LIST[i].m_eBuildTarget.ToString();
				if (Constants.BUILDER_TARGETS_LIST[i].m_eBuildTarget == EditorUserBuildSettings.activeBuildTarget)
					m_nSelectedTarget = i;
			}
			m_BuilderTarget = Constants.BUILDER_TARGETS_LIST[m_nSelectedTarget];
		}
		
		public void LoadProfiles(string sSelectedProfile = "")
		{
			m_sLastProfile = EditorPrefs.GetString("Builder_lastProfile", string.Empty);

			// Set default profile to last profile if not exist
			if (string.IsNullOrEmpty(sSelectedProfile))
				sSelectedProfile = m_sLastProfile;

			sSelectedProfile = Path.GetFileName(sSelectedProfile);
			m_builderProfilerNames = new List<string>();
			m_builderProfiles = new List<Profile>();

			m_builderProfilerNames.Add("None");
			m_builderProfiles.Add(Profile.GetTypedProfile(string.Empty));

			m_nCurrentProfile = -1;
			int nSelectedProfile = 0;
			if (Directory.Exists(Constants.GetProfilesPath()))
			{
				foreach (string sFilePath in Directory.GetFiles(Constants.GetProfilesPath(), "*.xml"))
				{
					string sFileName = Path.GetFileNameWithoutExtension(sFilePath);
					m_builderProfilerNames.Add(sFileName);
					m_builderProfiles.Add(Profile.GetTypedProfile(sFilePath));
					if (sFileName == sSelectedProfile)
						nSelectedProfile = m_builderProfilerNames.Count - 1;
				}
			}

			SetCurrentProfile(nSelectedProfile);
		}

		public virtual void SetCurrentProfile(int nNewProfile)
		{
			if (nNewProfile != m_nCurrentProfile)
			{
				// 0 --> if we select the none profile we copy the current profile in none
				if (nNewProfile == 0)
				{
					if (m_nCurrentProfile != -1)
						m_builderProfiles[nNewProfile] = Profile.GetTypedProfile(m_builderProfiles[m_nCurrentProfile]);
				}

				m_BuilderTarget = Constants.GetBuilderTarget(m_builderProfiles[nNewProfile].m_ePlatform);
				if (m_BuilderTarget == null)
				{
					Debug.LogError(m_builderProfiles[nNewProfile].m_ePlatform + " is not handled in the builder");
					if (m_nCurrentProfile != -1)
						m_currentBuilderProfile = m_builderProfiles[m_nCurrentProfile];
					if (m_currentBuilderProfile != null)
						m_BuilderTarget = new Target(Constants.GetBuilderTarget(m_currentBuilderProfile.m_ePlatform));
					return;
				}

				m_nCurrentProfile = nNewProfile;
				EditorPrefs.SetString("Builder_lastProfile", Path.GetFileNameWithoutExtension(m_builderProfilerNames[m_nCurrentProfile]));
				
				m_currentBuilderProfile = m_builderProfiles[m_nCurrentProfile];

				ResetGlobalDefines();

				m_nSelectedTarget = Array.IndexOf(Constants.BUILDER_TARGETS_LIST, m_BuilderTarget);

				//we reload the build settings because we just changed from profile
				UpdateProjSettings();

				if (string.IsNullOrEmpty(m_currentBuilderProfile.m_sBundleIdentifier))
					m_currentBuilderProfile.m_sBundleIdentifier = "com.tog." + Application.productName;

				Helpers.SetApplicationIdentifier(m_currentBuilderProfile.m_sBundleIdentifier);

				BuildTargetGroup currentTargetGroup = BuildPipeline.GetBuildTargetGroup(m_BuilderTarget.m_eBuildTarget);

				PlayerSettings.SetScriptingBackend(currentTargetGroup, m_currentBuilderProfile.m_scriptingBackend);
#if !UNITY_2019_3_OR_NEWER
				PlayerSettings.scriptingRuntimeVersion = m_currentBuilderProfile.m_scriptingRuntimeVersion;
#endif
				PlayerSettings.SetApiCompatibilityLevel(currentTargetGroup, m_currentBuilderProfile.m_apiCompatibilityLevel);
#if !UNITY_IOS //Make no sense in iOS cause it's always set to Release. Moreover it will trigger a warning with Unity 2020.1 and above.
				PlayerSettings.SetIl2CppCompilerConfiguration(currentTargetGroup, m_currentBuilderProfile.m_compilerConfiguration);
#endif
				PlayerSettings.stripEngineCode = m_currentBuilderProfile.stripEngineCode;
				PlayerSettings.SetManagedStrippingLevel(currentTargetGroup, m_currentBuilderProfile.managedStrippingLevel);

				if (currentTargetGroup == BuildTargetGroup.Standalone)
				{
					PlayerSettings.fullScreenMode = ((StandaloneProfile)m_currentBuilderProfile)._fullscreenMode;
					PlayerSettings.defaultScreenWidth = ((StandaloneProfile)m_currentBuilderProfile)._defaultScreenWidth;
					PlayerSettings.defaultScreenHeight = ((StandaloneProfile)m_currentBuilderProfile)._defaultScreenHeight;

					string[] sdkArray = null;
					switch (m_currentBuilderProfile.vrSdk)
					{
						default:
						case Profile.VR_SDK.None:
							sdkArray = new string[] { Profile.VR_SDK.None.ToString(), Profile.VR_SDK.OpenVR.ToString() };
							break;
						case Profile.VR_SDK.OpenVR:
							sdkArray = new string[] { Profile.VR_SDK.OpenVR.ToString(), Profile.VR_SDK.None.ToString() };
							break;
					}
					UnityEditorInternal.VR.VREditor.SetVREnabledDevicesOnTargetGroup(BuilderTarget.m_eBuildTargetGroup, sdkArray);
				}
				else if (currentTargetGroup == BuildTargetGroup.Android)
				{
					if (m_currentBuilderProfile.typedBuildingTargetGroup == BuildTargetGroup.Android)
					{
						AndroidProfile currentAndroiduilderProfile = (AndroidProfile) m_currentBuilderProfile;
						PlayerSettings.Android.targetArchitectures = currentAndroiduilderProfile.m_architecture;
						PlayerSettings.Android.buildApkPerCpuArchitecture = currentAndroiduilderProfile.m_bSplitArchitectureApk;
						EditorUserBuildSettings.buildAppBundle = currentAndroiduilderProfile.m_bUseAABSystem;
						if (EditorUserBuildSettings.buildAppBundle)
							EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
					}
					else
					{
						Debug.LogWarning("currentTargetGroup inconsistent with currentBuilderProfile : " + currentTargetGroup + " vs " + m_currentBuilderProfile.typedBuildingTargetGroup);
					}
				}
				else if (currentTargetGroup == BuildTargetGroup.iOS)
				{
					IOSProfile iOSProfile = (IOSProfile)m_currentBuilderProfile;
					if (string.IsNullOrEmpty(PlayerSettings.iOS.appleDeveloperTeamID))
					{
						PlayerSettings.iOS.appleEnableAutomaticSigning = true;
						PlayerSettings.iOS.appleDeveloperTeamID = iOSProfile.m_sTeamIds;
					}
					PlayerSettings.iOS.targetDevice = iOSTargetDevice.iPhoneAndiPad;
					PlayerSettings.iOS.targetOSVersionString = iOSProfile.m_targetOS;
				}
				else if (currentTargetGroup == BuildTargetGroup.WebGL)
				{
					RecomputeWebGLTemplate();
					PlayerSettings.WebGL.exceptionSupport = ((WebGLprofile)m_currentBuilderProfile).webGLExceptionSupport;
				}

				SetBundleVersion();

				EnableGlobalDefinesFromCurrentProfile();

				LoadPlugins();

				SetAndroidInfo(true);
				SetSavedGlobalDefines();
			}
		}

		public void DeleteProfile()
		{
			if (m_nCurrentProfile < 1 || m_builderProfilerNames == null)
				return;

			if (EditorUtility.DisplayDialog("Remove profile", "You will delete the current profile, are you sure?", "Yes", "No"))
			{
				if (File.Exists(Constants.GetProfilesPath() + m_builderProfilerNames[m_nCurrentProfile]))
					File.Delete(Constants.GetProfilesPath() + m_builderProfilerNames[m_nCurrentProfile]);

				m_builderProfilerNames.RemoveAt(m_nCurrentProfile);
				m_builderProfiles.RemoveAt(m_nCurrentProfile);

				SetCurrentProfile(Mathf.Max(m_nCurrentProfile - 1, 0));
			}
		}

		public void SaveProfile()
		{
			string profilePath = string.Empty;
			string sNewProfileName = string.Empty;
			if (m_nCurrentProfile < 1)
			{
				if (!Directory.Exists(Constants.GetProfilesPath()))
					Directory.CreateDirectory(Constants.GetProfilesPath());

				profilePath = EditorUtility.SaveFilePanel("New profile", Constants.GetProfilesPath(), "ProfileName", "xml");
				sNewProfileName = Path.GetFileNameWithoutExtension(profilePath);
			}


			if (!string.IsNullOrEmpty(sNewProfileName))
			{
				int nIdx = m_builderProfilerNames.IndexOf(sNewProfileName);
				if (nIdx == -1)
				{
					m_builderProfilerNames.Add(sNewProfileName);
					m_builderProfiles.Add(Profile.GetTypedProfile(CurrentBuilderProfile));
					SetCurrentProfile(m_builderProfiles.Count - 1);
				}
				else
				{
					SetCurrentProfile(nIdx);
				}
			}
			else
			{
				CurrentBuilderProfile.m_globalDefines.Clear();
				foreach (GlobalDefine define in m_globalDefineManager.GlobalDefines)
				{
					if (define.bEnabled)
					{
						define.bSavedEnabled = true;
						CurrentBuilderProfile.m_globalDefines.Add(define.sName);
					}
				}

				ResetGlobalDefines();
				EnableGlobalDefinesFromCurrentProfile();
				SetSavedGlobalDefines();
			}

			profilePath = Constants.GetProfilesPath() + m_builderProfilerNames[m_nCurrentProfile];
			CurrentBuilderProfile.WriteXml(profilePath + ".xml", m_BuilderTarget.m_eBuildTarget, m_sPluginsPath);
			UpdateProjSettings();
		}

		public void ReloadGlobalDefinesFromProfiles()
		{
			m_globalDefineManager.ReloadGlobalDefines();
			m_currentBuilderProfile.ReloadGlobalDefines();
			EnableGlobalDefinesFromCurrentProfile();
			SetSavedGlobalDefines();
		}

		public void EnableGlobalDefinesFromCurrentProfile()
		{
			for (int i = 0; i < m_currentBuilderProfile.m_globalDefines.Count; i++)
				EnableGlobalDefine(m_currentBuilderProfile.m_globalDefines[i]);
		}

		public void OpenProfile()
		{
			string fileToOpen = m_builderProfilerNames[m_nCurrentProfile] + ".xml";
			if (m_nCurrentProfile < 1 || 
				m_builderProfilerNames == null || 
				string.IsNullOrEmpty(m_builderProfilerNames[m_nCurrentProfile]) || 
				!File.Exists(Constants.GetProfilesPath() + fileToOpen))
				return;

			string sPath = Constants.GetProfilesPath() + fileToOpen;
			if (Application.platform == RuntimePlatform.WindowsEditor)
				Utils.ThreadProcess(this, sPath, "", "", true, false, true);
			else if (Application.platform == RuntimePlatform.OSXEditor)
				Utils.ThreadProcess(this, "open", '"' + sPath + '"', "", true, false, true);
		}
#endregion
		
		public bool RetryBuildMethod()
		{
			if (!IsBuilding() || m_bBuildAbort)
				return false;

			m_nCurrentBuildMethodPhase = 0;
			m_nCurrentBuildMethod--;

			m_bBuildPaused = m_bBuildErrorPause = false;
			m_nBuildErrors--;

			return true;
		}

		private bool NextBuildMethodPhase(string sBuildInfo = null, int nPhase = -1)
		{
			if (!string.IsNullOrEmpty(sBuildInfo))
				UpdateLogs(sBuildInfo, MessageType.Info, false);

			if (nPhase < 0)
				m_nCurrentBuildMethodPhase++;
			else
				m_nCurrentBuildMethodPhase = nPhase;

			return true;
		}

		public bool AbortBuild(string sBuildInfo = null, bool bByUser = true)
		{
			if (!string.IsNullOrEmpty(sBuildInfo))
				UpdateLogs(sBuildInfo, MessageType.Error, false);

			if (!IsBuilding())
				return false;

			m_nAbortBuildMethod = m_nCurrentBuildMethod;

			if (bByUser)
				m_nAbortBuildMethod--;
			else
				m_nBuildErrors++;

			m_bBuildAbortByUser = bByUser;
			m_bBuildAbort = true;
			m_bBuildPaused = m_bBuildErrorPause = false;

			return false;
		}

		private bool NextBuildMethod(Func<bool> caller, string sBuildInfo = null, MessageType messageType = MessageType.Info)
		{
			UpdateLogs(sBuildInfo, messageType, false);

			if (!IsBuilding() || GetCurrentBuildMethods() == null || caller == null || m_nCurrentBuildMethod != Array.IndexOf(GetCurrentBuildMethods(), caller))
				return true;

			m_nCurrentBuildMethodPhase = 0;
			m_nCurrentBuildMethod++;

			if (m_bBuildAbort)
				return true;

			if (m_eBuildControl == BuildControl.StepByStep)
				m_bBuildPaused = true;

			if (sBuildInfo == null)
				return true;

			if (sBuildInfo.Length < 6 || !sBuildInfo.ToLower().StartsWith("build ", StringComparison.Ordinal))
			{
				if (messageType == MessageType.Error)
				{
					if (m_eBuildControl != BuildControl.AbortOnCriticalError)
						m_bBuildPaused = m_bBuildErrorPause = true;

					m_nBuildErrors++;
				}
				else if (messageType == MessageType.Warning)
				{
					if (m_eBuildControl == BuildControl.PauseOnWarning)
						m_bBuildPaused = true;

					m_nBuildWarning++;
				}
			}
			return true;
		}

		protected virtual bool SetEditorPrefs()
		{
			if (IsBuilding() && m_bBuildAbort)
				return NextBuildMethod(SetEditorPrefs);

			if (Application.isBatchMode)
			{
				EditorPrefs.SetString("AndroidSdkRoot", m_sAndroidSdkLocation);
				EditorPrefs.SetString("AndroidNdkRootR16b", m_sAndroidNdkLocation);
			}

			if (EditorPrefs.GetBool("VerifySavingAssets"))
			{
				EditorPrefs.SetBool("VerifySavingAssets", false);
				m_bVerifySavingAssets = true;

				return NextBuildMethod(SetEditorPrefs, "Disabled 'Verify Saving Assets' setting.");
			}

			return NextBuildMethod(SetEditorPrefs);
		}

		public bool UpdateProjSettings()
		{
			if (IsBuilding() && m_bBuildAbort)
				return NextBuildMethod(UpdateProjSettings);

			string sPathBuildSetting = Constants.GetAssetsPath() + "/Resources/BuildSettings.txt";
			if (!File.Exists(sPathBuildSetting))
			{
				string directoryToFilePath = Path.GetDirectoryName(sPathBuildSetting);
				if(!Directory.Exists(directoryToFilePath))
					Directory.CreateDirectory(directoryToFilePath);
				File.Create(sPathBuildSetting).Dispose();
				AssetDatabase.Refresh();
			}

			ProjSettings.Save( CurrentBuilderProfile.buildVariables );
			string roomId = ProjSettings.GetRoomId();
			if (!string.IsNullOrEmpty(roomId))
			{
				string path = Application.streamingAssetsPath + "/mp_roomId.txt";
				File.WriteAllText(path, roomId);
			}

			UpdateLogs("Updated projSettings file with informations from profile");

			return NextBuildMethod(UpdateProjSettings, "Proj Settings updated.");
		}

		public virtual void LoadPlugins()
		{
			if (m_currentBuilderProfile != null && m_currentBuilderProfile.m_ePlatform == BuildTarget.Android || m_currentBuilderProfile.m_ePlatform == BuildTarget.iOS)
				PopulatePlugins(m_currentBuilderProfile.m_ePlatform.ToString(), ref m_sPluginsPath);
		}

		private void PopulatePlugins(string sPluginPlatform, ref List<string> pluginPathList)
		{
			string[] sPluginList = Directory.GetDirectories(IOUtility.CombinePaths(Application.dataPath, "Plugins"));

			pluginPathList.Clear();

			for (int i = 0; i < sPluginList.Length; ++i)
			{
				if (Directory.GetDirectories(sPluginList[i], m_currentBuilderProfile.m_ePlatform.ToString()).Length > 0)
				{
					pluginPathList.Add(Path.GetFileName(sPluginList[i]));
				}
			}

			UpdateLogs(sPluginPlatform + " plugins loaded.");
		}

		public bool SwitchPlatform()
		{
			return SwitchPlatform(false);
		}

		public virtual bool SwitchPlatform(bool bChangeEnabledScenes)
		{
			if ((IsBuilding() && m_bBuildAbort) || m_BuilderTarget.m_eBuildTarget == EditorUserBuildSettings.activeBuildTarget)
				return NextBuildMethod(SwitchPlatform);

			switch (m_nCurrentBuildMethodPhase)
			{
				case 0:
					return NextBuildMethodPhase("Switching platform to " + m_BuilderTarget.m_eBuildTarget + "...");
				default:
					bool bCanSwitch = EditorUserBuildSettings.SwitchActiveBuildTarget(BuildPipeline.GetBuildTargetGroup(m_BuilderTarget.m_eBuildTarget), m_BuilderTarget.m_eBuildTarget);
					if (!bCanSwitch)
						AbortBuild("Can't switch platform to " + m_BuilderTarget.m_eBuildTarget + ".", false);

					EditorUserBuildSettings.selectedBuildTargetGroup = m_BuilderTarget.m_eBuildTargetGroup;
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();

					if (bChangeEnabledScenes)
						ChangeEnabledScenes();

					return NextBuildMethod(SwitchPlatform, "Platform switched to " + m_BuilderTarget.m_eBuildTarget + ".");
			}
		}

		private bool SetAndroidInfo()
		{
			return SetAndroidInfo(true);
		}

		public bool SetAndroidInfo(bool bResetAndroidKey)
		{
			if ((IsBuilding() && m_bBuildAbort) || m_BuilderTarget.m_eBuildTarget != BuildTarget.Android)
				return NextBuildMethod(SetAndroidInfo);

			if (bResetAndroidKey)
				SetAndroidKey(false);

			return NextBuildMethod(SetAndroidInfo, "Android Bundle Identifier set to '" + Helpers.GetApplicationIdentifier() + "'.");
		}

		public void SetAndroidKey(bool bResetInfo)
		{
			if ((IsBuilding() && m_bBuildAbort) || m_BuilderTarget.m_eBuildTarget != BuildTarget.Android)
				return;

			AndroidProfile androidProfile = (AndroidProfile) m_currentBuilderProfile;
			Keys.Key key = Keys.GetKey(androidProfile.m_nAndroidKeyIndex);

			PlayerSettings.Android.keystoreName = GetBuilderScriptFolder() + "/SignTools/" + key.sName + ".keystore";
			PlayerSettings.Android.keystorePass = key.sStorePass;
			PlayerSettings.Android.keyaliasName = key.sAliasName;
			PlayerSettings.Android.keyaliasPass = key.sAliasPass;

			UpdateLogs("Android key set to '" + key.sName + "'.", MessageType.Info, false);

			if (bResetInfo)
				SetAndroidInfo(false);
		}

		protected virtual bool SetAndroidParameters()
		{
			if ((IsBuilding() && m_bBuildAbort) || m_BuilderTarget.m_eBuildTarget != BuildTarget.Android)
				return NextBuildMethod(SetAndroidParameters);

			bool bPlugins = m_currentBuilderProfile.m_selectedPlugins.Count > 0;

			if (!bPlugins)
			{
				SetAndroidManifest();
				return NextBuildMethod(SetAndroidParameters);
			}

			switch (m_nCurrentBuildMethodPhase)
			{
				case 0:
					SetAndroidManifest();
					return NextBuildMethodPhase("Setting Android (manifest) plugins...");
				case 1:
					bool bContainFirebase = false;
					for (int i = 0; i < m_currentBuilderProfile.m_selectedPlugins.Count; i++)
					{
						ManifestOptions.AndroidPlugin androidPlugin = ManifestOptions.GetAndroidPlugin(IOUtility.GetFileName(m_currentBuilderProfile.m_selectedPlugins[i]));
						if (androidPlugin == ManifestOptions.AndroidPlugin.Firebase)
						{
							bContainFirebase = true;
							break;
						}
					}
					if (bContainFirebase)
					{
						/*
						string sPluginFileName = string.Empty;
						if (!m_currentBuilderProfile.TryGetPluginConfig(ManifestOptions.AndroidPlugin.Firebase.ToString(), out sPluginFileName))
							AbortBuild("Missing PLUGIN_CONFIG_FILE in the profile for " + ManifestOptions.AndroidPlugin.Firebase);
						string sPluginPath = IOUtility.CombinePaths(Constants.GetPluginConfigFilesPath(), sPluginFileName);
						if (!File.Exists(sPluginPath))
							AbortBuild(sPluginPath + " does not exist : can't parse it for Firebase");

						string sFirebaseGenerator = IOUtility.CombinePaths(Application.dataPath, "Plugins/Firebase/Scripts/Editor/generate_xml_from_google_services_json");
						string sFinalPath = IOUtility.CombinePaths(Application.dataPath, "Plugins/Android/res/values/");
						if (!Directory.Exists(sFinalPath))
							Directory.CreateDirectory(sFinalPath);

						string sBundleId = Helpers.GetApplicationIdentifier();
						if (Application.platform == RuntimePlatform.OSXEditor)
						{
							string sScriptPath = "python";
							string sArguments = '"' + sFirebaseGenerator + ".py\"" +
								" -i \"" + sPluginPath + "\"" +
								" -o \"" + IOUtility.CombinePaths(sFinalPath, "google-services.xml") + "\"" +
								" -p " + sBundleId;

							Utils.RunProcess(this, sScriptPath, sArguments, "", true, false, Constants.GetAssetsPath());
						}
						else
						{
							string sFirebaseGenWinPath = '"' + sFirebaseGenerator + ".exe\"";
							Utils.RunProcess(this, sFirebaseGenWinPath,
								" -i \"" + sPluginPath + "\"" +
								" -o \"" + IOUtility.CombinePaths(sFinalPath, "google-services.xml") + "\"" +
								" -p " + sBundleId, "", true, false, Constants.GetAssetsPath());
						}
						*/
						return NextBuildMethodPhase("Firebase Plugin : google_services.json parsed");
					}
					return NextBuildMethodPhase("No Firebase Plugin : No need to parse google_services.json");
				default:
					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();
					return NextBuildMethod(SetAndroidParameters, "Android parameters set.");
			}
		}

		public virtual bool SetWebGLParameters()
		{
			if ((IsBuilding() && m_bBuildAbort) || m_BuilderTarget.m_eBuildTarget != BuildTarget.WebGL)
				return NextBuildMethod(SetWebGLParameters);

			RecomputeWebGLTemplate();

			return NextBuildMethod(SetWebGLParameters, "WebGL parameters set.");
		}

		public void RecomputeWebGLTemplate()
		{
			WebGLprofile webGLProfile = (WebGLprofile)m_currentBuilderProfile;
			templateIdx = Mathf.Max(0, _templates.IndexOf(webGLProfile.webGLTemplate));
			string prefixe = templateIdx < 2 ? "APPLICATION:" : "PROJECT:";
			PlayerSettings.WebGL.template = prefixe + webGLProfile.webGLTemplate;
		}

		private bool SetPlugins()
		{
#if UNITY_ANDROID || UNITY_IOS
			List<string> sPluginsPathList = m_sPluginsPath;
			List<string> sPluginsSelectedList = m_currentBuilderProfile.m_selectedPlugins;

			// first we check if we set some plugins in our profile that are not in the project
			// if there's at least one plugin missing we abort
			for (int i = 0; i < sPluginsSelectedList.Count; i++)
			{
				if (!sPluginsPathList.Contains(sPluginsSelectedList[i]))
					AbortBuild("Plugin not found : " + sPluginsSelectedList[i] + "'.", false);
			}

			// special case : link.xml -> we need to disable them if the plugin is not selected
			for (int i = 0; i < sPluginsPathList.Count; i++)
			{
				if (!sPluginsSelectedList.Contains(sPluginsPathList[i]))
				{
					string sDirectory = IOUtility.CombinePaths(Application.dataPath, "Plugins/" + sPluginsPathList[i]);
					string[] linkFiles = Directory.GetFiles(sDirectory, "link.xml", SearchOption.AllDirectories);
					for (int j = 0; j < linkFiles.Length; j++)
					{
						linkFiles[j] = linkFiles[j].Replace("\\", "/");
						int nIdxAssets = linkFiles[j].IndexOf("Assets/", StringComparison.Ordinal);
						string sAssetPath = linkFiles[j].Substring(nIdxAssets);
						AssetDatabase.RenameAsset(sAssetPath, "link_backup");
					}
				}
			}

			PluginImporter[] importerAllPlugins = PluginImporter.GetAllImporters();

			foreach (PluginImporter plugin in importerAllPlugins)
			{
				for (int i = 0; i < sPluginsSelectedList.Count; i++)
				{
					// Special case : some dll are for all platforms so we don't need to patch them
					if (plugin.assetPath.Contains("Script"))
						continue;
					plugin.SetCompatibleWithPlatform(m_currentBuilderProfile.m_ePlatform, false);
					if (plugin.assetPath.Contains(sPluginsSelectedList[i]) && plugin.assetPath.Contains(m_currentBuilderProfile.m_ePlatform.ToString()))
					{
						plugin.SetCompatibleWithAnyPlatform(false);
						if (sPluginsPathList.Contains(sPluginsSelectedList[i]))
						{
							plugin.SetCompatibleWithPlatform(m_currentBuilderProfile.m_ePlatform, true);
							break;
						}
					}
				}
			}
#endif
			return NextBuildMethod(SetPlugins, "Moved unused plugins outside");
		}

		private bool CheckIOSFolders()
		{
			if ((IsBuilding() && m_bBuildAbort) || m_BuilderTarget.m_eBuildTarget != BuildTarget.iOS)
				return NextBuildMethod(CheckIOSFolders);

			string sBuildPath = GetSelectedTargetDestinationFolder();
			if (Directory.Exists(sBuildPath))
				Utils.ThreadDelete(sBuildPath);

			return NextBuildMethod(CheckIOSFolders, "iOS build folders checked.");
		}

		public void SetAndroidManifest()
		{
			m_ManifestOptions.ResetPlugins();

			for (int i = 0; i < m_currentBuilderProfile.m_selectedPlugins.Count; i++)
			{
				string sPluginPath = m_currentBuilderProfile.m_selectedPlugins[i];
				if (m_sPluginsPath.Contains(sPluginPath))
				{
					ManifestOptions.AndroidPlugin androidPlugin = ManifestOptions.GetAndroidPlugin(IOUtility.GetFileName(sPluginPath));
					if (androidPlugin != ManifestOptions.AndroidPlugin.Count)
					{
						m_ManifestOptions.SetPlugin(androidPlugin);
					}
				}
			}

			m_ManifestOptions.Save();

			UpdateLogs("Saved 'AndroidManifest.xml'.");
		}

		protected virtual bool SaveGlobalDefines()
		{
			if (!IsBuilding() || m_bBuildAbort)
				return NextBuildMethod(SaveGlobalDefines);

			if (m_BuilderTarget.m_eBuildTarget == BuildTarget.iOS || m_BuilderTarget.m_eBuildTarget == BuildTarget.Android)
			{
				for (int i = 0; i < m_sPluginsPath.Count; ++i)
				{
					if (!m_currentBuilderProfile.m_selectedPlugins.Contains(m_sPluginsPath[i]))
					{
						m_sPluginsPath[i] = IOUtility.ReplacePathSeparator(m_sPluginsPath[i]);
						string sFolderName = IOUtility.GetFolderName(m_sPluginsPath[i]);

						AddGlobalDefine("DONT_USE_" + sFolderName.ToUpper(), true, true);
					}
				}
			}

			// then we saved the global defines needed
			m_globalDefineManager.Save(EditorUserBuildSettings.selectedBuildTargetGroup);
			SetSavedGlobalDefines();
			UpdateLogs("Saved global defines for platform : " + EditorUserBuildSettings.selectedBuildTargetGroup);

			return NextBuildMethod(SaveGlobalDefines, "Recompiling. Please wait....");
		}
        
		private bool PreBuildCustomMethod()
		{
			if (IsBuilding() && m_bBuildAbort)
				return NextBuildMethod(PreBuildCustomMethod);

			Type preBuildCustomType = null;
			string[] allAsmDef = Directory.GetFiles(Application.dataPath + "/", "*.asmdef", SearchOption.AllDirectories);
			int nBuildMethodsFound = 0;
			int nAssemblyCount = allAsmDef.Length;
			if (allAsmDef.Length == 0)
			{
				preBuildCustomType = Type.GetType("BuildMethod,Assembly-CSharp-Editor");
				if (preBuildCustomType != null)
				{
					MethodInfo methodInfo = preBuildCustomType.GetMethod("PreBuild");
					if (methodInfo != null)
						methodInfo.Invoke(null, null);
					nBuildMethodsFound++;
					nAssemblyCount++;
				}
			}
			else
			{
				foreach (string singleAsmDef in allAsmDef)
				{
					string sAssemblyName = Path.GetFileNameWithoutExtension(singleAsmDef);
					preBuildCustomType = GetTypeFromAssemblyName(sAssemblyName, "BuildMethod");
					if (preBuildCustomType != null)
					{
						nBuildMethodsFound++;
						MethodInfo methodInfo = preBuildCustomType.GetMethod("PreBuild");
						if (methodInfo != null)
							methodInfo.Invoke(null, null);
					}
				}
			}

			return NextBuildMethod(PreBuildCustomMethod, nBuildMethodsFound.ToString() + " build methods found in the " + nAssemblyCount.ToString() + " assemblies");
		}

		private Type GetTypeFromAssemblyName(string assemblyName, string className)
		{
			Assembly[] assemblyList = AppDomain.CurrentDomain.GetAssemblies();
			foreach (Assembly assembly in assemblyList)
			{
				if (assembly.FullName.Contains(assemblyName))
				{
					foreach (Type type in assembly.GetTypes())
					{
						if (type.Name.Contains(className))
							return type;
					}
				}
			}
			return null;
		}

		public bool BuildLanguages()
		{
			if ((IsBuilding() && m_bBuildAbort))
				return NextBuildMethod(BuildLanguages);

			string sError = string.Empty;
			if (CountryCode.WriteLangs(m_currentBuilderProfile.m_sLanguageCultures, out sError))
				return NextBuildMethod(BuildLanguages, "Language files wrote.");
			else
				AbortBuild(sError, false);

			return NextBuildMethod(BuildLanguages, "", MessageType.Warning);
		}

		public bool BuildAddressables()
		{
			if ((IsBuilding() && m_bBuildAbort))
				return NextBuildMethod(BuildAddressables);
#if USE_ADDRESSABLES
			AddressableAssetSettings.BuildPlayerContent();

			return NextBuildMethod(BuildAddressables, "Builded local addressables!");
#else
			// Skip
			return NextBuildMethod(BuildAddressables);
#endif
		}

		public void SetEditorBuildSettingsScenes()
		{
			// Find valid Scene paths and make a list of EditorBuildSettingsScene
			List<EditorBuildSettingsScene> newScenes = new List<EditorBuildSettingsScene>();
			
			for (int i = 0; i < CurrentBuilderProfile.scenes.Count; i++)
			{
				if (!(CurrentBuilderProfile.scenes[i] is null))
				{
					string scenePath = AssetDatabase.GetAssetPath(CurrentBuilderProfile.scenes[i]);
					if (!string.IsNullOrEmpty(scenePath))
						newScenes.Add(new EditorBuildSettingsScene(scenePath, true));
				}
			}

			// Set the Build Settings window Scene list
			EditorBuildSettings.scenes = newScenes.ToArray();
		}

#region PreBuild
		private bool SetGameName()
		{
			if (!IsBuilding() || m_bBuildAbort)
				return NextBuildMethod(SetGameName);

			if (string.IsNullOrEmpty(GetProductName()))
				AbortBuild("Product name is empty");

			//Define the company name
			PlayerSettings.companyName = Constants.COMPANY_NAME;
			PlayerSettings.productName = GetProductName();
			
			return NextBuildMethod(SetGameName, "Game name set.");
		}

		private bool SetAppSettings()
		{
			if (!IsBuilding() || m_bBuildAbort)
				return NextBuildMethod(SetAppSettings);

			if (m_currentBuilderProfile.m_ePlatform == BuildTarget.iOS)
			{
				PlayerSettings.statusBarHidden = !ParseTools.ParseBoolSafe(((IOSProfile)m_currentBuilderProfile).m_sUseStatusBar);
			}

			return NextBuildMethod(SetAppSettings, "Application settings set");
		}

		private bool SetIcons()
		{
			if (!IsBuilding() 
				|| m_bBuildAbort 
				|| (m_currentBuilderProfile.iconTexture is null && m_BuilderTarget.m_eBuildTarget != BuildTarget.Android) 
				|| (m_BuilderTarget.m_eBuildTargetGroup != BuildTargetGroup.Standalone 
				&& m_BuilderTarget.m_eBuildTarget != BuildTarget.Android 
				&& m_BuilderTarget.m_eBuildTarget != BuildTarget.iOS))
				return NextBuildMethod(SetIcons, "No icon to patch cause not on a viable platform or no icon defined");

			if (!ValidIcons())
				return NextBuildMethod(SetIcons, "An icon is missing. Icons patching process aborted.", MessageType.Warning);

			// Special case : we can override icons for Android ( in order to handle the new format for Android O )
			if (m_BuilderTarget.m_eBuildTarget == BuildTarget.Android)
			{
				if (m_ManifestOptions.GetPlugin(ManifestOptions.AndroidPlugin.Icons))
				{
					string sPluginFileName = string.Empty;
					if (!m_currentBuilderProfile.TryGetPluginConfig(ManifestOptions.AndroidPlugin.Icons.ToString(), out sPluginFileName))
						AbortBuild("Missing PLUGIN_CONFIG_FILE in the profile for " + ManifestOptions.AndroidPlugin.Icons);

					string sPluginPath = IOUtility.CombinePaths(Constants.GetPluginConfigFilesPath(), sPluginFileName);
					if (!File.Exists(sPluginPath))
						AbortBuild(sPluginPath + " does not exist : can't copy icons .aar");

					string sProjectPath = "Plugins/Icons/Android/IconRes.aar";
					string sDestFile = IOUtility.CombinePaths(Application.dataPath, sProjectPath);

					if (File.Exists(sDestFile))
						File.Delete(sDestFile);
					File.Copy(sPluginPath, sDestFile);
					
					AssetDatabase.ImportAsset("Assets/" + sProjectPath);
					PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, new Texture2D[0]);
					return NextBuildMethod(SetIcons, "The icon will be set through the icons .aar");
				}
			}

			Texture2D[] defaultIcon = new Texture2D[1];
			defaultIcon[0] = m_currentBuilderProfile.iconTexture;
			PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, defaultIcon);
			return NextBuildMethod(SetIcons, "Icon(s) correctly set.");
		}

		private bool SetSplashs()
		{
			bool bUseSplashSystem = m_currentBuilderProfile.m_bUseSplashSystem;

			if (m_currentBuilderProfile.m_SplashDefinesInProfile)
			{
				bUseSplashSystem = m_currentBuilderProfile.m_bUseSplashSystem;
			}

			if (!bUseSplashSystem)
			{
				PlayerSettings.SplashScreen.show = false;
				return NextBuildMethod(SetSplashs, "Use Unity Splash Screen system : " + PlayerSettings.SplashScreen.showUnityLogo);
			}
			else
			{
				bool bShowUnityLogo = m_currentBuilderProfile.m_bUseUnitySplash;
				Color cBackgroundColor = new Color(.1333f, .1725f, .2117f);
				ColorUtility.TryParseHtmlString( m_currentBuilderProfile.m_sSplashColor, out cBackgroundColor);

				if (m_currentBuilderProfile.m_SplashDefinesInProfile)
				{
					bShowUnityLogo = m_currentBuilderProfile.m_bUseUnitySplash;
					ColorUtility.TryParseHtmlString(m_currentBuilderProfile.m_sSplashColor, out cBackgroundColor);
				}

				//TODO : Add again the splash background picture management
				PlayerSettings.SplashScreen.showUnityLogo = bShowUnityLogo;
				PlayerSettings.SplashScreen.backgroundColor = cBackgroundColor;
				return NextBuildMethod(SetSplashs, "Use Unity Splash Screen system : " + PlayerSettings.SplashScreen.showUnityLogo);
			}
		}

		private bool IsProfileSceneReady()
		{
			SetEditorBuildSettingsScenes();
			return CurrentBuilderProfile.scenes.Count > 0;
		}

		private bool ChangeEnabledScenes()
		{
			if (IsBuilding() && m_bBuildAbort)
				return NextBuildMethod(ChangeEnabledScenes);

			if (m_nSelectedTarget < 0 || !IsProfileSceneReady())
				AbortBuild( "No scenes defined into the current profile." );

			return NextBuildMethod(ChangeEnabledScenes, "Scenes switched successfully");
		}
#endregion

#region PostBuild
#if UNITY_IOS
        private int ValidIOSSplashs()
        {
             if( m_BuilderTarget.m_eBuildTarget == BuildTarget.iOS )
            {
                if( string.IsNullOrEmpty( CurrentBuilderProfile.m_sSplashProfilePath ) )
                {
                    UpdateLogs( "'SPLASHS_FOLDER' is missing in the config file!", MessageType.Warning );
                    return 2;
                }

                string sSplashPath = IOUtility.CombineMultiplePaths(Constants.GetAssetsPath(), CurrentBuilderProfile.m_sSplashProfilePath);
                if(!Directory.Exists(sSplashPath))
                {
                    UpdateLogs("Directory '" + sSplashPath + "' not found!", MessageType.Error);
                    return 2;
                }

                string[] sFiles = Directory.GetFiles(sSplashPath);
                int nSplashsFound = 0;

                foreach(string sFile in sFiles)
                {
                    int nIndex = sFile.LastIndexOf(Path.DirectorySeparatorChar) + 1;
                    string sFileName = sFile.Substring(nIndex, sFile.LastIndexOf('.') - nIndex);
                    if (IOSUtils.HasSplashSize(ref sFileName))
                        nSplashsFound++;
                }

                if (nSplashsFound <= 0)
                {
                    UpdateLogs( "All iOS splash screens are missing.", MessageType.Warning );
                    return 2;
                }

                if (nSplashsFound < Constants.IOS_SPLASH_SIZES.Length)
                    UpdateLogs((Constants.IOS_SPLASH_SIZES.Length - nSplashsFound) + " iOS splash screens are missing.", MessageType.Warning);
            }
            return 0;
        }
    
        private bool UpdateIOSPlistFiles()
        {
            if ( !IsBuilding() || m_bBuildAbort || m_BuilderTarget.m_eBuildTarget != BuildTarget.iOS )
                return NextBuildMethod( UpdateIOSPlistFiles );

            string sFolder = GetSelectedTargetDestinationFolder();
            UpdateIOSPlistInDirectory( sFolder );
    
            return NextBuildMethod( UpdateIOSPlistFiles );
        }

        private int UpdateIOSPlistInDirectory( string sDirectory, bool bUpdateBundelIdentifier = false )
        {
            int nPlistFilesPatched = 0;
            if ( Directory.Exists( sDirectory ) )
            {
                foreach ( string sFile in Directory.GetFiles( sDirectory, "*.plist" ) )
                {
                    if ( !UpdateIOSPlist( sFile, bUpdateBundelIdentifier ) )
                        continue;

                    UpdateLogs( "'" + sFile + "' file patched." );
                    nPlistFilesPatched++;
                }
            }
            return nPlistFilesPatched;
        }
		
		private bool UpdateIOSPlist( string sPlistFile, bool bUpdateBundelIdentifier = false )
        {
            if ( !File.Exists( sPlistFile ) )
                return false;

            PListParser pListParser = new PListParser( sPlistFile );

            List<object> sLangList = new List<object>();

            for (int i = 0; i < m_currentBuilderProfile.m_sLanguageCultures.Count; i++)
            {
                Country country = CountryCode.GetCountry( m_currentBuilderProfile.m_sLanguageCultures[i] );
                sLangList.Add( country.m_sLanguageISO6391 );
            }
            pListParser.SetOneKeyMultipleValue( "CFBundleLocalizations", sLangList );
            if ( bUpdateBundelIdentifier )
                pListParser.SetOneKeyOneValue( "CFBundleIdentifier", Helpers.GetApplicationIdentifier() );
            pListParser.SetOneKeyOneValue( "NSCameraUsageDescription", ProjSettings.GetiOSCameraDescription () );
            pListParser.SetOneKeyOneValue( "NSPhotoLibraryUsageDescription", ProjSettings.GetiOSLibraryDescription () );
            pListParser.SetOneKeyOneValue( "NSLocationWhenInUseUsageDescription", ProjSettings.GetiOSLocationDescription() );
			string bluetoothDesc = ProjSettings.GetiOSBluetoothDescription();
			if (!string.IsNullOrEmpty(bluetoothDesc))
			{
				pListParser.SetOneKeyOneValue("NSBluetoothAlwaysUsageDescription", bluetoothDesc);
				// For older iOS than 13 (deprecated now)
				pListParser.SetOneKeyOneValue("NSBluetoothPeripheralUsageDescription", bluetoothDesc);
				// Needed if Bluetooth is available in background
				pListParser.SetOneKeyOneValue( "NSLocationAlwaysAndWhenInUseUsageDescription", ProjSettings.GetiOSLocationDescription() );
			}
            pListParser.SetOneKeyOneValue( "CFBundleDisplayName", GetProductName() );
            pListParser.SetOneKeyOneValue( "CFBundleVersion", m_currentBuilderProfile.m_sVersion );
            pListParser.SetOneKeyOneValue( "CFBundleShortVersionString", m_currentBuilderProfile.m_sVersion );

			//NOTE : Special case for iOS 13 
			pListParser.DeleteKeyEntry("UIApplicationExitsOnSuspend");

			pListParser.WriteToFile();

            return true;
        }
#endif
		private bool RestorePlugins()
		{
#if UNITY_ANDROID || UNITY_IOS
			if (!IsBuilding() || m_BuilderTarget.m_eBuildTarget !=
#if UNITY_ANDROID
				BuildTarget.Android
#elif UNITY_IOS
                BuildTarget.iOS
#endif
				|| (m_bBuildAbort && m_nAbortBuildMethod < Array.IndexOf(GetCurrentBuildMethods(), SetPlugins)))
				return NextBuildMethod(RestorePlugins, "Plugins restoration abort and current phase is " + m_nCurrentBuildMethodPhase.ToString(), MessageType.Warning);

			switch (m_nCurrentBuildMethodPhase)
			{
				case 0:
					return NextBuildMethodPhase("Restoring plugins...");
				case 1:
					string[] linkFiles = Directory.GetFiles(IOUtility.CombinePaths(Application.dataPath, "Plugins/"), "link_backup.xml", SearchOption.AllDirectories);
					for (int j = 0; j < linkFiles.Length; j++)
					{
						linkFiles[j] = linkFiles[j].Replace("\\", "/");
						int nIdxAssets = linkFiles[j].IndexOf("Assets/", StringComparison.Ordinal);
						string sAssetPath = linkFiles[j].Substring(nIdxAssets);
						AssetDatabase.RenameAsset(sAssetPath, "link");
					}

					foreach (PluginImporter plugin in PluginImporter.GetAllImporters())
					{
						for (int i = 0; i < m_sPluginsPath.Count; ++i)
						{
							if (plugin.assetPath.Contains(m_currentBuilderProfile.m_ePlatform.ToString()) && plugin.assetPath.Contains(m_sPluginsPath[i]))
								plugin.SetCompatibleWithPlatform(m_currentBuilderProfile.m_ePlatform, true);
						}
					}
					break;
			}
#endif
			return NextBuildMethod(RestorePlugins, "Plugins backups restored");
		}

		private bool PostBuildCustomMethod()
		{
			if (!IsBuilding() || (m_bBuildAbort && m_nAbortBuildMethod < Array.IndexOf(GetCurrentBuildMethods(), PreBuildCustomMethod)))
				return NextBuildMethod(PostBuildCustomMethod);

			Type postBuildCustomType = null;
			string[] allAsmDef = Directory.GetFiles(Application.dataPath + "/", "*.asmdef", SearchOption.AllDirectories);
			int nBuildMethodsFound = 0;
			int nAssemblyCount = allAsmDef.Length;
			if (allAsmDef.Length == 0)
			{
				postBuildCustomType = Type.GetType("BuildMethod,Assembly-CSharp-Editor");
				if (postBuildCustomType != null)
				{
					MethodInfo methodInfo = postBuildCustomType.GetMethod("PostBuild");
					if (methodInfo != null)
						methodInfo.Invoke(null, null);
					nBuildMethodsFound++;
					nAssemblyCount++;
				}
			}
			else
			{
				foreach (string singleAsmDef in allAsmDef)
				{
					string sAssemblyName = Path.GetFileNameWithoutExtension(singleAsmDef);
					postBuildCustomType = GetTypeFromAssemblyName(sAssemblyName, "BuildMethod");
					if (postBuildCustomType != null)
					{
						nBuildMethodsFound++;
						MethodInfo methodInfo = postBuildCustomType.GetMethod("PostBuild");
						if (methodInfo != null)
							methodInfo.Invoke(null, null);
					}
				}
			}

			return NextBuildMethod(PostBuildCustomMethod, nBuildMethodsFound.ToString() + " build methods found in the " + nAssemblyCount.ToString() + " assemblies");
		}

#if UNITY_IOS
        private bool PatchXCodeProject()
        {
            if( !IsBuilding() || m_bBuildAbort ||
                Application.platform != RuntimePlatform.OSXEditor ||
                m_BuilderTarget.m_eBuildTarget != BuildTarget.iOS )
                return NextBuildMethod( PatchXCodeProject );

            string sXCodeProjName = Directory.GetDirectories( GetSelectedTargetDestinationFolder(), "*.xcodeproj" )[ 0 ];
            string sUnityDataXcasset = Path.Combine(GetSelectedTargetDestinationFolder(), "UnityData.xcassets");

            if (Directory.Exists( sUnityDataXcasset ) )
                Directory.Delete( sUnityDataXcasset, true );
    
            //Trying to remove some files...
            string[] filesToRemove =
            {
                "libiconv.2.dylib",
                PBXProject.GetUnityTestTargetName ()
            };

            string sProjectPath = Path.Combine( sXCodeProjName, "project.pbxproj" );
            PBXProject project = new PBXProject();
            project.ReadFromString( File.ReadAllText( sProjectPath ) );

            for( int i = 0 ; i < filesToRemove.Length ; ++i )
            {
                string fileGuid = project.FindFileGuidByProjectPath( filesToRemove[i] );
                if (fileGuid != null)
                {
                    UpdateLogs( "Removing " + filesToRemove[i] + " from xcode project" );
                    project.RemoveFile( fileGuid );
                }
            }

            File.WriteAllText( sProjectPath, project.WriteToString() );
            return NextBuildMethod( PatchXCodeProject );
        }
#endif
		private void AddPluginToIncludes(ref string sIncludes, bool bPack, string sName)
		{
			if (bPack)
			{
				if (!string.IsNullOrEmpty(sIncludes))
					sIncludes += "%";
				sIncludes += sName;
			}
		}

#if UNITY_IOS
        private bool UpdateXCodeProj()
        {
            if( !IsBuilding() || m_bBuildAbort || m_BuilderTarget.m_eBuildTarget != BuildTarget.iOS )
                return NextBuildMethod( UpdateXCodeProj );

            string sLibrariesToInclude = string.Empty;
            string sPluginName = string.Empty;
        
            for (int i = 0; i < m_currentBuilderProfile.m_selectedPlugins.Count; ++i)
            {
                sPluginName = IOUtility.GetFolderName( m_currentBuilderProfile.m_selectedPlugins[ i ] ).ToUpper();
                AddPluginToIncludes( ref sLibrariesToInclude, true, sPluginName );

                if ( sPluginName.Equals( "FIREBASE" ))
                {
                    string sPluginFileName = string.Empty;
                    if (!m_currentBuilderProfile.TryGetPluginConfig("Firebase", out sPluginFileName))
                        AbortBuild("Missing PLUGIN_CONFIG_FILE in the profile for Firebase");

                    string sPluginPath = IOUtility.CombinePaths(Constants.GetPluginConfigFilesPath(), sPluginFileName);
                    if (!File.Exists(sPluginPath))
                        AbortBuild(sPluginPath + " does not exist : can't copy it for Firebase in iOS Project");

                    int nStartIndex = sLibrariesToInclude.IndexOf("FIREBASE") + "FIREBASE".Length;
                    sLibrariesToInclude = sLibrariesToInclude.Insert(nStartIndex, "=" + Constants.GetPluginConfigFilesPath() + sPluginFileName );
                }
                else
                {
                    int nStartIndex = sLibrariesToInclude.IndexOf(sPluginName) + sPluginName.Length;
                    sLibrariesToInclude = sLibrariesToInclude.Insert(nStartIndex, "=\"\"");
                }
            }

            if ( sLibrariesToInclude == string.Empty )
                return NextBuildMethod( UpdateXCodeProj );

            IOSUtils.XCodePatcher( m_currentBuilderProfile, GetSelectedTargetDestinationFolder(), sLibrariesToInclude );
            return NextBuildMethod( UpdateXCodeProj );
        }
#endif
		private bool BuildRunArchiveUpload()
		{
			if (!IsBuilding() || m_bBuildAbort
#if UNITY_IOS
                || m_BuilderTarget.m_eBuildTarget != BuildTarget.iOS
#elif UNITY_ANDROID
				|| m_BuilderTarget.m_eBuildTarget != BuildTarget.Android
#elif UNITY_STANDALONE_WIN
				|| m_BuilderTarget.m_eBuildTarget != BuildTarget.StandaloneWindows64
#endif
					)
				return NextBuildMethod(BuildRunArchiveUpload);

			string sBuildingOptions = string.Empty;
			AddPluginToIncludes(ref sBuildingOptions, m_bArchiveXCode, "archive");
			AddPluginToIncludes(ref sBuildingOptions, m_bUploadAppCenter, "appcenter");

			if (sBuildingOptions == string.Empty)
				return NextBuildMethod(BuildRunArchiveUpload);

			string logsPath = Application.dataPath + "/../../Logs";
#if UNITY_IOS
            string sXCodeProjName = Directory.GetDirectories( GetSelectedTargetDestinationFolder(), "*.xcodeproj" )[0];
            string sDefaultProjectName = IOSUtils.GetXCodeProjectName();
            if( Directory.Exists( sXCodeProjName+"/xcshareddata" ))
            {
                string sSchemesPath = sXCodeProjName + "/xcshareddata/xcschemes/";
                string[] files = Directory.GetFiles(sSchemesPath, "*.xcscheme" );
                if( files.Length == 1 && files[0].Contains( sDefaultProjectName ) )
                {
                    string sFinalName = "sch-" + IOSUtils.GetXCodeProjectName() + ".xcscheme";
                    File.Move(files[0], Path.GetDirectoryName(files[0]) + "/" + sFinalName);
                }
            }
    
            string sExportFilePath = GetSelectedTargetDestinationFolder() + "/build/exportOptions.plist";
            PlistDocument docExportOptions = new PlistDocument();
            docExportOptions.Create();
            PlistElementDict dicElem = docExportOptions.root;
            dicElem.SetString( "method", "ad-hoc" );
            dicElem.SetBoolean( "compileBitcode", false );
            dicElem.SetBoolean( "uploadBitcode", false );
            docExportOptions.WriteToFile(sExportFilePath);

            string sExportFilePathRelease = GetSelectedTargetDestinationFolder() + "/build/exportOptions-store.plist";
            PlistDocument docExportOptionsRelease = new PlistDocument();
            docExportOptionsRelease.ReadFromFile(sExportFilePath);
            docExportOptionsRelease.root.SetString( "method", "app-store" );
            docExportOptionsRelease.WriteToFile(sExportFilePathRelease);

            string sIdentifier = PlayerSettings.applicationIdentifier.Substring( Helpers.GetApplicationIdentifier().LastIndexOf(".") + 1 );
            string sPListPath = GeneratePListExportOptions();

            string sArguments = "\"" + GetBuilderScriptFolder() + "/ProjectEdit/BuildGenerator.py" + "\" "
                        + sBuildingOptions +
                        " \"" + GetSelectedTargetDestinationFolder() + "\"" +
                        " \"" + sXCodeProjName + "\""+
                        " \"" + ProjSettings.GetOwnerName() + "\"" +
                        " \"" + ProjSettings.GetAppName() + "\"" +
                        " \"" + _AppCenterTeamsName + "\"" +
                        " \"" + sIdentifier + "\"" +
                        " \"" + sPListPath + "\"" +
						" \"" + logsPath + "\"";
            Utils.ThreadProcess( this, "python", sArguments, "", true, false, true );
#elif UNITY_ANDROID
			if (!m_bUploadAppCenter)
				return NextBuildMethod(BuildRunArchiveUpload);

			string sPathToAPK = GetSelectedTargetDestinationFolder() + '/' + GetExecutableName() + ".apk";

			if (Application.platform == RuntimePlatform.OSXEditor)
			{
				string sScriptPath = "python";
				string sArguments = '"' + GetBuilderScriptFolder() + "/ProjectEdit/UploadHockeyAppApk.py\"" +
					" \"" + sPathToAPK + "\"" +
					" \"" + ProjSettings.GetOwnerName() + "\"" +
					" \"" + ProjSettings.GetAppName() + "\"" +
					" \"" + _AppCenterTeamsName + "\"";
				Utils.ThreadProcess(this, sScriptPath, sArguments, "", true, false, true);
			}
#elif UNITY_STANDALONE_WIN

			if (!m_bUploadAppCenter)
				return NextBuildMethod(BuildRunArchiveUpload);

			if (Application.platform == RuntimePlatform.OSXEditor)
			{
				string sArguments = '"' + GetBuilderScriptFolder() + "/ProjectEdit/UploadAppCenter_Windows.py\"" +
					" \"" + ProjSettings.GetOwnerName() + "\"" +
					" \"" + ProjSettings.GetAppName() + "\"" +
					" \"" + _AppCenterTeamsName + "\"" +
					" \"" + GetSelectedTargetDestinationFolder() + "\"" +
					" \"" + PlayerSettings.bundleVersion + "\"";
				Utils.ThreadProcess(this, "python", sArguments, "", true, false, true);
			}

#endif
			return NextBuildMethod(BuildRunArchiveUpload);
		}

#if UNITY_IOS
		private string GeneratePListExportOptions()
		{
			string plistPath = IOUtility.CombinePaths( GetBuilderScriptFolder(), "ProjectEdit/ExportOptions.plist" );

			// Get plist
			PlistDocument plist = new PlistDocument();
			plist.Create();

			// Get root
			PlistElementDict rootDict = plist.root;

			// Change value of CFBundleVersion in Xcode plist
			var buildKey = "method";
			rootDict.SetString(buildKey, IOSProfile.ConvertToXCodeMethodName( ((IOSProfile)m_currentBuilderProfile).m_XCodeMethod ));

			// Write to file
			File.WriteAllText(plistPath, plist.WriteToString());
			return plistPath;
		}
#endif

		private bool RestoreBuildSettings()
		{
			if (File.Exists(Constants.GetAssetsPath() + "/Resources/BuildSettings.txt"))
				AssetDatabase.DeleteAsset("Assets/Resources/BuildSettings.txt");

			return NextBuildMethod(RestoreBuildSettings, "BuildSettings.txt removed.");
		}

		private bool RestoreGlobalDefines()
		{
			if (!IsBuilding() || (m_bBuildAbort && m_nAbortBuildMethod < Array.IndexOf(GetCurrentBuildMethods(), SaveGlobalDefines)))
				return NextBuildMethod(RestoreGlobalDefines);

			bool bSave = false;

			if (m_BuilderTarget.m_eBuildTarget == BuildTarget.iOS || m_BuilderTarget.m_eBuildTarget == BuildTarget.Android)
			{
				for (int i = 0; i < m_sPluginsPath.Count; ++i)
				{
					if (!m_currentBuilderProfile.m_selectedPlugins.Contains(m_sPluginsPath[i]))
					{
						string sFolderName = IOUtility.GetFolderName(m_sPluginsPath[i]);
						bSave |= RemoveGlobalDefine("DONT_USE_" + sFolderName.ToUpper());
					}
				}
			}

			if (!bSave)
				return NextBuildMethod(RestoreGlobalDefines);

			m_globalDefineManager.Save(EditorUserBuildSettings.selectedBuildTargetGroup);

			return NextBuildMethod(RestoreGlobalDefines, "Restored global defines.");
		}

		private bool RestoreEditorPrefs()
		{
			if (!IsBuilding() || (m_bBuildAbort && m_nAbortBuildMethod < Array.IndexOf(GetCurrentBuildMethods(), SetEditorPrefs)))
				return NextBuildMethod(RestoreEditorPrefs);

			if (m_bVerifySavingAssets)
			{
				EditorPrefs.SetBool("VerifySavingAssets", true);
				m_bVerifySavingAssets = false;

				return NextBuildMethod(RestoreEditorPrefs, "Enabled 'Verify Saving Assets' setting.");
			}

			return NextBuildMethod(RestoreEditorPrefs);
		}

		private bool AndroidInstall()
		{
			if (!IsBuilding() || m_bBuildAbort || m_BuilderTarget.m_eBuildTarget != BuildTarget.Android || !m_bAndroidInstallAfterBuild)
				return NextBuildMethod(AndroidInstall);

			UpdateLogs("If no Android device is found. This step will freeze.\nConnect an Android device to unfreeze it.");

			string sExecutableName = GetExecutableName();

			string sAdbPath = EditorPrefs.GetString("AndroidSdkRoot") + "/platform-tools/adb.exe";
			Utils.ThreadProcess(this, sAdbPath, "install -r \"" + GetSelectedTargetDestinationFolder() + '/' + sExecutableName + ".apk\"", "", true, true, true);

			return NextBuildMethod(AndroidInstall, "Device found. Installing the APK. Please wait...");
		}

		private bool BuildLog()
		{
			if (!IsBuilding())
				return NextBuildMethod(BuildLog);

			string sFilePath = Application.dataPath + "/../../Logs/BuilderLog-" + m_currentBuilderProfile.m_ePlatform.ToString() + "_" + m_currentBuilderProfile.m_sBundleIdentifier + ".log";

			Directory.CreateDirectory(Path.GetDirectoryName(sFilePath));
			using (TextWriter streamWriter = new StreamWriter(File.Open(sFilePath, FileMode.Create)))
			{
				streamWriter.Write(m_sSimpleBuildInfos);

				if (m_bBuildAbort)
					streamWriter.WriteLine("\n\nBuild abort.");
				else
					streamWriter.WriteLine("\n\nBuild successfully complete.");

				if (m_nBuildWarning > 0)
					streamWriter.WriteLine("Number of warnings detected: " + m_nBuildWarning + '.');
				if (m_nBuildErrors > 0)
					streamWriter.WriteLine("Number of errors detected: " + m_nBuildErrors + '.');
			}

			return NextBuildMethod(BuildLog, "Build log saved.");
		}

		private bool BuildPlayer()
		{
			if (!IsBuilding() || m_bBuildAbort)
				return NextBuildMethod(BuildPlayer);

			switch (m_nCurrentBuildMethodPhase)
			{
				case 0:
					string sDestFolder = GetSelectedTargetDestinationFolder();
					if (!Directory.Exists(sDestFolder))
						Directory.CreateDirectory(sDestFolder);
					EditorUserBuildSettings.SetBuildLocation(m_BuilderTarget.m_eBuildTarget, sDestFolder);

					AssetDatabase.SaveAssets();
					AssetDatabase.Refresh();

					return NextBuildMethodPhase("Building player...");
				default:
					List<string> sScenes = new List<string>();

					if (m_BuilderState == BuilderState.Building)
					{
						for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
						{
							if (EditorBuildSettings.scenes[i].enabled)
							{
								if (File.Exists(Constants.GetAssetsPath() + "/../" + EditorBuildSettings.scenes[i].path))
									sScenes.Add(EditorBuildSettings.scenes[i].path);
								else
								{
									UpdateLogs("Scene '" + EditorBuildSettings.scenes[i].path + "' not found.", MessageType.Warning);
								}
							}
						}
					}

					BuildOptions buildOptions = ProjSettings.IsReleaseBuild() ? BuildOptions.None : BuildOptions.Development;
					if (m_bAutoRun && (m_BuilderTarget.m_eBuildTargetGroup == BuildTargetGroup.Standalone))
						buildOptions |= BuildOptions.AutoRunPlayer;
					else if (m_BuilderTarget.m_eBuildTarget == BuildTarget.Android && m_bGoogleProject)
						buildOptions |= BuildOptions.AcceptExternalModificationsToPlayer;

					string sError = string.Empty;
#if UNITY_2018_1_OR_NEWER
					UnityEditor.Build.Reporting.BuildReport report = BuildPipeline.BuildPlayer(sScenes.ToArray(), GetFullPathBuild(), m_BuilderTarget.m_eBuildTarget, buildOptions);
					if (report.steps.Length > 0)
					{
						for (int nStepIdx = 0; nStepIdx < report.steps.Length; ++nStepIdx)
						{
							if (report.steps[nStepIdx].messages.Length > 0)
							{
								for (int nMsgIdx = 0; nMsgIdx < report.steps[nStepIdx].messages.Length; ++nMsgIdx)
								{
									if (report.steps[nStepIdx].messages[nMsgIdx].type == LogType.Error)
										sError += report.steps[nStepIdx].messages[nMsgIdx].content + '\n';
								}
							}
						}
					}
#else
					sError = BuildPipeline.BuildPlayer(sScenes.ToArray(), GetFullPathBuild(), m_BuilderTarget.m_eBuildTarget, buildOptions);
#endif
					if (!string.IsNullOrEmpty(sError))
					{
						return AbortBuild("Player build failed: " + sError, false);
					}

					UpdateLogs("Player build complete.");

					if (m_BuilderTarget.m_eBuildTarget == BuildTarget.Android && m_bAndroidInstallAfterBuild)
						UpdateLogs("Don't forget to connect an Android device.");

					return NextBuildMethod(BuildPlayer, "Recompiling. Please wait...");
			}
		}

		private bool FinalStepBuild()
		{
			switch (m_nCurrentBuildMethodPhase)
			{
				case 0:
					if (!m_bBuildAbort)
					{
						if (!Application.isBatchMode)
						{
							string sLocation = EditorUserBuildSettings.GetBuildLocation(m_BuilderTarget.m_eBuildTarget);
							EditorUtility.RevealInFinder(IOUtility.ReplacePathSeparator(sLocation));
						}
					}

					if (!IsBuilding() || (m_bBuildAbort && m_nAbortBuildMethod < Array.IndexOf(GetCurrentBuildMethods(), SaveGlobalDefines)))
						return NextBuildMethod(FinalStepBuild);

					return NextBuildMethod(FinalStepBuild);
			}

			return NextBuildMethod(FinalStepBuild);
		}

		private bool EndBuildGame()
		{
			m_BuilderState = BuilderState.BuildComplete;

			bool bReturn;

			if (m_bBuildAbort)
				bReturn = NextBuildMethod(EndBuildGame, "Build abort. " + m_nBuildErrors + " errors detected.", MessageType.Error);
			else if (m_nBuildErrors > 0)
				bReturn = NextBuildMethod(EndBuildGame, "Build complete. " + m_nBuildErrors + " errors detected.", MessageType.Error);
			else if (m_nBuildWarning > 0)
				bReturn = NextBuildMethod(EndBuildGame, "Build complete. " + m_nBuildWarning + " warnings detected.", MessageType.Warning);
			else
				bReturn = NextBuildMethod(EndBuildGame, "Build successfully complete!");

			if (Application.isBatchMode)
				EditorApplication.Exit((m_bBuildAbort || (m_nBuildErrors > 0)) ? 1 : 0);

			return bReturn;
		}
#endregion

#region Tools
		public void NeedToChangeTarget(int nNewTarget)
		{
			if (m_nSelectedTarget != nNewTarget)
			{
				m_nSelectedTarget = nNewTarget;
				m_BuilderTarget = new Target(Constants.BUILDER_TARGETS_LIST[m_nSelectedTarget]);
				Profile p = Profile.GetTypedProfile(m_currentBuilderProfile, m_BuilderTarget);
				m_currentBuilderProfile = p;
				LoadPlugins();
			}
		}

		public void SetBundleVersion(string sVersion = "")
		{
			if (!string.IsNullOrEmpty(sVersion))
				m_currentBuilderProfile.m_sVersion = sVersion;

			int nBundleCodeVersion = GetBundleVersionCode();
			if (nBundleCodeVersion >= 0)
				PlayerSettings.Android.bundleVersionCode = nBundleCodeVersion;

			PlayerSettings.bundleVersion = m_currentBuilderProfile.m_sVersion;
		}

		public int GetBundleVersionCode()
		{
			string[] sVersionNumberArray = m_currentBuilderProfile.m_sVersion.Split('.');
			if (sVersionNumberArray.Length > 2 &&
				sVersionNumberArray[0].Length > 0 &&
				sVersionNumberArray[1].Length > 0 &&
				sVersionNumberArray[2].Length > 0)
				return ParseTools.ParseIntSafe(sVersionNumberArray[0]) * 100000 + ParseTools.ParseIntSafe(sVersionNumberArray[1]) * 1000 + ParseTools.ParseIntSafe(sVersionNumberArray[2]);

			return -1;
		}
#endregion

		public Func<bool>[] GetCurrentBuildMethods()
		{
			return m_BuildMethods;
		}

		private string GetBuilderScriptFolder()
		{
			// Search the path of this file
			string[] fullFilePath = Directory.GetFiles(Directory.GetCurrentDirectory(), "BuilderModel.cs", SearchOption.AllDirectories);

			if (fullFilePath.Length > 0)
				return Path.GetDirectoryName(fullFilePath[0]); // Return it directy path
			else
				return Directory.GetCurrentDirectory() + "/Assets/Plugins/Scripts/Editor/BuildSettings"; // Return the default path (Path valid only on the App project)
		}

#region Getters
		private string GetFullPathBuild()
		{
			string sFullPath;
			switch (m_BuilderTarget.m_eBuildTarget)
			{
				case BuildTarget.iOS:
				case BuildTarget.WebGL:
				case BuildTarget.StandaloneLinux64:
					sFullPath = GetSelectedTargetDestinationFolder();
					break;
				default:
					sFullPath = IOUtility.CombinePaths(GetSelectedTargetDestinationFolder(), GetExecutableName());
					break;
			}

			// En fonction de la cible on rajoute l'extension nécessaire
			switch (m_BuilderTarget.m_eBuildTarget)
			{
				case BuildTarget.StandaloneWindows:
				case BuildTarget.StandaloneWindows64:
					sFullPath += ".exe";
					break;
				case BuildTarget.Android:
					if (!m_bGoogleProject)
						sFullPath += ".apk";
					break;
				case BuildTarget.StandaloneOSX:
					sFullPath += ".app";
					break;
			}

			Debug.Log("GetFullPathBuild => sFullPath : " + sFullPath);

			return sFullPath;
		}

		public bool IsBuilding()
		{
			if (m_BuilderState == BuilderState.Building)
				return true;

			return false;
		}

		public bool IsSigning()
		{
			if (m_BuilderState == BuilderState.SigningAPK)
				return true;

			return false;
		}

		private string GetProductName()
		{
			if (!string.IsNullOrEmpty(m_currentBuilderProfile.m_sProductName))
				return m_currentBuilderProfile.m_sProductName;
			return string.Empty;
		}

		public string GetExecutableName()
		{
			string sResult = null;
			if (!Application.isBatchMode)
			{
				sResult = EditorPrefs.GetString("Builder.ExecutableName." + m_currentBuilderProfile.m_sBundleIdentifier + "." + m_nCurrentProfile.ToString("00"));
				Debug.Log("GetExecutableName sResult : " + sResult);
			}
			if (string.IsNullOrEmpty(sResult))
				sResult = m_currentBuilderProfile.m_sProductName;

			return sResult;
		}

		public string GetSelectedTargetDestinationFolder()
		{
			Debug.Log("GetSelectedTargetDestinationFolder => sLocation : " + m_sDestinationFolders[m_nCurrentProfile]);
			return IOUtility.ReplacePathSeparator(m_sDestinationFolders[m_nCurrentProfile]);
		}
#endregion

#region Checks
		public bool ValidIcons()
		{
			//No icons needed with WebGL target
			if (m_BuilderTarget.m_eBuildTargetGroup == BuildTargetGroup.WebGL || m_BuilderTarget.m_eBuildTargetGroup == BuildTargetGroup.Android)
				return true;

			return !(m_currentBuilderProfile.iconTexture is null);
		}

		public string CheckMissingPlugins()
		{
			if (m_currentBuilderProfile != null)
			{
				for (int i = 0; i < m_currentBuilderProfile.m_selectedPlugins.Count; i++)
				{
					if (!m_sPluginsPath.Contains(m_currentBuilderProfile.m_selectedPlugins[i]))
						return m_currentBuilderProfile.m_selectedPlugins[i];
				}
			}
			return string.Empty;
		}
#endregion

		public void SetBuildLocation(string sLocation)
		{
			DLog.Log($"TEST - SetBuildLocation enter sLocation = {sLocation}");
			if (string.IsNullOrEmpty(sLocation))
				return;

			sLocation = IOUtility.ReplacePathSeparator(sLocation);
			Debug.Log("SetBuildLocation => sLocation : " + sLocation);
			m_sDestinationFolders[m_nCurrentProfile] = Path.GetDirectoryName(sLocation);

			EditorPrefs.SetString("Builder.ExecutableName." + m_currentBuilderProfile.m_sBundleIdentifier + "." + m_nCurrentProfile.ToString("00"), m_currentBuilderProfile.m_sProductName);
			EditorPrefs.SetString("Builder.destinationFolders." + m_currentBuilderProfile.m_sBundleIdentifier + "." + m_nCurrentProfile.ToString("00"), GetSelectedTargetDestinationFolder());
		}

		public bool StartBuildingGame()
		{
			return m_update();
		}

		public void ResetInfosOnBuilderChanges(BuilderState state)
		{
			//Caution with this one 
			m_BuilderState = state;
			m_bBuildAbort = m_bBuildAbortByUser = m_bBuildPaused = m_bBuildErrorPause = false;
			m_nCurrentBuildMethod = m_nBuildErrors = m_nBuildWarning = 0;
			m_nAbortBuildMethod = m_nCurrentBuildMethodPhase = -1;

			m_sSimpleBuildInfos = string.Empty;
		}

#region Building
		public void StartBuildingCheck()
		{
			if (m_BuilderTarget.m_eBuildTarget == BuildTarget.Android && !AndroidUtils.CheckAndroidSDKAndJDK())
				return;

			bool isTargetGroupStandaloneWindows = m_BuilderTarget.m_eBuildTarget == BuildTarget.StandaloneWindows || m_BuilderTarget.m_eBuildTarget == BuildTarget.StandaloneWindows64;
			string sLocation = null;
			if (isTargetGroupStandaloneWindows || (m_BuilderTarget.m_eBuildTarget == BuildTarget.Android && !m_bGoogleProject))
			{
				sLocation = EditorUtility.SaveFilePanel("Build destination file", GetSelectedTargetDestinationFolder(), GetExecutableName(), isTargetGroupStandaloneWindows ? "exe" : "apk");
				SetBuildLocation(sLocation);
			}
			else
			{
				sLocation = EditorUtility.SaveFolderPanel("Build destination folder", GetSelectedTargetDestinationFolder(), string.Empty);
				// SetBuildLocation need a folder with "/" at end
				if (!string.IsNullOrEmpty(sLocation) && sLocation[sLocation.Length - 1] != Path.DirectorySeparatorChar)
					sLocation += Path.DirectorySeparatorChar;
				SetBuildLocation(sLocation);
			}

			if (string.IsNullOrEmpty(sLocation))
				return;

			StartBuildingGame();
		}

		public void UpdatePause()
		{
			if (m_eBuildControl == BuildControl.PhaseByPhase && !m_bBuildAbort && m_nCurrentBuildMethod > 0 &&
			   nCurrentBuildMethodPhase == 0 && BuildEndPhaseMethods.Contains(GetCurrentBuildMethods()[m_nCurrentBuildMethod - 1]))
				m_bBuildPaused = true;
		}

		public void UpdateLogs(string sInfo, MessageType type = MessageType.Info, bool bHelpBox = true, bool bIncrementErrors = false, bool bNewLine = true)
		{
			if (bIncrementErrors && IsBuilding() && type == MessageType.Error)
				IncrementErrors();

			if (IsBuilding() && type == MessageType.Warning)
				IncrementWarnings();

			if (string.IsNullOrEmpty(sInfo))
				return;

			switch (type)
			{
				case MessageType.Info:
					m_sSimpleBuildInfos += "<INFO>";
					break;
				case MessageType.Warning:
					m_sSimpleBuildInfos += "<WARNING>";
					break;
				case MessageType.Error:
					m_sSimpleBuildInfos += "<ERROR>";
					break;
			}

			if (m_logs != null)
				m_logs(sInfo, type, bHelpBox, bIncrementErrors, bNewLine);

			if (sInfo.Length > 1 && sInfo[sInfo.Length - 1] != '\n')
				sInfo += '\n';

			m_sSimpleBuildInfos += sInfo;
		}

		// ENTRY POINT FOR UNITY BATCH MODE
		//
		// Supported parameters :
		// -BUILD_PROFILE=<name> : selects the profile to build (located in Config/Profiles), without the .xml extension
		// -BUILD_DESTINATION=<path> : set the build destination path, use "" if path contains spaces
		// Example :
		// "C:\Program Files (x86)\Unity\Editor\Unity.exe" -batchmode -executeMethod Builder.BatchBuild -BUILD_PROFILE=Google_beta -BUILD_DESTINATION="P:/Projects/Test/Build/Android/GoogleTestApp.apk"
		//
		// Return values :
		// 0=Build completed, no errors
		// 1=Build completed, with errors
		// 2=Build aborted
		public static void BatchBuild()
		{
			s_model = new BuilderModel();
			s_model.m_bRecompileOnly = ParseTools.ParseBoolSafe(GlobalTools.GetCommandLineArg("RECOMPILE_ONLY"), false);
			s_model.Init();

			string sProfile = GlobalTools.GetCommandLineArg("BUILD_PROFILE");
			m_sTempLocation = GlobalTools.GetCommandLineArg("BUILD_DESTINATION");

			string sProjectVersion = GlobalTools.GetCommandLineArg("BUILD_PROJECT_VERSION");

			m_sAndroidSdkLocation = GlobalTools.GetCommandLineArg("ANDROID_SDK_PATH");
			m_sAndroidNdkLocation = GlobalTools.GetCommandLineArg("ANDROID_NDK_PATH");

#if UNITY_IOS || UNITY_ANDROID
			s_model._AppCenterTeamsName = GlobalTools.GetCommandLineArg("APPCENTER_TEAMS", "Unity-Dev");
#endif
			s_model.LoadProfiles(sProfile);

			string sBundleVersion = s_model.m_currentBuilderProfile.m_sVersion;

			if (sProjectVersion != string.Empty)
				sBundleVersion = sProjectVersion;

			int nBundleVersionLastPartIndex = sBundleVersion.LastIndexOf('.') + 1;

			if (nBundleVersionLastPartIndex >= 0)
			{
				string sBundleVersionLastPart = (ParseTools.ParseIntSafe(sBundleVersion.Substring(nBundleVersionLastPartIndex)) + 1).ToString();
				if (sProjectVersion != string.Empty)
					sBundleVersionLastPart = (ParseTools.ParseIntSafe(sBundleVersion.Substring(nBundleVersionLastPartIndex))).ToString();
				s_model.m_currentBuilderProfile.m_sVersion = PlayerSettings.bundleVersion = sBundleVersion.Substring(0, nBundleVersionLastPartIndex) + sBundleVersionLastPart;
				PlayerSettings.Android.bundleVersionCode = s_model.GetBundleVersionCode();
			}

			if (s_model.m_BuilderTarget.m_eBuildTarget == BuildTarget.iOS)
			{
				s_model.m_bBuildXCode = GlobalTools.CommandLineArgExist("BUILD_XCODE");
				s_model.m_bArchiveXCode = GlobalTools.CommandLineArgExist("BUILD_ARCHIVE");
				s_model.m_sAppleID = GlobalTools.GetCommandLineArg("APPLE_DEVELOPER_ID");
				if (!string.IsNullOrEmpty(s_model.m_sAppleID))
				{
					PlayerSettings.iOS.appleEnableAutomaticSigning = true;
					PlayerSettings.iOS.appleDeveloperTeamID = s_model.m_sAppleID;
				}

			}
			if (s_model.m_BuilderTarget.m_eBuildTarget == BuildTarget.Android || s_model.m_BuilderTarget.m_eBuildTarget == BuildTarget.iOS || s_model.m_BuilderTarget.m_eBuildTarget == BuildTarget.StandaloneWindows64)
				s_model.m_bUploadAppCenter = GlobalTools.CommandLineArgExist("BUILD_APPCENTER");

			s_model.SetBuildLocation(m_sTempLocation);
			s_model.ResetInfosOnBuilderChanges(BuilderState.Building);
			s_model.ProcessMethodInBatchingMode(); //Replace EditorApplication.update in batchmode;  
		}

#endregion
		public void UpdateBuild()
		{
			if ((!IsBuilding() && !IsSigning()))
				return;

			for (int i = 0; i < Utils.m_ThreadsRunning.Count; i++)
				if (Utils.m_ThreadsRunning[i] == null || !Utils.m_ThreadsRunning[i].IsAlive)
					Utils.m_ThreadsRunning.RemoveAt(i--);

			if (Utils.m_ThreadsRunning.Count <= 0 && Utils.m_ThreadsToRun.Count > 0)
			{
				Utils.m_ThreadsToRun[0].Start();
				Utils.m_ThreadsRunning.Add(Utils.m_ThreadsToRun[0]);
				Utils.m_ThreadsToRun.RemoveAt(0);
				while (Utils.m_ThreadsRunning[0].ThreadState == ThreadState.Unstarted)
					;
			}

			if (GetCurrentBuildMethods() == null)
				InitBuildMethods();

			GetCurrentBuildMethods()[m_nCurrentBuildMethod]();
		}
		
		public void ProcessMethodInBatchingMode()
		{
			while (GetCurrentBuildMethods() != null)
			{
				UpdateBuild();
			}
		}

		private bool WriteDateVersion()
		{
			if (IsBuilding() && m_bBuildAbort)
				return NextBuildMethod(WriteDateVersion);

			Common.Version.Version.WriteVersion(m_currentBuilderProfile.m_sVersion);
			AssetDatabase.Refresh();

			return NextBuildMethod(WriteDateVersion, "Date version written.");
		}
	}
}