﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.U2D;
using UnityEngine;
using UnityEngine.U2D;

[InitializeOnLoad]
public class MultiPPUInit : AssetPostprocessor
{
	static bool bRegenerating = false;
	static void OnPostprocessSprites(Texture2D tex, Sprite[] sprites)
	{
		if (!bRegenerating)
		{
			bRegenerating = true;
			RegenerateAll();
			bRegenerating = false;
		}
	}
	static MultiPPUInit()
	{
		//DO NOT DO THIS : RegenerateAll();
		EditorApplication.playModeStateChanged += HandleOnPlayModeChanged;
	}
	

	private static void RegenerateAll()
	{
		//on startup, we regenerate the sprites
		var list = FindAssetsByType<MultiPPUSprite>();
		foreach (MultiPPUSprite spriteGroup in list)
		{
			spriteGroup.OnValidate();
		}
	}

	static void HandleOnPlayModeChanged(PlayModeStateChange state)
	{
		if(state == PlayModeStateChange.EnteredPlayMode || state == PlayModeStateChange.EnteredEditMode)
			RegenerateAll();
	}

	public static List<T> FindAssetsByType<T>() where T : UnityEngine.Object
	{
		List<T> assets = new List<T>();
		string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
		for (int i = 0; i < guids.Length; i++)
		{
			string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
			T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
			if (asset != null)
			{
				assets.Add(asset);
			}
		}
		return assets;
	}
}
