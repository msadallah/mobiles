namespace AllMyScripts.Editor.GlobalDefines
{
    using UnityEngine;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using UnityEditor;
    using AllMyScripts.Common.Tools;
    using AllMyScripts.Editor.Builder;
    using System;

    [System.Serializable]
	public class GlobalDefine : IEquatable<GlobalDefine>
	{
		[SerializeField] private string m_sName = default;
		[SerializeField] private bool m_bEnabled = default;
		[SerializeField] private bool m_bSavedEnabled = default;

		[NonSerialized]
		private bool _isCurrentlyUsed = default;

		public GlobalDefine( string sName, bool bEnabled, bool isCurrentlyUsed) 
		{
			m_sName = sName;
			m_bEnabled = bEnabled;
			m_bSavedEnabled = bEnabled;
			_isCurrentlyUsed = isCurrentlyUsed;
		}

		public GlobalDefine( GlobalDefine globalDefine )
		{
			m_sName = globalDefine.sName;
			m_bEnabled = globalDefine.bEnabled;
			m_bSavedEnabled = globalDefine.m_bSavedEnabled;
			_isCurrentlyUsed = globalDefine.isCurrentlyUsed;
		}

		public GlobalDefine( SerializationInfo info )
		{
			m_sName = info.GetString( "m_sName" );
			m_bEnabled = info.GetBoolean( "m_bEnabled" );
			m_bSavedEnabled = info.GetBoolean("m_bSavedEnabled");
			_isCurrentlyUsed = false;
		}

		public void GetObjectData( SerializationInfo info )
		{
			info.AddValue( "m_sName", m_sName );
			info.AddValue( "m_bEnabled", m_bEnabled );
			info.AddValue( "m_bSavedEnabled", m_bSavedEnabled );
		}

		public string sName
		{
			get
			{
				return m_sName;
			}
			set
			{
				m_sName = value;
			}
		}

		public bool bEnabled
		{
			get
			{
				return m_bEnabled;
			}
			set
			{
				m_bEnabled = value;
			}
		}

		public bool bSavedEnabled
		{
			get
			{
				return m_bSavedEnabled;
			}
			set
			{
				m_bSavedEnabled = value;
			}
		}

		public bool isCurrentlyUsed
		{
			get => _isCurrentlyUsed;
			set => _isCurrentlyUsed = value;
		}

		public void Reset()
		{
			m_bEnabled = false;
			m_bSavedEnabled = false;
		}

		/// <summary>
		///  Only use the name to define defines equality
		/// </summary>
		/// <param name="globalDefine">Global define to compare with</param>
		/// <returns>true if names are equals</returns>
		public bool Equals(GlobalDefine globalDefine)
		{
			return sName == globalDefine.sName;
		}

		/// <summary>
		/// Define hashcode based on name cause the rest is pointless to test global define equality
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public int GetHashCode(GlobalDefine obj)
		{
			return sName.GetHashCode();
		}
	}

	[System.Serializable]
	public class GlobalDefineManager
	{
		[SerializeField]
		private List<GlobalDefine> m_globalDefines = new List<GlobalDefine>();
		public List<GlobalDefine> GlobalDefines { get { return m_globalDefines; } }
				
		public void Init()
		{
			ReloadGlobalDefines();
		}

		public void UpdateCurrentlyUsedGlobalDefines(BuildTargetGroup targetGroup)
		{
			foreach (GlobalDefine globalDefine in m_globalDefines)
			{
				globalDefine.isCurrentlyUsed = false;
			}

			string[] sGlobalDefines = ReadScriptingSymbols( targetGroup );

			if (sGlobalDefines != null && !string.IsNullOrEmpty(sGlobalDefines[0]))
			{
				foreach (string sGlobalDefine in sGlobalDefines)
				{
					bool bFound = false;
					foreach (GlobalDefine globalDefine in m_globalDefines)
					{
						if (globalDefine.sName == sGlobalDefine)
						{
							globalDefine.isCurrentlyUsed = true;
							bFound = true;
							break;
						}
					}

					if (!bFound)
					{
						GlobalDefine globalDefine = new GlobalDefine(sGlobalDefine, true, true);
						m_globalDefines.Add(globalDefine);
					}
				}
			}
		}
		
		public string[] GetEnabledGlobalDefines( List<GlobalDefine> globalDefines )
		{
			List<string> sGlobalDefines = new List<string>();
			foreach ( GlobalDefine globalDefine in globalDefines )
			{
				if ( globalDefine.bEnabled )
					sGlobalDefines.Add( globalDefine.sName );
			}
			return sGlobalDefines.ToArray();
		}

		public bool AlreadyContainsGDAtId(int globalDefinesIdx)
		{
			bool alreadyExists = false;
			for (int i = 0; i < GlobalDefines.Count; ++i)
			{
				if (i == globalDefinesIdx)
					continue;

				if (GlobalDefines[i].Equals(GlobalDefines[globalDefinesIdx]))
				{
					alreadyExists = true;
					break;
				}
			}

			return alreadyExists;
		}

		public List<GlobalDefine> LoadFromXmlFile()
		{
			List<GlobalDefine> globalDefines = new List<GlobalDefine>();
			string globalDefinesFilePath = IOUtility.CombineMultiplePaths(Utils.GetBuildFolder(),"Config","GlobalDefines.xml");
			string filecontent = GlobalTools.LoadTextFileAtFullPath( globalDefinesFilePath );
			Common.Tools.Xml.XmlReader xr = new Common.Tools.Xml.XmlReader();
			if ( xr.Init( filecontent ) )
			{
				while ( xr.Read() )
				{
					if ( !xr.IsNodeElement() ) continue;

					switch ( xr.Name.ToUpper() )
					{
						case "GLOBALDEFINE":
							string name = xr.GetAttribute( "name", "" );
							if( !string.IsNullOrEmpty(name))
							{
								GlobalDefine globalDefine = new GlobalDefine(name, false, false);
								globalDefines.Add(globalDefine);
							}
						break;
					}
				}
			}
			return globalDefines;
		}

		public void ReloadGlobalDefines()
		{
			m_globalDefines = LoadFromXmlFile();
			UpdateCurrentlyUsedGlobalDefines(EditorUserBuildSettings.selectedBuildTargetGroup);
		}

		public void SaveInXmlFile()
		{
			Common.Tools.Xml.XmlWriter xw = new Common.Tools.Xml.XmlWriter();

			foreach ( GlobalDefine globalDefine in GlobalDefines )
			{
				xw.WriteStartElement( "GLOBALDEFINE" );
				xw.WriteAttribute( "name", globalDefine.sName );
				xw.WriteEndElement();
			}

			string globalDefinesFilePath = IOUtility.CombineMultiplePaths( Utils.GetBuildFolder(), "Config", "GlobalDefines.xml" );

			GlobalTools.SaveTextFile(globalDefinesFilePath, xw.GetString());
		}

		public void Save( BuildTargetGroup targetGroup )
		{
			string[] sGlobalDefinesToSave = GetEnabledGlobalDefines( m_globalDefines );
			if ( sGlobalDefinesToSave.Length > 0 )
			{
				string sDefines = string.Join (";", sGlobalDefinesToSave);
				Debug.Log( "[BUILER_DEBUG] GlobalDefine.Save " + targetGroup + " sDefines " + sDefines );
				WriteScriptingSymbols( targetGroup, sDefines);
			}
			else
			{
				PlayerSettings.SetScriptingDefineSymbolsForGroup( targetGroup, "" );
			}
			// Update currently used Global defines
			foreach (var gd in m_globalDefines)
				gd.isCurrentlyUsed = gd.bEnabled;
		}

		private static string[] ReadScriptingSymbols( BuildTargetGroup targetGroup )
		{
			string sText = PlayerSettings.GetScriptingDefineSymbolsForGroup( targetGroup );
			return sText.Split( ';' );
		}

		private static void WriteScriptingSymbols( BuildTargetGroup targetGroup, string sData )
		{
			PlayerSettings.SetScriptingDefineSymbolsForGroup( targetGroup, sData );
		}
	}
}