﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
namespace CustomDatePicker
{
    public class CalendarController : MonoBehaviour
    {
        public static readonly string[] months = new string[] { "", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" };
        public static readonly string[] days = new string[] { "D", "L", "M", "M", "J", "V", "S" };

        public DateTile currentSelectedTile => _currentSelectedTile;

        public GameObject _calendarPanel;
        public GameObject preButton;
        public GameObject nextButton;
        public TextMeshProUGUI monthAndYear;
        public GameObject gridView;
        public GameObject monthAndYearPicker;
        public DateTile _tile;
        public RectTransform layoutPanel;
        public List<DateTile> _dateItems = new List<DateTile>();
        public List<DateTile> _daysItem = new List<DateTile>();
        public TextMeshProUGUI date;
        public UnityAction<string> OnDateSelect;
        public Color onEditingDateTextColor = Color.green;
        public float dayFontSize = 65;
        public Color dayFontColor = Color.black;
        public float dateTileFontSize = 65;
        public Color dateTileColor = Color.black;
        public Color currentDateTextColor = Color.green;
        public Color currentDateBackgroundColor = Color.white;
        public Color currentMonthTextColor = Color.gray;
        public Color currentMonthBackgroundColor = Color.white;
        public Color defaultBackgroundColor = Color.grey;
        public Color clickedDateTextColor = Color.black;
        public Color clickedDateBackgroundColor = Color.gray;
        [SerializeField]
        private GameObject _timeRoot = null;
        [SerializeField]
        private Toggle _allDayToggle = null;
        [SerializeField]
        private TMP_Dropdown _hourDropdown = null;
        [SerializeField]
        private TMP_Dropdown _minuteDropdown = null;
        [SerializeField]
        private GameObject _buttonRoot = null;
        [SerializeField]
        private Button _chooseBtn = null;

        //private List<string> years = new List<string>();
        //private List<GameObject> monthPrefabs = new List<GameObject>();
        //private List<GameObject> yearPrefabs = new List<GameObject>();
        const int _totalDateNum = 42;
        private DateTime _dateTime;
        public static CalendarController calendarInstance;
        DateTime today;
        private int selectedDay, selectedMonth, selectedYear;
        private DateTile _currentSelectedTile;
        private bool _isDateTimeSet = false;
        private bool _useTime = false;
        private bool _allDay = false;
        private int _hour = 0;
        private int _minute = 0;

        public void SetDateTime(DateTime dateTime, bool useTime)
		{
            _dateTime = dateTime;
            _useTime = useTime;
            _isDateTimeSet = true;
            _timeRoot.SetActive(useTime);
            _buttonRoot.SetActive(useTime);
            if (useTime)
            {
                _hour = dateTime.Hour;
                _minute = dateTime.Minute;
                _hourDropdown.SetValueWithoutNotify(_hour);
                _minuteDropdown.SetValueWithoutNotify(_minute);
                _allDay = _hour == 0 && _minute == 0;
                _allDayToggle.SetValue(_allDay);
                OnToggleAllDayChanged(_allDay);
            }
        }

		private void Awake()
		{
            List<TMP_Dropdown.OptionData> hourOptions = new List<TMP_Dropdown.OptionData>();
            for (int i = 0; i < 24; ++i)
                hourOptions.Add(new TMP_Dropdown.OptionData(i.ToString("00")));
            _hourDropdown.options = hourOptions;

            List<TMP_Dropdown.OptionData> minuteOptions = new List<TMP_Dropdown.OptionData>();
            for (int i = 0; i < 60; ++i)
                minuteOptions.Add(new TMP_Dropdown.OptionData(i.ToString("00")));
            _minuteDropdown.options = minuteOptions;

            _hourDropdown.onValueChanged.AddListener(OnHourChanged);
            _minuteDropdown.onValueChanged.AddListener(OnMinuteChanged);
            _chooseBtn.onClick.AddListener(OnClickChoose);
            _allDayToggle.onValueChanged.AddListener(OnToggleAllDayChanged);
        }

		private void OnDestroy()
		{
            _hourDropdown.onValueChanged.RemoveListener(OnHourChanged);
            _minuteDropdown.onValueChanged.RemoveListener(OnMinuteChanged);
            _chooseBtn.onClick.RemoveListener(OnClickChoose);
            _allDayToggle.onValueChanged.RemoveListener(OnToggleAllDayChanged);
        }

		void Start()
        {
            calendarInstance = this;
            _dateItems.Clear();
            _dateItems.Add(_tile);
            for (int i = 1; i < _totalDateNum + 8; i++)
            {
                DateTile obj = Instantiate(_tile);
                obj.transform.SetParent(gridView.transform, false);
                obj.calendarController = this;
                if (i < 8)
                {
                    if (i <= days.Length)
                    {
                        obj.name = "Tile:" + days[i - 1];
                        obj.text.text = days[i - 1];
                        obj.text.fontSize = dayFontSize;
                        obj.text.color = dayFontColor;
                        obj.text.fontStyle = FontStyles.Bold;
                        _daysItem.Add(obj);
                        obj.ChangeVisility(true);
                        obj.clickable = false;
                        obj.image.color = Color.clear;
                    }
                    else
                    {
                        _daysItem.Add(obj);
                        obj.clickable = true;
                        obj.ChangeVisility(false);

                    }


                }
                else
                {
                    obj.name = "Tile:" + (i + 1 - 8).ToString();
                    _dateItems.Add(obj);
                }

            }
            if (!_isDateTimeSet)
                _dateTime = DateTime.Now;
            today = _dateTime.Date;
            if (date != null) date.text = ConvertDateToString(_useTime && !_allDay ? _dateTime : today, _useTime && !_allDay);

            CreateCalendar();
        }

        public static string ConvertDateToString(DateTime date, bool useTime)
		{
            if (useTime && (date.Hour != 0 || date.Minute != 0))
                return date.Day + " " + months[date.Month] + " " + date.Year + " " + date.Hour.ToString("00") + ":" + date.Minute.ToString("00");
            else
                return date.Day + " " + months[date.Month] + " " + date.Year;
        }

        private void OnEnable()
        {
            calendarInstance = this;
            if (date != null) date.color = onEditingDateTextColor;
        }
        private void OnDisable()
        {
            if (date != null) date.color = Color.black;
        }
        // Update is called once per frame

        void CreateCalendar()
        {
            DateTime firstDay = _dateTime.AddDays(-(_dateTime.Day - 1));
            int index = GetDays(firstDay.DayOfWeek);

            int date = 0;
            for (int i = 0; i < _totalDateNum; i++)
            {
                _dateItems[i].ChangeVisility(false);
                _dateItems[i].previousDate = false;

                if (i > index)
                {

                    DateTime thatDay = firstDay.AddDays(date);
                    if (thatDay.Month == firstDay.Month)
                    {
                        _dateItems[i].ChangeVisility(true);
                        _dateItems[i].text.text = (date + 1).ToString();
                        _dateItems[i].text.fontSize = dateTileFontSize;
                        _dateItems[i].text.color = dateTileColor;

                        if (_dateTime.Month == today.Month && date == today.Day - 1 && _dateTime.Year == today.Year)
                        {
                            _dateItems[i].text.color = currentDateTextColor;
                            _dateItems[i].text.fontStyle = FontStyles.Bold;
                            _dateItems[i].image.color = currentDateBackgroundColor;
                            _dateItems[i].previousDate = true;
                        }
                        else
                        {
                            _dateItems[i].text.fontStyle = FontStyles.Normal;
                            _dateItems[i].image.color = Color.clear;
                        }
                        if(date==selectedDay-1&&selectedMonth== _dateTime.Month&&selectedYear== _dateTime.Year)
                        {
                            _dateItems[i].text.color = clickedDateTextColor;
                            _dateItems[i].image.color = clickedDateBackgroundColor;
                        }

                        date++;
                    }
                }
                else
                {

                }
            }
            //if (this.date != null) this.date.text = months[_dateTime.Month] + " " + _dateTime.Day + "," + _dateTime.Year;
            monthAndYear.text = months[_dateTime.Month] + ", " + _dateTime.Year.ToString();
        }
        int GetDays(DayOfWeek day)
        {
            switch (day)
            {
                case DayOfWeek.Monday: return 1;
                case DayOfWeek.Tuesday: return 2;
                case DayOfWeek.Wednesday: return 3;
                case DayOfWeek.Thursday: return 4;
                case DayOfWeek.Friday: return 5;
                case DayOfWeek.Saturday: return 6;
                case DayOfWeek.Sunday: return 0;
            }

            return 0;
        }


        protected void button_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            // identify which button was clicked and perform necessary actions
        }
        public void showMonthAndYearPicker()
        {
            if (monthAndYearPicker.activeSelf)
            {
                monthAndYearPicker.SetActive(false);
                preButton.SetActive(true);
                nextButton.SetActive(true);
            }
            else
            {
                monthAndYearPicker.SetActive(true);
                preButton.SetActive(false);
                nextButton.SetActive(false);
            }
            if (layoutPanel != null)
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(layoutPanel);
            }

        }

        public void YearPrev()
        {
            _dateTime = _dateTime.AddYears(-1);
            CreateCalendar();
        }

        public void YearNext()
        {
            _dateTime = _dateTime.AddYears(1);
            CreateCalendar();
        }

        public void MonthPrev()
        {
            _dateTime = _dateTime.AddMonths(-1);
            CreateCalendar();
        }

        public void MonthNext()
        {
            _dateTime = _dateTime.AddMonths(1);
            CreateCalendar();
        }

        public void ShowCalendar(Text target)
        {
            _calendarPanel.SetActive(true);
            _calendarPanel.transform.position = Input.mousePosition - new Vector3(0, 120, 0);
        }

        public void OnDateItemClick(string day, DateTile dateTile)
        {
            string[] str = this.monthAndYear.text.Split(',');
            selectedDay = int.Parse(day);
            selectedMonth = GetMonthIndex(str[0].Replace(" ", ""));
            selectedYear = int.Parse(str[1].Replace(" ",""));
            _dateTime = new DateTime(selectedYear, selectedMonth, selectedDay);
            if (_useTime)
                UpdateDateWithHoursMinutes();
            if (date != null && !_useTime)
            {
                date.text = ConvertDateToString(_dateTime, _useTime);
                OnDateSelect?.Invoke(date.text);
            }
            if (_currentSelectedTile != null)
            {
                if (_currentSelectedTile.previousDate)
				{
                    _currentSelectedTile.image.color = currentDateBackgroundColor;
                    _currentSelectedTile.text.color = currentDateTextColor;
                }
                else
				{
                    _currentSelectedTile.image.color = Color.clear;
                    _currentSelectedTile.text.color = dayFontColor;
                }
            }

            dateTile.text.color = clickedDateTextColor;
            dateTile.image.color = clickedDateBackgroundColor;

            _currentSelectedTile = dateTile;

            if (_calendarPanel != null && !_useTime) 
                _calendarPanel.SetActive(false);
        }
        public void MonthAndYearSelect(string text)
        {
            string[] str = text.Split(',');
            int monthIndex = GetMonthIndex(str[0]);
            int yearValue = Int32.Parse(str[1]);
            _dateTime = new DateTime(yearValue, monthIndex, _dateTime.Day);
            if (_useTime && !_allDay)
                UpdateDateWithHoursMinutes();
            CreateCalendar();
        }
        public void CalendarActiveDeactiveToggle()
        {
            if (_calendarPanel.activeSelf)
            {
                _calendarPanel.SetActive(false);
            }
            else
            {
                _calendarPanel.SetActive(true);
            }
        }

        private int GetMonthIndex(string month)
		{
            for (int i = 0; i < months.Length; ++i)
			{
                if (months[i] == month)
                    return i;
			}
            return 0;
		}

        private void OnHourChanged(int hour)
        {
            _hour = hour;
            UpdateDateWithHoursMinutes();
        }

        private void OnMinuteChanged(int minute)
        {
            _minute = minute;
            UpdateDateWithHoursMinutes();
        }

        private void UpdateDateWithHoursMinutes()
		{
            if (_hour == 0 && _minute == 0)
                _allDayToggle.isOn = true;
            _dateTime = _dateTime.Date;
            _dateTime = _dateTime.AddHours(_hour);
            _dateTime = _dateTime.AddMinutes(_minute);
        }

        private void OnClickChoose()
        {
            if (date != null)
            {
                date.text = ConvertDateToString(_dateTime, _useTime && !_allDay);
                OnDateSelect?.Invoke(date.text);
            }
            if (_calendarPanel != null)
                _calendarPanel.SetActive(false);
        }

        private void OnToggleAllDayChanged(bool allDay)
		{
            _allDay = allDay;
            if (allDay)
			{
                _hourDropdown.SetValueWithoutNotify(0);
                _minuteDropdown.SetValueWithoutNotify(0);
            }
            _hourDropdown.interactable = !allDay;
            _minuteDropdown.interactable = !allDay;
        }
    }
}
