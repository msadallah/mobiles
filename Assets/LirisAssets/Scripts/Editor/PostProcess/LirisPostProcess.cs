#if UNITY_IOS
#define USE_POST_PROCESS
#endif

namespace Liris.App
{
#if USE_POST_PROCESS
	using System;
	using System.IO;
	using UnityEditor;
	using UnityEditor.Callbacks;
	using UnityEditor.iOS.Xcode;
#endif

	public class PostProcessBuild
	{
#if USE_POST_PROCESS
		[PostProcessBuild]
		public static void OnPostprocessBuild(BuildTarget buildTarget, string buildPath)
		{
			if(buildTarget == BuildTarget.iOS )
			{
				string plistPath = Path.Combine( buildPath, "Info.plist" );

				PlistDocument plist = new PlistDocument();
				plist.ReadFromString( File.ReadAllText( plistPath ) );

				PlistElementDict rootDict = plist.root;
				PlistElementDict securityDict = rootDict.CreateDict("NSAppTransportSecurity");
				PlistElementDict exceptDict = securityDict.CreateDict("NSExceptionDomains");
				PlistElementDict lirisDict = exceptDict.CreateDict("KTBS_URL");
				lirisDict.SetBoolean("NSIncludesSubdomains", true);
				lirisDict.SetBoolean("NSExceptionAllowsInsecureHTTPLoads", true);
				lirisDict.SetBoolean("NSExceptionRequiresForwardSecrecy", false);
				File.WriteAllText( plistPath, plist.WriteToString() );
			}
		}
#endif
	}
}