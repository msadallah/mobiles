using UnityEngine;
using UnityEditor;
using TMPro;


public class EmoPacker
{
	[MenuItem("Tools/Pack Emojis")]
	static public void PackEmojis()
	{
		Object o = Selection.activeObject;
		if (o is TMP_SpriteAsset)
		{
			string path = AssetDatabase.GetAssetPath(o);
			TMP_SpriteAsset spriteAsset = AssetDatabase.LoadAssetAtPath(path, typeof(TMP_SpriteAsset)) as TMP_SpriteAsset;
			//Debug.Log("spriteAsset " + spriteAsset);
			foreach (var character in spriteAsset.spriteCharacterTable)
			{
				string[] splitName = character.name.Split('-');
				if (splitName != null && splitName.Length > 0)
				{
					uint unicode = 0;
					foreach (string split in splitName)
					{
						//Debug.Log("Convert " + split);
						if (uint.TryParse(split, System.Globalization.NumberStyles.HexNumber, null, out uint code))
						{
							unicode += code;
						}
						break;
					}
					character.unicode = unicode;
				}
			}
			EditorUtility.CopySerialized(spriteAsset, o);
			AssetDatabase.SaveAssets();
		}
		else
		{
			Debug.Log("Selection is not a TMP_SpriteAsset " + o);
		}
	}
}