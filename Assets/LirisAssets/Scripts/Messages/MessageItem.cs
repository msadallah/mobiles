namespace Liris.App.Messages
{
	using AllMyScripts.Common.Tools;
	using AllMyScripts.LangManager;
	using Liris.App.Assets;
	using Liris.App.Controllers;
	using Liris.App.Media;
	using Liris.App.WebServices;
	using System.Collections;
	using System.Collections.Generic;
	using TMPro;
	using UnityEngine;
	using UnityEngine.UI;

	public class MessageItem : MonoBehaviour
	{
        public enum ItemAction
		{
            UPDATED,
            DELETED
		}

        [SerializeField]
        private TextMeshProUGUI _title = null;
        [SerializeField]
        private GameObject _pictureRoot = null;
        [SerializeField]
        private RawImage _picture = null;
        [SerializeField]
        private SpritesList _placeIconSprites = null;
        [SerializeField]
        private MsgIcon _iconsPrefab = null;
        [SerializeField]
        private Transform _iconsParent = null;
        [SerializeField]
        private GameObject _iconsRoot = null;
        [SerializeField]
        private GameObject _emoticonRoot = null;
        [SerializeField]
        private Image _emoticonImage = null;
        [SerializeField]
        private SpritesList _emoticonSprites = null;
        [SerializeField]
        private GameObject _tagsRoot = null;
        [SerializeField]
        private GameObject _emoticonAndTagsRoot = null;
        [SerializeField]
        private TextMeshProUGUI _tagsText = null;
        [SerializeField]
        private GameObject _commentRoot = null;
        [SerializeField]
        private TextMeshProUGUI _commentText = null;
        [SerializeField]
        private GameObject _loading = null;
        [SerializeField]
		private Button _editButton = null;
        [SerializeField]
        private Button _deleteButton = null;
        [SerializeField]
        private Button _photoButton = null;

        private ControllerMessage _message = null;
        private ControllerUser _user = null;
        private bool _canEdit = false;
        private System.Action<MessageItem, ItemAction> _onItemAction = null;

		public void InitController(ControllerMessage message, System.Action<MessageItem, ItemAction> onItemAction = null)
		{
            _message = message;
            _onItemAction = onItemAction;
            _user = _message.GetUser();
            if (message.tex != null)
                SetTexture(message.tex);
            else
                SetImage(message.imageId);
            SetIcons(message.icons);
            SetEmoticon(message.emoticon);
            SetTags(message.tags);
            SetComment(message.comment);
            if (_user != null)
                SetTitle(_user.username + "\n" + message.GetFormattedDate());
            else
                SetTitle(message.GetFormattedDate());
            _canEdit = _user == ApplicationManager.instance.user;
            _editButton.gameObject.SetActive(_canEdit);
            _deleteButton.gameObject.SetActive(_canEdit);
        }

        public void SetTexture(Texture2D tex)
        {
            _loading.SetActive(false);
            _picture.texture = tex;
            bool show = tex != null;
            _pictureRoot.SetActive(show);
            if (show)
            {
                AspectRatioFitter aspect = _picture.GetComponent<AspectRatioFitter>();
                if (aspect != null)
                {
                    float ratio = (float)tex.width / (float)tex.height;
                    aspect.aspectRatio = ratio;
                }
            }
        }

        public void SetImage(string imageId)
        {
            _loading.SetActive(false);
            bool show = !string.IsNullOrEmpty(imageId);
            _pictureRoot.SetActive(show);
            if (show)
            {
                ShowImage(imageId);
            }
        }

        public void SetEmoticon(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                _emoticonRoot.SetActive(name != _emoticonSprites.GetFirstName());
                _emoticonImage.sprite = _emoticonSprites.GetData(name).sprite;
            }
            else
            {
                _emoticonRoot.SetActive(false);
            }
            UpdateEmoticonAndTagsRoot();
        }

        public void SetIcons(string name)
        {
            foreach (Transform tr in _iconsParent)
                GameObject.Destroy(tr.gameObject);

            if (!string.IsNullOrEmpty(name))
            {
                _iconsRoot.SetActive(true);
                string[] split = name.Split(',');
                foreach (string iconName in split)
                {
                    SpritesList.SpriteData iconData = _placeIconSprites.GetData(iconName);
                    if (iconData != null)
                    {
                        MsgIcon icon = GameObject.Instantiate<MsgIcon>(_iconsPrefab, _iconsParent);
                        icon.SetIconSprite(iconData.sprite);
                        icon.SetIconColor(iconData.color);
                    }
                }
            }
            else
			{
                _iconsRoot.SetActive(false);
            }
        }

        public void SetComment(string comment)
        {
            bool valid = !string.IsNullOrEmpty(comment);
            _commentRoot.SetActive(valid);
            if (valid)
                _commentText.text = comment;
        }

        public void SetTitle(string title)
        {
            _title.text = title;
        }

        public void SetTags(string tags)
        {
            bool valid = !string.IsNullOrEmpty(tags);
            _tagsRoot.SetActive(valid);
            _tagsText.text = tags;
            UpdateEmoticonAndTagsRoot();
        }

        private void Awake()
        {
            _editButton.onClick.AddListener(OnEditClicked);
            _deleteButton.onClick.AddListener(OnDeleteClicked);
            _photoButton.onClick.AddListener(OnPhotoClicked);
        }

        private void OnDestroy()
        {
            _editButton.onClick.RemoveListener(OnEditClicked);
            _deleteButton.onClick.RemoveListener(OnDeleteClicked);
            _photoButton.onClick.RemoveListener(OnPhotoClicked);
            _message = null;
            _user = null;
            _onItemAction = null;
        }

        private void OnEditClicked()
		{
            MainUI.instance.ShowEditMessagesPopup(_message.annotationId, _message.tourId, _message, OnRefresh);
		}

        private void OnDeleteClicked()
        {
            MainUI.instance.ShowChoicePopup(L.Get(TextManager.MESSAGES_TITLE), L.Get(TextManager.MESSAGES_DELETE),
                L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO), (bool result) =>
                {
                    if (result)
                    {
                        ApplicationManager.instance.dataManager.DeleteMessage(_message.id, OnDelete);
                    }
                });
        }

        private void OnRefresh(ControllerMessage message)
		{
            InitController(message);
            _onItemAction?.Invoke(this, ItemAction.UPDATED);
        }

        private void OnDelete(ControllerMessage message)
		{
            _onItemAction?.Invoke(this, ItemAction.DELETED);
        }

        private void ShowImage(string imageId)
        {
            _loading.SetActive(true);

            Texture2D tex = null;

            if (SpriteLibrary.HasSprite(imageId))
            {
                tex = SpriteLibrary.GetSprite(imageId);
                ApplyTexture(tex);
                // TO EXPORT ALL IMAGES FROM CACHE
                /*
#if UNITY_EDITOR
                WebServiceMediaObjectsGetFromId ws = new WebServiceMediaObjectsGetFromId(ApplicationManager.instance.dataManager.token, imageId);
                yield return ws.Run();
                if (!ws.hasFailed)
				{
                    Dictionary<string, object> dic = ws.GetResultAsDic();
                    string name = DicTools.GetValueString(dic, "contentUrl");
                    if (!string.IsNullOrEmpty(name))
					{
                        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/images/" + name, tex.EncodeToPNG());
                    }
                }
#endif
                */
                // TO EXPORT ALL IMAGES FROM CACHE
            }
            else
            {
                ApplicationManager.instance.mediaManager.RequestMedia(imageId, OnRequestMediaResult);
            }
        }

        private void OnRequestMediaResult(MediaManager.MediaRequest request)
		{
            ApplyTexture(request.tex);
        }

        private void ApplyTexture(Texture2D tex)
        {
            if (tex != null)
            {
                _message.tex = tex;
                SetTexture(tex);
            }

            _loading.SetActive(false);
        }

        private void OnPhotoClicked()
        {
            MainUI.instance.ShowPhotoPopup(_message.tex);
        }

        private void UpdateEmoticonAndTagsRoot()
		{
            _emoticonAndTagsRoot.SetActive(_tagsRoot.activeSelf || _emoticonRoot.activeSelf);
        }
    }
}
