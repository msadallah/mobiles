using UnityEngine;

public class OfflineIds : MonoBehaviour
{
	public enum OfflineIdType
	{
		ANNOTATION,
		IMAGE,
		TOUR,
		MESSAGE
	}

	public const string OFFLINE_TAG = "_OFF_";
	private const string PP_OFFLINE_ID = "PP_OFFLINE_ID";
	private static int OFFLINE_ID = 0;
	public static string GetId(OfflineIdType type)
	{
		string id = OFFLINE_TAG + type.ToString() + "_" + OFFLINE_ID++;
		Debug.Log("[OFFLINE_ID] " + id);
		return id;
	}

	private void Awake()
	{
		OFFLINE_ID = PlayerPrefs.GetInt(PP_OFFLINE_ID, 0);
	}

	private void OnDestroy()
	{
		PlayerPrefs.SetInt(PP_OFFLINE_ID, OFFLINE_ID);
		PlayerPrefs.Save();
	}
}
