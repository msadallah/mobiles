namespace Liris.App.Network
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Networking;

    public class CheckOffline : MonoBehaviour
    {
        public delegate void OnConnectionResult(bool isOk);
        public static OnConnectionResult onConnectionCbk = null;
        public static bool lastResult = false;
        public static bool firstResult = false;

        private bool _doCount;
        private float _timeCount, _count;

        IEnumerator Start()
        {
            _timeCount = 3f;
            yield return new WaitForSeconds(1f);
            StartCoroutine(CheckRoutine());
            StartCoroutine(VerifyTimeRoutine(15f));
        }

        public void Update()
        {
            if (_doCount)
            {
                if (_count <= _timeCount)
                {
                    _count += Time.deltaTime;
                }
                else
                {
                    _doCount = false;
                    _count = 0;
                    StopAllCoroutines();
                    StartCoroutine(CheckRoutine());
                    StartCoroutine(VerifyTimeRoutine(15f));
                }
            }
        }

        IEnumerator CheckRoutine()
        {
            UnityWebRequest request = new UnityWebRequest("https://www.google.com/");
            request.downloadHandler = new DownloadHandlerBuffer();
            yield return request.SendWebRequest();
            _doCount = true;
            lastResult = string.IsNullOrEmpty(request.error);
            firstResult = true;
            onConnectionCbk?.Invoke(lastResult);
            StopAllCoroutines();
        }

        IEnumerator VerifyTimeRoutine(float duration)
        {
            float time = Time.time;
            while (Time.time - time < duration)
			{
                if (_doCount)
                    yield break;
                else
                    yield return null;
			}
            if (!_doCount)
			{
                lastResult = false;
                firstResult = true;
                onConnectionCbk?.Invoke(lastResult);
                StopAllCoroutines();
                _doCount = true;
            }
        }
    }
}
