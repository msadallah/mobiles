using AllMyScripts.Common.Tools;
using Liris.App.Controllers;
using System.Collections.Generic;
using UnityEngine;

public class CacheData : MonoBehaviour
{
    private const string PP_VISIBILITY = "PP_VISIBILITY_";
	private const string PP_CURRENT_USER = "PP_CURRENT_USER";
    private const string PP_USERS = "PP_USERS";
    private const string PP_ANNOTATIONS = "PP_ANNOTATIONS";
	private const string PP_TOURS = "PP_TOURS";
	private const string PP_MESSAGES = "PP_MESSAGES";

	private bool _needToSave = false;

	#region Visibility

	public void SaveVisibility(string userId, int visibility)
	{
		PlayerPrefs.SetInt(PP_VISIBILITY + userId, visibility);
		_needToSave = true;
	}

	public int GetVisibility(string userId, int defaultValue)
	{
		return PlayerPrefs.GetInt(PP_VISIBILITY + userId, defaultValue);
	}

	#endregion // Visibility

	#region Users

	public void SaveCurrentUserId(string id)
	{
		PlayerPrefs.SetString(PP_CURRENT_USER, id);
		_needToSave = true;
	}

	public string GetCurrentUserId()
	{
		if (PlayerPrefs.HasKey(PP_CURRENT_USER))
			return PlayerPrefs.GetString(PP_CURRENT_USER);
		else
			return null;
	}

    public void SaveAllUsers(List<ControllerUser> listUsers)
    {
        if (listUsers != null)
        {
            List<object> list = new List<object>();
            foreach (ControllerUser user in listUsers)
            {
                list.Add(user.GetDataAsDic());
            }
            string json = JSON.Serialize(list);
            Debug.Log("SaveAllUsers " + json);
            PlayerPrefs.SetString(PP_USERS, json);
            _needToSave = true;
        }
    }

    public List<ControllerUser> GetAllUsers()
    {
        if (PlayerPrefs.HasKey(PP_USERS))
        {
            List<ControllerUser> listUsers = new List<ControllerUser>();
            string json = PlayerPrefs.GetString(PP_USERS);
            if (!string.IsNullOrEmpty(json))
            {
                List<object> list = JSON.Deserialize(json) as List<object>;
                foreach (object o in list)
                {
                    listUsers.Add(new ControllerUser(o as Dictionary<string, object>));
                }
                return listUsers;
            }
        }
        return null;
    }

    #endregion // Users

    #region Annotations

    public void SaveAllAnotations(List<ControllerAnnotation> listAnnotations)
	{
		if (listAnnotations != null)
		{
            List<object> list = new List<object>();
            foreach (ControllerAnnotation annotation in listAnnotations)
            {
                list.Add(annotation.GetDataAsDic());
            }
            string json = JSON.Serialize(list);
            Debug.Log("SaveAllAnotations " + json);
            PlayerPrefs.SetString(PP_ANNOTATIONS, json);
            _needToSave = true;
        }
	}

	public List<ControllerAnnotation> GetAllAnnotations()
	{
		if (PlayerPrefs.HasKey(PP_ANNOTATIONS))
		{
			List<ControllerAnnotation> listAnnotations = new List<ControllerAnnotation>();
			string json = PlayerPrefs.GetString(PP_ANNOTATIONS);
			if (!string.IsNullOrEmpty(json))
			{
				List<object> list = JSON.Deserialize(json) as List<object>;
				foreach (object o in list)
				{
					listAnnotations.Add(new ControllerAnnotation(o as Dictionary<string, object>));
				}
				return listAnnotations;
			}
		}
		return null;
	}

	#endregion // Annotations

	#region Tours

	public void SaveAllTours(List<ControllerTour> listTours)
	{
		if (listTours != null)
		{
            List<object> list = new List<object>();
            foreach (ControllerTour tour in listTours)
            {
                list.Add(tour.GetDataAsDic());
            }
            string json = JSON.Serialize(list);
			Debug.Log("SaveAllTours " + json);
            PlayerPrefs.SetString(PP_TOURS, json);
            _needToSave = true;
        }
	}

	public List<ControllerTour> GetAllTours()
	{
		if (PlayerPrefs.HasKey(PP_TOURS))
		{
			List<ControllerTour> listTours = new List<ControllerTour>();
			string json = PlayerPrefs.GetString(PP_TOURS);
			if (!string.IsNullOrEmpty(json))
			{
				List<object> list = JSON.Deserialize(json) as List<object>;
				foreach (object o in list)
				{
					listTours.Add(new ControllerTour(o as Dictionary<string, object>));
				}
				return listTours;
			}
		}
		return null;
	}

	#endregion // Tours

	#region Messages

	public void SaveAllMessages(List<ControllerMessage> listMessages)
	{
		if (listMessages != null)
		{
			List<object> list = new List<object>();
			foreach (ControllerMessage message in listMessages)
			{
				list.Add(message.GetDataAsDic());
			}
			string json = JSON.Serialize(list);
			Debug.Log("SaveAllMessages " + json);
			PlayerPrefs.SetString(PP_MESSAGES, json);
			_needToSave = true;
		}
	}

	public List<ControllerMessage> GetAllMessages()
	{
		if (PlayerPrefs.HasKey(PP_MESSAGES))
		{
			List<ControllerMessage> listMessages = new List<ControllerMessage>();
			string json = PlayerPrefs.GetString(PP_MESSAGES);
			if (!string.IsNullOrEmpty(json))
			{
				List<object> list = JSON.Deserialize(json) as List<object>;
				foreach (object o in list)
				{
					listMessages.Add(new ControllerMessage(o as Dictionary<string, object>));
				}
				return listMessages;
			}
		}
		return null;
	}

	#endregion // Messages

	private void LateUpdate()
	{
		CheckSave();
	}

	private void OnApplicationQuit()
	{
		CheckSave();
	}

	private void CheckSave()
	{
		if (_needToSave)
		{
			PlayerPrefs.Save();
			_needToSave = false;
		}
	}
}
