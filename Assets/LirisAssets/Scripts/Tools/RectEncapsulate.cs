namespace Liris.App.Tools
{
    using UnityEngine;
    public static class RectEncapsulate
    {
        public static Rect Encapsulate(this Rect rect, float x, float y)
        {
            var xmin = Mathf.Min(rect.min.x, rect.max.x, x);
            var xmax = Mathf.Max(rect.min.x, rect.max.x, x);
            var ymin = Mathf.Min(rect.min.y, rect.max.y, y);
            var ymax = Mathf.Max(rect.min.y, rect.max.y, y);
            return Rect.MinMaxRect(xmin, ymin, xmax, ymax);
        }

        public static Rect Encapsulate(this Rect rect, Vector2 point)
        {
            var xmin = Mathf.Min(rect.min.x, rect.max.x, point.x);
            var xmax = Mathf.Max(rect.min.x, rect.max.x, point.x);
            var ymin = Mathf.Min(rect.min.y, rect.max.y, point.y);
            var ymax = Mathf.Max(rect.min.y, rect.max.y, point.y);
            return Rect.MinMaxRect(xmin, ymin, xmax, ymax);
        }

        public static Rect Encapsulate(this Rect rect, Rect other)
        {
            var xmin = Mathf.Min(rect.min.x, rect.max.x, other.min.x, other.max.x);
            var xmax = Mathf.Max(rect.min.x, rect.max.x, other.min.x, other.max.x);
            var ymin = Mathf.Min(rect.min.y, rect.max.y, other.min.y, other.max.y);
            var ymax = Mathf.Max(rect.min.y, rect.max.y, other.min.y, other.max.y);
            return Rect.MinMaxRect(xmin, ymin, xmax, ymax);
        }
    }
}