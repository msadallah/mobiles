namespace Liris.App.Options
{
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
	using System.Collections.Generic;
	using UnityEngine;

	public class OptionsManager : MonoBehaviour
    {
		private const string PP_USER_OPTIONS = "USER_OPTIONS_";

		public bool savePhotoInGallery => _savePhotoInGallery;

		private bool _savePhotoInGallery = true;
		private string _user = null;

		public void SetSavePhotoInGallery(bool save)
		{
			if (_user != null)
			{
				_savePhotoInGallery = save;
				Dictionary<string, object> dic = new Dictionary<string, object>();
				dic["_savePhotoInGallery"] = _savePhotoInGallery;
				string json = JSON.Serialize(dic);
				PlayerPrefs.SetString(PP_USER_OPTIONS + _user, json);
				PlayerPrefs.Save();
			}
		}

		private void Start()
		{
			ApplicationManager.onLoginEvent += OnUserLogin;
			ApplicationManager.onLogoutEvent += OnUserLogout;
		}

		private void OnDestroy()
		{
			ApplicationManager.onLoginEvent -= OnUserLogin;
			ApplicationManager.onLogoutEvent -= OnUserLogout;
			_user = null;
		}

		private void OnUserLogin(ControllerUser user)
		{
			_user = user.id;
			string json = PlayerPrefs.GetString(PP_USER_OPTIONS + _user);
			if (!string.IsNullOrEmpty(json))
			{
				Dictionary<string, object> dic = JSON.Deserialize(json) as Dictionary<string, object>;
				_savePhotoInGallery = DicTools.GetValueBool(dic, "_savePhotoInGallery");
			}
		}

		private void OnUserLogout(ControllerUser user)
		{
			_user = null;
		}
	}
}