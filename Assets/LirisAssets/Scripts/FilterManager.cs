namespace Liris.App.Filter
{
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
    using System.Collections.Generic;
    using UnityEngine;

    public class FilterManager : MonoBehaviour
    {
        public const string PP_FILTERS_KEY = "FILTERS_KEY_";
        public const string NONE_TEXT = "none";

        public string beginDate => _beginDate;
        public string endDate => _endDate;
        public List<string> institutionSelection => _institutionSelection;
        public List<string> nationalitySelection => _nationalitySelection;
        public List<string> diplomaSelection => _diplomaSelection;
        public List<string> placeTypeSelection => _placeTypeSelection;
        public List<string> placeIconSelection => _placeIconSelection;
        public List<string> emoticonSelection => _emoticonSelection;
        public List<string> hashtagSelection => _hashtagSelection;


        private string _beginDate = null;
        private string _endDate = null;
        private List<string> _institutionSelection = null;
        private List<string> _nationalitySelection = null;
        private List<string> _diplomaSelection = null;
        private List<string> _placeTypeSelection = null;
        private List<string> _placeIconSelection = null;
        private List<string> _emoticonSelection = null;
        private List<string> _hashtagSelection = null;

        public void Clear()
        {
            _beginDate = null;
            _endDate = null;
            _institutionSelection = null;
            _nationalitySelection = null;
            _diplomaSelection = null;
            _placeTypeSelection = null;
            _placeIconSelection = null;
            _emoticonSelection = null;
            _hashtagSelection = null;
        }

        public int GetFilterCount()
		{
            int count = 0;
            if (_beginDate != null)
                count++;
            if (_endDate != null)
                count++;
            if (_institutionSelection != null)
                count++;
            if (_nationalitySelection != null)
                count++;
            if (_diplomaSelection != null)
                count++;
            if (_placeTypeSelection != null)
                count++;
            if (_placeIconSelection != null)
                count++;
            if (_emoticonSelection != null)
                count++;
            if (_hashtagSelection != null)
                count++;
            return count;
        }

        public void SetBeginDate(string date)
        {
            _beginDate = date;
        }

        public void SetEndDate(string date)
        {
            _endDate = date;
        }

        public void AddInstitution(string institution)
        {
            if (_institutionSelection == null)
                _institutionSelection = new List<string>();
            _institutionSelection.Add(institution);
        }

        public void AddNationality(string nationality)
        {
            if (_nationalitySelection == null)
                _nationalitySelection = new List<string>();
            _nationalitySelection.Add(nationality);
        }

        public void AddDiploma(string diploma)
        {
            if (_diplomaSelection == null)
                _diplomaSelection = new List<string>();
            _diplomaSelection.Add(diploma);
        }

        public void AddPlaceType(string placeType)
        {
            if (_placeTypeSelection == null)
                _placeTypeSelection = new List<string>();
            _placeTypeSelection.Add(placeType);
        }

        public void AddPlaceIcon(string placeIcon)
        {
            if (_placeIconSelection == null)
                _placeIconSelection = new List<string>();
            _placeIconSelection.Add(placeIcon);
        }

        public void AddEmoticon(string emoticon)
        {
            if (_emoticonSelection == null)
                _emoticonSelection = new List<string>();
            _emoticonSelection.Add(emoticon);
        }

        public void AddHashtag(string tag)
        {
            if (_hashtagSelection == null)
                _hashtagSelection = new List<string>();
            _hashtagSelection.Add(tag);
        }

        public bool IsUserVisible(ControllerUser user)
        {
            if (user == null)
                return false;
            if (_institutionSelection != null && !_institutionSelection.Contains(user.institution))
                return false;
            if (_nationalitySelection != null && !_nationalitySelection.Contains(user.nationality))
                return false;
            if (_diplomaSelection != null && !_diplomaSelection.Contains(user.diploma))
                return false;
            return true;
        }

        public bool IsAnnotationVisible(ControllerAnnotation annotation, ControllerUser user = null)
        {
            if (annotation == null)
                return false;
            if (user == null)
                user = annotation.GetUser();
            if (!IsUserVisible(user))
                return false;
            if (!CheckListWithNone(_placeTypeSelection, annotation.placeType))
                return false;
            if (!CheckListWithNone(_placeIconSelection, annotation.icons))
                return false;
            if (!CheckListWithNone(_emoticonSelection, annotation.emoticon))
                return false;
            if (!CheckTags(annotation.tags))
                return false;
            return CheckDate(ControllerBase.ConvertInDateTime(annotation.dateDisplay));
        }

        public bool CheckListWithNone(List<string> list, string value)
		{
            if (list == null)
                return true;
            if (list.Contains(value))
                return true;
            if (list.Contains(NONE_TEXT))
                return string.IsNullOrEmpty(value);
            if (!string.IsNullOrEmpty(value) && value.Contains(","))
			{
                string[] split = value.Split(",");
                foreach (string elem in split)
				{
                    if (CheckListWithNone(list, elem))
                        return true;
				}
			}
            return false;
		}

        private bool CheckTags(string tags)
		{
            if (_hashtagSelection == null)
                return true;
            if (!string.IsNullOrEmpty(tags))
            {
                string[] tagSplit = tags.Split(" ");
                for (int tag = 0; tag < tagSplit.Length; ++tag)
                {
                    string tagName = tagSplit[tag];
                    if (tagName.StartsWith("#"))
                    {
                        if (_hashtagSelection.Contains(tagName))
                            return true;
                    }
                }
            }
            else
			{
                return _hashtagSelection.Contains(NONE_TEXT);
            }
            return false;
        }


        public bool IsTourVisible(ControllerTour tour, ControllerUser user = null)
        {
            if (tour == null)
                return false;
            if (user == null)
                user = tour.GetUser();
            if (!IsUserVisible(user))
                return false;
            return CheckDate(ControllerBase.ConvertInDateTime(tour.dateDisplay));
        }

        public void SavePreferences(ControllerUser user)
        {
            if (user != null)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["beginDate"] = _beginDate;
                dic["endDate"] = _endDate;
                dic["institutions"] = _institutionSelection;
                dic["nationalities"] = _nationalitySelection;
                dic["diplomas"] = _diplomaSelection;
                dic["placeTypes"] = _placeTypeSelection;
                dic["placeIcons"] = _placeIconSelection;
                dic["emoticons"] = _emoticonSelection;
                dic["hashtags"] = _hashtagSelection;
                string json = JSON.Serialize(dic);
                Debug.Log("[FILTER] Save " + json);
                PlayerPrefs.SetString(PP_FILTERS_KEY + user.id, json);
                PlayerPrefs.Save();
            }
        }

        public void LoadPreferences(ControllerUser user)
        {
            if (user != null)
            {
                string json = PlayerPrefs.GetString(PP_FILTERS_KEY + user.id);
                Debug.Log("[FILTER] Load " + json);
                Dictionary<string, object> dic = JSON.Deserialize(json) as Dictionary<string, object>;
                _beginDate = DicTools.GetValueString(dic, "beginDate");
                _endDate = DicTools.GetValueString(dic, "endDate");
                string[] institutionArray = DicTools.GetValueListAsStringArray(dic, "institutions");
                _institutionSelection = institutionArray != null ? new List<string>(institutionArray) : null;
                string[] nationalityArray = DicTools.GetValueListAsStringArray(dic, "nationalities");
                _nationalitySelection = nationalityArray != null ? new List<string>(nationalityArray) : null;
                string[] diplomaArray = DicTools.GetValueListAsStringArray(dic, "diplomas");
                _diplomaSelection = diplomaArray != null ? new List<string>(diplomaArray) : null;
                string[] placeTypeArray = DicTools.GetValueListAsStringArray(dic, "placeTypes");
                _placeTypeSelection = placeTypeArray != null ? new List<string>(placeTypeArray) : null;
                string[] placeIconArray = DicTools.GetValueListAsStringArray(dic, "placeIcons");
                _placeIconSelection = placeIconArray != null ? new List<string>(placeIconArray) : null;
                string[] emoticonArray = DicTools.GetValueListAsStringArray(dic, "emoticons");
                _emoticonSelection = emoticonArray != null ? new List<string>(emoticonArray) : null;
                string[] hashtagArray = DicTools.GetValueListAsStringArray(dic, "hashtags");
                _hashtagSelection = hashtagArray != null ? new List<string>(hashtagArray) : null;
            }
        }

        private bool CheckDate(System.DateTime date)
        {
            if (!string.IsNullOrEmpty(_beginDate))
            {
                System.DateTime beginDate = ControllerBase.ConvertInDateTime(_beginDate);
                if (date < beginDate)
                    return false;
            }
            if (!string.IsNullOrEmpty(_endDate))
            {
                System.DateTime endDate = ControllerBase.ConvertInDateTime(_endDate);
                if (date > endDate.AddSeconds(1))
                    return false;
            }
            return true;
        }

    }
}
