//#define DEBUG_FAVORITES

namespace Liris.App.Favorites
{
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
	using Liris.App.Network;
	using System.Collections.Generic;
	using UnityEngine;

    public class FavoritesManager : MonoBehaviour
    {
		private const string PP_USER_FAVORITES = "USER_FAVORITES_";

		public int[] tourArray => _tourFavorites != null ? _tourFavorites.ToArray() : null;
		public int[] annotationArray => _annotationFavorites != null ? _annotationFavorites.ToArray() : null;

		private List<int> _annotationFavorites = null;
        private List<int> _tourFavorites = null;
		private bool _needToUpdateFavorites = false;
		private float _elapsedTime = 0f;
		private string _user = null;

		public bool IsAnnotationFavorite(string id)
		{
			int num = ConvertTools.ToInt32(id, -1);
			if (_annotationFavorites != null)
				return _annotationFavorites.Contains(num);
			return false;
		}

		public bool IsTourFavorite(string id)
		{
			int num = ConvertTools.ToInt32(id, -1);
			if (_tourFavorites != null)
				return _tourFavorites.Contains(num);
			return false;
		}

		public void InitFromController(ControllerFavorites favorites)
		{
			LogFavorites("InitFromController _needToUpdateFavorites " + _needToUpdateFavorites);
			if (_needToUpdateFavorites)
				return;

			int[] annotationArray = favorites.annotationArray;
			if (annotationArray != null)
			{
				foreach (int annotationId in annotationArray)
				{
					AddAnnotationFavorite(annotationId.ToString());
				}
			}
			int[] tourArray = favorites.tourArray;
			if (tourArray != null)
			{
				foreach (int tourId in tourArray)
				{
					AddTourFavorite(tourId.ToString());
				}
			}

			LogFavorites("_annotationFavorites " + _annotationFavorites.Count);
			LogFavorites("_tourFavorites " + _tourFavorites.Count);
		}

		private void Awake()
		{
			_annotationFavorites = new List<int>();
			_tourFavorites = new List<int>();
			ApplicationManager.onLoginEvent += OnUserLogin;
			ApplicationManager.onLogoutEvent += OnUserLogout;
		}

		private void OnDestroy()
		{
			ApplicationManager.onLoginEvent -= OnUserLogin;
			ApplicationManager.onLogoutEvent -= OnUserLogout;
			_annotationFavorites = null;
			_tourFavorites = null;
		}

		private void Update()
		{
			_elapsedTime += Time.deltaTime;
			if (_needToUpdateFavorites && _elapsedTime > 10f && _user != null && CheckOffline.lastResult)
			{
				LogFavorites("_needToUpdateFavorites");
				string annotations = JSON.Serialize(_annotationFavorites);
				string tours = JSON.Serialize(_tourFavorites);
				ApplicationManager.instance.dataManager.UpdateFavorites(_user, annotations, tours);
				_needToUpdateFavorites = false;
				_elapsedTime = 0f;
			}
		}

		public void AddAnnotationFavorite(string id)
		{
			int num = ConvertTools.ToInt32(id, -1);
			if (num >= 0 && !_annotationFavorites.Contains(num))
			{
				_annotationFavorites.Add(num);
				_needToUpdateFavorites = true;
			}
		}

		public void RemoveAnnotationFavorite(string id)
		{
			int num = ConvertTools.ToInt32(id, -1);
			if (num >= 0 && _annotationFavorites.Contains(num))
			{
				_annotationFavorites.Remove(num);
				_needToUpdateFavorites = true;
			}
		}

		public void AddTourFavorite(string id)
		{
			int num = ConvertTools.ToInt32(id, -1);
			if (num >= 0 && !_tourFavorites.Contains(num))
			{
				_tourFavorites.Add(num);
				_needToUpdateFavorites = true;
			}
		}

		public void RemoveTourFavorite(string id)
		{
			int num = ConvertTools.ToInt32(id, -1);
			if (num >= 0 && _tourFavorites.Contains(num))
			{
				_tourFavorites.Remove(num);
				_needToUpdateFavorites = true;
			}
		}

		private void OnUserLogin(ControllerUser user)
		{
			_user = user.id;
			LogFavorites("OnUserLogin user " + _user);
			string json = PlayerPrefs.GetString(PP_USER_FAVORITES + _user);
			if (!string.IsNullOrEmpty(json))
			{
				LogFavorites("GetString json " + json);
				Dictionary<string, object> dic = JSON.Deserialize(json) as Dictionary<string, object>;
				_annotationFavorites = new List<int>(DicTools.GetValueListAsIntArray(dic, "annotations"));
				_tourFavorites = new List<int>(DicTools.GetValueListAsIntArray(dic, "tours"));
				LogFavorites("_annotationFavorites " + _annotationFavorites.Count);
				LogFavorites("_tourFavorites " + _tourFavorites.Count);
				_needToUpdateFavorites = true;
			}
		}

		private void OnUserLogout(ControllerUser user)
		{
			Debug.Log("[FAVORITES] OnUserLogout _needToUpdateFavorites " + _needToUpdateFavorites);
			if (_user != null)
			{
				if (_needToUpdateFavorites)
				{
					Dictionary<string, object> dic = new Dictionary<string, object>();
					dic["annotations"] = _annotationFavorites;
					dic["tours"] = _tourFavorites;
					string json = JSON.Serialize(dic);
					LogFavorites("SetString for user " + _user + " json " + json);
					PlayerPrefs.SetString(PP_USER_FAVORITES + _user, json);
					_needToUpdateFavorites = false;
				}
				else
				{
					PlayerPrefs.SetString(PP_USER_FAVORITES + _user, string.Empty);
				}
				_user = null;
			}
		}

		[System.Diagnostics.Conditional("DEBUG_FAVORITES")]
		private void LogFavorites(string log)
		{
			Debug.Log("[FAVORITES] " + log);
		}
	}
}