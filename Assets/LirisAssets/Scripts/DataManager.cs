namespace Liris.App.Data
{
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
	using Liris.App.Popups;
	using Liris.App.WebServices;
    using Liris.App.Network;
	using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
	using AllMyScripts.LangManager;
    using Liris.App.Trace;
	using Liris.App.Emojis;

	public class DataManager : MonoBehaviour
    {
        public string token => _token;
        public bool fetchedUsers => _fetchedUsers;
        public List<ControllerUser> users => _users;
        public bool fetchedAnnotations => _fetchedAnnotations;
        public List<ControllerAnnotation> annotations => _annotations;
        public bool fetchedTours => _fetchedTours;
        public List<ControllerTour> tours => _tours;
        public bool fetchedFavorites => _fetchedFavorites;
        public List<ControllerFavorites> favorites => _favorites;
        public List<ControllerMessage> messages => _messages;
        public List<ControllerNotification> notifications => _notifications;
        public List<ControllerGroup> groups => _groups;
        public List<string> userNamesList => _userNamesList;
        public List<string> userMailsList => _userMailsList;
        public bool lastResultFailed => _lastResultFailed;
        public bool fetchedConfigs => _fetchedConfigs;
        public bool fetchedEmojis => _fetchedEmojis;
        public bool fetchedMessages => _fetchedMessages;
        public bool fetchedNotifications => _fetchedNotifications;
        public bool fetchedGroups => _fetchedGroups;


        private string _token = null;
        private List<ControllerUser> _users = null;
        private bool _fetchedUsers = false;
        private List<ControllerAnnotation> _annotations = null;
        private bool _fetchedAnnotations = false;
        private List<ControllerTour> _tours = null;
        private bool _fetchedTours = false;
        private List<ControllerFavorites> _favorites = null;
        private bool _fetchedFavorites = false;
        private List<ControllerEmojis> _emojis = null;
        private bool _fetchedEmojis = false;
        private List<ControllerMessage> _messages = null;
        private bool _fetchedMessages = false;
        private List<ControllerNotification> _notifications = null;
        private bool _fetchedNotifications = false;
        private List<ControllerGroup> _groups = null;
        private bool _fetchedGroups = false;
        private List<string> _userNamesList = null;
        private List<string> _userMailsList = null;
        private bool _lastResultFailed = false;
        private Dictionary<string, string> _configsDic = null;
        private bool _fetchedConfigs = false;

        #region Login

        public Coroutine Login(string mail, string password, System.Action<bool> result = null)
        {
            return StartCoroutine(LoginEnum(mail, password, result));
        }

        private IEnumerator LoginEnum(string mail, string password, System.Action<bool> result)
        {
            Debug.Log("LoginEnum firstResult " + CheckOffline.firstResult);

            while (!CheckOffline.firstResult)
            {
                yield return null;
            }

            Debug.Log("LoginEnum firstResult OK lastResult " + CheckOffline.lastResult);

            if (!CheckOffline.lastResult)
            {
                result?.Invoke(false);
                yield break;
            }

            WebServiceLoginCheck query = new WebServiceLoginCheck(mail, password);
            yield return query.Run();

            _token = null;
            if (!query.hasFailed)
            {
                Dictionary<string, object> dic = query.GetResultAsDic();
                _token = DicTools.GetValueString(dic, "token");
            }

            result?.Invoke(_token != null);
        }

        #endregion

        #region Users

        public void SetAllUsers(List<ControllerUser> list)
        {
            _users = list;
        }

        public Coroutine GetAllUsers(System.Action queryDoneCbk = null)
        {
            _fetchedUsers = false;
            _users = null;
            return StartCoroutine(GetAllUsersEnum(queryDoneCbk));
        }

        private IEnumerator GetAllUsersEnum(System.Action queryDoneCbk = null)
        {
            WebServiceUsersGetAll query = new WebServiceUsersGetAll(_token);
            yield return query.Run();

            if (!query.hasFailed)
            {
                _users = new List<ControllerUser>();

                int nextPage;

                do
                {
                    foreach (object o in query.GetResultAsList())
                    {
                        CreateUser(o as Dictionary<string, object>);
                    }

                    nextPage = query.GetNextPage();
                    if (nextPage > 0)
                    {
                        yield return query.Run(nextPage);
                        if (query.hasFailed)
                            nextPage = 0;
                    }
                }
                while (nextPage > 0);
            }
            else
            {
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.USERS_TITLE), L.Get(TextManager.USERS_LIST_ERROR), FeedbackPopup.IconType.Alert);
            }

            _fetchedUsers = true;
            queryDoneCbk?.Invoke();
        }

        public Coroutine GetAllUserNamesAndMails()
        {
            return StartCoroutine(GetAllUserNamesAndMailsEnum());
        }

        private IEnumerator GetAllUserNamesAndMailsEnum()
        {
            WebServiceUsersGetAllNamesAndMails query = new WebServiceUsersGetAllNamesAndMails();
            yield return query.Run();

            _userNamesList = new List<string>();
            _userMailsList = new List<string>();

            if (!query.hasFailed)
            {
                int nextPage;

                do
                {
                    foreach (object o in query.GetResultAsList())
                    {
                        Dictionary<string, object> dic = o as Dictionary<string, object>;
                        _userNamesList.Add(DicTools.GetValueString(dic, "username"));
                        _userMailsList.Add(DicTools.GetValueString(dic, "mail"));
                    }

                    nextPage = query.GetNextPage();
                    if (nextPage > 0)
                    {
                        yield return query.Run(nextPage);
                        if (query.hasFailed)
                            nextPage = 0;
                    }
                }
                while (nextPage > 0);
            }
            else
            {
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.USERS_TITLE), L.Get(TextManager.USERS_LIST_ERROR), FeedbackPopup.IconType.Alert);
            }
        }

        public ControllerUser CreateUser(Dictionary<string, object> dic)
        {
            if (_users == null)
                _users = new List<ControllerUser>();

            ControllerUser user = new ControllerUser(dic);
            _users.Add(user);

            return user;
        }

        public ControllerUser GetUserFromId(string id)
        {
            if (!string.IsNullOrEmpty(id) && _users != null)
            {
                foreach (ControllerUser user in _users)
                {
                    if (user.id == id)
                        return user;
                }
            }
            return null;
        }

        public Coroutine UpdateUserVersion(string userId, string version)
		{
            return StartCoroutine(UpdateUserVersionEnum(userId, version));
		}

        public Coroutine UpdateUserDevices(string userId, string devices)
        {
            return StartCoroutine(UpdateUserDevicesEnum(userId, devices));
        }

        public Coroutine UpdateUserGroups(string userId, string groups)
        {
            return StartCoroutine(UpdateUserGroupsEnum(userId, groups));
        }

        private IEnumerator UpdateUserVersionEnum(string userId, string version)
        {
            WebServiceUsersUpdateVersion query = new WebServiceUsersUpdateVersion(_token, userId, version);
            yield return query.Run();
            _lastResultFailed = query.hasFailed;
        }

        private IEnumerator UpdateUserDevicesEnum(string userId, string devices)
        {
            WebServiceUsersUpdateDevices query = new WebServiceUsersUpdateDevices(_token, userId, devices);
            yield return query.Run();
            _lastResultFailed = query.hasFailed;
        }

        private IEnumerator UpdateUserGroupsEnum(string userId, string groups)
        {
            WebServiceUsersUpdateGroups query = new WebServiceUsersUpdateGroups(_token, userId, groups);
            yield return query.Run();
            _lastResultFailed = query.hasFailed;
        }

        #endregion

        #region Annotations

        public void SetAllAnnotations(List<ControllerAnnotation> list)
        {
            _annotations = list;
        }

        public ControllerAnnotation CreateAnnotation(Dictionary<string, object> dic)
        {
            if (_annotations == null)
                _annotations = new List<ControllerAnnotation>();

            ControllerAnnotation annotation = new ControllerAnnotation(dic);
            _annotations.Add(annotation);

            return annotation;
        }

        public string RemoveAnnotation(ControllerAnnotation annotation)
        {
            if (_annotations != null)
            {
                if (_annotations.Contains(annotation))
                {
                    string id = annotation.id;
                    _annotations.Remove(annotation);
                    return id;
                }
            }
            return null;
        }

        public IEnumerator SendAnnotationEnum(ControllerAnnotation annotation)
		{
            _lastResultFailed = false;
            string dateDisplay = ControllerBase.ConvertDateTimeToSend(ControllerBase.ConvertInDateTime(annotation.dateDisplay));
            WebServiceAnnotationsCreate query = new WebServiceAnnotationsCreate(token, annotation.comment, annotation.imageId, annotation.placeType,
                    annotation.icons, annotation.emoticon, annotation.tags, annotation.coords, annotation.share,
                    dateDisplay, annotation.timing, annotation.userId, annotation.tourId);
            yield return query.Run();
            if (!query.hasFailed)
            {
                Dictionary<string, object> dic = query.GetResultAsDic();
                string id = DicTools.GetValueString(dic, "id");
                if (!string.IsNullOrEmpty(id))
                {
                    annotation.id = id;
                    CreateAnnotation(dic);
                }
            }
            else
            {
                _lastResultFailed = true;
            }
        }

        public IEnumerator UpdateAnnotationEnum(ControllerAnnotation annotation)
        {
            _lastResultFailed = false;
            string dateDisplay = ControllerBase.ConvertDateTimeToSend(ControllerBase.ConvertInDateTime(annotation.dateDisplay));
            WebServiceAnnotationsUpdateContent query = new WebServiceAnnotationsUpdateContent(token, annotation.id, annotation.comment, annotation.imageId, 
                annotation.placeType, annotation.icons, annotation.emoticon, annotation.tags, annotation.share, dateDisplay, annotation.timing);
            yield return query.Run();
            if (!query.hasFailed)
            {
            }
            else
            {
                _lastResultFailed = true;
            }
        }

        public Coroutine GetAllAnnotations()
        {
            _fetchedAnnotations = false;
            _annotations = null;
            return StartCoroutine(GetAllAnnotationsEnum());
        }

        private IEnumerator GetAllAnnotationsEnum()
        {
            _lastResultFailed = false;
            WebServiceAnnotationsGetAll query = new WebServiceAnnotationsGetAll(_token);
            yield return query.Run();
            if (!query.hasFailed)
            {
                _annotations = new List<ControllerAnnotation>();

                int nextPage;

                do
                {
                    List<object> list = query.GetResultAsList();
                    foreach (object elem in list)
                    {
                        CreateAnnotation(elem as Dictionary<string, object>);
                    }

                    nextPage = query.GetNextPage();
                    if (nextPage > 0)
                    {
                        yield return query.Run(nextPage);
                        if (query.hasFailed)
                            nextPage = 0;
                    }
                }
                while (nextPage > 0);
            }
            else
            {
                _lastResultFailed = true;
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.ANNOTATIONS_TITLE), L.Get(TextManager.ANNOTATIONS_LIST_ERROR), FeedbackPopup.IconType.Alert);
            }

            _fetchedAnnotations = true;
        }

        public ControllerAnnotation GetAnnotationFromId(string id)
        {
            if (!string.IsNullOrEmpty(id) && _annotations != null)
            {
                foreach (ControllerAnnotation annotation in _annotations)
                {
                    if (annotation.id == id)
                        return annotation;
                }
            }
            return null;
        }

        public List<ControllerAnnotation> GetAnnotationsFromTourId(string tourId)
        {
            if (!string.IsNullOrEmpty(tourId) && _annotations != null)
            {
                List<ControllerAnnotation> list = new List<ControllerAnnotation>();
                foreach (ControllerAnnotation annotation in _annotations)
                {
                    if (annotation.tourId == tourId)
                        list.Add(annotation);
                }
                return list;
            }
            return null;
        }

        public IEnumerator UpdateAnnotationCoords(ControllerAnnotation annotation, string coords)
		{
            _lastResultFailed = false;
            WebServiceAnnotationsUpdatePosition query = new WebServiceAnnotationsUpdatePosition(token, annotation.id, coords);
            yield return query.Run();
            if (!query.hasFailed)
            {
                string prevCoords = annotation.coords ;
                annotation.coords = coords;
                ////////////////// KTBS ////////////////////////
                Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:movePost"},
                    {"m:postId", annotation.id},
                    {"m:userId", annotation.userId},
                    {"m:tourId", annotation.tourId},
                    {"m:prev_coordinates", prevCoords},
                    {"m:coordinates", coords}
                };
                TraceManager.DispatchEventTraceToManager(trace);
                ////////////////// END ////////////////////////
            }
            else
			{
                _lastResultFailed = true;
            }
        }

        public Coroutine UpdateAnnotationTour(ControllerAnnotation annotation, string tourId)
		{
            return StartCoroutine(UpdateAnnotationTourEnum(annotation, tourId));
		}

        private IEnumerator UpdateAnnotationTourEnum(ControllerAnnotation annotation, string tourId)
        {
            _lastResultFailed = false;
            WebServiceAnnotationsUpdateTour query = new WebServiceAnnotationsUpdateTour(token, annotation.id, tourId);
            yield return query.Run();
            if (!query.hasFailed)
            {
                string prevCoords = annotation.coords;
                annotation.tourId = tourId;
            }
            else
            {
                _lastResultFailed = true;
            }
        }

        public Coroutine DeleteAnnotation(ControllerAnnotation annotation)
		{
            return StartCoroutine(DeleteAnnotationEnum(annotation));
		}

        private IEnumerator DeleteAnnotationEnum(ControllerAnnotation annotation)
		{
            WebServiceAnnotationsDelete queryDel = new WebServiceAnnotationsDelete(_token, annotation.id);
            yield return queryDel.Run();
            if (!queryDel.hasFailed)
            {
                if (!string.IsNullOrEmpty(annotation.imageId))
                {
                    WebServiceMediaObjectsDelete queryDelImg = new WebServiceMediaObjectsDelete(token, annotation.imageId);
                    yield return queryDelImg.Run();
                }
                ////////////////// KTBS ////////////////////////             
                Dictionary<string, object> trace = new Dictionary<string, object>()
                    {
                        {"@type", "m:deletePost"},
                        {"m:postId", annotation.id},
                        {"m:userId", annotation.userId}
                    };
                TraceManager.DispatchEventTraceToManager(trace);
                ////////////////// END ////////////////////////
            }
        }

        #endregion

        #region Tours

        public IEnumerator SendTourEnum(ControllerTour tour)
        {
            _lastResultFailed = false;
            string dateDisplay = ControllerBase.ConvertDateTimeToSend(ControllerBase.ConvertInDateTime(tour.dateDisplay));
            WebServiceToursCreateTour query = new WebServiceToursCreateTour(token, tour.userId, tour.name, tour.body, tour.share, tour.canEdit, dateDisplay);
            yield return query.Run();
            if (!query.hasFailed)
            {
                Dictionary<string, object> dic = query.GetResultAsDic();
                string id = DicTools.GetValueString(dic, "id");
                if (!string.IsNullOrEmpty(id))
                {
                    tour.id = id;
                    CreateTour(dic);
                }
            }
            else
            {
                _lastResultFailed = true;
            }
        }

        public void SetAllTours(List<ControllerTour> list)
        {
            _tours = list;
        }

        public ControllerTour CreateTour(Dictionary<string, object> dic)
        {
            if (_tours == null)
                _tours = new List<ControllerTour>();

            ControllerTour tour = new ControllerTour(dic);
            _tours.Add(tour);

            return tour;
        }

        public string RemoveTour(ControllerTour tour)
        {
            if (_tours != null)
            {
                if (_tours.Contains(tour))
                {
                    string id = tour.id;
                    _tours.Remove(tour);
                    return id;
                }
            }
            return null;
        }

        public IEnumerator UpdateTourContentEnum(ControllerTour tour, string name, string share, string dateDisplay)
		{
            _lastResultFailed = false;
            string dateToSend = ControllerBase.ConvertDateTimeToSend(ControllerBase.ConvertInDateTime(dateDisplay));
            WebServiceToursUpdateContent query = new WebServiceToursUpdateContent(token, tour.id, name, share, dateToSend);
            yield return query.Run();
            if (!query.hasFailed)
            {
                tour.name = name;
                tour.share = share;
                tour.dateDisplay = dateDisplay;
            }
            else
			{
                _lastResultFailed = true;
            }
        }

        public Coroutine UpdateTour(ControllerTour tour, string body, string linkedAnnotations, string status, bool updateAnnotations = false)
        {
            return StartCoroutine(UpdateTourEnum(tour, body, linkedAnnotations, status, updateAnnotations));
        }

        private IEnumerator UpdateTourEnum(ControllerTour tour, string body, string linkedAnnotations, string status, bool updateAnnotations)
        {
            _lastResultFailed = false;
            WebServiceToursUpdateTour query = new WebServiceToursUpdateTour(_token, tour.id, body, linkedAnnotations);
            yield return query.Run();
            if (!query.hasFailed)
            {
                tour.UpdateBody(body);
                tour.linkedAnnotations = linkedAnnotations;

                // kTBS
                Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:moveTour"},
                    {"m:userId", tour.userId},
                    {"m:tourId", tour.id},
                    {"m:tourBody", body},
                    {"m:linkPosts", linkedAnnotations},
                    {"m:status", status}
                };
                TraceManager.DispatchEventTraceToManager(trace);
                ///

            }
            else
            {
                _lastResultFailed = true;
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.TOURS_TITLE), L.Get(TextManager.TOURS_LIST_ERROR), FeedbackPopup.IconType.Alert);
            }
            if (updateAnnotations)
			{
                List<ControllerAnnotation> annotations = GetAnnotationsFromTourId(tour.id);
                if (annotations != null)
                {
                    foreach (ControllerAnnotation annotation in annotations)
                    {
                        yield return UpdateAnnotationCoords(annotation, annotation.coords);
                    }
                }
            }
        }

        public Coroutine DeleteTour(ControllerTour tour, bool autoDelete)
        {
            return StartCoroutine(DeleteTourEnum(tour, autoDelete));
        }

        private IEnumerator DeleteTourEnum(ControllerTour tour, bool autoDelete)
        {
            _lastResultFailed = false;
            WebServiceToursDelete query = new WebServiceToursDelete(_token, tour.id);
            yield return query.Run();
            if (!query.hasFailed)
            {
                // kTBS
                Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:deleteTour"},
                    {"m:userId", tour.userId},
                    {"m:tourId", tour.id},
                    {"m:autoDelete", autoDelete}
                };
                TraceManager.DispatchEventTraceToManager(trace);
                ///

            }
            else
            {
                _lastResultFailed = true;
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.TOURS_TITLE), L.Get(TextManager.TOURS_DELETE_ERROR), FeedbackPopup.IconType.Alert);
            }
        }

        public Coroutine GetAllTours()
        {
            _fetchedTours = false;
            _tours = null;
            return StartCoroutine(GetAllToursEnum());
        }

        private IEnumerator GetAllToursEnum()
        {
            _lastResultFailed = false;
            WebServiceToursGetAll query = new WebServiceToursGetAll(_token);
            yield return query.Run();
            if (!query.hasFailed)
            {
                _tours = new List<ControllerTour>();

                int nextPage;

                do
                {
                    List<object> list = query.GetResultAsList();
                    foreach (object elem in list)
                    {
                        CreateTour(elem as Dictionary<string, object>);
                    }

                    nextPage = query.GetNextPage();
                    if (nextPage > 0)
                    {
                        yield return query.Run(nextPage);
                        if (query.hasFailed)
                            nextPage = 0;
                    }
                }
                while (nextPage > 0);
            }
            else
            {
                _lastResultFailed = true;
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.TOURS_TITLE), L.Get(TextManager.TOURS_LIST_ERROR), FeedbackPopup.IconType.Alert);
            }

            _fetchedTours = true;
        }

        public ControllerTour GetTourFromId(string id)
        {
            if (!string.IsNullOrEmpty(id) && _tours != null)
            {
                foreach (ControllerTour tour in _tours)
                {
                    if (tour.id == id)
                        return tour;
                }
            }
            return null;
        }

        #endregion

        #region Configs

        public Coroutine GetAllConfigs()
        {
            return StartCoroutine(GetAllConfigsEnum());
        }

        public string GetConfig(string key)
		{
            if (_configsDic != null && _configsDic.TryGetValue(key, out string val))
			{
                return val;
			}
            return null;
		}

        private IEnumerator GetAllConfigsEnum()
		{
            _lastResultFailed = false;
            _configsDic = new Dictionary<string, string>();
            WebServiceConfigsGetAll query = new WebServiceConfigsGetAll(_token);
            yield return query.Run();
            if (!query.hasFailed)
			{
                foreach (object o in query.GetResultAsList())
                {
                    AddConfig(o as Dictionary<string, object>);
                }
			}
            else
			{
                _lastResultFailed = true;
            }
            _fetchedConfigs = true;
        }

        private void AddConfig(Dictionary<string, object> dic)
		{
            if (dic != null)
			{
                string key = DicTools.GetValueString(dic, "key");
                string val = DicTools.GetValueString(dic, "value");
                if (!string.IsNullOrEmpty(key))
                    _configsDic.Add(key, val);
            }
		}

        #endregion

        #region Favorites

        public ControllerFavorites GetFavoritesFromUser(string userId)
		{
            if (_favorites != null)
			{
                foreach (ControllerFavorites favorites in _favorites)
                {
                    if (favorites.userId == userId)
                        return favorites;
                }
            }
            return null;
		}

        public Coroutine GetFavoritesFromId(string id)
        {
            return StartCoroutine(GetFavoritesFromIdEnum(id));
        }

        public Coroutine GetAllFavorites()
        {
            return StartCoroutine(GetAllFavoritesEnum());
        }

        public Coroutine UpdateFavorites(string id, string annotations, string tours)
		{
            return StartCoroutine(UpdateFavoritesEnum(id, annotations, tours));
        }

        private IEnumerator UpdateFavoritesEnum(string id, string annotations, string tours)
        {
            _lastResultFailed = false;
            ControllerFavorites favorites = GetFavoritesFromUser(id);
            if (favorites != null)
			{
                WebServiceFavoritesUpdate query = new WebServiceFavoritesUpdate(_token, favorites.id, annotations, tours);
                yield return query.Run();
                if (!query.hasFailed)
                {
                    favorites.UpdateFromDic(query.GetResultAsDic());
                }
                else
                {
                    _lastResultFailed = true;
                }
            }
            else
			{
                WebServiceFavoritesCreate query = new WebServiceFavoritesCreate(_token, id, annotations, tours);
                yield return query.Run();
                if (!query.hasFailed)
                {
                    _favorites.Add(new ControllerFavorites(query.GetResultAsDic()));
                }
                else
                {
                    _lastResultFailed = true;
                }
            }
        }

        private IEnumerator GetFavoritesFromIdEnum(string id)
        {
            _lastResultFailed = false;
            WebServiceFavoritesGetFromId query = new WebServiceFavoritesGetFromId(_token, id);
            yield return query.Run();
            if (!query.hasFailed)
            {
                _favorites = new List<ControllerFavorites>();
                _favorites.Add(new ControllerFavorites(query.GetResultAsDic()));
            }
            else
            {
                _lastResultFailed = true;
            }
            _fetchedFavorites = true;
        }

        private IEnumerator GetAllFavoritesEnum()
        {
            _lastResultFailed = false;
            WebServiceFavoritesGetAll query = new WebServiceFavoritesGetAll(_token);
            yield return query.Run();
            if (!query.hasFailed)
            {
                _favorites = new List<ControllerFavorites>();

                int nextPage;

                do
                {
                    foreach (object o in query.GetResultAsList())
                    {
                        _favorites.Add(new ControllerFavorites(o as Dictionary<string, object>));
                    }

                    nextPage = query.GetNextPage();
                    if (nextPage > 0)
                    {
                        yield return query.Run(nextPage);
                        if (query.hasFailed)
                            nextPage = 0;
                    }
                }
                while (nextPage > 0);
            }
            else
            {
                _lastResultFailed = true;
            }
            _fetchedFavorites = true;
        }

        #endregion

        #region Emojis

        public Dictionary<string, int> GetEmojisForAnnotation(string id)
        {
            EmojisManager emojisMgr = ApplicationManager.instance.emojisManager;

            Dictionary<string, int> emojisCount = null;
            int annotationId = ConvertTools.ToInt32(id);
            foreach (ControllerEmojis emojis in _emojis)
            {
                string emoji = null;
                if (emojis.userId == emojisMgr.userId)
				{
                    if (emojisMgr.annotationDic.ContainsKey(annotationId))
                        emoji = emojisMgr.annotationDic[annotationId];
                }
                else
				{
                    if (emojis.annotationDic.ContainsKey(annotationId))
                        emoji = emojis.annotationDic[annotationId];
                }
                if (emoji != null)
				{
                    if (emojisCount == null)
                        emojisCount = new Dictionary<string, int>();
                    if (emojisCount.ContainsKey(emoji))
                        emojisCount[emoji]++;
                    else
                        emojisCount.Add(emoji, 1);
                }
            }
            return emojisCount;
        }

        public Dictionary<string, int> GetEmojisForTour(string id)
        {
            EmojisManager emojisMgr = ApplicationManager.instance.emojisManager;

            Dictionary<string, int> emojisCount = null;
            int tourId = ConvertTools.ToInt32(id);
            foreach (ControllerEmojis emojis in _emojis)
            {
                string emoji = null;
                if (emojis.userId == emojisMgr.userId)
                {
                    if (emojisMgr.tourDic.ContainsKey(tourId))
                        emoji = emojisMgr.tourDic[tourId];
                }
                else
                {
                    if (emojis.tourDic.ContainsKey(tourId))
                        emoji = emojis.tourDic[tourId];
                }
                if (emoji != null)
                {
                    if (emojisCount == null)
                        emojisCount = new Dictionary<string, int>();
                    if (emojisCount.ContainsKey(emoji))
                        emojisCount[emoji]++;
                    else
                        emojisCount.Add(emoji, 1);
                }
            }
            return emojisCount;
        }

        public ControllerEmojis GetEmojisFromUser(string userId)
        {
            if (_emojis != null)
			{
                foreach (ControllerEmojis emojis in _emojis)
                {
                    if (emojis.userId == userId)
                        return emojis;
                }
            }
            return null;
        }

        public Coroutine GetAllEmojis()
        {
            return StartCoroutine(GetAllEmojisEnum());
        }

        public Coroutine UpdateEmojis(string id, string annotations, string tours)
        {
            return StartCoroutine(UpdateEmojisEnum(id, annotations, tours));
        }

        private IEnumerator GetAllEmojisEnum()
        {
            _lastResultFailed = false;
            WebServiceEmojisGetAll query = new WebServiceEmojisGetAll(_token);
            yield return query.Run();
            if (!query.hasFailed)
            {
                _emojis = new List<ControllerEmojis>();

                int nextPage;

                do
                {
                    foreach (object o in query.GetResultAsList())
                    {
                        _emojis.Add(new ControllerEmojis(o as Dictionary<string, object>));
                    }

                    nextPage = query.GetNextPage();
                    if (nextPage > 0)
                    {
                        yield return query.Run(nextPage);
                        if (query.hasFailed)
                            nextPage = 0;
                    }
                }
                while (nextPage > 0);
            }
            else
            {
                _lastResultFailed = true;
            }
            _fetchedEmojis = true;
        }

        private IEnumerator UpdateEmojisEnum(string id, string annotations, string tours)
        {
            _lastResultFailed = false;
            ControllerEmojis emojis = GetEmojisFromUser(id);
            if (emojis != null)
            {
                WebServiceEmojisUpdate query = new WebServiceEmojisUpdate(_token, emojis.id, annotations, tours);
                yield return query.Run();
                if (!query.hasFailed)
                {
                    emojis.UpdateFromDic(query.GetResultAsDic());
                }
                else
                {
                    _lastResultFailed = true;
                }
            }
            else
            {
                WebServiceEmojisCreate query = new WebServiceEmojisCreate(_token, id, annotations, tours);
                yield return query.Run();
                if (!query.hasFailed)
                {
                    _emojis.Add(new ControllerEmojis(query.GetResultAsDic()));
                }
                else
                {
                    _lastResultFailed = true;
                }
            }
        }

        #endregion

        #region Messages

        public void SetAllMessages(List<ControllerMessage> list)
        {
            _messages = list;
        }

        public ControllerMessage GetMessageFromId(string id)
        {
            if (_messages != null)
            {
                foreach (ControllerMessage message in _messages)
                {
                    if (message.id == id)
                        return message;
                }
            }
            return null;
        }

        public List<ControllerMessage> GetMessagesFromAnnotation(string annotationId)
        {
            List<ControllerMessage> messages = new List<ControllerMessage>(); 
            if (_messages != null)
            {
                foreach (ControllerMessage message in _messages)
                {
                    if (message.annotationId == annotationId)
                        messages.Add(message);
                }
            }
            return messages;
        }

        public List<ControllerMessage> GetMessagesFromTour(string tourId)
        {
            List<ControllerMessage> messages = new List<ControllerMessage>();
            if (_messages != null)
            {
                foreach (ControllerMessage message in _messages)
                {
                    if (message.tourId == tourId)
                        messages.Add(message);
                }
            }
            return messages;
        }

        public Coroutine GetAllMessages()
        {
            return StartCoroutine(GetAllMessagesEnum());
        }

        public Coroutine CreateMessage(string userId, string comment, string imageId, string icons, string emoticon, 
            string tags, string annotationId, string tourId, System.Action<ControllerMessage> result = null)
        {
            return StartCoroutine(CreateMessageEnum(userId, comment, imageId, icons, emoticon, tags, annotationId, tourId, result));
        }

        public Coroutine UpdateMessage(string id, string comment, string imageId, string icons, string emoticon, string tags)
        {
            return StartCoroutine(UpdateMessageEnum(id, comment, imageId, icons, emoticon, tags));
        }

        public Coroutine DeleteMessage(string id, System.Action<ControllerMessage> result = null)
		{
            return StartCoroutine(DeleteMessageEnum(id, result));
		}

        public void AddMessage(ControllerMessage message)
		{
            if (message != null)
			{
                if (GetMessageFromId(message.id) == null)
                {
                    _messages.Add(message);
                }
            }   
        }

        private IEnumerator GetAllMessagesEnum()
        {
            _lastResultFailed = false;
            WebServiceMessagesGetAll query = new WebServiceMessagesGetAll(_token);
            yield return query.Run();
            if (!query.hasFailed)
            {
                _messages = new List<ControllerMessage>();

                int nextPage;

                do
                {
                    foreach (object o in query.GetResultAsList())
                    {
                        AddMessage(new ControllerMessage(o as Dictionary<string, object>));
                    }

                    nextPage = query.GetNextPage();
                    if (nextPage > 0)
                    {
                        yield return query.Run(nextPage);
                        if (query.hasFailed)
                            nextPage = 0;
                    }
                }
                while (nextPage > 0);
            }
            else
            {
                _lastResultFailed = true;
            }
            _fetchedMessages = true;
        }

        private IEnumerator CreateMessageEnum(string userId, string comment, string imageId, string icons, string emoticon, 
            string tags, string annotationId, string tourId, System.Action<ControllerMessage> result = null)
        {
            _lastResultFailed = false;
            WebServiceMessagesCreate query = new WebServiceMessagesCreate(_token, userId, comment, imageId, icons, emoticon, tags, annotationId, tourId);
            yield return query.Run();
            if (!query.hasFailed) 
            {
                ControllerMessage message = new ControllerMessage(query.GetResultAsDic());
                AddMessage(message);
                result?.Invoke(message);

                // KTBS2
                Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:addMessage"},
                    {"m:messageId", message.id},
                    {"m:userId", message.userId},
                    {"m:postId", message.annotationId},                    
                    {"m:emoticon", message.emoticon},
                    {"m:icons", message.icons},
                    {"m:imageId", message.imageId},
                    {"m:tourId", message.tourId},
                    {"m:dateDisplay", message.date},
                    {"m:dateModif", message.dateModif}
                };
                TraceManager.DispatchEventTraceToManager(trace);
                
                
            }
            else
            {
                _lastResultFailed = true;
            }
        }

        private IEnumerator UpdateMessageEnum(string id, string comment, string imageId, string icons, string emoticon, string tags)
        {
            _lastResultFailed = false;
            ControllerMessage message = GetMessageFromId(id);
            if (message != null)
            {
                WebServiceMessagesUpdate query = new WebServiceMessagesUpdate(_token, id, comment, imageId, icons, emoticon, tags);
                yield return query.Run();
                if (!query.hasFailed)
                {
                    message.comment = comment;
                    message.imageId = imageId;
                    message.icons = icons;
                    message.emoticon = emoticon;
                    message.tags = tags;

                    // KTBS2
                    Dictionary<string, object> trace = new Dictionary<string, object>()
                    {
                        {"@type", "m:editMessage"},
                        {"m:messageId", message.id},
                        {"m:userId", message.userId},
                        {"m:postId", message.annotationId},                    
                        {"m:emoticon", message.emoticon},
                        {"m:icons", message.icons},
                        {"m:imageId", message.imageId},
                        {"m:tourId", message.tourId},
                        {"m:dateDisplay", message.date},
                        {"m:dateModif", message.dateModif}
                    };
                    TraceManager.DispatchEventTraceToManager(trace);
                
                }
                else
                {
                    _lastResultFailed = true;
                }
            }
        }

        private IEnumerator DeleteMessageEnum(string id, System.Action<ControllerMessage> result = null)
		{
            _lastResultFailed = false;
            ControllerMessage message = GetMessageFromId(id);
            if (message != null)
            {
                WebServiceMessagesDelete query = new WebServiceMessagesDelete(_token, id);
                yield return query.Run();
                if (!query.hasFailed)
                {
                    _messages.Remove(message);
                    result?.Invoke(message);

                    // KTBS2
                    Dictionary<string, object> trace = new Dictionary<string, object>()
                    {
                        {"@type", "m:deleteMessage"},
                        {"m:messageId", message.id},
                        {"m:userId", message.userId},
                        {"m:postId", message.annotationId},                                            
                        {"m:dateDisplay", message.date},
                        {"m:dateModif", message.dateModif}
                    };
                    TraceManager.DispatchEventTraceToManager(trace);
                
                }
                else
                {
                    _lastResultFailed = true;
                }
            }
        }

        #endregion

        #region Notifications

        public List<ControllerNotification> GetUserNotifications(string userId)
		{
            List<ControllerNotification> notifs = new List<ControllerNotification>();
            foreach (ControllerNotification notif in _notifications)
			{
                if (notif.userId == userId)
                    notifs.Add(notif);
			}
            return notifs;
        }

        public ControllerNotification GetNotificationFromId(string id)
		{
            foreach (ControllerNotification notif in _notifications)
            {
                if (notif.id == id)
                    return notif;
            }
            return null;
        }

        public int GetNotReadNotificationCount()
        {
            int count = 0;
            if (_notifications != null)
			{
                foreach (ControllerNotification notif in _notifications)
                {
                    if (notif.isNotRead)
                        count++;
                }
            }
            return count;
        }

        public List<ControllerNotification> GetNotReadNotificationsToOpenAtLogin()
        {
            List<ControllerNotification> notifs = null; 
            foreach (ControllerNotification notif in _notifications)
            {
                if (notif.isNotRead && notif.openAtLogin)
				{
                    if (notifs == null)
                        notifs = new List<ControllerNotification>();
                    notifs.Add(notif);
                }
            }
            return notifs;
        }

        public Coroutine GetAllNotifications(string userId)
        {
            return StartCoroutine(GetAllNotificationsEnum(userId));
        }

        public Coroutine PatchNotificationRead(string id, System.Action<bool> result = null)
        {
            return StartCoroutine(PatchNotificationReadEnum(id, result));
        }

        public Coroutine SendNotification(string destUserId, string title, string content, string category, string parameters, string expiredDate)
		{
            return StartCoroutine(SendNotificationEnum(destUserId, title, content, category, parameters, expiredDate));
        }

        public Coroutine DeleteNotification(string id, System.Action<bool> result = null)
        {
            return StartCoroutine(DeleteNotificationEnum(id, result));
        }

        private IEnumerator GetAllNotificationsEnum(string userId)
        {
            _lastResultFailed = false;
            WebServiceNotificationsGetAllForUser query = new WebServiceNotificationsGetAllForUser(_token, userId);
            yield return query.Run();
            if (!query.hasFailed)
            {
                _notifications = new List<ControllerNotification>();

                int nextPage;

                do
                {
                    foreach (object o in query.GetResultAsList())
                    {
                        _notifications.Add(new ControllerNotification(o as Dictionary<string, object>));
                    }

                    nextPage = query.GetNextPage();
                    if (nextPage > 0)
                    {
                        yield return query.Run(nextPage);
                        if (query.hasFailed)
                            nextPage = 0;
                    }
                }
                while (nextPage > 0);
            }
            else
            {
                _lastResultFailed = true;
            }
            _fetchedNotifications = true;
        }

        private IEnumerator PatchNotificationReadEnum(string id, System.Action<bool> result = null)
        {
            ControllerNotification notif = GetNotificationFromId(id);
            if (notif == null)
			{
                _lastResultFailed = true;
                result?.Invoke(false);
                yield break;
            }
            _lastResultFailed = false;
            WebServiceNotificationsUpdateRead query = new WebServiceNotificationsUpdateRead(_token, id);
            yield return query.Run();
            if (!query.hasFailed)
            {
                Dictionary<string, object> dic = query.GetResultAsDic();
                notif.readDate = DicTools.GetValueString(dic, "readDate");
                result?.Invoke(true);
            }
            else
            {
                _lastResultFailed = true;
                result?.Invoke(false);
            }
        }

        private IEnumerator SendNotificationEnum(string destUserId, string title, string content, string category, string parameters, string expiredDate)
		{
            _lastResultFailed = false;
            WebServiceNotificationsCreate query = new WebServiceNotificationsCreate(_token, destUserId, title, content, category, parameters, expiredDate);
            yield return query.Run();
            if (!query.hasFailed)
            {
            }
            else
            {
                _lastResultFailed = true;
            }
        }

        private IEnumerator DeleteNotificationEnum(string id, System.Action<bool> result = null)
        {
            ControllerNotification notif = GetNotificationFromId(id);
            if (notif == null)
            {
                _lastResultFailed = true;
                result?.Invoke(false);
                yield break;
            }
            _lastResultFailed = false;
            WebServiceNotificationsDelete query = new WebServiceNotificationsDelete(_token, id);
            yield return query.Run();
            if (!query.hasFailed)
            {
                _notifications.Remove(notif);
                result?.Invoke(true);
            }
            else
            {
                _lastResultFailed = true;
                result?.Invoke(false);
            }
        }

        #endregion

        #region Groups

        public ControllerGroup GetGroupFromId(string id)
		{
            foreach (ControllerGroup group in _groups)
			{
                if (group.id == id)
                    return group;
			}
            return null;
		}

        public Coroutine GetAllGroups()
        {
            return StartCoroutine(GetAllGroupsEnum());
        }

        private IEnumerator GetAllGroupsEnum()
        {
            _lastResultFailed = false;
            WebServiceGroupsGetAll query = new WebServiceGroupsGetAll(_token);
            yield return query.Run();
            if (!query.hasFailed)
            {
                _groups = new List<ControllerGroup>();

                int nextPage;

                do
                {
                    foreach (object o in query.GetResultAsList())
                    {
                        _groups.Add(new ControllerGroup(o as Dictionary<string, object>));
                    }

                    nextPage = query.GetNextPage();
                    if (nextPage > 0)
                    {
                        yield return query.Run(nextPage);
                        if (query.hasFailed)
                            nextPage = 0;
                    }
                }
                while (nextPage > 0);
            }
            else
            {
                _lastResultFailed = true;
            }
            _fetchedGroups = true;
        }

        #endregion
    }
}
