namespace Liris.App
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
	using Liris.App.Popups;
	using Liris.App.Controllers;
	using Liris.App.UI;
	using AllMyScripts.LangManager;
    using System.Collections.Generic;
    using Liris.App.Trace;

	public class MainUI : MonoBehaviour
    {
        public static MainUI instance = null;

        public enum UIEvent
        {
            MODE_NONE,
            MODE_GPS,
            MODE_EDIT,
            MODE_POINT,
            START_PATH,
            STOP_PATH,
            CENTER_GPS,
            MOVE_VALIDATE,
            MOVE_CANCEL,
            EXIT_PREVIEW_TOUR
        }

        public delegate void OnUIEvent(UIEvent evt);

        public static OnUIEvent onUIEvent = null;

        public UserPosition userPosition => _userPosition;
        public MsgMove indicatorPosition => _indicatorPosition;

        [Header("Button Groups")]
        [SerializeField]
        private GameObject _topButtons = null;
        [SerializeField]
        private GameObject _middleButtons = null;
        [SerializeField]
        private GameObject _bottomButtons = null;
        [Header("Buttons")]
        [SerializeField]
        private Button _tourStartBtn = null;
        [SerializeField]
        private Button _tourStopBtn = null;
        [SerializeField]
        private Button _editStartBtn = null;
        [SerializeField]
        private Button _editStopBtn = null;
        [SerializeField]
        private Button _notifStartBtn = null;
        [SerializeField]
        private Button _notifStopBtn = null;
        [SerializeField]
        private Button _notifBtn = null;
        [SerializeField]
        private Button _userBtn = null;
        [SerializeField]
        private Button _mapBtn = null;
        [SerializeField]
        private Button _visibilityBtn = null;
        [SerializeField]
        private Button _filterBtn = null;
        [SerializeField]
        private Button _favoritesBtn = null;
        [SerializeField]
        private Button _gpsSearchBtn = null;
        [SerializeField]
        private Button _validateMoveBtn = null;
        [SerializeField]
        private Button _cancelMoveBtn = null;
        [SerializeField]
        private Button _addressBtn = null;
        [SerializeField]
        private Button _notificationsBtn = null;
        [SerializeField]
        private Button _refreshBtn = null;
        [SerializeField]
        private Button _finalizeBtn = null;
        [SerializeField]
        private Button _deleteTourBtn = null;
        [Header("Messages")]
        [SerializeField]
        private MsgMixte _msgMixtePrefab = null;
        [SerializeField]
        private MsgPicture _msgPicturePrefab = null;
        [SerializeField]
        private MsgIcon _msgIconPrefab = null;
        [SerializeField]
        private MsgText _msgTextPrefab = null;
        [SerializeField]
        private MsgMove _msgMovePrefab = null;
        [SerializeField]
        private TextMeshProUGUI _versionText = null;
        [SerializeField]
        private Transform _messagesRoot = null;
        [SerializeField]
        private Transform _moveRoot = null;
        [Header("User sprites")]
        [SerializeField]
        private Image _userIcon = null;
        [SerializeField]
        private Sprite _userOnSprite = null;
        [SerializeField]
        private Sprite _userOffSprite = null;
        [Header("Popup Prefabs")]
        [SerializeField]
        private LoginPopup _loginPopupPrefab = null;
        [SerializeField]
        private CreateUserPopup _createUserPopupPrefab = null;
        [SerializeField]
        private NewUserPopup _newUserPopupPrefab = null;
        [SerializeField]
        private FeedbackPopup _feedbackPopupPrefab = null;
        [SerializeField]
        private UserInfoPopup _userInfoPopupPrefab = null;
        [SerializeField]
        private DisclaimerPopup _disclaimerPopupPrefab = null;
        [SerializeField]
        private PlaceTypePopup _placeTypePopupPrefab = null;
        [SerializeField]
        private AnnotationPopup _annotationPopupPrefab = null;
        [SerializeField]
        private TagsPopup _tagsPopupPrefab = null;
        [SerializeField]
        private EmoticonsPopup _emoticonsPopupPrefab = null;
        [SerializeField]
        private IconsPopup _iconsPopupPrefab = null;
        [SerializeField]
        private MsgMixtePopup _msgMixtePopupPrefab = null;
        [SerializeField]
        private LoadingPopup _loadingPopupPrefab = null;
        [SerializeField]
        private ChoicePopup _choicePopupPrefab = null;
        [SerializeField]
        private VisibilityPopup _visibilityPopupPrefab = null;
        [SerializeField]
        private FiltersPopup _filtersPopupPrefab = null;
        [SerializeField]
        private TourPopup _tourPopupPrefab = null;
        [SerializeField]
        private EditTourPopup _editTourPopupPrefab = null;
        [SerializeField]
        private CalendarPopup _calendarPopupPrefab = null;
        [SerializeField]
        private FavoritesPopup _favoritesPopupPrefab = null;
        [SerializeField]
        private PhotoPopup _photoPopupPrefab = null;
        [SerializeField]
        private EmojisPopup _emojisPopupPrefab = null;
        [SerializeField]
        private MessagesPopup _messagesPopupPrefab = null;
        [SerializeField]
        private EditMessagePopup _editMessagePopupPrefab = null;
        [SerializeField]
        private SearchAddressPopup _searchAddressPopupPrefab = null;
		[SerializeField]
		private NotificationsPopup _notificationsPopupPrefab = null;
        [SerializeField]
        private GroupsPopup _groupsPopupPrefab = null;
        [SerializeField]
        private SharePopup _sharePopupPrefab = null;
        [SerializeField]
        private OptionsPopup _optionsPopupPrefab = null;
        [SerializeField]
        private NotificationPopup _notificationPopupPrefab = null;
        [SerializeField]
        private NotificationDeletePopup _notificationDeletePopupPrefab = null;
        [Header("User position")]
        [SerializeField]
        private UserPosition _userPosition = null;
        [Header("Indicator position")]
        [SerializeField]
        private MsgMove _indicatorPosition = null;
        [Header("Internet On/Off")]
        [SerializeField]
        private GameObject _internetRoot = null;
        [SerializeField]
        private GameObject _internetOn = null;
        [SerializeField]
        private GameObject _internetOff = null;
        [Header("Notifs")]
        [SerializeField]
        private NotifCount _filtersNotifCount = null;
        [SerializeField]
        private NotifCount _notificationsNotifCount = null;
        [Header("Preview Tour Part")]
        [SerializeField]
        private GameObject _previewTourGroup = null;
        [SerializeField]
        private Button _exitPreviewTourBtn = null;
        [SerializeField]
        private Slider _previewTourSlider = null;
        [SerializeField]
        private TextMeshProUGUI _previewTourTitle = null;
        [SerializeField]
        private TextMeshProUGUI _previewTourBody = null;

        private LoadingPopup _loading = null;

        public void SetFilterNotifCount(int count)
		{
            _filtersNotifCount.SetCount(count);
        }

        public void SetNotificationsNotifCount(int count)
		{
            _notificationsNotifCount.SetCount(count);
        }

        public MsgMixte CreateMixteAnnotation()
        {
            MsgMixte msg = GameObject.Instantiate<MsgMixte>(_msgMixtePrefab);
            SetParentMsg(msg);
            return msg;
        }

        public MsgPicture CreatePicture()
        {
            MsgPicture msg = GameObject.Instantiate<MsgPicture>(_msgPicturePrefab);
            SetParentMsg(msg);
            return msg;
        }

        public MsgIcon CreateIcon()
        {
            MsgIcon msg = GameObject.Instantiate<MsgIcon>(_msgIconPrefab);
            SetParentMsg(msg);
            return msg;
        }

        public MsgText CreateText()
        {
            MsgText msg = GameObject.Instantiate<MsgText>(_msgTextPrefab);
            SetParentMsg(msg);
            return msg;
        }

        public MsgMove CreateMove()
        {
            MsgMove msg = GameObject.Instantiate<MsgMove>(_msgMovePrefab);
            SetParentMsg(msg);
            return msg;
        }

        public void StartMoveMessage(MsgBase msg)
        {
            _moveRoot.gameObject.SetActive(true);
            if (msg != null)
                msg.transform.SetParent(_moveRoot);
            ResetAllModes();
        }

        public void StopMoveMessage(MsgBase msg)
        {
            _moveRoot.gameObject.SetActive(false);
            if (msg != null)
                msg.transform.SetParent(_messagesRoot);
            ResetAllModes();
        }

        public void ShowGpsSearchButton(bool show)
        {
            _gpsSearchBtn.gameObject.SetActive(show);
        }

        public void ShowLoginPopup()
        {
            ControllerUser user = ApplicationManager.instance.user;
            if (user != null)
            {
                string title = $"{user.username}";
                string body = $"{user.firstName} {user.lastName}\n" +
                    $"{user.mail}\n" +
                    $"{ControllerBase.ConvertDateDay(user.birthDate)}\n" +
                    $"{L.Get(TextManager.INFO_INSTITUTION)} {user.institution}\n" +
                    $"{L.Get(TextManager.INFO_DIPLOMA)} {user.diploma}\n" +
                    $"{L.Get(TextManager.INFO_FORMATION)} {user.formation}\n" +
                    $"{L.Get(TextManager.INFO_NATIONALITY)} {user.nationality}";
                ShowUserInfoPopup(title, body, user.admin);
            }
            else
            {
                GameObject.Instantiate(_loginPopupPrefab, transform);
            }
        }

        public void ShowCreateUserPopup()
        {
            CreateUserPopup popup = GameObject.Instantiate(_createUserPopupPrefab, transform);
            popup.SetUser(ApplicationManager.instance.user);
        }

        public void ShowNewUserPopup()
        {
            GameObject.Instantiate(_newUserPopupPrefab, transform);
        }

        public void ShowFeedbackPopup(string title, string message, FeedbackPopup.IconType iconType = FeedbackPopup.IconType.None, System.Action onCloseEvent = null)
        {
            FeedbackPopup popup = GameObject.Instantiate<FeedbackPopup>(_feedbackPopupPrefab, transform);
            popup.SetTitle(title);
            popup.SetMessage(message);
            popup.SetIconType(iconType);
            popup.SetOnCloseEvent(onCloseEvent);
        }

        public void ShowUserInfoPopup(string title, string infos, bool canAddUser)
        {
            UserInfoPopup popup = GameObject.Instantiate<UserInfoPopup>(_userInfoPopupPrefab, transform);
            popup.SetTitle(title);
            popup.SetInfos(infos);
            popup.ShowAddUser(canAddUser);
        }

        public void ShowDisclaimerPopup()
        {
            GameObject.Instantiate<DisclaimerPopup>(_disclaimerPopupPrefab, transform);
        }

        public void CheckDisclaimerAndVersion()
        {
            ControllerUser user = ApplicationManager.instance.user;
            if (user != null)
            {
                if (!user.disclaimer)
                    ShowDisclaimerPopup();
                else if (!ControllerUser.CheckPseudo(user.username))
                    ShowCreateUserPopup();
                if (user.version != Application.version)
                    ApplicationManager.instance.dataManager.UpdateUserVersion(user.id, Application.version);
            }
        }

        public void ShowAnnotationPopup(string placeTypeName, string date, ApplicationManager.TimingType timing, ControllerTour tour)
        {
            AnnotationPopup popup = GameObject.Instantiate<AnnotationPopup>(_annotationPopupPrefab, transform);
            popup.SetPlaceTypeName(placeTypeName, date, timing, tour);
        }

        public void EditNotificationPopup(ControllerAnnotation annotation, ControllerTour tour)
        {
            AnnotationPopup popup = GameObject.Instantiate<AnnotationPopup>(_annotationPopupPrefab, transform);
            popup.SetEditionMode(annotation, tour);
        }

        public void ShowPlaceTypePopup(ControllerTour tour = null, AnnotationPopup annotPopup = null)
        {
            PlaceTypePopup popup = GameObject.Instantiate<PlaceTypePopup>(_placeTypePopupPrefab, transform);
            popup.SetCurrentTour(tour);
            popup.SetCurrentAnnotationPopup(annotPopup);
        }

        public void ShowTagsPopup(string selectedTags, System.Action<string> onResult = null)
        {
            TagsPopup popup = GameObject.Instantiate<TagsPopup>(_tagsPopupPrefab, transform);
            popup.SetSelectedTags(selectedTags);
            popup.SetOnResultAction(onResult);
        }

        public void ShowEmoticonsPopup(string numIcon, System.Action<string> onResult = null)
        {
            EmoticonsPopup popup = GameObject.Instantiate<EmoticonsPopup>(_emoticonsPopupPrefab, transform);
            popup.SetOnResultAction(onResult);
            popup.SetSelectedIcon(numIcon);
        }

        public void ShowPlaceIconsPopup(string selectedIcons, System.Action<string> onResult = null)
        {
            IconsPopup popup = GameObject.Instantiate<IconsPopup>(_iconsPopupPrefab, transform);
            popup.SetOnResultAction(onResult);
            popup.SetSelectedIcons(selectedIcons);
        }

        public void ShowLoadingPopup()
        {
            _loading = GameObject.Instantiate<LoadingPopup>(_loadingPopupPrefab, transform);
        }

        public void HideLoadingPopup()
        {
            _loading.Hide();
        }

        public void ShowChoicePopup(string title, string message, string ok, string cancel, System.Action<bool> result = null, bool modal = true)
        {
            ChoicePopup popup = GameObject.Instantiate<ChoicePopup>(_choicePopupPrefab, transform);
            popup.SetTitle(title);
            popup.SetMessage(message);
            popup.SetOkText(ok);
            popup.SetCancelText(cancel);
            popup.SetOnChoiceEvent(result);
            popup.SetModal(modal);
        }

        public void ShowVisibilityPopup(ControllerUser user)
        {
            VisibilityPopup popup = GameObject.Instantiate<VisibilityPopup>(_visibilityPopupPrefab, transform);
            popup.InitFromUser(user);
        }

        public void ShowFilterPopup()
        {
            GameObject.Instantiate<FiltersPopup>(_filtersPopupPrefab, transform);
        }

        public void CreateTourPopup(bool canEdit, System.Action<ControllerTour> onResult = null)
        {
            TourPopup popup = GameObject.Instantiate<TourPopup>(_tourPopupPrefab, transform);
            popup.SetEditable(canEdit);
            popup.SetOnResultAction(onResult);
        }

        public void ShowTourPopupOnEdition(ControllerTour tour)
        {
            TourPopup popup = GameObject.Instantiate<TourPopup>(_tourPopupPrefab, transform);
            popup.SetTourOnEdition(tour);
        }

        public void ShowEditTourPopup(ControllerTour tour, bool isInPreview = false, bool openToDelete = false, System.Action<bool> onDeleteResult = null)
        {
            EditTourPopup popup = GameObject.Instantiate<EditTourPopup>(_editTourPopupPrefab, transform);
            popup.SetTour(tour, true, isInPreview);
            if (openToDelete)
                popup.OpenChoiceToDelete(onDeleteResult);
        }

        public void ShowCalendarPopup(DatePickerTitle datePicker, System.DateTime dateTime, bool useTime = false)
        {
            CalendarPopup popup = GameObject.Instantiate<CalendarPopup>(_calendarPopupPrefab, transform);
            popup.SetDatePicker(datePicker, dateTime, useTime);
        }

        public void ShowFavoritesPopup(List<ControllerTour> tours, List<ControllerAnnotation> annotations)
        {
            FavoritesPopup popup = GameObject.Instantiate<FavoritesPopup>(_favoritesPopupPrefab, transform);
            popup.InitItems(tours, annotations);
        }

        public void ShowPhotoPopup(Texture2D tex)
        {
            PhotoPopup popup = GameObject.Instantiate<PhotoPopup>(_photoPopupPrefab, transform);
            popup.SetTexture(tex);
        }

        public void ShowEmojisPopup(int choosenEmoji, System.Action<int> onResult = null)
        {
            EmojisPopup popup = GameObject.Instantiate<EmojisPopup>(_emojisPopupPrefab, transform);
            popup.SetChoosenEmoji(choosenEmoji);
            popup.SetOnResultAction(onResult);
        }

        public void ShowMessagesPopup(string annotationId, string tourId, List<ControllerMessage> mesages, System.Action<ControllerMessage> needRefresh = null)
        {
            MessagesPopup popup = GameObject.Instantiate<MessagesPopup>(_messagesPopupPrefab, transform);
            popup.InitMessages(annotationId, tourId, mesages, needRefresh);
        }

        public void ShowEditMessagesPopup(string annotationId, string tourId, ControllerMessage mesage = null, System.Action<ControllerMessage> needRefresh = null)
        {
            EditMessagePopup popup = GameObject.Instantiate<EditMessagePopup>(_editMessagePopupPrefab, transform);
            popup.SetEditionMode(annotationId, tourId, mesage, needRefresh);
        }

        public void ShowSearchAddressPopup()
		{
            GameObject.Instantiate<SearchAddressPopup>(_searchAddressPopupPrefab, transform);
		}
        
        public void ShowNotificationsPopup()
		{
            NotificationsPopup popup = GameObject.Instantiate<NotificationsPopup>(_notificationsPopupPrefab, transform);
            popup.InitItems(ApplicationManager.instance.dataManager.GetUserNotifications(ApplicationManager.instance.user.id));
		}

        public void ShowGroupsPopup()
		{
            GroupsPopup popup = GameObject.Instantiate<GroupsPopup>(_groupsPopupPrefab, transform);
            popup.InitItems(ApplicationManager.instance.dataManager.groups);
        }

        public void ShowSharePopupPrefab(string share, System.Action<string> result)
        {
            SharePopup popup = GameObject.Instantiate<SharePopup>(_sharePopupPrefab, transform);
            popup.InitItems(share, ApplicationManager.instance.groupsManager.GetUserGroups(), result);
        }

        public void ShowOptionsPopup()
		{
            OptionsPopup popup = GameObject.Instantiate<OptionsPopup>(_optionsPopupPrefab, transform);
        }

        public void ShowNotificationPopup(ControllerNotification notif)
		{
            NotificationPopup popup = GameObject.Instantiate<NotificationPopup>(_notificationPopupPrefab, transform);
            popup.SetNotification(notif);

        }

        public void ShowNotificationDeletePopup(ControllerNotification notif, System.Action<NotificationDeletePopup.Reason> result)
		{
            NotificationDeletePopup popup = GameObject.Instantiate<NotificationDeletePopup>(_notificationDeletePopupPrefab, transform);
            popup.InitPopup(notif.id, result);
        }

        public void ShowMsgMixtePopup(ControllerAnnotation annotation, bool showBottomButtons = true, bool isInPreview = false)
        {
            MsgMixtePopup popup = GameObject.Instantiate<MsgMixtePopup>(_msgMixtePopupPrefab, transform);
            popup.SetAnnotation(annotation, showBottomButtons, isInPreview);
        }

        public void ResetCreateTour()
        {
            if (_editStopBtn.gameObject.activeInHierarchy)
            {
                OnEditStopClick();
            }
            else if (_tourStopBtn.gameObject.activeInHierarchy)
            {
                OnTourStopClick();
            }
        }

        public void SetInternetState(bool on)
		{
            _internetRoot.SetActive(true);
            _internetOn.SetActive(on);
            _internetOff.SetActive(!on);
		}

        public void HideInternetState()
        {
            _internetRoot.SetActive(false);
        }

        public void ResetAllModes()
		{
            _tourStartBtn.gameObject.SetActive(true);
            _tourStartBtn.interactable = true;
            _tourStopBtn.gameObject.SetActive(false);
            _notifBtn.gameObject.SetActive(false);
            _editStartBtn.gameObject.SetActive(true);
            _editStartBtn.interactable = true;
            _editStopBtn.gameObject.SetActive(false);
            _notifStartBtn.gameObject.SetActive(true);
            _notifStartBtn.interactable = true;
            _notifStopBtn.gameObject.SetActive(false);
            _finalizeBtn.gameObject.SetActive(false);
            _deleteTourBtn.gameObject.SetActive(false);
        }

        public void LoadingData(bool load)
		{
            _filterBtn.interactable = !load;
            _visibilityBtn.interactable = !load;
            _favoritesBtn.interactable = !load;
            _notificationsBtn.interactable = !load;
            _refreshBtn.interactable = !load;
        }

        public void SetPreviewTourMode(ControllerTour tour)
		{
            bool isInPreviewTour = tour != null;
            _previewTourGroup.SetActive(isInPreviewTour);
            _topButtons.SetActive(!isInPreviewTour);
            _middleButtons.SetActive(!isInPreviewTour);
            _bottomButtons.SetActive(!isInPreviewTour);
            if (isInPreviewTour)
			{
                _previewTourSlider.SetValueWithoutNotify(0f);
                ControllerUser user = tour.GetUser();
                if (user != null)
                    _previewTourTitle.text = user.username + "\n" + tour.GetFormattedDate();
                else
                    _previewTourTitle.text = tour.GetFormattedDate();
                _previewTourBody.text = tour.name;
            }
        }

        private void SetParentMsg(MsgBase msg)
        {
            msg.transform.SetParent(_messagesRoot);
            msg.transform.localPosition = Vector3.zero;
            msg.transform.localScale = Vector3.one;
            msg.transform.localRotation = Quaternion.identity;
        }

        private void Awake()
        {
            instance = this;
            ApplicationManager.onLoginEvent += OnUserLogin;
            ApplicationManager.onLogoutEvent += OnUserLogout;
        }

        private void Start()
        {
            _tourStartBtn.onClick.AddListener(OnTourStartClick);
            _tourStopBtn.onClick.AddListener(OnTourStopClick);
            _editStartBtn.onClick.AddListener(OnEditStartClick);
            _editStopBtn.onClick.AddListener(OnEditStopClick);
            _notifStartBtn.onClick.AddListener(OnNotifStartClick);
            _notifStopBtn.onClick.AddListener(OnNotifStopClick);

            _notifBtn.onClick.AddListener(OnNotifClick);
            _userBtn.onClick.AddListener(OnUserClick);
            _mapBtn.onClick.AddListener(OnMapClick);
            _visibilityBtn.onClick.AddListener(OnVisibilityClick);
            _filterBtn.onClick.AddListener(OnFilterClick);
            _favoritesBtn.onClick.AddListener(OnFavoritesClick);
            _gpsSearchBtn.onClick.AddListener(OnGpsSearchClick);
            _addressBtn.onClick.AddListener(OnAddressClick);
            _notificationsBtn.onClick.AddListener(OnNotificationsClick);
            _refreshBtn.onClick.AddListener(OnRefreshClick);
            _finalizeBtn.onClick.AddListener(OnFinalizeClick);
            _deleteTourBtn.onClick.AddListener(OnDeleteTourClick);

            _validateMoveBtn.onClick.AddListener(OnValidateMoveClick);
            _cancelMoveBtn.onClick.AddListener(OnCancelMoveClick);

            _exitPreviewTourBtn.onClick.AddListener(OnExitPreviewTourClick);
            _previewTourSlider.onValueChanged.AddListener(OnPreviewTourSlide);

            InputLocationData.onLocationData += OnLocationData;
            
            _tourStartBtn.gameObject.SetActive(true);
            _tourStartBtn.interactable = false;
            _tourStopBtn.gameObject.SetActive(false);
            _notifBtn.gameObject.SetActive(false);
            _editStartBtn.gameObject.SetActive(true);
            _editStartBtn.interactable = false;
            _editStopBtn.gameObject.SetActive(false);
            _notifStartBtn.gameObject.SetActive(true);
            _notifStartBtn.interactable = false;
            _notifStopBtn.gameObject.SetActive(false);
            _finalizeBtn.gameObject.SetActive(false);
            _deleteTourBtn.gameObject.SetActive(false);

            _visibilityBtn.interactable = false;
            _filterBtn.interactable = false;
            _favoritesBtn.interactable = false;
            _notificationsBtn.interactable = false;
            _refreshBtn.interactable = false;

            _versionText.text = "Version " + Application.version;
            _userPosition.gameObject.SetActive(false);
            _userPosition.SetControl(OnlineMapsTileSetControl.instance);
            _indicatorPosition.gameObject.SetActive(false);
            _indicatorPosition.SetControl(OnlineMapsTileSetControl.instance);

            _moveRoot.gameObject.SetActive(false);
            _previewTourGroup.SetActive(false);

            ShowGpsSearchButton(false);
            UpdateUserSprite(false);
        }

        private void OnDestroy()
        {
            ApplicationManager.onLoginEvent -= OnUserLogin;
            ApplicationManager.onLogoutEvent -= OnUserLogout;
            instance = null;

            _tourStartBtn.onClick.RemoveListener(OnTourStartClick);
            _tourStopBtn.onClick.RemoveListener(OnTourStopClick);
            _editStartBtn.onClick.RemoveListener(OnEditStartClick);
            _editStopBtn.onClick.RemoveListener(OnEditStopClick);
            _notifStartBtn.onClick.RemoveListener(OnNotifStartClick);
            _notifStopBtn.onClick.RemoveListener(OnNotifStopClick);

            _notifBtn.onClick.RemoveListener(OnNotifClick);
            _userBtn.onClick.RemoveListener(OnUserClick);
            _mapBtn.onClick.RemoveListener(OnMapClick);            
            _visibilityBtn.onClick.RemoveListener(OnVisibilityClick);
            _filterBtn.onClick.RemoveListener(OnFilterClick);
            _favoritesBtn.onClick.RemoveListener(OnFavoritesClick);
            _gpsSearchBtn.onClick.RemoveListener(OnGpsSearchClick);
            _addressBtn.onClick.RemoveListener(OnAddressClick);
            _notificationsBtn.onClick.RemoveListener(OnNotificationsClick);
            _refreshBtn.onClick.RemoveListener(OnRefreshClick);
            _finalizeBtn.onClick.RemoveListener(OnFinalizeClick);
            _deleteTourBtn.onClick.RemoveListener(OnDeleteTourClick);

            _validateMoveBtn.onClick.RemoveListener(OnValidateMoveClick);
            _cancelMoveBtn.onClick.RemoveListener(OnCancelMoveClick);

            _exitPreviewTourBtn.onClick.RemoveListener(OnExitPreviewTourClick);
            _previewTourSlider.onValueChanged.RemoveListener(OnPreviewTourSlide);

            InputLocationData.onLocationData -= OnLocationData;
        }

        private void OnTourStartClick()
        {
            ActivateTourGPS(true);
        }

        public void ActivateTourGPS(bool newPath)
		{
            _topButtons.SetActive(false);
            _tourStartBtn.gameObject.SetActive(false);
            _tourStopBtn.gameObject.SetActive(true);
            _notifBtn.gameObject.SetActive(true);
            _finalizeBtn.gameObject.SetActive(true);
            _deleteTourBtn.gameObject.SetActive(true);
            _editStartBtn.interactable = false;
            _notifStartBtn.interactable = false;
            _refreshBtn.interactable = false;
            onUIEvent?.Invoke(UIEvent.MODE_GPS);
            onUIEvent?.Invoke(UIEvent.CENTER_GPS);
            if (newPath)
                onUIEvent?.Invoke(UIEvent.START_PATH);
        }

        public void DeactivateTourGPS()
        {
            _topButtons.SetActive(true);
            _tourStartBtn.gameObject.SetActive(true);
            _tourStopBtn.gameObject.SetActive(false);
            _notifBtn.gameObject.SetActive(false);
            _finalizeBtn.gameObject.SetActive(false);
            _deleteTourBtn.gameObject.SetActive(false);
            _editStartBtn.interactable = true;
            _notifStartBtn.interactable = true;
            _refreshBtn.interactable = true;
            onUIEvent?.Invoke(UIEvent.STOP_PATH);
            onUIEvent?.Invoke(UIEvent.MODE_NONE);
        }

        private void OnTourStopClick()
        {
            if (ApplicationManager.instance.tour == null)
			{
                DeactivateTourGPS();
                return;
            }

            if (ApplicationManager.instance.currentLine.pointCount < 2)
			{
                ShowFeedbackPopup(L.Get(TextManager.TOURS_CREATION_TITLE), L.Get(TextManager.TOURS_CREATION_NOT_ENOUGH_POINTS_GPS));
                return;
            }

            ShowChoicePopup(L.Get(TextManager.TOURS_CREATION_TITLE), L.Get(TextManager.TOURS_CREATION_FINALIZE),
                   L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO), (bool result) =>
                   {
                       if (result)
					   {
                           DeactivateTourGPS();
                       }
                   });
        }        

        private void OnEditStartClick()
        {
            ActivateTourEdition(true);
        }

        public void ActivateTourEdition(bool newPath)
		{
            _topButtons.SetActive(false);
            _editStartBtn.gameObject.SetActive(false);
            _editStopBtn.gameObject.SetActive(true);
            _notifBtn.gameObject.SetActive(true);
            _finalizeBtn.gameObject.SetActive(true);
            _deleteTourBtn.gameObject.SetActive(true);
            _tourStartBtn.interactable = false;
            _notifStartBtn.interactable = false;
            _refreshBtn.interactable = false;
            onUIEvent?.Invoke(UIEvent.MODE_EDIT);
            if (newPath)
                onUIEvent?.Invoke(UIEvent.START_PATH);
        }

        public void DeactivateTourEdition()
        {
            _topButtons.SetActive(true);
            _editStartBtn.gameObject.SetActive(true);
            _editStopBtn.gameObject.SetActive(false);
            _notifBtn.gameObject.SetActive(false);
            _finalizeBtn.gameObject.SetActive(false);
            _deleteTourBtn.gameObject.SetActive(false);
            _tourStartBtn.interactable = true;
            _notifStartBtn.interactable = true;
            _refreshBtn.interactable = true;
            onUIEvent?.Invoke(UIEvent.STOP_PATH);
            onUIEvent?.Invoke(UIEvent.MODE_NONE);
        }

        private void OnEditStopClick()
        {
            if (ApplicationManager.instance.tour == null)
			{
                DeactivateTourEdition();
                return;
            }

            if (ApplicationManager.instance.currentLine.pointCount < 2)
            {
                ShowFeedbackPopup(L.Get(TextManager.TOURS_CREATION_TITLE), L.Get(TextManager.TOURS_CREATION_NOT_ENOUGH_POINTS_MANUAL));
                return;
            }

            ShowChoicePopup(L.Get(TextManager.TOURS_CREATION_TITLE), L.Get(TextManager.TOURS_CREATION_FINALIZE),
                   L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO), (bool result) =>
                   {
                       if (result)
                       {
                           DeactivateTourEdition();
                       }
                   });
        }

        private void OnNotifStartClick()
        {
            _notifStartBtn.gameObject.SetActive(false);
            _notifStopBtn.gameObject.SetActive(true);
            _notifBtn.gameObject.SetActive(false);
            _finalizeBtn.gameObject.SetActive(false);
            _deleteTourBtn.gameObject.SetActive(false);
            _tourStartBtn.interactable = false;
            _editStartBtn.interactable = false;
            _refreshBtn.interactable = false;
            onUIEvent?.Invoke(UIEvent.MODE_POINT);
        }

        private void OnNotifStopClick()
        {
            _notifStartBtn.gameObject.SetActive(true);
            _notifStopBtn.gameObject.SetActive(false);
            _notifBtn.gameObject.SetActive(false);
            _finalizeBtn.gameObject.SetActive(false);
            _deleteTourBtn.gameObject.SetActive(false);
            _tourStartBtn.interactable = true;
            _editStartBtn.interactable = true;
            _refreshBtn.interactable = true;
            onUIEvent?.Invoke(UIEvent.MODE_NONE);
        }

        private void OnNotifClick()
        {
            ShowPlaceTypePopup(ApplicationManager.instance.tour);
        }

        private void OnUserClick()
        {
            ShowLoginPopup();

            // kTBS
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:seeMyProfile"},
                {"m:userId", ApplicationManager.instance.user?.id}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            ///

        }

        private void OnMapClick()
        {
            switch (ApplicationManager.instance.mapType)
            {
                case ApplicationManager.MapType.STREET:
                    ApplicationManager.instance.SetMapType(ApplicationManager.MapType.SATELLITE);
                    break;
                case ApplicationManager.MapType.SATELLITE:
                    ApplicationManager.instance.SetMapType(ApplicationManager.MapType.STREET);
                    break;
            }
        }

        private void OnVisibilityClick()
        {
            ShowVisibilityPopup(ApplicationManager.instance.user);
        }

        private void OnFilterClick()
        {
            ShowFilterPopup();
        }

        private void OnFavoritesClick()
		{
            List<ControllerTour> tours = new List<ControllerTour>();
            List<ControllerAnnotation> annotations = new List<ControllerAnnotation>();
            int[] tourArray = ApplicationManager.instance.favoritesManager.tourArray;
            if (tourArray != null)
			{
                foreach (int tourId in tourArray)
				{
                    ControllerTour tour = ApplicationManager.instance.dataManager.GetTourFromId(tourId.ToString());
                    if (tour != null)
                        tours.Add(tour);
                }
			}
            int[] annotationArray = ApplicationManager.instance.favoritesManager.annotationArray;
            if (annotationArray != null)
            {
                foreach (int annotationId in annotationArray)
                {
                    ControllerAnnotation annotation = ApplicationManager.instance.dataManager.GetAnnotationFromId(annotationId.ToString());
                    if (annotation != null)
                        annotations.Add(annotation);
                }
            }
            ShowFavoritesPopup(tours, annotations);

            // KTBS2
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:showFavorites"},
                {"m:userId", ApplicationManager.instance.user.id}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            
		}

        private void OnGpsSearchClick()
        {
            onUIEvent?.Invoke(UIEvent.CENTER_GPS);
        }

        private void OnAddressClick()
		{
            ShowSearchAddressPopup();
            // KTBS2
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:showAddressSearch"},
                {"m:userId", ApplicationManager.instance.user.id}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            
		}

        private void OnNotificationsClick()
		{
            ShowNotificationsPopup();
            // KTBS2
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:showNotifications"},
                {"m:userId", ApplicationManager.instance.user.id}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            
		}

        private void OnRefreshClick()
		{
            ApplicationManager.instance.RefreshAllData(true);
            // KTBS2
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:refresh"},
                {"m:userId", ApplicationManager.instance.user.id}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            
        }

        private void OnFinalizeClick()
		{
            if (ApplicationManager.instance.mode == ApplicationManager.Mode.GPS)
                OnTourStopClick();
            if (ApplicationManager.instance.mode == ApplicationManager.Mode.EDIT)
                OnEditStopClick();
        }

        private void OnDeleteTourClick()
		{
            if (ApplicationManager.instance.tour != null)
                ShowEditTourPopup(ApplicationManager.instance.tour, openToDelete: true, onDeleteResult: OnDeleteTourResult);
            else
                OnDeleteTourResult(true);
        }

        private void OnDeleteTourResult(bool result)
		{
            if (result)
			{
                ApplicationManager.instance.CancelTourCreation();
                if (ApplicationManager.instance.mode == ApplicationManager.Mode.GPS)
                    DeactivateTourGPS();
                if (ApplicationManager.instance.mode == ApplicationManager.Mode.EDIT)
                    DeactivateTourEdition();
            }
		}

        private void OnValidateMoveClick()
        {
            if (ApplicationManager.instance.currentLine.pointCount < 2)
            {
                ShowFeedbackPopup(L.Get(TextManager.TOURS_CREATION_TITLE), L.Get(TextManager.TOURS_CREATION_NOT_ENOUGH_POINTS_MANUAL));
                return;
            }
            onUIEvent?.Invoke(UIEvent.MOVE_VALIDATE);
        }

        private void OnCancelMoveClick()
        {
            onUIEvent?.Invoke(UIEvent.MOVE_CANCEL);
        }

        private void OnExitPreviewTourClick()
		{
            onUIEvent?.Invoke(UIEvent.EXIT_PREVIEW_TOUR);
        }

        private void OnPreviewTourSlide(float slide)
		{
            ApplicationManager.instance.SetMapOnCurrentPathLine(slide);
		}

        private void OnLocationData(float longitude, float latitude, float altitude)
        {
            if (!_userPosition.gameObject.activeInHierarchy)
            {
                _userPosition.gameObject.SetActive(true);
                _userPosition.SetTargetCoords(longitude, latitude, true);
            }
            else
            {
                _userPosition.SetTargetCoords(longitude, latitude);
            }
        }

        private void OnUserLogin(ControllerUser user)
        {
            UpdateUserSprite(true);
            _tourStartBtn.interactable = true;
            _editStartBtn.interactable = true;
            _notifStartBtn.interactable = true;
            _visibilityBtn.interactable = true;
            _filterBtn.interactable = true;
            _favoritesBtn.interactable = true;
            _notificationsBtn.interactable = true;
            ShowGpsSearchButton(true);

            // kTBS
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:Login"},
                {"m:userId", ApplicationManager.instance.user.id}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            ///
        }

        private void OnUserLogout(ControllerUser user)
        {
            UpdateUserSprite(false);
            _tourStartBtn.gameObject.SetActive(true);
            _tourStartBtn.interactable = false;
            _tourStopBtn.gameObject.SetActive(false);
            _notifBtn.gameObject.SetActive(false);
            _editStartBtn.gameObject.SetActive(true);
            _editStartBtn.interactable = false;
            _editStopBtn.gameObject.SetActive(false);
            _notifStartBtn.gameObject.SetActive(true);
            _notifStartBtn.interactable = false;
            _notifStopBtn.gameObject.SetActive(false);
            _visibilityBtn.interactable = false;
            _filterBtn.interactable = false;
            ShowGpsSearchButton(false);

            // /////////////// KTBS ////////////////////////
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:Logout"},
                {"m:userId", user.id}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            ///
        }

        private void UpdateUserSprite(bool on)
        {
            _userIcon.sprite = on ? _userOnSprite : _userOffSprite;
        }
    }
}
