namespace Liris.App
{
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
    using System.Collections.Generic;
    using UnityEngine;

    public class PathLine : MonoBehaviour
    {
        public string tourId => _tourId;
        public string tourBody => _tourBody;
        public bool hasPoints => pointCount > 0;
        public int pointCount => _points != null ? _points.Count : 0;
        public List<Vector2> points => _points;
        public Vector3[] positions => _positions;
        public bool moveEdition => _moveEdition;

        public List<string> linkedAnnotations => _linkedAnnotations;

        [SerializeField]
        private LineRenderer _line = null;
        [SerializeField]
        private SpriteRenderer _startPoint = null;
        [SerializeField]
        private SpriteRenderer _endPoint = null;
        [SerializeField]
        private SpriteRenderer _startPointDir = null;
        [SerializeField]
        private SpriteRenderer _endPointDir = null;
        [SerializeField]
        private MeshCollider _meshCol = null;

        private List<Vector2> _points = null;
        private Vector3[] _positions = null;
        private OnlineMapsTileSetControl _mapControl = null;
        private string _tourId = null;
        private string _tourBody = null;
        private Mesh _mesh = null;
        private Camera _cam = null;
        private bool _init = false;
        private float _distance = 0f;
        private bool _moveEdition = false;
        private bool _filtered = false;
        private Rect _viewportRect;
        private List<string> _linkedAnnotations = null;

        public void Init(OnlineMapsTileSetControl control, float size = 15f)
        {
            _mapControl = control;
            _line.startWidth = size;
            _line.endWidth = size;
            Color color = new Color(Random.Range(0.7f, 1f), Random.Range(0.7f, 1f), Random.Range(0.7f, 1f));
            _line.startColor = color;
            _line.endColor = color;
            _startPoint.gameObject.SetActive(false);
            _startPoint.color = color;
            _endPoint.gameObject.SetActive(false);
            _endPoint.color = color;
            _mesh = new Mesh();
            _cam = GetComponentInParent<Camera>();
            _viewportRect = new Rect(Vector2.zero, new Vector2(1024f * (float)Screen.width / (float)Screen.height, 1024f));
            _viewportRect.position = -_viewportRect.center;
            NeedToRecomputeDistance();
        }

        public void InitFromTour(ControllerTour tour)
        {
            _tourBody = tour.body;
            _tourId = tour.id;
            _linkedAnnotations = InitLinksFromJson(tour.linkedAnnotations);
            InitFromBody(_tourBody);
        }

        public void UpdateBodyFromPoints()
		{
            _tourBody = GetPositionDataAsString();
		}

        public void SetLinkedAnnotations(List<string> linkedAnnotations)
        {
            _linkedAnnotations = linkedAnnotations;
        }

        public void ResetLineFromBody()
		{
            InitFromBody(_tourBody);
        }

		public void InitFromBody(string body)
		{
            if (body != null)
			{
                _points = null;
                List<object> list = JSON.Deserialize(body) as List<object>;
                int count = list.Count / 2;
                for (int i = 0; i < count; ++i)
                {
                    float x = ConvertTools.ToFloat(list[i * 2 + 0]);
                    float y = ConvertTools.ToFloat(list[i * 2 + 1]);
                    AddPoint(x, y);
                }
            }
		}

        public List<string> InitLinksFromJson(string json)
		{
            List<string> links = new List<string>();
            if (json != null)
			{
                List<object> list = JSON.Deserialize(json) as List<object>;
                if (list != null)
				{
                    foreach (object o in list)
                    {
                        string id = ConvertTools.ToString(o);
                        if (!string.IsNullOrEmpty(id))
                            links.Add(id);
                    }
                }
            }
            return links;   
        }

        public string GetJsonFromLinkedAnnotations()
        {
            if (linkedAnnotations != null && linkedAnnotations.Count > 0)
                return JSON.Serialize(linkedAnnotations);
            else
                return null;
        }

        public void UpdateTourId(string tourId)
		{
            _tourId = tourId;
        }

        private void OnDestroy()
        {
            _line = null;
            _points = null;
            _positions = null;
            _mapControl = null;
        }

        public bool AddPoint(float x, float y)
        {
            if (_points == null)
                _points = new List<Vector2>();
            Vector2 point = new Vector2(x, y);
            int count = _points.Count;
            if (count == 0 || IsDistanceValid(_points[count - 1], point))
            {
                _points.Add(point);
                count++;
                _positions = new Vector3[count];
                _line.positionCount = count;
                UpdateExtremities();
                NeedToRecomputeDistance();
                return true;
            }
            return false;
        }

        public bool InsertPoint(float x, float y, out Vector2 nearestPos, out int index)
        {
            nearestPos = new Vector2(x, y);
            index = -1;
            int count = pointCount;
            if (count > 1)
            {
                for (int i= 0; i < count; ++i)
				{
                    if (!IsDistanceValid(nearestPos, _points[i]))
                        return false;
				}
                nearestPos = ComputeNearestPointOnLine(nearestPos, out int firstIdx, out float ratio);
                if (firstIdx >= 0)
				{
                    index = firstIdx + 1;
                    _points.Insert(index, nearestPos);
                    count++;
                    _positions = new Vector3[count];
                    _line.positionCount = count;
                    NeedToRecomputeDistance();
                    return true;
                }
            }
            return false;
        }

        public bool DeletePoint(int index)
		{
            int count = pointCount;
            if (index >= 0 && index < count)
			{
                _points.RemoveAt(index);
                count--;
                _positions = new Vector3[count];
                _line.positionCount = count;
                NeedToRecomputeDistance();
                UpdateExtremities();
                return true;
            }
            return false;
        }

        public Vector2 GetPositionAtRatio(float ratio)
		{
            if (_points != null && _points.Count > 0)
			{
                float target = _distance * ratio;
                float dist = 0f;
                float segmentDist = 0f;
                Vector2 pos, lastPos = Vector2.zero;
                int count = _points.Count;
                for (int i = 0; i < count; ++i)
                {
                    pos = _points[i];
                    if (i > 0)
                    {
                        segmentDist = (pos - lastPos).magnitude;
                        dist += segmentDist;
                        if (dist >= target)
                        {
                            ratio = (target - dist + segmentDist) / segmentDist;
                            return Vector2.Lerp(lastPos, pos, ratio);
                        }
                    }
                    lastPos = pos;
                }
                return _points[0];
            }
            return Vector2.zero;
        }

        public void UpdateExtremities()
		{
            int count = pointCount;
            if (count > 1)
			{
                _startPointDir.gameObject.SetActive(true);
                _startPoint.gameObject.SetActive(true);
                _endPointDir.gameObject.SetActive(true);
                _endPoint.gameObject.SetActive(true);
                _startPoint.transform.localRotation = GetRotationFromPoints(_points[0], _points[1]);
			}
            else
			{
                _startPointDir.gameObject.SetActive(false);
                _startPoint.gameObject.SetActive(count == 1);
                _endPointDir.gameObject.SetActive(false);
                _endPoint.gameObject.SetActive(false);
            }
		}

        public Vector2 ComputeNearestPointOnLine(Vector2 pos, out int firstIdx, out float ratio)
		{
            float minSqrDist = float.MaxValue;
            firstIdx = -1;
            ratio = 0f;
            Vector2 result = pos;
            int count = pointCount;
            if (count > 1)
            {
                List<Vector2> points = _points;
                for (int i = 0; i < count - 1; ++i)
                {
                    Vector2 pointA = points[i];
                    Vector2 pointB = points[i + 1];
                    Vector2 dir = pointB - pointA;
                    float sqrMag = dir.sqrMagnitude;
                    if (sqrMag == 0f)
                        continue;
                    float magnitude = dir.magnitude;
                    dir /= magnitude;
                    float dot = Vector2.Dot(pos - pointA, dir);
                    dot = Mathf.Clamp(dot, 0f, magnitude);
                    Vector2 pointH = pointA + dir * dot;
                    float sqrDist = (pos - pointH).sqrMagnitude;
                    if (sqrDist < minSqrDist)
                    {
                        minSqrDist = sqrDist;
                        firstIdx = i;
                        ratio = dot / magnitude;
                        result = pointH;
                    }
                }
            }
            return result;
        }

        private Quaternion GetRotationFromPoints(Vector2 a, Vector2 b)
		{
            return Quaternion.FromToRotation(Vector3.up, ComputePositionFromPoint(b)  - ComputePositionFromPoint(a));
		}

        public bool IsDistanceValid(Vector2 a, Vector2 b)
        {
            return ((a - b) * 10000f).sqrMagnitude > 1f;
        }

        public void NeedToRecomputeDistance()
		{
            _init = false;
        }

        public void RenderLine(bool visible, bool testSize, float size, float sizeMin)
        {
            if (_positions != null)
            {
                bool show = visible && !_filtered;

                if (!show)
				{
                    _line.enabled = false;
                    _startPoint.gameObject.SetActive(false);
                    _endPoint.gameObject.SetActive(false);
                    return;
                }

                int posCount = _line.positionCount;
                bool canShowLine = (!testSize || size >= sizeMin) && posCount > 1;                
                _line.enabled = canShowLine;
                _startPoint.gameObject.SetActive(true);
                _endPoint.gameObject.SetActive(canShowLine);

                float sizeRatio = 3f + Mathf.Max(size - sizeMin, 0f);                
                float lineSize = 2f + sizeRatio * 2f;
                float pointSize = lineSize * 0.8f;

                if (!canShowLine)
				{
                    _startPoint.transform.localPosition = ComputePositionFromPoint(_points[0]);
                    _startPoint.transform.localScale = Vector3.one * pointSize;
                    return;
                }

                float shaderAmount = 5f + sizeRatio * sizeRatio;
                float distanceRatio = Mathf.Clamp(_distance * 100f, 0.01f, 100f);

                int count = _line.GetPositions(_positions);
                bool isVisible = false;
                Vector2 pos, lastPos = Vector2.zero;
                for (int i = 0; i < count; ++i)
				{
                    pos = ComputePositionFromPoint(_points[i]);
                    if (!isVisible)
                    {
                        if (_viewportRect.Contains(pos))
                            isVisible = true;
                        else if (i > 0 && Intersects(lastPos, pos, _viewportRect))
                            isVisible = true;
                    }
                    lastPos = pos;
                    _positions[i] = pos;
                }

                if (!isVisible)
				{
                    _line.enabled = false;
					_startPoint.gameObject.SetActive(false);
					_endPoint.gameObject.SetActive(false);
					return;
                }           

                if (!_init)
				{
                    _distance = 0f;
                    
                    for (int i = 0; i < count; ++i)
                    {
                        pos = _points[i];
                        if (i > 0)
                            _distance += (pos - lastPos).magnitude;
                        lastPos = pos;
                    }
                }

                _line.startWidth = lineSize;
                _line.endWidth = lineSize;
                _line.materials[0].SetFloat("_Amount", shaderAmount * distanceRatio);
                _line.SetPositions(_positions);

                if (count > 0)
                {
                    _startPoint.transform.localPosition = _positions[0];
                    _startPoint.transform.localScale = Vector3.one * pointSize;
                }
                
                if (count > 1)
                {
                    _endPoint.transform.localPosition = _positions[count - 1];
                    _endPoint.transform.localScale = Vector3.one * pointSize;
                    if (_init)
                        UpdateCollider();
                }
                _init = true;
            }
        }

        static bool Intersects(Vector2 a1, Vector2 a2, Rect rect)
        {
            if (Intersects(a1, a2, new Vector2(rect.xMin, rect.yMin), new Vector2(rect.xMin, rect.yMax)))
                return true;
            if (Intersects(a1, a2, new Vector2(rect.xMin, rect.yMax), new Vector2(rect.xMax, rect.yMax)))
                return true;
            if (Intersects(a1, a2, new Vector2(rect.xMax, rect.yMax), new Vector2(rect.xMax, rect.yMin)))
                return true;
            if (Intersects(a1, a2, new Vector2(rect.xMax, rect.yMin), new Vector2(rect.xMin, rect.yMin)))
                return true;
            return false;
        }

        static bool Intersects(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2)
        {
            Vector2 b = a2 - a1;
            Vector2 d = b2 - b1;
            float bDotDPerp = b.x * d.y - b.y * d.x;

            // if b dot d == 0, it means the lines are parallel so have infinite intersection points
            if (bDotDPerp == 0)
                return false;

            Vector2 c = b1 - a1;
            float t = (c.x * d.y - c.y * d.x) / bDotDPerp;
            if (t < 0 || t > 1)
                return false;

            float u = (c.x * b.y - c.y * b.x) / bDotDPerp;
            if (u < 0 || u > 1)
                return false;

            return true;
        }

        public void ActivateMoveEdition(bool activate)
		{
            _moveEdition = activate;
            if (activate)
                UpdateBodyFromPoints();
        }

        public void SetFiltered(bool filtered)
		{
            _filtered = filtered;
        }

        private void UpdateCollider()
        {
            _line.BakeMesh(_mesh, _cam, true);
            _meshCol.sharedMesh = _mesh;
        }

        public string GetPositionDataAsString()
        {
            if (_points != null && _points.Count > 0)
            {
                List<object> list = new List<object>();
                foreach (Vector2 point in _points)
                {
                    list.Add(point.x);
                    list.Add(point.y);
                }
                return JSON.Serialize(list);
            }
            return "[]";
        }

        private Vector3 ComputePositionFromPoint(Vector2 point)
        {
            Vector2 pos = _mapControl.GetPosition(point);
            return new Vector3(pos.x - 512f, 512f - pos.y, 0f);
        }
    }
}
