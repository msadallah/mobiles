namespace Liris.App.Assets
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpritesList", order = 1)]
    public class SpritesList : ScriptableObject
    {
        [System.Serializable]
        public class SpriteData
        {
            public string name;
            public Sprite sprite;
            public Color color;
            public string text;
        }

        public string textIdBase = null;
        public SpriteData[] data;

        public SpriteData GetData(string name)
        {
            int count = data.Length;
            for (int i = 0; i < count; ++i)
            {
                SpriteData sd = data[i];
                if (sd.name == name)
                    return sd;
            }
            return null;
        }

        public SpriteData GetDataFromIndex(int i)
        {
            if (i >= 0 && i < data.Length)
                return data[i];
            return null;
        }

        public int GetIndexFromName(string name)
        {
            int count = data.Length;
            for (int i = 0; i < count; ++i)
            {
                SpriteData sd = data[i];
                if (sd.name == name)
                    return i;
            }
            return -1;
        }

        public string GetNameFromIndex(int i)
        {
            if (i >= 0 && i < data.Length)
                return data[i].name;
            return null;
        }

        public string GetTextId(int i)
        {
            return textIdBase + data[i].name;
        }

        public string GetTextIdFromName(string name)
        {
            return GetTextIdFromData(GetData(name));
        }

        public string GetTextIdFromData(SpriteData data)
        {
            if (data != null)
                return textIdBase + data.name;
            return null;
        }

        public SpriteData GetDataFromText(string text, System.Func<string, string> localizedFunc = null)
        {
            int count = data.Length;
            for (int i = 0; i < count; ++i)
            {
                SpriteData sd = data[i];
                if (localizedFunc != null)
				{
                    if (localizedFunc(GetTextId(i)) == text)
                        return sd;
                }
                else
				{
                    if (sd.text == text)
                        return sd;
                }                
            }
            return null;
        }

        public string GetFirstName()
        {
            return data[0].name;
        }
    }
}
