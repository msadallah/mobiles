namespace Liris.App.Assets
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/TagsList", order = 1)]
    public class TagsList : ScriptableObject
    {
        [System.Serializable]
        public class TagData
        {
            public string name;
            public string text;
        }

        public TagData[] data;

        public TagData GetData(string name)
        {
            int count = data.Length;
            for (int i = 0; i < count; ++i)
            {
                TagData sd = data[i];
                if (sd.name == name)
                    return sd;
            }
            return null;
        }

        public TagData GetDataFromText(string text, System.Func<string, string> localizedFunc = null)
		{
            int count = data.Length;
            for (int i = 0; i < count; ++i)
            {
                TagData sd = data[i];
                if (localizedFunc != null)
				{
                    if (localizedFunc(sd.text) == text)
                        return sd;
                }
                else
				{
                    if (sd.text == text)
                        return sd;
                }
            }
            return null;
        }
    }
}
