namespace Liris.App.Address
{
	using UnityEngine;
	using UnityEngine.UI;
	using TMPro;
    using System.Collections.Generic;
    using Liris.App.Trace;

    public class AddressItem : MonoBehaviour
	{
		[SerializeField]
		private Button _button = null;
		[SerializeField]
		private TextMeshProUGUI _addressText = null; 

		private System.Action<AddressItem> _onClick = null;
		private float _longitude = 0f;
		private float _latitude = 0f;

		private void Awake()
		{
			_button.onClick.AddListener(OnButtonClicked);
		}

		private void OnDestroy()
		{
			_button.onClick.RemoveListener(OnButtonClicked);
			_onClick = null;
		}

		public void Init(string address, float lon, float lat, System.Action<AddressItem> onClick = null)
		{
			_addressText.text = address;
			_longitude = lon;
			_latitude = lat;
			_onClick = onClick;
		}

		private void OnButtonClicked()
		{
			ApplicationManager.instance.SetMapOnPosition(_longitude, _latitude, 16);
			_onClick?.Invoke(this);

			// KTBS2
			Dictionary<string, object> trace = new Dictionary<string, object>()
			{
				{"@type", "m:selectSearchResult"},
				{"m:userId", ApplicationManager.instance.user.id},
				{"m:coordinates", new Dictionary<string, double>(){ {"lon", _longitude}, {"lat", _latitude} }}
			};
			TraceManager.DispatchEventTraceToManager(trace);
            
		}

	}
}
