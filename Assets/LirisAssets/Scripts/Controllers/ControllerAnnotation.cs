namespace Liris.App.Controllers
{
    using AllMyScripts.Common.Tools;
	using Liris.App.Data;
	using System.Collections.Generic;
	using UnityEngine;

	public class ControllerAnnotation : ControllerBase
    {
        private const string USER_FIELD = "/api/users/";
        private const string TOUR_FIELD = "/api/tours/";
        private const string MEDIA_FIELD = "/api/media_objects/";

        public string id;
        public string placeType;
        public string icons;
        public string emoticon;
        public string tags;
        public string coords;
        public string comment;
        public string date;
        public string dateModif;
        public string dateDisplay;
        public string imageId;
        public string userId;
        public string share;
        public string tourId;
        public string timing;
        public Texture2D tex;

        public ControllerAnnotation(Dictionary<string, object> dic)
        {
            InitFromDic(dic);
        }

        public ControllerAnnotation(string json)
        {
            if (!string.IsNullOrEmpty(json))
			{
                InitFromDic(JSON.Deserialize(json) as Dictionary<string, object>);
            }
        }

        public Dictionary<string, object> GetDataAsDic()
		{
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["id"] = id;            
            dic["image"] = imageId;
            dic["placeType"] = placeType;
            dic["placeIcon"] = icons;
            dic["emoticon"] = emoticon;
            dic["tags"] = tags;
            dic["coords"] = coords;
            dic["comment"] = comment;
            dic["date"] = date;
            dic["dateModif"] = dateModif;
            dic["dateDisplay"] = dateDisplay;
            dic["user"] = userId;
            dic["tour"] = tourId;
            dic["share"] = share;
            dic["timing"] = timing;
            return dic;
        }

        public string GetDataAsJson()
		{
            return JSON.Serialize(GetDataAsDic());
		}

        public ControllerUser GetUser()
        {
            DataManager dataMgr = ApplicationManager.instance.dataManager;
            return dataMgr.GetUserFromId(userId);
        }

        public string GetFormattedDate()
        {
            return ConvertDateDayHourMinute(dateDisplay);
        }

        public bool CompareTo(ControllerAnnotation annotation)
		{
            if (id != annotation.id ||
                imageId != annotation.imageId ||
                placeType != annotation.placeType ||
                icons != annotation.icons ||
                emoticon != annotation.emoticon ||
                tags != annotation.tags ||
                coords != annotation.coords ||
                comment != annotation.comment ||
                dateDisplay != annotation.dateDisplay ||
                userId != annotation.userId ||
                tourId != annotation.tourId ||
                share != annotation.share ||
                timing != annotation.timing
                )
                return false;
            return true;
		}

        private void InitFromDic(Dictionary<string, object> dic)
        {
            if (dic != null)
			{
                id = DicTools.GetValueString(dic, "id");
                tex = null;
                if (dic.ContainsKey("tex"))
                    tex = dic["tex"] as Texture2D;
                imageId = DicTools.GetValueString(dic, "image");
                placeType = DicTools.GetValueString(dic, "placeType");
                icons = DicTools.GetValueString(dic, "placeIcon");
                emoticon = DicTools.GetValueString(dic, "emoticon");
                tags = DicTools.GetValueString(dic, "tags");
                coords = DicTools.GetValueString(dic, "coords");
                comment = DicTools.GetValueString(dic, "comment");
                date = DicTools.GetValueString(dic, "date");
                dateModif = DicTools.GetValueString(dic, "dateModif");
                dateDisplay = DicTools.GetValueString(dic, "dateDisplay");
                userId = DicTools.GetValueString(dic, "user");
                tourId = DicTools.GetValueString(dic, "tour");
                share = DicTools.GetValueString(dic, "share");
                timing = DicTools.GetValueString(dic, "timing");

                if (!string.IsNullOrEmpty(userId))
                {
                    if (userId.Contains(USER_FIELD))
                        userId = userId.Substring(USER_FIELD.Length);
                }
                else
                {
                    userId = null;
                }

                if (!string.IsNullOrEmpty(tourId))
                {
                    if (tourId.Contains(TOUR_FIELD))
                        tourId = tourId.Substring(TOUR_FIELD.Length);
                }
                else
                {
                    tourId = null;
                }

                if (!string.IsNullOrEmpty(imageId))
                {
                    if (imageId.Contains(MEDIA_FIELD))
                        imageId = imageId.Substring(MEDIA_FIELD.Length);
                }
                else
                {
                    imageId = null;
                }

                if (string.IsNullOrEmpty(dateDisplay) || ConvertInDateTime(dateDisplay).Year < 2020)
                    dateDisplay = date;
                if (string.IsNullOrEmpty(dateModif) || ConvertInDateTime(dateModif).Year < 2020)
                    dateModif = date;
            }
        }
    }
}
