namespace Liris.App.Controllers
{
    using AllMyScripts.Common.Tools;
	using Liris.App.Data;
	using System.Collections.Generic;
	using UnityEngine;

	public class ControllerTour : ControllerBase
    {
        private const string USER_FIELD = "/api/users/";

        public Vector2[] points => _points;
        public int pointCount => _points != null ? _points.Length : 0;

        public string id;
        public string name;
        public string body;
        public string userId;
        public string share;
        public string linkedAnnotations;
        public string linkedTours;
        public string date;
        public string dateModif;
        public string dateDisplay;
        public bool canEdit;

        private Vector2[] _points = null;

        public ControllerTour(Dictionary<string, object> dic)
        {
            InitFromDic(dic);
        }

        public ControllerTour(string json)
        {
            if (!string.IsNullOrEmpty(json))
			{
                InitFromDic(JSON.Deserialize(json) as Dictionary<string, object>);
            }
        }

        public void UpdateBody(string newBody)
		{
            body = newBody;
            ParseBody();
		}

        public Dictionary<string, object> GetDataAsDic()
		{
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["id"] = id;
            dic["name"] = name;
            dic["body"] = body;
            dic["user"] = userId;
            dic["share"] = share;
            dic["linkedAnnotations"] = linkedAnnotations;
            dic["linkedTours"] = linkedTours;
            dic["date"] = date;
            dic["dateModif"] = dateModif;
            dic["dateDisplay"] = dateDisplay;
            dic["canEdit"] = canEdit;
            return dic;
        }

        public string GetDataAsJson()
		{
            return JSON.Serialize(GetDataAsDic());
		}

        public ControllerUser GetUser()
        {
            DataManager dataMgr = ApplicationManager.instance.dataManager;
            return dataMgr.GetUserFromId(userId);
        }

        public string GetFormattedDate()
        {
            return ConvertDateDayHourMinute(dateDisplay);
        }

        public bool CompareTo(ControllerTour tour)
		{
            if (id != tour.id ||
                name != tour.name ||
                body != tour.body ||
                userId != tour.userId ||
                share != tour.share ||
                linkedAnnotations != tour.linkedAnnotations ||
                linkedTours != tour.linkedTours ||
                dateDisplay != tour.dateDisplay ||
                canEdit != tour.canEdit)
                return false;
            return true;
		}

        private void InitFromDic(Dictionary<string, object> dic)
		{
            if (dic != null)
			{
                id = DicTools.GetValueString(dic, "id");
                name = DicTools.GetValueString(dic, "name");
                body = DicTools.GetValueString(dic, "body");
                userId = DicTools.GetValueString(dic, "user");
                share = DicTools.GetValueString(dic, "share");
                linkedAnnotations = DicTools.GetValueString(dic, "linkedAnnotations");
                linkedTours = DicTools.GetValueString(dic, "linkedTours");
                date = DicTools.GetValueString(dic, "date");
                dateModif = DicTools.GetValueString(dic, "dateModif");
                dateDisplay = DicTools.GetValueString(dic, "dateDisplay");
                canEdit = DicTools.GetValueBool(dic, "canEdit");

                if (!string.IsNullOrEmpty(userId))
                {
                    if (userId.Contains(USER_FIELD))
                        userId = userId.Substring(USER_FIELD.Length);
                }
                else
                {
                    userId = null;
                }

                if (string.IsNullOrEmpty(dateDisplay) || ConvertInDateTime(dateDisplay).Year < 2020)
                    dateDisplay = date;
                if (string.IsNullOrEmpty(dateModif) || ConvertInDateTime(dateModif).Year < 2020)
                    dateModif = date;

                ParseBody();
            }
		}

        private void ParseBody()
        {
            if (body != null)
            {
                List<object> list = JSON.Deserialize(body) as List<object>;
                int count = list.Count / 2;
                _points = new Vector2[count];
                for (int i = 0; i < count; ++i)
                {
                    float x = ConvertTools.ToFloat(list[i * 2 + 0]);
                    float y = ConvertTools.ToFloat(list[i * 2 + 1]);
                    _points[i] = new Vector2(x, y);
                }
            }
        }
    }
}
