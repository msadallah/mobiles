namespace Liris.App.Controllers
{
    using AllMyScripts.Common.Tools;
	using Liris.App.Data;
	using System.Collections.Generic;

	public class ControllerEmojis : ControllerBase
    {
        private const string USER_FIELD = "/api/users/";

        public string id;
        public string userId;
        public string annotations;
        public string tours;

        public Dictionary<int, string> annotationDic => _annotationDic;
        public Dictionary<int, string> tourDic => _tourDic;

        private Dictionary<int, string> _annotationDic;
        private Dictionary<int, string> _tourDic;
        

        public ControllerEmojis(Dictionary<string, object> dic)
        {
            InitFromDic(dic);
        }

        public ControllerEmojis(string json)
        {
            if (!string.IsNullOrEmpty(json))
			{
                InitFromDic(JSON.Deserialize(json) as Dictionary<string, object>);
            }
        }

        public Dictionary<string, object> GetDataAsDic()
		{
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["id"] = id;
            dic["user"] = userId;
            dic["annotations"] = annotations;
            dic["tours"] = tours;
            return dic;
        }

        public string GetDataAsJson()
		{
            return JSON.Serialize(GetDataAsDic());
		}

        public bool CompareTo(ControllerFavorites favorites)
		{
            if (id != favorites.id ||
                userId != favorites.userId ||
                annotations != favorites.annotations ||
                tours != favorites.tours)
                return false;
            return true;
		}

        public void UpdateFromDic(Dictionary<string, object> dic)
        {
            if (dic != null)
            {
                annotations = DicTools.GetValueString(dic, "annotations");
                tours = DicTools.GetValueString(dic, "tours");
                InitAnnotationList(annotations);
                InitTourList(tours);
            }
        }

        private void InitAnnotationList(string json)
		{
            _annotationDic = new Dictionary<int, string>();
            Dictionary<string, object> dic = JSON.Deserialize(json) as Dictionary<string, object>;
            if (dic != null)
			{
                foreach (var keyval in dic)
                {
                    _annotationDic.Add(ConvertTools.ToInt32(keyval.Key), ConvertTools.ToString(keyval.Value));
                }
            }
        }

        private void InitTourList(string json)
        {
            _tourDic = new Dictionary<int, string>();
            Dictionary<string, object> dic = JSON.Deserialize(json) as Dictionary<string, object>;
            if (dic != null)
            {
                foreach (var keyval in dic)
                {
                    _tourDic.Add(ConvertTools.ToInt32(keyval.Key), ConvertTools.ToString(keyval.Value));
                }
            }
        }

        private void InitFromDic(Dictionary<string, object> dic)
		{
            if (dic != null)
			{
                id = DicTools.GetValueString(dic, "id");
                userId = DicTools.GetValueString(dic, "user");
                annotations = DicTools.GetValueString(dic, "annotations");
                tours = DicTools.GetValueString(dic, "tours");
                InitAnnotationList(annotations);
                InitTourList(tours);

                if (!string.IsNullOrEmpty(userId))
                {
                    if (userId.Contains(USER_FIELD))
                        userId = userId.Substring(USER_FIELD.Length);
                }
                else
                {
                    userId = null;
                }
            }
		}
    }
}
