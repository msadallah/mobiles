namespace Liris.App.Controllers
{
    using AllMyScripts.Common.Tools;
	using System.Collections.Generic;
	using UnityEngine;

	public class ControllerNotification : ControllerBase
    {
        public enum CoordsType
        {
            NONE,
            DIRECT,
            REF_ANNOTATION,
            REF_TOUR,
            URL
        }

        private const string USER_FIELD = "/api/users/";

        public bool isNotRead => string.IsNullOrEmpty(readDate);
        public CoordsType coordsType => _coordsType;
        public Vector2 coords => _coords;
        public string tourId => _tourId;
        public string annotationId => _annotationId;
        public string url => _url;
        public float zoom => _zoom;
        public bool showRef => _showRef;
        public bool openAtLogin => _openAtLogin;

        public string id;
        public string userId;
        public string title;
        public string content;
        public string parameters;
        public string category;        
        public string date;
        public string readDate;
        public string expiredDate;

        private CoordsType _coordsType = CoordsType.NONE;
        private Vector2 _coords = Vector2.zero;
        private string _tourId = null;
        private string _annotationId = null;
        private string _url = null;
        private float _zoom = 19.9f;
        private bool _showRef = false;
        private bool _openAtLogin = false;

        public ControllerNotification(Dictionary<string, object> dic)
        {
            InitFromDic(dic);
        }

        public ControllerNotification(string json)
        {
            if (!string.IsNullOrEmpty(json))
			{
                InitFromDic(JSON.Deserialize(json) as Dictionary<string, object>);
            }
        }

        public Dictionary<string, object> GetDataAsDic()
		{
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["id"] = id;
            dic["user"] = userId;
            dic["title"] = title;
            dic["content"] = content;
            dic["parameters"] = parameters;
            dic["category"] = category;
            dic["date"] = date;
            dic["readDate"] = readDate;
            dic["expiredDate"] = expiredDate;
            return dic;
        }

        public string GetDataAsJson()
		{
            return JSON.Serialize(GetDataAsDic());
		}

        public bool CompareTo(ControllerNotification notif)
		{
            if (id != notif.id ||
                userId != notif.userId ||
                title != notif.title ||
                content != notif.content ||
                parameters != notif.parameters ||
                category != notif.category ||
                date != notif.date ||
                readDate != notif.readDate||
                expiredDate != notif.expiredDate)
                return false;
            return true;
		}

        private void InitFromDic(Dictionary<string, object> dic)
		{
            if (dic != null)
			{
                id = DicTools.GetValueString(dic, "id");
                userId = DicTools.GetValueString(dic, "user");
                title = DicTools.GetValueString(dic, "title");
                content = DicTools.GetValueString(dic, "content");
                parameters = DicTools.GetValueString(dic, "parameters");
                category = DicTools.GetValueString(dic, "category");
                date = DicTools.GetValueString(dic, "date");
                readDate = DicTools.GetValueString(dic, "readDate");
                expiredDate = DicTools.GetValueString(dic, "expiredDate");

                if (!string.IsNullOrEmpty(userId))
                {
                    if (userId.Contains(USER_FIELD))
                        userId = userId.Substring(USER_FIELD.Length);
                }
                else
                {
                    userId = null;
                }
                ParseParameters();
            }
		}

        private void ParseParameters()
        {
            if (parameters != null)
            {
                Dictionary<string, object> dic = JSON.Deserialize(parameters) as Dictionary<string, object>;
                if (dic != null)
				{
                    _coordsType = CoordsType.NONE;
                    if (dic.ContainsKey("lon") && dic.ContainsKey("lat"))
                    {
                        _coords = ApplicationManager.ConvertCoordsToVector2(parameters);
                        _coordsType = CoordsType.DIRECT;
                    }
                    else if (dic.ContainsKey("annotationId"))
                    {
                        _annotationId = DicTools.GetValueString(dic, "annotationId");
                        _coordsType = CoordsType.REF_ANNOTATION;
                    }
                    else if (dic.ContainsKey("tourId"))
                    {
                        _tourId = DicTools.GetValueString(dic, "tourId");
                        _coordsType = CoordsType.REF_TOUR;
                    }
                    else if (dic.ContainsKey("url"))
					{
                        _url = DicTools.GetValueString(dic, "url");
                        _coordsType = CoordsType.URL;
                    }

                    _zoom = DicTools.GetValueFloat(dic, "zoom", 19.9f);
                    _showRef = DicTools.GetValueBool(dic, "showRef");
                    _openAtLogin = DicTools.GetValueBool(dic, "openAtLogin");
                }
            }
        }
    }
}
