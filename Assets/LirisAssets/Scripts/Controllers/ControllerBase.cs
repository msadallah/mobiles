namespace Liris.App.Controllers
{
	using System.Globalization;

	public class ControllerBase
    {
        public static System.DateTime ConvertInDateTime(string date, System.DateTime? defaultValue = null)
        {
            var cultureInfo = new CultureInfo("fr-FR");
            if (System.DateTime.TryParse(date, cultureInfo, DateTimeStyles.AssumeLocal, out System.DateTime dt))
                return dt;
            if (defaultValue.HasValue)
                return defaultValue.Value;
            return System.DateTime.MinValue;
        }

        public static string ConvertDateDay(string date)
        {
            return ConvertInDateTime(date).ToString("dd/MM/yyyy");
        }

        public static string ConvertDateDayHourMinute(string date)
        {
            System.DateTime dateTime = ConvertInDateTime(date);
            if (dateTime.Hour != 0 || dateTime.Minute != 0)
                return dateTime.ToString("dd/MM/yyyy HH:mm");
            else
                return dateTime.ToString("dd/MM/yyyy");
        }

        public static string ConvertDateTimeToSend(System.DateTime dt)
		{
            return dt.ToString("yyyy-MM-ddTHH:mm zzz");
        }
    }
}
