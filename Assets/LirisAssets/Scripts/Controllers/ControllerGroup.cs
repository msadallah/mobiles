namespace Liris.App.Controllers
{
    using AllMyScripts.Common.Tools;
	using System.Collections.Generic;

	public class ControllerGroup : ControllerBase
    {
        public string id;
        public string name;
        public string description;
        public bool admin;

        public ControllerGroup(Dictionary<string, object> dic)
        {
            InitFromDic(dic);
        }

        public ControllerGroup(string json)
        {
            if (!string.IsNullOrEmpty(json))
			{
                InitFromDic(JSON.Deserialize(json) as Dictionary<string, object>);
            }
        }

        public Dictionary<string, object> GetDataAsDic()
		{
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["id"] = id;
            dic["name"] = name;
            dic["description"] = description;
            dic["admin"] = admin;
            return dic;
        }

        public string GetDataAsJson()
		{
            return JSON.Serialize(GetDataAsDic());
		}

        public bool CompareTo(ControllerGroup group)
		{
            if (id != group.id ||
                name != group.name ||
                description != group.description ||
                admin != group.admin)
                return false;
            return true;
		}

        private void InitFromDic(Dictionary<string, object> dic)
		{
            if (dic != null)
			{
                id = DicTools.GetValueString(dic, "id");
                name = DicTools.GetValueString(dic, "name");
                description = DicTools.GetValueString(dic, "description");
                admin = DicTools.GetValueBool(dic, "admin");
            }
		}
    }
}
