namespace Liris.App.Controllers
{
    using AllMyScripts.Common.Tools;
	using Liris.App.Data;
	using System.Collections.Generic;

	public class ControllerFavorites : ControllerBase
    {
        private const string USER_FIELD = "/api/users/";

        public string id;
        public string userId;
        public string annotations;
        public string tours;

        public int[] annotationArray => _annotationList != null ? _annotationList.ToArray() : null;
        public int[] tourArray => _tourList != null ? _tourList.ToArray() : null;

        private List<int> _annotationList;
        private List<int> _tourList;
        

        public ControllerFavorites(Dictionary<string, object> dic)
        {
            InitFromDic(dic);
        }

        public ControllerFavorites(string json)
        {
            if (!string.IsNullOrEmpty(json))
			{
                InitFromDic(JSON.Deserialize(json) as Dictionary<string, object>);
            }
        }

        public Dictionary<string, object> GetDataAsDic()
		{
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["id"] = id;
            dic["user"] = userId;
            dic["annotations"] = annotations;
            dic["tours"] = tours;
            return dic;
        }

        public string GetDataAsJson()
		{
            return JSON.Serialize(GetDataAsDic());
		}

        public bool CompareTo(ControllerFavorites favorites)
		{
            if (id != favorites.id ||
                userId != favorites.userId ||
                annotations != favorites.annotations ||
                tours != favorites.tours)
                return false;
            return true;
		}

        public void UpdateFromDic(Dictionary<string, object> dic)
        {
            if (dic != null)
            {
                annotations = DicTools.GetValueString(dic, "annotations");
                tours = DicTools.GetValueString(dic, "tours");
                InitAnnotationList(annotations);
                InitTourList(tours);
            }
        }

        private void InitAnnotationList(string json)
		{
            _annotationList = new List<int>();
            List<object> list = JSON.Deserialize(json) as List<object>;
            if (list != null)
			{
                foreach (object o in list)
                {
                    _annotationList.Add(ConvertTools.ToInt32(o));
                }
                _annotationList.Sort();
            }
        }

        private void InitTourList(string json)
        {
            _tourList = new List<int>();
            List<object> list = JSON.Deserialize(json) as List<object>;
            if (list != null)
			{
                foreach (object o in list)
                {
                    _tourList.Add(ConvertTools.ToInt32(o));
                }
                _tourList.Sort();
            }
        }

        private void InitFromDic(Dictionary<string, object> dic)
		{
            if (dic != null)
			{
                id = DicTools.GetValueString(dic, "id");
                userId = DicTools.GetValueString(dic, "user");
                annotations = DicTools.GetValueString(dic, "annotations");
                tours = DicTools.GetValueString(dic, "tours");
                InitAnnotationList(annotations);
                InitTourList(tours);

                if (!string.IsNullOrEmpty(userId))
                {
                    if (userId.Contains(USER_FIELD))
                        userId = userId.Substring(USER_FIELD.Length);
                }
                else
                {
                    userId = null;
                }
            }
		}
    }
}
