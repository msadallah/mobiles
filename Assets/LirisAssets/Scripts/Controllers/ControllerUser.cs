namespace Liris.App.Controllers
{
    using AllMyScripts.Common.Tools;
	using Liris.App.WebServices;
	using System.Collections.Generic;
	using UnityEngine;

	public class ControllerUser : ControllerBase
    {
        public enum Gender
        {
            None,
            Male,
            Female,
            Nope
        }

        private const string PP_USER_LAST_MAIL = "PP_USER_LAST_MAIL";
        private const string PP_USER_LAST_PASS = "PP_USER_LAST_PASS";

        public string id;
        public string username;
        public string mail;
        public string lastName;
        public string firstName;
        public string gender;
        public string institution;
        public string formation;
        public string diploma;
        public string nationality;
        public string birthDate;
        public string arrivalDate;
        public string address;
        public string postalcode;
        public string city;
        public string testgroup;
        public string version;
        public bool disclaimer;
        public bool admin;
        public string usergroups;
        public string devices;

        public ControllerUser(Dictionary<string, object> dicUser)
        {
            InitFromDic(dicUser);
        }

        public ControllerUser(string userJson)
        {
            if (!string.IsNullOrEmpty(userJson))
			{
                InitFromDic(JSON.Deserialize(userJson) as Dictionary<string, object>);
            }
        }

        public void InitFromDic(Dictionary<string, object> dicUser)
		{
            if (dicUser != null)
			{
                id = DicTools.GetValueString(dicUser, "id");
                username = DicTools.GetValueString(dicUser, "username");
                mail = DicTools.GetValueString(dicUser, "mail");
                lastName = DicTools.GetValueString(dicUser, "lastname");
                firstName = DicTools.GetValueString(dicUser, "firstname");
                gender = DicTools.GetValueString(dicUser, "gender");
                institution = DicTools.GetValueString(dicUser, "institution");
                formation = DicTools.GetValueString(dicUser, "formation");
                diploma = DicTools.GetValueString(dicUser, "diploma");
                nationality = DicTools.GetValueString(dicUser, "nationality");
                birthDate = DicTools.GetValueString(dicUser, "birthdate");
                arrivalDate = DicTools.GetValueString(dicUser, "arrivaldate");
                address = DicTools.GetValueString(dicUser, "address");
                postalcode = DicTools.GetValueString(dicUser, "postalcode");
                city = DicTools.GetValueString(dicUser, "city");
                testgroup = DicTools.GetValueString(dicUser, "testgroup");
                version = DicTools.GetValueString(dicUser, "version");
                disclaimer = DicTools.GetValueBool(dicUser, "disclaimer");
                admin = DicTools.GetValueBool(dicUser, "admin");
                usergroups = DicTools.GetValueString(dicUser, "usergroups");
                devices = DicTools.GetValueString(dicUser, "devices");
            }            
        }

        public Dictionary<string, object> GetDataAsDic()
		{
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["id"] = id;
            dic["username"] = username;
            dic["mail"] = mail;
            dic["lastname"] = lastName;
            dic["firstname"] = firstName;
            dic["gender"] = gender;
            dic["institution"] = institution;
            dic["formation"] = formation;
            dic["diploma"] = diploma;
            dic["nationality"] = nationality;
            dic["birthdate"] = birthDate;
            dic["arrivaldate"] = arrivalDate;
            dic["address"] = address;
            dic["postalcode"] = postalcode;
            dic["city"] = city;
            dic["testgroup"] = testgroup;
            dic["version"] = version;
            dic["disclaimer"] = disclaimer;
            dic["admin"] = admin;
            dic["usergroups"] = usergroups;
            dic["devices"] = devices;
            return dic;
        }

        public string GetDataAsJson()
        {
            return JSON.Serialize(GetDataAsDic());
        }

        public void UpdateDevices()
		{
            Dictionary<string, object> devicesDic = null;
            if (!string.IsNullOrEmpty(devices))
                devicesDic = JSON.Deserialize(devices) as Dictionary<string, object>;

            Dictionary<string, object> deviceDic = new Dictionary<string, object>();
            deviceDic["platform"] = Application.platform;
            deviceDic["model"] = SystemInfo.deviceModel;
            deviceDic["token"] = ApplicationManager.instance.firebaseToken;
            deviceDic["date"] = WebServiceBase.GetDateNow();

            string deviceId = SystemInfo.deviceUniqueIdentifier;

            if (devicesDic == null)
                devicesDic = new Dictionary<string, object>();

            if (devicesDic.ContainsKey(deviceId))
			{
                devicesDic[deviceId] = deviceDic;
            }
            else
			{
                devicesDic.Add(deviceId, deviceDic);
            }

            devices = JSON.Serialize(devicesDic);
        }

        public static void SaveLoginData(string mail, string pass)
        {
            PlayerPrefs.SetString(PP_USER_LAST_MAIL, mail);
            PlayerPrefs.SetString(PP_USER_LAST_PASS, pass);
        }

        public static void GetLoginData(out string mail, out string pass)
        {
            mail = PlayerPrefs.GetString(PP_USER_LAST_MAIL, null);
            pass = PlayerPrefs.GetString(PP_USER_LAST_PASS, null);
        }

        public static void DeleteLoginData()
        {
            PlayerPrefs.DeleteKey(PP_USER_LAST_MAIL);
            PlayerPrefs.DeleteKey(PP_USER_LAST_PASS);
        }

        public static bool CheckPseudo(string pseudo)
		{
            if (string.IsNullOrEmpty(pseudo))
                return false;
            return pseudo.Length >= 5;
		}
    }
}
