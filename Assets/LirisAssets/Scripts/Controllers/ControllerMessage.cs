namespace Liris.App.Controllers
{
    using AllMyScripts.Common.Tools;
    using Liris.App.Data;
    using System.Collections.Generic;
    using UnityEngine;

    public class ControllerMessage : ControllerBase
    {
        private const string USER_FIELD = "/api/users/";
        private const string TOUR_FIELD = "/api/tours/";
        private const string ANNOTATION_FIELD = "/api/annotations/";
        private const string MEDIA_FIELD = "/api/media_objects/";

        public string id;
        public string icons;
        public string emoticon;
        public string tags;
        public string comment;
        public string date;
        public string dateModif;
        public string imageId;
        public string userId;
        public string annotationId;
        public string tourId;
        public Texture2D tex;

        public ControllerMessage(Dictionary<string, object> dic)
        {
            InitFromDic(dic);
        }

        public ControllerMessage(string json)
        {
            if (!string.IsNullOrEmpty(json))
            {
                InitFromDic(JSON.Deserialize(json) as Dictionary<string, object>);
            }
        }

        public Dictionary<string, object> GetDataAsDic()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["id"] = id;
            dic["image"] = imageId;
            dic["icons"] = icons;
            dic["emoticon"] = emoticon;
            dic["tags"] = tags;
            dic["comment"] = comment;
            dic["date"] = date;
            dic["dateModif"] = dateModif;
            dic["user"] = userId;
            dic["tour"] = tourId;
            dic["annotation"] = annotationId;
            return dic;
        }

        public string GetDataAsJson()
        {
            return JSON.Serialize(GetDataAsDic());
        }

        public ControllerUser GetUser()
        {
            DataManager dataMgr = ApplicationManager.instance.dataManager;
            return dataMgr.GetUserFromId(userId);
        }

        public string GetFormattedDate()
        {
            return ConvertDateDayHourMinute(dateModif);
        }

        public bool CompareTo(ControllerMessage message)
        {
            if (id != message.id ||
                imageId != message.imageId ||
                icons != message.icons ||
                emoticon != message.emoticon ||
                tags != message.tags ||
                comment != message.comment ||
                userId != message.userId ||
                tourId != message.tourId ||
                annotationId != message.annotationId
                )
                return false;
            return true;
        }

        private void InitFromDic(Dictionary<string, object> dic)
        {
            if (dic != null)
            {
                id = DicTools.GetValueString(dic, "id");
                tex = null;
                if (dic.ContainsKey("tex"))
                    tex = dic["tex"] as Texture2D;
                imageId = DicTools.GetValueString(dic, "image");
                icons = DicTools.GetValueString(dic, "icons");
                emoticon = DicTools.GetValueString(dic, "emoticon");
                tags = DicTools.GetValueString(dic, "tags");
                comment = DicTools.GetValueString(dic, "comment");
                date = DicTools.GetValueString(dic, "date");
                dateModif = DicTools.GetValueString(dic, "dateModif");
                userId = DicTools.GetValueString(dic, "user");
                tourId = DicTools.GetValueString(dic, "tour");
                annotationId = DicTools.GetValueString(dic, "annotation");

                if (!string.IsNullOrEmpty(userId))
                {
                    if (userId.Contains(USER_FIELD))
                        userId = userId.Substring(USER_FIELD.Length);
                }
                else
                {
                    userId = null;
                }

                if (!string.IsNullOrEmpty(tourId))
                {
                    if (tourId.Contains(TOUR_FIELD))
                        tourId = tourId.Substring(TOUR_FIELD.Length);
                }
                else
                {
                    tourId = null;
                }

                if (!string.IsNullOrEmpty(annotationId))
                {
                    if (annotationId.Contains(ANNOTATION_FIELD))
                        annotationId = annotationId.Substring(ANNOTATION_FIELD.Length);
                }
                else
                {
                    annotationId = null;
                }

                if (!string.IsNullOrEmpty(imageId))
                {
                    if (imageId.Contains(MEDIA_FIELD))
                        imageId = imageId.Substring(MEDIA_FIELD.Length);
                }
                else
                {
                    imageId = null;
                }

                if (string.IsNullOrEmpty(dateModif) || ConvertInDateTime(dateModif).Year < 2020)
                    dateModif = date;
            }
        }
    }
}
