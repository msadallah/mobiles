namespace Liris.App.Controllers
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class ControllerAddress : ControllerBase
    {
        public string name;
        public float longitude;
        public float latitude;
        public float distance;

        public ControllerAddress(Dictionary<string, object> dic)
        {
            InitFromDic(dic);
        }

        public ControllerAddress(string json)
        {
            if (!string.IsNullOrEmpty(json))
            {
                InitFromDic(JSON.Deserialize(json) as Dictionary<string, object>);
            }
        }

        public Dictionary<string, object> GetDataAsDic()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["name"] = name;
            dic["lon"] = longitude;
            dic["lat"] = latitude;
            return dic;
        }

        public string GetDataAsJson()
        {
            return JSON.Serialize(GetDataAsDic());
        }

        public bool CompareTo(ControllerAddress address)
        {
            if (name != address.name ||
                longitude != address.longitude ||
                latitude != address.latitude)
                return false;
            return true;
        }

        private void InitFromDic(Dictionary<string, object> dic)
        {
            if (dic != null)
            {
                Dictionary<string, object> properties = DicTools.GetValueDictionary(dic, "properties");
                name = DicTools.GetValueString(properties, "label");
                distance = DicTools.GetValueFloat(properties, "distance");

                Dictionary<string, object> geometry = DicTools.GetValueDictionary(dic, "geometry");
                float[] coordinates = DicTools.GetValueListAsFloatArray(geometry, "coordinates");                
                longitude = coordinates[0];
                latitude = coordinates[1];
            }
        }
    }
}
