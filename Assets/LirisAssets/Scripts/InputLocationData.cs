namespace Liris.App
{
	using AllMyScripts.LangManager;
    using Liris.App.Trace;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
#if UNITY_ANDROID
    using UnityEngine.Android;
#endif

    public class InputLocationData : MonoBehaviour
    {
        public enum InputLocationEvent
        {
            START,
            STOP
        }

        public delegate void OnLocationData(float longitude, float latitude, float altitude);
        public delegate void OnLocationEvent(InputLocationEvent evt);

        public static OnLocationData onLocationData = null;
        public static OnLocationEvent onLocationEvent = null;

        public static bool isRunning => _isRunning;
        public static bool isStarting => _isStarting;

        private static bool _isRunning = false;
        private static bool _isStarting = false;

        private const float CALLBACK_DELAY = 0.2f;

        public bool CheckEnableByUser()
        {
            if (!Input.location.isEnabledByUser)
            {
                Debug.Log("[GPS] User don't accept to use GPS location!");
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.GPS_TITLE), L.Get(TextManager.GPS_DISABLED), Popups.FeedbackPopup.IconType.Alert);
                return false;
            }
            return true;
        }

        public void StartGetUserLocation()
        {
            if (_isStarting || _isRunning)
                return;

#if UNITY_ANDROID
            if (Permission.HasUserAuthorizedPermission(Permission.FineLocation))
            {
                Debug.Log("[GPS] FineLocation permission has been granted.");
            }
            else
            {
                PermissionCallbacks callbacks = new PermissionCallbacks();
                callbacks.PermissionDenied += PermissionCallbacks_PermissionDenied;
                callbacks.PermissionGranted += PermissionCallbacks_PermissionGranted;
                callbacks.PermissionDeniedAndDontAskAgain += PermissionCallbacks_PermissionDeniedAndDontAskAgain;
                Permission.RequestUserPermission(Permission.FineLocation, callbacks);
                return;
            }
#endif
            if (CheckEnableByUser())
            {
                Debug.Log("[GPS] User location permission ok!");
                StopAllCoroutines();
                StartCoroutine(GetLatLonUsingGPS());
            }
            else
            {
#if UNITY_EDITOR
                StartCoroutine(SimulateLatLonUsingGPS());
#endif
            }
        }

        public void StopGetUserLocation()
        {
            Debug.Log("[GPS] Stop retrieving location!");
            Input.location.Stop();
            StopAllCoroutines();
            _isRunning = false;
            _isStarting = false;
            onLocationEvent?.Invoke(InputLocationEvent.STOP);
        }

#if UNITY_ANDROID
        internal void PermissionCallbacks_PermissionDeniedAndDontAskAgain(string permissionName)
        {
            Debug.Log($"[GPS] {permissionName} PermissionDeniedAndDontAskAgain");
        }

        internal void PermissionCallbacks_PermissionGranted(string permissionName)
        {
            Debug.Log($"[GPS] {permissionName} PermissionCallbacks_PermissionGranted");
            StartGetUserLocation();
        }

        internal void PermissionCallbacks_PermissionDenied(string permissionName)
        {
            Debug.Log($"[GPS] {permissionName} PermissionCallbacks_PermissionDenied");
        }
#endif

        private IEnumerator GetLatLonUsingGPS()
        {
            _isStarting = true;

            Debug.Log("[GPS] Start retrieving location!");
            if (Input.location.status == LocationServiceStatus.Stopped)
                Input.location.Start();
            int maxWait = 20;
            while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
            {
                yield return new WaitForSeconds(1);
                maxWait--;
            }

            if (maxWait < 1)
            {
                Debug.Log($"[GPS] Failed To Initialize in {maxWait} seconds");
                yield break;
            }

            if (Input.location.status == LocationServiceStatus.Failed)
            {
                Debug.Log("[GPS] Failed To Initialize Location service");
                yield break;
            }

            _isStarting = false;
            _isRunning = true;
            onLocationEvent?.Invoke(InputLocationEvent.START);

            // kTBS
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:startGPSTracking"},
                {"m:userId", ApplicationManager.instance.user.id}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            ///

            while (Input.location.status == LocationServiceStatus.Running)
            {
                float longitude = (float)Input.location.lastData.longitude;
                float latitude = (float)Input.location.lastData.latitude;
                float altitude = (float)Input.location.lastData.altitude;
                //Debug.Log($"[GPS] longitude {longitude} latitude {latitude} altitude {altitude}");
                onLocationData?.Invoke(longitude, latitude, altitude);
                yield return new WaitForSeconds(CALLBACK_DELAY);
            }

            _isRunning = false;
            onLocationEvent?.Invoke(InputLocationEvent.STOP);
        }

        private IEnumerator SimulateLatLonUsingGPS() 
        {
            Debug.Log("[GPS] Start retrieving location!");
             // kTBS
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:startGPSTracking"},
                {"m:userId", ApplicationManager.instance.user.id}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            ///
            

            yield return new WaitForSeconds(1f);

            _isRunning = true;
            onLocationEvent?.Invoke(InputLocationEvent.START);

            float startLongitude = 4.82754707336426f;
            float startLatitude = 45.7606141325123f;
            float altitude = 400f;

            while (_isRunning)
            {
                float time = Time.realtimeSinceStartup * 0.001f;
                float angle1 = time * Mathf.PI * 0.23f;
                float angle2 = time * Mathf.PI * 0.67f;
                float angle3 = time * Mathf.PI * 1.13f;
                float longitude = startLongitude + Mathf.Cos(angle1) * 0.007f + Mathf.Cos(angle2) * 0.01f + Mathf.Cos(angle3) * 0.03f;
                float latitude = startLatitude + Mathf.Sin(angle1) * 0.007f + Mathf.Sin(angle2) * 0.01f + Mathf.Sin(angle3) * 0.03f;
                //Debug.Log($"[GPS] longitude {longitude} latitude {latitude} altitude {altitude}");
                onLocationData?.Invoke(longitude, latitude, altitude);
                yield return new WaitForSeconds(CALLBACK_DELAY);
            }

            _isRunning = false;
            onLocationEvent?.Invoke(InputLocationEvent.STOP);
        }
    }
}
