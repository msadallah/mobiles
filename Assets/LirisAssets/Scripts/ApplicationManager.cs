namespace Liris.App
{
	using AllMyScripts.Common.Tools;
	using AllMyScripts.Common.UI;
	using AllMyScripts.LangManager;
	using Liris.App.Controllers;
	using Liris.App.Data;
	using Liris.App.Emojis;
	using Liris.App.Favorites;
	using Liris.App.Filter;
	using Liris.App.Groups;
	using Liris.App.Media;
	using Liris.App.Network;
	using Liris.App.Notification;
	using Liris.App.Options;
	using Liris.App.Popups;
	using Liris.App.Tour;
	using Liris.App.Trace;
	using Liris.App.WebServices;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.EventSystems;

	public class ApplicationManager : MonoBehaviour
    {
        public class MessagesGroup
        {
            public MsgText message = null;
            public List<MsgBase> messages = null;
            public Vector2 uiPos = Vector2.zero;

            public void Clean()
            {
                if (message != null)
                {
                    message.Close();
                    message = null;
                }
                if (messages != null)
                    messages.Clear();
                uiPos = Vector2.zero;
            }
        }

        public delegate void OnLoginDelegate(ControllerUser user);
        public delegate void OnLogoutDelegate(ControllerUser user);

        public static ApplicationManager instance = null;
        public static OnLoginDelegate onLoginEvent = null;
        public static OnLogoutDelegate onLogoutEvent = null;

        public const float LERP_GPS_SPEED = 5f;

        public enum Mode
        {
            NONE,
            GPS,
            EDIT,
            POINT,
            MOVE_ANNOTATION,
            MOVE_TOUR,
            PREVIEW_TOUR
        }

        public enum MapType
        {
            STREET,
            SATELLITE
        }

        public enum Visibility
        {
            ONLY_ME,
            GROUPS,
            PUBLIC,
            ALL,
            ALL_WITHOUT_ME
        }

        public enum TimingType
        {
            UNIQUE,
            OCCASIONAL,
            REGULAR
        }

        public float currentZoom => _currentZoom;
        public float currentPointLongitude => _currentPointLongitude;
        public float currentPointLatitude => _currentPointLatitude;

        public Mode mode => _mode;
        public MapType mapType => _mapType;
        public Visibility visibility => _visibility;
        public bool offlineMode => _offlineMode;

        public ControllerUser user => _user;
        public ControllerTour tour => _tour;
        public PathLine currentLine => _currentLine;
        public bool isInPreviewTour => _previewTour != null;

        public DataManager dataManager => _dataManager;
        public FilterManager filterManager => _filterManager;
        public FavoritesManager favoritesManager => _favoritesManager;
        public EmojisManager emojisManager => _emojisManager;
        public GroupsManager groupsManager => _groupsManager;
        public OptionsManager optionsManager => _optionsManager;
        public MediaManager mediaManager => _mediaManager;
        public NotificationManager notificationManager => _notificationManager;
        public CreateTourManager createTourManager => _createTourManager;

        public CacheData cacheData => _cacheData;

        public bool isInit => _isInit;
        public bool isDragging => _isDragging;

        public string firebaseToken => _firebaseToken;

        [SerializeField]
        private OnlineMaps _maps = null;
        [SerializeField]
        private PathLine _linePrefab = null;
        [SerializeField]
        private Transform _linesRoot = null;
        [SerializeField]
        private DataManager _dataManager = null;
        [SerializeField]
        private FilterManager _filterManager = null;
        [SerializeField]
        private FavoritesManager _favoritesManager = null;
        [SerializeField]
        private EmojisManager _emojisManager = null;
        [SerializeField]
        private GroupsManager _groupsManager = null;
        [SerializeField]
        private OptionsManager _optionsManager = null;
        [SerializeField]
        private MediaManager _mediaManager = null;
        [SerializeField]
        private NotificationManager _notificationManager = null;
        [SerializeField]
        private CreateTourManager _createTourManager = null;
        [SerializeField]
        private InputLocationData _inputLocation = null;
        [SerializeField]
        private GameObject _mapRawTexture = null;
        [SerializeField]
        private OnlineMapsRawImageTouchForwarder _mapsForwarder = null;
        [SerializeField]
        private Camera _uiCamera = null;
        [SerializeField]
        private CacheData _cacheData = null;
        [SerializeField]
        private TextAsset _localisation = null;
        [SerializeField]
        private List<string> _localisationsOnCloud = null;

        private float _longitudeTarget = 0f;
        private float _latitudeTarget = 0f;
        private float _zoomTarget = 18f;

        private float _currentLongitude = 0f;
        private float _currentLatitude = 0f;
        private float _currentZoom = 13f;
        private bool _followGps = false;

        private List<PathLine> _lines = null;
        private PathLine _currentLine = null;
        private PathLine _lastSelectedLine = null;
        private Mode _mode = Mode.NONE;
        private MapType _mapType = MapType.STREET;
        private Visibility _visibility = Visibility.PUBLIC;
        private List<MsgBase> _messages = null;
        private List<MessagesGroup> _messagesGroup = null;
        private List<MsgMove> _moveMessages = null;
        private List<FollowPath> _followPaths = null;
        private MsgMove _currentMoveMessage = null;
        private Camera _mapCamera = null;

        private float _pressTime = 0f;
        private Vector3 _pressPosition = Vector3.zero;
        private ControllerUser _user = null;
        private float _lastGpsPointTime = 0f;
        private float _currentPointLongitude = 0f;
        private float _currentPointLatitude = 0f;

        private ControllerTour _tour = null;
        private ControllerTour _pressTour = null;
        private ControllerTour _previewTour = null;
        private Vector3 _pressTourPosition = Vector3.zero;
        private float _pressTourTime = 0f;
        private bool _showPositionIndicator = false;
        private const float _indicatorOffsetY = 48f;
        private float _lastCurrentTourUpdateTime = 0f;

        private ControllerAnnotation _annotationToMove = null;
        private MsgMixte _msgToMove = null;
        private Vector2 _moveOffet = Vector2.zero;
        private float _beforeMoveLongitude = 0f;
        private float _beforeMoveLatitude = 0f;

        private ControllerTour _tourToMove = null;
        private PathLine _lineToMove = null;
        private ControllerTour _tourToLinkMsgToMove = null;

        private bool _isInit = false;
        private bool _offlineMode = false;
        private bool _waitingChoice = false;
        private bool _wantToStayOffline = false;
        private bool _isDragging = false;

        private string _firebaseToken = null;

        public void SetMode(Mode mode)
        {
            _mode = mode;
        }

        public void SetVisibility(Visibility visibility)
        {
            _visibility = visibility;
            _cacheData.SaveVisibility(_user.id, (int)visibility);
            GetAllData(true, true);
        }

        public void SetMapType(MapType map)
        {
            _mapType = map;
            string mapType = null;
            switch (_mapType)
            {
                case MapType.STREET:
                    OnlineMaps.instance.mapType = "arcgis.worldstreetmap"; 
                    mapType = "Street";
                    break;
                case MapType.SATELLITE:
                    OnlineMaps.instance.mapType = "arcgis.worldimagery";
                    mapType = "Satellite";
                    break;
            }

            // kTBS
            Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:selectLayer"},
                    {"m:userId", user.id},
                    {"m:layer", mapType}
                };
            TraceManager.DispatchEventTraceToManager(trace);
            ///
        }

        public void SetUser(ControllerUser user, bool offline = false)
        {
            if (_user != null && user == null)
            {
                onLogoutEvent(_user);
                ClearData();
                _user = null;
            }
            else if (_user == null && user != null)
            {
                _user = user;
                _offlineMode = offline;
                Debug.Log("[OFFLINE] " + _offlineMode);
                onLoginEvent(user);
                _cacheData.SaveAllUsers(_dataManager.users);
                _cacheData.SaveCurrentUserId(user.id);
                _visibility = (Visibility)_cacheData.GetVisibility(user.id, (int)Visibility.PUBLIC);
                _filterManager.LoadPreferences(user);
                GetAllData(true, true);
                StartCoroutine(UpdateDevicesEnum());
            }
            MainUI.instance.SetFilterNotifCount(0);
            MainUI.instance.SetNotificationsNotifCount(0);
        }

        public string GetCurrentLineData()
        {
            string data = null;
            if (_currentLine != null)
            {
                return _currentLine.GetPositionDataAsString();
            }
            return data;
        }

        public void AddMessage(MsgBase.MsgType msgType, object data)
        {
            switch (msgType)
            {
                case MsgBase.MsgType.Mixte:
                    CreateMsgMixte(data as Dictionary<string,object>);
                    break;
                case MsgBase.MsgType.Picture:
                    CreateMsgPicture(data as Texture2D);
                    break;
                case MsgBase.MsgType.Icon:
                    CreateMsgIcon(data as Sprite);
                    break;
                case MsgBase.MsgType.Text:
                    CreateMsgText(data as string);
                    break;
            }
        }

        public void SetMapOnPosition(float lon, float lat, float zoom)
		{
            _currentLongitude = lon;
            _currentLatitude = lat;
            _currentZoom = zoom;
            _maps.SetPositionAndZoom(_currentLongitude, _currentLatitude, _currentZoom);
        }

        public void SetMapOnCurrentPathLine(float ratio)
		{
            if (_currentLine != null)
			{
                Vector2 pos = _currentLine.GetPositionAtRatio(ratio);
                SetMapOnPosition(pos.x, pos.y, _maps.floatZoom);
            }
		}

        public void SetFirebaseToken(string token)
		{
            _firebaseToken = token;
        }

        private void Awake()
        {
            instance = this;
            Debug.Log("*********************************************");
            Debug.Log("Platform : " + Application.platform);
            Debug.Log("Device ID : " + SystemInfo.deviceUniqueIdentifier);
            Debug.Log("Device Model : " + SystemInfo.deviceModel);
            Debug.Log("Processor infos : " + SystemInfo.processorCount + " // " + SystemInfo.processorFrequency);
            Debug.Log("Memory infos : " + SystemInfo.systemMemorySize);
            Debug.Log("Graphic information : " + SystemInfo.graphicsMemorySize + " // " + SystemInfo.graphicsShaderLevel + " // " + SystemInfo.supports3DTextures);
            Debug.Log("Shadow support infos : " + SystemInfo.supportsShadows);
            Debug.Log("Battery infos : " + SystemInfo.batteryStatus + " // " + SystemInfo.batteryLevel);
            Debug.Log("*********************************************");
            Screen.fullScreen = false;
            L.LoadLocalisationFile(_localisation);
            StartCoroutine(L.LoadLocalizationFromCloud("URL_Localisation", _localisationsOnCloud));
        }

        // Start is called before the first frame update
        private void Start()
        {
            InputLocationData.onLocationData += OnLocationData;
            OnlineMaps.instance.OnChangeZoom += OnChangeZoom;
            OnlineMaps.instance.OnChangePosition += OnChangePosition;
            MainUI.onUIEvent += OnUIEvent;
            _currentLongitude = _maps.position.x;
            _currentLatitude = _maps.position.y;
            _currentZoom = _maps.floatZoom;
            _longitudeTarget = _currentLongitude;
            _latitudeTarget = _currentLatitude;
            _zoomTarget = _currentZoom;
            _lines = new List<PathLine>();
            _messages = new List<MsgBase>();
            MainUI.instance.ShowLoginPopup();
            _mapsForwarder.SetGetTargetGameObjectOverride(MapsGetTargetGameObject);
            CheckOffline.onConnectionCbk += OnConnectionResult;
            MainUI.instance.SetFilterNotifCount(0);
            MainUI.instance.SetNotificationsNotifCount(0);
            _mapCamera = _maps.GetComponentInChildren<Camera>();
            Application.runInBackground = true;
        }

        private void OnDestroy()
        {
            Debug.Log("OnDestroy");
            instance = null;
            SetUser(null);
            InputLocationData.onLocationData -= OnLocationData;
            if (OnlineMaps.instance != null)
			{
                OnlineMaps.instance.OnChangeZoom -= OnChangeZoom;
                OnlineMaps.instance.OnChangePosition -= OnChangePosition;
            }   
            MainUI.onUIEvent -= OnUIEvent;
            _lines = null;
            _messages = null;
            if (_mapsForwarder != null)
                _mapsForwarder.SetGetTargetGameObjectOverride(null);
            CheckOffline.onConnectionCbk -= OnConnectionResult;
            _mapCamera = null;
            _tourToLinkMsgToMove = null;
            _previewTour = null;
        }

		private void OnApplicationQuit()
		{
            Debug.Log("OnApplicationQuit");
            SaveAllDataToCache();
        }

        private void OnApplicationPause(bool pause)
        {
            Debug.Log("OnApplicationPause " + pause);
            if (pause)
			{
                SaveAllDataToCache();
            }
        }

        private void Update()
        {
            UpdateCommon();
            UpdateGpsData();
            UpdateCurrentTour();
            switch (_mode)
            {
                case Mode.NONE:
                case Mode.PREVIEW_TOUR:
                    UpdateNoMode();
                    break;
                case Mode.GPS:
                    break;
                case Mode.EDIT:
                    UpdateMoveTourMode();
                    break;
                case Mode.POINT:
                    UpdatePointMode();
                    break;
                case Mode.MOVE_ANNOTATION:
                    UpdateMoveAnnotationMode();
                    break;
                case Mode.MOVE_TOUR:
                    UpdateMoveTourMode();
                    break;
            }
        }

        private void UpdateCommon()
		{
            if (_isDragging && !Input.anyKey)
			{
                _isDragging = false;
                //Debug.Log("DRAGGING FALSE");
            }
		}

        private void UpdateNoMode()
        {
            ControllerTour tour = UpdatePressTourDetection();
            if (tour != null)
            {
                MainUI.instance.ShowEditTourPopup(tour, isInPreviewTour);
                _lastSelectedLine = FindPathLineById(tour.id);
            }
        }

        private void UpdateGpsData()
        {
            if (InputLocationData.isRunning)
            {
                if (Input.anyKey)
                {
                    _currentLongitude = _maps.position.x;
                    _currentLatitude = _maps.position.y;
                    _currentZoom = _maps.floatZoom;
                    _followGps = false;
                    MainUI.instance.ShowGpsSearchButton(true);
                }

                if (_followGps)
                {
                    float deltaTime = Time.deltaTime;
                    _currentLongitude = Mathf.Lerp(_currentLongitude, _longitudeTarget, deltaTime * LERP_GPS_SPEED);
                    _currentLatitude = Mathf.Lerp(_currentLatitude, _latitudeTarget, deltaTime * LERP_GPS_SPEED);
                    float diffLon = (_currentLongitude - _longitudeTarget);
                    float diffLat = (_currentLatitude - _latitudeTarget);
                    float sqrDist = diffLon * diffLon + diffLat * diffLat;
                    float ratio = 1f - Mathf.InverseLerp(-10f, 1f, Mathf.Log10(sqrDist + 0.000001f));
                    _currentZoom = Mathf.Lerp(_currentZoom, _zoomTarget, deltaTime * ratio);
                    _maps.SetPositionAndZoom(_currentLongitude, _currentLatitude, _currentZoom);
                }
            }
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.V))
            {
                UpdateMessagesVisibility();
            }
#endif
        }

        private void UpdateCurrentTour()
        {
            if (_tour != null && _currentLine != null && Time.time - _lastCurrentTourUpdateTime > 10f)
            {
                _lastCurrentTourUpdateTime = Time.time;
                UpdateLinkedAnnotationsOnPath(_currentLine);
                string body = _currentLine.GetPositionDataAsString();
                string links = _currentLine.GetJsonFromLinkedAnnotations();

                if (body != _tour.body)
				{
                    if (_offlineMode)
                    {
                        _tour.UpdateBody(body);
                        _tour.linkedAnnotations = links;

                        // kTBS
                        //Dictionary<string, object> trace = new Dictionary<string, object>()
                        //{
                        //    {"@type", "m:moveTour"},
                        //    {"m:userId", tour.userId},
                        //    {"m:tourId", tour.id},
                        //    {"m:tourBody", body},
                        //    {"m:linkPosts", links},
                        //    {"m:status", ControllerTrace.MoveTourStatus.BUILD.ToString()}
                        //};
                        //TraceManager.DispatchEventTraceToManager(trace);
                        ///
                    }
                    else
                    {
                        _dataManager.UpdateTour(tour, body, links, ControllerTrace.MoveTourStatus.BUILD.ToString());
                    }
                } 
            }
        }

        private Ray GenerateMapRayFromScreenPos(Vector2 screenPos)
		{
            float ratio = (float)Screen.width / (float)Screen.height;
            float x = ((screenPos.x / Screen.width) - 0.5f) * ratio + 0.5f;
            float y = (screenPos.y / Screen.height);
            Vector3 pos = new Vector3(x, y, 0f);
            return _mapCamera.ViewportPointToRay(pos);
        }

        private ControllerTour UpdatePressTourDetection()
        {
            if (Input.GetMouseButtonDown(0) && !IsOverGUI())
            {
                _pressTour = null;
                _pressTourTime = 0f;
                Ray ray = GenerateMapRayFromScreenPos(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit hit))
                {
                    PathLine line = hit.collider.GetComponentInParent<PathLine>();
                    if (line != null)
                    {
                        foreach (PathLine l in _lines)
                            l.transform.localPosition -= Vector3.forward * l.transform.localPosition.z;

                        line.transform.localPosition -= Vector3.forward * 2f;

                        ControllerTour tour = _dataManager.GetTourFromId(line.tourId);
                        if (tour != null)
                        {
                            _pressTourPosition = Input.mousePosition;
                            _pressTourTime = Time.time;
                            _pressTour = tour;
                        }
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0) && _pressTour != null)
            {
                if (_pressTourTime > 0f && Time.time - _pressTourTime < 1f)
                {
                    if ((Input.mousePosition - _pressTourPosition).sqrMagnitude < 25f)
                    {
                        return _pressTour;
                    }
                }
            }
            return null;
        }

        private void UpdatePointMode()
        {
            if (Input.GetMouseButton(0))
            {
                Vector3 mousePos = Input.mousePosition;
                if (Input.GetMouseButtonDown(0) && !IsOverGUI())
                {
                    _pressTime = Time.time;
                    _pressPosition = mousePos;
                }
                else if (_pressTime > 0f)
                {
                    if ((_pressPosition - mousePos).sqrMagnitude > 25f)
                    {
                        _pressTime = 0f;
                    }
                    else if (Time.time - _pressTime > 0.2f)
                    {
                        MainUI.instance.indicatorPosition.gameObject.SetActive(true);
                        MainUI.instance.indicatorPosition.transform.SetAsLastSibling();
                        _showPositionIndicator = true;
                        _maps.blockAllInteractions = true;
                        _pressTime = 0f;
                    }
                }
                if (_showPositionIndicator)
                {
                    Vector2 pointerPos = OnlineMapsTileSetControl.instance.GetInputPosition();
                    pointerPos.y += _indicatorOffsetY;
                    OnlineMapsTileSetControl.instance.GetCoords(pointerPos, out double mx, out double my);
                    if (mx != 0f && my != 0f)
                    {   
                        MainUI.instance.indicatorPosition.SetCoords((float)mx, (float)my);
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (_showPositionIndicator)
                {
                    Vector2 pointerPos = OnlineMapsTileSetControl.instance.GetInputPosition();
                    pointerPos.y += _indicatorOffsetY;
                    OnlineMapsTileSetControl.instance.GetCoords(pointerPos, out double mx, out double my);
                    if (mx != 0f && my != 0f)
                    {
                        _currentPointLongitude = (float)mx;
                        _currentPointLatitude = (float)my;
                        MainUI.instance.ShowPlaceTypePopup();
                    }
                    MainUI.instance.indicatorPosition.gameObject.SetActive(false);
                    _showPositionIndicator = false;
                    _maps.blockAllInteractions = false;
                }
                _pressTime = 0f;
            }
        }

        private Vector2 GetMessageMapPosition(MsgBase msg)
        {
            Vector3 screenPos = _uiCamera.WorldToScreenPoint(msg.transform.position);
            return GetScreenToMapPosition(screenPos);
        }

        private Vector2 GetScreenToMapPosition(Vector3 screenPos)
        {
            float ratio = (_uiCamera.orthographicSize * 2f) / (float)Screen.height;
            screenPos.x -= (float)Screen.width * 0.5f;
            screenPos.x = _uiCamera.orthographicSize + screenPos.x * ratio;
            screenPos.y *= ratio;
            return new Vector2(screenPos.x, screenPos.y);
        }

        private Vector2 GetMapToScreenPosition(Vector2 mapPos)
        {
            float ratio = (float)Screen.height / (_uiCamera.orthographicSize * 2f);
            mapPos.x -= _uiCamera.orthographicSize;
            mapPos.x = Screen.width * 0.5f + mapPos.x * ratio;
            mapPos.y *= ratio;
            return mapPos;
        }

        private void UpdateMoveAnnotationMode()
        {
            if (Input.GetMouseButton(0))
            {
                Vector3 mousePos = Input.mousePosition;
                if (Input.GetMouseButtonDown(0))
                {
                    if (_msgToMove.IsMouseOver(mousePos, _uiCamera))
                    {
                        _pressTime = Time.time;
                        if (!string.IsNullOrEmpty(_annotationToMove.tourId))
                            _currentLine = FindPathLineById(_annotationToMove.tourId);                        
                        _moveOffet = GetScreenToMapPosition(mousePos) - GetMessageMapPosition(_msgToMove);
                        _maps.blockAllInteractions = true;
                    }
                }
                else if (_pressTime > 0f)
                {
                    Vector2 pointerPos = GetScreenToMapPosition(mousePos);
                    OnlineMapsTileSetControl.instance.GetCoords(pointerPos - _moveOffet, out double mx, out double my);
                    if (mx != 0f && my != 0f)
                    {
                        _tourToLinkMsgToMove = null;
                        float coordX = (float)mx;
                        float coordY = (float)my;
                        if (_currentLine != null)
                        {
                            float minSqrDist = 10000f;
                            Vector2 pos = new Vector2(coordX, coordY);
                            Vector2 screenPos = OnlineMapsTileSetControl.instance.GetScreenPosition(mx, my);
                            Vector2 linePos = _currentLine.ComputeNearestPointOnLine(pos, out int firstIdx, out float ratio);
                            if (firstIdx >= 0)
							{
                                Vector2 screenLinePos = OnlineMapsTileSetControl.instance.GetScreenPosition(linePos.x, linePos.y);
                                if (_annotationToMove.placeType == PlaceTypePopup.PLACE_TYPE_TOUR || (screenPos - screenLinePos).sqrMagnitude < minSqrDist)
                                {
                                    _tourToLinkMsgToMove = _dataManager.GetTourFromId(_currentLine.tourId);
                                    coordX = linePos.x;
                                    coordY = linePos.y;
                                }
                            }
                            _msgToMove.SetCoords(coordX, coordY);
                        }
                        else
                        {
                            if (mx != 0f && my != 0f)
                            {
                                _msgToMove.SetCoords(coordX, coordY);
                                // Check over a tour
                                Vector2 screenPos = GetMapToScreenPosition(OnlineMapsTileSetControl.instance.GetScreenPosition(mx, my));
                                Ray ray = GenerateMapRayFromScreenPos(screenPos);
                                if (Physics.Raycast(ray, out RaycastHit hit))
                                {
                                    PathLine line = hit.collider.GetComponentInParent<PathLine>();
                                    if (line != null)
                                    {
                                        ControllerTour tour = _dataManager.GetTourFromId(line.tourId);
                                        if (tour != null && tour.userId == _user.id)
										{
                                            _tourToLinkMsgToMove = tour;
                                        }
                                    }
                                }
                            }
                        }
                        _msgToMove.SetMoveColor(_tourToLinkMsgToMove != null ? Color.yellow : Color.green);
                    }                     
                }
            }
            else
            {
                if (_pressTime > 0f)
                {
                    _maps.blockAllInteractions = false;
                    _pressTime = 0f;
                }
            }
        }

        private void UpdateMoveTourMode()
        {
            if (Input.GetMouseButton(0))
            {
                Vector3 mousePos = Input.mousePosition;
                if (Input.GetMouseButtonDown(0))
                {
                    _currentMoveMessage = null;
                    // Try to select a move selector
                    if (_moveMessages != null)
					{
                        MsgMove clickedMoveMessage = null;
                        int highLineIdx = -1;
                        foreach (MsgMove move in _moveMessages)
                        {
                            if (move.IsMouseOver(mousePos, _uiCamera))
                            {
                                if (move.lineIndex > highLineIdx)
								{
                                    clickedMoveMessage = move;
                                    highLineIdx = move.lineIndex;
                                }
                            }
                        }
                        if (clickedMoveMessage != null)
						{
                            _currentMoveMessage = clickedMoveMessage;
                            _currentMoveMessage.ShowMoveVisual(true);
                            _moveOffet = GetScreenToMapPosition(mousePos) - GetMessageMapPosition(_currentMoveMessage);
                            _maps.blockAllInteractions = true;
                        }
                    }
                    // Try to create a move selector
                    if (_currentMoveMessage == null)
                    {
                        Ray ray = GenerateMapRayFromScreenPos(mousePos);
                        if (Physics.Raycast(ray, out RaycastHit hit))
                        {
                            PathLine line = hit.collider.GetComponentInParent<PathLine>();
                            if (line != null)
                            {
                                if (_currentLine == line)
								{
                                    Vector2 pointerPos = GetScreenToMapPosition(mousePos);
                                    OnlineMapsTileSetControl.instance.GetCoords(pointerPos, out double mx, out double my);
                                    if (line.InsertPoint((float)mx, (float)my, out Vector2 newPos, out int index))
									{
                                        _currentMoveMessage = CreateMoveMessageAtIndex(line, index);
                                        _currentMoveMessage.ShowMoveVisual(true);
                                        RecomputeFollowPathsFromLine();
                                        _maps.blockAllInteractions = true;
                                        _createTourManager.SaveInLocal();
                                    }
                                }
                            }
                        }
                    }
                    if (_currentMoveMessage == null && !IsOverGUI())
					{
                        _pressTime = Time.time;
                        _pressPosition = mousePos;
                    }
				}
				else if (_currentMoveMessage != null)
                {
                    Vector2 pointerPos = GetScreenToMapPosition(mousePos);
                    OnlineMapsTileSetControl.instance.GetCoords(pointerPos - _moveOffet, out double mx, out double my);
                    if (mx != 0f && my != 0f)
                    {
                        _currentMoveMessage.SetCoords((float)mx, (float)my);
                        if (_followPaths != null && _followPaths.Count > 0)
						{
                            foreach (FollowPath follow in _followPaths)
                                follow.RefreshPosition();
						}
                        Color color = Color.green;
                        MsgMixte msgNear = GetAnnotationNearMessage(_currentMoveMessage);
                        if (msgNear != null)
						{
                            color = Color.yellow;
                            _currentMoveMessage.SetCoords(msgNear.longitude, msgNear.latitude);
                        }
                        bool areMoveMsgOver = AreMoveMessagesOver(_currentMoveMessage.lineIndex);
                        if (areMoveMsgOver)
                        {
                            color = Color.red;
                        }
                        _currentMoveMessage.SetMoveColor(color);
                    }
                }
                else if (_pressTime > 0f)
				{
                    if ((_pressPosition - mousePos).sqrMagnitude > 25f)
                    {
                        //Debug.Log("UpdateEditMode - too far " + (_pressPosition - Input.mousePosition).sqrMagnitude);
                        _pressTime = 0f;
                    }
                    else if (Time.time - _pressTime > 0.2f)
                    {
                        MainUI.instance.indicatorPosition.gameObject.SetActive(true);
                        MainUI.instance.indicatorPosition.transform.SetAsLastSibling();
                        MainUI.instance.indicatorPosition.ShowMoveVisual(true);
                        _showPositionIndicator = true;
                        _maps.blockAllInteractions = true;
                        _pressTime = 0f;
                    }
                }
                if (_showPositionIndicator)
                {
                    Vector2 pointerPos = OnlineMapsTileSetControl.instance.GetInputPosition();
                    pointerPos.y += _indicatorOffsetY;
                    OnlineMapsTileSetControl.instance.GetCoords(pointerPos, out double mx, out double my);
                    if (mx != 0f && my != 0f)
                    {
                        MainUI.instance.indicatorPosition.SetCoords((float)mx, (float)my);
                        Color color = Color.green;
                        MsgMixte msgNear = GetAnnotationNearMessage(MainUI.instance.indicatorPosition);
                        if (msgNear != null)
                        {
                            MainUI.instance.indicatorPosition.SetCoords(msgNear.longitude, msgNear.latitude);
                            color = Color.yellow;
                        }
                        MainUI.instance.indicatorPosition.SetMoveColor(color);
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (_currentMoveMessage != null)
                {
                    if (_followPaths != null && _followPaths.Count > 0)
                    {
                        foreach (FollowPath follow in _followPaths)
                            follow.RefreshPosition();
                    }
                    MsgMixte msgNear = GetAnnotationNearMessage(_currentMoveMessage);
                    if (msgNear != null)
                    {
                        _currentMoveMessage.SetCoords(msgNear.longitude, msgNear.latitude);
                        _currentMoveMessage.SetLinkedAnnotation(msgNear.id);
                    }
                    else
                    {
                        _currentMoveMessage.SetLinkedAnnotation(null);
                    }
                    UpdateLinkedAnnotationsOnPath(_currentLine);
                    bool areMoveMsgOver = AreMoveMessagesOver(_currentMoveMessage.lineIndex);
                    if (areMoveMsgOver && _currentLine != null)
					{
                        int deleteIdx = _currentMoveMessage.lineIndex;
                        if (_currentLine.DeletePoint(deleteIdx))
                        {
                            GameObject.Destroy(_currentMoveMessage.gameObject);
                            DeleteMoveMessageAtIndex(_currentLine, deleteIdx);
                            _currentMoveMessage = null;
                            RecomputeFollowPathsFromLine();
                            _createTourManager.SaveInLocal();
                        }
                    }
                    if (_currentMoveMessage != null)
					{
                        _currentMoveMessage.ShowMoveVisual(false);
                        _currentMoveMessage = null;
                    }
                    _maps.blockAllInteractions = false;
                }
                else
                {
                    if (_showPositionIndicator)
                    {
                        string linkedAnnotationId = null;
                        Vector2 pointerPos = OnlineMapsTileSetControl.instance.GetInputPosition();
						pointerPos.y += _indicatorOffsetY;
						OnlineMapsTileSetControl.instance.GetCoords(pointerPos, out double mx, out double my);
                        if (mx != 0f && my != 0f)
                        {
                            MainUI.instance.indicatorPosition.SetCoords((float)mx, (float)my);
                            MsgMixte msgNear = GetAnnotationNearMessage(MainUI.instance.indicatorPosition, 30f);
                            if (msgNear != null)
                            {
                                _currentPointLongitude = msgNear.longitude;
                                _currentPointLatitude = msgNear.latitude;
                                MainUI.instance.indicatorPosition.SetCoords(msgNear.longitude, msgNear.latitude);
                                linkedAnnotationId = msgNear.annotation.id;
                            }
                            else
							{
                                _currentPointLongitude = (float)mx;
                                _currentPointLatitude = (float)my;
                            }
                            if (_currentLine.AddPoint(_currentPointLongitude, _currentPointLatitude))
							{
                                _currentMoveMessage = CreateMoveMessageAtIndex(_currentLine, _currentLine.pointCount - 1);
                                _currentMoveMessage.SetLinkedAnnotation(linkedAnnotationId);
                                RecomputeFollowPathsFromLine();
                                UpdateLinkedAnnotationsOnPath(_currentLine);
                                _createTourManager.SaveInLocal();
                            }
                        }
                        MainUI.instance.indicatorPosition.gameObject.SetActive(false);
                        _showPositionIndicator = false;
                        _maps.blockAllInteractions = false;
                    }
                    _pressTime = 0f;
                }
            }
        }

        private bool AreMoveMessagesOver(int index)
		{
            bool over = false;
            if (index > 0 && AreMsgClose(_moveMessages[index - 1], _moveMessages[index], 10f))
            {
                over = true;
            }
            else if (index < _moveMessages.Count - 1 && AreMsgClose(_moveMessages[index + 1], _moveMessages[index], 10f))
            {
                over = true;
            }
            return over;
        }

        private MsgMixte GetAnnotationNearMessage(MsgBase msgRef, float distMax = 30f)
        {
            if (msgRef != null)
			{
                foreach (MsgMixte msg in _messages)
                {
                    if (msg.visible)
					{
                        if (AreMsgClose(msg, msgRef, distMax))
                        {
                            return msg;
                        }
                    }
			    }
            }
            return null;
        }

        private bool AreMsgClose(MsgBase msgA, MsgBase msgB, float dist)
		{
            Vector2 posA = OnlineMapsTileSetControl.instance.GetScreenPosition(msgA.longitude, msgA.latitude);
            Vector2 posB = OnlineMapsTileSetControl.instance.GetScreenPosition(msgB.longitude, msgB.latitude);
            return ((posA - posB).sqrMagnitude) < (dist * dist);
        }

        private bool IsOverGUI()
        {
            return UtilsUI.IsPointerOverUIObject(ShouldIgnore);
        }

        private bool ShouldIgnore(RaycastResult res)
        {
            if (res.gameObject == _mapRawTexture)
                return true;
            if (res.gameObject.name == "MoveRoot")
                return true;
            return false;
        }

        private void LateUpdate()
        {
            if (_lines != null && _lines.Count > 0)
            {
                float lineSize = Mathf.Max(_maps.floatZoom, 1f);
                float minSize = 13f;
                if (_currentLine != null)
				{
                    foreach (PathLine line in _lines)
                        line.RenderLine(line == _currentLine, false, lineSize, minSize);
                }
                else
				{
                    foreach (PathLine line in _lines)
                        line.RenderLine(true, line != _lastSelectedLine, lineSize, minSize);
                }
            }
            if (_messages != null && _messages.Count > 0)
            {
                foreach (MsgBase msg in _messages)
                    msg.Render();
            }
            if (_messagesGroup != null && _messagesGroup.Count > 0)
            {
                foreach (MessagesGroup group in _messagesGroup)
                    group.message.Render();
            }
            if (_moveMessages != null && _moveMessages.Count > 0)
            {
                foreach (MsgMove move in _moveMessages)
                    move.Render();
            }
            if (_showPositionIndicator)
            {
                MainUI.instance.indicatorPosition.Render();
            }
        }

        private void OnLocationData(float longitude, float latitude, float altitude)
        {
            _longitudeTarget = longitude;
            _latitudeTarget = latitude;
            _zoomTarget = 18f;
            if (_mode == Mode.GPS)
            {
                if (_currentLine != null)
                {
                    if (_lastGpsPointTime == 0f || Time.time - _lastGpsPointTime > 5f)
                    {
                        if (_currentLine.AddPoint(_longitudeTarget, _latitudeTarget))
						{
                            _currentPointLongitude = _longitudeTarget;
                            _currentPointLatitude = _latitudeTarget;
                            _createTourManager.SaveInLocal();
                        }
                        _lastGpsPointTime = Time.time;
                    }
                }
            }
        }

        private void OnChangeZoom()
        {
            UpdateMessagesVisibility();
        }

        private void OnChangePosition()
		{
            if (!_isDragging)
			{
                _isDragging = true;
                //Debug.Log("DRAGGING TRUE");
            }
        }

        private void OnUIEvent(MainUI.UIEvent evt)
        {
            switch (evt)
            {
                case MainUI.UIEvent.MODE_GPS:
                    SetMode(Mode.GPS);
                    break;
                case MainUI.UIEvent.MODE_EDIT:
                    SetMode(Mode.EDIT);
                    break;
                case MainUI.UIEvent.MODE_POINT:
                    SetMode(Mode.POINT);
                    break;
                case MainUI.UIEvent.MODE_NONE:
                    SetMode(Mode.NONE);
                    break;
                case MainUI.UIEvent.START_PATH:
                    BeginLine();
                    break;
                case MainUI.UIEvent.STOP_PATH:
                    EndLine();
                    break;
                case MainUI.UIEvent.CENTER_GPS:
                    _inputLocation.StartGetUserLocation();
                    _followGps = true;
                    MainUI.instance.ShowGpsSearchButton(false);
                    break;
                case MainUI.UIEvent.MOVE_VALIDATE:
                    if (_msgToMove != null)
                        StopMoveAnnotation(true);
                    if (_lineToMove != null)
                        StopMoveTour(true);
                    break;
                case MainUI.UIEvent.MOVE_CANCEL:                    
                    if (_msgToMove != null)
                        StopMoveAnnotation(false);
                    if (_lineToMove != null)
                        StopMoveTour(false);
                    break;
                case MainUI.UIEvent.EXIT_PREVIEW_TOUR:
                    if (isInPreviewTour)
					{
                        ShowPreviewOnTour(null);
					}
                    break;
            }
        }

        public void BeginLine()
        {
            MainUI.instance.CreateTourPopup(_mode == Mode.EDIT, OnCreateTour);
        }

        private PathLine CreatePathLine()
        {
            PathLine line = GameObject.Instantiate<PathLine>(_linePrefab, _linesRoot);
            line.Init(OnlineMapsTileSetControl.instance);
            _lines.Add(line);
            return line;
        }

        private PathLine CreatePathLineFromTour(ControllerTour tour)
        {
            PathLine line = CreatePathLine();
            line.InitFromTour(tour);
            return line;
        }

        private void OnCreateTour(ControllerTour tour)
        {
            if (tour != null)
            {
                _currentLine = CreatePathLineFromTour(tour);
                _tour = tour;
                _lastCurrentTourUpdateTime = Time.time;
                CreateMoveMessagesFromLine(_currentLine);
                UpdateLinkedAnnotationsOnPath(_currentLine);
                _createTourManager.Init(tour, _currentLine, GetCenterCoords());
                _createTourManager.SaveInLocal();
            }
            else
            {
                MainUI.instance.ResetCreateTour();
            }
        }

        private List<ControllerAnnotation> UpdateLinkedAnnotationsOnPath(PathLine path)
		{
            List<ControllerAnnotation> annotationTourIds = null;
            if (path != null && _moveMessages != null)
			{
                List<string> linkedAnnotations = new List<string>();
                foreach (MsgMove move in _moveMessages)
                {
                    string id = move.linkedAnnotation;
                    if (id != null)
                    {
                        linkedAnnotations.Add(id);
                        ControllerAnnotation annotation = _dataManager.GetAnnotationFromId(id);
                        if (annotation != null && annotation.userId == _user.id && annotation.tourId == null)
                        {
                            if (annotationTourIds == null)
                                annotationTourIds = new List<ControllerAnnotation>();
                            annotationTourIds.Add(annotation);
                        }
                    }
                }
                path.SetLinkedAnnotations(linkedAnnotations);
            }            
            return annotationTourIds;
        }

        public void CancelTourCreation()
		{
            ClearMoveMessages();
            ClearFollowPaths();
            _tour = null;
            _currentLine = null;
            _createTourManager.Reset();
            _createTourManager.SaveInLocal();
        }

        public void EndLine()
        {
            if (_tour != null && _currentLine != null)
			{
                if (_currentLine.pointCount > 1)
                {
                    List<ControllerAnnotation> annotationTourIds = UpdateLinkedAnnotationsOnPath(_currentLine);
                    _currentLine.UpdateBodyFromPoints();
                    string body = _currentLine.GetPositionDataAsString();
                    string links = _currentLine.GetJsonFromLinkedAnnotations();
                    StartCoroutine(UpdateTourPosition(_tour, body, links, ControllerTrace.MoveTourStatus.VALIDATE.ToString(), annotationTourIds, (result) =>
                    {
                        if (result)
						{
                            ClearMoveMessages();
                            ClearFollowPaths();
                            _tour = null;
                            _currentLine = null;
                            _createTourManager.Reset();
                            _createTourManager.SaveInLocal();
                        }
                    }));                    
                }
                else
                {
                    MainUI.instance.ShowChoicePopup(L.Get(TextManager.TOURS_CREATION_TITLE), L.Get(TextManager.TOURS_CREATION_NOT_ENOUGH_POINTS_DELETE),
                        L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO), (bool result) =>
                        {
                            if (result)
                            {
                                if (_offlineMode)
                                {
                                    RemoveTour(_tour);

                                    // kTBS
                                    Dictionary<string, object> trace = new Dictionary<string, object>()
                                    {
                                        {"@type", "m:deleteTour"},
                                        {"m:userId", _tour.userId},
                                        {"m:tourId", _tour.id},
                                        {"m:autoDelete",  true}
                                    };
                                    TraceManager.DispatchEventTraceToManager(trace);
                                    ///
                                }
                                else
                                {
                                    _dataManager.DeleteTour(_tour, true);
                                    RemoveTour(_tour);
                                }
                                ClearMoveMessages();
                                ClearFollowPaths();
                                _tour = null;
                                _currentLine = null;
                                _createTourManager.Reset();
                                _createTourManager.SaveInLocal();
                            }
                            else
                            {
                                if (_createTourManager.gps)
                                    MainUI.instance.ActivateTourGPS(false);
                                else
                                    MainUI.instance.ActivateTourEdition(false);
                            }
                    });
                }
            }
        }

        public void RemoveAnnotation(ControllerAnnotation annotation)
        {
            _dataManager.RemoveAnnotation(annotation);
            MsgBase msg = FindMessageById(annotation.id);
            if (msg != null)
                RemoveMessage(msg);
        }

        public void RemoveMessage(MsgBase msg)
        {
            if (msg != null)
            {
                msg.Close();
                _messages.Remove(msg);
            }
        }

        public void MoveAnnotation(ControllerAnnotation annotation)
        {
            string id = annotation != null ? annotation.id : null;
            if (id != null)
            {
                MsgBase msg = FindMessageById(id);
                if (msg != null && msg is MsgMixte)
                {
                    _annotationToMove = annotation;
                    _msgToMove = msg as MsgMixte;
                    _msgToMove.ActivateMoveEdition(true);
                    SetMode(Mode.MOVE_ANNOTATION);
                    MainUI.instance.StartMoveMessage(_msgToMove);
                    _beforeMoveLongitude = _msgToMove.longitude;
                    _beforeMoveLatitude = _msgToMove.latitude;
                    if (annotation.tourId != null)
                        _tourToLinkMsgToMove = _dataManager.GetTourFromId(annotation.tourId);
                    _msgToMove.SetMoveColor(_tourToLinkMsgToMove != null ? Color.yellow : Color.green);
                }
            }
        }

        public void StopMoveAnnotation(bool validate)
        {
            if (validate)
            {
                string tourId = null;
                if (_tourToLinkMsgToMove != null)
				{
                    tourId = _tourToLinkMsgToMove.id;
                }
                string coords = ConvertVector2ToCoords(new Vector2(_msgToMove.longitude, _msgToMove.latitude));
                StartCoroutine(UpdateAnnotationPositionEnum(_msgToMove.annotation, coords, tourId));
            }
            else
            {
                _msgToMove.SetCoords(_beforeMoveLongitude, _beforeMoveLatitude);
            }
            _msgToMove.ActivateMoveEdition(false);
            MainUI.instance.StopMoveMessage(_msgToMove);
            SetMode(Mode.NONE);
            _msgToMove = null;
            _annotationToMove = null;
            _currentLine = null;
        }

        public void MoveTour(ControllerTour tour)
        {
            string id = tour != null ? tour.id : null;
            if (id != null)
            {
                PathLine line = FindPathLineById(id);
                if (line != null)
                {
                    _tourToMove = tour;
                    _lineToMove = line;
                    _currentLine = line;
                    MainUI.instance.StartMoveMessage(null);
                    _lineToMove.ActivateMoveEdition(true);
                    SetMode(Mode.MOVE_TOUR);
                    CreateMoveMessagesFromLine(line);
                    CreateFollowPathsFromLine(line);
                    UpdateLinkedAnnotationsOnPath(line);
                }
            }
        }

        public void CreateMoveMessagesFromLine(PathLine line)
		{
            int pointCount = line.pointCount;
            if (pointCount > 0)
            {
                _moveMessages = new List<MsgMove>(pointCount);
                for (int i = 0; i < pointCount; ++i)
                {
                    Vector2 point = line.points[i];
                    MsgMove move = MainUI.instance.CreateMove();
                    move.Init(point.x, point.y, OnlineMapsTileSetControl.instance);
                    move.SetPathLine(line, i);

                    if (line.linkedAnnotations != null)
                    {
                        foreach (string id in line.linkedAnnotations)
                        {
                            MsgMixte msg = FindMessageById(id) as MsgMixte;
                            if (msg != null)
                            {
                                if (AreMsgClose(move, msg, 1f))
                                    move.SetLinkedAnnotation(msg.annotation.id);
                            }
                        }
                    }

                    _moveMessages.Add(move);
                }
            }
            else
			{
                _moveMessages = new List<MsgMove>();
            }
        }

        public MsgMove CreateMoveMessageAtIndex(PathLine line, int index)
        {
            if (_moveMessages != null)
            {
                Vector2 point = line.points[index];
                MsgMove move = MainUI.instance.CreateMove();
                move.Init(point.x, point.y, OnlineMapsTileSetControl.instance);
                move.SetPathLine(line, index);
                _moveMessages.Insert(index, move);
                int pointCount = line.pointCount;
                for (int i = index + 1; i < pointCount; ++i)
                {
                    _moveMessages[i].SetPathLine(line, i);
                }
                return move;
            }
            return null;
        }

        public void DeleteMoveMessageAtIndex(PathLine line, int index)
        {
            if (_moveMessages != null && _moveMessages.Count > 0 && index < _moveMessages.Count)
            {
                _moveMessages.RemoveAt(index);
                int pointCount = line.pointCount;
                for (int i = index; i < pointCount; ++i)
                {
                    _moveMessages[i].SetPathLine(line, i);
                }
            }
        }

        public MsgMove GetMoveMessageFromLineIndex(int index)
        {
            if (_moveMessages != null && _moveMessages.Count > 0)
            {
                foreach (MsgMove move in _moveMessages)
				{
                    if (move.lineIndex == index)
                        return move;
				}
            }
            return null;
        }

        public void CreateFollowPathsFromLine(PathLine line)
		{
            List<ControllerAnnotation> annotations = _dataManager.GetAnnotationsFromTourId(line.tourId);
            if (annotations != null && annotations.Count > 0)
            {
                if (_followPaths == null)
                    _followPaths = new List<FollowPath>();
                else
                    _followPaths.Clear();

                foreach (ControllerAnnotation annotation in annotations)
                {
                    MsgBase msg = FindMessageById(annotation.id);
                    FollowPath followPath = msg.gameObject.AddComponent<FollowPath>();
                    followPath.SetPathLine(line);
                    _followPaths.Add(followPath);
                }
            }
        }

        public void RecomputeFollowPathsFromLine()
        {
            if (_followPaths != null && _followPaths.Count > 0)
            {
                foreach (FollowPath followPath in _followPaths)
                {
                    followPath.ComputeLineIdxAndRatio();
                }
            }
        }

        public void ShowPreviewOnTour(ControllerTour tour)
		{
            _previewTour = tour;

            if (isInPreviewTour)
			{
                string tourId = tour.id;
                _currentLine = FindPathLineById(tourId);

                if (_messages != null && _messages.Count > 0)
                {
                    foreach (MsgBase msg in _messages)
                    {
                        MsgMixte msgMixte = msg as MsgMixte;
                        if (msgMixte != null)
						{
                            bool show = msgMixte.annotation.tourId == tourId || _currentLine.linkedAnnotations.Contains(msgMixte.annotation.id);
                            msgMixte.ShowInPreview(show);
                        }
                        
                    }
                }
                SetMapOnCurrentPathLine(0);
                SetMode(Mode.PREVIEW_TOUR);
                MainUI.instance.SetPreviewTourMode(_previewTour);
            }
            else
			{
                _currentLine = null;
                SetMode(Mode.NONE);
                MainUI.instance.SetPreviewTourMode(null);
            }
            UpdateMessagesVisibility();
        }

        public void StopMoveTour(bool validate)
		{
            if (validate)
			{
                if (_followPaths != null && _followPaths.Count > 0)
                {
                    foreach (FollowPath follow in _followPaths)
                        follow.ValidPosition();
                }
                List<ControllerAnnotation> annotationTourIds = UpdateLinkedAnnotationsOnPath(_lineToMove);
                _lineToMove.UpdateBodyFromPoints();
                string body = _lineToMove.GetPositionDataAsString();
                string links = _lineToMove.GetJsonFromLinkedAnnotations();
                StartCoroutine(UpdateTourPosition(_tourToMove, body, links, ControllerTrace.MoveTourStatus.VALIDATE.ToString(), annotationTourIds));
            }
            else
			{
                foreach (MsgMove move in _moveMessages)
                    move.Revert();

                if (_followPaths != null && _followPaths.Count > 0)
                {
                    foreach (FollowPath follow in _followPaths)
                        follow.ResetPosition();
                }
                _lineToMove.ResetLineFromBody();
            }

            _lineToMove.UpdateExtremities();

            ClearMoveMessages();
            ClearFollowPaths();
            MainUI.instance.StopMoveMessage(null);
            _lineToMove.ActivateMoveEdition(false);            
            SetMode(Mode.NONE);
            _lineToMove = null;
            _tourToMove = null;
            _currentLine = null;
        }

        public void NoInternet()
		{
            OnConnectionResult(false);
		}

        private IEnumerator UpdateAnnotationPositionEnum(ControllerAnnotation annotation, string coords, string tourId)
        {
            if (_offlineMode)
			{
                annotation.coords = coords;
                annotation.tourId = tourId;
            }
            else
			{
                MainUI.instance.ShowLoadingPopup();

                if (tourId != annotation.tourId)
				{
                    yield return _dataManager.UpdateAnnotationTour(annotation, tourId);
                }

                yield return _dataManager.UpdateAnnotationCoords(annotation, coords);

                if (_dataManager.lastResultFailed)
                {
                    MsgBase msg = FindMessageById(annotation.id);
                    if (msg != null)
                        msg.SetCoords(_beforeMoveLongitude, _beforeMoveLatitude);
                }
                MainUI.instance.HideLoadingPopup();
            }
        }

        private IEnumerator UpdateTourPosition(ControllerTour tour, string body, string links, string status, List<ControllerAnnotation> annotationTourIds, System.Action<bool> doneCbk=null)
        {
            bool result = true;
            if (_offlineMode)
			{
                tour.UpdateBody(body);
                tour.linkedAnnotations = links;
                if (annotationTourIds != null)
				{
                    foreach (ControllerAnnotation annotation in annotationTourIds)
                        annotation.tourId = _tour.id;
                }

                // kTBS
                Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:moveTour"},
                    {"m:userId", tour.userId},
                    {"m:tourId", tour.id},
                    {"m:tourBody", body},
                    {"m:linkPosts", links},
                    {"m:status", status}
                };
                TraceManager.DispatchEventTraceToManager(trace);
                ///                
            }
            else
			{
                MainUI.instance.ShowLoadingPopup();

                yield return _dataManager.UpdateTour(tour, body, links, status, true);
                if (_dataManager.lastResultFailed)
                    result = false;
                if (!result && annotationTourIds != null)
                {
                    foreach (ControllerAnnotation annotation in annotationTourIds)
					{
                        yield return _dataManager.UpdateAnnotationTour(annotation, tour.id);
                        if (_dataManager.lastResultFailed)
						{
                            result = false;
                            break;
                        }   
                    }   
                }

                MainUI.instance.HideLoadingPopup();
            }
            doneCbk?.Invoke(result);
        }

        private MsgBase FindMessageById(string id)
        {
            foreach (MsgBase msg in _messages)
            {
                if (msg.id == id)
                {
                    return msg;
                }
            }
            return null;
        }

        public void RemoveTour(ControllerTour tour)
        {
            if (tour != null)
			{
                _dataManager.RemoveTour(tour);
                string id = tour.id;
                PathLine line = FindPathLineById(id);
                RemovePathLine(line);
                List<ControllerAnnotation> annotations = _dataManager.GetAnnotationsFromTourId(id);
                if (annotations != null && annotations.Count > 0)
                {
                    foreach (ControllerAnnotation annotation in annotations)
                    {
                        RemoveAnnotation(annotation);
                    }
                }
            }
        }

        public void RemovePathLine(PathLine line)
		{
            if (line != null)
            {
                GameObject.Destroy(line.gameObject);
                _lines.Remove(line);
            }
        }

        public void RemoveMessagesFromPathLine(PathLine line)
        {
            if (line != null)
            {
                List<ControllerAnnotation> annotations = _dataManager.GetAnnotationsFromTourId(line.tourId);
                if (annotations != null && annotations.Count > 0)
                {
                    foreach (ControllerAnnotation annotation in annotations)
                    {
                        MsgBase msg = FindMessageById(annotation.id);
                        RemoveMessage(msg);
                    }
                }
            }
        }

        private PathLine FindPathLineById(string id)
        {
            foreach (PathLine line in _lines)
            {
                if (line.tourId == id)
                {
                    return line;
                }
            }
            return null;
        }

        private MsgMixte CreateMsgMixte(Dictionary<string, object> dic)
        {
            return CreateMsgMixte(_dataManager.CreateAnnotation(dic));
        }

        private MsgMixte CreateMsgMixte(ControllerAnnotation annotation)
        {
            Vector2 pos = ConvertCoordsToVector2(annotation.coords);
            MsgMixte msg = MainUI.instance.CreateMixteAnnotation();
            msg.Init(pos.x, pos.y, OnlineMapsTileSetControl.instance);
            msg.SetAnnotation(annotation);
            msg.SetVisible(true);
            _messages.Add(msg);
            return msg;
        }

        public MsgMixte UpdateMsgMixte(ControllerAnnotation annotation)
        {
            foreach (MsgBase msg in _messages)
            {
                if (msg.id == annotation.id)
                {
                    MsgMixte msgMixte = msg as MsgMixte;
                    if (msgMixte != null)
                        msgMixte.SetAnnotation(annotation);
                    return msgMixte;
                }
            }
            return null;
        }

        public void RefreshAllData(bool clearAll = false)
        {
            if (clearAll)
			{
                GetAllData(true, true);
            }
            else
			{
                GetAllData();
                MainUI.instance.SetFilterNotifCount(_filterManager.GetFilterCount());
            }
        }

        public Vector2 GetMapsCoords()
		{
            if (_maps != null)
                return _maps.position;
            return Vector2.zero;
		}

        public static Vector2 ConvertCoordsToVector2(string coords)
        {
            Dictionary<string, object> dic = JSON.Deserialize(coords) as Dictionary<string,object>;
            float x = DicTools.GetValueFloat(dic, "lon");
            float y = DicTools.GetValueFloat(dic, "lat");
            return new Vector2(x, y);
        }

        public static string ConvertVector2ToCoords(Vector2 pos)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("lon", pos.x);
            dic.Add("lat", pos.y);
            return JSON.Serialize(dic);
        }

        public static string GetCurrentPointCoords()
        {
            return ConvertVector2ToCoords(new Vector2(instance.currentPointLongitude, instance.currentPointLatitude));
        }

        public static string GetCenterCoords()
		{
            Vector2 pointerPos = new Vector2(512f, 512f);
            OnlineMapsTileSetControl.instance.GetCoords(pointerPos, out double mx, out double my);
            if (mx != 0f && my != 0f)
			{
                Debug.Log("GetCenterCoords mx " + mx + " my " + my);
                return ConvertVector2ToCoords(new Vector2((float)mx, (float)my));
            }
            return null;
        }

        public void OnTourCreationDetected()
		{
            StartCoroutine(TourCreationChoicesEnum());
        }

        private IEnumerator TourCreationChoicesEnum()
		{
            while (!_isInit)
                yield return null;
            while (PopupBase.isPopupOpened)
                yield return null;

            ControllerTour tour = _dataManager.GetTourFromId(_createTourManager.tour.id);
            if (tour == null)
			{
                yield return _dataManager.SendTourEnum(_createTourManager.tour);
            }

            yield return new WaitForSeconds(0.2f);

            string tourData = "\n" + _createTourManager.tourName + "\n" + ControllerBase.ConvertDateDayHourMinute(_createTourManager.tourDate);
            string message = L.Get(TextManager.TOURS_CREATION_DETECTED).Replace("%TOUR_DATA%", tourData);
            MainUI.instance.ShowChoicePopup(L.Get(TextManager.TOURS_CREATION_TITLE), message,
                L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO), OnTourCreationContinue);

            while (PopupBase.isPopupOpened || _mode != Mode.NONE)
			{
                yield return new WaitForSeconds(1f);
			}

            CheckNotFinalizedTours();
        }

        private void OnTourCreationContinue(bool result)
		{
            if (result)
            {
                Vector2 pos = Vector2.zero;
                Vector2[] points = _createTourManager.tour.points;
                if (points != null && points.Length > 0)
                    pos = points[0];
                else
                    pos = ConvertCoordsToVector2(_createTourManager.tourCoords);
                SetMapOnPosition(pos.x, pos.y, _maps.floatZoom);

                ControllerTour tour = _dataManager.GetTourFromId(_createTourManager.tour.id);
                if (tour == null)
				{
                    tour = _createTourManager.tour;
                    _dataManager.tours.Add(tour);
                }
                else
				{
                    RemovePathLine(FindPathLineById(tour.id));
                    tour.UpdateBody(_createTourManager.tour.body);
                    tour.linkedAnnotations = _createTourManager.tour.linkedAnnotations;
                }
                    
                OnCreateTour(tour);

                if (_createTourManager.gps)
                {
                    MainUI.instance.ShowChoicePopup(L.Get(TextManager.TOURS_CREATION_TITLE), L.Get(TextManager.TOURS_CREATION_MODE_CHOICE),
                        L.Get(TextManager.TOURS_CREATION_MODE_GPS), L.Get(TextManager.TOURS_CREATION_MODE_MANUAL), OnTourCreationModeChoice);
                }
                else
				{
                    MainUI.instance.ActivateTourEdition(false);
                }
            }
            else
            {
                _tour = _dataManager.GetTourFromId(_createTourManager.tour.id);
                if (_tour != null)
                    MainUI.instance.ShowEditTourPopup(_tour, openToDelete:true, onDeleteResult:OnDeleteTourResult);
            }
        }

        public void CheckNotFinalizedTours()
		{
            StartCoroutine(CheckNotFinalizedToursEnum());
        }

        private IEnumerator CheckNotFinalizedToursEnum()
		{
            while (!_isInit)
                yield return null;
            while (PopupBase.isPopupOpened)
                yield return null;

            yield return new WaitForSeconds(0.2f);

            List<ControllerTour> badTours = null;

            foreach (ControllerTour tour in _dataManager.tours)
			{
                if (tour.userId == user.id && tour.pointCount < 2)
				{
                    if (badTours == null)
                        badTours = new List<ControllerTour>();
                    Debug.Log("[TOUR_CREATION] Detected bad tours " + tour.id + " - " + tour.name);
                    badTours.Add(tour);
                }
			}

            if (badTours != null)
			{
                foreach (ControllerTour tour in badTours)
                {
                    tour.canEdit = true;
                    _createTourManager.Init(tour, FindPathLineById(tour.id), GetCenterCoords());

                    string tourData = "\n" + tour.name + "\n" + ControllerBase.ConvertDateDayHourMinute(tour.date);
                    string message = L.Get(TextManager.TOURS_CREATION_DETECTED).Replace("%TOUR_DATA%", tourData);
                    MainUI.instance.ShowChoicePopup(L.Get(TextManager.TOURS_CREATION_TITLE), message,
                        L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO), OnTourCreationContinue);

                    while (PopupBase.isPopupOpened || _mode != Mode.NONE)
                    {
                        yield return new WaitForSeconds(1f);
                    }
                }
            }
        }

        private void OnDeleteTourResult(bool result)
		{
            if (result)
                return;

            foreach (PopupBase popup in PopupBase.popups)
            {
                if (popup is EditTourPopup)
                    popup.Close();
            }
            OnTourCreationDetected();
        }

        private void OnTourCreationModeChoice(bool gps)
		{
            if (gps)
			{
                MainUI.instance.ActivateTourGPS(false);
            }
            else
			{
                _createTourManager.tour.canEdit = true;
                MainUI.instance.ActivateTourEdition(false);
            }
		}

        private void CreateMsgPicture(Texture2D tex)
        {
            MsgPicture msg = MainUI.instance.CreatePicture();
            msg.Init(_currentPointLongitude, _currentPointLatitude, OnlineMapsTileSetControl.instance);
            msg.SetTexture(tex);
            _messages.Add(msg);
        }

        private void CreateMsgIcon(Sprite sprite)
        {
            MsgIcon msg = MainUI.instance.CreateIcon();
            msg.Init(_currentPointLongitude, _currentPointLatitude, OnlineMapsTileSetControl.instance);
            msg.SetIconSprite(sprite);
            _messages.Add(msg);
        }

        private MsgText CreateMsgText(string text, float lon, float lat)
        {
            MsgText msg = MainUI.instance.CreateText();
            msg.Init(lon, lat, OnlineMapsTileSetControl.instance);
            msg.SetText(text);
            msg.SetVisible(true);
            return msg;
        }

        private MsgText CreateMsgText(string text)
        {
            MsgText msg = CreateMsgText(text, _currentPointLongitude, _currentPointLatitude);
            _messages.Add(msg);
            return msg;
        }

        private void ClearData()
        {
            if (_dataManager.annotations != null)
            {
                int count = _dataManager.annotations.Count;
                for (int i = count - 1; i >= 0; i--)
                {
                    RemoveAnnotation(_dataManager.annotations[i]);
                }
            }
            if (_dataManager.tours != null)
            {
                int count = _dataManager.tours.Count;
                for (int i = count - 1; i >= 0; i--)
                {
                    RemoveTour(_dataManager.tours[i]);
                }
            }
            if (_messagesGroup != null && _messagesGroup.Count > 0)
            {
                foreach (MessagesGroup group in _messagesGroup)
                    group.Clean();
                _messagesGroup.Clear();
            }
            ClearMoveMessages();
            ClearFollowPaths();
        }

        private void ClearMoveMessages()
		{
            if (_moveMessages != null && _moveMessages.Count > 0)
            {
                foreach (MsgMove move in _moveMessages)
                    GameObject.Destroy(move.gameObject);
                _moveMessages.Clear();
                _moveMessages = null;
            }
        }

        private void ClearFollowPaths()
        {
            if (_followPaths != null && _followPaths.Count > 0)
            {
                foreach (FollowPath follow in _followPaths)
                    GameObject.Destroy(follow);
                _followPaths.Clear();
                _followPaths = null;
            }
        }

        private void GetAllData(bool clear = false, bool request = false)
        {
            if (clear)
                ClearData();
            if (_offlineMode)
			{
                _dataManager.SetAllAnnotations(_cacheData.GetAllAnnotations());
                _dataManager.SetAllTours(_cacheData.GetAllTours());
                _dataManager.SetAllMessages(_cacheData.GetAllMessages());
                StartCoroutine(GetAllDataEnum(false));
                return;
			}
            StartCoroutine(GetAllDataEnum(request));
        }

        private IEnumerator GetAllDataEnum(bool requestData = true)
        {
            _isInit = false;

            MainUI.instance.LoadingData(true);

            if (requestData)
			{
                MainUI.instance.ShowLoadingPopup();
                // Configs
                yield return _dataManager.GetAllConfigs();
                // Groups
                yield return _dataManager.GetAllGroups();
                _groupsManager.InitFromController(_user);
                // Annotations
                yield return _dataManager.GetAllAnnotations();
            }

            if (_dataManager.annotations != null)
            {
                int instanceCounter = 0;
                int instanceMaxByFrame = 30;

                foreach (ControllerAnnotation annotation in _dataManager.annotations)
                {
                    ControllerUser annotationUser = annotation.GetUser();
                    bool show = CanShowPost(annotationUser, annotation.share);
                    bool filtered = false;
                    if (!_filterManager.IsAnnotationVisible(annotation, annotationUser))
                        filtered = true;
                    MsgBase msg = FindMessageById(annotation.id);
                    if (msg != null)
					{
                        if (show)
						{
                            msg.SetVisible(false);
                            msg.SetFiltered(filtered);
                        }
                        else
						{
                            RemoveMessage(msg);
						}
                    }
                    else
					{
                        if (show)
                        {
                            msg = CreateMsgMixte(annotation);
                            msg.SetVisible(false);
                            msg.SetFiltered(filtered);
                            instanceCounter++;
                            if (instanceCounter % instanceMaxByFrame == 0)
                                yield return null;
                        }
                    }
                }
            }

            if (requestData)
                yield return _dataManager.GetAllTours();

            yield return new WaitForSeconds(0.2f);

            if (_dataManager.tours != null)
            {
                int instanceCounter = 0;
                int instanceMaxByFrame = 20;

                foreach (ControllerTour tour in _dataManager.tours)
                {
                    ControllerUser tourUser = tour.GetUser();
                    bool show = CanShowPost(tourUser, tour.share);
                    bool filtered = false;
                    if (!_filterManager.IsTourVisible(tour, tourUser))
                        filtered = true;
                    PathLine line = FindPathLineById(tour.id);
                    if (line != null)
					{
                        if (show)
						{
                            line.SetFiltered(filtered);
                        }
                        else
						{
                            RemovePathLine(line);
                            RemoveMessagesFromPathLine(line);
                        }
                    }
                    else
					{
                        if (show)
                        {
                            line = CreatePathLineFromTour(tour);
                            line.SetFiltered(filtered);
                            instanceCounter++;
                            if (instanceCounter % instanceMaxByFrame == 0)
                                yield return null;
                        }
                    }                    
                }
            }

            yield return new WaitForSeconds(0.2f);

            UpdateMessagesVisibility();

            if (requestData)
			{
                string userId = _user.id;
                // Favorites
                yield return _dataManager.GetAllFavorites();
                ControllerFavorites favorites = _dataManager.GetFavoritesFromUser(userId);
                if (favorites != null)
                {
                    _favoritesManager.InitFromController(favorites);
                }
                // Emojis
                yield return _dataManager.GetAllEmojis();
                ControllerEmojis emojis = _dataManager.GetEmojisFromUser(userId);
                if (emojis != null)
                {
                    _emojisManager.InitFromController(emojis);
                }
                // Messages
                yield return _dataManager.GetAllMessages();
                // Notifications
                yield return _dataManager.GetAllNotifications(_user.id);

                MainUI.instance.HideLoadingPopup();
            }

            MainUI.instance.LoadingData(false);

            if (!_offlineMode)
            {
                SaveAllDataToCache();
            }

            StartCoroutine(UpdateNotifsEnum());

            _isInit = true;
        }

        private IEnumerator UpdateDevicesEnum()
		{
            float time = Time.time;
            while (Time.time - time < 2f)
			{
                if (_firebaseToken != null)
                    break;
                yield return null;
			}
            if (_user != null)
            {
                _user.UpdateDevices();
                _dataManager.UpdateUserDevices(_user.id, _user.devices);
            }
        }

        private IEnumerator UpdateNotifsEnum()
		{
            MainUI.instance.SetFilterNotifCount(_filterManager.GetFilterCount());

            int notifNotReadCount = _dataManager.GetNotReadNotificationCount();
            MainUI.instance.SetNotificationsNotifCount(notifNotReadCount);
            if (notifNotReadCount > 0)
            {
                List<ControllerNotification> notifsToOpen = _dataManager.GetNotReadNotificationsToOpenAtLogin();
                if (notifsToOpen != null)
                {
                    do
                    {
                        if (PopupBase.isPopupOpened)
                        {
                            while (PopupBase.isPopupOpened)
                                yield return null;
                        }
                        yield return new WaitForSeconds(1f);
                    }
                    while (PopupBase.isPopupOpened);

                    for (int i = notifsToOpen.Count - 1; i >= 0; --i)
					{
                        MainUI.instance.ShowNotificationPopup(notifsToOpen[i]);
                    }
                }
            }
        }

        public bool CanShowPost(ControllerUser postUser, string share)
        {
            bool isPublic = share == SharePopup.PUBLIC;
            bool isInShareGroups = groupsManager.IsUserInShareGroups(share);
            bool isVisible = isPublic || isInShareGroups;
            bool isMe = postUser == _user;
            bool isTestgroupOk = _user.admin || postUser.testgroup == _user.testgroup;
            bool show = false;
            switch (_visibility)
            {
                case Visibility.ONLY_ME:
                    show = isMe;
                    break;
                case Visibility.GROUPS:
                    show = isInShareGroups;
                    break;
                case Visibility.PUBLIC:
                    show = isTestgroupOk && (isMe || isVisible);
                    break;
                case Visibility.ALL:
                    show = isTestgroupOk;
                    break;
                case Visibility.ALL_WITHOUT_ME:
                    show = isTestgroupOk && !isMe && (_user.admin || isVisible);
                    break;
            }
            return show;
        }

        private void SaveAllDataToCache()
		{
            if (_cacheData != null && _dataManager != null && _isInit)
			{
                _cacheData.SaveAllUsers(_dataManager.users);
                _cacheData.SaveAllAnotations(_dataManager.annotations);
                _cacheData.SaveAllTours(_dataManager.tours);
                _cacheData.SaveAllMessages(_dataManager.messages);
            }            
        }

        private void UpdateMessagesVisibility()
        {
            if (_messagesGroup == null)
            {
                _messagesGroup = new List<MessagesGroup>();
            }
            else
            {
                foreach (MessagesGroup group in _messagesGroup)
                    group.Clean();
                _messagesGroup.Clear();
            }

            if (_messages == null)
                return;

            if (isInPreviewTour)
                return;

            int count = _messages.Count;

            if (count > 0)
            {
                foreach (MsgBase msg in _messages)
                    msg.Render(true, false);
            }

            for (int i = 0; i < count; ++i)
            {
                _messages[i].SetGrouped(false);
                _messages[i].SetOffset(Vector2.zero);
            }

            Vector2 offsetToSeperate = new Vector2(64f, 32f);
            
            for (int i = 0; i < count - 1; ++i)
            {
                MsgBase msg1 = _messages[i];
                if (!msg1.isGrouped && !msg1.isFiltered && _msgToMove != msg1)
                {
                    for (int j = i + 1; j < count; ++j)
                    {
                        MsgBase msg2 = _messages[j];
                        if (!msg2.isGrouped && !msg2.isFiltered && _msgToMove != msg2)
                        {
                            if (AreMessagesTooClose(msg1, msg2))
                            {
                                if (_maps.floatZoom >= 19f)
                                {
                                    msg2.AddOffset(offsetToSeperate);
                                }
                                else
                                {
                                    MessagesGroup group = GetMessagesGroupFromPos((msg1.uiPos + msg2.uiPos) * 0.5f);
                                    if (group != null)
                                    {
                                        AddOnMessageGroup(group, msg1);
                                        AddOnMessageGroup(group, msg2);
                                    }
                                    else
                                    {
                                        CreateMessageGroup(msg1, msg2);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < count; ++i)
                _messages[i].SetVisible(!_messages[i].isGrouped);
        }

        private bool AreMessagesTooClose(MsgBase msg1, MsgBase msg2)
        {
            Vector2 pos1 = msg1.uiPos;
            Vector2 pos2 = msg2.uiPos;
            return AreClosePositions(pos1, pos2);
        }

        private bool AreClosePositions(Vector2 pos1, Vector2 pos2)
        {
            float sqrDist = (pos1 - pos2).sqrMagnitude;
            return sqrDist < 2500f;
        }

        private MessagesGroup CreateMessageGroup(MsgBase msg1, MsgBase msg2)
        {
            MessagesGroup group = new MessagesGroup();
            group.message = CreateMsgText("2", (msg1.longitude + msg2.longitude) * 0.5f, (msg1.latitude + msg2.latitude) * 0.5f);
            group.messages = new List<MsgBase>();
            group.messages.Add(msg1);
            group.messages.Add(msg2);
            group.uiPos = (msg1.uiPos + msg2.uiPos) * 0.5f;
            msg1.SetGrouped(true);
            msg2.SetGrouped(true);
            _messagesGroup.Add(group);
            return group;
        }

        private void AddOnMessageGroup(MessagesGroup group, MsgBase msg)
        {
            if (group != null)
            {
                if (!group.messages.Contains(msg))
                {
                    group.messages.Add(msg);
                    int count = group.messages.Count;
                    group.uiPos = (group.uiPos * (count - 1) + msg.uiPos) / count;
                    group.message.SetText(count.ToString());
                    msg.SetGrouped(true);
                }
            }
        }

        private MessagesGroup GetMessagesGroupFromPos(Vector2 pos)
        {
            foreach (MessagesGroup group in _messagesGroup)
            {
                if (AreClosePositions(group.uiPos, pos))
                {
                    return group;
                }
            }
            return null;
        }

        private GameObject MapsGetTargetGameObject(Vector3 position)
        {
            GameObject defaultGo = _mapsForwarder.image.gameObject;

            PointerEventData pe = new PointerEventData(EventSystem.current);
            pe.position = position;

            List<RaycastResult> hits = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pe, hits);
            if (hits.Count == 0) return defaultGo;

            GameObject result = hits[0].gameObject;

            if (result.GetComponentInParent<MsgBase>() != null)
                return defaultGo;
            if (result.tag == "AllowMaps")
                return defaultGo;

            return result;
        }

        private void OnConnectionResult(bool isOk)
		{
            MainUI.instance.SetInternetState(isOk);
            if (!isOk && _user != null && !_offlineMode && !_waitingChoice)
			{
                _waitingChoice = true;
                MainUI.instance.ShowChoicePopup(L.Get(TextManager.LOGIN_TITLE), L.Get(TextManager.OFFLINE_CHOICE), L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO),
                        (bool result) =>
                        {
                            if (result)
                            {
                                SaveAllDataToCache();
                                _offlineMode = true;
                                Debug.Log("[OFFLINE] " + _offlineMode);

                                // kTBS
                                Dictionary<string, object> trace = new Dictionary<string, object>()
                                {
                                    {"@type", "m:goOffline"},
                                    {"m:userId", user.id},
                                    {"m:keepOn", "TRUE"}
                                };
                                TraceManager.DispatchEventTraceToManager(trace);
                                ///
                            }
                            else
                            {
                                Debug.Log("Connexion Lost!");
                                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.LOGOUT_TITLE), L.Get(TextManager.LOGOUT_NO_INTERNET), FeedbackPopup.IconType.Alert);
                                // kTBS
                                Dictionary<string, object> trace = new Dictionary<string, object>()
                                {
                                    {"@type", "m:goOffline"},
                                    {"m:userId", user.id},
                                    {"m:keepOn", "FALSE"}
                                };
                                TraceManager.DispatchEventTraceToManager(trace);
                                ///

                                SetUser(null);
                            }
                            _waitingChoice = false;
                        });
			}
            else if (isOk && _user != null && _offlineMode && !_waitingChoice && !_wantToStayOffline)
            {
                _waitingChoice = true;
                Debug.Log("Connexion is back!");
                MainUI.instance.ShowChoicePopup(L.Get(TextManager.LOGIN_TITLE), L.Get(TextManager.SYNCHRO_CHOICE), L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO),
                        (bool result) =>
                        {
                            if (result)
                            {
                                _offlineMode = false;
                                Debug.Log("[OFFLINE] " + _offlineMode);
                                // TODO Synchro
                                StartCoroutine(SynchroDataEnum());      

                                // kTBS
                                Dictionary<string, object> trace = new Dictionary<string, object>()
                                {
                                    {"@type", "m:synchrOnline"},
                                    {"m:userId", user.id},
                                    {"m:synchronize", "TRUE"}
                                };
                                TraceManager.DispatchEventTraceToManager(trace);
                                ///                          
                            }
                            else
                            {
                                _wantToStayOffline = true;

                                // kTBS
                                Dictionary<string, object> trace = new Dictionary<string, object>()
                                {
                                    {"@type", "m:synchrOnline"},
                                    {"m:userId", user.id},
                                    {"m:synchronize", "FALSE"}
                                };
                                TraceManager.DispatchEventTraceToManager(trace);
                                /// 
                            }
                            _waitingChoice = false;
                        });
            }
		}

        private IEnumerator SynchroDataEnum()
		{
            MainUI.instance.ShowLoadingPopup();

            // keep current data
            List<ControllerAnnotation> annotations = _dataManager.annotations;
            List<ControllerTour> tours = _dataManager.tours;
            List<ControllerMessage> messages = _dataManager.messages;
            // List annotation Ids
            List<string> annotationIds = new List<string>();
            // List tour Ids
            List<string> tourIds = new List<string>();
            // List message Ids
            List<string> messageIds = new List<string>();
            // Collect new IDs
            Dictionary<string, string> convertIds = new Dictionary<string, string>();
            // Login
            ControllerUser.GetLoginData(out string mail, out string pass);
            if (!string.IsNullOrEmpty(mail) && !string.IsNullOrEmpty(pass))
            {
                yield return _dataManager.Login(mail, pass);
            }
            // Check token
            if (string.IsNullOrEmpty(_dataManager.token))
			{
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.LOGOUT_TITLE), L.Get(TextManager.LOGOUT_SUCCESS), FeedbackPopup.IconType.Alert);
                SetUser(null);
                yield break;
            }

            // Get all server data
            yield return GetAllDataEnum(true);

            // Deactivate KTBS
            TraceManager.traceEnabled = false;

            // Update tours 
            foreach (ControllerTour tour in tours)
            {
                if (tour.id.Contains(OfflineIds.OFFLINE_TAG))
                {
                    string oldId = tour.id;
                    Debug.Log("tour " + tour.id + " need to be synchro!");
                    yield return _dataManager.SendTourEnum(tour);
                    if (!_dataManager.lastResultFailed)
					{
                        foreach (ControllerAnnotation annotation in annotations)
                        {
                            if (annotation.tourId == oldId)
                                annotation.tourId = tour.id;
                        }
                        PathLine line = FindPathLineById(oldId);
                        if (line != null)
                            line.UpdateTourId(tour.id);
                        convertIds[oldId] = tour.id;
                    }
                }
                else
				{
                    ControllerTour serverTour = _dataManager.GetTourFromId(tour.id);
                    if (serverTour != null)
					{
                        if (!serverTour.CompareTo(tour))
                        {
                            if (tour.userId == _user.id)
                            {
                                Debug.Log("tour " + tour.id + " need to be synchro!");
                                yield return _dataManager.UpdateTour(serverTour, tour.body, tour.linkedAnnotations, ControllerTrace.MoveTourStatus.UPDATE.ToString());
                                yield return _dataManager.UpdateTourContentEnum(serverTour, tour.name, tour.share, tour.dateDisplay);
                            }
                        }
                    }
                    else
					{
                        Debug.Log("tour " + tour.id + " need to be removed!");
                        RemoveTour(tour);
					}
                }

                tourIds.Add(tour.id);
            }
            // Update annotations
            foreach (ControllerAnnotation annotation in annotations)
            {
                bool needToUpdate = false;

                if (!string.IsNullOrEmpty(annotation.imageId))
				{
                    if (annotation.imageId.Contains(OfflineIds.OFFLINE_TAG))
                    {
                        string oldId = annotation.imageId;
                        Debug.Log("annotation image " + oldId + " need to be synchro!");
                        WebServiceMediaObjectsUpload query = new WebServiceMediaObjectsUpload(_dataManager.token, name, annotation.tex.EncodeToPNG());
                        yield return query.Run();
                        if (!query.hasFailed)
                        {
                            Dictionary<string, object> dic = query.GetResultAsDic();
                            annotation.imageId = DicTools.GetValueString(dic, "id");
                            needToUpdate = true;
                            convertIds[oldId] = annotation.imageId;
                        }
                    }
                }
                
                if (annotation.id.Contains(OfflineIds.OFFLINE_TAG))
				{
                    string oldId = annotation.id;
                    Debug.Log("annotation " + oldId + " need to be synchro!");
                    yield return _dataManager.SendAnnotationEnum(annotation);
                    if (!_dataManager.lastResultFailed)
					{
                        MsgBase msg = FindMessageById(oldId);
                        if (msg != null && msg is MsgMixte)
						{
                            (msg as MsgMixte).SetId(annotation.id);
                        }
                        convertIds[oldId] = annotation.id;
                    }
                }
                else
				{
                    ControllerAnnotation serverAnnotation = _dataManager.GetAnnotationFromId(annotation.id);
                    if (serverAnnotation != null)
                    {
                        if (needToUpdate || !serverAnnotation.CompareTo(annotation))
                        {
                            if (annotation.userId == _user.id)
                            {
                                Debug.Log("annotation " + annotation.id + " need to be synchro!");
                                if (serverAnnotation.coords != annotation.coords)
                                    yield return _dataManager.UpdateAnnotationCoords(annotation, annotation.coords);
                                yield return _dataManager.UpdateAnnotationEnum(annotation);
                            }
                        }
                    }
                    else
                    {
                        Debug.Log("annotation " + annotation.id + " need to be removed!");
                        RemoveAnnotation(annotation);
                    }
                }

                annotationIds.Add(annotation.id);
            }

            // Update messages
            foreach (ControllerMessage message in messages)
            {
                bool needToUpdate = false;

                if (!string.IsNullOrEmpty(message.imageId))
                {
                    if (message.imageId.Contains(OfflineIds.OFFLINE_TAG))
                    {
                        string oldId = message.imageId;
                        Debug.Log("annotation image " + oldId + " need to be synchro!");
                        WebServiceMediaObjectsUpload query = new WebServiceMediaObjectsUpload(_dataManager.token, name, message.tex.EncodeToPNG());
                        yield return query.Run();
                        if (!query.hasFailed)
                        {
                            Dictionary<string, object> dic = query.GetResultAsDic();
                            message.imageId = DicTools.GetValueString(dic, "id");
                            needToUpdate = true;
                            convertIds[oldId] = message.imageId;
                        }
                    }
                }

                if (message.id.Contains(OfflineIds.OFFLINE_TAG))
                {
                    string oldId = message.id;
                    Debug.Log("message " + oldId + " need to be synchro!");
                    yield return _dataManager.CreateMessage(message.userId, message.comment, message.imageId, message.icons, message.emoticon, 
                        message.tags, message.annotationId, message.tourId, (ControllerMessage result) =>
                        {
                            message.id = result.id;
                            convertIds[oldId] = result.id;
                        }
                        );
                }
                else
                {
                    ControllerMessage serverMessage = _dataManager.GetMessageFromId(message.id);
                    if (serverMessage != null)
                    {
                        if (needToUpdate || !serverMessage.CompareTo(message))
                        {
                            if (message.userId == _user.id)
                            {
                                Debug.Log("message " + message.id + " need to be synchro!");
                                yield return _dataManager.UpdateMessage(message.id, message.comment, message.imageId, message.icons, message.emoticon, message.tags);
                            }
                        }
                    }
                    else
                    {
                        //Debug.Log("message " + message.id + " need to be removed!");
                    }
                }

                messageIds.Add(message.id);
            }

            // Delete Annotations
            List<ControllerAnnotation> annotationsToRemove = new List<ControllerAnnotation>();
            foreach (ControllerAnnotation dataAnnotation in _dataManager.annotations)
			{
                if (!annotationIds.Contains(dataAnnotation.id) && dataAnnotation.userId == _user.id)
				{
                    Debug.Log("annotation " + dataAnnotation.id + " need to be delete!");
                    _dataManager.DeleteAnnotation(dataAnnotation);
                    annotationsToRemove.Add(dataAnnotation);
                }   
			}
            foreach (ControllerAnnotation annotation in annotationsToRemove)
                RemoveAnnotation(annotation);

            // Delete Tours
            List<ControllerTour> toursToRemove = new List<ControllerTour>();
            foreach (ControllerTour dataTour in _dataManager.tours)
            {
                if (!tourIds.Contains(dataTour.id) && dataTour.userId == _user.id)
                {
                    Debug.Log("tour " + dataTour.id + " need to be delete!");
                    _dataManager.DeleteTour(dataTour, false);
                    toursToRemove.Add(dataTour);
                }
            }
            foreach (ControllerTour tour in toursToRemove)
                RemoveTour(tour);

            // Reactivate KTBS
            TraceManager.traceEnabled = true;

            ////////////// KTBS ////////////////
            yield return TraceManager.instance.SynchroniseToKTBS(convertIds);
            ///////////////////////////////////////////

            MainUI.instance.HideLoadingPopup();
        }
    }
}