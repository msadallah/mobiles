//#define TEST_TRACES

namespace Liris.App.Trace
{
	using AllMyScripts.Common.Tools;
	using Liris.App.Network;
	using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Networking;


    public class TraceManager : MonoBehaviour
    {
        private string _url = null;
       
        protected UnityWebRequest _request = null;
        protected string _response = null;
        
        private List<object> _offlinetraces = null;        
        
        public static TraceManager instance = null;
        public static bool traceEnabled = true;

        public static void DispatchEventTraceToManager(Dictionary<string, object> trace)
		{
            if (instance != null && traceEnabled)
			{
                instance.SaveTrack(trace);
            }
		}
        
        private void Awake()
        {
            instance = this;
        }

        private IEnumerator Start()
		{
            string json = PlayerPrefs.GetString("KTBS_TRACES");
            if (!string.IsNullOrEmpty(json))
            {
                _offlinetraces = JSON.Deserialize(json) as List<object>;
            }

            while (!CheckOffline.firstResult)
                yield return null;

            if (CheckOffline.lastResult)
			{
                while (!ApplicationManager.instance.dataManager.fetchedConfigs)
                    yield return null;

                _url = ApplicationManager.instance.dataManager.GetConfig("ktbsURL");
                Debug.Log($"<color=cyan>[KTBS] _url: {_url}</color>");

                if (_url != null)
                    yield return SynchroniseToKTBS();
			}
		}

		private void OnDestroy()
		{
            instance = null;
            if (_offlinetraces != null && _offlinetraces.Count > 0)
			{
                string json = JSON.Serialize(_offlinetraces);
                PlayerPrefs.SetString("KTBS_TRACES", json);
                _offlinetraces = null;
            }
        }

		public void GetLocation(System.Action<string> callback)
        {
            string location;

            if (!Input.location.isEnabledByUser)
            {
                Debug.Log("Le service de localisation n'est pas activé par l'utilisateur");
                location = "lon:0-lat:0";
                callback(location);
                return;
            }

            if (Input.location.status == LocationServiceStatus.Running)
            {
                float longitude = Input.location.lastData.longitude;
                float latitude = Input.location.lastData.latitude;
                //float altitude = Input.location.lastData.altitude;

                location = string.Format("lon:{0}-lat:{1}", longitude, latitude);
                callback(location);
                return;
            }

            StartCoroutine(GetLocationCoroutine(callback));
        }

        private IEnumerator GetLocationCoroutine(System.Action<string> callback)
        {
            string location;

            if (Input.location.status == LocationServiceStatus.Stopped)
			{
                Input.location.Start();
            }

            int maxWait = 20; 
            while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
            {
                yield return new WaitForSeconds(1);
                maxWait--;
            }

            if (maxWait < 1 || Input.location.status == LocationServiceStatus.Failed)
            {
                Debug.Log("Le service de localisation n'a pas pu être initialisé");
                location = "lon:0-lat:0";
                callback(location);
                yield break;
            }

            float longitude = Input.location.lastData.longitude;
            float latitude = Input.location.lastData.latitude;
            //float altitude = Input.location.lastData.altitude;

            location = string.Format("lon:{0}-lat:{1}", longitude, latitude);
            callback(location);
        }
    
        public void SaveTrack(Dictionary<string, object> data)
        {
            long m_timestamp = UtilsDateFormat.GetTimestampFromUnixEpoch(System.DateTime.UtcNow);
            data.Add("begin", m_timestamp);

            GetLocation(location => {
                data.Add("m:position", location);

                if(ApplicationManager.instance == null || ApplicationManager.instance.offlineMode || !gameObject.activeInHierarchy || _url == null)
                {
                    if (_offlinetraces == null)
                        _offlinetraces = new List<object>();
                    _offlinetraces.Add(data);
                }
                else
                {
                    StartCoroutine(_postToKTBS(data));
                }
            });
        }

        public IEnumerator SynchroniseToKTBS(Dictionary<string, string> convertIds = null)
        {
            if (_offlinetraces != null && _offlinetraces.Count > 0)
			{
                List<Dictionary<string, object>> itemsToRemove = new List<Dictionary<string, object>>();
                foreach (Dictionary<string, object> trace in _offlinetraces)
                {
                    itemsToRemove.Add(trace);

                    if (convertIds != null)
					{
                        ConvertIdInTrace(trace, convertIds, "m:postId");
                        ConvertIdInTrace(trace, convertIds, "m:tourId");
                        ConvertIdInTrace(trace, convertIds, "m:imageId");                        
                    }

                    yield return _postToKTBS(trace);
                }

                foreach (Dictionary<string, object> item in itemsToRemove)
                {
                    _offlinetraces.Remove(item);
                }
            }
        }

        private void ConvertIdInTrace(Dictionary<string, object> trace, Dictionary<string, string> convertIds, string key)
		{
            if (trace.ContainsKey(key))
            {
                string id = DicTools.GetValueString(trace, key);
                if (id != null && convertIds.ContainsKey(id))
                {
                    Debug.Log($"<color=cyan>[KTBS] convert ID: {id} to {convertIds[id]}</color>");
                    trace[key] = convertIds[id];
                }
            }
        }

        private IEnumerator _postToKTBS(Dictionary<string, object> data)
        {
            string traceType = DicTools.GetValueString(data, "@type");
            if (string.IsNullOrEmpty(traceType))
			{
                Debug.LogWarning($"<color=red>[KTBS] trace has no type!");
                yield break;
            }

            string trace = JSON.Serialize(data);

            Debug.Log($"<color=cyan>[KTBS] trace: {trace}</color>");
#if UNITY_EDITOR && !TEST_TRACES
            yield return null;
            Debug.Log($"<color=cyan>[KTBS] {traceType} not send (in editor)!</color>");
#else

            byte[] postData = System.Text.Encoding.UTF8.GetBytes(trace);

            using (_request = new UnityWebRequest(_url, UnityWebRequest.kHttpVerbPOST))
            {
                _request.uploadHandler = new UploadHandlerRaw(postData);
                _request.downloadHandler = new DownloadHandlerBuffer();
                _request.SetRequestHeader("Content-Type", "application/json");
                _request.certificateHandler = new BypassCertificate();

                UnityWebRequestAsyncOperation operation = _request.SendWebRequest();

                while (!operation.isDone)
                    yield return null;
                
                if (_request.result == UnityWebRequest.Result.Success)
                {
                    _response = _request.downloadHandler.text;
                    Debug.Log($"<color=cyan>[KTBS] {traceType} success! </color> {_response}");
                }
                else
                {
                    Debug.LogWarning($"<color=red>[KTBS] {traceType} failed! (error {(int)_request.responseCode}:) </color> {_request.error} {_request.downloadHandler.text}  ");
                }
            }
#endif
        }
    }

    public class BypassCertificate : CertificateHandler
    {
        protected override bool ValidateCertificate(byte[] certificateData)
        {
            // Simply return true no matter what
            return true;
        }
    }
}
