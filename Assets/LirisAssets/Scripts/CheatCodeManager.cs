namespace Liris.App
{
	using UnityEngine;
	using IngameDebugConsole;
	using AllMyScripts.CheatCodes;
	using AllMyScripts.Common.Version;
	using AllMyScripts.Common.Tools;

	public class CheatCodeManager : MonoBehaviour, CheatCodes.ICheatCode, CheatCodes.ISpecialCheatCode
	{
		#region Enums

		public enum CheatCodeType
		{
			VERSION,
			FPS,
			SIZE,			
			CONSOLE,
			NO_INTERNET,
			Count
		}

		public enum SpecialCheatCodeType
		{
			FRAMERATE_XX,
			Count
		}

		#endregion

		#region Properties

		public static CheatCodeManager instance = null;

		public GUISkin m_guiSkin = null;
		public DebugLogManager debugLogManagerPrefab = null;

		private CheatCodes _cheatCodes = null;
		private ShowVersion _showVersion = null;
		private FPS _fps = null;
		private ShowSize _showSize = null;
		private DebugLogManager _debugLogManager = null;

		#endregion

		public void ShowDebugLogManager()
		{
			_debugLogManager = Instantiate(debugLogManagerPrefab);
		}

		void Awake()
		{
			instance = this;
		}

		// Use this for initialization
		void Start()
		{
			// CheatCodes Init
			_cheatCodes = gameObject.AddComponent<CheatCodes>();
			_cheatCodes.m_eTriggerMode = CheatCodes.TriggerMode.Corners;
			_cheatCodes.AddCheatCodeCbk(this);
			_cheatCodes.AddSpecialCheatCodeCbk(this);
			for (int i = 0; i < (int)CheatCodeType.Count; i++)
			{
				string sInput = ((CheatCodeType)i).ToString().ToLower().Replace("_", string.Empty);
				_cheatCodes.m_sCodeList.Add(sInput);
			}
			for (int i = 0; i < (int)SpecialCheatCodeType.Count; i++)
			{
				string sKey;
				int nLength;
				if (ConvertSpecialCheatCodeToKey((SpecialCheatCodeType)i, out sKey, out nLength))
				{
					_cheatCodes.m_dictSpecialCodes.Add(sKey, nLength);
				}
			}
		}

		private void OnDestroy()
		{
			ClearAllReferences();
		}

		public void ClearAllReferences()
		{
			if (_cheatCodes != null)
			{
				_cheatCodes.RemoveCheatCodeCbk(this);
				_cheatCodes.RemoveSpecialCheatCodeCbk(this);
				_cheatCodes = null;
			}
			instance = null;
		}

		// ----------------------------------------------------------------------------------
		// Created Raph 02/10/2015
		// ----------------------------------------------------------------------------------
		void CheatCodes.ICheatCode.OnCheatCode(int nCode)
		{
			string sAdditionalLog = string.Empty;

			CheatCodeType eCode = (CheatCodeType)nCode;
			switch (eCode)
			{
				case CheatCodeType.VERSION:
					if (_showVersion == null)
					{
						_showVersion = gameObject.AddComponent<ShowVersion>();
						_showVersion.m_skin = m_guiSkin;
						_showVersion.m_rectGui = new Rect(Screen.width * 0.5f, 5f, Screen.width * 0.5f - 5f, 50f);
						sAdditionalLog = "OK";
					}
					else
					{
						GameObject.Destroy(_showVersion);
						_showVersion = null;
						sAdditionalLog = "NONE";
					}
					break;

				case CheatCodeType.FPS:
					if (_fps == null)
					{
						_fps = gameObject.AddComponent<FPS>();
						_fps.m_skin = m_guiSkin;
						_fps.m_rectGui = new Rect(5, 5, Screen.width * 0.5f - 5f, 50);
						sAdditionalLog = "OK";
					}
					else
					{
						GameObject.Destroy(_fps);
						_fps = null;
						sAdditionalLog = "NONE";
					}
					break;

				case CheatCodeType.SIZE:
					if (_showSize == null)
					{
						_showSize = gameObject.AddComponent<ShowSize>();
						_showSize.m_skin = m_guiSkin;
						_showSize.m_rectGui = new Rect(Screen.width * 0.5f, 55, Screen.width * 0.5f - 5f, 50);
						sAdditionalLog = "OK";
					}
					else
					{
						GameObject.Destroy(_showSize);
						_showSize = null;
						sAdditionalLog = "NONE";
					}
					break;

				
				case CheatCodeType.CONSOLE:
					if (_debugLogManager == null)
					{
						_debugLogManager = Instantiate(debugLogManagerPrefab);
					}
					else
					{
						GameObject.Destroy(_debugLogManager.gameObject);
						_debugLogManager = null;
					}
					break;
				case CheatCodeType.NO_INTERNET:
					ApplicationManager.instance.NoInternet();
					break;
				
			}
			Debug.Log("CC " + eCode + " " + sAdditionalLog);
		}

		bool CheatCodes.ISpecialCheatCode.OnSpecialCheatCode(string sLeft, string sRight)
		{
			SpecialCheatCodeType eCheatCode = ConvertSpecialCheatCode(sLeft, sRight);
			bool bProcessed = OnSpecialCheatCode(eCheatCode, sRight);
			if (bProcessed)
				Debug.Log("CC " + sLeft.ToUpper() + " " + sRight);
			return bProcessed;
		}

		private bool OnSpecialCheatCode(SpecialCheatCodeType eCheatCode, string sRight)
		{
			switch (eCheatCode)
			{
				case SpecialCheatCodeType.FRAMERATE_XX:
					int nFramerate;
					if (int.TryParse(sRight, out nFramerate))
					{
						Application.targetFrameRate = nFramerate;
					}
					break;
			}
			return false;
		}

		public static SpecialCheatCodeType ConvertSpecialCheatCode(string sLeft, string sRight)
		{
			int nRightLength = sRight.Length;

			SpecialCheatCodeType[] cheatCodes = (SpecialCheatCodeType[])System.Enum.GetValues(typeof(SpecialCheatCodeType));
			int cheatCodeIndex = 0;
			SpecialCheatCodeType result = SpecialCheatCodeType.Count;
			while (cheatCodeIndex < cheatCodes.Length && result == SpecialCheatCodeType.Count)
			{
				string sKey;
				int nLength;
				if (ConvertSpecialCheatCodeToKey(cheatCodes[cheatCodeIndex], out sKey, out nLength))
				{
					if (string.CompareOrdinal(sLeft, sKey) == 0 && nRightLength == nLength - sKey.Length)
					{
						result = cheatCodes[cheatCodeIndex];
					}
				}

				++cheatCodeIndex;
			}

			return result;
		}

		public static bool ConvertSpecialCheatCodeToKey(SpecialCheatCodeType cheatCode, out string sKey, out int nLength)
		{
			string sCheat = cheatCode.ToString();
			string[] sSplit = sCheat.Split('_');
			if (sSplit.Length >= 2)
			{
				sKey = string.Join("", sSplit, 0, sSplit.Length - 1).ToLower();
				nLength = sKey.Length + sSplit[sSplit.Length - 1].Length;
				return true;
			}
			else
			{
				sKey = null;
				nLength = 0;
				return false;
			}
		}

	}
}