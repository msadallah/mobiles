//#define DEBUG_EMOJIS

namespace Liris.App.Emojis
{
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
	using Liris.App.Network;
	using System.Collections.Generic;
	using UnityEngine;

	public class EmojisManager : MonoBehaviour
	{
		public string userId => _user;
		public Dictionary<int, string> annotationDic => _annotationEmojis;
		public Dictionary<int, string> tourDic => _tourEmojis;

		private const string PP_USER_EMOJIS = "USER_EMOJIS_";

		private Dictionary<int, string> _annotationEmojis = null;
		private Dictionary<int, string> _tourEmojis = null;
		private bool _needToUpdateEmojis = false;
		private float _elapsedTime = 0f;
		private string _user = null;
		private ControllerEmojis _controller = null;

		public string GetAnnotationEmoji(string id)
		{
			int num = ConvertTools.ToInt32(id, -1);
			if (_annotationEmojis != null && _annotationEmojis.ContainsKey(num))
				return _annotationEmojis[num];
			return null;
		}

		public string GetTourEmoji(string id)
		{
			int num = ConvertTools.ToInt32(id, -1);
			if (_tourEmojis != null && _tourEmojis.ContainsKey(num))
				return _tourEmojis[num];
			return null;
		}

		public void InitFromController(ControllerEmojis emojis)
		{
			_controller = emojis;

			LogEmojis("InitFromController _needToUpdateFavorites " + _needToUpdateEmojis);
			if (_needToUpdateEmojis)
				return;

			if (emojis.annotationDic != null)
			{
				foreach (var keyval in emojis.annotationDic)
				{
					AddAnnotationEmoji(keyval.Key, keyval.Value);
				}
			}
			if (emojis.tourDic != null)
			{
				foreach (var keyval in emojis.tourDic)
				{
					AddTourEmoji(keyval.Key, keyval.Value);
				}
			}

			LogEmojis("_annotationFavorites " + _annotationEmojis.Count);
			LogEmojis("_tourFavorites " + _tourEmojis.Count);
		}

		private void Awake()
		{
			_annotationEmojis = new Dictionary<int, string>();
			_tourEmojis = new Dictionary<int, string>();
			ApplicationManager.onLoginEvent += OnUserLogin;
			ApplicationManager.onLogoutEvent += OnUserLogout;
		}

		private void OnDestroy()
		{
			ApplicationManager.onLoginEvent -= OnUserLogin;
			ApplicationManager.onLogoutEvent -= OnUserLogout;
			_annotationEmojis = null;
			_tourEmojis = null;
			_controller = null;
		}

		private void Update()
		{
			_elapsedTime += Time.deltaTime;
			if (_needToUpdateEmojis && _elapsedTime > 10f && _user != null && CheckOffline.lastResult && ApplicationManager.instance.dataManager.fetchedEmojis)
			{
				LogEmojis("_needToUpdateFavorites");
				string annotations = JSON.Serialize(_annotationEmojis);
				string tours = JSON.Serialize(_tourEmojis);
				ApplicationManager.instance.dataManager.UpdateEmojis(_user, annotations, tours);
				_needToUpdateEmojis = false;
				_elapsedTime = 0f;
			}
			else if (_elapsedTime > 10f)
			{
				_elapsedTime = 8f;
			}
		}

		public void AddAnnotationEmoji(int num, string emoji)
		{
			if (num >= 0)
			{
				if (_annotationEmojis.ContainsKey(num))
				{
					_annotationEmojis[num] = emoji;
				}
				else
				{
					_annotationEmojis.Add(num, emoji);
				}
				_needToUpdateEmojis = true;
			}
		}

		public void RemoveAnnotationEmoji(int num)
		{
			if (_annotationEmojis.ContainsKey(num))
			{
				_annotationEmojis.Remove(num);
				_needToUpdateEmojis = true;
			}
		}

		public void AddTourEmoji(int num, string emoji)
		{
			if (num >= 0)
			{
				if (_tourEmojis.ContainsKey(num))
				{
					_tourEmojis[num] = emoji;
				}
				else
				{
					_tourEmojis.Add(num, emoji);
				}
				_needToUpdateEmojis = true;
			}
		}

		public void RemoveTourEmoji(int num)
		{
			if (_tourEmojis.ContainsKey(num))
			{
				_tourEmojis.Remove(num);
				_needToUpdateEmojis = true;
			}
		}

		private void OnUserLogin(ControllerUser user)
		{
			_user = user.id;
			LogEmojis("OnUserLogin user " + _user);
			string json = PlayerPrefs.GetString(PP_USER_EMOJIS + _user);
			if (!string.IsNullOrEmpty(json))
			{
				LogEmojis("GetString json " + json);
				Dictionary<string, object> dic = JSON.Deserialize(json) as Dictionary<string, object>;

				_annotationEmojis = new Dictionary<int, string>();
				Dictionary<string, object> dicAnnotation = DicTools.GetValueDictionary(dic, "annotations");
				foreach (var keyval in dicAnnotation)
					_annotationEmojis.Add(ConvertTools.ToInt32(keyval.Key), ConvertTools.ToString(keyval.Value));

				_tourEmojis = new Dictionary<int, string>();
				Dictionary<string, object> dicTour = DicTools.GetValueDictionary(dic, "tours");
				foreach (var keyval in dicTour)
					_tourEmojis.Add(ConvertTools.ToInt32(keyval.Key), ConvertTools.ToString(keyval.Value));

				LogEmojis("_annotationEmojis " + _annotationEmojis.Count);
				LogEmojis("_tourEmojis " + _tourEmojis.Count);
				_needToUpdateEmojis = true;
			}
		}

		private void OnUserLogout(ControllerUser user)
		{
			Debug.Log("[EMOJIS] OnUserLogout _needToUpdateFavorites " + _needToUpdateEmojis);
			if (_user != null)
			{
				if (_needToUpdateEmojis)
				{
					Dictionary<string, object> dic = new Dictionary<string, object>();
					dic["annotations"] = _annotationEmojis;
					dic["tours"] = _tourEmojis;
					string json = JSON.Serialize(dic);
					LogEmojis("SetString for user " + _user + " json " + json);
					PlayerPrefs.SetString(PP_USER_EMOJIS + _user, json);
					_needToUpdateEmojis = false;
				}
				else
				{
					PlayerPrefs.SetString(PP_USER_EMOJIS + _user, string.Empty);
				}
				_user = null;
			}
		}

		[System.Diagnostics.Conditional("DEBUG_EMOJIS")]
		private void LogEmojis(string log)
		{
			Debug.Log("[EMOJIS] " + log);
		}
	}
}