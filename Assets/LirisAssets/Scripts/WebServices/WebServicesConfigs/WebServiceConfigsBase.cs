namespace Liris.App.WebServices
{
    public class WebServiceConfigsBase : WebServiceBase
    {
        public WebServiceConfigsBase(string token) : base(token)
        {
            URL += "/configs";
        }
    }
}
