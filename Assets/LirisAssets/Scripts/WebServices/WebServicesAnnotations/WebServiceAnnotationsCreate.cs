namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceAnnotationsCreate : WebServiceAnnotationsBase
    {
        public WebServiceAnnotationsCreate(string token, string comment, string imageId, string placeType, string placeIcon, string emoticon, 
            string tags, string coords, string share, string dateDisplay, string timing, string userId, string tourId = null) : base(token)
        {
            _requestType = RequestType.POST;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["comment"] = comment;
            if (!string.IsNullOrEmpty(imageId))
                dic["image"] = "/api/media_objects/" + imageId;
            dic["placeType"] = placeType;
            dic["placeIcon"] = placeIcon;
            dic["emoticon"] = emoticon;
            dic["tags"] = tags;
            dic["coords"] = coords;
            dic["user"] = "/api/users/" + userId;
            if (!string.IsNullOrEmpty(tourId))
                dic["tour"] = "/api/tours/" + tourId;
            dic["share"] = share;
            dic["date"] = GetDateNow();
            dic["dateModif"] = GetDateNow();
            dic["dateDisplay"] = dateDisplay;
            dic["timing"] = timing;
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
