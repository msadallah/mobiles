namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceAnnotationsUpdateTour : WebServiceAnnotationsBase
    {
        public WebServiceAnnotationsUpdateTour(string token, string id, string tourId) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.PATCH;

            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (!string.IsNullOrEmpty(tourId))
                dic["tour"] = "/api/tours/" + tourId;
            else
                dic["tour"] = null;
            dic["dateModif"] = GetDateNow();
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
