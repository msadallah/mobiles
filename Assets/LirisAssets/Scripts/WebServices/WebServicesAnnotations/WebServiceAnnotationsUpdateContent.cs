namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceAnnotationsUpdateContent : WebServiceAnnotationsBase
    {
        public WebServiceAnnotationsUpdateContent(string token, string id, string comment, string imageId, string placeType, string placeIcon, string emoticon, 
            string tags, string share, string dateDisplay, string timing) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.PATCH;

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["comment"] = comment;
            if (!string.IsNullOrEmpty(imageId))
                dic["image"] = "/api/media_objects/" + imageId;
            else
                dic["image"] = null;
            dic["placeType"] = placeType;
            dic["placeIcon"] = placeIcon;
            dic["emoticon"] = emoticon;
            dic["tags"] = tags;
            dic["share"] = share;
            dic["dateModif"] = GetDateNow();
            dic["dateDisplay"] = dateDisplay;
            dic["timing"] = timing;
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
