namespace Liris.App.WebServices
{
    public class WebServiceAnnotationsBase : WebServiceBase
    {
        public WebServiceAnnotationsBase(string token) : base(token)
        {
            URL += "/annotations";
        }
    }
}
