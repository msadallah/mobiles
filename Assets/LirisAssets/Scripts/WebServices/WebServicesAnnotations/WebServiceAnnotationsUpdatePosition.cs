namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceAnnotationsUpdatePosition : WebServiceAnnotationsBase
    {
        public WebServiceAnnotationsUpdatePosition(string token, string id, string coords) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.PATCH;

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["coords"] = coords;
            dic["dateModif"] = GetDateNow();
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
