namespace Liris.App.WebServices
{
    public class WebServiceAnnotationsDelete : WebServiceAnnotationsBase
    {
        public WebServiceAnnotationsDelete(string token, string id) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.DELETE;
        }
    }
}
