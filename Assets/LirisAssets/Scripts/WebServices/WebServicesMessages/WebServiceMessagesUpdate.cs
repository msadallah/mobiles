namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceMessagesUpdate : WebServiceMessagesBase
    {
        public WebServiceMessagesUpdate(string token, string id, string comment, string imageId, 
            string icons, string emoticon, string tags) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.PATCH;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["comment"] = comment;
            if (!string.IsNullOrEmpty(imageId))
                dic["image"] = "/api/media_objects/" + imageId;
            else
                dic["image"] = null;
            dic["icons"] = icons;
            dic["emoticon"] = emoticon;
            dic["tags"] = tags;
            dic["dateModif"] = GetDateNow();
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
