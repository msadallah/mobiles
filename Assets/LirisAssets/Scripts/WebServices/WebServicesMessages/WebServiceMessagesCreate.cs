namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceMessagesCreate : WebServiceMessagesBase
    {
        public WebServiceMessagesCreate(string token, string userId, string comment, string imageId,
            string icons, string emoticon, string tags, string annotationId, string tourId) : base(token)
        {
            _requestType = RequestType.POST;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["comment"] = comment;
            if (!string.IsNullOrEmpty(imageId))
                dic["image"] = "/api/media_objects/" + imageId;
            dic["icons"] = icons;
            dic["emoticon"] = emoticon;
            dic["tags"] = tags;
            dic["user"] = "/api/users/" + userId;
            if (!string.IsNullOrEmpty(tourId))
                dic["tour"] = "/api/tours/" + tourId;
            if (!string.IsNullOrEmpty(annotationId))
                dic["annotation"] = "/api/annotations/" + annotationId;
            dic["date"] = GetDateNow();
            dic["dateModif"] = GetDateNow();
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
