namespace Liris.App.WebServices
{
    public class WebServiceMessagesGetFromId : WebServiceMessagesBase
    {
        public WebServiceMessagesGetFromId(string token, string id) : base(token)
        {
            URL += $"/{id}";
        }
    }
}
