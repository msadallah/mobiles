namespace Liris.App.WebServices
{
    public class WebServiceMessagesDelete : WebServiceMessagesBase
    {
        public WebServiceMessagesDelete(string token, string id) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.DELETE;
        }
    }
}
