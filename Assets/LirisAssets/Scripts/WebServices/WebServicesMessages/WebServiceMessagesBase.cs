namespace Liris.App.WebServices
{
    public class WebServiceMessagesBase : WebServiceBase
    {
        public WebServiceMessagesBase(string token) : base(token)
        {
            URL += "/messages";
        }
    }
}
