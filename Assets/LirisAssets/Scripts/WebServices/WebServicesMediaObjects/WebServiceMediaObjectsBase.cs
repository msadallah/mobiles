namespace Liris.App.WebServices
{
    public class WebServiceMediaObjectsBase : WebServiceBase
    {
        public WebServiceMediaObjectsBase(string token) : base(token)
        {
            URL += "/media_objects";
        }
    }
}
