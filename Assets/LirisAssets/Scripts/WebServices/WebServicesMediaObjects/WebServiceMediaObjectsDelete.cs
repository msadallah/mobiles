namespace Liris.App.WebServices
{
    public class WebServiceMediaObjectsDelete : WebServiceMediaObjectsBase
    {
        public WebServiceMediaObjectsDelete(string token, string id) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.DELETE;
        }
    }
}
