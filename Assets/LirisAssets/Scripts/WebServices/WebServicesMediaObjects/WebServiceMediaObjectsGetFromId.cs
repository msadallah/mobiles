namespace Liris.App.WebServices
{
	public class WebServiceMediaObjectsGetFromId : WebServiceMediaObjectsBase
    {
        public WebServiceMediaObjectsGetFromId(string token, string id) : base(token)
        {
            URL += $"/{id}";
        }
    }
}
