namespace Liris.App.WebServices
{
	public class WebServiceMediaObjectsDownload : WebServiceBase
    {
        public WebServiceMediaObjectsDownload(string mediaName) : base()
        {
            URL = URL_BASE + mediaName;
        }
    }
}
