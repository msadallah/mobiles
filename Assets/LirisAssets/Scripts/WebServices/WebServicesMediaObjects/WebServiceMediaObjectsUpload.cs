namespace Liris.App.WebServices
{
	public class WebServiceMediaObjectsUpload : WebServiceMediaObjectsBase
    {
        public WebServiceMediaObjectsUpload(string token, string fileName, byte[] data) : base(token)
        {
            _requestType = RequestType.POST_FORM;
            _requestPostFormData = new UnityEngine.WWWForm();
            _requestPostFormData.AddBinaryData("file", data, fileName);
        }
    }
}
