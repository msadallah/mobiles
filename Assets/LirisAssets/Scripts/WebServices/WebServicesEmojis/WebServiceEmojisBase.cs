namespace Liris.App.WebServices
{
    public class WebServiceEmojisBase : WebServiceBase
    {
        public WebServiceEmojisBase(string token) : base(token)
        {
            URL += "/emoji";
        }
    }
}
