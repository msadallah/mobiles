namespace Liris.App.WebServices
{
    public class WebServiceEmojisGetFromId : WebServiceEmojisBase
    {
        public WebServiceEmojisGetFromId(string token, string id) : base(token)
        {
            URL += $"/{id}";
        }
    }
}
