namespace Liris.App.WebServices
{
    public class WebServiceFavoritesBase : WebServiceBase
    {
        public WebServiceFavoritesBase(string token) : base(token)
        {
            URL += "/favorites";
        }
    }
}
