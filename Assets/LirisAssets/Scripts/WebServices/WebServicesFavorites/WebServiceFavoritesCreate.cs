namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceFavoritesCreate : WebServiceFavoritesBase
    {
        public WebServiceFavoritesCreate(string token, string userId, string annotations, string tours) : base(token)
        {
            _requestType = RequestType.POST;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["user"] = "/api/users/" + userId;
            dic["annotations"] = annotations;
            dic["tours"] = tours;
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
