namespace Liris.App.WebServices
{
    public class WebServiceFavoritesGetFromId : WebServiceFavoritesBase
    {
        public WebServiceFavoritesGetFromId(string token, string id) : base(token)
        {
            URL += $"/{id}";
        }
    }
}
