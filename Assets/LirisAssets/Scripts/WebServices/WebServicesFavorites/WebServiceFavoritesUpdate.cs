namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceFavoritesUpdate : WebServiceFavoritesBase
    {
        public WebServiceFavoritesUpdate(string token, string id, string annotations, string tours) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.PATCH;

            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (!string.IsNullOrEmpty(annotations))
                dic["annotations"] = annotations;
            if (!string.IsNullOrEmpty(tours))
                dic["tours"] = tours;
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
