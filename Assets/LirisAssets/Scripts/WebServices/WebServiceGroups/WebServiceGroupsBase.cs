namespace Liris.App.WebServices
{
    public class WebServiceGroupsBase : WebServiceBase
    {
        public WebServiceGroupsBase(string token) : base(token)
        {
            URL += "/user_groups";
        }
    }
}
