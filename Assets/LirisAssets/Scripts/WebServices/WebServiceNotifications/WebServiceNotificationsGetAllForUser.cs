namespace Liris.App.WebServices
{
    public class WebServiceNotificationsGetAllForUser : WebServiceNotificationsBase
    {
        public WebServiceNotificationsGetAllForUser(string token, string userId) : base(token)
        {
            URL += $"?user={userId}";
        }
    }
}
