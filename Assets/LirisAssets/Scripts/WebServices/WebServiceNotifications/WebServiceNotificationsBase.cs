namespace Liris.App.WebServices
{
    public class WebServiceNotificationsBase : WebServiceBase
    {
        public WebServiceNotificationsBase(string token) : base(token)
        {
            URL += "/notifications";
        }
    }
}
