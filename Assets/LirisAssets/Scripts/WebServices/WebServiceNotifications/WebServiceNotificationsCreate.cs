namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceNotificationsCreate : WebServiceNotificationsBase
    {
        public WebServiceNotificationsCreate(string token, string destUserId, string title, string content, string category, string parameters = null, string expiredDate = null) : base(token)
        {
            _requestType = RequestType.POST;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["user"] = "/api/users/" + destUserId;
            dic["title"] = title;
            dic["content"] = content;
            dic["category"] = category;
            if (!string.IsNullOrEmpty(parameters))
                dic["parameters"] = parameters;
            if (!string.IsNullOrEmpty(expiredDate))
                dic["expiredDate"] = expiredDate;
            dic["date"] = GetDateNow();
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
