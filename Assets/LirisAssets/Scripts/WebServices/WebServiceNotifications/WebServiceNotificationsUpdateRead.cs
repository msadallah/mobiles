namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceNotificationsUpdateRead : WebServiceNotificationsBase
    {
        public WebServiceNotificationsUpdateRead(string token, string id) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.PATCH;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["readDate"] = GetDateNow();
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
