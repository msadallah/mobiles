namespace Liris.App.WebServices
{
    public class WebServiceNotificationsDelete : WebServiceNotificationsBase
    {
        public WebServiceNotificationsDelete(string token, string id) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.DELETE;
        }
    }
}
