namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceUsersUpdateDevices : WebServiceUsersBase
    {
        public WebServiceUsersUpdateDevices(string token, string id, string version) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.PATCH;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["devices"] = version;
            dic["dateModif"] = GetDateNow();
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
