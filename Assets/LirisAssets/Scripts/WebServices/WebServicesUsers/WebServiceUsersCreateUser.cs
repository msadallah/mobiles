namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceUsersCreateUser : WebServiceUsersBase
    {
        public WebServiceUsersCreateUser(string mail, string password) : base()
        {
            _requestType = RequestType.POST;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["mail"] = mail;
            dic["plainPassword"] = password;
            dic["date"] = GetDateNow();
            dic["dateModif"] = GetDateNow();
            _requestPostData = JSON.Serialize(dic);
        }

        public WebServiceUsersCreateUser(string username, string mail, string password, 
            string lastname, string firstname, string gender, string birthdate, string nationality,
            string address, string postalcode, string city, string arrivaldate,
            string institution, string diploma, string formation) : base()
        {
            _requestType = RequestType.POST;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["username"] = username;
            dic["mail"] = mail;
            dic["plainPassword"] = password;
            dic["lastname"] = lastname;
            dic["firstname"] = firstname;
            dic["gender"] = gender;
            dic["birthdate"] = birthdate;
            dic["nationality"] = nationality;
            dic["address"] = address;
            dic["postalcode"] = postalcode;
            dic["city"] = city;
            dic["arrivaldate"] = arrivaldate;
            dic["institution"] = institution;
            dic["diploma"] = diploma;
            dic["formation"] = formation;
            dic["date"] = GetDateNow();
            dic["dateModif"] = GetDateNow();
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
