namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceUsersUpdateGroups : WebServiceUsersBase
    {
        public WebServiceUsersUpdateGroups(string token, string id, string groups) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.PATCH;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["usergroups"] = groups;
            dic["dateModif"] = GetDateNow();
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
