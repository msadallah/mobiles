namespace Liris.App.WebServices
{
    public class WebServiceUsersBase : WebServiceBase
    {
        public WebServiceUsersBase() : base()
        {
            URL += "/users";
        }

        public WebServiceUsersBase(string token) : base(token)
        {
            URL += "/users";
        }
    }
}
