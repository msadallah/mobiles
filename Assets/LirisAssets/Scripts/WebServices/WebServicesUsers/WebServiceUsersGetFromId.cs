namespace Liris.App.WebServices
{
    public class WebServiceUsersGetFromId : WebServiceUsersBase
    {
        public WebServiceUsersGetFromId(string token, string id) : base(token)
        {
            URL += $"/{id}";
        }
    }
}
