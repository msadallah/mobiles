namespace Liris.App.WebServices
{
    public class WebServiceUsersGetAllNamesAndMails : WebServiceUsersBase
    {
        public WebServiceUsersGetAllNamesAndMails() : base()
        {
            URL += "_short";
        }
    }
}
