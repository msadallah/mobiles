namespace Liris.App.WebServices
{
	using AllMyScripts.Common.Tools;
	using System.Collections.Generic;

    public class WebServiceUsersValidDisclaimer : WebServiceUsersBase
    {
        public WebServiceUsersValidDisclaimer(string token, string id) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.PATCH;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["disclaimer"] = true;
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
