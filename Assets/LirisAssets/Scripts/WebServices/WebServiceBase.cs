//#define DEBUG_LOCAL

namespace Liris.App.WebServices
{
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
    using UnityEngine.Networking;

    public class WebServiceBase
    {
        public enum RequestType
        {
            GET,
            POST,
            POST_FORM,
            PATCH,
            DELETE
        }


        public const string URL_BASE = "PROJECT_DB_URL";
        public string URL = $"{URL_BASE}/api";
        public bool hasFailed => _hasFailed;
        public string error => _error;
        public int errorCode => _errorCode;
        public string response => _response;
        public byte[] responseAsBytes => _responseAsBytes;

        protected UnityWebRequest _request = null;
        protected RequestType _requestType = RequestType.GET;
        protected string _requestPostData = null;
        protected WWWForm _requestPostFormData = null;
        protected bool _hasFailed = false;
        protected string _response = null;
        protected byte[] _responseAsBytes = null;
        protected string _error = null;
        protected int _errorCode = 0;
        protected string _token = null;
        protected int _numPage = 0;

        public WebServiceBase()
        {
            Debug.Log($"WS {this} starting!");
        }

        public WebServiceBase(string token)
        {
            _token = token;
            Debug.Log($"WS {this} starting!");
        }

        public IEnumerator Run(int numPage = 0)
        {
            Debug.Log($"WS Run {this} on URL: {URL} with {_requestType}!");

            _hasFailed = false;
            _error = null;
            _errorCode = 0;
            _numPage = numPage;

            switch (_requestType)
            {
                case RequestType.GET:
                    yield return RunGetRequest();
                    break;
                case RequestType.POST:
                case RequestType.PATCH:
                    yield return RunPostRequest();
                    break;
                case RequestType.POST_FORM:
                    yield return RunPostFormRequest();
                    break;
                case RequestType.DELETE:
                    yield return RunDeleteRequest();
                    break;
            }
        }

        private IEnumerator RunGetRequest()
        {
            string url = URL;
            if (_numPage > 0)
			{
                if (url.Contains("?"))
                    url += "&page=" + _numPage;
                else
                    url += "?page=" + _numPage;
            }

            using (_request = UnityWebRequest.Get(url))
            {
                _request.SetRequestHeader("Content-Type", "application/json");
                
                if (!string.IsNullOrEmpty(_token))
                    _request.SetRequestHeader("Authorization", "Bearer " + _token);

                UnityWebRequestAsyncOperation operation = _request.SendWebRequest();

                while (!operation.isDone)
                    yield return null;

                if (_request.result == UnityWebRequest.Result.Success)
                {
                    _responseAsBytes = _request.downloadHandler.data;
                    _response = _request.downloadHandler.text;
                    Debug.Log($"<color=green>WS {this} success!</color> {_response}");
                }
                else
                {
                    Debug.Log($"<color=red>WS {this} failed!</color> {_request.error} {_request.downloadHandler.text}");
                    _hasFailed = true;
                    _error = _request.error;
                    _errorCode = (int)_request.responseCode;
                }
            }
        }

        private IEnumerator RunPostRequest()
        {
            Debug.Log($"<color=green>WS {this} RunPostRequest {_requestPostData}</color>");
            using (_request = UnityWebRequest.Put(URL, _requestPostData))
            {
                switch (_requestType)
                {
                    case RequestType.POST:
                        _request.SetRequestHeader("Content-Type", "application/json");
                        _request.method = UnityWebRequest.kHttpVerbPOST;
                        break;
                    case RequestType.PATCH:
                        _request.SetRequestHeader("Content-Type", "application/merge-patch+json");
                        _request.method = "PATCH";
                        break;
                }

                if (!string.IsNullOrEmpty(_token))
                    _request.SetRequestHeader("Authorization", "Bearer " + _token);

                UnityWebRequestAsyncOperation operation = _request.SendWebRequest();

                while (!operation.isDone)
                    yield return null;

                if (_request.result == UnityWebRequest.Result.Success)
                {
                    _response = _request.downloadHandler.text;
                    Debug.Log($"<color=green>WS {this} success!</color> {_response}");
                }
                else
                {
                    Debug.Log($"<color=red>WS {this} failed!</color> {_request.error} {_request.downloadHandler.text}");
                    _hasFailed = true;
                    _error = _request.error;
                    _errorCode = (int)_request.responseCode;
                }
            }
        }

        private IEnumerator RunPostFormRequest()
        {
            using (_request = UnityWebRequest.Post(URL, _requestPostFormData))
            {
                if (!string.IsNullOrEmpty(_token))
                    _request.SetRequestHeader("Authorization", "Bearer " + _token);

                UnityWebRequestAsyncOperation operation = _request.SendWebRequest();

                while (!operation.isDone)
                    yield return null;

                if (_request.result == UnityWebRequest.Result.Success)
                {
                    _response = _request.downloadHandler.text;
                    Debug.Log($"<color=green>WS {this} success!</color> {_response}");
                }
                else
                {
                    Debug.Log($"<color=red>WS {this} failed!</color> {_request.error} {_request.downloadHandler.text}");
                    _hasFailed = true;
                    _error = _request.error;
                    _errorCode = (int)_request.responseCode;
                }
            }
        }

        private IEnumerator RunDeleteRequest()
        {
            using (_request = UnityWebRequest.Delete(URL))
            {
                _request.SetRequestHeader("accept", "*/*");
                _request.downloadHandler = new DownloadHandlerBuffer();

                if (!string.IsNullOrEmpty(_token))
                    _request.SetRequestHeader("Authorization", "Bearer " + _token);

                UnityWebRequestAsyncOperation operation = _request.SendWebRequest();

                while (!operation.isDone)
                    yield return null;

                if (_request.result == UnityWebRequest.Result.Success)
                {
                    _response = _request.downloadHandler.text;
                    Debug.Log($"<color=green>WS {this} success!</color> {_response}");
                }
                else
                {
                    Debug.Log($"<color=red>WS {this} failed!</color> {_request.error} {_request.downloadHandler.text}");
                    _hasFailed = true;
                    _error = _request.error;
                    _errorCode = (int)_request.responseCode;
                }
            }
        }

        public List<object> GetResultAsList()
        {
            Dictionary<string, object> dic = JSON.Deserialize(response) as Dictionary<string, object>;
            return DicTools.GetValueList(dic, "hydra:member") as List<object>;
        }

        public int GetNextPage()
        {
            Dictionary<string, object> dic = JSON.Deserialize(response) as Dictionary<string, object>;
            if (dic.ContainsKey("hydra:view"))
            {
                Dictionary<string, object> dicView = DicTools.GetValue(dic, "hydra:view") as Dictionary<string, object>;
                if (dicView != null)
                {
                    string nextPage = DicTools.GetValueString(dicView, "hydra:next");
                    if (!string.IsNullOrEmpty(nextPage))
                    {
                        int idx = nextPage.LastIndexOf("page=");
                        if (idx > 0)
                        {
                            string page = nextPage.Substring(idx + 5);
                            if (int.TryParse(page, out int result))
                            {
                                return result;
                            }
                        }
                    }
                }
            }
            return 0;
        }

        public Dictionary<string, object> GetResultAsDic()
        {
            Dictionary<string, object> dic = JSON.Deserialize(response) as Dictionary<string, object>;
            if (dic.ContainsKey("hydra:member"))
                return DicTools.GetValue(dic, "hydra:member") as Dictionary<string, object>;
            else
                return dic;
        }

        public static string GetDateNow()
		{
            return ControllerBase.ConvertDateTimeToSend(System.DateTime.Now);
		}

    }
}
