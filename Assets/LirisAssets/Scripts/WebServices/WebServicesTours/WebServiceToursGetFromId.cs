namespace Liris.App.WebServices
{
    public class WebServiceToursGetFromId : WebServiceToursBase
    {
        public WebServiceToursGetFromId(string token, string id) : base(token)
        {
            URL += $"/{id}";
        }
    }
}
