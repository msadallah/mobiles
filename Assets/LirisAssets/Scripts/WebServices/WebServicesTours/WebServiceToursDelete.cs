namespace Liris.App.WebServices
{
    public class WebServiceToursDelete : WebServiceToursBase
    {
        public WebServiceToursDelete(string token, string id) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.DELETE;
        }
    }
}
