namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceToursCreateTour : WebServiceToursBase
    {
        public WebServiceToursCreateTour(string token, string userId, string name, string body, string share, bool canEdit, string dateDisplay) : base(token)
        {
            _requestType = RequestType.POST;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["user"] = "/api/users/" + userId;
            dic["name"] = name;
            dic["body"] = body;
            dic["share"] = share;
            dic["canEdit"] = canEdit;
            dic["date"] = GetDateNow();
            dic["dateModif"] = GetDateNow();
            dic["dateDisplay"] = dateDisplay;
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
