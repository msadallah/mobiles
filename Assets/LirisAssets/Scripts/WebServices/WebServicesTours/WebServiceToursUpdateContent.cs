namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceToursUpdateContent : WebServiceToursBase
    {
        public WebServiceToursUpdateContent(string token, string id, string name, string share, string dateDisplay) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.PATCH;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["name"] = name;
            dic["share"] = share;
            dic["dateModif"] = GetDateNow();
            dic["dateDisplay"] = dateDisplay;
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
