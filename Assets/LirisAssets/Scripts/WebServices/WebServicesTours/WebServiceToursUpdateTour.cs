namespace Liris.App.WebServices
{
    using AllMyScripts.Common.Tools;
    using System.Collections.Generic;

    public class WebServiceToursUpdateTour : WebServiceToursBase
    {
        public WebServiceToursUpdateTour(string token, string id, string body, string linkedAnnotations) : base(token)
        {
            URL += $"/{id}";
            _requestType = RequestType.PATCH;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["body"] = body;
            dic["linkedAnnotations"] = linkedAnnotations;
            dic["dateModif"] = GetDateNow();
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
