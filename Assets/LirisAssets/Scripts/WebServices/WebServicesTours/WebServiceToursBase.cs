namespace Liris.App.WebServices
{
    public class WebServiceToursBase : WebServiceBase
    {
        public WebServiceToursBase(string token) : base(token)
        {
            URL += "/tours";
        }
    }
}
