namespace Liris.App.WebServices
{
	using AllMyScripts.Common.Tools;
	using System.Collections.Generic;

    public class WebServiceLoginCheck : WebServiceBase
    {
        public WebServiceLoginCheck(string mail, string password) : base()
        {
            URL += "/login_check";

            _requestType = RequestType.POST;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["mail"] = mail;
            dic["password"] = password;
            _requestPostData = JSON.Serialize(dic);
        }
    }
}
