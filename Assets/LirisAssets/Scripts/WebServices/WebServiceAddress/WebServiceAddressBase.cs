namespace Liris.App.WebServices
{
    public class WebServiceAddressBase : WebServiceBase
    {
        public WebServiceAddressBase() : base()
		{
            URL = "https://api-adresse.data.gouv.fr/search/?q=";
        }
    }
}
