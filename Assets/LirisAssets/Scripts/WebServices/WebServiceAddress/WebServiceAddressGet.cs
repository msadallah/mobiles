using System.Globalization;

namespace Liris.App.WebServices
{
    public class WebServiceAddressGet : WebServiceAddressBase
    {
        public WebServiceAddressGet(string address, float[] coords = null) : base()
		{
            address = address.Replace(" ", "+");
            URL += address;
            if (coords != null && coords.Length == 2)
			{
                URL += $"&lon={coords[0].ToString(CultureInfo.InvariantCulture)}&lat={coords[1].ToString(CultureInfo.InvariantCulture)}";
            }
        }
    }
}
