namespace Liris.App.Tour
{
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
	using System.Collections.Generic;
	using UnityEngine;

    public class CreateTourManager : MonoBehaviour
    {
        private const string PP_LAST_TOUR_CREATION = "LAST_TOUR_CREATION_";


        public bool hasData => _tour != null && _userId != null;
        public ControllerTour tour => _tour;
        public string tourName => _tour.name;
        public string tourDate => _tour.date;
        public string tourCoords => _tourCoords;
        public bool gps => !_tour.canEdit;

        private ControllerTour _tour = null;
        private PathLine _pathLine = null;
        private string _tourCoords = null;
        private string _userId = null;

        public void Init(ControllerTour tour, PathLine pahtLine, string coords)
        {
            _tour = tour;
            _pathLine = pahtLine;
            _tourCoords = coords;
        }

        public void Reset()
		{
            _tour = null;
            _pathLine = null;
        }

        public void SaveInLocal()
		{
            Debug.Log("[TOUR_CREATION] SaveInLocal");
            PlayerPrefs.SetString(PP_LAST_TOUR_CREATION + _userId, SerializeData());
            PlayerPrefs.Save();
        }

        public void LoadFromLocal()
		{
            Debug.Log("[TOUR_CREATION] LoadFromLocal");
            string data = PlayerPrefs.GetString(PP_LAST_TOUR_CREATION + _userId);
            DeserializeData(data);
        }

        private void DeserializeData(string data)
		{
            Debug.Log("[TOUR_CREATION] DeserializeData " + data);
            if (data != null)
            {
                Dictionary<string, object> dic = JSON.Deserialize(data) as Dictionary<string, object>;
                if (dic != null)
                {
                    string tourJson = DicTools.GetValueString(dic, "tour");
                    string pathJson = DicTools.GetValueString(dic, "path");
                    string linksJson = DicTools.GetValueString(dic, "links");
                    _tour = new ControllerTour(tourJson);
                    _tourCoords = DicTools.GetValueString(dic, "coords");
                    _tour.UpdateBody(pathJson);
                    _tour.linkedAnnotations = linksJson;
                }
            }
            else
            {
                Reset();
            }
        }

        private string SerializeData()
		{
            Debug.Log("[TOUR_CREATION] SerializeData");
            if (_tour != null && _pathLine != null)
			{
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["tour"] = _tour.GetDataAsJson();
                dic["path"] = _pathLine.GetPositionDataAsString();
                dic["links"] = _pathLine.GetJsonFromLinkedAnnotations();
                dic["coords"] = _tourCoords;
                return JSON.Serialize(dic);
            }
            else
			{
                return string.Empty;
			}
        }

		private void Awake()
		{
            ApplicationManager.onLoginEvent += OnLogin;
            ApplicationManager.onLogoutEvent += OnLogout;
        }

        private void OnDestroy()
        {
            ApplicationManager.onLogoutEvent -= OnLogin;
            ApplicationManager.onLogoutEvent -= OnLogout;
        }

        private void OnLogin(ControllerUser user)
		{
            _userId = user.id;
            LoadFromLocal();
            if (hasData)
			{
                Debug.Log("[TOUR_CREATION] CREATE TOUR HAS DATA!");
                ApplicationManager.instance.OnTourCreationDetected();
			}
            else
			{
                Debug.Log("[TOUR_CREATION]CREATE TOUR HAS NO DATA!");
                ApplicationManager.instance.CheckNotFinalizedTours();
            }
        }

        private void OnLogout(ControllerUser user)
		{
            SaveInLocal();
            _userId = null;
        }

		private void OnApplicationQuit()
		{
            SaveInLocal();
        }

        private void OnApplicationPause(bool pause)
        {
            Debug.Log("OnApplicationPause " + pause);
            if (pause)
            {
                SaveInLocal();
            }
        }
    }
}
