namespace Liris.App.Popups
{
	using Liris.App.Tools;
	using System.Collections.Generic;
	using UnityEngine;

    public class PopupBase : MonoBehaviour
    {
        public static List<PopupBase> popups = null;
        public static bool isPopupOpened => _counter > 0;

        private static int _counter = 0;

        protected virtual void Awake()
        {
            if (popups == null)
                popups = new List<PopupBase>();
            popups.Add(this);
            _counter++;
            //Debug.Log("[POPUP] counter " + _counter);
        }

        protected virtual void OnDestroy()
        {
            _counter--;
            if (popups != null && popups.Contains(this))
                popups.Remove(this);
            //Debug.Log("[POPUP] counter " + _counter);
        }

        protected virtual void ClosePopup()
        {
            GameObject.Destroy(gameObject);
        }

        public void Close()
		{
            ClosePopup();
        }

        protected void TakePhoto(System.Action<Texture2D> result)
        {
            if (NativeCamera.DeviceHasCamera())
            {
                NativeCamera.TakePicture((string path) =>
                {
                    Debug.Log("[CAM] TakePicture path " + path);
                    if (path != null)
                    {
                        Texture2D tex = NativeCamera.LoadImageAtPath(path, -1, false, false);
                        if (tex != null)
                        {
                            TextureScale.Scale(tex, 1024, 1024);
                            if (ApplicationManager.instance.optionsManager.savePhotoInGallery)
                            {
                                string galleryName = "Photo_" + System.DateTime.Now.ToString().Replace(" ", "_").Replace("/", "-");
                                NativeGallery.SaveImageToGallery(tex, "ANR-Mobiles", galleryName);
                            }
                            result?.Invoke(tex);
                        }
                    }
                });
            }
        }

        protected void GetPhotoFromGallery(System.Action<Texture2D> result)
        {
            if (NativeGallery.CanSelectMultipleFilesFromGallery())
            {
                NativeGallery.GetImageFromGallery((string path) =>
                {
                    Debug.Log("[CAM] GetImageFromGallery path " + path);
                    if (path != null)
                    {
                        Texture2D tex = NativeCamera.LoadImageAtPath(path, -1, false, false);
                        if (tex != null)
                        {
                            TextureScale.Scale(tex, 1024, 1024);
                            result?.Invoke(tex);
                        }
                    }
                });
            }
        }
    }
}
