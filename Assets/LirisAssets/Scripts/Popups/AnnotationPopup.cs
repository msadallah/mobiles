namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;
    using System.Collections;
    using System.Collections.Generic;
    using TMPro;
	using Liris.App.Assets;
	using Liris.App.WebServices;
	using Liris.App.Controllers;
    using Liris.App.Trace;
	using CustomDatePicker;
	using AllMyScripts.LangManager;
	using AllMyScripts.Common.UI;
    using AllMyScripts.Common.Tools;


    public class AnnotationPopup : PopupBase
    {
        [Header("Title")]
        [SerializeField]
        private TextMeshProUGUI _topTitle = null;
        [Header("Date")]
        [SerializeField]
        private TextMeshProUGUI _dateLabel = null;
        [SerializeField]
        private GameObject _dateNotEditable = null;
        [SerializeField]
        private DatePickerTitle _datePicker = null;
        [Header("Timing")]
        [SerializeField]
        private TextMeshProUGUI _timingLabel = null;
        [Header("Place Type")]
        [SerializeField]
        private Image _placeTypeIcon = null;
        [SerializeField]
        private TextMeshProUGUI _placeTypeTitle = null;
        [SerializeField]
        private SpritesList _placeTypeSprites = null;
        [SerializeField]
        private Button _placeTypeButton = null;
        [Header("Icons")]
        [SerializeField]
        private Button _iconButton = null;
        [SerializeField]
        private SpritesList _iconsSprites = null;
        [SerializeField]
        private MsgIcon _iconPrefab;
        [SerializeField]
        private Transform _iconsParent;
        [Header("Image")]
        [SerializeField]
        private Button _photoButton = null;
        [SerializeField]
        private Button _galleryButton = null;
        [SerializeField]
        private Button _trashImageButton = null;
        [SerializeField]
        private RawImage _pictureRawImage = null;
        [Header("Message")]
        [SerializeField]
        private TMP_InputField _inputMessage = null;
        [Header("Tags")]
        [SerializeField]
        private Button _tagsButton = null;
        [SerializeField]
        private TextMeshProUGUI _tagsText = null;
        [Header("Emoticons")]
        [SerializeField]
        private Button _emoticonsButton = null;
        [SerializeField]
        private Image _emoticonsImage = null;
        [SerializeField]
        private SpritesList _emoticonsSprites = null;
        [SerializeField]
        private TextMeshProUGUI _emoticonText = null;
        [Header("Public/Private")]
        //[SerializeField]
        //private Toggle _privateToggle = null;
        //[SerializeField]
        //private Toggle _publicToggle = null;
        [SerializeField]
        private Button _visibilityButton = null;
        [SerializeField]
        private TextMeshProUGUI _visibilityButtonText = null;
        [Header("Back")]
        [SerializeField]
        private Button _backButton = null;
        [Header("Validation")]
        [SerializeField]
        private Button _okButton = null;

        public string selectedTiming => _selectedTiming;
        public string selectedDate => _selectedDate;
        public string selectedPlaceType => _selectedPlaceType;

        private string _selectedMessage = "";
        private string _selectedPlaceType = "";
        private string _selectedIcons = "";
        private string _selectedEmoticon = "";
        private string _selectedTags = "";
        private string _selectedDate = null;
        private string _selectedTiming = null;
        private string _selectedShare = null;
        private Sprite _defaultEmoticonSprite = null;
        private Texture2D _currentTex = null;
        //private bool _isPrivate = false;
        private bool _isTexCapture = false;
        private bool _hasChangedTex = false;
        private ControllerAnnotation _annotationInEdition = null;
        private System.DateTime _selectedDateTime;
        private System.DateTime _annotationInEditionDateTime;
        private ControllerTour _currentTour = null;

        public void SetPlaceTypeName(string name, string date, ApplicationManager.TimingType timing, ControllerTour tour)
        {
            _selectedPlaceType = name;
            _currentTour = tour;
            _selectedTiming = timing.ToString();
            SpritesList.SpriteData data = _placeTypeSprites.GetData(name);
            _placeTypeIcon.sprite = data.sprite;
            _placeTypeIcon.color = data.color;
            _placeTypeTitle.text = L.Get(_placeTypeSprites.GetTextIdFromData(data));
            _placeTypeTitle.color = data.color;
            _timingLabel.text = L.Get("id_timing_" + timing.ToString().ToLower());
            _timingLabel.color = data.color;
            if (!string.IsNullOrEmpty(date))
			{
                _selectedDate = date;
            }
            else if (string.IsNullOrEmpty(_selectedDate))
			{
                if (_currentTour != null && _currentTour.canEdit)
				{
                    _selectedDateTime = ControllerBase.ConvertInDateTime(_currentTour.dateDisplay);
                }
                else
				{
                    _selectedDateTime = System.DateTime.Now;
                }
                _selectedDate = CalendarController.ConvertDateToString(_selectedDateTime, true);
            }
            _dateNotEditable.SetActive(_currentTour != null && !_currentTour.canEdit);
            _datePicker.SetDate(_selectedDate, true);
            _dateLabel.text = _selectedDate;
            _dateLabel.color = data.color;
            CheckValidateButton();
            UpdateShareText();
        }

        public void SetEditionMode(ControllerAnnotation annotation, ControllerTour tour)
        {
            _annotationInEdition = annotation;
            _annotationInEditionDateTime = ControllerBase.ConvertInDateTime(_annotationInEdition.dateDisplay);
            _selectedDateTime = _annotationInEditionDateTime;
            _selectedDate = CalendarController.ConvertDateToString(_selectedDateTime, true);
            _selectedIcons = annotation.icons;
            _selectedEmoticon = annotation.emoticon;
            _selectedTags = annotation.tags;
            _selectedMessage = annotation.comment;
            _currentTex = annotation.tex;
            ApplicationManager.TimingType timing = ApplicationManager.TimingType.UNIQUE;
            if (System.Enum.TryParse<ApplicationManager.TimingType>(annotation.timing, out ApplicationManager.TimingType t))
                timing = t;
            if (_currentTex != null)
                ShowTexture(_currentTex);
            if (!string.IsNullOrEmpty(_selectedIcons))
                SetSelectedIcons(_selectedIcons);
            if (!string.IsNullOrEmpty(_selectedEmoticon))
                SetEmoticon(_selectedEmoticon);
            if (!string.IsNullOrEmpty(_selectedTags))
                SetTags(_selectedTags);
            if (!string.IsNullOrEmpty(_selectedMessage))
                SetMessage(_selectedMessage, true);
            //_isPrivate = !annotation.isPublic;
            _selectedShare = annotation.share;
            //_privateToggle.SetValue(_isPrivate);
            //_publicToggle.SetValue(!_isPrivate);
            _topTitle.gameObject.GetComponent<TextLocalisation>().SetId(TextManager.ANNOTATIONS_TITLE_EDITION);
            SetPlaceTypeName(annotation.placeType, _selectedDate, timing, tour);
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
            _visibilityButton.onClick.AddListener(OnVisibilityClicked);
            _placeTypeButton.onClick.AddListener(OnPlaceTypeClicked);
            _photoButton.onClick.AddListener(OnPhotoClicked);
            _galleryButton.onClick.AddListener(OnGalleryClicked);
            _trashImageButton.onClick.AddListener(OnTrashImageClicked);
            _iconButton.onClick.AddListener(OnIconClicked);
            _tagsButton.onClick.AddListener(OnTagsClicked);
            _emoticonsButton.onClick.AddListener(OnEmoticonsClicked);
            //_privateToggle.onValueChanged.AddListener(OnPrivateChanged);
            _inputMessage.onValueChanged.AddListener(OnMessageChanged);
            _datePicker.onDateSelected.AddListener(OnDateChanged);
            _pictureRawImage.gameObject.SetActive(false);
            _tagsText.text = "";
            _defaultEmoticonSprite = _emoticonsImage.sprite;
            _emoticonText.text = L.Get(_emoticonsSprites.GetTextIdFromName(_emoticonsSprites.GetFirstName()));
            _selectedShare = SharePopup.PUBLIC;
            UpdateShareText();
            CheckValidateButton();
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            _visibilityButton.onClick.RemoveListener(OnVisibilityClicked);
            _placeTypeButton.onClick.RemoveListener(OnPlaceTypeClicked);
            _photoButton.onClick.RemoveListener(OnPhotoClicked);
            _galleryButton.onClick.RemoveListener(OnGalleryClicked);
            _trashImageButton.onClick.RemoveListener(OnTrashImageClicked);
            _iconButton.onClick.RemoveListener(OnIconClicked);
            _tagsButton.onClick.RemoveListener(OnTagsClicked);
            _emoticonsButton.onClick.RemoveListener(OnEmoticonsClicked);
            //_privateToggle.onValueChanged.RemoveListener(OnPrivateChanged);
            _inputMessage.onValueChanged.RemoveListener(OnMessageChanged);
            _datePicker.onDateSelected.RemoveListener(OnDateChanged);
            _defaultEmoticonSprite = null;
            _currentTour = null;
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            if (_annotationInEdition != null)
                StartCoroutine(UpdateNotifEnum());
            else
                StartCoroutine(SendNotifEnum());
        }

        private void OnVisibilityClicked()
		{
            MainUI.instance.ShowSharePopupPrefab(_selectedShare, OnShareUpdated);
        }

        private void OnShareUpdated(string result)
		{
            _selectedShare = result;
            UpdateShareText();
            CheckValidateButton();
        }

        private void UpdateShareText()
		{
            _visibilityButtonText.text = SharePopup.GetShareText(_selectedShare);
        }

        private void OnPlaceTypeClicked()
		{
            MainUI.instance.ShowPlaceTypePopup(_currentTour, this);
        }

        private void CheckValidateButton()
        {
            _selectedDateTime = ControllerBase.ConvertInDateTime(_selectedDate);

            bool interactable = false;
            if (_annotationInEdition != null)
            {
                if (_selectedDateTime.Hour != _annotationInEditionDateTime.Hour)
                    interactable = true;
                if (_selectedDateTime.Minute != _annotationInEditionDateTime.Minute)
                    interactable = true;
                if (_selectedDateTime.Year != _annotationInEditionDateTime.Year)
                    interactable = true;
                if (_selectedDateTime.Month != _annotationInEditionDateTime.Month)
                    interactable = true;
                if (_selectedDateTime.Day != _annotationInEditionDateTime.Day)
                    interactable = true;
                if (_annotationInEdition.placeType != _selectedPlaceType)
                    interactable = true;
                if (_annotationInEdition.icons != _selectedIcons)
                    interactable = true;
                if (_annotationInEdition.emoticon != _selectedEmoticon)
                    interactable = true;
                if (_annotationInEdition.tags != _selectedTags)
                    interactable = true;
                if (_annotationInEdition.comment != _selectedMessage)
                    interactable = true;
                if (_annotationInEdition.tex != _currentTex)
                    interactable = true;
                if (_annotationInEdition.share != _selectedShare)
                    interactable = true;
                if (_annotationInEdition.timing != _selectedTiming)
                    interactable = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(_selectedIcons))
                    interactable = true;
                if (!string.IsNullOrEmpty(_selectedEmoticon))
                    interactable = true;
                if (!string.IsNullOrEmpty(_selectedTags))
                    interactable = true;
                if (!string.IsNullOrEmpty(_selectedMessage))
                    interactable = true;
                if (_currentTex != null)
                    interactable = true;
            }
            _okButton.interactable = interactable;
        }

        private IEnumerator SendNotifEnum()
        {
            MainUI.instance.ShowLoadingPopup();

            string token = ApplicationManager.instance.dataManager.token;

            string imageId = "";
            if (_currentTex != null)
            {
                string name = _isTexCapture ? "Capture.png" : "Gallery.png";
                if (ApplicationManager.instance.offlineMode)
                {
                    imageId = OfflineIds.GetId(OfflineIds.OfflineIdType.IMAGE);
                    SpriteLibrary.SetSprite(imageId, _currentTex, System.DateTime.Now.ToString(), format: "png");
                    SpriteLibrary.ApplySaving();
                }
                else
                {
                    WebServiceMediaObjectsUpload query = new WebServiceMediaObjectsUpload(token, name, _currentTex.EncodeToPNG());
                    yield return query.Run();
                    if (!query.hasFailed)
                    {
                        Dictionary<string, object> dic = query.GetResultAsDic();
                        imageId = DicTools.GetValueString(dic, "id");
                    }
                }
            }

            ControllerUser user = ApplicationManager.instance.user;
            if (user != null)
            {
                string coords = ApplicationManager.GetCurrentPointCoords();
                string tourId = ApplicationManager.instance.tour?.id;
                string dateDisplay = ControllerBase.ConvertDateTimeToSend(_selectedDateTime);

                if (ApplicationManager.instance.offlineMode)
                {
                    string id = OfflineIds.GetId(OfflineIds.OfflineIdType.ANNOTATION);
                    CreateMessage(coords, id, imageId, user.id, _selectedShare, dateDisplay, _selectedTiming, tourId);
                    ////////////////// KTBS ////////////////////////
                        Dictionary<string, object> trace = new Dictionary<string, object>()
                            {
                                {"@type", "m:addPost"},
                                {"m:postId", id},
                                {"m:userId", user.id},
                                {"m:tourId", tourId},
                                {"m:coordinates", coords},
                                {"m:emoticon", _selectedEmoticon},
                                {"m:icons", _selectedIcons},
                                {"m:dateDisplay", dateDisplay},
                                {"m:tags", _selectedTags},
                                {"m:imageId", imageId},
                                {"m:placeType", _selectedPlaceType},
                                {"m:timing", _selectedTiming},
                                {"m:message", _selectedMessage},
                                {"m:shared", _selectedShare}
                            };
                    TraceManager.DispatchEventTraceToManager(trace);
                    ////////////////// END ////////////////////////
                }
                else
                {
                    WebServiceAnnotationsCreate queryA = new WebServiceAnnotationsCreate(token, _selectedMessage, imageId, _selectedPlaceType,
                    _selectedIcons, _selectedEmoticon, _selectedTags, coords, _selectedShare, dateDisplay, _selectedTiming, user.id, tourId);
                    yield return queryA.Run();
                    if (!queryA.hasFailed)
                    {
                        Dictionary<string, object> dic = queryA.GetResultAsDic();
                        string id = DicTools.GetValueString(dic, "id");
                        if (!string.IsNullOrEmpty(id))
                        {
                            CreateMessage(coords, id, imageId, user.id, _selectedShare, dateDisplay, _selectedTiming, tourId);
                        }
                    ////////////////// KTBS ////////////////////////
                        Dictionary<string, object> trace = new Dictionary<string, object>()
                            {
                                {"@type", "m:addPost"},
                                {"m:postId", id},
                                {"m:userId", user.id},
                                {"m:tourId", tourId},
                                {"m:coordinates", coords},
                                {"m:emoticon", _selectedEmoticon},
                                {"m:icons", _selectedIcons},
                                {"m:dateDisplay", dateDisplay},
                                {"m:tags", _selectedTags},
                                {"m:imageId", imageId},
                                {"m:placeType", _selectedPlaceType},
                                {"m:timing", _selectedTiming},
                                {"m:message", _selectedMessage},
                                {"m:shared", _selectedShare}
                            };
                        TraceManager.DispatchEventTraceToManager(trace);
                        ////////////////// END ////////////////////////
                    }
                }
                
            }

            MainUI.instance.HideLoadingPopup();

            ClosePopup();
        }

        private IEnumerator UpdateNotifEnum()
        {
            MainUI.instance.ShowLoadingPopup();

            string token = ApplicationManager.instance.dataManager.token;

            string imageId = _hasChangedTex ? "" : _annotationInEdition.imageId;
            if (_currentTex != null && _hasChangedTex)
            {
                string name = _isTexCapture ? "Capture.png" : "Gallery.png";
                if (ApplicationManager.instance.offlineMode)
                {
                    imageId = OfflineIds.GetId(OfflineIds.OfflineIdType.IMAGE);
                    SpriteLibrary.SetSprite(imageId, _currentTex, System.DateTime.Now.ToString(), format: "png");
                    SpriteLibrary.ApplySaving();
                }
                else
				{
                    WebServiceMediaObjectsUpload query = new WebServiceMediaObjectsUpload(token, name, _currentTex.EncodeToPNG());
                    yield return query.Run();
                    if (!query.hasFailed)
                    {
                        Dictionary<string, object> dic = query.GetResultAsDic();
                        imageId = DicTools.GetValueString(dic, "id");
                    }
                }
            }

            ControllerUser user = ApplicationManager.instance.user;
            if (user != null)
            {
                string dateDisplay = ControllerBase.ConvertDateTimeToSend(_selectedDateTime);
                if (ApplicationManager.instance.offlineMode)
                {
                    UpdateMessage(imageId);
                }
                else
				{
                    WebServiceAnnotationsUpdateContent queryA = new WebServiceAnnotationsUpdateContent(token, _annotationInEdition.id,
                    _selectedMessage, imageId, _selectedPlaceType, _selectedIcons, _selectedEmoticon,
                    _selectedTags, _selectedShare, dateDisplay, _selectedTiming);
                    yield return queryA.Run();
                    if (!queryA.hasFailed)
                    {
                        UpdateMessage(imageId);
                    }
                }
                ////////////////// KTBS ////////////////////////
                Debug.LogWarning("EDIT ANNOTATION");
                Dictionary<string, object> details = new()
                {
                    { "@type", "m:editPost" },
                    { "m:postId", _annotationInEdition.id },
                    { "m:userId", user.id },
                    { "m:tourId", _annotationInEdition.tourId },
                    { "m:coordinates", _annotationInEdition.coords },
                    { "m:emoticon", _selectedEmoticon },
                    { "m:icons", _selectedIcons },
                    { "m:dateDisplay", dateDisplay },
                    { "m:tags", _selectedTags },
                    { "m:imageId", imageId },
                    { "m:placeType", _selectedPlaceType },
                    { "m:timing", _selectedTiming },
                    { "m:message", _selectedMessage },
                    { "m:shared", _selectedShare }
                };
                TraceManager.DispatchEventTraceToManager(details);
                ////////////////// END ////////////////////////    
            }

            MainUI.instance.HideLoadingPopup();

            ClosePopup();
        }

        private void CreateMessage(string coords, string id, string imageId, string userId, string share, string dateDisplay, string timing, string tourId = null)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["id"] = id;
            if (!string.IsNullOrEmpty(imageId))
                dic["image"] = $"/api/media_objects/{imageId}";
            else
                dic["image"] = null;
            dic["tex"] = _pictureRawImage.texture;
            dic["placeType"] = _selectedPlaceType;
            dic["placeIcon"] = _selectedIcons;
            dic["emoticon"] = _selectedEmoticon;
            dic["tags"] = _selectedTags;
            dic["comment"] = _selectedMessage;
            dic["coords"] = coords;
            dic["user"] = userId;
            dic["share"] = share;
            dic["date"] = System.DateTime.Now;
            dic["dateDisplay"] = dateDisplay;
            dic["timing"] = timing;
            if (!string.IsNullOrEmpty(tourId))
                dic["tour"] = tourId;
            ApplicationManager.instance.AddMessage(MsgBase.MsgType.Mixte, dic);
        }

        private void UpdateMessage(string imageId)
        {
            _annotationInEdition.imageId = imageId;
            _annotationInEdition.tex = _pictureRawImage.texture as Texture2D;
            _annotationInEdition.placeType = _selectedPlaceType;
            _annotationInEdition.icons = _selectedIcons;
            _annotationInEdition.emoticon = _selectedEmoticon;
            _annotationInEdition.tags = _selectedTags;
            _annotationInEdition.comment = _selectedMessage;
            _annotationInEdition.share = _selectedShare;
            _annotationInEdition.dateDisplay = _selectedDate;
            _annotationInEdition.timing = _selectedTiming;
            ApplicationManager.instance.UpdateMsgMixte(_annotationInEdition);
        }

        private void OnPhotoClicked()
        {
            TakePhoto((Texture2D tex) =>
            {
                _currentTex = tex;
                _isTexCapture = true;
                _hasChangedTex = true;
                ShowTexture(tex);
                CheckValidateButton();
            });
        }

        private void OnGalleryClicked()
        {
            GetPhotoFromGallery((Texture2D tex) =>
            {
                _currentTex = tex;
                _isTexCapture = false;
                _hasChangedTex = true;
                ShowTexture(tex);
                CheckValidateButton();
            });
        }

        private void ShowTexture(Texture2D tex)
        {
            _pictureRawImage.texture = tex;
            _pictureRawImage.GetComponent<AspectRatioFitter>().aspectRatio = (float)tex.width / (float)tex.height;
            _pictureRawImage.gameObject.SetActive(true);
        }

        private void OnTrashImageClicked()
        {
            _currentTex = null;
            _pictureRawImage.gameObject.SetActive(false);
            _pictureRawImage.texture = null;
            _hasChangedTex = true;
            CheckValidateButton();
        }

        private void OnIconClicked()
        {
            MainUI.instance.ShowPlaceIconsPopup(_selectedIcons, (string icons) =>
            {
                SetSelectedIcons(icons);
                CheckValidateButton();
            });
        }

        private void SetSelectedIcons(string icons)
        {
            _selectedIcons = icons;

            foreach (Transform tr in _iconsParent)
                GameObject.Destroy(tr.gameObject);

            string[] names = icons.Split(',');
            foreach (string name in names)
			{
                bool visible = name != _iconsSprites.GetFirstName();
                if (visible)
                {
                    SpritesList.SpriteData data = _iconsSprites.GetData(name);
                    if (data != null)
                    {
                        MsgIcon msg = GameObject.Instantiate<MsgIcon>(_iconPrefab, _iconsParent);
                        msg.SetIconSprite(data.sprite);
                        msg.SetIconColor(data.color);
                    }
                }
            }
        }

        private void OnTagsClicked()
        {
            MainUI.instance.ShowTagsPopup(_selectedTags, (string tags) =>
            {
                SetTags(tags);
                CheckValidateButton();
            });
        }

        private void SetTags(string tags)
        {
            _selectedTags = tags;
            _tagsText.text = tags;
        }

        private void OnEmoticonsClicked()
        {
            MainUI.instance.ShowEmoticonsPopup(_selectedEmoticon, (string name) =>
            {
                SetEmoticon(name);
                CheckValidateButton();
            });
        }

        private void SetEmoticon(string name)
        {
            string firstName = _emoticonsSprites.GetFirstName();
            SpritesList.SpriteData data = _emoticonsSprites.GetData(name);
            if (data != null)
            {
                if (name == firstName)
                {
                    _emoticonsImage.sprite = _defaultEmoticonSprite;
                    _selectedEmoticon = "";
                }
                else
                {
                    _emoticonsImage.sprite = data.sprite;
                    _selectedEmoticon = name;
                }
                _emoticonText.text = L.Get(_emoticonsSprites.GetTextIdFromName(name));
                _emoticonsImage.color = data.color;
            }
            else
            {
                _emoticonsImage.sprite = _defaultEmoticonSprite;
                _selectedEmoticon = "";
                _emoticonText.text = L.Get(firstName);
            }
        }

        //private void OnPrivateChanged(bool value)
        //{
        //    _isPrivate = value;
        //    CheckValidateButton();
        //}

        private void OnMessageChanged(string message)
        {
            SetMessage(message);
            CheckValidateButton();
        }

        private void OnDateChanged(string date)
		{
            _selectedDate = date;
            CheckValidateButton();
        }

        private void SetMessage(string message, bool edit = false)
        {
            _selectedMessage = message;
            if (edit)
                _inputMessage.text = message;
        }
    }
}
