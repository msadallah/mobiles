namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;

    public class ChoicePopup : PopupBase
    {
        [SerializeField]
        private TextMeshProUGUI _title = null;
        [SerializeField]
        private TextMeshProUGUI _message = null;
        [SerializeField]
        private TextMeshProUGUI _okText = null;
        [SerializeField]
        private TextMeshProUGUI _cancelText = null;
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private Button _cancelButton = null;

        private System.Action<bool> _onChoiceEvent = null;
        private bool _isModal = false;

        public void SetTitle(string titleText)
        {
            _title.text = titleText;
        }

        public void SetMessage(string msgText)
        {
            _message.text = msgText;
        }

        public void SetOkText(string okText)
        {
            _okText.text = okText;
        }

        public void SetCancelText(string cancelText)
        {
            _cancelText.text = cancelText;
        }

        public void SetOnChoiceEvent(System.Action<bool> onChoiceEvent)
        {
            _onChoiceEvent = onChoiceEvent;
        }

        public void SetModal(bool modal)
        {
            _isModal = modal;
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
            _cancelButton.onClick.AddListener(OnCancelClicked);
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            _cancelButton.onClick.RemoveListener(OnCancelClicked);
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            if (!_isModal)
                ClosePopup();
        }

        private void OnOkClicked()
        {
            if (_onChoiceEvent != null)
                _onChoiceEvent.Invoke(true);
            ClosePopup();
        }

        private void OnCancelClicked()
        {
            if (_onChoiceEvent != null)
                _onChoiceEvent.Invoke(false);
            ClosePopup();
        }
    }
}
