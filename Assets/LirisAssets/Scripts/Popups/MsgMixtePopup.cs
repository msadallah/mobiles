namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
	using Liris.App.Assets;
	using Liris.App.Controllers;
    using Liris.App.Favorites;
    using System.Collections;
	using AllMyScripts.LangManager;
    using System.Collections.Generic;
    using Liris.App.Trace;
	using Liris.App.UI;
	using Liris.App.Emojis;
	using AllMyScripts.Common.Tools;
	using Liris.App.Notification;

	public class MsgMixtePopup : PopupBase
    {
        [SerializeField]
        private TextMeshProUGUI _title = null;
        [SerializeField]
        private GameObject _pictureRoot = null;
        [SerializeField]
        private RawImage _picture = null;
        [SerializeField]
        private GameObject _placeIconRoot = null;
        [SerializeField]
        private Image _placeIconImage = null;
        [SerializeField]
        private SpritesList _placeIconSprites = null;
        [SerializeField]
        private MsgIcon _iconsPrefab = null;
        [SerializeField]
        private Transform _iconsRoot = null;
        [SerializeField]
        private GameObject _emoticonRoot = null;
        [SerializeField]
        private Image _emoticonImage = null;
        [SerializeField]
        private SpritesList _emoticonSprites = null;
        [SerializeField]
        private Image _placeTypeIcon = null; 
        [SerializeField]
        private SpritesList _placeTypeSprites = null;
        [SerializeField]
        private GameObject _tagsRoot = null;
        [SerializeField]
        private TextMeshProUGUI _tagsText = null;
        [SerializeField]
        private GameObject _commentRoot = null;
        [SerializeField]
        private TextMeshProUGUI _commentText = null;
        [Header("Buttons")]
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private GameObject _bottomButtons = null;
        [SerializeField]
        private Button _deleteButton = null;
        [SerializeField]
        private Button _moveButton = null;
        [SerializeField]
        private Button _editButton = null;
        [SerializeField]
        private Button _likeButton = null;
        [SerializeField]
        private Button _commentButton = null;
        [SerializeField]
        private Button _photoButton = null;
        [Header("Favorites")]
        [SerializeField]
        private FavoritesButtons _favorites = null;
        [Header("Emojis")]
        [SerializeField]
        private UI_Emojis _emojis = null;
        [Header("Messages")]
        [SerializeField]
        private NotifCount _messageCount = null;

        private ControllerAnnotation _annotation = null;
        private ControllerUser _user = null;
        private List<ControllerMessage> _messages = null;

        public void SetAnnotation(ControllerAnnotation annotation, bool canShowBottomButtons = true, bool isInPreview = false)
        {
            _annotation = annotation;
            _user = annotation.GetUser();
            SetTexture(annotation.tex);
            SetPlaceType(annotation.placeType);
            SetPlaceIcon(annotation.icons);
            SetEmoticon(annotation.emoticon);
            SetTags(annotation.tags);
            SetComment(annotation.comment);
            if (_user != null)
                SetTitle(_user.username + "\n" + annotation.GetFormattedDate());
            else
                SetTitle(annotation.GetFormattedDate());
            bool isMe = _user == ApplicationManager.instance.user;
            if (_bottomButtons != null)
                _bottomButtons.gameObject.SetActive(canShowBottomButtons);
            if (_moveButton != null)
                _moveButton.gameObject.SetActive(isMe && !isInPreview);
            if (_deleteButton != null)
                _deleteButton.gameObject.SetActive(isMe && !isInPreview);
            if (_editButton != null)
                _editButton.gameObject.SetActive(isMe && !isInPreview);
            if (_favorites != null)
                _favorites.SetFavoritesFull(ApplicationManager.instance.favoritesManager.IsAnnotationFavorite(_annotation.id), false);
            
            if (_emojis != null)
			{
                if (canShowBottomButtons)
                    FillEmojis(_annotation.id);
                else
                    _emojis.gameObject.SetActive(false);
            }

            UpdateMessages(null);
        }

        public void SetTexture(Texture2D tex)
        {
            _picture.texture = tex;
            bool show = tex != null;
            _pictureRoot.SetActive(show);
            if (show)
            {
                AspectRatioFitter aspect = _picture.GetComponent<AspectRatioFitter>();
                if (aspect != null)
                {
                    float ratio = (float)tex.width / (float)tex.height;
                    aspect.aspectRatio = ratio;
                }
            }
        }

        public void SetPlaceType(string name)
        {
            SpritesList.SpriteData data = _placeTypeSprites.GetData(name);
            _placeTypeIcon.sprite = data.sprite;
            _placeTypeIcon.color = data.color;
        }

        public void SetEmoticon(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                _emoticonRoot.SetActive(name != _emoticonSprites.GetFirstName());
                _emoticonImage.sprite = _emoticonSprites.GetData(name).sprite;
            }
            else
            {
                _emoticonRoot.SetActive(false);
            }
        }

        public void SetPlaceIcon(string name)
        {
            foreach (Transform tr in _iconsRoot)
                GameObject.Destroy(tr.gameObject);

            if (!string.IsNullOrEmpty(name))
            {
                if (name.Contains(',') || _placeIconRoot == null)
				{
                    if (_placeIconRoot != null)
                        _placeIconRoot.SetActive(false);
                    string[] split = name.Split(',');
                    foreach (string iconName in split)
					{
                        SpritesList.SpriteData iconData = _placeIconSprites.GetData(iconName);
                        if (iconData != null)
						{
                            MsgIcon icon = GameObject.Instantiate<MsgIcon>(_iconsPrefab, _iconsRoot);
                            icon.SetIconSprite(iconData.sprite);
                            icon.SetIconColor(iconData.color);
                        }
					}
                }
                else
				{
                    _placeIconRoot.SetActive(name != _placeIconSprites.GetFirstName());
                    SpritesList.SpriteData data = _placeIconSprites.GetData(name);
                    if (data != null)
                    {
                        _placeIconImage.sprite = data.sprite;
                        _placeIconImage.color = data.color;
                    }
                    else
                    {
                        _placeIconRoot.SetActive(false);
                    }
                }
            }
            else
            {
                if (_placeIconRoot != null)
                    _placeIconRoot.SetActive(false);
            }
        }

        public void SetComment(string comment)
        {
            bool valid = !string.IsNullOrEmpty(comment);
            _commentRoot.SetActive(valid);
            if (valid)
                _commentText.text = comment;
        }

        public void SetTitle(string title)
        {
            _title.text = title;
        }

        public void SetTags(string tags)
        {
            bool valid = !string.IsNullOrEmpty(tags);
            _tagsRoot.SetActive(valid);
            _tagsText.text = tags;
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
            _deleteButton?.onClick.AddListener(OnDeleteClicked);
            _moveButton?.onClick.AddListener(OnMoveClicked);
            _editButton?.onClick.AddListener(OnEditClicked);
            _likeButton?.onClick.AddListener(OnLikeCliked);
            _commentButton?.onClick.AddListener(OnCommentClicked);
            _photoButton.onClick.AddListener(OnPhotoClicked);
            if (_favorites != null)
                _favorites.onFavoriteStateChanged += OnFavoriteStateChanged;
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            _deleteButton?.onClick.RemoveListener(OnDeleteClicked);
            _moveButton?.onClick.RemoveListener(OnMoveClicked);
            _editButton?.onClick.RemoveListener(OnEditClicked);
            _likeButton?.onClick.RemoveListener(OnLikeCliked);
            _commentButton?.onClick.RemoveListener(OnCommentClicked);
            _photoButton.onClick.RemoveListener(OnPhotoClicked);
            if (_favorites != null)
                _favorites.onFavoriteStateChanged -= OnFavoriteStateChanged;
            _annotation = null;
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            ClosePopup();
        }

        private void OnDeleteClicked()
        {
            MainUI.instance.ShowChoicePopup(L.Get(TextManager.ANNOTATIONS_TITLE), L.Get(TextManager.ANNOTATIONS_DELETE),
                L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO), (bool result) =>
            {
                if (result)
                {
                    StartCoroutine(DeleteMessageEnum());
                }
            });
        }

        private IEnumerator DeleteMessageEnum()
        {
            if (ApplicationManager.instance.offlineMode)
			{
                ApplicationManager.instance.RemoveAnnotation(_annotation);

                ////////////////// KTBS ////////////////////////             
                Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:deletePost"},
                    {"m:postId", _annotation.id},
                    {"m:userId", _annotation.userId}
                };
                TraceManager.DispatchEventTraceToManager(trace);
                ////////////////// END ////////////////////////
            }
            else
			{
                MainUI.instance.ShowLoadingPopup();

                yield return ApplicationManager.instance.dataManager.DeleteAnnotation(_annotation);

                ApplicationManager.instance.RemoveAnnotation(_annotation);

                MainUI.instance.HideLoadingPopup();
            }

            ClosePopup();
        }

        private void OnMoveClicked()
        {
            ApplicationManager.instance.MoveAnnotation(_annotation);
            ClosePopup();
        }

        private void OnEditClicked()
        {
            ControllerTour tour = null;
            if (!string.IsNullOrEmpty(_annotation.tourId))
			{
                tour = ApplicationManager.instance.dataManager.GetTourFromId(_annotation.tourId);
			}
            MainUI.instance.EditNotificationPopup(_annotation, tour);
            ClosePopup();
        }

        private void OnLikeCliked()
		{
            EmojisManager emojisMgr = ApplicationManager.instance.emojisManager;
            NotificationManager notifMgr = ApplicationManager.instance.notificationManager;
            string emoji = emojisMgr.GetAnnotationEmoji(_annotation.id);
            int oldIcon = _emojis.GetIndexFromName(emoji);

            MainUI.instance.ShowEmojisPopup(oldIcon, (int numIcon) =>
            {
                int annotationId = ConvertTools.ToInt32(_annotation.id);
                if (oldIcon >= 0)
				{
                    _emojis.RemoveIcon(oldIcon);
                    emojisMgr.RemoveAnnotationEmoji(annotationId);
                    
                    // KTBS2
                    Dictionary<string, object> traceRemove = new Dictionary<string, object>()
                    {
                        {"@type", "m:removeEmoji"},
                        {"m:userId", ApplicationManager.instance.user.id},
                        {"m:target", "post"},
                        {"m:postId", annotationId},
                        {"m:emoji", _emojis.GetNameFromIndex(oldIcon)}
                    };
                    TraceManager.DispatchEventTraceToManager(traceRemove);
 
                }
                if (oldIcon != numIcon)
                {
                    _emojis.AddIcon(numIcon);
                    string name = _emojis.GetNameFromIndex(numIcon);
                    emojisMgr.AddAnnotationEmoji(annotationId, name);
                    notifMgr.AddNotification(NotificationManager.NotificationType.EMOJI_ON_ANNOTATION, annotationId.ToString(), name);

                    // KTBS2
                    Dictionary<string, object> traceAdd = new Dictionary<string, object>()
                    {
                        {"@type", "m:addEmoji"},
                        {"m:userId", ApplicationManager.instance.user.id},
                        {"m:target", "post"},
                        {"m:postId", annotationId},
                        {"m:emoji", _emojis.GetNameFromIndex(numIcon)}
                    };
                    TraceManager.DispatchEventTraceToManager(traceAdd);

                }
            });
        }

        private void OnCommentClicked()
		{
            if (_messages == null || _messages.Count == 0)
                MainUI.instance.ShowEditMessagesPopup(_annotation.id, null, null, UpdateMessages);
            else
                MainUI.instance.ShowMessagesPopup(_annotation.id, null, _messages, UpdateMessages);

                // KTBS2
                    Dictionary<string, object> trace = new Dictionary<string, object>()
                    {
                        {"@type", "m:openMessage"},
                        {"m:userId", ApplicationManager.instance.user.id},
                        {"m:postId", _annotation.id}
                    };
                    TraceManager.DispatchEventTraceToManager(trace);
                    
		}

        private void UpdateMessages(ControllerMessage message)
		{
            if (_messageCount != null)
			{
                //Debug.Log("UpdateMessages " + message + " on annotation " + _annotation.id);
                _messages = ApplicationManager.instance.dataManager.GetMessagesFromAnnotation(_annotation.id);
                int messageCount = _messages != null ? _messages.Count : 0;
                _messageCount.gameObject.SetActive(messageCount > 0);
                _messageCount.SetCount(messageCount);
            }
        }

        private void OnPhotoClicked()
		{
            MainUI.instance.ShowPhotoPopup(_annotation.tex);
		}

        private void OnFavoriteStateChanged(FavoritesButtons.FavoriteState state)
		{
            if (_annotation == null)
                return;
            switch (state)
			{
                case FavoritesButtons.FavoriteState.EMPTY:
                    ApplicationManager.instance.favoritesManager.RemoveAnnotationFavorite(_annotation.id);
                    // KTBS2
                    Dictionary<string, object> trace = new Dictionary<string, object>()
                    {
                        {"@type", "m:addFavorite"}, 
                        {"m:target", "post"}, 
                        {"m:userId", ApplicationManager.instance.user.id},
                        {"m:postId", _annotation.id}
                    };
                    TraceManager.DispatchEventTraceToManager(trace);
                    
                    break;
                case FavoritesButtons.FavoriteState.FULL:
                    ApplicationManager.instance.favoritesManager.AddAnnotationFavorite(_annotation.id);
                    // KTBS2
                    Dictionary<string, object> traceRemove = new Dictionary<string, object>()
                    {
                        {"@type", "m:removeFavorite"},
                        {"m:target", "post"}, 
                        {"m:userId", ApplicationManager.instance.user.id},
                        {"m:postId", _annotation.id}
                    };
                    TraceManager.DispatchEventTraceToManager(traceRemove);                    

                    break;
			}
		}

        private void FillEmojis(string id)
		{
            Dictionary<string, int> dicEmojis = ApplicationManager.instance.dataManager.GetEmojisForAnnotation(id);
            if (dicEmojis != null)
			{
                foreach (var keyval in dicEmojis)
				{
                    int numIcon = _emojis.GetIndexFromName(keyval.Key);
                    int count = keyval.Value;
                    _emojis.AddIcon(numIcon, count);
                }
            }
        }
    }
}
