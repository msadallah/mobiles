namespace Liris.App.Popups
{   
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
	using Liris.App.Controllers;
	using AllMyScripts.LangManager;

	public class UserInfoPopup : PopupBase
    {
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _logoutButton = null;
        [SerializeField]
        private Button _addUserButton = null;
        [SerializeField]
        private Button _updateUserButton = null;
        [SerializeField]
        private Button _manageGroupsButton = null;
        [SerializeField]
        private Button _optionsButton = null;
        [SerializeField]
        private TextMeshProUGUI _title = null;
        [SerializeField]
        private TextMeshProUGUI _infos = null;
        [SerializeField]
        private GameObject _addUserRoot = null;

        public void SetTitle(string title)
        {
            _title.text = title;
        }

        public void SetInfos(string infos)
        {
            _infos.text = infos;
        }

        public void ShowAddUser(bool show)
        {
            _addUserRoot.SetActive(show);
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _logoutButton.onClick.AddListener(OnLogoutClicked);
            _addUserButton.onClick.AddListener(OnAddUserClicked);
            _updateUserButton.onClick.AddListener(OnUpdateUserClicked);
            _manageGroupsButton.onClick.AddListener(OnManageGroupsClicked);
            _optionsButton.onClick.AddListener(OnOptionsClicked);
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _logoutButton.onClick.RemoveListener(OnLogoutClicked);
            _addUserButton.onClick.RemoveListener(OnAddUserClicked);
            _updateUserButton.onClick.RemoveListener(OnUpdateUserClicked);
            _manageGroupsButton.onClick.RemoveListener(OnManageGroupsClicked);
            _optionsButton.onClick.RemoveListener(OnOptionsClicked);
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnLogoutClicked()
        {
            MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.LOGOUT_TITLE), L.Get(TextManager.LOGOUT_SUCCESS), FeedbackPopup.IconType.Success);
            ApplicationManager.instance.SetUser(null);
            ControllerUser.DeleteLoginData();
            ClosePopup();
        }

        private void OnAddUserClicked()
		{
            MainUI.instance.ShowNewUserPopup();
        }

        private void OnUpdateUserClicked()
		{
            MainUI.instance.ShowCreateUserPopup();
        }

        private void OnManageGroupsClicked()
		{
            MainUI.instance.ShowGroupsPopup();
        }

        private void OnOptionsClicked()
		{
			MainUI.instance.ShowOptionsPopup();
		}
    }
}