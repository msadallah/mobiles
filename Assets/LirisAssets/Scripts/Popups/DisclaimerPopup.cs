namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;
	using System.Collections;
	using Liris.App.WebServices;
	using Liris.App.Controllers;
	using AllMyScripts.LangManager;

	public class DisclaimerPopup : PopupBase
    {
        [SerializeField]
        private Button _validateButton = null;
        [SerializeField]
        private Scrollbar _scrollBar = null;
        [SerializeField]
        private ScrollRect _scrollRect = null;

        protected override void Awake()
        {
            base.Awake();
            _validateButton.onClick.AddListener(OnValidateClicked);
            _scrollBar.onValueChanged.AddListener(OnScrollBarChanged);
            _scrollRect.onValueChanged.AddListener(OnScroolRectChanged);
        }

        private void Start()
        {
            _validateButton.interactable = false;
        }

        protected override void OnDestroy()
        {
            _validateButton.onClick.RemoveListener(OnValidateClicked);
            _scrollBar.onValueChanged.RemoveListener(OnScrollBarChanged);
            _scrollRect.onValueChanged.RemoveListener(OnScroolRectChanged);
            base.OnDestroy();
        }

        private void OnValidateClicked()
        {
            StartCoroutine(ValidateDisclaimerEnum());
        }

        private IEnumerator ValidateDisclaimerEnum()
        {
            ControllerUser user = ApplicationManager.instance.user;
            if (user != null)
            {
                string token = ApplicationManager.instance.dataManager.token;
                WebServiceUsersValidDisclaimer query = new WebServiceUsersValidDisclaimer(token, user.id);

                MainUI.instance.ShowLoadingPopup();

                yield return query.Run();

                MainUI.instance.HideLoadingPopup();

                if (query.hasFailed)
                {
                    MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.DISCLAIMER_TITLE), L.Get(TextManager.DISCLAIMER_FAILED), FeedbackPopup.IconType.Alert);
                    user.disclaimer = false;
                }
                else
                {
                    MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.DISCLAIMER_TITLE), L.Get(TextManager.DISCLAIMER_SUCCESS), FeedbackPopup.IconType.Success, MainUI.instance.ShowCreateUserPopup);
                    user.disclaimer = true;
                    ClosePopup();
                }
            }
        }

        private void OnScrollBarChanged(float value)
        {
            _validateButton.interactable = value <= 0f;
        }

        private void OnScroolRectChanged(Vector2 v)
        {
            _validateButton.interactable = v.y <= 0f;
        }
    }
}
