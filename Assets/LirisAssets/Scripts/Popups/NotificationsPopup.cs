namespace Liris.App.Popups
{
	using Liris.App.Controllers;
	using Liris.App.Favorites;
	using Liris.App.Notifications;
    using Liris.App.Trace;
    using System.Collections.Generic;
	using UnityEngine;
    using UnityEngine.UI;

    public class NotificationsPopup : PopupBase
    {
        private const string PP_HIDE_READ = "HideReadNotifications_";

        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private Toggle _hideReadToggle = null;
        [SerializeField]
        private Transform _parent = null;
        [SerializeField]
        private NotificationItem _item = null;
        [SerializeField]
        private GameObject _noData = null;

        private List<ControllerNotification> _notifications = null;
        private bool _hideRead = true;

        public void InitItems(List<ControllerNotification> notifications, bool hideRead = true)
		{
            _notifications = notifications;
            _hideRead = PlayerPrefs.GetInt(PP_HIDE_READ, hideRead ? 1 : 0) != 0;
            _hideReadToggle.SetIsOnWithoutNotify(_hideRead);
            UpdateDisplay();
        }

        private void UpdateDisplay()
		{
            foreach (Transform tr in _parent)
			{
                GameObject.Destroy(tr.gameObject);
			}

            if (_notifications != null && _notifications.Count > 0)
            {
                foreach (ControllerNotification notif in _notifications)
                {
                    if (!_hideRead || notif.isNotRead)
                    {
                        NotificationItem item = GameObject.Instantiate<NotificationItem>(_item, _parent);
                        item.InitController(notif, OnClickItem);
                    }
                }
                _noData.SetActive(false);
            }
            else
            {
                _noData.SetActive(true);
            }
        }

        private void OnClickItem(NotificationItem item)
		{
            // KTBS2
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:selectNotification"},
                {"m:notificationId", item.notification.id},
                {"m:userId", ApplicationManager.instance.user.id},
                {"m:title", item.notification.title},
                {"m:category", item.notification.category},
                {"m:content", item.notification.content},
                {"m:parameters", item.notification.parameters},
                {"m:readDate", item.notification.readDate},
                {"m:expiredDate", item.notification.expiredDate}
            };
            TraceManager.DispatchEventTraceToManager(trace);

            ClosePopup();
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
            _hideReadToggle.onValueChanged.AddListener(OnHideReadToggleChanged);
        }

		protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            _hideReadToggle.onValueChanged.RemoveListener(OnHideReadToggleChanged);
            base.OnDestroy();
        }

        private void OnHideReadToggleChanged(bool toggle)
		{
            _hideRead = toggle;
            UpdateDisplay();
            PlayerPrefs.SetInt(PP_HIDE_READ, toggle ? 1 : 0);
            PlayerPrefs.Save();
            // KTBS2
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:hideReadNotifications"},
                {"m:userId", ApplicationManager.instance.user.id},
                {"m:status", _hideRead}

            };
            TraceManager.DispatchEventTraceToManager(trace);
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            ClosePopup();
        }
    }
}
