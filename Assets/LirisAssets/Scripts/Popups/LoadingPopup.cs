namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;

    public class LoadingPopup : PopupBase
    {
        [SerializeField]
        private Button _backButton = null;

        public void Hide()
		{
            ClosePopup();
		}

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
        }
    }
}
