namespace Liris.App.Popups
{
	using Liris.App.Assets;
	using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
	using AllMyScripts.LangManager;

	public class EmoticonsPopup : PopupBase
    {
        [SerializeField]
        private Button _iconWithSelectionButtonPrefab = null;
        [SerializeField]
        private Transform _emoticonsParent = null;
        [SerializeField]
        private SpritesList _emoticonSprites = null;
        [SerializeField]
        private TextMeshProUGUI _nameText = null;
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;

        private System.Action<string> _onResult = null;
        private Button[] _emoticonsButton = null;
        private GameObject[] _emoticonsSelection = null;
        private string _emoticonSelected = null;
     
        public void SetOnResultAction(System.Action<string> onResult)
        {
            _onResult = onResult;
        }

        public void SetSelectedIcon(string name)
        {
            if (string.IsNullOrEmpty(name))
                name = _emoticonSprites.GetFirstName();
            OnEmoticonClicked(name);
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
            int count = _emoticonSprites.data.Length;
            _emoticonsButton = new Button[count];
            _emoticonsSelection = new GameObject[count];
            for (int i = 0; i < count; ++i)
            {
                SpritesList.SpriteData data = _emoticonSprites.data[i];
                Button btn = GameObject.Instantiate<Button>(_iconWithSelectionButtonPrefab, _emoticonsParent);
                Image image = btn.transform.Find("Icon").GetComponent<Image>();
                image.sprite = data.sprite;
                image.color = data.color;
                btn.onClick.AddListener(() => OnEmoticonClicked(data.name));
                _emoticonsButton[i] = btn;
                _emoticonsSelection[i] = btn.transform.Find("Selection").gameObject;
            }
            OnEmoticonClicked(_emoticonSprites.GetFirstName());
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            for (int i = 0; i < _emoticonsButton.Length; ++i)
            {
                _emoticonsButton[i].onClick.RemoveAllListeners();
                _emoticonsSelection[i] = null;
            }
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            if (_onResult != null)
                _onResult(_emoticonSelected);
            ClosePopup();
        }

        private void OnEmoticonClicked(string name)
        {
            _emoticonSelected = name;
            for (int i = 0; i < _emoticonsSelection.Length; ++i)
            {
                SpritesList.SpriteData data = _emoticonSprites.data[i];
                if (data.name == name)
                {
                    _emoticonsSelection[i].SetActive(true);
                    _nameText.text = L.Get(_emoticonSprites.GetTextIdFromName(name));
                }
                else
                {
                    _emoticonsSelection[i].SetActive(false);
                }
            }
        }
    }
}
