namespace Liris.App.Popups
{
	using CustomDatePicker;
	using UnityEngine;
	using UnityEngine.UI;

	public class CalendarPopup : PopupBase
    {
        [SerializeField]
        private CalendarController _calendarCtrl = null;
        [SerializeField]
        private Button _closeBtn = null;

        DatePickerTitle _datePicker = null;

        public void SetDatePicker(DatePickerTitle datePicker, System.DateTime dateTime, bool useTime = false)
		{
            _datePicker = datePicker;
            _calendarCtrl.date = _datePicker.dateField;
            _calendarCtrl.OnDateSelect += datePicker.OnDateSelected;
            _calendarCtrl.SetDateTime(dateTime, useTime);
        }

		protected override void Awake()
		{
            base.Awake();
			_closeBtn.onClick.AddListener(OnClickClose);
        }

        protected override void OnDestroy()
		{
			_closeBtn.onClick.RemoveListener(OnClickClose);
            if (_datePicker != null && _calendarCtrl != null)
				_calendarCtrl.OnDateSelect -= _datePicker.OnDateSelected;
            base.OnDestroy();
        }

		private void OnClickClose()
		{
            ClosePopup();
        }
    }
}
