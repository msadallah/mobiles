namespace Liris.App.Popups
{   
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
    using System.Text.RegularExpressions;
    using Liris.App.Controllers;
	using Liris.App.Data;
	using Liris.App.Network;
	using AllMyScripts.LangManager;

	public class LoginPopup : PopupBase
    {
        [SerializeField]
        private TMP_InputField _emailInput = null;
        [SerializeField]
        private TMP_InputField _passwordInput = null;
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _validateButton = null;
        [SerializeField]
        private Button _createButton = null;

        private string _email = null;
        private string _password = null;

        protected override void Awake()
        {
            base.Awake();
            _emailInput.onValueChanged.AddListener(OnEmailChanged);
            _passwordInput.onValueChanged.AddListener(OnPasswordChanged);
            _backButton.onClick.AddListener(OnBackClicked);
            _validateButton.onClick.AddListener(OnValidateClicked);
            _createButton.onClick.AddListener(OnCreateClicked);
        }

        private void Start()
        {
            ControllerUser.GetLoginData(out string mail, out string pass);
            if (!string.IsNullOrEmpty(mail) && ! string.IsNullOrEmpty(pass))
            {
                _emailInput.text = mail;
                _passwordInput.text = pass;
            }
            UpdateValidation();
        }

        protected override void OnDestroy()
        {
            _emailInput.onValueChanged.RemoveListener(OnEmailChanged);
            _passwordInput.onValueChanged.RemoveListener(OnPasswordChanged);
            _backButton.onClick.RemoveListener(OnBackClicked);
            _validateButton.onClick.RemoveListener(OnValidateClicked);
            _createButton.onClick.RemoveListener(OnCreateClicked);
            base.OnDestroy();
        }

        private void OnEmailChanged(string text)
        {
            _email = text;
            UpdateValidation();
        }

        private void OnPasswordChanged(string text)
        {
            _password = text;
            UpdateValidation();
        }

        private void UpdateValidation()
        {
            bool isValid = true;
            if (!UpdateChecked(_emailInput, IsValidEmail(_email)))
                isValid = false;
            else if (!UpdateChecked(_passwordInput, IsValidPassword(_password)))
                isValid = false;
            _validateButton.interactable = isValid;
        }

        private bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;
            else
                return Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }

        private bool IsValidPassword(string password)
        {
            if (string.IsNullOrEmpty(password))
                return false;
            else
                return Regex.IsMatch(password, @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
        }

        private bool UpdateChecked(TMP_InputField input, bool check)
        {
            Transform tr = input.transform.Find("Checked");
            if (tr != null)
                tr.gameObject.SetActive(check);
            return check;
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnValidateClicked()
        {
            VerifyLogin();
        }

        private void OnCreateClicked()
        {
            MainUI.instance.ShowCreateUserPopup();
        }

        private void VerifyLogin()
        {
            MainUI.instance.ShowLoadingPopup();
            DataManager dataMgr = ApplicationManager.instance.dataManager;
            dataMgr.Login(_email, _password, OnLoginResult);
        }

        private void OnLoginResult(bool ok)
        {
            if (ok)
            {
                DataManager dataMgr = ApplicationManager.instance.dataManager;
                dataMgr.GetAllUsers(OnGetAllUsers);
            }
            else
            {
                MainUI.instance.HideLoadingPopup();
                if (CheckOffline.lastResult)
                    MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.LOGIN_TITLE), L.Get(TextManager.LOGIN_BAD_CREDENTIALS), FeedbackPopup.IconType.Alert);
                else
                {
                    ApplicationManager.instance.dataManager.SetAllUsers(ApplicationManager.instance.cacheData.GetAllUsers());
                    string userId = ApplicationManager.instance.cacheData.GetCurrentUserId();
                    ControllerUser user = ApplicationManager.instance.dataManager.GetUserFromId(userId);
                    if (user != null)
					{
                        MainUI.instance.ShowChoicePopup(L.Get(TextManager.LOGIN_TITLE), L.Get(TextManager.OFFLINE_CHOICE), 
                            L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO),
                        (bool result) =>
                        {
                            if (result)
                            {
                                ApplicationManager.instance.SetUser(user, true);
                                ClosePopup();
                            }
                        });
					}
                    else
					{
                        MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.LOGIN_TITLE), L.Get(TextManager.LOGOUT_NO_INTERNET), FeedbackPopup.IconType.Alert);
                    }
                }   
            }
        }

        private void OnGetAllUsers()
        {
            MainUI.instance.HideLoadingPopup();
            DataManager dataMgr = ApplicationManager.instance.dataManager;
            if (dataMgr.users != null)
            {
                bool foundMail = false;
                foreach (ControllerUser user in dataMgr.users)
                {
                    if (user.mail.ToLower() == _email.ToLower())
                    {
                        foundMail = true;
                        ApplicationManager.instance.SetUser(user);
                        ControllerUser.SaveLoginData(_email.ToLower(), _password);
                        MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.LOGIN_TITLE), L.Get(TextManager.LOGIN_CONNECTED),
                            FeedbackPopup.IconType.Success, MainUI.instance.CheckDisclaimerAndVersion);
                        ClosePopup();
                        break;
                    }
                }
                if (!foundMail)
                {
                    MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.LOGIN_TITLE), L.Get(TextManager.LOGIN_BAD_CREDENTIALS), FeedbackPopup.IconType.Alert);
                }
            }
            else
            {
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.USERS_TITLE), L.Get(TextManager.USERS_LIST_ERROR), FeedbackPopup.IconType.Alert);
            }
        }
    }
}