namespace Liris.App.Popups
{
	using Liris.App.Controllers;
	using Liris.App.Favorites;
    using Liris.App.Trace;
    using System.Collections.Generic;
	using UnityEngine;
    using UnityEngine.UI;

    public class FavoritesPopup : PopupBase
    {
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private Transform _parent = null;
        [SerializeField]
        private FavoritesItem _itemTour = null;
        [SerializeField]
        private FavoritesItem _itemAnnotation = null;
        [SerializeField]
        private GameObject _noData = null;

        public void InitItems(List<ControllerTour> tours, List<ControllerAnnotation> annotations)
		{
            bool showNoData = true;
            foreach (ControllerTour tour in tours)
			{
                if (ApplicationManager.instance.CanShowPost(tour.GetUser(), tour.share))
				{
                    FavoritesItem item = GameObject.Instantiate<FavoritesItem>(_itemTour, _parent);
                    item.InitController(tour, OnClickItem);
                    showNoData = false;
                }
            }
            foreach (ControllerAnnotation annotation in annotations)
            {
                if (ApplicationManager.instance.CanShowPost(annotation.GetUser(), annotation.share))
                {
                    FavoritesItem item = GameObject.Instantiate<FavoritesItem>(_itemAnnotation, _parent);
                    item.InitController(annotation, OnClickItem);
                    showNoData = false;
                }
            }
            _noData.SetActive(showNoData);
        }

        private void OnClickItem(FavoritesItem item)
		{
            ClosePopup();

            if (item.tour != null)
            {
                MainUI.instance.ShowEditTourPopup(item.tour);

                // KTBS2
                Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:selectFavorite"},
                    {"m:userId", ApplicationManager.instance.user.id},
                    {"m:tourId", item.tour.id},
                    {"m:target", "tour"} 
                };
                TraceManager.DispatchEventTraceToManager(trace);
                
            }
            if (item.annotation != null)
            {
                MainUI.instance.ShowMsgMixtePopup(item.annotation);

                // KTBS2
                Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:selectFavorite"},
                    {"m:userId", ApplicationManager.instance.user.id},
                    {"m:postId", item.annotation.id},                    
                    {"m:target", "post"} 
                };
                TraceManager.DispatchEventTraceToManager(trace);
                
            }

        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            ClosePopup();
        }
    }
}
