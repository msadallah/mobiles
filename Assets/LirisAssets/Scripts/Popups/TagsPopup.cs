namespace Liris.App.Popups
{
	using Liris.App.Assets;
	using Liris.App.UI;
	using System.Collections.Generic;
	using UnityEngine;
    using UnityEngine.UI;

    public class TagsPopup : PopupBase
    {
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private TagsPair _tagsPairPrefab = null;
        [SerializeField]
        private Transform _tagsParent = null;
        [SerializeField]
        private TagsList _tagsList = null;

        private System.Action<string> _onResult = null;
        private string _tags = null;
        private List<TagsPair> _tagsPairList = null; 
     
        public void SetOnResultAction(System.Action<string> onResult)
        {
            _onResult = onResult;
        }

        public void SetSelectedTags(string tags)
        {
            if (!string.IsNullOrEmpty(tags))
            {
                string[] tagSplit = tags.Split(" ");
                for (int tag = 0; tag < tagSplit.Length; ++tag)
                {
                    string tagName = tagSplit[tag];
                    if (tagName.StartsWith("#"))
                    {
                        for (int i = 0; i < _tagsPairList.Count; ++i)
                        {
                            _tagsPairList[i].SetTag(tagName);
                        }
                    }
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);

            _tagsPairList = new List<TagsPair>();
            int count = _tagsList.data.Length / 2;
            for (int i = 0; i < count; ++i)
			{
                TagsPair tagsPair = GameObject.Instantiate<TagsPair>(_tagsPairPrefab, _tagsParent);
                TagsList.TagData dataL = _tagsList.data[i * 2 + 0];
                TagsList.TagData dataR = _tagsList.data[i * 2 + 1];
                tagsPair.SetTagLeft(dataL.name, dataL.text);
                tagsPair.SetTagRight(dataR.name, dataR.text);
                _tagsPairList.Add(tagsPair);
            }
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            _tags = "";
            for (int i=0; i < _tagsPairList.Count; ++i)
            {
                string tag = _tagsPairList[i].GetTag();
                if (!string.IsNullOrEmpty(tag))
                {
                    if (i > 0)
                        _tags += " ";
                    _tags += tag;
                }
            }
            if (_onResult != null)
                _onResult(_tags);
            ClosePopup();
        }
    }
}
