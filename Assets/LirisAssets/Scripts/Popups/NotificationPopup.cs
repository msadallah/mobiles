namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
	using Liris.App.Controllers;
	using AllMyScripts.LangManager;
	using System.Collections.Generic;
	using Liris.App.Trace;

	public class NotificationPopup : PopupBase
    {
        [SerializeField]
        private TextMeshProUGUI _title = null;
        [SerializeField]
        private TextMeshProUGUI _content = null;
        [SerializeField]
        private TextMeshProUGUI _showButtonLabel = null;
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private Button _showButton = null;
        [SerializeField]
        private Button _deleteButton = null;
        [Header("Adapt text")]
        [SerializeField]
        private RectTransform _scrollRT = null;
        [SerializeField]
        private RectTransform _contentRT = null;
        [SerializeField]
        private float _minHeight = 128;
        [SerializeField]
        private float _maxHeight = 512;

        private ControllerNotification _notification = null;
        private bool _forceDelete = false;
        private string _deletionReason = null;

        public void SetNotification(ControllerNotification notif)
        {
            _notification = notif;
            if (!string.IsNullOrEmpty(notif.title))
                SetTitle(notif.title + "\n" + ControllerBase.ConvertDateDayHourMinute(notif.date));
            else
                SetTitle(ControllerBase.ConvertDateDayHourMinute(notif.date));
            SetContent(notif.content);
            UpdateShowButton();
        }

        public void SetTitle(string title)
        {
            _title.text = title;
        }

        public void SetContent(string name)
		{
            _content.text = name;
            LayoutRebuilder.ForceRebuildLayoutImmediate(_contentRT);
            float height = Mathf.Clamp(_contentRT.sizeDelta.y, _minHeight, _maxHeight);
            _scrollRT.sizeDelta = new Vector2(_scrollRT.sizeDelta.x, height);
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
            _showButton.onClick.AddListener(OnShowClicked);
            _deleteButton.onClick.AddListener(OnDeleteClicked);
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            _showButton.onClick.RemoveListener(OnShowClicked);
            _deleteButton.onClick.RemoveListener(OnDeleteClicked);
            _notification = null;
            base.OnDestroy();
        }

		private void UpdateShowButton()
		{
            if (_notification.coordsType == ControllerNotification.CoordsType.DIRECT)
            {
                _showButton.gameObject.SetActive(true);
                _showButtonLabel.text = L.Get("id_notification_show_coords");
            }
            else if (_notification.coordsType == ControllerNotification.CoordsType.REF_ANNOTATION)
            {
                ControllerAnnotation annotation = ApplicationManager.instance.dataManager.GetAnnotationFromId(_notification.annotationId);
                if (annotation != null)
                {
                    _showButton.gameObject.SetActive(true);
                    if (_notification.showRef)
                        _showButtonLabel.text = L.Get("id_notification_show_annotation");
                    else
                        _showButtonLabel.text = L.Get("id_notification_show_coords");
                }
                else
				{
                    _showButton.gameObject.SetActive(false);
                }
            }
            else if (_notification.coordsType == ControllerNotification.CoordsType.REF_TOUR)
            {
                ControllerTour tour = ApplicationManager.instance.dataManager.GetTourFromId(_notification.tourId);
                if (tour != null)
                {
                    _showButton.gameObject.SetActive(true);
                    if (_notification.showRef)
                        _showButtonLabel.text = L.Get("id_notification_show_tour");
                    else
                        _showButtonLabel.text = L.Get("id_notification_show_coords");
                }
                else
				{
                    _showButton.gameObject.SetActive(false);
                }
            }
            else if (_notification.coordsType == ControllerNotification.CoordsType.URL)
            {
                if (!string.IsNullOrEmpty(_notification.url))
                {
                    _showButton.gameObject.SetActive(true);
                    _showButtonLabel.text = L.Get("id_notification_show_url");
                }
                else
                {
                    _showButton.gameObject.SetActive(false);
                }
            }
            else
			{
                _showButton.gameObject.SetActive(false);
            }
        }

		private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            ClosePopup();
        }

        private void OnShowClicked()
		{
            string actionType = "none";
            string actionTarget = "none";
            if (_notification.coordsType == ControllerNotification.CoordsType.DIRECT)
            {
                ApplicationManager.instance.SetMapOnPosition(_notification.coords.x, _notification.coords.y, _notification.zoom);
                actionType = "coords";
                actionTarget = $"({_notification.coords.x},{_notification.coords.y})";
            }
            else if (_notification.coordsType == ControllerNotification.CoordsType.REF_ANNOTATION)
            {
                ControllerAnnotation annotation = ApplicationManager.instance.dataManager.GetAnnotationFromId(_notification.annotationId);
                if (annotation != null)
                {
                    Vector2 pos = ApplicationManager.ConvertCoordsToVector2(annotation.coords);
                    ApplicationManager.instance.SetMapOnPosition(pos.x, pos.y, _notification.zoom);
                    if (_notification.showRef)
                    {
                        MainUI.instance.ShowMsgMixtePopup(annotation);
                    }
                    actionType = "annotation";
                    actionTarget = annotation.id;
                }
            }
            else if (_notification.coordsType == ControllerNotification.CoordsType.REF_TOUR)
            {
                ControllerTour tour = ApplicationManager.instance.dataManager.GetTourFromId(_notification.tourId);
                if (tour != null)
                {
                    Vector2 pos = tour.points[0];
                    ApplicationManager.instance.SetMapOnPosition(pos.x, pos.y, _notification.zoom);
                    if (_notification.showRef)
                    {
                        MainUI.instance.ShowEditTourPopup(tour);
                    }
                    actionType = "tour";
                    actionTarget = tour.id;
                }
            }
            else if (_notification.coordsType == ControllerNotification.CoordsType.URL)
            {
                Application.OpenURL(_notification.url);
                actionType = "url";
                actionTarget = _notification.url;
            }

            // KTBS2
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:doNotificationAction"},
                {"m:notificationId", _notification.id},
                {"m:userId", ApplicationManager.instance.user.id},
                {"m:title", _notification.title},
                {"m:category", _notification.category},
                {"m:content", _notification.content},
                {"m:parameters", _notification.parameters},
                {"m:readDate", _notification.readDate},
                {"m:expiredDate", _notification.expiredDate},
                {"m:actionType", actionType},
                {"m:actionTarget", actionTarget}
            };
            TraceManager.DispatchEventTraceToManager(trace);

            ClosePopup();
        }

        private void OnDeleteClicked()
		{
            _deletionReason = null;
            if (_notification.category == "Recommandation")
			{
                MainUI.instance.ShowNotificationDeletePopup(_notification, OnDeleteReason);
			}
            else
			{
                _forceDelete = true;
                ClosePopup();
            }
        }

		private void OnDeleteReason(NotificationDeletePopup.Reason reason)
		{
            switch (reason)
			{
                case NotificationDeletePopup.Reason.IRRELEVANT:
                    _deletionReason = "irrelevant";
                    break;
                case NotificationDeletePopup.Reason.ALREADY:
                    _deletionReason = "alreadySeen";
                    break;
                case NotificationDeletePopup.Reason.TOO_MANY:
                    _deletionReason = "tooMany";
                    break;
                case NotificationDeletePopup.Reason.OTHER:
                    _deletionReason = "other";
                    break;
            }
            _forceDelete = true;
            ClosePopup();
        }

		protected override void ClosePopup()
		{
            bool needToDelete = _forceDelete;
            bool needToPatchRead = false;
            bool autoDelete = false;

            if (!needToDelete && !string.IsNullOrEmpty(_notification.expiredDate))
			{
                System.DateTime expiredDateTime = ControllerBase.ConvertInDateTime(_notification.expiredDate);
                if (System.DateTime.Now > expiredDateTime)
				{
                    needToDelete = true;
                    autoDelete = true;
                }
            }

            if (!needToDelete && _notification.isNotRead)
			{
                needToPatchRead = true;
            }

            if (needToDelete)
			{
                // KTBS2
                Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:deleteNotification"},
                    {"m:notificationId", _notification.id},
                    {"m:userId", ApplicationManager.instance.user.id},
                    {"m:title", _notification.title},
                    {"m:category", _notification.category},
                    {"m:content", _notification.content},
                    {"m:parameters", _notification.parameters},
                    {"m:readDate", _notification.readDate},
                    {"m:expiredDate", _notification.expiredDate}
                };
                if (!string.IsNullOrEmpty(_deletionReason))
                    trace.Add("m:deletionReason", _deletionReason);
                trace.Add("m:autoDelete", autoDelete);

                TraceManager.DispatchEventTraceToManager(trace);

                ApplicationManager.instance.dataManager.DeleteNotification(_notification.id,
                        (bool result) =>
                        {
                            if (result)
                            {
                                MainUI.instance.SetNotificationsNotifCount(ApplicationManager.instance.dataManager.GetNotReadNotificationCount());
                            }
                        }
                    );
            }
            else if (needToPatchRead)
			{
                // KTBS2
                Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:readNotification"},
                    {"m:notificationId", _notification.id},
                    {"m:userId", ApplicationManager.instance.user.id},
                    {"m:title", _notification.title},
                    {"m:category", _notification.category},
                    {"m:content", _notification.content},
                    {"m:parameters", _notification.parameters},
                    {"m:readDate", _notification.readDate},
                    {"m:expiredDate", _notification.expiredDate}
                };
                TraceManager.DispatchEventTraceToManager(trace);

                ApplicationManager.instance.dataManager.PatchNotificationRead(_notification.id,
                    (bool result) =>
                    {
                        if (result)
                        {
                            MainUI.instance.SetNotificationsNotifCount(ApplicationManager.instance.dataManager.GetNotReadNotificationCount());
                        }
                    }
                );
            }
            base.ClosePopup();
		}
	}
}
