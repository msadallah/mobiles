namespace Liris.App.Popups
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.UI;
    using System.Text.RegularExpressions;
    using Liris.App.WebServices;
	using System.Collections.Generic;
	using Liris.App.UI;
	using Liris.App.Controllers;
	using Liris.App.Data;
	using AllMyScripts.LangManager;
    using TMPro;
    using Liris.App.Trace;

    public class CreateUserPopup : PopupBase
    {
        [SerializeField]
        private TextMeshProUGUI _title = null;
        [SerializeField]
        private GameObject[] _pages = null;
        [Header("Page 1")]
        [SerializeField]
        private InputFieldTitle _pseudoInput = null;
        [SerializeField]
        private InputFieldTitle _emailInput = null;
        [SerializeField]
        private InputFieldTitle _passwordInput = null;
        [SerializeField]
        private InputFieldTitle _passwordVerifInput = null;
        [SerializeField]
        private GameObject _newPasswordLink = null;
        [Header("Page 2")]
        [SerializeField]
        private InputFieldTitle _lastNameInput = null;
        [SerializeField]
        private InputFieldTitle _firstNameInput = null;
        [SerializeField]
        private DatePickerTitle _birthDateInput = null;
        [SerializeField]
        private DropdownTitle _genderDropdown = null;
        [SerializeField]
        private DropdownTitle _nationalityDropdown = null;
        [Header("Page 3")]
        [SerializeField]
        private InputFieldTitle _addressInput = null;
        [SerializeField]
        private InputFieldTitle _postalCodeInput = null;
        [SerializeField]
        private InputFieldTitle _cityInput = null;
        [SerializeField]
        private DatePickerTitle _arrivalDateInput = null;
        [Header("Page 4")]
        [SerializeField]
        private DropdownTitle _institutionDropdown = null;
        [SerializeField]
        private InputFieldTitle _otherInstitutionInput = null;
        [SerializeField]
        private DropdownTitle _diplomaDropdown = null;
        [SerializeField]
        private InputFieldTitle _otherDiplomaInput = null;
        [SerializeField]
        private InputFieldTitle _formationInput = null;


        [Header("Buttons")]
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _prevButton = null;
        [SerializeField]
        private Button _nextButton = null;
        [SerializeField]
        private Button _createButton = null;

        [Header("Texts")]
        [SerializeField]
        private TextAsset _statesAsset = null;

        private string _pseudo = null;
        private string _email = null;
        private string _password = null;
        private string _passwordVerif = null;
        private string _lastName = null;
        private string _firstName = null;
        private string _gender = null;
        private string _nationality = null;
        private string _birthDate = null;
        private string _birthDateToSend = null;
        private string _address = null;
        private string _postalcode = null;
        private string _city = null;
        private string _arrivalDate = null;
        private string _arrivalDateToSend = null;
        private string _institution = null;
        private string _diploma = null;
        private string _formation = null;
        private int _currentPage = 0;
        private bool _canCheckUsersInBase = false;
        private List<string> _stateList = null;
        private ControllerUser _user = null;

        protected override void Awake()
        {
            base.Awake();
            _pseudoInput.input.onValueChanged.AddListener(OnPseudoChanged);
            _emailInput.input.onValueChanged.AddListener(OnEmailChanged);
            _passwordInput.input.onValueChanged.AddListener(OnPasswordChanged);
            _passwordVerifInput.input.onValueChanged.AddListener(OnPasswordVerifChanged);
            _lastNameInput.input.onValueChanged.AddListener(OnLastNameChanged);
            _firstNameInput.input.onValueChanged.AddListener(OnFirstNameChanged);
            _birthDateInput.onDateSelected.AddListener(OnBirthDateChanged);
            _genderDropdown.dropdown.onValueChanged.AddListener(OnGenderChanged);
            _addressInput.input.onValueChanged.AddListener(OnAddressChanged);
            _postalCodeInput.input.onValueChanged.AddListener(OnPostalCodeChanged);
            _cityInput.input.onValueChanged.AddListener(OnCityChanged);
            _arrivalDateInput.onDateSelected.AddListener(OnArrivalDateChanged);
            _nationalityDropdown.dropdown.onValueChanged.AddListener(OnNationalityChanged);
            _institutionDropdown.dropdown.onValueChanged.AddListener(OnInstitutionChanged);
            _otherInstitutionInput.input.onValueChanged.AddListener(OnOtherInstitutionChanged);
            _diplomaDropdown.dropdown.onValueChanged.AddListener(OnDiplomaChanged);
            _otherDiplomaInput.input.onValueChanged.AddListener(OnOtherDiplomaChanged);
            _formationInput.input.onValueChanged.AddListener(OnFormationChanged);

            _backButton.onClick.AddListener(OnBackClicked);
            _prevButton.onClick.AddListener(OnPrevClicked);
            _nextButton.onClick.AddListener(OnNextClicked);
            _createButton.onClick.AddListener(OnCreateClicked);

            //_birthDateInput.input.keyboardType = TouchScreenKeyboardType.NumberPad;
            //_arrivalDateInput.input.keyboardType = TouchScreenKeyboardType.NumberPad;

            _otherInstitutionInput.gameObject.SetActive(false);
            _otherDiplomaInput.gameObject.SetActive(false);

            LoadStateList();
            ShowCurrentPage();
        }

        private void Start()
        {
            StartCoroutine(GetAllUserDataEnum());
        }

        protected override void OnDestroy()
        {
            _pseudoInput.input.onValueChanged.RemoveListener(OnPseudoChanged);
            _emailInput.input.onValueChanged.RemoveListener(OnEmailChanged);
            _passwordInput.input.onValueChanged.RemoveListener(OnPasswordChanged);
            _passwordVerifInput.input.onValueChanged.RemoveListener(OnPasswordVerifChanged);
            _lastNameInput.input.onValueChanged.RemoveListener(OnLastNameChanged);
            _firstNameInput.input.onValueChanged.RemoveListener(OnFirstNameChanged);
            _birthDateInput.onDateSelected.RemoveListener(OnBirthDateChanged);
            _genderDropdown.dropdown.onValueChanged.RemoveListener(OnGenderChanged);
            _nationalityDropdown.dropdown.onValueChanged.RemoveListener(OnNationalityChanged);
            _addressInput.input.onValueChanged.RemoveListener(OnAddressChanged);
            _postalCodeInput.input.onValueChanged.RemoveListener(OnPostalCodeChanged);
            _cityInput.input.onValueChanged.RemoveListener(OnCityChanged);
            _arrivalDateInput.onDateSelected.RemoveListener(OnArrivalDateChanged);
            _institutionDropdown.dropdown.onValueChanged.RemoveListener(OnInstitutionChanged);
            _otherInstitutionInput.input.onValueChanged.RemoveListener(OnOtherInstitutionChanged);
            _diplomaDropdown.dropdown.onValueChanged.RemoveListener(OnDiplomaChanged);
            _otherDiplomaInput.input.onValueChanged.RemoveListener(OnOtherDiplomaChanged);
            _formationInput.input.onValueChanged.RemoveListener(OnFormationChanged);

            _backButton.onClick.RemoveListener(OnBackClicked);
            _prevButton.onClick.RemoveListener(OnPrevClicked);
            _nextButton.onClick.RemoveListener(OnNextClicked);
            _createButton.onClick.RemoveListener(OnCreateClicked);

            _user = null;
            _stateList = null;

            base.OnDestroy();
        }

        public void SetUser(ControllerUser user)
		{
            _user = user;
            if (user != null)
			{
                _pseudo = user.username;
                _email = user.mail;
                _lastName = user.lastName;
                _firstName = user.firstName;
                _birthDate = user.birthDate;
                _arrivalDate = user.arrivalDate;
                _gender = user.gender;
                _nationality = user.nationality;
                _address = user.address;
                _postalcode = user.postalcode;
                _city = user.city;
                _institution = user.institution;
                _diploma = user.diploma;
                _formation = user.formation;
                _emailInput.input.text = _email;
                _emailInput.input.interactable = false;
                _pseudoInput.input.text = _pseudo;
                _passwordInput.input.gameObject.SetActive(false);
                _passwordInput.visibility.gameObject.SetActive(false);
                _newPasswordLink.SetActive(true);
                _passwordVerifInput.gameObject.SetActive(false);
                _lastNameInput.input.text = _lastName;
                _firstNameInput.input.text = _firstName;
                _birthDateInput.SetDate(ControllerBase.ConvertDateDay(_birthDate));
                _arrivalDateInput.SetDate(ControllerBase.ConvertDateDay(_arrivalDate));
                if (_gender == L.Get(TextManager.GENDER_MALE))
                    _genderDropdown.dropdown.SetValueWithoutNotify((int)ControllerUser.Gender.Male);
                if (_gender == L.Get(TextManager.GENDER_FEMALE))
                    _genderDropdown.dropdown.SetValueWithoutNotify((int)ControllerUser.Gender.Female);
                if (_gender == L.Get(TextManager.GENDER_NOPE))
                    _genderDropdown.dropdown.SetValueWithoutNotify((int)ControllerUser.Gender.Nope);
                _nationalityDropdown.dropdown.SetValueWithoutNotify(_stateList.IndexOf(_nationality));
                int institutionIdx = _institutionDropdown.dropdown.options.FindIndex(x => x.text == _institution);
                if (institutionIdx < 0 && !string.IsNullOrEmpty(_institution))
				{
                    institutionIdx = _institutionDropdown.dropdown.options.Count - 1;
                    _otherInstitutionInput.gameObject.SetActive(true);
                }   
                _institutionDropdown.dropdown.SetValueWithoutNotify(institutionIdx);
                int diplomaIdx = _diplomaDropdown.dropdown.options.FindIndex(x => x.text == _diploma);
                if (diplomaIdx < 0 && !string.IsNullOrEmpty(_diploma))
                {
                    diplomaIdx = _diplomaDropdown.dropdown.options.Count - 1;
                    _otherDiplomaInput.gameObject.SetActive(true);
                }
                _diplomaDropdown.dropdown.SetValueWithoutNotify(diplomaIdx);
                _otherDiplomaInput.input.text = _diploma;
                _otherInstitutionInput.input.text = _institution;
                _addressInput.input.text = _address;
                _postalCodeInput.input.text = _postalcode;
                _cityInput.input.text = _city;
                _formationInput.input.text = _formation;
                _title.text = string.IsNullOrEmpty(_pseudo) ? L.Get("id_new_user") : L.Get("id_edit_user");
            }
            else
			{
                _newPasswordLink.SetActive(false);
            }
		}

        private IEnumerator GetAllUserDataEnum()
        {
            MainUI.instance.ShowLoadingPopup();

            DataManager dataMgr = ApplicationManager.instance.dataManager;

            yield return dataMgr.GetAllUserNamesAndMails();

            MainUI.instance.HideLoadingPopup();

            if (dataMgr.userNamesList != null && dataMgr.userMailsList != null)
            {
                _canCheckUsersInBase = true;
                UpdateValidation();
            }
            else
            {
                ClosePopup();
            }
        }

        private void ShowCurrentPage()
        {
            int pageCount = _pages.Length;
            for (int i = 0; i < pageCount; ++i)
            {
                _pages[i].SetActive(_currentPage == i);
            }
            _prevButton.gameObject.SetActive(_currentPage > 0);
            _nextButton.gameObject.SetActive(_currentPage < pageCount - 1);
            _createButton.gameObject.SetActive(_currentPage == pageCount - 1);
            UpdateValidation();
        }

        private void OnPseudoChanged(string text)
        {
            _pseudo = text;
            UpdateValidation();
        }

        private void OnEmailChanged(string text)
        {
            _email = text;
            UpdateValidation();
        }

        private void OnPasswordChanged(string text)
        {
            _password = text;
            UpdateValidation();
        }

        private void OnPasswordVerifChanged(string text)
        {
            _passwordVerif = text;
            UpdateValidation();
        }

        private void OnLastNameChanged(string text)
        {
            _lastName = text;
            UpdateValidation();
        }

        private void OnFirstNameChanged(string text)
        {
            _firstName = text;
            UpdateValidation();
        }

        private void OnGenderChanged(int numElem)
        {
            if (numElem > 0)
			{
                switch ((ControllerUser.Gender)numElem)
				{
                    case ControllerUser.Gender.Male:
                        _gender = L.Get(TextManager.GENDER_MALE);
                        break;
                    case ControllerUser.Gender.Female:
                        _gender = L.Get(TextManager.GENDER_FEMALE);
                        break;
                    case ControllerUser.Gender.Nope:
                        _gender = L.Get(TextManager.GENDER_NOPE);
                        break;
                }
			}
            else
			{
                _gender = null;
            }
            
            UpdateValidation();
        }

        private void OnNationalityChanged(int numElem)
        {
            if (numElem > 0)
            {
                _nationality = _stateList[numElem];
            }
            else
            {
                _nationality = null;
            }
            UpdateValidation();
        }

        private void OnInstitutionChanged(int numElem)
        {
            _institution = null;
            if (numElem > 0 && numElem < _institutionDropdown.dropdown.options.Count - 1)
            {
                _institution = _institutionDropdown.dropdown.options[numElem].text;
                _otherInstitutionInput.gameObject.SetActive(false);
            }
            else if (numElem > 0)
            {
                _otherInstitutionInput.gameObject.SetActive(true);
                _institution = _otherInstitutionInput.input.text;
            }
            UpdateValidation();
        }

        private void OnOtherInstitutionChanged(string text)
        {
            _institution = text;
            UpdateValidation();
        }

        private void OnDiplomaChanged(int numElem)
        {
            _diploma = null;
            if (numElem > 0 && numElem < _diplomaDropdown.dropdown.options.Count - 1)
            {
                _diploma = _diplomaDropdown.dropdown.options[numElem].text;
                _otherDiplomaInput.gameObject.SetActive(false);
            }
            else if (numElem > 0)
            {
                _otherDiplomaInput.gameObject.SetActive(true);
                _diploma = _otherDiplomaInput.input.text;
            }
            UpdateValidation();
        }

        private void OnOtherDiplomaChanged(string text)
        {
            _diploma = text;
            UpdateValidation();
        }

        private void OnFormationChanged(string text)
        {
            _formation = text;
            UpdateValidation();
        }

        private void OnBirthDateChanged(string text)
        {
            _birthDate = text;
            UpdateValidation();
        }

        private void OnAddressChanged(string text)
        {
            _address = text;
            UpdateValidation();
        }

        private void OnPostalCodeChanged(string text)
        {
            _postalcode = text;
            UpdateValidation();
        }

        private void OnCityChanged(string text)
        {
            _city = text;
            UpdateValidation();
        }


        private void OnArrivalDateChanged(string text)
        {
            _arrivalDate = text;
            UpdateValidation();
        }

        private void UpdateValidation()
        {
            bool isValid = true;
            if (_currentPage == 0)
            {
                if (!IsPseudoValid(_pseudoInput, _pseudo))
                    isValid = false;
                if (_user == null && !IsValidEmail(_emailInput, _email))
                    isValid = false;
                if (_user == null && !IsValidPassword(_passwordInput, _password))
                    isValid = false;
                if (_user == null && !IsValidPasswordVerif(_passwordVerifInput, _passwordVerif))
                    isValid = false;
                _nextButton.interactable = isValid;
            }
            else if (_currentPage == 1)
            {
                if (!IsNameValid(_lastNameInput, _lastName))
                    isValid = false;
                if (!IsNameValid(_firstNameInput, _firstName))
                    isValid = false;
                if (!IsDateValid(_birthDateInput, _birthDate, out _birthDateToSend, maxYear: System.DateTime.Now.Year - 18))
                    isValid = false;
                if (!IsDropdownValid(_genderDropdown, _gender))
                    isValid = false;
                if (!IsDropdownValid(_nationalityDropdown, _nationality))
                    isValid = false;
                _nextButton.interactable = isValid;
            }
            else if (_currentPage == 2)
            {
                if (!IsAddressValid(_addressInput, _address))
                    isValid = false;
                if (!IsCodeValid(_postalCodeInput, _postalcode))
                    isValid = false;
                if (!IsCityValid(_cityInput, _city))
                    isValid = false;
                if (DatePickerTitle.GetDateTimeOfDate(_birthDate, out System.DateTime dt))
				{
                    if (!IsDateValid(_arrivalDateInput, _arrivalDate, out _arrivalDateToSend, dt.Year, System.DateTime.Now.Year))
                        isValid = false;
                }
                _nextButton.interactable = isValid;
            }
            else if (_currentPage == 3)
            {
                if (!IsInstitutionValid(_institutionDropdown, _institution))
                    isValid = false;
                if (!IsOtherInstitutionValid(_otherInstitutionInput, _institution))
                    isValid = false;
                if (!IsDiplomaValid(_diplomaDropdown, _diploma))
                    isValid = false;
                if (!IsOtherDiplomaValid(_otherDiplomaInput, _diploma))
                    isValid = false;
                if (!IsFormationValid(_formationInput, _formation))
                    isValid = false;
                _createButton.interactable = isValid;
            }
        }

        private void LoadStateList()
        {
            if (_statesAsset != null)
            {
                _stateList = new List<string>();
                var splitDataset = _statesAsset.text.Split(new char[] { '\r', '\n' });
                foreach (string line in splitDataset)
                {
                    if (!string.IsNullOrEmpty(line))
                    {
                        string name = line.Substring(0, line.IndexOf(';'));
                        _stateList.Add(name);
                    }
                }

                // Update first line
                _stateList[0] = "Choisissez un pays";

                //foreach (var state in _stateList)
                //    Debug.Log("STATE " + state);
                _nationalityDropdown.dropdown.AddOptions(_stateList);
            }
        }   

        private bool IsPseudoValid(InputFieldTitle input, string pseudo)
        {
            input.Clear();
            if (string.IsNullOrEmpty(pseudo))
                return false;
            if (!_canCheckUsersInBase)
                return false;
            if (pseudo.Length < 5)
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_MIN_5_CHAR));
                return false;
            }
            DataManager dataMgr = ApplicationManager.instance.dataManager;
            if (dataMgr.userNamesList.Contains(pseudo))
            {
                bool isPseudoUser = _user != null && _user.username == pseudo;
                if (!isPseudoUser)
				{
                    input.SetError(L.Get(TextManager.INPUT_ERROR_USERNAME_USED));
                    return false;
                }
            }
            input.SetChecked();
            return true;
        }

        private bool IsValidEmail(InputFieldTitle input, string email)
        {
            input.Clear();
            if (string.IsNullOrEmpty(email))
                return false;
            if (!_canCheckUsersInBase)
                return false;
            bool valid = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            if (!valid)
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_MAIL_INVALID));
                return false;
            }
            DataManager dataMgr = ApplicationManager.instance.dataManager;
            if (dataMgr.userMailsList.Contains(email))
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_MAIL_USED));
                return false;
            }
            input.SetChecked();
            return true;
        }

        private bool IsValidPassword(InputFieldTitle input, string password)
        {
            input.Clear();
            if (string.IsNullOrEmpty(password))
                return false;
            if (!_canCheckUsersInBase)
                return false;
            bool valid = Regex.IsMatch(password, @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
            if (!valid)
            {
                if (password.Length < 8)
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_MIN_8_CHAR));
                    return false;
                }
                if (!Regex.IsMatch(password, @"^(?=.*?[a-z])"))
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_LOWER_CASE));
                    return false;
                }
                if (!Regex.IsMatch(password, @"^(?=.*?[A-Z])"))
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_UPPER_CASE));
                    return false;
                }
                if (!Regex.IsMatch(password, @"^(?=.*?[0-9])"))
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_DIGIT));
                    return false;
                }
                if (!Regex.IsMatch(password, @"^(?=.*?[#?!@$%^&*-])"))
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_SPECIAL_CHAR));
                    return false;
                }
                return false;
            }
            input.SetChecked();
            return true;
        }

        private bool IsValidPasswordVerif(InputFieldTitle input, string password)
        {
            input.Clear();
            if (string.IsNullOrEmpty(password))
                return false;
            bool same = _password == password;
            if (!same)
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_PASSWORDS));
                return false;
            }
            input.SetChecked();
            return true;
        }

        private bool IsNameValid(InputFieldTitle input, string name)
        {
            input.Clear();
            if (string.IsNullOrEmpty(name))
                return false;
            if (name.Length < 3)
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_MIN_3_CHAR));
                return false;
            }
            input.SetChecked();
            return true;
        }

        private bool IsDateValid(DatePickerTitle input, string date, out string dateToSend, int minYear = 1930, int maxYear=2023)
        {
            dateToSend = null;
            input.Clear();
            if (string.IsNullOrEmpty(date))
                return false;
            if (DatePickerTitle.GetDateTimeOfDate(date, out System.DateTime dt))
            {
                if (dt.Year < minYear || dt.Year > maxYear)
				{
                    input.SetError(L.Get(TextManager.INPUT_ERROR_DATE_YEAR));
                    return false;
                }
                else
				{
                    dateToSend = ControllerBase.ConvertDateTimeToSend(dt);
                }
            }
            else
            {   
                input.SetError(L.Get(TextManager.INPUT_ERROR_DATE_EXIST));
                return false;
            }
            input.SetChecked();
            return true;
        }

        private bool IsDropdownValid(DropdownTitle dropdown, string text)
        {
            dropdown.Clear();
            if (string.IsNullOrEmpty(text))
                return false;
            dropdown.SetChecked();
            return true;
        }

        private bool IsAddressValid(InputFieldTitle input, string name)
        {
            input.Clear();
            if (string.IsNullOrEmpty(name))
                return false;
            if (name.Length < 3)
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_MIN_3_CHAR));
                return false;
            }
            input.SetChecked();
            return true;
        }

        private bool IsCodeValid(InputFieldTitle input, string name)
        {
            input.Clear();
            if (string.IsNullOrEmpty(name))
                return false;
            if (name.Length != 5)
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_POSTAL_CODE));
                return false;
            }
            input.SetChecked();
            return true;
        }

        private bool IsCityValid(InputFieldTitle input, string name)
        {
            input.Clear();
            if (string.IsNullOrEmpty(name))
                return false;
            if (name.Length < 3)
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_MIN_3_CHAR));
                return false;
            }
            input.SetChecked();
            return true;
        }

        private bool IsInstitutionValid(DropdownTitle dropdown, string name)
        {
            dropdown.Clear();
            if (string.IsNullOrEmpty(name))
                return false;
            dropdown.SetChecked();
            return true;
        }

        private bool IsOtherInstitutionValid(InputFieldTitle input, string name)
        {
            if (input.gameObject.activeInHierarchy)
            {
                input.Clear();
                if (string.IsNullOrEmpty(name))
                    return false;
                if (name.Length < 3)
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_MIN_3_CHAR));
                    return false;
                }
                input.SetChecked();
            }
            return true;
        }

        private bool IsDiplomaValid(DropdownTitle dropdown, string name)
        {
            dropdown.Clear();
            if (string.IsNullOrEmpty(name))
                return false;
            dropdown.SetChecked();
            return true;
        }

        private bool IsOtherDiplomaValid(InputFieldTitle input, string name)
        {
            if (input.gameObject.activeInHierarchy)
            {
                input.Clear();
                if (string.IsNullOrEmpty(name))
                    return false;
                if (name.Length < 3)
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_MIN_3_CHAR));
                    return false;
                }
                input.SetChecked();
            }
            return true;
        }

        private bool IsFormationValid(InputFieldTitle input, string name)
        {
            input.Clear();
            if (string.IsNullOrEmpty(name))
                return false;
            if (name.Length < 3)
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_MIN_3_CHAR));
                return false;
            }
            input.SetChecked();
            return true;
        }

        private void OnBackClicked()
        {
            if (_user == null && !ControllerUser.CheckPseudo(_pseudo))
                return;
            if (_user != null && !ControllerUser.CheckPseudo(_user.username))
                return;
            ClosePopup();
        }

        private void OnPrevClicked()
        {
            if (_currentPage > 0)
                _currentPage--;
            ShowCurrentPage();
        }

        private void OnNextClicked()
        {
            if (_currentPage < _pages.Length)
                _currentPage++;
            ShowCurrentPage();
        }

        private void OnCreateClicked()
        {
            StartCoroutine(CreateUserEnum(_pseudo, _email, _password, 
                _lastName, _firstName, _gender, _birthDateToSend, _nationality,
                _address, _postalcode, _city, _arrivalDateToSend,
                _institution, _diploma, _formation));
        }

        private IEnumerator CreateUserEnum(string username, string mail, string password,
            string lastname, string firstname, string gender, string birthdate, string nationality,
            string address, string postalcode, string city, string arrivaldate,
            string institution, string diploma, string formation)
        {
            if (_user != null)
			{
                string token = ApplicationManager.instance.dataManager.token;
                WebServiceUsersUpdateUser queryUser = new WebServiceUsersUpdateUser(token, _user.id, username/*, mail, password*/,
                lastname, firstname, gender, birthdate, nationality,
                address, postalcode, city, arrivaldate, institution, diploma, formation);

                yield return queryUser.Run();

                if (!queryUser.hasFailed)
                {
                    string message = L.Get(TextManager.USER_UPDATED_SUCCESS).Replace("{username}", username);
                    MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.USER_EDIT), message, FeedbackPopup.IconType.Success);
                    _user.InitFromDic(queryUser.GetResultAsDic());
                    // kTBS
                    Dictionary<string, object> trace = new Dictionary<string, object>()
                        {
                            {"@type", "m:updateUser"},
                            {"m:userId", _user.id},
                            {"m:username", username},
                            {"m:lastname", lastname},
                            {"m:firstname", firstname},
                            {"m:gender", gender},
                            {"m:birthdate", birthdate},
                            {"m:nationality", nationality },
                            {"m:address", address },
                            {"m:postalcode", postalcode},
                            {"m:city", city },
                            {"m:arrivaldate", arrivaldate },
                            {"m:institution", institution },
                            {"m:diploma", diploma },
                            {"m:formation", formation }
                        };
                    TraceManager.DispatchEventTraceToManager(trace);
                    ///
                }
                else
                {
                    string message = L.Get(TextManager.USER_UPDATED_FAILED).Replace("{username}", _user.mail).Replace("{error}", queryUser.error);
                    MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.USER_EDIT), message, FeedbackPopup.IconType.Alert);
                }
            }
            else
			{
                WebServiceUsersCreateUser queryUser = new WebServiceUsersCreateUser(username, mail, password,
                lastname, firstname, gender, birthdate, nationality,
                address, postalcode, city, arrivaldate, institution, diploma, formation);

                yield return queryUser.Run();

                if (!queryUser.hasFailed)
                {
                    string message = L.Get(TextManager.NEW_USER_CREATE_SUCCESS).Replace("{username}", username);
                    MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.NEW_USER_TITLE), message, FeedbackPopup.IconType.Success);
                    ControllerUser createdUser = ApplicationManager.instance.dataManager.CreateUser(queryUser.GetResultAsDic());
                    // kTBS
                    Dictionary<string, object> trace = new Dictionary<string, object>()
                        {
                            {"@type", "m:createUser"},
                            {"m:userId", createdUser.id},
                            {"m:username", username},
                            {"m:lastname", lastname},
                            {"m:firstname", firstname},
                            {"m:gender", gender},
                            {"m:birthdate", birthdate},
                            {"m:nationality", nationality },
                            {"m:address", address },
                            {"m:postalcode", postalcode},
                            {"m:city", city },
                            {"m:arrivaldate", arrivaldate },
                            {"m:institution", institution },
                            {"m:diploma", diploma },
                            {"m:formation", formation }
                        };
                    TraceManager.DispatchEventTraceToManager(trace);
                    ///
                }
                else
                {
                    string message = L.Get(TextManager.NEW_USER_CREATE_FAILED).Replace("{username}", username).Replace("{error}", queryUser.error);
                    MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.NEW_USER_TITLE), message, FeedbackPopup.IconType.Alert);
                }
            }

            ClosePopup();
        }
    }
}