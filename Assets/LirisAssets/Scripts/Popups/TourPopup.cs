namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
	using System.Collections.Generic;
	using Liris.App.WebServices;
	using System.Collections;
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
	using CustomDatePicker;
	using AllMyScripts.LangManager;
    using Liris.App.Trace;

    public class TourPopup : PopupBase
    {
        [Header("Title")]
        [SerializeField]
        private TextMeshProUGUI _topTitle = null;
        [SerializeField]
        private TMP_InputField _inputTitle = null;
        [Header("Public/Private")]
        [SerializeField]
        private Button _visibilityButton = null;
        [SerializeField]
        private TextMeshProUGUI _visibilityButtonText = null;
        //[SerializeField]
        //private Toggle _privateToggle = null;
        //[SerializeField]
        //private Toggle _publicToggle = null;
        [Header("Date")]
		[SerializeField]
        private GameObject _dateNotEditable = null;
        [SerializeField]
        private DatePickerTitle _datePicker = null;
        [Header("Back")]
        [SerializeField]
        private Button _backButton = null;
        [Header("Validation")]
        [SerializeField]
        private Button _okButton = null;
        
        private string _title = null;
        private string _body = null;
        private string _selectedDate = null;
        private string _selectedShare = null;
        //private bool _isPrivate = false;
        private System.Action<ControllerTour> _onResult = null;
        private ControllerTour _tourInEdition = null;
        private bool _canEdit = false;
        private System.DateTime _selectedDateTime;
        private System.DateTime _tourInEditionDateTime;
        
        public void SetPathLine(PathLine line)
        {
            _body = line.GetPositionDataAsString();
        }

        public void SetEditable(bool edit)
        {
            _canEdit = edit;
            _dateNotEditable.SetActive(!edit);

            _selectedDateTime = System.DateTime.Now;
            if (_tourInEdition != null)
			{
                _tourInEditionDateTime = ControllerBase.ConvertInDateTime(_tourInEdition.dateDisplay);
                _selectedDateTime = _tourInEditionDateTime;
            }

            _selectedDate = CalendarController.ConvertDateToString(_selectedDateTime, true);
            _datePicker.SetDate(_selectedDate, true);
        }

        public void SetOnResultAction(System.Action<ControllerTour> onResult)
        {
            _onResult = onResult;
        }

        public void SetTourOnEdition(ControllerTour tour)
        {
            _tourInEdition = tour;
            SetTitle(tour.name);
            _selectedShare = tour.share;
            UpdateShareText();
            //_isPrivate = !tour.isPublic;
            //_privateToggle.SetValue(_isPrivate);
            //_publicToggle.SetValue(!_isPrivate);
            _topTitle.text = L.Get(TextManager.TOURS_TITLE_EDITION);
            SetEditable(tour.canEdit);
        }

        public void SetTitle(string title)
		{
            _title = title;
            _inputTitle.text = title;
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
            _visibilityButton.onClick.AddListener(OnVisibilityChanged);
            //_privateToggle.onValueChanged.AddListener(OnPrivateChanged);
            _inputTitle.onValueChanged.AddListener(OnTitleChanged);
            _datePicker.onDateSelected.AddListener(OnDateChanged);
            _body = "[]";
            _selectedShare = SharePopup.PUBLIC;
            UpdateShareText();
        }

		private void Start()
		{
            CheckValidateButton();
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            _visibilityButton.onClick.RemoveListener(OnVisibilityChanged);
            //_privateToggle.onValueChanged.RemoveListener(OnPrivateChanged);
            _inputTitle.onValueChanged.RemoveListener(OnTitleChanged);
            _datePicker.onDateSelected.RemoveListener(OnDateChanged);
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            if (_tourInEdition != null)
                StartCoroutine(UpdateTourEnum());
            else
                StartCoroutine(SendTourEnum(_canEdit));
        }
        private void OnVisibilityChanged()
        {
            MainUI.instance.ShowSharePopupPrefab(_selectedShare, OnShareUpdated);
        }

        private void OnShareUpdated(string result)
        {
            _selectedShare = result;
            UpdateShareText();
            CheckValidateButton();
        }

        private void UpdateShareText()
        {
            _visibilityButtonText.text = SharePopup.GetShareText(_selectedShare);
        }
        private void CheckValidateButton()
        {
            _selectedDateTime = ControllerBase.ConvertInDateTime(_selectedDate);

            bool interactable = false;
            if (_tourInEdition != null)
            {
                if (_selectedDateTime.Hour != _tourInEditionDateTime.Hour)
                    interactable = true;
                if (_selectedDateTime.Minute != _tourInEditionDateTime.Minute)
                    interactable = true;
                if (_selectedDateTime.Year != _tourInEditionDateTime.Year)
                    interactable = true;
                if (_selectedDateTime.Month != _tourInEditionDateTime.Month)
                    interactable = true;
                if (_selectedDateTime.Day != _tourInEditionDateTime.Day)
                    interactable = true;
                if (_tourInEdition.name != _title)
                    interactable = true;
                if (_tourInEdition.share != _selectedShare)
                    interactable = true;
            }
            else
			{
                if (!string.IsNullOrEmpty(_title))
                    interactable = true;
            }            
            _okButton.interactable = interactable;
        }

        private IEnumerator SendTourEnum(bool canEdit)
        {
            ControllerTour tour = null;
            ControllerUser user = ApplicationManager.instance.user;

            if (user != null)
            {
                string dateDisplay = ControllerBase.ConvertDateTimeToSend(_selectedDateTime);

                if (ApplicationManager.instance.offlineMode)
				{
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["id"] = OfflineIds.GetId(OfflineIds.OfflineIdType.TOUR);
                    dic["user"] = user.id;
                    dic["name"] = _title;
                    dic["body"] = _body;
                    dic["share"] = _selectedShare;
                    dic["canEdit"] = canEdit;
                    dic["date"] = WebServiceBase.GetDateNow();
                    dic["dateModif"] = WebServiceBase.GetDateNow();
                    dic["dateDisplay"] = dateDisplay;
                    tour = ApplicationManager.instance.dataManager.CreateTour(dic);

                    ////////////////// KTBS ////////////////////////             
                    Dictionary<string, object> trace = new Dictionary<string, object>()
                    {
                        {"@type", "m:startTour"},
                        {"m:tourId", tour.id},
                        {"m:tourMode", ApplicationManager.instance.mode},
                        {"m:userId", tour.userId},
                        {"m:dateDisplay", tour.dateDisplay},
                        {"m:dateModif", tour.dateModif},
                        {"m:tourName", tour.name},
                        {"m:tourBody", tour.body},
                        {"m:shared", tour.share}
                    };
                    TraceManager.DispatchEventTraceToManager(trace);
                    ////////////////// END ////////////////////////
                }
                else
				{
                    MainUI.instance.ShowLoadingPopup();

                    string token = ApplicationManager.instance.dataManager.token;

                    WebServiceToursCreateTour query = new WebServiceToursCreateTour(token, user.id, _title, _body, _selectedShare, canEdit, dateDisplay);
                    yield return query.Run();
                    if (!query.hasFailed)
                    {
                        Dictionary<string, object> dic = query.GetResultAsDic();
                        string id = DicTools.GetValueString(dic, "id");
                        if (!string.IsNullOrEmpty(id))
                        {
                            tour = ApplicationManager.instance.dataManager.CreateTour(dic);

                            ////////////////// KTBS ////////////////////////             
                            Dictionary<string, object> trace = new Dictionary<string, object>()
                            {
                                {"@type", "m:startTour"},
                                {"m:tourId", tour.id},
                                {"m:tourMode", ApplicationManager.instance.mode},
                                {"m:userId", tour.userId},
                                {"m:dateDisplay", tour.dateDisplay},
                                {"m:dateModif", tour.dateModif},
                                {"m:tourName", tour.name},
                                {"m:tourBody", tour.body},
                                {"m:shared", tour.share}
                            };
                            TraceManager.DispatchEventTraceToManager(trace);
                            ////////////////// END ////////////////////////
                        }
                    }

                    MainUI.instance.HideLoadingPopup();
                }
                
            }
            
            ClosePopup(tour);
        }

        private IEnumerator UpdateTourEnum()
		{
            string dateDisplay = ControllerBase.ConvertDateTimeToSend(_selectedDateTime);

            if (ApplicationManager.instance.offlineMode)
			{
                _tourInEdition.name = _title;
                _tourInEdition.share = _selectedShare;
                _tourInEdition.dateDisplay = dateDisplay;
            }
            else
			{
                MainUI.instance.ShowLoadingPopup();

                string token = ApplicationManager.instance.dataManager.token;

                ControllerUser user = ApplicationManager.instance.user;
                if (user != null)
                {
                    WebServiceToursUpdateContent query = new WebServiceToursUpdateContent(token, _tourInEdition.id, _title, _selectedShare, dateDisplay);
                    yield return query.Run();
                    if (!query.hasFailed)
                    {
                        Dictionary<string, object> dic = query.GetResultAsDic();
                        string id = DicTools.GetValueString(dic, "id");
                        if (!string.IsNullOrEmpty(id))
                        {
                            _tourInEdition.name = _title;
                            _tourInEdition.share = _selectedShare;
                            _tourInEdition.dateDisplay = dateDisplay;
                        }
                    }
                }

                MainUI.instance.HideLoadingPopup();
            }
            ////////////////// KTBS ////////////////////////             
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:editTour"}, 
                {"m:tourId", _tourInEdition.id},
                {"m:tourMode", ApplicationManager.instance.mode},
                {"m:userId", _tourInEdition.userId},
                {"m:dateDisplay", _tourInEdition.dateDisplay},
                {"m:dateModif", _tourInEdition.dateModif},
                {"m:tourName", _tourInEdition.name},
                {"m:tourBody", _tourInEdition.body},
                {"m:shared", _tourInEdition.share}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            ////////////////// END ////////////////////////

            ClosePopup(_tourInEdition);
        }

        //private void OnPrivateChanged(bool value)
        //{
        //    _isPrivate = value;
        //    CheckValidateButton();
        //}

        private void OnTitleChanged(string title)
        {
            _title = title;
            CheckValidateButton();
        }

        private void OnDateChanged(string text)
		{
            _selectedDate = text;
            CheckValidateButton();
		}

        private void ClosePopup(ControllerTour tour = null)
        {
            _onResult?.Invoke(tour);
            GameObject.Destroy(gameObject);
        }
    }
}
