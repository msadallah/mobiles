namespace Liris.App.Popups
{
    using System.Collections.Generic;
    using Liris.App.Controllers;
    using Liris.App.Trace;
    using UnityEngine;
    using UnityEngine.UI;

    public class VisibilityPopup : PopupBase
    {
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private Toggle[] _toggles = null;

        public void InitFromUser(ControllerUser user)
        {
            bool showAll = user.admin;
            _toggles[(int)ApplicationManager.Visibility.ALL].gameObject.SetActive(showAll);
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
        }

        private void Start()
        {
            int idx = (int)ApplicationManager.instance.visibility;
            for (int i = 0; i < _toggles.Length; ++i)
            {
                _toggles[i].isOn = i == idx;
            }
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            for (int i = 0; i < _toggles.Length; ++i)
            {
                if (_toggles[i].isOn)
                {
                    ApplicationManager.instance.SetVisibility((ApplicationManager.Visibility)i);

                    // kTBS
                    Dictionary<string, object> trace = new Dictionary<string, object>()
                        {
                            {"@type", "m:seePostsMode"},
                            {"m:userId", ApplicationManager.instance.user.id},
                            {"m:mode", ApplicationManager.instance.visibility}
                        };
                    TraceManager.DispatchEventTraceToManager(trace);
                    ///
                }
            }
            ClosePopup();
        }
    }
}
