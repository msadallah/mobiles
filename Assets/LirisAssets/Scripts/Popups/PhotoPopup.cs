namespace Liris.App.Popups
{
	using Lean.Touch;
	using System.Collections.Generic;
	using UnityEngine;
    using UnityEngine.UI;

    public class PhotoPopup : PopupBase
    {
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _closeButton = null;
        [SerializeField]
        private RectTransform _content = null;
        [SerializeField]
        private RectTransform _zoomParent = null;
        [SerializeField]
        private RawImage _image = null;
        [SerializeField]
        private AspectRatioFitter _fitter = null;

        private bool _swiping = false;
        private Rect _startRect;
        private float _startDistance = 0f;
        private Vector2 _minSize = Vector2.zero;
        private Vector2 _maxSize = Vector2.zero;
        private Vector2 _startPos = Vector2.zero;

        public void SetTexture(Texture2D tex)
		{
            _image.texture = tex;
            _fitter.aspectRatio = (float)tex.width / (float)tex.height;
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _closeButton.onClick.AddListener(OnCloseClicked);
            LeanTouch.OnGesture += OnLeanTouchGesture;
            _minSize = _content.rect.size;
            _maxSize = _minSize * 8f;
            _zoomParent.sizeDelta = _minSize;
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _closeButton.onClick.RemoveListener(OnCloseClicked);
            LeanTouch.OnGesture -= OnLeanTouchGesture;
            base.OnDestroy();
        }

		private void LateUpdate()
		{
            if (_swiping)
			{
                _zoomParent.anchoredPosition = _startPos;
                if (!Input.anyKey)
                {
                    _swiping = false;
                }
            }
		}

		private void OnLeanTouchGesture(List<LeanFinger> fingers)
		{
            if (fingers.Count > 1)
			{
                if (_swiping)
				{
                    float distance = Vector2.Distance(fingers[0].LastScreenPosition, fingers[1].LastScreenPosition);
                    float zoom = distance / _startDistance;
                    Vector2 size = _startRect.size * zoom;
                    if (size.x > _maxSize.x || size.y > _maxSize.y)
                        size = _maxSize;
                    if (size.x < _minSize.x || size.y < _minSize.y)
                        size = _minSize;
                    _zoomParent.sizeDelta = size;
                }
                else
                {
                    // Start Swiping
                    _startRect = _zoomParent.rect;
                    _startDistance = Vector2.Distance(fingers[0].LastScreenPosition, fingers[1].LastScreenPosition);
                    _startPos = _zoomParent.anchoredPosition;
                    _swiping = true;
                }
            }
            else
            {
                _swiping = false;
            }
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnCloseClicked()
		{
            ClosePopup();
        }
    }
}
