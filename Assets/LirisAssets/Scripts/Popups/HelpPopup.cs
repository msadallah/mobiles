namespace Liris.App.Popups
{
    using AllMyScripts.LangManager;
    using Liris.App;
    using UnityEngine;
    using UnityEngine.UI;

    public class HelpPopup : PopupBase
    {
        [SerializeField]
        private Button _button = null;
        [SerializeField]
        private string _titleTextId = null;
        [SerializeField]
        private string _contentTextId = null;

        protected override void Awake()
        {
            base.Awake();
            _button.onClick.AddListener(OnButtonClicked);
        }

        protected override void OnDestroy()
        {
            _button.onClick.RemoveListener(OnButtonClicked);
            base.OnDestroy();
        }

        private void OnButtonClicked()
        {
            if (!string.IsNullOrEmpty(_titleTextId) && !string.IsNullOrEmpty(_contentTextId))
            {
                MainUI.instance.ShowFeedbackPopup(L.Get(_titleTextId), L.Get(_contentTextId), Liris.App.Popups.FeedbackPopup.IconType.Help);
            }
        }
    }
}
