namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;

    public class OptionsPopup : PopupBase
    {
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private Toggle _galleryToggle = null;

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
            _galleryToggle.onValueChanged.AddListener(OnGalleryChanged);
        }

        private void Start()
        {
            bool savePhotoInGallery = ApplicationManager.instance.optionsManager.savePhotoInGallery;
            _galleryToggle.SetIsOnWithoutNotify(savePhotoInGallery);
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            _galleryToggle.onValueChanged.RemoveListener(OnGalleryChanged);
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            ClosePopup();
        }

        private void OnGalleryChanged(bool isOn)
		{
            ApplicationManager.instance.optionsManager.SetSavePhotoInGallery(isOn);
        }
    }
}
