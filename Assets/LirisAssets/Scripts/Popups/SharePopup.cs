namespace Liris.App.Popups
{
	using AllMyScripts.Common.Tools;
	using AllMyScripts.LangManager;
	using Liris.App.Controllers;
	using Liris.App.Groups;
	using System.Collections.Generic;
	using UnityEngine;
    using UnityEngine.UI;

    public class SharePopup : PopupBase
    {
        public const string PUBLIC = "public";
        public const string PRIVATE = "private";
        public const string GROUPS = "groups";

        [SerializeField]
        private Toggle _publicToggle = null;
        [SerializeField]
        private Toggle _privateToggle = null;
        [SerializeField]
        private Toggle _groupsToggle = null;
        [SerializeField]
        private GameObject _groupsNotInteractable = null;
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private Transform _parent = null;
        [SerializeField]
        private GroupsItem _itemPrefab = null;

        private Dictionary<string, bool> _groupsDic = null;
        private System.Action<string> _result = null;

        public void InitItems(string share, List<ControllerGroup> ctrlGroups, System.Action<string> result)
        {
            _result = result;
            _groupsDic = new Dictionary<string, bool>();
            List<string> selectedGroups = new List<string>();
            if (share.StartsWith(GROUPS))
			{
                string groups = share.Substring(GROUPS.Length);
                List<object> list = JSON.Deserialize(groups) as List<object>;
                foreach (object o in list)
				{
                    selectedGroups.Add(ConvertTools.ToString(o));
                }
            }
            
            if (ctrlGroups != null && ctrlGroups.Count > 0)
			{
                GroupsManager groupsMgr = ApplicationManager.instance.groupsManager;
                foreach (ControllerGroup group in ctrlGroups)
                {
                    GroupsItem item = GameObject.Instantiate<GroupsItem>(_itemPrefab, _parent);
                    string groupId = group.id;
                    bool isOn = selectedGroups.Contains(groupId);
                    item.InitController(group, isOn, OnClickItem);
                    _groupsDic.Add(groupId, isOn);
                }
            }
            else
			{
                if (share.StartsWith(GROUPS))
                    share = PRIVATE;
                _groupsToggle.interactable = false;
            }

            UpdateTogglesFromShare(share);
        }

        public static string GetShareText(string share)
		{
            string text = null;
            if (share == PUBLIC)
            {
                text = L.Get(TextManager.SHARE_PUBLIC);
            }
            else if (share == PRIVATE)
            {
                text = L.Get(TextManager.SHARE_PRIVATE);
            }
            else if (share.StartsWith(GROUPS))
            {
                string groups = share.Substring(GROUPS.Length);
                List<object> list = JSON.Deserialize(groups) as List<object>;
                
                int count = 0;
                foreach (object o in list)
				{
                    string groupId = ConvertTools.ToString(o);
                    if (!string.IsNullOrEmpty(groupId))
					{
                        if (ApplicationManager.instance.dataManager.GetGroupFromId(groupId) != null)
                        {
                            if (ApplicationManager.instance.groupsManager.IsUserInGroup(groupId))
                                count++;
                        }
                    }
				}
                if (count < 2)
                    text = L.Get(TextManager.SHARE_ONE_GROUP);
                else
                    text = L.Get(TextManager.SHARE_X_GROUPS).Replace("%X%", count.ToString());
            }
            return text;
        }

        private void UpdateTogglesFromShare(string share)
		{
            if (share == PUBLIC)
            {
                _publicToggle.SetValue(true);
            }
            else if (share == PRIVATE)
            {
                _privateToggle.SetValue(true);
            }
            else if (share.StartsWith(GROUPS))
            {
                _groupsToggle.SetValue(true);
            }
        }

        private void OnClickItem(GroupsItem item, bool on)
        {
            if (_groupsDic != null)
			{
                if (_groupsDic.ContainsKey(item.group.id))
				{
                    _groupsDic[item.group.id] = on;
                    int count = GetSelectedGroupCount();
                    if (count == 0)
					{
                        _groupsDic[item.group.id] = true;
                        item.SetIsOn(true);
                    }
                }
			}
        }

        private int GetSelectedGroupCount()
		{
            int count = 0;
            foreach (var keyval in _groupsDic)
			{
                if (keyval.Value)
                    count++;
			}
            return count;
		}

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
            _publicToggle.onValueChanged.AddListener(OnToggleChanged);
            _privateToggle.onValueChanged.AddListener(OnToggleChanged);
            _groupsToggle.onValueChanged.AddListener(OnToggleChanged);
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            _publicToggle.onValueChanged.RemoveListener(OnToggleChanged);
            _privateToggle.onValueChanged.RemoveListener(OnToggleChanged);
            _groupsToggle.onValueChanged.RemoveListener(OnToggleChanged);
            base.OnDestroy();
        }

        private void OnToggleChanged(bool isOn)
		{
            _groupsNotInteractable.SetActive(!_groupsToggle.isOn);
		}

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            if (_publicToggle.isOn)
			{
                _result?.Invoke(PUBLIC);
            }
            else if (_privateToggle.isOn)
			{
                _result?.Invoke(PRIVATE);
            }
            else
			{
                List<int> groupList = new List<int>();
                foreach (var keyval in _groupsDic)
                {
                    if (keyval.Value)
                    {
                        if (int.TryParse(keyval.Key, out int val))
                        {
                            groupList.Add(val);
                        }
                    }
                }
                string groups = JSON.Serialize(groupList);
                _result?.Invoke(GROUPS + groups);
            }
            ClosePopup();
        }
    }
 }
