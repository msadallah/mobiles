namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
	using Liris.App.Controllers;
	using System.Collections;
	using AllMyScripts.LangManager;
    using System.Collections.Generic;
    using Liris.App.Trace;
	using Liris.App.Favorites;
	using Liris.App.UI;
	using AllMyScripts.Common.Tools;
	using Liris.App.Emojis;
	using Liris.App.Notification;

	public class EditTourPopup : PopupBase
    {
        [SerializeField]
        private TextMeshProUGUI _title = null;
        [SerializeField]
        private TextMeshProUGUI _name = null;
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private GameObject _bottomButtons = null;
        [SerializeField]
        private Button _deleteButton = null;
        [SerializeField]
        private Button _moveButton = null;
        [SerializeField]
        private Button _editButton = null;
        [SerializeField]
        private Button _likeButton = null;
        [SerializeField]
        private Button _commentButton = null;
        [Header("Adapt text")]
        [SerializeField]
        private RectTransform _scrollRT = null;
        [SerializeField]
        private RectTransform _contentRT = null;
        [SerializeField]
        private float _minHeight = 128;
        [SerializeField]
        private float _maxHeight = 512;
        [Header("Favorites")]
        [SerializeField]
        private FavoritesButtons _favorites = null;
        [Header("Emojis")]
        [SerializeField]
        private UI_Emojis _emojis = null;
        [Header("Messages")]
        [SerializeField]
        private NotifCount _messageCount = null;
        [Header("Preview")]
        [SerializeField]
        private GameObject _previewGroup = null;
        [SerializeField]
        private Button _previewButton = null;

        private ControllerTour _tour = null;
        private ControllerUser _user = null;
        private List<ControllerMessage> _messages = null;

        public void SetTour(ControllerTour tour, bool canShowBottomButtons = true, bool isInPreview = false)
        {
            _tour = tour;
            _user = tour.GetUser();
            if (_user != null)
                SetTitle(_user.username + "\n" + tour.GetFormattedDate());
            else
                SetTitle(tour.GetFormattedDate());
            SetName(tour.name);
            bool isMe = _user == ApplicationManager.instance.user;
            _bottomButtons.gameObject.SetActive(canShowBottomButtons);
            _moveButton.gameObject.SetActive(isMe && tour.canEdit && !isInPreview);
            _deleteButton.gameObject.SetActive(isMe && !isInPreview);
            _editButton.gameObject.SetActive(isMe && !isInPreview);
            if (_previewGroup != null)
                _previewGroup.SetActive(!isInPreview);
            _favorites.SetFavoritesFull(ApplicationManager.instance.favoritesManager.IsTourFavorite(_tour.id), false);
            if (_emojis != null)
			{
                if (canShowBottomButtons)
                    FillEmojis(_tour.id);
                else
                    _emojis.gameObject.SetActive(false);
            }
            UpdateMessages(null);
        }

        public void SetTitle(string title)
        {
            _title.text = title;
        }

        public void SetName(string name)
		{
            _name.text = name;
            LayoutRebuilder.ForceRebuildLayoutImmediate(_contentRT);
            float height = Mathf.Clamp(_contentRT.sizeDelta.y, _minHeight, _maxHeight);
            _scrollRT.sizeDelta = new Vector2(_scrollRT.sizeDelta.x, height);
        }

        public void OpenChoiceToDelete(System.Action<bool> onDeleteResult = null)
		{
            MainUI.instance.ShowChoicePopup(L.Get(TextManager.TOURS_TITLE), L.Get(TextManager.TOURS_DELETE),
                L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO), (bool result) =>
                {
                    if (result)
                    {
                        List<ControllerAnnotation> annotations = ApplicationManager.instance.dataManager.GetAnnotationsFromTourId(_tour.id);
                        if (annotations != null && annotations.Count > 0)
                        {
                            MainUI.instance.ShowChoicePopup(L.Get(TextManager.TOURS_TITLE), L.Get(TextManager.TOURS_DELETE_ANNOTATIONS),
                            L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO), (bool result) =>
                            {
                                if (result)
                                {
                                    StartCoroutine(DeleteTourEnum(false, onDeleteResult: onDeleteResult));
                                }
                                else
                                {
                                    StartCoroutine(DeleteTourEnum(false, annotations, onDeleteResult));
                                }
                            });
                        }
                        else
                        {
                            StartCoroutine(DeleteTourEnum(false, onDeleteResult: onDeleteResult));
                        }
                    }
                    else
					{
                        onDeleteResult?.Invoke(false);
                    }
                });
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
            _deleteButton.onClick.AddListener(OnDeleteClicked);
            _moveButton.onClick.AddListener(OnMoveClicked);
            _editButton.onClick.AddListener(OnEditClicked);
            _likeButton.onClick.AddListener(OnLikeCliked);
            _commentButton.onClick.AddListener(OnCommentClicked);
            if (_previewButton != null)
                _previewButton.onClick.AddListener(OnPreviewClicked);
            _favorites.onFavoriteStateChanged += OnFavoriteStateChanged;
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            _deleteButton.onClick.RemoveListener(OnDeleteClicked);
            _moveButton.onClick.AddListener(OnMoveClicked);
            _editButton.onClick.AddListener(OnEditClicked);
            _likeButton.onClick.RemoveListener(OnLikeCliked);
            _commentButton.onClick.RemoveListener(OnCommentClicked);
            if (_previewButton != null)
                _previewButton.onClick.RemoveListener(OnPreviewClicked);
            _favorites.onFavoriteStateChanged -= OnFavoriteStateChanged;
            _tour = null;
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            ClosePopup();
        }

        private void OnDeleteClicked()
        {
            OpenChoiceToDelete();
        }

        private IEnumerator DeleteTourEnum(bool autoDelete, List<ControllerAnnotation> annotationsToDetach = null, System.Action<bool> onDeleteResult = null)
        {
            if (ApplicationManager.instance.offlineMode)
			{
                if (annotationsToDetach != null)
				{
                    foreach (ControllerAnnotation annotation in annotationsToDetach)
					{
                        annotation.tourId = null;
					}
				}

                ApplicationManager.instance.RemoveTour(_tour);

                // kTBS
                Dictionary<string, object> trace = new Dictionary<string, object>()
                {
                    {"@type", "m:deleteTour"},
                    {"m:userId", _tour.userId},
                    {"m:tourId", _tour.id},
                    {"m:autoDelete",  autoDelete}
                };
                TraceManager.DispatchEventTraceToManager(trace);
                ///
            }
            else
			{
                MainUI.instance.ShowLoadingPopup();

                if (annotationsToDetach != null)
                {
                    foreach (ControllerAnnotation annotation in annotationsToDetach)
                    {
                        yield return ApplicationManager.instance.dataManager.UpdateAnnotationTour(annotation, null);
                    }
                }

                yield return ApplicationManager.instance.dataManager.DeleteTour(_tour, autoDelete);
                if (!ApplicationManager.instance.dataManager.lastResultFailed)
                {
                    ApplicationManager.instance.RemoveTour(_tour);
                }

                MainUI.instance.HideLoadingPopup();
            }

            onDeleteResult?.Invoke(true);

            ClosePopup();
        }

        private void OnMoveClicked()
        {
            ApplicationManager.instance.MoveTour(_tour);
            ClosePopup();
        }

        private void OnEditClicked()
        {
            MainUI.instance.ShowTourPopupOnEdition(_tour);
            ClosePopup();
        }

        private void OnLikeCliked()
        {
            EmojisManager emojisMgr = ApplicationManager.instance.emojisManager;
            NotificationManager notifMgr = ApplicationManager.instance.notificationManager;
            string emoji = emojisMgr.GetTourEmoji(_tour.id);
            int oldIcon = _emojis.GetIndexFromName(emoji);

            MainUI.instance.ShowEmojisPopup(oldIcon, (int numIcon) =>
            {
                int tourId = ConvertTools.ToInt32(_tour.id);
                if (oldIcon >= 0)
                {
                    _emojis.RemoveIcon(oldIcon);
                    emojisMgr.RemoveTourEmoji(tourId);
                    // KTBS2
                    Dictionary<string, object> trace = new Dictionary<string, object>()
                    {
                        {"@type", "m:removeEmoji"},
                        {"m:userId", ApplicationManager.instance.user.id},
                        {"m:target", "tour"},
                        {"m:tourId", tourId},
                        {"m:emoji", emoji}
                    };
                    TraceManager.DispatchEventTraceToManager(trace);
                
                }
                if (oldIcon != numIcon)
                {
                    _emojis.AddIcon(numIcon);
                    string name = _emojis.GetNameFromIndex(numIcon);
                    emojisMgr.AddTourEmoji(tourId, name);
                    notifMgr.AddNotification(NotificationManager.NotificationType.EMOJI_ON_TOUR, tourId.ToString(), name);
                    // KTBS2
                    Dictionary<string, object> trace = new Dictionary<string, object>()
                    {
                        {"@type", "m:addEmoji"},
                        {"m:userId", ApplicationManager.instance.user.id},
                        {"m:target", "tour"},
                        {"m:tourId", tourId},
                        {"m:emoji", _emojis.GetNameFromIndex(numIcon)}
                    };
                    TraceManager.DispatchEventTraceToManager(trace);
                
                }
            });
        }

        private void OnCommentClicked()
        {
            if (_messages == null || _messages.Count == 0)
                MainUI.instance.ShowEditMessagesPopup(null, _tour.id, null, UpdateMessages);
            else
                MainUI.instance.ShowMessagesPopup(null, _tour.id, _messages, UpdateMessages);
        }

        private void OnPreviewClicked()
		{
            ApplicationManager.instance.ShowPreviewOnTour(_tour);
            ClosePopup();
        }

        private void UpdateMessages(ControllerMessage message)
		{
            _messages = ApplicationManager.instance.dataManager.GetMessagesFromTour(_tour.id);
            int messageCount = _messages != null ? _messages.Count : 0;
            _messageCount.gameObject.SetActive(messageCount > 0);
            _messageCount.SetCount(messageCount);
        }

        private void OnFavoriteStateChanged(FavoritesButtons.FavoriteState state)
        {
            if (_tour == null)
                return;
            switch (state)
            {
                case FavoritesButtons.FavoriteState.EMPTY:
                    ApplicationManager.instance.favoritesManager.RemoveTourFavorite(_tour.id);
                    // KTBS2
                    Dictionary<string, object> trace = new Dictionary<string, object>()
                    {
                        {"@type", "m:addFavorite"}, 
                        {"m:target", "tour"}, 
                        {"m:userId", ApplicationManager.instance.user.id},
                        {"m:tourId", _tour.id}
                    };
                    TraceManager.DispatchEventTraceToManager(trace);
                    
                    break;
                case FavoritesButtons.FavoriteState.FULL:
                    ApplicationManager.instance.favoritesManager.AddTourFavorite(_tour.id);
                    // KTBS2
                    Dictionary<string, object> traceRemove = new Dictionary<string, object>()
                    {
                        {"@type", "m:removeFavorite"}, 
                        {"m:target", "tour"}, 
                        {"m:userId", ApplicationManager.instance.user.id},
                        {"m:tourId", _tour.id}
                    };
                    TraceManager.DispatchEventTraceToManager(traceRemove);
                    
                    break;
            }
        }

        private void FillEmojis(string id)
        {
            Dictionary<string, int> dicEmojis = ApplicationManager.instance.dataManager.GetEmojisForTour(id);
            if (dicEmojis != null)
            {
                foreach (var keyval in dicEmojis)
                {
                    int numIcon = _emojis.GetIndexFromName(keyval.Key);
                    int count = keyval.Value;
                    _emojis.AddIcon(numIcon, count);
                }
            }
        }
    }
}
