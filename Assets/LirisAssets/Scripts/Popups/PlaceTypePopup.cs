namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
	using Liris.App.Assets;
	using Liris.App.Controllers;
	using AllMyScripts.LangManager;
	using CustomDatePicker;
	using Liris.App.UI;
	using System.Collections.Generic;

	public class PlaceTypePopup : PopupBase
    {
        public const string PLACE_TYPE_TOUR = "tour";

        [Header("Place Type")]
        [SerializeField]
        private DatePickerTitle _dateInput = null;
        [SerializeField]
        private Toggle[] _timingToggles = null;
        [SerializeField]
        private SpritesList _placeTypeSprites = null;
        [SerializeField]
        private UI_IconWithTextButton _placeTypeButtonPrefab = null;
        [SerializeField]
        private Transform _placeTypeParent = null;
        [SerializeField]
        private RectTransform _panelRT = null;
        [SerializeField]
        private Button _validButton = null;
        [Header("Back")]
        [SerializeField]
        private Button _backButton = null;

        private UI_IconWithTextButton[] _placeTypeButtons = null;
        private AnnotationPopup _currentAnnotationPopup = null;
        private ControllerTour _currentTour = null;
        private string _selectedPlaceType = null;
        private string _selectedDate = null;
        private ApplicationManager.TimingType _selectedTiming = ApplicationManager.TimingType.UNIQUE;
        private Dictionary<string, int> _placeTypeNameDic = null;

        public void SetCurrentTour(ControllerTour tour)
        {
            _currentTour = tour;
            if (_currentTour == null)
                HideTourType();
        }

        public void SetCurrentAnnotationPopup(AnnotationPopup popup)
		{
            _currentAnnotationPopup = popup;
            if (popup != null)
			{
                _selectedPlaceType = popup.selectedPlaceType;
                UpdatePlaceTypeSelection(_placeTypeNameDic[_selectedPlaceType]);
                _selectedDate = popup.selectedDate;
                System.Enum.TryParse<ApplicationManager.TimingType>(popup.selectedTiming, out _selectedTiming);
                _dateInput.SetDate(_selectedDate, true);
            }
            else
			{
                UpdatePlaceTypeSelection(-1);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            int count = _placeTypeSprites.data.Length;
            _placeTypeNameDic = new Dictionary<string, int>();
            _placeTypeButtons = new UI_IconWithTextButton[count];
            for (int i = 0; i < count; ++i)
            {
                UI_IconWithTextButton btn = GameObject.Instantiate<UI_IconWithTextButton>(_placeTypeButtonPrefab, _placeTypeParent);
                SpritesList.SpriteData data = _placeTypeSprites.data[i];
                btn.SetSprite(data.sprite);
                btn.SetColor(data.color);
                btn.SetLabel(L.Get(_placeTypeSprites.GetTextId(i)));
                _placeTypeNameDic.Add(data.name, i);
                btn.OnClickEvent.AddListener(() => OnPlaceTypeClicked(data.name));
                _placeTypeButtons[i] = btn;
            }
            _validButton.onClick.AddListener(OnValidButton);
            _backButton.onClick.AddListener(OnBackClicked);
            _dateInput.onDateSelected.AddListener(OnDateChanged);
            _selectedDate = ControllerBase.ConvertDateDay(CalendarController.ConvertDateToString(System.DateTime.Now, true));
            _dateInput.SetDate(_selectedDate, true);
            int numToggle = 0;
            foreach (Toggle toggle in _timingToggles)
			{
                int num = numToggle;
                toggle.onValueChanged.AddListener((bool on) => OnTimingToggleChanged(num, on));
                numToggle++;
			}
        }

		private void Start()
		{
            Toggle toggle = _timingToggles[(int)_selectedTiming];
            toggle.SetIsOnWithoutNotify(true);
            UpdateValidButton();
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < _placeTypeButtons.Length; ++i)
            {
                _placeTypeButtons[i].OnClickEvent.RemoveAllListeners();
            }
            _validButton.onClick.RemoveListener(OnValidButton);
            _backButton.onClick.RemoveListener(OnBackClicked);
            _dateInput.onDateSelected.RemoveListener(OnDateChanged);
            foreach (Toggle toggle in _timingToggles)
                toggle.onValueChanged.RemoveAllListeners();
            _currentAnnotationPopup = null;
            base.OnDestroy();
        }

        private void OnValidButton()
		{
            if (_currentAnnotationPopup != null)
                _currentAnnotationPopup.SetPlaceTypeName(_selectedPlaceType, _selectedDate, _selectedTiming, _currentTour);
            else
                MainUI.instance.ShowAnnotationPopup(_selectedPlaceType, _selectedDate, _selectedTiming, _currentTour);
            ClosePopup();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnDateChanged(string text)
		{
            Debug.Log("OnDateChanged " + text);
            _selectedDate = text;
            UpdateValidButton();
        }

        private void OnTimingToggleChanged(int numToggle, bool on)
		{
            Debug.Log("OnTimingToggleChanged " + numToggle + " :" + on);
            _selectedTiming = (ApplicationManager.TimingType)numToggle;
		}

        private void OnPlaceTypeClicked(string name)
        {
            Debug.Log("OnPlaceTypeClicked " + name);
            _selectedPlaceType = name;
            UpdatePlaceTypeSelection(_placeTypeNameDic[name]);
            UpdateValidButton();
        }

        private void UpdatePlaceTypeSelection(int num)
		{
            for (int i = 0; i < _placeTypeButtons.Length; ++i)
            {
                _placeTypeButtons[i].ShowSelection(i == num);
            }
        }

        private void UpdateValidButton()
		{
            bool valid = true;
            if (string.IsNullOrEmpty(_selectedPlaceType))
                valid = false;
            if (string.IsNullOrEmpty(_selectedDate))
                valid = false;
            _validButton.interactable = valid;
		}

        private void HideTourType()
        {
            int count = _placeTypeSprites.data.Length;
            for (int i = 0; i < count; ++i)
            {
                SpritesList.SpriteData data = _placeTypeSprites.data[i];
                if (data.name == PLACE_TYPE_TOUR)
                {
                    _placeTypeButtons[i].gameObject.SetActive(false);
                    _panelRT.sizeDelta = new Vector2(_panelRT.sizeDelta.x, _panelRT.sizeDelta.y - 100f);
                    break;
                }
            }
        }
    }
}
