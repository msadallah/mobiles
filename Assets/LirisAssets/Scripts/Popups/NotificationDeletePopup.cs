namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;

    public class NotificationDeletePopup : PopupBase
    {
        public enum Reason
		{
            IRRELEVANT,
            ALREADY,
            TOO_MANY,
            OTHER
		}

        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private Toggle[] _toggles = null;

        private string _notificationId = null;
        private System.Action<Reason> _result = null;

        public void InitPopup(string notifId, System.Action<Reason> result)
        {
            _notificationId = notifId;
            _result = result;
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
        }

        private void Start()
        {
            int idx = (int)Reason.OTHER;
            for (int i = 0; i < _toggles.Length; ++i)
            {
                _toggles[i].isOn = i == idx;
            }
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            for (int i = 0; i < _toggles.Length; ++i)
            {
                if (_toggles[i].isOn)
                {
                    _result?.Invoke((Reason)i);
                }
            }
            ClosePopup();
        }
    }
}
