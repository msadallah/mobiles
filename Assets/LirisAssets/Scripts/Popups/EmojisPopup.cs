namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;

    public class EmojisPopup : PopupBase
    {
        [SerializeField]
        private Button[] _buttons = null;
        [SerializeField]
        private GameObject[] _cancels = null;
        [SerializeField]
        private Button _backButton = null;

        private System.Action<int> _onResult = null;

        public void SetOnResultAction(System.Action<int> onResult)
        {
            _onResult = onResult;
        }

        public void SetChoosenEmoji(int numIcon)
		{
            for (int i = 0; i < _cancels.Length; ++i)
            {
                _cancels[i].SetActive(i == numIcon);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            for (int i = 0; i < _buttons.Length; ++i)
			{
                int num = i;
                Button btn = _buttons[i];
                btn.onClick.AddListener(() => OnEmojiClicked(num));
            }
            SetChoosenEmoji(-1);
            _backButton.onClick.AddListener(OnBackClicked);
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            for (int i = 0; i < _buttons.Length; ++i)
            {
                _buttons[i].onClick.RemoveAllListeners();
            }
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnEmojiClicked(int numIcon)
        {
            if (_onResult != null)
                _onResult(numIcon);
            ClosePopup();
        }
    }
}
