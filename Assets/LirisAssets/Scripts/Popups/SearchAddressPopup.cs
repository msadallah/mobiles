namespace Liris.App.Popups
{
	using Liris.App.Controllers;
	using Liris.App.Address;
	using System.Collections.Generic;
	using UnityEngine;
    using UnityEngine.UI;
	using Liris.App.UI;
	using System.Collections;
	using Liris.App.WebServices;
	using AllMyScripts.Common.Tools;
    using Liris.App.Trace;

    public class SearchAddressPopup : PopupBase
    {
        [SerializeField]
        private Button _searchButton = null;
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private Transform _parent = null;
        [SerializeField]
        private AddressItem _item = null;
        [SerializeField]
        private InputFieldTitle _input = null;

        public void InitItems(List<ControllerAddress> addresses)
		{
            foreach (Transform tr in _parent)
			{
                GameObject.Destroy(tr.gameObject);
			}
            addresses.Sort((ControllerAddress a, ControllerAddress b) => { return a.distance < b.distance ? -1 : 1; });
            foreach (ControllerAddress address in addresses)
			{
                AddressItem item = GameObject.Instantiate<AddressItem>(_item, _parent);
                item.Init(address.name, address.longitude, address.latitude, OnClickItem);
            }
        }

        private void OnClickItem(AddressItem item)
		{
            ClosePopup();
        }

        protected override void Awake()
        {
            base.Awake();
            _searchButton.onClick.AddListener(OnSearchClick);
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
        }

        protected override void OnDestroy()
        {
            _searchButton.onClick.RemoveListener(OnSearchClick);
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            base.OnDestroy();
        }

        private void OnSearchClick()
		{
            string address = _input.input.text;
            Vector2 mapCoords = ApplicationManager.instance.GetMapsCoords();
            float[] coords = new float[2];
            coords[0] = mapCoords.x;
            coords[1] = mapCoords.y;
            StartCoroutine(LaunchSearcEnum(address, coords));
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            ClosePopup();
        }

        private IEnumerator LaunchSearcEnum(string address, float[] coords)
		{
            WebServiceAddressGet ws = new WebServiceAddressGet(address, coords);
            yield return ws.Run();
            if (!ws.hasFailed)
			{
                List<ControllerAddress> addresses = new List<ControllerAddress>();
                Dictionary<string, object> dic = ws.GetResultAsDic();
                if (dic != null)
				{
                    List<object> features = DicTools.GetValueList(dic, "features");
                    foreach (object o in features)
					{
                        Dictionary<string, object> feature = o as Dictionary<string, object>;
                        ControllerAddress ctrlAddress = new ControllerAddress(feature);
                        addresses.Add(ctrlAddress);
                    }
				}
                if (addresses.Count > 0)
				{
                    InitItems(addresses);
				}
			}

            // KTBS2
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:addressSearch"},
                {"m:userId", ApplicationManager.instance.user.id},
                {"m:address", address}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            
        }
    }
}
