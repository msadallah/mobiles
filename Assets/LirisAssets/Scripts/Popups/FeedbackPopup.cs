namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;

    public class FeedbackPopup : PopupBase
    {
        public enum IconType
        {
            None,
            Success,
            Alert,
            Help
        }

        [SerializeField]
        private TextMeshProUGUI _title = null;
        [SerializeField]
        private TextMeshProUGUI _message = null;
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private GameObject _successIcon = null;
        [SerializeField]
        private GameObject _alertIcon = null;
        [SerializeField]
        private Image _panelImage = null;
        [SerializeField]
        private Color _defaultPanelColor = Color.white;
        [SerializeField]
        private Color _helpPanelColor = Color.white;

        private System.Action _onCloseEvent = null;

        public void SetTitle(string titleText)
        {
            _title.text = titleText;
        }

        public void SetMessage(string msgText)
        {
            _message.text = msgText;
        }

        public void SetIconType(IconType iconType)
        {
            _successIcon.SetActive(iconType == IconType.Success);
            _alertIcon.SetActive(iconType == IconType.Alert);
            _panelImage.color = iconType == IconType.Help ? _helpPanelColor : _defaultPanelColor;
        }

        public void SetOnCloseEvent(System.Action onCloseEvent)
        {
            _onCloseEvent = onCloseEvent;
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            ClosePopup();
        }

        protected override void ClosePopup()
        {
            if (_onCloseEvent != null)
                _onCloseEvent.Invoke();
            base.ClosePopup();
        }
    }
}
