namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;
    using System.Collections;
    using System.Collections.Generic;
    using TMPro;
	using Liris.App.Assets;
    using Liris.App.Controllers;
    using Liris.App.WebServices;	
	using AllMyScripts.Common.Tools;
	using AllMyScripts.LangManager;
	using AllMyScripts.Common.UI;
	using Liris.App.Notification;

	public class EditMessagePopup : PopupBase
    {
        [Header("Title")]
        [SerializeField]
        private TextMeshProUGUI _topTitle = null;
        [Header("Icons")]
        [SerializeField]
        private Button _iconButton = null;
        [SerializeField]
        private SpritesList _iconsSprites = null;
        [SerializeField]
        private MsgIcon _iconPrefab;
        [SerializeField]
        private Transform _iconsParent;
        [Header("Image")]
        [SerializeField]
        private Button _photoButton = null;
        [SerializeField]
        private Button _galleryButton = null;
        [SerializeField]
        private Button _trashImageButton = null;
        [SerializeField]
        private RawImage _pictureRawImage = null;
        [Header("Message")]
        [SerializeField]
        private TMP_InputField _inputMessage = null;
        [Header("Tags")]
        [SerializeField]
        private Button _tagsButton = null;
        [SerializeField]
        private TextMeshProUGUI _tagsText = null;
        [Header("Emoticons")]
        [SerializeField]
        private Button _emoticonsButton = null;
        [SerializeField]
        private Image _emoticonsImage = null;
        [SerializeField]
        private SpritesList _emoticonsSprites = null;
        [SerializeField]
        private TextMeshProUGUI _emoticonText = null;
        [Header("Back")]
        [SerializeField]
        private Button _backButton = null;
        [Header("Validation")]
        [SerializeField]
        private Button _okButton = null;

        public string selectedTiming => _selectedTiming;
        public string selectedDate => _selectedDate;
        public string selectedPlaceType => _selectedPlaceType;

        private string _selectedMessage = "";
        private string _selectedPlaceType = "";
        private string _selectedIcons = "";
        private string _selectedEmoticon = "";
        private string _selectedTags = "";
        private string _selectedDate = null;
        private string _selectedTiming = null;
        private Sprite _defaultEmoticonSprite = null;
        private Texture2D _currentTex = null;
        private bool _isTexCapture = false;
        private bool _hasChangedTex = false;
        private ControllerMessage _messageInEdition = null;
        private string _annotationId = null;
        private string _tourId = null;
        private System.Action<ControllerMessage> _needRefresh = null;

        public void SetEditionMode(string annotationId, string tourId, ControllerMessage message, System.Action<ControllerMessage> needRefresh = null)
        {
            _annotationId = annotationId;
            _tourId = tourId;
            _messageInEdition = message;
            _needRefresh = needRefresh;
            if (message != null)
			{
                _selectedIcons = message.icons;
                _selectedEmoticon = message.emoticon;
                _selectedTags = message.tags;
                _selectedMessage = message.comment;
                _currentTex = message.tex;
                if (_currentTex != null)
                    ShowTexture(_currentTex);
                if (!string.IsNullOrEmpty(_selectedIcons))
                    SetSelectedIcons(_selectedIcons);
                if (!string.IsNullOrEmpty(_selectedEmoticon))
                    SetEmoticon(_selectedEmoticon);
                if (!string.IsNullOrEmpty(_selectedTags))
                    SetTags(_selectedTags);
                if (!string.IsNullOrEmpty(_selectedMessage))
                    SetMessage(_selectedMessage, true);
                _topTitle.gameObject.GetComponent<TextLocalisation>().SetId(TextManager.MESSAGES_TITLE_EDITION);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
            _photoButton.onClick.AddListener(OnPhotoClicked);
            _galleryButton.onClick.AddListener(OnGalleryClicked);
            _trashImageButton.onClick.AddListener(OnTrashImageClicked);
            _iconButton.onClick.AddListener(OnIconClicked);
            _tagsButton.onClick.AddListener(OnTagsClicked);
            _emoticonsButton.onClick.AddListener(OnEmoticonsClicked);
            _inputMessage.onValueChanged.AddListener(OnMessageChanged);
            _pictureRawImage.gameObject.SetActive(false);
            _tagsText.text = "";
            _defaultEmoticonSprite = _emoticonsImage.sprite;
            _emoticonText.text = L.Get(_emoticonsSprites.GetTextIdFromName(_emoticonsSprites.GetFirstName()));
            CheckValidateButton();
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            _photoButton.onClick.RemoveListener(OnPhotoClicked);
            _galleryButton.onClick.RemoveListener(OnGalleryClicked);
            _trashImageButton.onClick.RemoveListener(OnTrashImageClicked);
            _iconButton.onClick.RemoveListener(OnIconClicked);
            _tagsButton.onClick.RemoveListener(OnTagsClicked);
            _emoticonsButton.onClick.RemoveListener(OnEmoticonsClicked);
            _inputMessage.onValueChanged.RemoveListener(OnMessageChanged);
            _defaultEmoticonSprite = null;
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            if (_messageInEdition != null)
                StartCoroutine(UpdateMessageEnum());
            else
                StartCoroutine(SendMessageEnum());
        }

        private void CheckValidateButton()
        {
            bool interactable = false;
            if (_messageInEdition != null)
            {
                if (_messageInEdition.icons != _selectedIcons)
                    interactable = true;
                if (_messageInEdition.emoticon != _selectedEmoticon)
                    interactable = true;
                if (_messageInEdition.tags != _selectedTags)
                    interactable = true;
                if (_messageInEdition.comment != _selectedMessage)
                    interactable = true;
                if (_messageInEdition.tex != _currentTex)
                    interactable = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(_selectedIcons))
                    interactable = true;
                if (!string.IsNullOrEmpty(_selectedEmoticon))
                    interactable = true;
                if (!string.IsNullOrEmpty(_selectedTags))
                    interactable = true;
                if (!string.IsNullOrEmpty(_selectedMessage))
                    interactable = true;
                if (_currentTex != null)
                    interactable = true;
            }
            _okButton.interactable = interactable;
        }

        private IEnumerator SendMessageEnum()
        {
            MainUI.instance.ShowLoadingPopup();

            string token = ApplicationManager.instance.dataManager.token;

            string imageId = "";
            if (_currentTex != null)
            {
                string name = _isTexCapture ? "Capture.png" : "Gallery.png";
                if (ApplicationManager.instance.offlineMode)
                {
                    imageId = OfflineIds.GetId(OfflineIds.OfflineIdType.IMAGE);
                    SpriteLibrary.SetSprite(imageId, _currentTex, System.DateTime.Now.ToString(), format: "png");
                    SpriteLibrary.ApplySaving();
                }
                else
                {
                    WebServiceMediaObjectsUpload query = new WebServiceMediaObjectsUpload(token, name, _currentTex.EncodeToPNG());
                    yield return query.Run();
                    if (!query.hasFailed)
                    {
                        Dictionary<string, object> dic = query.GetResultAsDic();
                        imageId = DicTools.GetValueString(dic, "id");
                    }
                }
            }

            ControllerUser user = ApplicationManager.instance.user;
            if (user != null)
            {
                if (ApplicationManager.instance.offlineMode)
                {
                    string id = OfflineIds.GetId(OfflineIds.OfflineIdType.MESSAGE);
                    ControllerMessage message = CreateMessage(id, imageId, user.id, _annotationId, _tourId);
                    ApplicationManager.instance.dataManager.AddMessage(message);
                    _needRefresh?.Invoke(message);
                }
                else
                {
                    yield return ApplicationManager.instance.dataManager.CreateMessage(user.id, _selectedMessage, imageId,
                        _selectedIcons, _selectedEmoticon, _selectedTags, _annotationId, _tourId, _needRefresh);
                }
                if (_annotationId != null)
                    ApplicationManager.instance.notificationManager.AddNotification(NotificationManager.NotificationType.MESSAGE_ON_ANNOTATION, _annotationId, _selectedMessage);
                else
                    ApplicationManager.instance.notificationManager.AddNotification(NotificationManager.NotificationType.MESSAGE_ON_TOUR, _tourId, _selectedMessage);
            }

            MainUI.instance.HideLoadingPopup();

            ClosePopup();
        }

        private IEnumerator UpdateMessageEnum()
        {
            MainUI.instance.ShowLoadingPopup();

            string token = ApplicationManager.instance.dataManager.token;

            string imageId = _hasChangedTex ? "" : _messageInEdition.imageId;
            if (_currentTex != null && _hasChangedTex)
            {
                string name = _isTexCapture ? "Capture.png" : "Gallery.png";
                if (ApplicationManager.instance.offlineMode)
                {
                    imageId = OfflineIds.GetId(OfflineIds.OfflineIdType.IMAGE);
                    SpriteLibrary.SetSprite(imageId, _currentTex, System.DateTime.Now.ToString(), format: "png");
                    SpriteLibrary.ApplySaving();
                }
                else
				{
                    WebServiceMediaObjectsUpload query = new WebServiceMediaObjectsUpload(token, name, _currentTex.EncodeToPNG());
                    yield return query.Run();
                    if (!query.hasFailed)
                    {
                        Dictionary<string, object> dic = query.GetResultAsDic();
                        imageId = DicTools.GetValueString(dic, "id");
                    }
                }
            }

            ControllerUser user = ApplicationManager.instance.user;
            if (user != null)
            {
                if (ApplicationManager.instance.offlineMode)
                {
                    UpdateMessage(imageId);
                    _needRefresh?.Invoke(_messageInEdition);
                }
                else
				{
                    WebServiceMessagesUpdate query = new WebServiceMessagesUpdate(token, _messageInEdition.id,
                    _selectedMessage, imageId, _selectedIcons, _selectedEmoticon, _selectedTags);
                    yield return query.Run();
                    if (!query.hasFailed)
                    {
                        UpdateMessage(imageId);
                        _needRefresh?.Invoke(_messageInEdition);
                    }
                }
            }

            MainUI.instance.HideLoadingPopup();

            ClosePopup();
        }

        private ControllerMessage CreateMessage(string id, string imageId, string userId, string annotationId = null, string tourId = null)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["id"] = id;
            if (!string.IsNullOrEmpty(imageId))
                dic["image"] = $"/api/media_objects/{imageId}";
            else
                dic["image"] = null;
            dic["tex"] = _pictureRawImage.texture;
            dic["icons"] = _selectedIcons;
            dic["emoticon"] = _selectedEmoticon;
            dic["tags"] = _selectedTags;
            dic["comment"] = _selectedMessage;
            dic["user"] = userId;
            dic["date"] = System.DateTime.Now;
            if (!string.IsNullOrEmpty(annotationId))
                dic["annotation"] = annotationId;
            if (!string.IsNullOrEmpty(tourId))
                dic["tour"] = tourId;
            ControllerMessage message = new ControllerMessage(dic);
            return message;
        }

        private void UpdateMessage(string imageId)
        {
            _messageInEdition.imageId = imageId;
            _messageInEdition.tex = _pictureRawImage.texture as Texture2D;
            _messageInEdition.icons = _selectedIcons;
            _messageInEdition.emoticon = _selectedEmoticon;
            _messageInEdition.tags = _selectedTags;
            _messageInEdition.comment = _selectedMessage;
        }

        private void OnPhotoClicked()
        {
            TakePhoto((Texture2D tex) =>
            {
                _currentTex = tex;
                _isTexCapture = true;
                _hasChangedTex = true;
                ShowTexture(tex);
                CheckValidateButton();
            });
        }

        private void OnGalleryClicked()
        {
            GetPhotoFromGallery((Texture2D tex) =>
            {
                _currentTex = tex;
                _isTexCapture = false;
                _hasChangedTex = true;
                ShowTexture(tex);
                CheckValidateButton();
            });
        }

        private void ShowTexture(Texture2D tex)
        {
            _pictureRawImage.texture = tex;
            _pictureRawImage.GetComponent<AspectRatioFitter>().aspectRatio = (float)tex.width / (float)tex.height;
            _pictureRawImage.gameObject.SetActive(true);
        }

        private void OnTrashImageClicked()
        {
            _currentTex = null;
            _pictureRawImage.gameObject.SetActive(false);
            _pictureRawImage.texture = null;
            _hasChangedTex = true;
            CheckValidateButton();
        }

        private void OnIconClicked()
        {
            MainUI.instance.ShowPlaceIconsPopup(_selectedIcons, (string icons) =>
            {
                SetSelectedIcons(icons);
                CheckValidateButton();
            });
        }

        private void SetSelectedIcons(string icons)
        {
            _selectedIcons = icons;

            foreach (Transform tr in _iconsParent)
                GameObject.Destroy(tr.gameObject);

            string[] names = icons.Split(',');
            foreach (string name in names)
			{
                bool visible = name != _iconsSprites.GetFirstName();
                if (visible)
                {
                    SpritesList.SpriteData data = _iconsSprites.GetData(name);
                    if (data != null)
                    {
                        MsgIcon msg = GameObject.Instantiate<MsgIcon>(_iconPrefab, _iconsParent);
                        msg.SetIconSprite(data.sprite);
                        msg.SetIconColor(data.color);
                    }
                }
            }
        }

        private void OnTagsClicked()
        {
            MainUI.instance.ShowTagsPopup(_selectedTags, (string tags) =>
            {
                SetTags(tags);
                CheckValidateButton();
            });
        }

        private void SetTags(string tags)
        {
            _selectedTags = tags;
            _tagsText.text = tags;
        }

        private void OnEmoticonsClicked()
        {
            MainUI.instance.ShowEmoticonsPopup(_selectedEmoticon, (string name) =>
            {
                SetEmoticon(name);
                CheckValidateButton();
            });
        }

        private void SetEmoticon(string name)
        {
            string firstName = _emoticonsSprites.GetFirstName();
            SpritesList.SpriteData data = _emoticonsSprites.GetData(name);
            if (data != null)
            {
                if (name == firstName)
                {
                    _emoticonsImage.sprite = _defaultEmoticonSprite;
                    _selectedEmoticon = "";
                }
                else
                {
                    _emoticonsImage.sprite = data.sprite;
                    _selectedEmoticon = name;
                }
                _emoticonText.text = L.Get(_emoticonsSprites.GetTextIdFromName(name));
                _emoticonsImage.color = data.color;
            }
            else
            {
                _emoticonsImage.sprite = _defaultEmoticonSprite;
                _selectedEmoticon = "";
                _emoticonText.text = L.Get(firstName);
            }
        }

        private void OnMessageChanged(string message)
        {
            SetMessage(message);
            CheckValidateButton();
        }

        private void SetMessage(string message, bool edit = false)
        {
            _selectedMessage = message;
            if (edit)
                _inputMessage.text = message;
        }
    }
}
