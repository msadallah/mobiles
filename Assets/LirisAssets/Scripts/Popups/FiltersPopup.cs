#define USE_PREFILLED_TEXT

namespace Liris.App.Popups
{
    using UnityEngine;
    using UnityEngine.UI;
	using System.Collections.Generic;
	using Liris.App.UI;
	using Liris.App.Controllers;
	using Liris.App.Data;
	using Liris.App.Filter;
	using Liris.App.Assets;
	using AllMyScripts.LangManager;
    using Liris.App.Trace;

    public class FiltersPopup : PopupBase
    {
        [SerializeField]
        private DatePickerTitle _beginDatePicker = null;
        [SerializeField]
        private DatePickerTitle _endDatePicker = null;
        [SerializeField]
        private DropdownMultiTitle _institutionDropdown = null;
        [SerializeField]
        private DropdownMultiTitle _nationalityDropdown = null;
        [SerializeField]
        private DropdownMultiTitle _diplomaDropdown = null;
        [SerializeField]
        private DropdownMultiTitle _placeTypeDropdown = null;
        [SerializeField]
        private DropdownMultiTitle _placeIconDropdown = null;
        [SerializeField]
        private DropdownMultiTitle _emoticonDropdown = null;
        [SerializeField]
        private DropdownMultiTitle _hashtagDropdown = null;
        [SerializeField]
        private SpritesList _placeTypeSprites = null;
        [SerializeField]
        private SpritesList _placeIconSprites = null;
        [SerializeField]
        private SpritesList _emoticonSprites = null;
        [SerializeField]
        private TagsList _tagsList = null;

        [Header("Buttons")]
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _resetButton = null;
        [SerializeField]
        private Button _validateButton = null;

        private string _beginDate = null;
        private string _endDate = null;
        private List<string> _institutionSelection = null;
        private List<string> _nationalitySelection = null;
        private List<string> _diplomaSelection = null;
        private List<string> _placeTypeSelection = null;
        private List<string> _placeIconSelection = null;
        private List<string> _emoticonSelection = null;
        private List<string> _hashtagSelection = null;
        private List<string> _institutionList = null;
        private List<string> _nationalityList = null;
        private List<string> _diplomaList = null;
        private List<string> _placeTypeList = null;
        private List<string> _placeIconList = null;
        private List<string> _emoticonList = null;
        private List<string> _hashtagList = null;

        private const int PLACE_HOLDER_IDX = 0;
        private const int ALL_IDX = 1;
        private const int NONE_IDX = 2;

        protected override void Awake()
        {
            base.Awake();
            _beginDatePicker.onDateSelected.AddListener(OnBeginDateSelected);
            _endDatePicker.onDateSelected.AddListener(OnEndDateSelected);

            _nationalityDropdown.dropdown.onValueChanged.AddListener(OnNationalityChanged);
            _institutionDropdown.dropdown.onValueChanged.AddListener(OnInstitutionChanged);
            _diplomaDropdown.dropdown.onValueChanged.AddListener(OnDiplomaChanged);
            _placeTypeDropdown.dropdown.onValueChanged.AddListener(OnPlaceTypeChanged);
            _placeIconDropdown.dropdown.onValueChanged.AddListener(OnPlaceIconChanged);
            _emoticonDropdown.dropdown.onValueChanged.AddListener(OnEmoticonChanged);
            _hashtagDropdown.dropdown.onValueChanged.AddListener(OnHashtagChanged);

            _backButton.onClick.AddListener(OnBackClicked);
            _resetButton.onClick.AddListener(OnResetClicked);
            _validateButton.onClick.AddListener(OnValidateClicked);
        }

        private void Start()
        {
            FillLists();
        }

        protected override void OnDestroy()
        {
            _beginDatePicker.onDateSelected.RemoveListener(OnBeginDateSelected);
            _endDatePicker.onDateSelected.RemoveListener(OnEndDateSelected);

            _nationalityDropdown.dropdown.onValueChanged.RemoveListener(OnNationalityChanged);
            _institutionDropdown.dropdown.onValueChanged.RemoveListener(OnInstitutionChanged);
            _diplomaDropdown.dropdown.onValueChanged.RemoveListener(OnDiplomaChanged);
            _placeTypeDropdown.dropdown.onValueChanged.RemoveListener(OnPlaceTypeChanged);
            _placeIconDropdown.dropdown.onValueChanged.RemoveListener(OnPlaceIconChanged);
            _emoticonDropdown.dropdown.onValueChanged.RemoveListener(OnEmoticonChanged);
            _hashtagDropdown.dropdown.onValueChanged.RemoveListener(OnHashtagChanged);

            _backButton.onClick.RemoveListener(OnBackClicked);
            _resetButton.onClick.RemoveListener(OnResetClicked);
            _validateButton.onClick.RemoveListener(OnValidateClicked);
            base.OnDestroy();
        }

        private void FillLists()
        {
            _institutionSelection = new List<string>();
            _nationalitySelection = new List<string>();
            _diplomaSelection = new List<string>();
            _placeTypeSelection = new List<string>();
            _placeIconSelection = new List<string>();
            _emoticonSelection = new List<string>();
            _hashtagSelection = new List<string>();

            _institutionList = new List<string>();
            _nationalityList = new List<string>();
            _diplomaList = new List<string>();
            _placeTypeList = new List<string>();
            _placeIconList = new List<string>();
            _emoticonList = new List<string>();
            _hashtagList = new List<string>();

            _institutionList.Add(_institutionDropdown.placeHolderText);
            _nationalityList.Add(_nationalityDropdown.placeHolderText);
            _diplomaList.Add(_diplomaDropdown.placeHolderText);
            _placeTypeList.Add(_placeTypeDropdown.placeHolderText);
            _placeIconList.Add(_placeIconDropdown.placeHolderText);
            _placeIconList.Add(L.Get(TextManager.FILTER_ALL_F));
            _emoticonList.Add(_emoticonDropdown.placeHolderText);
            _emoticonList.Add(L.Get(TextManager.FILTER_ALL_F));
            _hashtagList.Add(_hashtagDropdown.placeHolderText);
            _hashtagList.Add(L.Get(TextManager.FILTER_ALL_M));
            _hashtagList.Add(L.Get(TextManager.FILTER_NONE));            

            DataManager dataMgr = ApplicationManager.instance.dataManager;
            foreach (ControllerUser user in dataMgr.users)
            {
                string institution = user.institution;
                if (!_institutionList.Contains(institution))
                    _institutionList.Add(institution);
                string nationality = user.nationality;
                if (!_nationalityList.Contains(nationality))
                    _nationalityList.Add(nationality);
                string diploma = user.diploma;
                if (!_diplomaList.Contains(diploma))
                    _diplomaList.Add(diploma);
            }

            for (int i = 0; i < _placeTypeSprites.data.Length; ++i)
			{
                _placeTypeList.Add(L.Get(_placeTypeSprites.GetTextId(i)));
            }

            for (int i = 0; i < _placeIconSprites.data.Length; ++i)
            {
                _placeIconList.Add(L.Get(_placeIconSprites.GetTextId(i)));
            }

            for (int i = 0; i < _emoticonSprites.data.Length; ++i)
            {
                _emoticonList.Add(L.Get(_emoticonSprites.GetTextId(i)));
            }
            
            for (int i = 0; i < _tagsList.data.Length; ++i)
			{
                _hashtagList.Add(_tagsList.data[i].text);
			}

            _institutionDropdown.dropdown.ClearOptions();
            _institutionDropdown.dropdown.AddOptions(_institutionList);
            _nationalityDropdown.dropdown.ClearOptions();
            _nationalityDropdown.dropdown.AddOptions(_nationalityList);
            _diplomaDropdown.dropdown.ClearOptions();
            _diplomaDropdown.dropdown.AddOptions(_diplomaList);
            _placeTypeDropdown.dropdown.ClearOptions();
            _placeTypeDropdown.dropdown.AddOptions(_placeTypeList);
            _placeIconDropdown.dropdown.ClearOptions();
            _placeIconDropdown.dropdown.AddOptions(_placeIconList);
            _emoticonDropdown.dropdown.ClearOptions();
            _emoticonDropdown.dropdown.AddOptions(_emoticonList);
            _hashtagDropdown.dropdown.ClearOptions();
            _hashtagDropdown.dropdown.AddOptions(_hashtagList);

            FilterManager filterMgr = ApplicationManager.instance.filterManager;
            _beginDate = filterMgr.beginDate;
            _endDate = filterMgr.endDate;
            _beginDatePicker.SetDate(_beginDate, true);
            _endDatePicker.SetDate(_endDate, true);
            if (filterMgr.institutionSelection != null && filterMgr.institutionSelection.Count > 0)
            {
                List<int> elems = new List<int>();
                foreach (string sel in filterMgr.institutionSelection)
				{
                    int idx = _institutionList.IndexOf(sel);
                    if (idx >= 0)
					{
                        elems.Add(idx);
                        _institutionSelection.Add(sel);
                    }
                }   
                _institutionDropdown.dropdown.SetValueWithoutNotify(elems);
            }
            if (filterMgr.nationalitySelection != null && filterMgr.nationalitySelection.Count > 0)
            {
                List<int> elems = new List<int>();
                foreach (string sel in filterMgr.nationalitySelection)
				{
                    int idx = _nationalityList.IndexOf(sel);
                    if (idx >= 0)
					{
                        elems.Add(idx);
                        _nationalitySelection.Add(sel);
                    }
                }
                _nationalityDropdown.dropdown.SetValueWithoutNotify(elems);
            }
            if (filterMgr.diplomaSelection != null && filterMgr.diplomaSelection.Count > 0)
            {
                List<int> elems = new List<int>();
                foreach (string sel in filterMgr.diplomaSelection)
				{
                    int idx = _diplomaList.IndexOf(sel);
                    if (idx >= 0)
					{
                        elems.Add(idx);
                        _diplomaSelection.Add(sel);
                    }
                }
                _diplomaDropdown.dropdown.SetValueWithoutNotify(elems);
            }
            if (filterMgr.placeTypeSelection != null && filterMgr.placeTypeSelection.Count > 0)
            {
                List<int> elems = new List<int>();
                foreach (string sel in filterMgr.placeTypeSelection)
                {
                    SpritesList.SpriteData data = _placeTypeSprites.GetData(sel);
                    if (data != null)
                    {
                        string text = L.Get(_placeTypeSprites.GetTextIdFromName(data.name));
                        int idx = _placeTypeList.IndexOf(text);
                        if (idx >= 0)
						{
                            elems.Add(idx);
                            _placeTypeSelection.Add(sel);
                        }   
                    }
                    else
                    {
                        Debug.LogWarning($"FillList placeTypeSelection - data not found for {sel}");
                    }                    
                }
                _placeTypeDropdown.dropdown.SetValueWithoutNotify(elems);
            }
            if (filterMgr.placeIconSelection != null && filterMgr.placeIconSelection.Count > 0)
            {
                List<int> elems = new List<int>();
                foreach (string sel in filterMgr.placeIconSelection)
                {
                    SpritesList.SpriteData data = _placeIconSprites.GetData(sel);
                    if (data != null)
					{
                        string text = L.Get(_placeIconSprites.GetTextIdFromName(data.name));
                        int idx = _placeIconList.IndexOf(text);
                        if (idx >= 0)
						{
                            elems.Add(idx);
                            _placeIconSelection.Add(sel);
                        }
                    }
                    else
					{
                        Debug.LogWarning($"FillList placeIconSelection - data not found for {sel}");
					}
                }
                _placeIconDropdown.dropdown.SetValueWithoutNotify(elems);
            }
            if (filterMgr.emoticonSelection != null && filterMgr.emoticonSelection.Count > 0)
            {
                List<int> elems = new List<int>();
                foreach (string sel in filterMgr.emoticonSelection)
                {
                    SpritesList.SpriteData data = _emoticonSprites.GetData(sel);
                    if (data != null)
                    {
                        string text = L.Get(_emoticonSprites.GetTextIdFromName(data.name));
                        int idx = _emoticonList.IndexOf(text);
                        if (idx >= 0)
						{
                            elems.Add(idx);
                            _emoticonSelection.Add(sel);
                        }
                    }
                    else
					{
                        Debug.LogWarning($"FillList emoticonSelection - data not found for {sel}");
                    }
                }
                _emoticonDropdown.dropdown.SetValueWithoutNotify(elems);
            }
            if (filterMgr.hashtagSelection != null && filterMgr.hashtagSelection.Count > 0)
            {
                List<int> elems = new List<int>();
                foreach (string sel in filterMgr.hashtagSelection)
                {
                    if (sel == FilterManager.NONE_TEXT)
					{
                        elems.Add(NONE_IDX);
                    }
					else
					{
                        TagsList.TagData data = _tagsList.GetData(sel);
                        if (data != null)
                        {
                            string text = data.text;
                            int idx = _hashtagList.IndexOf(text);
                            if (idx >= 0)
							{
                                elems.Add(idx);
                                _hashtagSelection.Add(sel);
                            }
                        }
                        else
                        {
                            Debug.LogWarning($"FillList hashtagSelection - data not found for {sel}");
                        }
                    }
                }
                _hashtagDropdown.dropdown.SetValueWithoutNotify(elems);
            }
            _resetButton.interactable = filterMgr.GetFilterCount() > 0;
        }

        private void OnNationalityChanged(List<int> elems, int selected)
        {
            if (_nationalitySelection == null)
                _nationalitySelection = new List<string>();
            else
                _nationalitySelection.Clear();
            foreach (int elem in elems)
			{
                string text = _nationalityList[elem];
                if (text == _nationalityDropdown.placeHolderText)
                {
                    _nationalitySelection.Clear();
                    _nationalityDropdown.dropdown.SetValueWithoutNotify(new List<int>());
                    break;
                }
                else
                {
                    _nationalitySelection.Add(text);
                }
            }
            UpdateResetButton();
        }

        private void OnInstitutionChanged(List<int> elems, int selected)
        {
            if (_institutionSelection == null)
                _institutionSelection = new List<string>();
            else
                _institutionSelection.Clear();
            foreach (int elem in elems)
			{
                string text = _institutionList[elem];
                if (text ==_institutionDropdown.placeHolderText)
                {
                    _institutionSelection.Clear();
                    _institutionDropdown.dropdown.SetValueWithoutNotify(new List<int>());
                    break;
                }
                else
                {
                    _institutionSelection.Add(text);
                }
            }
            UpdateResetButton();
        }

        private void OnDiplomaChanged(List<int> elems, int selected)
        {
            if (_diplomaSelection == null)
                _diplomaSelection = new List<string>();
            else
                _diplomaSelection.Clear();
            foreach (int elem in elems)
            {
                string text = _diplomaList[elem];
                if (text == _diplomaDropdown.placeHolderText)
                {
                    _diplomaSelection.Clear();
                    _diplomaDropdown.dropdown.SetValueWithoutNotify(new List<int>());
                    break;
                }
                else
                {
                    _diplomaSelection.Add(text);
                }
            }
            UpdateResetButton();
        }

        private void OnPlaceTypeChanged(List<int> elems, int selected)
		{
            if (_placeTypeSelection == null)
                _placeTypeSelection = new List<string>();
            else
                _placeTypeSelection.Clear();
            foreach (int elem in elems)
            {
                string text = _placeTypeList[elem];
                if (text == _placeTypeDropdown.placeHolderText)
                {
                    _placeTypeSelection.Clear();
                    _placeTypeDropdown.dropdown.SetValueWithoutNotify(new List<int>());
                    break;
                }
                else
                {
                    SpritesList.SpriteData data = _placeTypeSprites.GetDataFromText(text, L.Get);
                    if (data != null)
					{
                        _placeTypeSelection.Add(data.name);
                    }
                }
            }
            UpdateResetButton();
        }

        private void OnPlaceIconChanged(List<int> elems, int selected)
        {
            if (_placeIconSelection == null)
                _placeIconSelection = new List<string>();
            else
                _placeIconSelection.Clear();

            if (selected == PLACE_HOLDER_IDX)
            {
                _placeIconDropdown.dropdown.SetValueWithoutNotify(new List<int>());
            }
            else if (selected == ALL_IDX)
            {
                List<int> idx = new List<int>();
                for (int i = 3; i < _placeIconList.Count; ++i)
                {
                    string text = _placeIconList[i];
                    SpritesList.SpriteData data = _placeIconSprites.GetDataFromText(text, L.Get);
                    if (data != null)
					{
                        _placeIconSelection.Add(data.name);
                        idx.Add(i);
                    }
                }
                _placeIconDropdown.dropdown.SetValueWithoutNotify(idx);
            }
            else if (selected == NONE_IDX)
            {
                _placeIconSelection.Add(FilterManager.NONE_TEXT);
                _placeIconDropdown.dropdown.SetValueWithoutNotify(new List<int>() { NONE_IDX });
            }
            else
            {
                if (elems.Contains(NONE_IDX))
                {
                    elems.Remove(NONE_IDX);
                    _placeIconDropdown.dropdown.RefreshShownValue();
                }
                foreach (int elem in elems)
                {
                    string text = _placeIconList[elem];
                    SpritesList.SpriteData data = _placeIconSprites.GetDataFromText(text, L.Get);
                    if (data != null)
                    {
                        _placeIconSelection.Add(data.name);
                        _resetButton.interactable = true;
                    }
                }
            }
            UpdateResetButton();
        }

        private void OnEmoticonChanged(List<int> elems, int selected)
        {
            if (_emoticonSelection == null)
                _emoticonSelection = new List<string>();
            else
                _emoticonSelection.Clear();

            if (selected == PLACE_HOLDER_IDX)
            {
                _emoticonDropdown.dropdown.SetValueWithoutNotify(new List<int>());
            }
            else if (selected == ALL_IDX)
            {
                List<int> idx = new List<int>();
                for (int i = 3; i < _emoticonList.Count; ++i)
                {
                    string text = _emoticonList[i];
                    SpritesList.SpriteData data = _emoticonSprites.GetDataFromText(text, L.Get);
                    if (data != null)
                    {
                        _emoticonSelection.Add(data.name);
                        idx.Add(i);
                    }
                }
                _emoticonDropdown.dropdown.SetValueWithoutNotify(idx);
            }
            else if (selected == NONE_IDX)
            {
                _emoticonSelection.Add(FilterManager.NONE_TEXT);
                _emoticonDropdown.dropdown.SetValueWithoutNotify(new List<int>() { NONE_IDX });
            }
            else
			{
                if (elems.Contains(NONE_IDX))
                {
                    elems.Remove(NONE_IDX);
                    _emoticonDropdown.dropdown.RefreshShownValue();
                }
                foreach (int elem in elems)
                {
                    string text = _emoticonList[elem];
                    SpritesList.SpriteData data = _emoticonSprites.GetDataFromText(text, L.Get);
                    if (data != null)
                    {
                        _emoticonSelection.Add(data.name);
                    }
                }
            }
            UpdateResetButton();
        }

        private void OnHashtagChanged(List<int> elems, int selected)
        {
            if (_hashtagSelection == null)
                _hashtagSelection = new List<string>();
            else
                _hashtagSelection.Clear();

            if (selected == PLACE_HOLDER_IDX)
            {
                _hashtagDropdown.dropdown.SetValueWithoutNotify(new List<int>());
            }
            else if (selected == ALL_IDX)
			{
                List<int> idx = new List<int>();
                for (int i = 3; i < _hashtagList.Count; ++i)
                {
                    string text = _hashtagList[i];
                    TagsList.TagData data = _tagsList.GetDataFromText(text);
                    if (data != null)
                    {
                        _hashtagSelection.Add(data.name);
                        idx.Add(i);
                    }
                }
                _hashtagDropdown.dropdown.SetValueWithoutNotify(idx);
            }
            else if (selected == NONE_IDX)
			{
                _hashtagSelection.Add(FilterManager.NONE_TEXT);
                _hashtagDropdown.dropdown.SetValueWithoutNotify(new List<int>() { NONE_IDX });
            }
			else
			{
                if (elems.Contains(NONE_IDX))
				{
                    elems.Remove(NONE_IDX);
                    _hashtagDropdown.dropdown.RefreshShownValue();
                }
                foreach (int elem in elems)
                {
                    string text = _hashtagList[elem];                    
                    TagsList.TagData data = _tagsList.GetDataFromText(text, L.Get);
                    if (data != null)
                    {
                        _hashtagSelection.Add(data.name);
                    }
                }            
            }
            UpdateResetButton();
        }

        private void OnBeginDateSelected(string date)
		{
            _beginDate = date;
            CheckDates();
            UpdateResetButton();
        }

        private void OnEndDateSelected(string date)
		{
            _endDate = date;
            CheckDates();
            UpdateResetButton();
        }
        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnResetClicked()
		{
            MainUI.instance.ShowChoicePopup(L.Get(TextManager.FILTER_TITLE), L.Get(TextManager.FILTER_RESET_POPUP), 
                L.Get(TextManager.GENERAL_YES), L.Get(TextManager.GENERAL_NO), (bool result) =>
                {
                    if (result)
                    {
                        FilterManager filterMgr = ApplicationManager.instance.filterManager;
                        filterMgr.Clear();
                        FillLists();
                    }
                });
        }

        private void CheckDates()
		{
            _endDatePicker.Clear();
            bool ok = true;
            if (!string.IsNullOrEmpty(_beginDate) && DatePickerTitle.GetDateTimeOfDate(_beginDate, out System.DateTime dt1))
			{
                if (!string.IsNullOrEmpty(_endDate) && DatePickerTitle.GetDateTimeOfDate(_endDate, out System.DateTime dt2))
                {
                    if (dt1 > dt2)
					{
                        _endDatePicker.SetError(L.Get(TextManager.INPUT_ERROR_DATE_EXIST));
                        ok = false;
					}
                }
            }
            _validateButton.interactable = ok;
        }

        private void UpdateResetButton()
		{
            bool canReset = false;
            if (!string.IsNullOrEmpty(_beginDate))
                canReset = true;
            if (!string.IsNullOrEmpty(_endDate))
                canReset = true;
            if (_institutionSelection != null && _institutionSelection.Count > 0)
                canReset = true;
            if (_nationalitySelection != null && _nationalitySelection.Count > 0)
                canReset = true;
            if (_diplomaSelection != null && _diplomaSelection.Count > 0)
                canReset = true;
            if (_placeTypeSelection != null && _placeTypeSelection.Count > 0)
                canReset = true;
            if (_placeIconSelection != null && _placeIconSelection.Count > 0)
                canReset = true;
            if (_emoticonSelection != null && _emoticonSelection.Count > 0)
                canReset = true;
            if (_hashtagSelection != null && _hashtagSelection.Count > 0)
                canReset = true;
            _resetButton.interactable = canReset;
        }

        private void OnValidateClicked()
        {
            FilterManager filterMgr = ApplicationManager.instance.filterManager;
            filterMgr.Clear();
            filterMgr.SetBeginDate(_beginDate);
            filterMgr.SetEndDate(_endDate);
            if (_institutionSelection != null)
            {
                foreach (string item in _institutionSelection)
                    filterMgr.AddInstitution(item);
            }
            if (_nationalitySelection != null)
            {
                foreach (string item in _nationalitySelection)
                    filterMgr.AddNationality(item);
            }
            if (_diplomaSelection != null)
            {
                foreach (string item in _diplomaSelection)
                    filterMgr.AddDiploma(item);
            }
            if (_placeTypeSelection != null)
            {
                foreach (string item in _placeTypeSelection)
                    filterMgr.AddPlaceType(item);
            }
            if (_placeIconSelection != null)
            {
                foreach (string item in _placeIconSelection)
                    filterMgr.AddPlaceIcon(item);
            }
            if (_emoticonSelection != null)
            {
                foreach (string item in _emoticonSelection)
                    filterMgr.AddEmoticon(item);
            }
            if (_hashtagSelection != null)
            {
                foreach (string item in _hashtagSelection)
                    filterMgr.AddHashtag(item);
            }
            filterMgr.SavePreferences(ApplicationManager.instance.user);
            ApplicationManager.instance.RefreshAllData();
            ClosePopup();
            // /////////////// KTBS ////////////////////////
            Dictionary<string, object> loginDetails = new Dictionary<string, object>()
            {
                {"@type", "m:Filter"},
                {"m:userId", ApplicationManager.instance.user.id},
                {"m:beginDate", filterMgr.beginDate},
                {"m:endDate", filterMgr.endDate},
                {"m:institution", filterMgr.institutionSelection},
                {"m:nationality", filterMgr.nationalitySelection},
                {"m:diploma", filterMgr.diplomaSelection},
                {"m:placeType", filterMgr.placeTypeSelection},
                {"m:icons", filterMgr.placeIconSelection},
                {"m:emoticon", filterMgr.emoticonSelection},
                {"m:tags", filterMgr.hashtagSelection}
            };
            TraceManager.DispatchEventTraceToManager(loginDetails);
            /// 
        }
    }
}