namespace Liris.App.Popups
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.UI;
    using System.Text.RegularExpressions;
    using Liris.App.WebServices;
	using System.Collections.Generic;
	using Liris.App.UI;
	using Liris.App.Controllers;
	using Liris.App.Data;
	using AllMyScripts.LangManager;

	public class NewUserPopup : PopupBase
    {
        [SerializeField]
        private InputFieldTitle _emailInput = null;
        [SerializeField]
        private InputFieldTitle _passwordInput = null;
        [SerializeField]
        private InputFieldTitle _passwordVerifInput = null;

        [Header("Buttons")]
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _createButton = null;

        private string _email = null;
        private string _password = null;
        private string _passwordVerif = null;
        private bool _canCheckUsersInBase = false;


        protected override void Awake()
        {
            base.Awake();
            _emailInput.input.onValueChanged.AddListener(OnEmailChanged);
            _passwordInput.input.onValueChanged.AddListener(OnPasswordChanged);
            _passwordVerifInput.input.onValueChanged.AddListener(OnPasswordVerifChanged);

            _backButton.onClick.AddListener(OnBackClicked);
            _createButton.onClick.AddListener(OnCreateClicked);
        }

        private void Start()
        {
            UpdateValidation();
            StartCoroutine(GetAllUserDataEnum());
        }

        protected override void OnDestroy()
        {
            _emailInput.input.onValueChanged.RemoveListener(OnEmailChanged);
            _passwordInput.input.onValueChanged.RemoveListener(OnPasswordChanged);
            _passwordVerifInput.input.onValueChanged.RemoveListener(OnPasswordVerifChanged);

            _backButton.onClick.RemoveListener(OnBackClicked);
            _createButton.onClick.RemoveListener(OnCreateClicked);
            base.OnDestroy();
        }

        private IEnumerator GetAllUserDataEnum()
        {
            MainUI.instance.ShowLoadingPopup();

            DataManager dataMgr = ApplicationManager.instance.dataManager;

            yield return dataMgr.GetAllUserNamesAndMails();

            MainUI.instance.HideLoadingPopup();

            if (dataMgr.userNamesList != null && dataMgr.userMailsList != null)
            {
                _canCheckUsersInBase = true;
            }
            else
            {
                ClosePopup();
            }
        }

        private void OnEmailChanged(string text)
        {
            _email = text;
            UpdateValidation();
        }

        private void OnPasswordChanged(string text)
        {
            _password = text;
            UpdateValidation();
        }

        private void OnPasswordVerifChanged(string text)
        {
            _passwordVerif = text;
            UpdateValidation();
        }

        private void UpdateValidation()
        {
            bool isValid = true;
            if (!IsValidEmail(_emailInput, _email))
                isValid = false;
            if (!IsValidPassword(_passwordInput, _password))
                isValid = false;
            if (!IsValidPasswordVerif(_passwordVerifInput, _passwordVerif))
                isValid = false;
            _createButton.interactable = isValid;
        }


        private bool IsValidEmail(InputFieldTitle input, string email)
        {
            input.Clear();
            if (string.IsNullOrEmpty(email))
                return false;
            if (!_canCheckUsersInBase)
                return false;
            bool valid = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            if (!valid)
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_MAIL_INVALID));
                return false;
            }
            DataManager dataMgr = ApplicationManager.instance.dataManager;
            if (dataMgr.userMailsList.Contains(email))
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_MAIL_USED));
                return false;
            }
            input.SetChecked();
            return true;
        }

        private bool IsValidPassword(InputFieldTitle input, string password)
        {
            input.Clear();
            if (string.IsNullOrEmpty(password))
                return false;
            bool valid = Regex.IsMatch(password, @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
            if (!valid)
            {
                if (password.Length < 8)
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_MIN_8_CHAR));
                    return false;
                }
                if (!Regex.IsMatch(password, @"^(?=.*?[a-z])"))
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_LOWER_CASE));
                    return false;
                }
                if (!Regex.IsMatch(password, @"^(?=.*?[A-Z])"))
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_UPPER_CASE));
                    return false;
                }
                if (!Regex.IsMatch(password, @"^(?=.*?[0-9])"))
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_DIGIT));
                    return false;
                }
                if (!Regex.IsMatch(password, @"^(?=.*?[#?!@$%^&*-])"))
                {
                    input.SetError(L.Get(TextManager.INPUT_ERROR_SPECIAL_CHAR));
                    return false;
                }
                return false;
            }
            input.SetChecked();
            return true;
        }

        private bool IsValidPasswordVerif(InputFieldTitle input, string password)
        {
            input.Clear();
            if (string.IsNullOrEmpty(password))
                return false;
            bool same = _password == password;
            if (!same)
            {
                input.SetError(L.Get(TextManager.INPUT_ERROR_PASSWORDS));
                return false;
            }
            input.SetChecked();
            return true;
        }

        private void OnCreateClicked()
        {
            StartCoroutine(CreateNewUserEnum(_email, _password));
        }

        private IEnumerator CreateNewUserEnum(string mail, string password)
        {
            WebServiceUsersCreateUser user = new WebServiceUsersCreateUser(mail, password);

            yield return user.Run();
            
            if (!user.hasFailed)
            {
                string message = L.Get(TextManager.NEW_USER_CREATE_SUCCESS).Replace("{username}", mail);
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.NEW_USER_TITLE), message, FeedbackPopup.IconType.Success);
                ApplicationManager.instance.dataManager.CreateUser(user.GetResultAsDic());
            }
            else
            {
                string message = L.Get(TextManager.NEW_USER_CREATE_FAILED).Replace("{username}", mail).Replace("{error}", user.error);
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.NEW_USER_TITLE), message, FeedbackPopup.IconType.Alert);
            }

            ClosePopup();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }
    }
}