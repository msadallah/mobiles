namespace Liris.App.Popups
{
	using AllMyScripts.Common.Tools;
	using AllMyScripts.LangManager;
	using Liris.App.Controllers;
	using Liris.App.Groups;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
    using UnityEngine.UI;

    public class GroupsPopup : PopupBase
    {
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private Transform _parent = null;
        [SerializeField]
        private GroupsItem _itemPrefab = null;

        private Dictionary<string, bool> _groupsDic = null;
        private Dictionary<string, bool> _groupsAtInitDic = null;

        public void InitItems(List<ControllerGroup> groups)
        {
            _groupsDic = new Dictionary<string, bool>();
            _groupsAtInitDic = new Dictionary<string, bool>();
            
            if (groups != null && groups.Count > 0)
			{
                bool admin = ApplicationManager.instance.user.admin;
                GroupsManager groupsMgr = ApplicationManager.instance.groupsManager;
                foreach (ControllerGroup group in groups)
                {
                    if (admin || !group.admin)
					{
                        GroupsItem item = GameObject.Instantiate<GroupsItem>(_itemPrefab, _parent);
                        string groupId = group.id;
                        bool isOn = groupsMgr.IsUserInGroup(group.id);
                        item.InitController(group, isOn, OnClickItem);
                        _groupsDic.Add(groupId, isOn);
                        _groupsAtInitDic.Add(groupId, isOn);
                    }
                }
            }
            
            CheckCanUpdate();
        }

        private void OnClickItem(GroupsItem item, bool on)
        {
            if (_groupsDic != null)
			{
                if (_groupsDic.ContainsKey(item.group.id))
				{
                    _groupsDic[item.group.id] = on;
                    CheckCanUpdate();
                }
			}
        }

        private void CheckCanUpdate()
		{
            bool canUpdate = false;
            foreach (var keyval in _groupsDic)
			{
                if (_groupsAtInitDic[keyval.Key] != keyval.Value)
				{
                    canUpdate = true;
                    break;
				}
			}
            _okButton.interactable = canUpdate;
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            base.OnDestroy();
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            List<int> groupList = new List<int>();
            foreach (var keyval in _groupsDic)
			{
                if (keyval.Value)
				{
                    if (int.TryParse(keyval.Key, out int val))
					{
                        groupList.Add(val);
                    }
				}
			}
            StartCoroutine(UpdateGroupsEnum(groupList));
        }

        private IEnumerator UpdateGroupsEnum(List<int> groupList)
		{
            MainUI.instance.ShowLoadingPopup();

            string groups = JSON.Serialize(groupList);
            yield return ApplicationManager.instance.dataManager.UpdateUserGroups(ApplicationManager.instance.user.id, groups);

            MainUI.instance.HideLoadingPopup();

            if (!ApplicationManager.instance.dataManager.lastResultFailed)
			{
                ApplicationManager.instance.user.usergroups = groups;
                GroupsManager groupsMgr = ApplicationManager.instance.groupsManager;
                groupsMgr.SetGroups(groupList);
                ApplicationManager.instance.SetVisibility(ApplicationManager.instance.visibility);
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.GROUPS_TITLE), L.Get(TextManager.GROUPS_UPDATE_OK), FeedbackPopup.IconType.Success);
            }
            else
			{
                MainUI.instance.ShowFeedbackPopup(L.Get(TextManager.GROUPS_TITLE), L.Get(TextManager.GROUPS_UPDATE_ERROR), FeedbackPopup.IconType.Alert);
			}

            ClosePopup();
        }
    }
 }
