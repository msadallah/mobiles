namespace Liris.App.Popups
{
	using UnityEngine;
    using UnityEngine.UI;
    using Liris.App.UI;
	using Liris.App.Assets;
	using TMPro;

	public class IconsPopup : PopupBase
    {
        [SerializeField]
        private IconsCategory[] _categories = null;
        [SerializeField]
        private MsgIcon _selectedIconPrefab = null;
        [SerializeField]
        private Transform _selectedIconsRoot = null;
        [SerializeField]
        private SpritesList _iconSprites = null;
        [SerializeField]
        private TextMeshProUGUI _iconCounter = null;
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private int _maxIcons = 10;

        private System.Action<string> _onResult = null;
        private string _selectedIcons = null;

        public void SetSelectedIcons(string icons, bool updateCanAddIcon = false)
		{
            _selectedIcons = icons;

            foreach (Transform tr in _selectedIconsRoot)
                GameObject.Destroy(tr.gameObject);

            int iconCount = 0;
            string[] names = icons.Split(',');
            foreach (string name in names)
			{
                SpritesList.SpriteData data = _iconSprites.GetData(name);
                if (data != null)
				{
                    MsgIcon msg = GameObject.Instantiate<MsgIcon>(_selectedIconPrefab, _selectedIconsRoot);
                    msg.SetIconSprite(data.sprite);
                    msg.SetIconColor(data.color);
                    iconCount++;
                }
            }

            _iconCounter.text = iconCount.ToString() + "/" + _maxIcons.ToString();

            if (updateCanAddIcon)
			{
                foreach (IconsCategory category in _categories)
                {
                    category.SetCanAddIcon(iconCount < _maxIcons);
                }
            }
        }
     
        public void SetOnResultAction(System.Action<string> onResult)
        {
            _onResult = onResult;
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _okButton.onClick.AddListener(OnOkClicked);
        }

		private void Start()
		{
            foreach (IconsCategory category in _categories)
            {
                category.SetSelectedIcon(_selectedIcons);
                category.RegisterSelectionEvent(UpdateSelection);
            }
            UpdateSelection();
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            foreach (IconsCategory category in _categories)
            {
                category.UnregisterSelectionEvent(UpdateSelection);
            }
            base.OnDestroy();
        }

        private void UpdateSelection()
		{
            _selectedIcons = "";
            foreach (IconsCategory category in _categories)
            {
                if (!string.IsNullOrEmpty(category.currentSelection))
                {
                    if (string.IsNullOrEmpty(_selectedIcons))
                        _selectedIcons = category.currentSelection;
                    else
                        _selectedIcons += "," + category.currentSelection;
                }
            }
            SetSelectedIcons(_selectedIcons, true);
        }

        private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {            
            if (_onResult != null)
                _onResult(_selectedIcons);
            ClosePopup();
        }
    }
}
