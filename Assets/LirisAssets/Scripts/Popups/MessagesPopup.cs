namespace Liris.App.Popups
{
	using Liris.App.Controllers;
	using Liris.App.Messages;
	using System.Collections.Generic;
	using UnityEngine;
    using UnityEngine.UI;

    public class MessagesPopup : PopupBase
    {
        [SerializeField]
        private Button _backButton = null;
        [SerializeField]
        private Button _answerButton = null;
        [SerializeField]
        private Button _okButton = null;
        [SerializeField]
        private Transform _answerButtonRoot = null;
        [SerializeField]
        private Transform _parent = null;
        [SerializeField]
        private MessageItem _itemPrefab = null;
        [SerializeField]
        private ScrollRect _scrollRect = null;
		[SerializeField]
		private Scrollbar _scrollbar = null;

		private string _annotationId;
        private string _tourId;
        private System.Action<ControllerMessage> _needRefresh = null;
        private bool _needToScrollAtBottom = false;

        public void InitMessages(string annotationId, string tourId, List<ControllerMessage> messages, System.Action<ControllerMessage> needRefresh = null)
		{
            _annotationId = annotationId;
            _tourId = tourId;
            _needRefresh = needRefresh;
            foreach (ControllerMessage message in messages)
			{
                MessageItem item = GameObject.Instantiate<MessageItem>(_itemPrefab, _parent);
                item.InitController(message, OnItemAction);
            }
            _answerButtonRoot.SetAsLastSibling();
            _needToScrollAtBottom = true;
        }

        protected override void Awake()
        {
            base.Awake();
            _backButton.onClick.AddListener(OnBackClicked);
            _answerButton.onClick.AddListener(OnAnswerClicked);
            _okButton.onClick.AddListener(OnOkClicked);
        }

        protected override void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackClicked);
            _answerButton.onClick.RemoveListener(OnAnswerClicked);
            _okButton.onClick.RemoveListener(OnOkClicked);
            base.OnDestroy();
        }

		public void Update()
		{
            if (_needToScrollAtBottom)
			{
                _scrollRect.verticalNormalizedPosition = 0f;
                _scrollbar.SetValueWithoutNotify(0f);
                _scrollRect.CalculateLayoutInputVertical();
                _needToScrollAtBottom = false;
            }
        }

		private void OnBackClicked()
        {
            ClosePopup();
        }

        private void OnOkClicked()
        {
            ClosePopup();
        }

        private void OnAnswerClicked()
		{
            MainUI.instance.ShowEditMessagesPopup(_annotationId, _tourId, null, OnNewMessage);
        }

        private void OnItemAction(MessageItem item, MessageItem.ItemAction action)
		{
            if (action == MessageItem.ItemAction.DELETED)
			{
                GameObject.Destroy(item.gameObject);
            }
            _needRefresh?.Invoke(null);
        }

        private void OnNewMessage(ControllerMessage message)
		{
            MessageItem item = GameObject.Instantiate<MessageItem>(_itemPrefab, _parent);
            item.InitController(message, OnItemAction);
            _answerButtonRoot.SetAsLastSibling();
            _needToScrollAtBottom = true;
            _needRefresh?.Invoke(message);
        }
    }
}
