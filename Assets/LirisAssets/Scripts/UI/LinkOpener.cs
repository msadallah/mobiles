using Liris.App;
using Liris.App.WebServices;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(TMP_Text))]
public class LinkOpener : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        Camera cam = null;
        Canvas canvas = gameObject.GetComponentInParent<Canvas>();
        if (canvas != null)
            cam = canvas.worldCamera;

        TMP_Text pTextMeshPro = GetComponent<TMP_Text>();
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(pTextMeshPro, eventData.position, cam);  // If you are not in a Canvas using Screen Overlay, put your camera instead of null
        if (linkIndex != -1)
        { // was a link clicked?
            TMP_LinkInfo linkInfo = pTextMeshPro.textInfo.linkInfo[linkIndex];
            string url = null;
            switch (linkInfo.GetLinkID())
            {
                case "ResetPassword":
                    url = WebServiceBase.URL_BASE + "/reset-password";
                    break;
                case "DataRequest":
                    url = "contact@mail.edu";
                    break;
            }
            if (url != null)
                Application.OpenURL(url);
        }
    }

    public void OpenMail()
    {
        //"mailto:" + emailaddress + "?subject=" + subject + "&body=" + body
        Application.OpenURL("mailto:contact@mail.edu?subject=[User" + ApplicationManager.instance.user.id + "]");
    }

}