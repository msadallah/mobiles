namespace Liris.App.UI
{
    using Liris.App;
    using UnityEngine;
    using UnityEngine.UI;

    public class UserPosition : MsgBase
    {
        [SerializeField]
        private Image _image = null;
        [SerializeField]
        private Sprite _gyroSprite = null;

        private float _targetLon = 0f;
        private float _targetLat = 0f;
        private bool _useGyro = false;
        private Quaternion _rot = new Quaternion(0f, 0f, 1f, 0f);

        protected void Start()
        {
            if (SystemInfo.supportsGyroscope)
            {
                _image.sprite = _gyroSprite;
                _useGyro = true;
                Input.gyro.enabled = true;
            }
            else
            {
                Debug.Log("NO GYRO!");
            }
        }


        public void SetTargetCoords(float lon, float lat, bool init = false)
        {
            _targetLon = lon;
            _targetLat = lat;
            if (init)
            {
                _longitude = lon;
                _latitude = lat;
            }
        }

        private void LateUpdate()
        {
            float deltaTime = Time.deltaTime;
            _longitude = Mathf.Lerp(_longitude, _targetLon, deltaTime * ApplicationManager.LERP_GPS_SPEED);
            _latitude = Mathf.Lerp(_latitude, _targetLat, deltaTime * ApplicationManager.LERP_GPS_SPEED);
            Render();
            if (_useGyro)
            {
                //Debug.Log("GYRO " + Input.gyro.attitude.eulerAngles);
                if (_rt != null)
                    _rt.localRotation = Quaternion.Euler(0f, 0f, Input.gyro.attitude.eulerAngles.z);
            }
        }
    }
}
