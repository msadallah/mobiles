namespace Liris.App.UI
{
    using UnityEngine;
    using Liris.App.Assets;
    using TMPro;
	using UnityEngine.UI;
	using System.Collections.Generic;
	using AllMyScripts.LangManager;

	public class IconsCategory : MonoBehaviour
    {
        public enum Category
		{
            Activity,
            Env,
            Senses,
            Memory,
            Social,
            Mission
		}

        public string currentSelection => _currentSelection;
        public bool canAddIcon => _canAddIcon;

        [SerializeField]
        private Category _category;
        [SerializeField]
        private bool _allowMultipleSelection = false;
        [SerializeField]
        private TextMeshProUGUI _title = null;
        [SerializeField]
        private Button _iconWithSelectionButtonPrefab = null;
        [SerializeField]
        private SpritesList _iconSprites = null;
        [SerializeField]
        private Transform _iconsParent = null;
        [SerializeField]
        private int _startIconIndex = 0;
        [SerializeField]
        private int _endIconIndex = 0;
        [SerializeField]
        private TextMeshProUGUI _nameText = null;

        private Button[] _iconsButton = null;
        private GameObject[] _iconsSelection = null;
        private Dictionary<string, int> _dataDic = null;
        private string _currentSelection = null;
        private event System.Action _onSelectionChanged = null;
        private bool _canAddIcon = true;

        public void SetTitle(string title)
		{
            _title.text = title;
		}

        public void SetSelectedIcon(string name)
        {
            if (string.IsNullOrEmpty(name))
                name = _iconSprites.GetFirstName();
            UpdateSelection(name.Split(','));
        }

        public void SetCanAddIcon(bool canAdd)
		{
            _canAddIcon = canAdd;
        }

        public void RegisterSelectionEvent(System.Action evt)
        {
            _onSelectionChanged += evt;
        }

        public void UnregisterSelectionEvent(System.Action evt)
        {
            _onSelectionChanged -= evt;
        }

        private void Awake()
        {
            int count = _iconSprites.data.Length;
            int iconCount = _endIconIndex - _startIconIndex + 2;
            _iconsButton = new Button[iconCount];
            _iconsSelection = new GameObject[iconCount];
            _dataDic = new Dictionary<string, int>();
            int num = 0;
            for (int i = 0; i < count; ++i)
            {
                if (i == 0 || (i >= _startIconIndex && i <= _endIconIndex))
				{
                    Button btn = GameObject.Instantiate<Button>(_iconWithSelectionButtonPrefab, _iconsParent);
                    Image image = btn.transform.Find("Icon").GetComponent<Image>();
                    SpritesList.SpriteData data = _iconSprites.data[i];
                    image.sprite = data.sprite;
                    image.color = data.color;
                    string name = data.name;
                    btn.onClick.AddListener(() => OnIconClicked(name));
                    _iconsButton[num] = btn;
                    _iconsSelection[num] = btn.transform.Find("Selection").gameObject;
                    _dataDic[name] = num;
                    num++;
                }
            }
        }

		private void Start()
		{
            SetTitle(L.Get("id_icon_category_" + _category.ToString().ToLower()));
        }

		private void OnDestroy()
        {
            for (int i = 0; i < _iconsButton.Length; ++i)
            {
                _iconsButton[i].onClick.RemoveAllListeners();
                _iconsSelection[i] = null;
            }
        }

        private void UpdateSelection(string[] names)
		{
            UpdateSelection(string.Empty, false);
            _nameText.text = "";
            foreach (string name in names)
                UpdateSelection(name, true);
            if (string.IsNullOrEmpty(_nameText.text))
                UpdateSelection(_iconSprites.GetFirstName(), false);
        }

        private void UpdateSelection(string name, bool allowMultipleSelection)
        {
            _currentSelection = "";
            string currentText = "";
            if (allowMultipleSelection)
			{
                if (_dataDic.ContainsKey(name))
				{
                    int num = _dataDic[name];
                    if (num != 0)
					{
                        bool activated = _iconsSelection[num].activeSelf;
                        bool toActivate = !activated;
                        if (toActivate && _canAddIcon || !toActivate)
						{
                            _iconsSelection[0].SetActive(false);
                            _iconsSelection[num].SetActive(toActivate);
                        }
                    }
                }                
                foreach (var keyval in _dataDic)
                {
                    int num = keyval.Value;
                    SpritesList.SpriteData data = _iconSprites.GetData(keyval.Key);
                    bool active = _iconsSelection[num].activeSelf;
                    if (active && data.name != _iconSprites.GetFirstName())
					{
                        if (string.IsNullOrEmpty(_currentSelection))
                            _currentSelection = data.name;
                        else
                            _currentSelection += "," + data.name;
                        if (string.IsNullOrEmpty(currentText))
                            currentText = L.Get(_iconSprites.GetTextIdFromName(data.name));
                        else
                            currentText += ", " + L.Get(_iconSprites.GetTextIdFromName(data.name));
                    }
                }
            }
            else
			{
                foreach (var keyval in _dataDic)
                {
                    int num = keyval.Value;
                    SpritesList.SpriteData data = _iconSprites.GetData(keyval.Key);
                    if (data.name == name)
                    {
                        _iconsSelection[num].SetActive(true);
                        if (name != _iconSprites.GetFirstName())
                            _currentSelection = data.name;
                        currentText = L.Get(_iconSprites.GetTextIdFromName(data.name));
                    }
                    else
                    {
                        _iconsSelection[num].SetActive(false);
                    }
                }
            }
            _nameText.text = currentText;
        }

        private void OnIconClicked(string name)
        {
            
            if (name == _iconSprites.GetFirstName())
                UpdateSelection(name, false);
            else
                UpdateSelection(name, _allowMultipleSelection);

            if (string.IsNullOrEmpty(_currentSelection))
                UpdateSelection(_iconSprites.GetFirstName(), false);
            _onSelectionChanged?.Invoke();
        }
    }
}
