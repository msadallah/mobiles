namespace Liris.App.UI
{
    using UnityEngine;

    public class LoadingRotation : MonoBehaviour
    {
        [SerializeField]
        private float _loadingSpeed = 10f;

        private float _cumulatedTime = 0f;

        private void Update()
        {
            _cumulatedTime += Time.deltaTime;
            float duration = 1f / _loadingSpeed;
            if (_cumulatedTime > duration)
            {
                transform.Rotate(Vector3.forward, -30f);
                _cumulatedTime -= duration;
            }
        }

    }
}
