namespace Liris.App.UI
{
    using UnityEngine;
    using TMPro;

    public class InputFieldTitle : MonoBehaviour
    {
        public TMP_InputField input => _inputField;
        public VisibilitySwitch visibility => _visibility;

        [SerializeField]
        private TMP_InputField _inputField = null;
        [SerializeField]
        private TextMeshProUGUI _error = null;
        [SerializeField]
        private GameObject _checked = null;
        [SerializeField]
        private VisibilitySwitch _visibility = null;

        public void Clear()
        {
            _checked.SetActive(false);
            _error.gameObject.SetActive(false);
        }

        public void SetError(string error)
        {
            _error.gameObject.SetActive(true);
            _error.text = error;
        }

        public void SetChecked()
        {
            _checked.SetActive(true);
        }

    }
}
