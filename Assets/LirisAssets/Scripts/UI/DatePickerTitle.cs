using Liris.App;
using Liris.App.Controllers;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DatePickerTitle : MonoBehaviour
{
	[System.Serializable]
	public class OnDateSelectedEvent : UnityEvent<string> { }

	public OnDateSelectedEvent onDateSelected = null;

	public TextMeshProUGUI dateField => _dateField;
	public string date => _date;

    [SerializeField]
    private Button _dateButton = null;
	[SerializeField]
	private Button _clearButton = null;
	[SerializeField]
    private TextMeshProUGUI _dateField = null;
	[SerializeField]
	private TextMeshProUGUI _placeHolder = null;
	[SerializeField]
    private TextMeshProUGUI _error = null;
    [SerializeField]
    private GameObject _checked = null;

	private string _date = null;
	private bool _useTime = false;

	public static bool GetDateTimeOfDate(string date, out System.DateTime dt)
	{
		var cultureInfo = new CultureInfo("fr-FR");
		if (System.DateTime.TryParse(date, cultureInfo, DateTimeStyles.AssumeLocal, out dt))
		{
			return true;
		}
		return false;
	}

	public void Clear()
	{
		_checked.SetActive(false);
		_error.gameObject.SetActive(false);
	}

	public void SetError(string error)
	{
		_error.gameObject.SetActive(true);
		_error.text = error;
	}

	public void SetChecked()
	{
		_checked.SetActive(true);
	}

	public void SetDate(string date, bool useTime = false)
	{
		_useTime = useTime;
		SelectDate(date, false);
		_dateField.text = date;
	}

	public void OnDateSelected(string date)
	{
		SelectDate(date, true);
	}

	private void SelectDate(string date, bool triggerEvent)
	{
		bool isDateValid = !string.IsNullOrEmpty(date);
		if (isDateValid)
			Debug.Log("SelectDate " + date);
		else
			Debug.Log("SelectDate no date!");
		_placeHolder.gameObject.SetActive(!isDateValid);
		_date = date;
		if (triggerEvent)
			onDateSelected?.Invoke(date);
	}

	private void Awake()
	{
		_dateButton.onClick.AddListener(OnClickDate);
		_clearButton.onClick.AddListener(OnClickClear);
	}

	private void OnDestroy()
	{
		_dateButton.onClick.RemoveListener(OnClickDate);
		_clearButton.onClick.RemoveListener(OnClickClear);
	}

	private void OnClickDate()
	{
		System.DateTime dateTime = System.DateTime.Now;
		if (!string.IsNullOrEmpty(_date))
			dateTime = ControllerBase.ConvertInDateTime(_date, dateTime);
		MainUI.instance.ShowCalendarPopup(this, dateTime, _useTime);
		_placeHolder.gameObject.SetActive(false);
	}

	private void OnClickClear()
	{
		_dateField.text = "";
		SelectDate(null, true);
	}
}
