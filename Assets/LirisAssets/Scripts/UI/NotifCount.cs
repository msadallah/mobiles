namespace Liris.App.UI
{
    using UnityEngine;
    using TMPro;

    public class NotifCount : MonoBehaviour
    {
        [SerializeField]
        private GameObject _bkg = null;
        [SerializeField]
        private TextMeshProUGUI _count = null;

        public void SetCount(int count)
		{
            _bkg.SetActive(count > 0);
            _count.text = count.ToString();
        }
    }
}