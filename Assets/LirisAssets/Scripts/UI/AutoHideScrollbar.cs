using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Scrollbar))]
public class AutoHideScrollbar : MonoBehaviour
{
    private Scrollbar scrollbar;
    private GameObject target;
    private Image image;

    private void Start()
    {
        scrollbar = GetComponent<Scrollbar>();
        image = GetComponent<Image>();
        target = scrollbar.targetGraphic.gameObject;
    }

	private void Update()
	{
        bool show = scrollbar.size < 1f;
        target.SetActive(show);
        image.enabled = show;
    }

}