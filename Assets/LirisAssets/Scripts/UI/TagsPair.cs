namespace Liris.App.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;

    public class TagsPair : MonoBehaviour
    {
        [SerializeField]
        private Button _buttonLeft = null;
        [SerializeField]
        private Button _buttonRight = null;
        [SerializeField]
        private GameObject _selectionLeft = null;
        [SerializeField]
        private GameObject _selectionRight = null;
        [SerializeField]
        private string _tagLeft = "#";
        [SerializeField]
        private string _tagRight = "#";
        [SerializeField]
        private TextMeshProUGUI _textLeft = null;
        [SerializeField]
        private TextMeshProUGUI _textRight = null;
        [SerializeField]
        private bool _selectedLeft = false;
        [SerializeField]
        private bool _selectedRight = false;

        public void SetTagLeft(string name, string text)
		{
            _tagLeft = name;
            _textLeft.text = text;
        }

        public void SetTagRight(string name, string text)
        {
            _tagRight = name;
            _textRight.text = text;
        }

        private void OnLeftClicked()
        {
            if (!_selectedLeft)
            {
                _selectedLeft = true;
                _selectedRight = false;
            }
            else
            {
                _selectedLeft = false;
                _selectedRight = false;
            }
            UpdateSelection();
        }

        private void OnRightClicked()
        {
            if (!_selectedRight)
            {
                _selectedLeft = false;
                _selectedRight = true;
            }
            else
            {
                _selectedLeft = false;
                _selectedRight = false;
            }
            UpdateSelection();
        }

        private void UpdateSelection()
        {
            _selectionLeft.SetActive(_selectedLeft);
            _selectionRight.SetActive(_selectedRight);
        }

        public string GetTag()
        {
            if (_selectedLeft)
                return _tagLeft;
            else if (_selectedRight)
                return _tagRight;
            else
                return null;
        }

        public void SetTag(string tag)
        {
            if (tag == _tagLeft || tag == _tagRight)
            {
                _selectedLeft = tag == _tagLeft;
                _selectedRight = tag == _tagRight;
                UpdateSelection();
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            _buttonLeft.onClick.AddListener(OnLeftClicked);
            _buttonRight.onClick.AddListener(OnRightClicked);
            UpdateSelection();
        }

        // Update is called once per frame
        void OnDestroy()
        {
            _buttonLeft.onClick.RemoveListener(OnLeftClicked);
            _buttonRight.onClick.RemoveListener(OnRightClicked);
        }
    }
}
