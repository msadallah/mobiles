namespace Liris.App.UI
{
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;
    using TMPro;

    public class UI_IconWithTextButton : MonoBehaviour
    {
        public UnityEvent OnClickEvent = null;

        [SerializeField]
        private Button _button = null;
        [SerializeField]
        private Image _icon = null;
        [SerializeField]
        private TextMeshProUGUI _label = null;
        [SerializeField]
        private GameObject _selection = null;
        [SerializeField]
        private Image _selectionHighlight = null;
        [SerializeField]
        private Image _selectionHighlightIcon = null;

        public void ShowSelection(bool selected)
        {
            _selection.SetActive(selected);
        }

        public void SetLabel(string text)
        {
            _label.text = text;
        }

        public void SetSprite(Sprite sprite)
        {
            _icon.sprite = sprite;
        }

        public void SetColor(Color color)
        {
            _icon.color = color;
            _label.color = color;
            _selectionHighlight.color = color;
            _selectionHighlightIcon.color = color;
        }

        private void Awake()
		{
            _button.onClick.AddListener(OnButtonClicked);
        }

		private void OnDestroy()
		{
            _button.onClick.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
		{
            OnClickEvent?.Invoke();
        }
    }
}
