namespace Liris.App.UI
{
	using Liris.App.Assets;
	using UnityEngine;
    using TMPro;
	using System.Collections.Generic;
	using UnityEngine.UI;

	public class UI_Emojis : MonoBehaviour
    {
        public class EmojiData
		{
            public GameObject go;
            public int count;
		}

        [SerializeField]
        private GameObject _emojiIconPrefab = null;
        [SerializeField]
        private Transform _emojiParent = null;
        [SerializeField]
        private SpritesList _emojiSprites = null;
        [SerializeField]
        private GameObject _emojiCountRoot = null;
        [SerializeField]
        private TextMeshProUGUI _emojiCount = null;

        private int _iconCount = 0;
        private Dictionary<int, EmojiData> _visibleIcons = null;

        public void AddIcon(int numIcon, int count = 1)
		{
            if (_visibleIcons == null)
                _visibleIcons = new Dictionary<int, EmojiData>();
            if (!_visibleIcons.ContainsKey(numIcon))
			{
                SpritesList.SpriteData data = _emojiSprites.GetDataFromIndex(numIcon);
                if (data != null)
                {
                    GameObject go = GameObject.Instantiate(_emojiIconPrefab, _emojiParent);
                    Image image = go.GetComponent<Image>();
                    image.sprite = data.sprite;

                    EmojiData emojiData = new EmojiData();
                    emojiData.go = go;
                    emojiData.count = count;

                    _visibleIcons.Add(numIcon, emojiData);
                    
                    _emojiCountRoot.transform.SetAsLastSibling();
                }
            }
            else
			{
                _visibleIcons[numIcon].count += count;
            }

            _iconCount += count;
            UpdateDisplayIconCount();
        }

        public void RemoveIcon(int numIcon)
        {
            if (_visibleIcons != null && _visibleIcons.ContainsKey(numIcon))
            {
                _visibleIcons[numIcon].count--;
                if (_visibleIcons[numIcon].count == 0)
				{
                    GameObject go = _visibleIcons[numIcon].go;
                    GameObject.Destroy(go);
                    _visibleIcons.Remove(numIcon);
                }
            }
            _iconCount--;
            UpdateDisplayIconCount();
        }

        public int GetIndexFromName(string name)
		{
            return _emojiSprites.GetIndexFromName(name);
        }

        public string GetNameFromIndex(int num)
        {
            return _emojiSprites.GetNameFromIndex(num);
        }

        private void Awake()
		{
            _emojiCountRoot.SetActive(false);
        }

        private int GetVisibleIconCount()
		{
            if (_visibleIcons == null)
                return 0;
            return _visibleIcons.Count;
        }

        private void UpdateDisplayIconCount()
		{
            _emojiCount.text = _iconCount.ToString();
            _emojiCountRoot.SetActive(_iconCount > GetVisibleIconCount());
        }
    }
}
