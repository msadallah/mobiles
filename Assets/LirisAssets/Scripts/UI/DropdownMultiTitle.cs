namespace Liris.App.UI
{
    using UnityEngine;
    using TMPro;

    public class DropdownMultiTitle : MonoBehaviour
    {
        public string placeHolderText => _dropdown.placeholder.GetComponent<TextMeshProUGUI>().text;
        public UI_DropdownMulti dropdown => _dropdown;

        [SerializeField]
        private TextMeshProUGUI _title = null;
        [SerializeField]
        private UI_DropdownMulti _dropdown = null;
        [SerializeField]
        private TextMeshProUGUI _error = null;
        [SerializeField]
        private GameObject _checked = null;

        public void SetTitle(string text)
        {
            _title.text = text;
        }

        public void Clear()
        {
            _checked.SetActive(false);
            _error.gameObject.SetActive(false);
        }

        public void SetError(string error)
        {
            _error.gameObject.SetActive(true);
            _error.text = error;
        }

        public void SetChecked()
        {
            _checked.SetActive(true);
        }
    }
}
