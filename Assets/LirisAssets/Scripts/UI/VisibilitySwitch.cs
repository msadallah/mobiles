namespace Liris.App.UI
{
	using TMPro;
	using UnityEngine;
    using UnityEngine.UI;

    public class VisibilitySwitch : MonoBehaviour
    {
        public Button button => _visibilityButton;

        [Header("Password visibility")]
        [SerializeField]
        private TMP_InputField _passwordInput = null;
        [SerializeField]
        private Button _visibilityButton = null;
        [SerializeField]
        private Image _visibilityIcon = null;
        [SerializeField]
        private Sprite _visibilityOnSprite = null;
        [SerializeField]
        private Sprite _visibilityOffSprite = null;

        private bool _isPasswordVisible = false;

        private void Awake()
        {
            _visibilityButton.onClick.AddListener(OnVisibilityClicked);
        }

        private void OnDestroy()
        {
            _visibilityButton.onClick.RemoveListener(OnVisibilityClicked);
        }

        private void OnVisibilityClicked()
        {
            _isPasswordVisible = !_isPasswordVisible;
            _visibilityIcon.sprite = _isPasswordVisible ? _visibilityOnSprite : _visibilityOffSprite;
            _passwordInput.inputType = _isPasswordVisible ? TMP_InputField.InputType.Standard : TMP_InputField.InputType.Password;
            _passwordInput.ForceLabelUpdate();
        }
    }
}
