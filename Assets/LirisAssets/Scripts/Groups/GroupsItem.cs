namespace Liris.App.Groups
{
	using Liris.App.Controllers;
	using UnityEngine;
	using UnityEngine.UI;
	using TMPro;

	public class GroupsItem : MonoBehaviour
	{
		public ControllerGroup group => _group;

		[SerializeField]
		private TextMeshProUGUI _label = null;
		[SerializeField]
		private Toggle _toggle = null;

		private System.Action<GroupsItem, bool> _onClick = null;
		private ControllerGroup _group = null;

		private void Awake()
		{
			_toggle.onValueChanged.AddListener(OnButtonClicked);
		}

		private void OnDestroy()
		{
			_toggle.onValueChanged.RemoveListener(OnButtonClicked);
			_onClick = null;
		}

		public void InitController(ControllerGroup group, bool toggle, System.Action<GroupsItem, bool> onClick = null)
		{
			_onClick = onClick;
			_group = group;
			_label.text = group.description;
			SetIsOn(toggle);
		}

		public void SetIsOn(bool isOn)
		{
			_toggle.SetIsOnWithoutNotify(isOn);
		}

		private void OnButtonClicked(bool toggle)
		{
			_onClick?.Invoke(this, toggle);
		}
	}
}
