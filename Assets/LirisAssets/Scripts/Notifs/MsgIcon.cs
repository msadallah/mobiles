namespace Liris.App
{
    using UnityEngine;
    using UnityEngine.UI;

    public class MsgIcon : MsgBase
    {
        [SerializeField]
        private Image _icon = null;

        public void SetIconSprite(Sprite sprite)
        {
            _icon.sprite = sprite;
        }

        public void SetIconColor(Color color)
        {
            _icon.color = color;
        }
    }
}
