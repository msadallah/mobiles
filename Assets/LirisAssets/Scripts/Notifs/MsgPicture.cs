namespace Liris.App
{
    using UnityEngine;
    using UnityEngine.UI;

    public class MsgPicture : MsgBase
    {
        [SerializeField]
        private RawImage _picture;
        [SerializeField]
        private Button _button;

        public void SetTexture(Texture2D tex)
        {
            _picture.texture = tex;
        }

        protected override void Awake()
        {
            base.Awake();
            _button.onClick.AddListener(OnClick);
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
        }
    }
}
