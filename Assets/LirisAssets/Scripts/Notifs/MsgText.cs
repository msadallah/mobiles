namespace Liris.App
{
    using UnityEngine;
    using TMPro;

    public class MsgText : MsgBase
    {
        [SerializeField]
        private TextMeshProUGUI _text = null;

        public void SetText(string text)
        {
            _text.text = text;
        }

        public override void SetVisible(bool show)
        {
            base.SetVisible(show);
            gameObject.SetActive(show && _isInViewport);
        }
    }
}
