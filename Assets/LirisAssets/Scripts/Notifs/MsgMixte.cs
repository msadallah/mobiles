namespace Liris.App
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
	using Liris.App.Assets;
	using System.Collections;
	using Liris.App.WebServices;
	using System.Collections.Generic;
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
    using Liris.App.Trace;
	using Liris.App.Media;

	public class MsgMixte : MsgBase
    {
        public bool visible => gameObject.activeInHierarchy;

        public ControllerAnnotation annotation => _annotation;

        [SerializeField]
        private RectTransform _panel = null;
        [SerializeField]
        private GameObject _pictureRoot = null;
        [SerializeField]
        private RawImage _picture = null;
        [SerializeField]
        private GameObject _loading = null;
        [SerializeField]
        private Button _button = null;
        [SerializeField]
        private Image[] _bkgToColor = null;
        [SerializeField]
        private SpritesList _placeTypeSprites = null;
        [SerializeField]
        private GameObject _placeIconRoot = null;
        [SerializeField]
        private GameObject _iconTwoOrPlus = null;
        [SerializeField]
        private GameObject _iconThreeOrPlus = null;
        [SerializeField]
        private Image _placeIconImage = null;
        [SerializeField]
        private SpritesList _placeIconSprites = null;
        [SerializeField]
        private GameObject _emoticonRoot = null;
        [SerializeField]
        private Image _emoticonImage = null;
        [SerializeField]
        private SpritesList _emoticonSprites = null;
        [SerializeField]
        private GameObject _tagsRoot = null;
        [SerializeField]
        private GameObject _commentRoot = null;
        [SerializeField]
        private TextMeshProUGUI _commentText = null;
        [Header("Move")]
        [SerializeField]
        private GameObject _moveRoot = null;
        [SerializeField]
        private Image _moveImage = null;
        [SerializeField]
        private RectTransform _moveInputZone = null;

        private ControllerAnnotation _annotation = null;

        public void SetAnnotation(ControllerAnnotation annotation)
        {
            _annotation = annotation;
            SetId(annotation.id);
            if (annotation.tex != null)
                SetTexture(annotation.tex);
            else
                SetImage(annotation.imageId);
            SetPlaceType(annotation.placeType);
            SetPlaceIcon(annotation.icons);
            SetEmoticon(annotation.emoticon);
            SetTags(annotation.tags);
            SetComment(annotation.comment);
        }

        public void SetId(string id)
        {
            _id = id;
        }

        public void SetTexture(Texture2D tex)
        {
            _picture.texture = tex;
            bool show = tex != null;
            if (show)
            {
                AspectRatioFitter aspect = _picture.GetComponent<AspectRatioFitter>();
                if (aspect != null)
                {
                    float ratio = (float)tex.width / (float)tex.height;
                    aspect.aspectRatio = ratio;
                }
            }
            _pictureRoot.SetActive(show);
            _panel.SetHeight(show ? 96f : 48f);
        }

        public void SetImage(string imageId)
        {
            bool show = !string.IsNullOrEmpty(imageId);
            _pictureRoot.SetActive(show);
            _panel.SetHeight(show ? 96f : 48f);
            if (show)
            {
                ShowImage(imageId);
            }
        }

        public void SetPlaceType(string name)
        {
            Color color = _placeTypeSprites.GetData(name).color;
            foreach (Image img in _bkgToColor)
                img.color = color;
        }

        public void SetPlaceIcon(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                if (name.Contains(','))
				{
                    string[] split = name.Split(',');
                    name = split[0];
                    _iconTwoOrPlus.SetActive(split.Length > 1);
                    _iconThreeOrPlus.SetActive(split.Length > 2);
                }
                else
                {
                    _iconTwoOrPlus.SetActive(false);
                    _iconThreeOrPlus.SetActive(false);
                }

                SpritesList.SpriteData data = _placeIconSprites.GetData(name);
                if (data != null)
                {
                    _placeIconRoot.SetActive(name != _placeIconSprites.GetFirstName());
                    _placeIconImage.sprite = data.sprite;
                    _placeIconImage.color = data.color;
                }
                else
                {
                    _placeIconRoot.SetActive(false);
                }
            }
            else
            {
                _placeIconRoot.SetActive(false);
            }
        }

        public void SetEmoticon(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                _emoticonRoot.SetActive(name != _emoticonSprites.GetFirstName());
                _emoticonImage.sprite = _emoticonSprites.GetData(name).sprite;
            }
            else
            {
                _emoticonRoot.SetActive(false);
            }
        }

        public void SetComment(string comment)
        {
            bool valid = !string.IsNullOrEmpty(comment);
            _commentRoot.SetActive(valid);
            if (valid)
                _commentText.text = comment;
        }

        public void SetTags(string tags)
        {
            bool valid = !string.IsNullOrEmpty(tags);
            _tagsRoot.SetActive(valid);
        }

        public void ActivateMoveEdition(bool canMove)
        {
            _moveRoot.SetActive(canMove);
            _button.interactable = !canMove;
        }

        public bool IsMouseOver(Vector3 mousePos, Camera cam)
        {
            return RectTransformUtility.RectangleContainsScreenPoint(_moveInputZone, mousePos, cam);
        }

        public void SetMoveColor(Color color)
		{
            _moveImage.color = color;
		}

        protected override void Awake()
        {
            base.Awake();
            _button.onClick.AddListener(OnClick);
            _loading.SetActive(false);
            _moveRoot.SetActive(false);
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveListener(OnClick);
            _annotation = null;
        }

        public override void SetVisible(bool show)
        {
            base.SetVisible(show);
            bool visible = show && !_isFiltered && _isInViewport;
            if (visible && ApplicationManager.instance.mode == ApplicationManager.Mode.PREVIEW_TOUR)
                visible = _showInPreview;
            gameObject.SetActive(visible);
        }

        private void OnClick()
        {
            if (ApplicationManager.instance.isDragging)
                return;

            MainUI.instance.ShowMsgMixtePopup(_annotation, ApplicationManager.instance.mode == ApplicationManager.Mode.NONE, ApplicationManager.instance.isInPreviewTour);
            // kTBS
            Dictionary<string, object> trace = new Dictionary<string, object>()
            {
                {"@type", "m:openPost"},
                {"m:userId", ApplicationManager.instance.user.id},
                {"m:postId", _annotation.id}
            };
            TraceManager.DispatchEventTraceToManager(trace);
            ///
        }

        private void ShowImage(string imageId)
        {
            _loading.SetActive(true);

            Texture2D tex = null;

            if (SpriteLibrary.HasSprite(imageId))
			{
                tex = SpriteLibrary.GetSprite(imageId);
                ApplyTexture(tex);
                // TO EXPORT ALL IMAGES FROM CACHE
                /*
#if UNITY_EDITOR
                WebServiceMediaObjectsGetFromId ws = new WebServiceMediaObjectsGetFromId(ApplicationManager.instance.dataManager.token, imageId);
                yield return ws.Run();
                if (!ws.hasFailed)
				{
                    Dictionary<string, object> dic = ws.GetResultAsDic();
                    string name = DicTools.GetValueString(dic, "contentUrl");
                    if (!string.IsNullOrEmpty(name))
					{
                        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/images/" + name, tex.EncodeToPNG());
                    }
                }
#endif
                */
                // TO EXPORT ALL IMAGES FROM CACHE
            }
            else
			{
                ApplicationManager.instance.mediaManager.RequestMedia(imageId, OnRequestMediaResult);
            }
        }

        private void OnRequestMediaResult(MediaManager.MediaRequest request)
        {
            ApplyTexture(request.tex);
        }

        private void ApplyTexture(Texture2D tex)
        {
            if (tex != null)
            {
                _annotation.tex = tex;
                SetTexture(tex);
            }

            _loading.SetActive(false);
        }
    }
}
