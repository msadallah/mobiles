namespace Liris.App
{
    using UnityEngine;
    using UnityEngine.UI;

    public class MsgMove : MsgBase
    {
        public int lineIndex => _lineIdx;
        public string linkedAnnotation => _linkedAnnotation;

        [SerializeField]
        private GameObject _moveVisual = null;
        [SerializeField]
        private Image _moveImage = null;
        [SerializeField]
        private RectTransform _moveInputZone = null;

        private PathLine _line = null;
        private int _lineIdx = 0;
        private float _startingLon = 0f;
        private float _startingLat = 0f;
        private string _linkedAnnotation = null;

        public override void Init(float lon, float lat, OnlineMapsTileSetControl control)
        {
            base.Init(lon, lat, control);
            _startingLon = lon;
            _startingLat = lat;
        }

        public void SetPathLine(PathLine line, int index)
		{
            _line = line;
            _lineIdx = index;
        }

        public void SetLinkedAnnotation(string annotationId)
		{
            _linkedAnnotation = annotationId;
		}

        public override void SetCoords(float lon, float lat)
        {
            base.SetCoords(lon, lat);
            if (_line != null)
			{
                _line.points[_lineIdx] = new Vector2(lon, lat);
                _line.NeedToRecomputeDistance();
            }   
        }

        public bool IsMouseOver(Vector3 mousePos, Camera cam)
        {
            return RectTransformUtility.RectangleContainsScreenPoint(_moveInputZone, mousePos, cam);
        }

        public void ShowMoveVisual(bool show)
		{
            _moveVisual.SetActive(show);
        }

        public void Revert()
		{
            SetCoords(_startingLon, _startingLat);
        }

        public void SetMoveColor(Color color)
        {
            _moveImage.color = color;
        }
    }
}
