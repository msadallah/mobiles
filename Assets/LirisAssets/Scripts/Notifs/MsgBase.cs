namespace Liris.App
{
    using UnityEngine;

    public class MsgBase : MonoBehaviour
    {
        public enum MsgType
        {
            Mixte,
            Picture,
            Icon,
            Text
        }

        public string id => _id;
        public float longitude => _longitude;
        public float latitude => _latitude;
        public bool isGrouped => _isGrouped;
        public bool isFiltered => _isFiltered;
        public bool showInPreview => _showInPreview;

        public Vector2 uiPos => _rt != null ? _rt.anchoredPosition : Vector2.zero;

        protected string _id = null;
        protected float _longitude = 0f;
        protected float _latitude = 0f;
        protected OnlineMapsTileSetControl _control = null;
        protected RectTransform _rt = null;
        protected bool _isGrouped = false;
        protected Vector2 _uiOffset = Vector2.zero;
        protected bool _isFiltered = false;
        protected bool _isVisible = false;
        protected bool _isInViewport = false;
        protected bool _showInPreview = false;

        private float _halfRangeX = 650f;
        private float _halfRangeY = 650f;

        protected virtual void Awake()
        {
            _rt = GetComponent<RectTransform>();
            _halfRangeX = _halfRangeY * (float)Screen.width / (float)Screen.height;
        }

        public virtual void Init(float lon, float lat, OnlineMapsTileSetControl control)
        {
            SetCoords(lon, lat);
            SetControl(control);
        }

        public virtual void SetVisible(bool show)
        {
            _isVisible = show;
        }

        public virtual void SetFiltered(bool filtered)
        {
            _isFiltered = filtered;
        }

        public virtual void ShowInPreview(bool show)
        {
            _showInPreview = show;
        }

        public void SetOffset(Vector2 offset)
        {
            _uiOffset = offset;
        }

        public void AddOffset(Vector2 offset)
        {
            _uiOffset += offset;
        }

        public virtual void SetCoords(float lon, float lat)
        {
            _longitude = lon;
            _latitude = lat;
        }

        public bool IsNear(MsgBase msg)
		{
            return Mathf.Approximately(msg.longitude, longitude) && Mathf.Approximately(msg.latitude, latitude);
        }

        public void SetControl(OnlineMapsTileSetControl control)
        {
            _control = control;
        }

        public void Render(bool force = false, bool withOffset = true)
        {
            bool render = force || !_isGrouped;
            if (_rt != null && _control != null && render)
            {
                Vector2 pos = _control.GetScreenPosition(_longitude, _latitude) - Vector2.one * 512f;
                if (withOffset)
                    pos += _uiOffset;                
                _rt.anchoredPosition = pos;
                _isInViewport = pos.x > -_halfRangeX && pos.x < _halfRangeX && pos.y > -_halfRangeY && pos.y < _halfRangeY;
                SetVisible(_isVisible);
            }
        }

        public void Close()
        {
            GameObject.Destroy(gameObject);
        }

        public void SetGrouped(bool grouped)
        {
            _isGrouped = grouped;
        }

        private void OnDestroy()
        {
            _control = null;
            _rt = null;
        }
    }
}
