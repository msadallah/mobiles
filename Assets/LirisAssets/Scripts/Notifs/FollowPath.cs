namespace Liris.App
{
	using Liris.App.Controllers;
	using UnityEngine;

	public class FollowPath : MonoBehaviour
	{
		private PathLine _line = null;
		private Vector2 _initialPos = Vector2.zero;
		private int _firstLineIdx = -1;
		private float _segmentRatio = 0f;
		private MsgMixte _msg = null;

		public void SetPathLine(PathLine line)
		{
			_line = line;
			_msg = GetComponent<MsgMixte>();
			_initialPos.x = _msg.longitude;
			_initialPos.y = _msg.latitude;
			ComputeLineIdxAndRatio();
		}

		public void ComputeLineIdxAndRatio()
		{
			_line.ComputeNearestPointOnLine(new Vector2(_msg.longitude, _msg.latitude), out _firstLineIdx, out _segmentRatio);
		}

		public void RefreshPosition()
		{
			if (_firstLineIdx != -1)
			{
				Vector2 pointA = _line.points[_firstLineIdx];
				Vector2 pointB = _line.points[_firstLineIdx+1];
				Vector2 pos = Vector2.Lerp(pointA, pointB, _segmentRatio);
				_msg.SetCoords(pos.x, pos.y);
			}
		}

		public void ResetPosition()
		{
			_msg.SetCoords(_initialPos.x, _initialPos.y);
		}

		public void ValidPosition()
		{
			ControllerAnnotation annotation = _msg.annotation;
			annotation.coords = ApplicationManager.ConvertVector2ToCoords(new Vector2(_msg.longitude, _msg.latitude));
		}
	}
}
