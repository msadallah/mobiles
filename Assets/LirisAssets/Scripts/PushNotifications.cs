namespace Liris.App
{
    using System.Collections.Generic;
    using UnityEngine;
    using Firebase.Messaging;
    using Firebase.Database;
    using System;
    using System.Threading.Tasks;
    using Liris.App.Controllers; 

    public class PushNotifications : MonoBehaviour
    {
        private DatabaseReference _databaseReference;
        private bool _isFirebaseReady = false;

        public void Start()
        {
            Debug.Log("[FIREBASE] PushNotifications - Started");

            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;

                    // Set the specific database URL here
                    _databaseReference = FirebaseDatabase.GetInstance("https://mobiles-rs-default-rtdb.europe-west1.firebasedatabase.app").RootReference;
                    Debug.Log(_databaseReference != null ? "[FIREBASE] Database reference initialized." : "[FIREBASE] Failed to initialize database reference.");

                    _isFirebaseReady = true;

                    FirebaseMessaging.TokenReceived += OnTokenReceived;
                    FirebaseMessaging.MessageReceived += OnMessageReceived;
                }
                else
                {
                    Debug.LogError($"[FIREBASE] Could not resolve all Firebase dependencies: {dependencyStatus}");
                }
            });
        }

        private void Awake()
        {
            ApplicationManager.onLoginEvent += OnUserLogin;
        }

        private void OnDestroy()
        {
            ApplicationManager.onLoginEvent -= OnUserLogin;
        }

        void OnUserLogin(ControllerUser user)
        {
            if (!_isFirebaseReady)
            {
                Debug.LogError("[FIREBASE] OnUserLogin - Firebase is not ready yet."); 
                return;
            }

            string userId = user.id;
            Debug.Log("[FIREBASE] User is signed in with userId: " + userId);

            Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
            _databaseReference = FirebaseDatabase.GetInstance("https://mobiles-rs-default-rtdb.europe-west1.firebasedatabase.app").RootReference;

#if UNITY_EDITOR
            Debug.Log("[FIREBASE] Don't update database with StubToken for userId: " + userId);
#else
            // Await the asynchronous operation
            Task.Run(async () =>
            {
                string token = await FirebaseMessaging.GetTokenAsync();

                if (!string.IsNullOrEmpty(token))
                {
                    Debug.Log("[FIREBASE] Received Registration Token: " + token);
                    ApplicationManager.instance.SetFirebaseToken(token);
                    await UpdateTokenInDatabase(userId, token);
                }
                else
                {
                    Debug.LogError("[FIREBASE] Failed to get token");
                }
            }).ConfigureAwait(true);
#endif
        }

        public void OnTokenReceived(object sender, TokenReceivedEventArgs token)
        {
            if (!_isFirebaseReady)
            {
                Debug.LogError("[FIREBASE] OnTokenReceived - Firebase is not ready yet."); 
                return;
            }

            Debug.Log("[FIREBASE] Received Registration Token: " + token.Token);
            ApplicationManager.instance.SetFirebaseToken(token.Token);
        }

        public void OnMessageReceived(object sender, MessageReceivedEventArgs e)
        {
            if (!_isFirebaseReady)
            {
                Debug.LogError("[FIREBASE] OnMessageReceived - Firebase is not ready yet."); 
                return;
            }

            Debug.Log("[FIREBASE] Received a new message from: " + e.Message.From);
        }

        public async Task UpdateTokenInDatabase(string userId, string token)
        {
            if (_databaseReference == null)
            {
                Debug.LogError("[FIREBASE] UpdateTokenInDatabase - Database reference is null.");
                return;
            }

            var tokenDictionary = new Dictionary<string, object>
            {
                { "token", token }
            };

            try
            {
                await _databaseReference.Child("tokens").Child(userId).UpdateChildrenAsync(tokenDictionary);
                Debug.Log("[FIREBASE] Token updated in Firebase Realtime Database.");
            }
            catch (Exception e)
            {
                Debug.LogError("[FIREBASE] Failed to update token in Firebase Realtime Database: " + e.Message);
            }
        }
    }
}
