namespace Liris.App.Notifications
{
	using Liris.App.Controllers;
	using UnityEngine;
	using UnityEngine.UI;
	using TMPro;

	public class NotificationItem : MonoBehaviour
	{
		public ControllerNotification notification => _notif;

		[SerializeField]
		private TextMeshProUGUI _title = null;
		[SerializeField]
		private TextMeshProUGUI _body = null;
		[SerializeField]
		private Button _button = null;
		[Header("Adapt text")]
		[SerializeField]
		private RectTransform _scrollRT = null;
		[SerializeField]
		private RectTransform _contentRT = null;
		[SerializeField]
		private float _minHeight = 128;
		[SerializeField]
		private float _maxHeight = 512;

		private System.Action<NotificationItem> _onClick = null;
		private ControllerNotification _notif = null;
		private bool _needToAdaptText = false;

		private void Awake()
		{
			_button.onClick.AddListener(OnButtonClicked);
		}

		private void OnDestroy()
		{
			_button.onClick.RemoveListener(OnButtonClicked);
			_onClick = null;
			_notif = null;
		}

		private void Update()
		{
			if (_needToAdaptText)
			{
				LayoutRebuilder.ForceRebuildLayoutImmediate(_contentRT);
				float height = Mathf.Clamp(_contentRT.sizeDelta.y, _minHeight, _maxHeight);
				_scrollRT.sizeDelta = new Vector2(_scrollRT.sizeDelta.x, height);
				LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent.GetRT());
				_needToAdaptText = false;
			}
		}

		public void InitController(ControllerNotification notif, System.Action<NotificationItem> onClick = null)
		{
			_notif = notif;
			_onClick = onClick;
			if (!string.IsNullOrEmpty(notif.title))
				_title.text = notif.title + " - " + ControllerBase.ConvertDateDayHourMinute(notif.date);
			else
				_title.text = ControllerBase.ConvertDateDayHourMinute(notif.date);
			_body.text = notif.content;
			_needToAdaptText = true;
		}

		private void OnButtonClicked()
		{
			MainUI.instance.ShowNotificationPopup(_notif);
			_onClick?.Invoke(this);
		}
	}
}
