namespace Liris.App.Groups
{
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
	using Liris.App.Popups;
	using System.Collections.Generic;
	using UnityEngine;

	public class GroupsManager : MonoBehaviour
    {
		private List<int> _groupList = null;

		public bool IsUserInGroup(string id)
		{
			int num = ConvertTools.ToInt32(id, -1);
			return IsUserInGroup(num);
		}

		public bool IsUserInGroup(int num)
		{
			if (_groupList != null)
				return _groupList.Contains(num);
			return false;
		}

		public bool IsUserInShareGroups(string share)
		{
			if (share.Contains(SharePopup.GROUPS))
			{
				string groups = share.Substring(SharePopup.GROUPS.Length);
				List<object> list = JSON.Deserialize(groups) as List<object>;
				foreach (object o in list)
				{
					int num = ConvertTools.ToInt32(o, -1);
					if (IsUserInGroup(num))
						return true;
				}
			}
			return false;
		}

		public List<ControllerGroup> GetUserGroups()
		{
			List<ControllerGroup> groups = new List<ControllerGroup>();
			foreach (ControllerGroup group in ApplicationManager.instance.dataManager.groups)
			{
				if (IsUserInGroup(group.id))
					groups.Add(group);
			}
			return groups;
		}

		public void InitFromController(ControllerUser user)
		{
			InitGroupList(user.usergroups);
		}

		public void SetGroups(List<int> groups)
		{
			_groupList = groups;
		}

		private void InitGroupList(string json)
		{
			_groupList = new List<int>();
			if (!string.IsNullOrEmpty(json))
			{
				List<object> list = JSON.Deserialize(json) as List<object>;
				if (list != null)
				{
					foreach (object o in list)
					{
						_groupList.Add(ConvertTools.ToInt32(o));
					}
					_groupList.Sort();
				}
			}
		}

		private void OnDestroy()
		{
			_groupList = null;
		}
	}
}