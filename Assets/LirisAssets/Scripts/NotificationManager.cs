namespace Liris.App.Notification
{
	using AllMyScripts.Common.Tools;
	using AllMyScripts.LangManager;
	using Liris.App.Assets;
	using Liris.App.Controllers;
	using System.Collections;
	using System.Collections.Generic;
    using UnityEngine;

    public class NotificationManager : MonoBehaviour
    {
        public enum NotificationType
        {
            EMOJI_ON_ANNOTATION,
            EMOJI_ON_TOUR,
            MESSAGE_ON_ANNOTATION,
            MESSAGE_ON_TOUR
        }

        public class NotificationData
        {
            public NotificationType notifType;
            public string id;
            public string value;
        }

        [SerializeField]
        private SpritesList _emojis;
        [SerializeField]
        private float _delayToNotif = 1f;
        [SerializeField]
        private float _delayToRefresh = 10f;

        private Queue<NotificationData> _notifs = null;
        private float _timeToNotif = 0f;
        private float _timeToRefresh = 0f;
        private Dictionary<string, System.DateTime> _spamDetection = null;

        public void AddNotification(NotificationType notifType, string id, string value)
        {
            NotificationData notifData = new NotificationData();
            notifData.notifType = notifType;
            notifData.id = id;
            notifData.value = value;
            _notifs.Enqueue(notifData);
        }

        // Start is called before the first frame update
        void Start()
        {
            _notifs = new Queue<NotificationData>();
            _spamDetection = new Dictionary<string, System.DateTime>();
        }

        private void OnDestroy()
        {
            _notifs = null;
            _spamDetection = null;
        }

        // Update is called once per frame
        void Update()
        {
            _timeToNotif += Time.deltaTime;
            if (_timeToNotif > _delayToNotif)
            {
                _timeToNotif = 0f;
                SendNotif();
            }

            _timeToRefresh += Time.deltaTime;
            if (_timeToRefresh > _delayToRefresh)
            {
                _timeToRefresh = 0f;
                Refresh();
            }
        }

        private void SendNotif()
		{
            if (_notifs.Count > 0)
            {
                NotificationData data = _notifs.Dequeue();
                switch (data.notifType)
                {
                    case NotificationType.EMOJI_ON_ANNOTATION:
                        SendEmojiOnAnnotation(data);
                        break;
                    case NotificationType.EMOJI_ON_TOUR:
                        SendEmojiOnTour(data);
                        break;
                    case NotificationType.MESSAGE_ON_ANNOTATION:
                        SendMessageOnAnnotation(data);
                        break;
                    case NotificationType.MESSAGE_ON_TOUR:
                        SendMessageOnTour(data);
                        break;
                }
			}
		}

		private void Refresh()
		{
			StartCoroutine(RefreshEnum());
		}

        private IEnumerator RefreshEnum()
		{
            ApplicationManager app = ApplicationManager.instance;
            if (app.user != null)
			{
                yield return app.dataManager.GetAllNotifications(app.user.id);
                int notifNotReadCount = app.dataManager.GetNotReadNotificationCount();
                MainUI.instance.SetNotificationsNotifCount(notifNotReadCount);
            }
        }

		private void SendEmojiOnAnnotation(NotificationData data)
        {
            ControllerAnnotation annotation = ApplicationManager.instance.dataManager.GetAnnotationFromId(data.id);
            if (annotation != null)
			{
                ControllerUser user = annotation.GetUser();
                if (user != null && ApplicationManager.instance.user != user)
                {
                    if (CheckSpam(user.id, data.notifType, data.id))
					{
                        string userName = ApplicationManager.instance.user.username;

                        Dictionary<string, object> dicParams = new Dictionary<string, object>();
                        dicParams["annotationId"] = data.id;
                        dicParams["showRef"] = true;

                        string title = L.Get("id_notification_send_emoji_title");
                        string content = L.Get("id_notification_send_emoji_annotation").Replace("%USER%", userName).Replace("%EMOJI%", _emojis.GetData(data.value).text);
                        string category = "Annotation Emoji";
                        string parameters = JSON.Serialize(dicParams);
                        ApplicationManager.instance.dataManager.SendNotification(user.id, title, content, category, parameters, GetExpiredDate(2f));
                    }
                }
            }
	    }

        private void SendEmojiOnTour(NotificationData data)
        {
            ControllerTour tour = ApplicationManager.instance.dataManager.GetTourFromId(data.id);
            if (tour != null)
            {
                ControllerUser user = tour.GetUser();
                if (user != null && ApplicationManager.instance.user != user)
                {
                    if (CheckSpam(user.id, data.notifType, data.id))
                    {
                        string userName = ApplicationManager.instance.user.username;

                        Dictionary<string, object> dicParams = new Dictionary<string, object>();
                        dicParams["tourId"] = data.id;
                        dicParams["showRef"] = true;

                        string title = L.Get("id_notification_send_emoji_title");
                        string content = L.Get("id_notification_send_emoji_tour").Replace("%USER%", userName).Replace("%EMOJI%", _emojis.GetData(data.value).text);
                        string category = "Tour Emoji";
                        string parameters = JSON.Serialize(dicParams);
                        ApplicationManager.instance.dataManager.SendNotification(user.id, title, content, category, parameters, GetExpiredDate(2f));
                    }
                }
            }
        }

        private void SendMessageOnAnnotation(NotificationData data)
        {
            ControllerAnnotation annotation = ApplicationManager.instance.dataManager.GetAnnotationFromId(data.id);
            if (annotation != null)
            {
                ControllerUser user = annotation.GetUser();
                if (user != null && ApplicationManager.instance.user != user)
                {
                    if (CheckSpam(user.id, data.notifType, data.id))
                    {
                        string userName = ApplicationManager.instance.user.username;

                        Dictionary<string, object> dicParams = new Dictionary<string, object>();
                        dicParams["annotationId"] = data.id;
                        dicParams["showRef"] = true;

                        string title = L.Get("id_notification_send_message_title");
                        string content = L.Get("id_notification_send_message_annotation").Replace("%USER%", userName) + data.value;
                        string category = "Annotation Message";
                        string parameters = JSON.Serialize(dicParams);
                        ApplicationManager.instance.dataManager.SendNotification(user.id, title, content, category, parameters, GetExpiredDate(3f));
                    }
                }
            }
        }

        private void SendMessageOnTour(NotificationData data)
        {
            ControllerTour tour = ApplicationManager.instance.dataManager.GetTourFromId(data.id);
            if (tour != null)
            {
                ControllerUser user = tour.GetUser();
                if (user != null && ApplicationManager.instance.user != user)
                {
                    if (CheckSpam(user.id, data.notifType, data.id))
                    {
                        string userName = ApplicationManager.instance.user.username;

                        Dictionary<string, object> dicParams = new Dictionary<string, object>();
                        dicParams["tourId"] = data.id;
                        dicParams["showRef"] = true;

                        string title = L.Get("id_notification_send_message_title");
                        string content = L.Get("id_notification_send_message_tour").Replace("%USER%", userName) + data.value;
                        string category = "Tour Message";
                        string parameters = JSON.Serialize(dicParams);
                        ApplicationManager.instance.dataManager.SendNotification(user.id, title, content, category, parameters, GetExpiredDate(3f));
                    }
                }
            }
        }

        private string GetExpiredDate(float days)
		{
            return ControllerBase.ConvertDateTimeToSend(System.DateTime.Now.AddDays(days));
        }

        private bool CheckSpam(string userId, NotificationType notifType, string id, float delayInSeconds = 60f)
		{
            string identifier = $"id_{userId}_{notifType}_{id}";
            if (_spamDetection.TryGetValue(identifier, out System.DateTime date))
			{
                if (date - System.DateTime.Now < System.TimeSpan.FromSeconds(delayInSeconds))
				{
                    return false;
				}
                else
				{
                    _spamDetection[identifier] = System.DateTime.Now;
                }
			}
            else
			{
                _spamDetection.Add(identifier, System.DateTime.Now);
            }
            return true;
		}
    }
}