namespace Liris.App.Media
{
	using AllMyScripts.Common.Tools;
	using Liris.App.WebServices;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class MediaManager : MonoBehaviour
	{
		public class MediaRequest
		{
			public string mediaId;
			public Texture2D tex;
			public System.Action<MediaRequest> result = null;
		}

		private Queue<MediaRequest> _medias = null;
		private bool _busy = false;

		public void RequestMedia(string id, System.Action<MediaRequest> result = null)
		{
			MediaRequest request = new MediaRequest();
			request.mediaId = id;
			request.tex = null;
			request.result = result;

			_medias.Enqueue(request);
		}

		private void Start()
		{
			_medias = new Queue<MediaRequest>();
		}

		private void Update()
		{
			if (_medias.Count > 0 && !_busy)
			{
				MediaRequest request = _medias.Dequeue();
				if (request != null)
				{
					_busy = true;
					StartCoroutine(RequestMediaEnum(request));
				}
			}
		}

		private void OnDestroy()
		{
			_medias = null;
		}

		private IEnumerator RequestMediaEnum(MediaRequest request)
		{
			string token = ApplicationManager.instance.dataManager.token;
			WebServiceMediaObjectsGetFromId query = new WebServiceMediaObjectsGetFromId(token, request.mediaId);
			yield return query.Run();
			if (!query.hasFailed)
			{
				Dictionary<string, object> dic = query.GetResultAsDic();
				string url = DicTools.GetValueString(dic, "contentUrl");
				if (!string.IsNullOrEmpty(url))
				{
					WebServiceMediaObjectsDownload queryD = new WebServiceMediaObjectsDownload(url);
					yield return queryD.Run();
					if (!queryD.hasFailed)
					{
						request.tex = new Texture2D(2, 2);
						request.tex.LoadImage(queryD.responseAsBytes);
						SpriteLibrary.SetSprite(request.mediaId, request.tex, System.DateTime.Now.ToString(), format: "png");
						SpriteLibrary.ApplySaving();
					}
				}
			}

			request.result?.Invoke(request);

			_busy = false;
		}
	}
}

