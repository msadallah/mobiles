namespace Liris.App.Favorites
{
	using AllMyScripts.Common.Tools;
	using Liris.App.Controllers;
	using Liris.App.Popups;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;

	public class FavoritesItem : MonoBehaviour
	{
		[SerializeField]
		private EditTourPopup _popupTour = null;
		[SerializeField]
		private MsgMixtePopup _popupAnnotation = null;
		[SerializeField]
		private Button _button = null;
		[SerializeField]
		private GameObject _filtered = null;

		public ControllerAnnotation annotation => _annotation;
		public ControllerTour tour => _tour;

		private System.Action<FavoritesItem> _onClick = null;
		private float _longitude = 0f;
		private float _latitude = 0f;
		private ControllerAnnotation _annotation = null;
		private ControllerTour _tour = null;

		private void Awake()
		{
			_button.onClick.AddListener(OnButtonClicked);
		}

		private void OnDestroy()
		{
			_button.onClick.RemoveListener(OnButtonClicked);
			_onClick = null;
			_tour = null;
			_annotation = null;
		}

		public void InitController(ControllerTour tour, System.Action<FavoritesItem> onClick = null)
		{
			_tour = tour;
			_onClick = onClick;
			_popupTour.SetTour(tour, false);
			Vector2 pos = tour.points[0];
			_longitude = pos.x;
			_latitude = pos.y;
			SetShowable(ApplicationManager.instance.filterManager.IsTourVisible(_tour));
		}

		public void InitController(ControllerAnnotation annotation, System.Action<FavoritesItem> onClick = null)
		{
			_annotation = annotation;
			_onClick = onClick;
			_popupAnnotation.SetAnnotation(annotation, false);
			Vector2 pos = ApplicationManager.ConvertCoordsToVector2(annotation.coords);
			_longitude = pos.x;
			_latitude = pos.y;
			SetShowable(ApplicationManager.instance.filterManager.IsAnnotationVisible(_annotation));
		}

		private void SetShowable(bool showable)
		{
			_button.image.color = showable ? Color.clear : new Color(0, 0, 0, 0.2f);
			_button.interactable = showable;
			_filtered.SetActive(!showable);
		}

		private void OnButtonClicked()
		{
			ApplicationManager.instance.SetMapOnPosition(_longitude, _latitude, 19.9f);
			_onClick?.Invoke(this);
		}
	}
}
