namespace Liris.App.Favorites
{
    using UnityEngine;
    using UnityEngine.UI;

    public class FavoritesButtons : MonoBehaviour
    {
        public delegate void OnFavoriteStateChanged(FavoriteState state);
        public OnFavoriteStateChanged onFavoriteStateChanged = null;

        public enum FavoriteState
        {
            EMPTY,
            FULL
        }

        [SerializeField]
        private Button _emptyButton = null;
        [SerializeField]
        private Button _fullButton = null;

        public void SetFavoritesFull(bool full, bool sendEvent = true)
        {
            SetFavoritesState(full ? FavoriteState.FULL : FavoriteState.EMPTY, sendEvent);
        }

        void Awake()
        {
            SetFavoritesFull(false, false);
            _emptyButton.onClick.AddListener(OnEmptyClicked);
            _fullButton.onClick.AddListener(OnFullClicked);
        }

        void OnDestroy()
        {
            _emptyButton.onClick.RemoveListener(OnEmptyClicked);
            _fullButton.onClick.RemoveListener(OnFullClicked);
        }

        private void OnEmptyClicked()
        {
            SetFavoritesFull(true);
        }

        private void OnFullClicked()
        {
            SetFavoritesFull(false);
        }

        private void SetFavoritesState(FavoriteState state, bool sendEvent)
        {
            _emptyButton.gameObject.SetActive(state == FavoriteState.EMPTY);
            _fullButton.gameObject.SetActive(state == FavoriteState.FULL);
            if (sendEvent)
                onFavoriteStateChanged?.Invoke(state);
        }
    }
}
