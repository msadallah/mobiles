// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Ubiant/Angle Lit/Vertex Color Dither"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		[Toggle(_TRANSPARENT_ON)] _Transparent("Transparent", Float) = 0
		[Enum(UnityEngine.Rendering.CullMode)]_Culling("Culling", Float) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Geometry-1" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull [_Culling]
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature _TRANSPARENT_ON
		struct Input
		{
			float3 worldNormal;
			half ASEVFace : VFACE;
			float3 worldPos;
			float4 vertexColor : COLOR;
			float4 screenPosition;
		};

		uniform float _Culling;
		uniform float _Cutoff = 0.5;


		inline float Dither8x8Bayer( int x, int y )
		{
			const float dither[ 64 ] = {
				 1, 49, 13, 61,  4, 52, 16, 64,
				33, 17, 45, 29, 36, 20, 48, 32,
				 9, 57,  5, 53, 12, 60,  8, 56,
				41, 25, 37, 21, 44, 28, 40, 24,
				 3, 51, 15, 63,  2, 50, 14, 62,
				35, 19, 47, 31, 34, 18, 46, 30,
				11, 59,  7, 55, 10, 58,  6, 54,
				43, 27, 39, 23, 42, 26, 38, 22};
			int r = y * 8 + x;
			return dither[r] / 64; // same # of instructions as pre-dividing due to compiler magic
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float4 ase_screenPos = ComputeScreenPos( UnityObjectToClipPos( v.vertex ) );
			o.screenPosition = ase_screenPos;
		}

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float temp_output_15_0_g119 = 0.5;
			float temp_output_16_0_g119 = 1.0;
			float3 temp_output_53_0_g119 = mul( unity_CameraToWorld, float4( float3(0,0,1) , 0.0 ) ).xyz;
			float3 break82_g119 = temp_output_53_0_g119;
			float3 appendResult81_g119 = (float3(break82_g119.x , 0.001 , break82_g119.z));
			float3 normalizeResult85_g119 = normalize( appendResult81_g119 );
			float smoothstepResult113_g119 = smoothstep( 0.95 , 1.0 , abs( break82_g119.y ));
			float3 lerpResult114_g119 = lerp( normalizeResult85_g119 , temp_output_53_0_g119 , smoothstepResult113_g119);
			float3 ase_worldNormal = i.worldNormal;
			float3 ase_normWorldNormal = normalize( ase_worldNormal );
			float3 switchResult121_g119 = (((i.ASEVFace>0)?(ase_normWorldNormal):(( float3( -1,-1,-1 ) * ase_normWorldNormal ))));
			float3 Normal125_g119 = switchResult121_g119;
			float3 break127_g119 = Normal125_g119;
			float3 appendResult84_g119 = (float3(break127_g119.x , 0.001 , break127_g119.z));
			float3 normalizeResult86_g119 = normalize( appendResult84_g119 );
			float smoothstepResult106_g119 = smoothstep( 0.95 , 1.0 , abs( break127_g119.y ));
			float3 lerpResult109_g119 = lerp( normalizeResult86_g119 , Normal125_g119 , smoothstepResult106_g119);
			float dotResult44_g119 = dot( lerpResult114_g119 , lerpResult109_g119 );
			float temp_output_1_0_g120 = 0.4;
			float lerpResult14_g119 = lerp( temp_output_15_0_g119 , temp_output_16_0_g119 , ( ( ( abs( acos( dotResult44_g119 ) ) / UNITY_PI ) - temp_output_1_0_g120 ) / ( 1.0 - temp_output_1_0_g120 ) ));
			float clampResult102_g119 = clamp( lerpResult14_g119 , temp_output_15_0_g119 , temp_output_16_0_g119 );
			float3 ase_worldPos = i.worldPos;
			float clampResult41_g119 = clamp( ( distance( _WorldSpaceCameraPos , ase_worldPos ) / 35.0 ) , 0.0 , 1.0 );
			float lerpResult30_g119 = lerp( 1.0 , 0.86 , clampResult41_g119);
			o.Emission = ( ( clampResult102_g119 * lerpResult30_g119 ) * i.vertexColor ).rgb;
			o.Alpha = 1;
			float4 ase_screenPos = i.screenPosition;
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 clipScreen1 = ase_screenPosNorm.xy * _ScreenParams.xy;
			float dither1 = Dither8x8Bayer( fmod(clipScreen1.x, 8), fmod(clipScreen1.y, 8) );
			dither1 = step( dither1, (0.0 + (i.vertexColor.a - 0.02) * (1.0 - 0.0) / (0.917 - 0.02)) );
			#ifdef _TRANSPARENT_ON
				float staticSwitch25 = dither1;
			#else
				float staticSwitch25 = 1.0;
			#endif
			clip( staticSwitch25 - _Cutoff );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Unlit keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float4 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float3 worldNormal : TEXCOORD3;
				half4 color : COLOR0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.customPack1.xyzw = customInputData.screenPosition;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.color = v.color;
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.screenPosition = IN.customPack1.xyzw;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				surfIN.vertexColor = IN.color;
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Legacy Shaders/Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18301
2262;88;1454;975;1148.653;537.3548;1.498147;True;False
Node;AmplifyShaderEditor.CommentaryNode;21;-591.7416,260.4277;Inherit;False;311.8179;266.0522;Due to dither starting and finishing early, we remap the extreme values so that we don't show .99 as "tiny holes";1;22;;1,1,1,1;0;0
Node;AmplifyShaderEditor.VertexColorNode;2;-894.5,-23.5;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;22;-541.5212,325.6045;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0.02;False;2;FLOAT;0.917;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DitheringNode;1;-197.6325,316.7617;Inherit;False;1;False;3;0;FLOAT;0;False;1;SAMPLER2D;;False;2;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;26;-138.1792,167.9222;Inherit;False;Constant;_One;One;2;0;Create;True;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;25;20.8208,165.9222;Inherit;False;Property;_Transparent;Transparent;1;0;Create;True;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;27;315.8208,-127.0778;Inherit;False;Property;_Culling;Culling;2;1;[Enum];Create;True;0;1;UnityEngine.Rendering.CullMode;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;36;-247.9238,-9.679298;Inherit;False;WallAlbedo;-1;;118;00f20638cc3ff814bb415742ea938c9a;0;2;25;COLOR;0,0,0,0;False;24;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;280.3177,-39.95764;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;Ubiant/Angle Lit/Vertex Color Dither;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;-1;True;TransparentCutout;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;Legacy Shaders/Diffuse;0;-1;-1;-1;0;False;0;0;True;27;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;22;0;2;4
WireConnection;1;0;22;0
WireConnection;25;1;26;0
WireConnection;25;0;1;0
WireConnection;36;25;2;0
WireConnection;0;2;36;0
WireConnection;0;10;25;0
ASEEND*/
//CHKSM=A1E955A935C23A312C415F4A047E9D7CFE6A399A