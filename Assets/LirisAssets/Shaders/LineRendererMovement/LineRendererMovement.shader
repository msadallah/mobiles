// Made with Amplify Shader Editor v1.9.1.3
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader ""
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,0.7921569)
		_Amount("Amount", Float) = 1
		_Speed("Speed", Float) = 1
		_Color2("Color2", Color) = (0,0,0,0.3254902)
		_RelativeSize("Relative Size", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Custom"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		ZTest [_ZTesting]
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
		};

		uniform float4 _Color;
		uniform float4 _Color2;
		uniform float _Amount;
		uniform float _Speed;
		uniform float _RelativeSize;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float lerpResult138 = lerp( 0.6 , 1.0 , ( step( i.uv_texcoord.y , 0.9 ) * step( ( 1.0 - i.uv_texcoord.y ) , 0.9 ) ));
			float4 temp_cast_0 = (0.5).xxxx;
			float mulTime2 = _Time.y * ( _Speed * -1.0 );
			float clampResult56 = clamp( pow( abs( ( ( frac( ( ( ( i.uv_texcoord.x * _Amount ) + mulTime2 ) * 0.5 ) ) * 2.0 ) - 1.0 ) ) , _RelativeSize ) , 0.0 , 1.0 );
			float Movement61 = clampResult56;
			float4 lerpResult36 = lerp( _Color , _Color2 , Movement61);
			float4 ColorSet176 = lerpResult36;
			o.Emission = ( lerpResult138 * ( ( i.vertexColor - temp_cast_0 ) + ColorSet176 ) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=19103
Node;AmplifyShaderEditor.CommentaryNode;34;-2396.908,84.431;Inherit;False;1640.769;663.9999;PingPong on Time And UVs;13;1;9;11;2;12;32;10;33;30;31;37;45;57;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;37;-2370.772,491.3301;Float;False;Property;_Speed;Speed;3;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;-2236.354,505.1647;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;1;-2346.908,134.431;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;9;-2278.908,416.4309;Float;False;Property;_Amount;Amount;2;0;Create;True;0;0;0;False;0;False;1;5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-2096.907,315.4309;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;12;-1915.969,440.1938;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;-1710.739,430.5701;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;10;-1523.469,429.4938;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;-1361.039,424.0702;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;30;-1147.839,421.4701;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;31;-922.6808,420.7804;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;55;-900.7772,854.4663;Inherit;False;Property;_RelativeSize;Relative Size;9;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;54;-677.3317,591.5447;Inherit;True;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;56;-441.5899,528.5916;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;61;-244.3987,550.8107;Inherit;False;Movement;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;64;-4392.216,-1138.184;Inherit;False;1023.565;538.8458;2nd Set Of Colors (only used in 2 and 3 colors mode);5;77;67;68;66;65;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;63;-4434.81,-1732.374;Inherit;False;1060.596;544.4006;1st Set Of Colors;5;76;36;35;17;62;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;68;-4362.341,-704.1147;Inherit;False;61;Movement;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;62;-4190.56,-1303.61;Inherit;False;61;Movement;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;86;-4410.019,-533.8013;Inherit;False;1023.565;538.8458;3rd Set Of Colors (only used in 3 colors mode);5;91;90;89;88;87;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;57;-2084.851,136.0607;Inherit;False;UVs;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ColorNode;66;-4324.266,-889.0806;Float;False;Property;_ColorSet2_2;ColorSet2_2;7;1;[PerRendererData];Create;True;0;0;0;False;0;False;0,0,0,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;65;-4350.145,-1087.795;Float;False;Property;_ColorSet2_1;ColorSet2_1;4;1;[PerRendererData];Create;True;0;0;0;False;0;False;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;92;-3221.186,-1136.991;Inherit;False;1523.703;830.2478;Fuse Three Colors (only used in 3 colors mode);10;93;94;103;102;84;101;98;97;96;95;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;96;-3171.186,-838.8812;Inherit;False;57;UVs;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;85;-3196.874,-1689.166;Inherit;False;1337.32;423.3516;Fuse Two Colors (only used in 3 colors mode);7;75;71;74;72;79;80;83;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;87;-4328.342,-114.5325;Inherit;False;61;Movement;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;90;-4348.276,-473.5773;Float;False;Property;_ColorSet3_1;ColorSet3_1;5;1;[PerRendererData];Create;True;0;0;0;False;0;False;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;67;-3944.471,-1026.161;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;91;-4342.069,-284.698;Float;False;Property;_ColorSet3_2;ColorSet3_2;8;1;[PerRendererData];Create;True;0;0;0;False;0;False;0,0,0,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;36;-3937.315,-1463.934;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;88;-3962.275,-421.7784;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;76;-3573.826,-1467.762;Inherit;False;ColorSet1;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;95;-2969.365,-872.2102;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.GetLocalVarNode;72;-3146.874,-1391.056;Inherit;False;57;UVs;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;77;-3590.49,-1028.942;Inherit;False;ColorSet2;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;94;-2693.412,-854.4605;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.32;False;2;FLOAT;0.34;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;89;-3608.294,-424.5594;Inherit;False;ColorSet3;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;98;-2756.436,-1024.038;Inherit;False;77;ColorSet2;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;97;-2752.732,-1086.991;Inherit;False;76;ColorSet1;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;74;-2945.053,-1424.385;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.GetLocalVarNode;103;-2445.255,-716.5496;Inherit;False;89;ColorSet3;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;71;-2667.319,-1518.814;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.49;False;2;FLOAT;0.51;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;80;-2732.124,-1576.213;Inherit;False;77;ColorSet2;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;79;-2728.42,-1639.166;Inherit;False;76;ColorSet1;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;101;-2673.174,-639.9833;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.65;False;2;FLOAT;0.67;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;93;-2468.237,-1020.477;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;102;-2231.582,-727.2332;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;75;-2413.655,-1576.213;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;84;-2005.689,-727.7855;Inherit;False;ThreeColorsValue;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;104;-1521.121,-312.1263;Inherit;False;1121.713;315.5483;Select the right calcuation;5;69;78;81;100;59;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;83;-2115.553,-1585.471;Inherit;False;TwoColorsValue;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;100;-1476.488,-104.4972;Inherit;False;84;ThreeColorsValue;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;78;-1428.535,-262.1263;Inherit;False;76;ColorSet1;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;81;-1471.121,-189.9153;Inherit;False;83;TwoColorsValue;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;59;-1164.549,-243.4685;Inherit;False;Property;_COLORSCOUNT;COLORSCOUNT;13;0;Create;True;0;0;0;False;0;False;1;0;0;False;;KeywordEnum;3;ONESET;TWOSETS;THREESETS;Create;False;True;All;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;69;-642.4083,-254.5779;Inherit;True;Color;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.VertexColorNode;46;-287.5483,-211.1031;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;118;-352.9123,-922.306;Inherit;False;1135.461;610.6134;Border;10;129;131;125;132;135;136;137;138;139;140;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleTimeNode;2;-2103.408,495.4309;Inherit;True;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;9.006333,-136.5718;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;39;1056.298,69.06278;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;_ZTesting;0;True;_ZTesting;False;0;False;;0;False;;False;0;Custom;0.5;True;True;0;True;Custom;;Transparent;All;12;all;True;True;True;True;0;False;;False;0;False;;255;False;;255;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;False;2;15;10;25;False;0.5;True;2;5;False;;10;False;;0;0;False;;0;False;;0;False;;0;False;;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;1;-1;-1;-1;0;False;0;0;False;;-1;0;False;;0;0;0;False;0.1;False;;0;False;;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.ColorNode;17;-4352.942,-1682.374;Float;False;Property;_Color;Color;0;0;Create;True;0;0;0;False;0;False;1,1,1,0.7921569;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;70;-431.803,170.711;Inherit;False;69;Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;35;-4361.469,-1496.441;Float;False;Property;_Color2;Color2;6;0;Create;True;0;0;0;False;0;False;0,0,0,0.3254902;0.6603774,0.1723626,0.1723626,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;124;98.83618,119.335;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;125;-68.8474,-737.2189;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StepOpNode;131;216.5465,-752.3626;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;137;376.9747,-682.6124;Inherit;False;Constant;_Float0;Float 0;10;0;Create;True;0;0;0;False;0;False;0.9;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;136;559.8795,-720.8602;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;129;-37.61583,-613.9942;Inherit;False;Constant;_Float1;Float 1;10;0;Create;True;0;0;0;False;0;False;0.9;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;132;322.491,-790.0113;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;138;163.6738,-422.4242;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;135;413.8821,-594.2916;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;139;-74.97354,-499.6628;Inherit;False;Constant;_Float2;Float 2;10;0;Create;True;0;0;0;False;0;False;0.6;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;140;-74.01038,-401.1827;Inherit;False;Constant;_Float3;Float 3;10;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;142;-158.6163,65.74792;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;143;-356.2164,81.34792;Inherit;False;Constant;_Float4;Float 4;10;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;123;-69.6504,277.7577;Inherit;True;76;ColorSet1;1;0;OBJECT;;False;1;COLOR;0
WireConnection;45;0;37;0
WireConnection;11;0;1;1
WireConnection;11;1;9;0
WireConnection;12;0;11;0
WireConnection;12;1;2;0
WireConnection;32;0;12;0
WireConnection;10;0;32;0
WireConnection;33;0;10;0
WireConnection;30;0;33;0
WireConnection;31;0;30;0
WireConnection;54;0;31;0
WireConnection;54;1;55;0
WireConnection;56;0;54;0
WireConnection;61;0;56;0
WireConnection;57;0;1;0
WireConnection;67;0;65;0
WireConnection;67;1;66;0
WireConnection;67;2;68;0
WireConnection;36;0;17;0
WireConnection;36;1;35;0
WireConnection;36;2;62;0
WireConnection;88;0;90;0
WireConnection;88;1;91;0
WireConnection;88;2;87;0
WireConnection;76;0;36;0
WireConnection;95;0;96;0
WireConnection;77;0;67;0
WireConnection;94;0;95;1
WireConnection;89;0;88;0
WireConnection;74;0;72;0
WireConnection;71;0;74;1
WireConnection;101;0;95;1
WireConnection;93;0;97;0
WireConnection;93;1;98;0
WireConnection;93;2;94;0
WireConnection;102;0;93;0
WireConnection;102;1;103;0
WireConnection;102;2;101;0
WireConnection;75;0;79;0
WireConnection;75;1;80;0
WireConnection;75;2;71;0
WireConnection;84;0;102;0
WireConnection;83;0;75;0
WireConnection;59;1;78;0
WireConnection;59;0;81;0
WireConnection;59;2;100;0
WireConnection;69;0;59;0
WireConnection;2;0;45;0
WireConnection;47;0;138;0
WireConnection;47;1;124;0
WireConnection;39;2;47;0
WireConnection;124;0;142;0
WireConnection;124;1;123;0
WireConnection;131;0;125;2
WireConnection;131;1;129;0
WireConnection;136;0;132;0
WireConnection;136;1;137;0
WireConnection;132;0;125;2
WireConnection;138;0;139;0
WireConnection;138;1;140;0
WireConnection;138;2;135;0
WireConnection;135;0;131;0
WireConnection;135;1;136;0
WireConnection;142;0;46;0
WireConnection;142;1;143;0
ASEEND*/
//CHKSM=6A270B60756E8D9D5A25A8AB9308039C61162EBE