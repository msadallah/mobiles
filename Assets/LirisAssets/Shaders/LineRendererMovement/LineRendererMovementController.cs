﻿using System.Collections.Generic;
using UnityEngine;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif
using System;

[RequireComponent(typeof(LineRenderer))]
[ExecuteAlways]
public class LineRendererMovementController : MonoBehaviour
{
	/// <summary>
	/// all the material sets that can be used
	/// </summary>
	private static Dictionary<Material,(Material twoSets, Material threeSets)> s_Materials = new Dictionary<Material, (Material twoSets, Material threeSets)>();

	/// <summary>
	/// list of the already existing/in use material property blocks
	/// </summary>
	private static List<MaterialPropertyBlockGroup> materialPropertyBlockGroups = new List<MaterialPropertyBlockGroup>();


	public static void ClearStatic()
	{
		s_Materials.Clear();
		materialPropertyBlockGroups.Clear();
	}
	/// <summary>
	/// does this render one, two or three colors
	/// </summary>
	public enum ColorCount
	{
		OneSet,
		TwoSets,
		ThreeSets
	}
	public ColorCount colorCount;

	/// colors per set
	public Color colorASet1 => _colorASet1;
	public Color colorBSet1 => _colorBSet1;

	public Color colorASet2 => _colorASet2;
	public Color colorBSet2 => _colorBSet2;

	public Color colorASet3 => _colorASet3;
	public Color colorBSet3 => _colorBSet3;

	public float opacity => _opacity;

	private LineRenderer _lineRenderer;
	[SerializeField] private Color _colorASet1;
	[SerializeField] private Color _colorBSet1;

#if ODIN_INSPECTOR
	[SerializeField, ShowIf("EDITORShowSet2")] private Color _colorASet2;
	[SerializeField, ShowIf("EDITORShowSet2")] private Color _colorBSet2;

	[SerializeField, ShowIf("EDITORShowSet3")] private Color _colorASet3;
	[SerializeField, ShowIf("EDITORShowSet3")] private Color _colorBSet3;
#else
	[SerializeField] private Color _colorASet2;
	[SerializeField] private Color _colorBSet2;

	[SerializeField] private Color _colorASet3;
	[SerializeField] private Color _colorBSet3;
#endif


	[Range(0f,1f),SerializeField] private float _opacity = 1;
	private Material[] _originalMaterials;

	// Start is called before the first frame update
	void Awake()
	{
		if (_lineRenderer == null)
		{
			_lineRenderer = GetComponent<LineRenderer>();
		}
		if(_originalMaterials==null)
			_originalMaterials = _lineRenderer.sharedMaterials;

		//the materials have never been set
		InitializeStaticMaterials();

		UpdateMaterial();
	}

	private void InitializeStaticMaterials()
	{
		for(int i=0;i<_originalMaterials.Length;++i)
		{
			if(!s_Materials.ContainsKey(_originalMaterials[i]))
			{
				//we need to create the variants for this material
				var twoSetsMaterial = Instantiate(_originalMaterials[i]);
				var threeSetsMaterial = Instantiate(_originalMaterials[i]);
				twoSetsMaterial.EnableKeyword("_COLORSCOUNTTWOSETS");
				threeSetsMaterial.EnableKeyword("_COLORSCOUNTTHREESETS");
				s_Materials.Add(_originalMaterials[i], (twoSetsMaterial, threeSetsMaterial))
;			}
		}
	}

	public void SetOpacity(float opacity)
	{
		_opacity = opacity;
		UpdateMaterial();
	}
	/// <summary>
	/// set one set of colors
	/// </summary>
	public void SetColors(Color color1, Color color2)
	{
		colorCount = ColorCount.OneSet;
		_colorASet1 = color1;
		_colorBSet1 = color2;
		UpdateMaterial();
	}

	/// <summary>
	/// set one set of colors
	/// </summary>
	public void SetColors(Color colorASet1, Color colorBSet1, Color colorASet2, Color colorBSet2)
	{
		colorCount = ColorCount.TwoSets;
		_colorASet1 = colorASet1;
		_colorBSet1 = colorBSet1;
		_colorASet2 = colorASet2;
		_colorBSet2 = colorBSet2;
		UpdateMaterial();
	}

	/// <summary>
	/// set one set of colors
	/// </summary>
	public void SetColors(Color colorASet1, Color colorBSet1, Color colorASet2, Color colorBSet2, Color colorASet3, Color colorBSet3)
	{
		colorCount = ColorCount.ThreeSets;
		_colorASet1 = colorASet1;
		_colorBSet1 = colorBSet1;
		_colorASet2 = colorASet2;
		_colorBSet2 = colorBSet2;
		_colorASet3 = colorASet3;
		_colorBSet3 = colorBSet3;
		UpdateMaterial();
	}

	private void UpdateMaterial()
	{
		if (_lineRenderer == null)
		{
			Awake();
		}
		InitializeStaticMaterials();
		MaterialPropertyBlockGroup block = GetPropertyBlock();
		block.Init(this);

		//get the materials
		Material [] materials = new Material[_originalMaterials.Length];
		for(int i=0;i<materials.Length;++i)
		{
			switch (colorCount)
			{
				case ColorCount.OneSet: materials[i] = _originalMaterials[i]; break;
				case ColorCount.TwoSets: materials[i] = s_Materials[_originalMaterials[i]].twoSets; break;
				case ColorCount.ThreeSets: materials[i] = s_Materials[_originalMaterials[i]].threeSets; break;
			}
		}
		//set the materials
		_lineRenderer.sharedMaterials = materials;
	}

	private MaterialPropertyBlockGroup GetPropertyBlock()
	{
		foreach(var mpb in materialPropertyBlockGroups)
		{
			switch(colorCount)
			{
				case ColorCount.OneSet:
					if (mpb.Matches(colorASet1, colorBSet1, opacity))
						return mpb;
					break;
				case ColorCount.TwoSets:
					if (mpb.Matches(colorASet1, colorBSet1, colorASet2, colorBSet2, opacity))
						return mpb;
					break;
				case ColorCount.ThreeSets:
					if (mpb.Matches(colorASet1, colorBSet1, colorASet2, colorBSet2, colorASet3, colorBSet3, opacity))
						return mpb;
					break;
			}
		}
		var newmpb = new MaterialPropertyBlockGroup();
		materialPropertyBlockGroups.Add(newmpb);
		return newmpb;
	}

	private void OnValidate()
	{
		if(_lineRenderer != null)
			UpdateMaterial();
	}

#if UNITY_EDITOR
	private bool EDITORShowSet2()
	{
		return colorCount != ColorCount.OneSet;
	}
	private bool EDITORShowSet3()
	{
		return colorCount == ColorCount.ThreeSets;
	}
#endif

	private class MaterialPropertyBlockGroup
	{
		private Color A1;
		private Color B1;
		private Color A2;
		private Color B2;
		private Color A3;
		private Color B3;
		private float opacity;
		public MaterialPropertyBlock _materialPropertyBlock = new MaterialPropertyBlock();
		public bool Matches(Color A1, Color B1, float opacity)
		{
			return A1 == this.A1 && B1 == this.B1 && opacity == this.opacity;
		}
		public bool Matches(Color A1, Color B1, Color A2, Color B2, float opacity)
		{
			return Matches(A1, B1, opacity) && A2 == this.A2 && B2 == this.B2;
		}
		public bool Matches(Color A1, Color B1, Color A2, Color B2, Color A3, Color B3, float opacity)
		{
			return Matches(A1, B1,A2,B2, opacity) && A3 == this.A3 && B3 == this.B3;
		}

		public void Init(LineRendererMovementController lineRendererMovementController)
		{
			lineRendererMovementController._lineRenderer.GetPropertyBlock(_materialPropertyBlock);

			//set the colors
			_materialPropertyBlock.SetColor("_Color", lineRendererMovementController._colorASet1);
			A1 = lineRendererMovementController.colorASet1;

			_materialPropertyBlock.SetColor("_Color2", lineRendererMovementController._colorBSet1);
			B1 = lineRendererMovementController.colorBSet1;

			if (lineRendererMovementController.colorCount != ColorCount.OneSet)
			{
				_materialPropertyBlock.SetColor("_ColorSet2_1", lineRendererMovementController._colorASet2);
				A2 = lineRendererMovementController.colorASet2;

				_materialPropertyBlock.SetColor("_ColorSet2_2", lineRendererMovementController._colorBSet2);
				B2 = lineRendererMovementController.colorBSet2;

				if (lineRendererMovementController.colorCount == ColorCount.ThreeSets)
				{
					_materialPropertyBlock.SetColor("_ColorSet3_1", lineRendererMovementController._colorASet3);
					A3 = lineRendererMovementController.colorASet3;

					_materialPropertyBlock.SetColor("_ColorSet3_2", lineRendererMovementController._colorBSet3);
					B3 = lineRendererMovementController.colorASet3;
				}
			}
			//set the opacity
			_materialPropertyBlock.SetFloat("_Opacity", lineRendererMovementController._opacity);
			opacity = lineRendererMovementController._opacity;

			lineRendererMovementController._lineRenderer.SetPropertyBlock(_materialPropertyBlock);
		}
	}
}
