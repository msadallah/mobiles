﻿namespace Ubiant.Common.Shader
{
	using UnityEngine;

	public static class DistanceTransparency
	{
		public static float currentNear => _currentNear;
		public static float currentFar => _currentFar;

		private const float DEFAULT_FAR = .88f;
		private const float DEFAULT_NEAR = .8f;
		private static int _nearId = Shader.PropertyToID("_NearDistanceTransparency");
		private static int _farId = Shader.PropertyToID("_FarDistanceTransparency");
		private static float _currentNear = DEFAULT_NEAR;
		private static float _currentFar = DEFAULT_FAR;

		/// <summary>
		/// sets the distance in meters from the camera for the transparency to start
		/// </summary>
		/// <param name="near">distance</param>
		public static void SetNear(float near)
		{
			_currentNear = near;
			Shader.SetGlobalFloat(_nearId, near);
		}
		/// <summary>
		/// sets the distance in meters from the camera for the transparency to end
		/// </summary>
		/// <param name="far">distance</param>
		public static void SetFar(float far)
		{
			_currentFar = far;
			Shader.SetGlobalFloat(_farId, far);
		}
		/// <summary>
		/// sets the distance in meters from the camera for the transparency to start and end
		/// </summary>
		/// <param name="near">distance to start</param>
		/// <param name="far">distance to end</param>
		public static void Set(float near, float far)
		{
			SetNear(near);
			SetFar(far);
		}

		/// <summary>
		/// sets the distance in meters for the transparency
		/// </summary>
		/// <param name="center">where the transparency is at 50%</param>
		/// <param name="distance">how large the transparency fade is</param>
		public static void SetDistance(float center, float distance)
		{
			float near = center - distance * 0.5f;

			float far = near+distance;

			Set(near, far);//wherever you are
		}
	}
}