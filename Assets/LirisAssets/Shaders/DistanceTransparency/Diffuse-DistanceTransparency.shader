// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Ubiant/Angle Lit/Distance Transparency"
{
	Properties
	{
		_MaxOpacity("MaxOpacity", Range( 0 , 1)) = 1
		[IntRange][Enum(UnityEngine.Rendering.CullMode)]_Culling("Culling", Range( 0 , 2)) = 2
		_MainTex("MainTex", 2D) = "white" {}
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_MultiplyColor("MultiplyColor", Color) = (1,1,1,1)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Geometry-1" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull [_Culling]
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 2.0
		struct Input
		{
			float3 worldNormal;
			float3 worldPos;
			float2 uv_texcoord;
			float4 screenPosition;
		};

		uniform float _Culling;
		uniform float4 _MultiplyColor;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _MaxOpacity;
		uniform float _NearDistanceTransparency;
		uniform float _FarDistanceTransparency;
		uniform float _Cutoff = 0.5;


		inline float Dither8x8Bayer( int x, int y )
		{
			const float dither[ 64 ] = {
				 1, 49, 13, 61,  4, 52, 16, 64,
				33, 17, 45, 29, 36, 20, 48, 32,
				 9, 57,  5, 53, 12, 60,  8, 56,
				41, 25, 37, 21, 44, 28, 40, 24,
				 3, 51, 15, 63,  2, 50, 14, 62,
				35, 19, 47, 31, 34, 18, 46, 30,
				11, 59,  7, 55, 10, 58,  6, 54,
				43, 27, 39, 23, 42, 26, 38, 22};
			int r = y * 8 + x;
			return dither[r] / 64; // same # of instructions as pre-dividing due to compiler magic
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float4 ase_screenPos = ComputeScreenPos( UnityObjectToClipPos( v.vertex ) );
			o.screenPosition = ase_screenPos;
		}

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float temp_output_15_0_g95 = 0.5;
			float temp_output_16_0_g95 = 1.0;
			float3 temp_output_53_0_g95 = mul( unity_CameraToWorld, float4( float3(0,0,1) , 0.0 ) ).xyz;
			float3 break82_g95 = temp_output_53_0_g95;
			float3 appendResult81_g95 = (float3(break82_g95.x , 0.001 , break82_g95.z));
			float3 normalizeResult85_g95 = normalize( appendResult81_g95 );
			float smoothstepResult113_g95 = smoothstep( 0.95 , 1.0 , abs( break82_g95.y ));
			float3 lerpResult114_g95 = lerp( normalizeResult85_g95 , temp_output_53_0_g95 , smoothstepResult113_g95);
			float3 ase_worldNormal = i.worldNormal;
			float3 ase_normWorldNormal = normalize( ase_worldNormal );
			float3 appendResult84_g95 = (float3(ase_normWorldNormal.x , 0.001 , ase_normWorldNormal.z));
			float3 normalizeResult86_g95 = normalize( appendResult84_g95 );
			float smoothstepResult106_g95 = smoothstep( 0.95 , 1.0 , abs( ase_normWorldNormal.y ));
			float3 lerpResult109_g95 = lerp( normalizeResult86_g95 , ase_normWorldNormal , smoothstepResult106_g95);
			float dotResult44_g95 = dot( lerpResult114_g95 , lerpResult109_g95 );
			float temp_output_1_0_g96 = 0.4;
			float lerpResult14_g95 = lerp( temp_output_15_0_g95 , temp_output_16_0_g95 , ( ( ( abs( acos( dotResult44_g95 ) ) / UNITY_PI ) - temp_output_1_0_g96 ) / ( 1.0 - temp_output_1_0_g96 ) ));
			float clampResult102_g95 = clamp( lerpResult14_g95 , temp_output_15_0_g95 , temp_output_16_0_g95 );
			float3 ase_worldPos = i.worldPos;
			float distanceToCamera40 = distance( ase_worldPos , _WorldSpaceCameraPos );
			float clampResult41_g95 = clamp( ( distanceToCamera40 / 35.0 ) , 0.0 , 1.0 );
			float lerpResult30_g95 = lerp( 1.0 , 0.86 , clampResult41_g95);
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 color165 = IsGammaSpace() ? float4(0.8,0.8,0.8,0) : float4(0.6038274,0.6038274,0.6038274,0);
			float4 color160 = IsGammaSpace() ? float4(0.3098039,0.3098039,0.3098039,0) : float4(0.07818741,0.07818741,0.07818741,0);
			float temp_output_170_0 = (0.0 + (ase_normWorldNormal.y - -1.0) * (1.0 - 0.0) / (1.0 - -1.0));
			float4 lerpResult163 = lerp( color165 , color160 , temp_output_170_0);
			float4 lerpResult180 = lerp( ( 1.5 * lerpResult163 ) , lerpResult163 , temp_output_170_0);
			float smoothstepResult177 = smoothstep( 0.95 , 0.955 , abs( ase_normWorldNormal.y ));
			float4 lerpResult175 = lerp( ( _MultiplyColor * ( ( clampResult102_g95 * lerpResult30_g95 ) * tex2D( _MainTex, uv_MainTex ) ) ) , lerpResult180 , smoothstepResult177);
			o.Emission = lerpResult175.rgb;
			o.Alpha = 1;
			float4 ase_screenPos = i.screenPosition;
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 clipScreen1 = ase_screenPosNorm.xy * _ScreenParams.xy;
			float dither1 = Dither8x8Bayer( fmod(clipScreen1.x, 8), fmod(clipScreen1.y, 8) );
			float Near32 = _NearDistanceTransparency;
			float Far30 = _FarDistanceTransparency;
			float clampResult53 = clamp( ( ( distanceToCamera40 - Near32 ) / ( Far30 - Near32 ) ) , 0.0 , 1.0 );
			float lerpResult57 = lerp( 0.0 , _MaxOpacity , clampResult53);
			dither1 = step( dither1, lerpResult57 );
			clip( dither1 - _Cutoff );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Unlit keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 customPack2 : TEXCOORD2;
				float3 worldPos : TEXCOORD3;
				float3 worldNormal : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.customPack2.xyzw = customInputData.screenPosition;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				surfIN.screenPosition = IN.customPack2.xyzw;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Legacy Shaders/Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18301
966;123;1225;927;429.8424;1353.793;2.116289;True;False
Node;AmplifyShaderEditor.CommentaryNode;41;-476.0917,-282.0757;Inherit;False;800.8771;385.0001;;4;19;20;21;40;Distance To camera;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;42;-141.7267,-639.8757;Inherit;False;594.3772;268.4322;;4;30;32;11;9;User Variables;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldPosInputsNode;19;-376.3917,-232.0758;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceCameraPos;20;-440.3915,-76.07547;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;11;-92.81584,-576.2037;Float;False;Global;_FarDistanceTransparency;_FarDistanceTransparency;3;0;Create;True;0;0;True;0;False;0.88;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-101.5095,-483.517;Float;False;Global;_NearDistanceTransparency;_NearDistanceTransparency;2;0;Create;True;0;0;True;0;False;0.8;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;21;-117.3925,-104.0755;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;40;51.78503,-50.04335;Float;False;distanceToCamera;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;30;191.0213,-575.9435;Float;False;Far;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;32;192.7558,-483.5648;Float;False;Near;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;166;608,-704;Inherit;False;967.7542;670.7765;Compute a color for Top wall & roof;8;181;179;180;170;162;163;160;165;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;52;-89.96093,218.4812;Inherit;False;741.1439;305.9231;;5;50;45;51;48;49;Inverse Lerp;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;45;-27.45957,409.4049;Inherit;False;32;Near;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;51;-27.0042,337.58;Inherit;False;30;Far;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;157;-47.58747,112.9348;Inherit;False;40;distanceToCamera;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;162;656,-208;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleSubtractOpNode;48;327.1207,268.4814;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;165;656,-640;Inherit;False;Constant;_RoofColor;RoofColor;4;0;Create;True;0;0;False;0;False;0.8,0.8,0.8,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;160;656,-464;Inherit;False;Constant;_TopWallColor;TopWallColor;4;0;Create;True;0;0;False;0;False;0.3098039,0.3098039,0.3098039,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;50;299.8453,385.539;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;167;608,-1184;Inherit;False;908.4821;461.6628;Compute a different color base on angle normals;4;188;184;148;137;;1,1,1,1;0;0
Node;AmplifyShaderEditor.TFHCRemapNode;170;896,-288;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;-1;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;148;656,-832;Inherit;False;40;distanceToCamera;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;137;656,-1040;Inherit;True;Property;_MainTex;MainTex;2;0;Create;True;0;0;False;0;False;-1;None;e69b8317041ea3748a98c94f50060edd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;181;1037.035,-624;Inherit;False;Constant;_LightMultiplier;LightMultiplier;4;0;Create;True;0;0;False;0;False;1.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;163;1088,-416;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;161;1616,-560;Inherit;False;623.8705;426.8513;Assign a different color for top wall & roof;3;175;177;178;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;49;494.1827,336.6704;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;204;1008,-912;Inherit;False;WallAlbedo;-1;;94;00f20638cc3ff814bb415742ea938c9a;0;2;25;COLOR;0,0,0,0;False;24;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;53;811.1842,309.1136;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;178;1632,-368;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;59;684.4394,189.1006;Float;False;Property;_MaxOpacity;MaxOpacity;0;0;Create;True;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;179;1232,-624;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;184;1104,-1120;Inherit;False;Property;_MultiplyColor;MultiplyColor;4;0;Create;True;0;0;False;0;False;1,1,1,1;0.8396226,0.8396226,0.8396226,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;57;1085.661,223.7751;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;180;1376,-480;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;188;1360,-1008;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;177;1792,-368;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.95;False;2;FLOAT;0.955;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;175;2064,-496;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;62;-366.8358,506.741;Float;False;Property;_Culling;Culling;1;2;[IntRange];[Enum];Create;True;0;1;UnityEngine.Rendering.CullMode;True;0;False;2;2;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.DitheringNode;1;1319.55,167.5001;Inherit;False;1;False;3;0;FLOAT;0;False;1;SAMPLER2D;;False;2;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2484.583,-272.4767;Float;False;True;-1;0;ASEMaterialInspector;0;0;Unlit;Ubiant/Angle Lit/Distance Transparency;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;56;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;-1;True;TransparentCutout;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;Legacy Shaders/Diffuse;3;-1;-1;-1;0;False;0;0;True;62;-1;0;False;145;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;21;0;19;0
WireConnection;21;1;20;0
WireConnection;40;0;21;0
WireConnection;30;0;11;0
WireConnection;32;0;9;0
WireConnection;48;0;157;0
WireConnection;48;1;45;0
WireConnection;50;0;51;0
WireConnection;50;1;45;0
WireConnection;170;0;162;2
WireConnection;163;0;165;0
WireConnection;163;1;160;0
WireConnection;163;2;170;0
WireConnection;49;0;48;0
WireConnection;49;1;50;0
WireConnection;204;25;137;0
WireConnection;204;24;148;0
WireConnection;53;0;49;0
WireConnection;178;0;162;2
WireConnection;179;0;181;0
WireConnection;179;1;163;0
WireConnection;57;1;59;0
WireConnection;57;2;53;0
WireConnection;180;0;179;0
WireConnection;180;1;163;0
WireConnection;180;2;170;0
WireConnection;188;0;184;0
WireConnection;188;1;204;0
WireConnection;177;0;178;0
WireConnection;175;0;188;0
WireConnection;175;1;180;0
WireConnection;175;2;177;0
WireConnection;1;0;57;0
WireConnection;0;2;175;0
WireConnection;0;10;1;0
ASEEND*/
//CHKSM=3B7D6587D56A7C5D7A9EDB3CBD8041FAE8257696