// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Ubiant/Diffuse/WhiteWallNoCut"
{
	Properties
	{
		[PerRendererData]_TopColor("TopColor", Color) = (1,1,1,1)
		[PerRendererData]_StateColor("StateColor", Color) = (1,1,1,1)
		_WallColor("WallColor", Color) = (0,0,0,0)
		[PerRendererData]_LockLevel("LockLevel", Range( 0 , 0.5)) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" }
		Cull Back
		AlphaToMask On
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
			half ASEVFace : VFACE;
			float4 vertexColor : COLOR;
		};

		uniform float4 _WallColor;
		uniform float4 _TopColor;
		uniform float4 _StateColor;
		uniform float _LockLevel;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float3 temp_cast_0 = (( 1.0 + ( 0.002 * ase_vertex3Pos.y ) )).xxx;
			float4 lerpResult221 = lerp( ( _TopColor * _StateColor ) , float4( 1,1,1,1 ) , _LockLevel);
			float temp_output_15_0_g162 = 0.5;
			float temp_output_16_0_g162 = 1.0;
			float3 temp_output_53_0_g162 = mul( unity_CameraToWorld, float4( float3(0,0,1) , 0.0 ) ).xyz;
			float3 break82_g162 = temp_output_53_0_g162;
			float3 appendResult81_g162 = (float3(break82_g162.x , 0.001 , break82_g162.z));
			float3 normalizeResult85_g162 = normalize( appendResult81_g162 );
			float smoothstepResult113_g162 = smoothstep( 0.95 , 1.0 , abs( break82_g162.y ));
			float3 lerpResult114_g162 = lerp( normalizeResult85_g162 , temp_output_53_0_g162 , smoothstepResult113_g162);
			float3 ase_worldNormal = i.worldNormal;
			float3 ase_normWorldNormal = normalize( ase_worldNormal );
			float3 switchResult121_g162 = (((i.ASEVFace>0)?(ase_normWorldNormal):(( float3( -1,-1,-1 ) * ase_normWorldNormal ))));
			float3 Normal125_g162 = switchResult121_g162;
			float3 break127_g162 = Normal125_g162;
			float3 appendResult84_g162 = (float3(break127_g162.x , 0.001 , break127_g162.z));
			float3 normalizeResult86_g162 = normalize( appendResult84_g162 );
			float smoothstepResult106_g162 = smoothstep( 0.95 , 1.0 , abs( break127_g162.y ));
			float3 lerpResult109_g162 = lerp( normalizeResult86_g162 , Normal125_g162 , smoothstepResult106_g162);
			float dotResult44_g162 = dot( lerpResult114_g162 , lerpResult109_g162 );
			float temp_output_1_0_g163 = 0.4;
			float lerpResult14_g162 = lerp( temp_output_15_0_g162 , temp_output_16_0_g162 , ( ( ( abs( acos( dotResult44_g162 ) ) / UNITY_PI ) - temp_output_1_0_g163 ) / ( 1.0 - temp_output_1_0_g163 ) ));
			float clampResult102_g162 = clamp( lerpResult14_g162 , temp_output_15_0_g162 , temp_output_16_0_g162 );
			float3 ase_worldPos = i.worldPos;
			float clampResult41_g162 = clamp( ( distance( _WorldSpaceCameraPos , ase_worldPos ) / 35.0 ) , 0.0 , 1.0 );
			float lerpResult30_g162 = lerp( 1.0 , 0.86 , clampResult41_g162);
			float4 temp_cast_5 = (i.vertexColor.g).xxxx;
			float4 lerpResult159 = lerp( _WallColor , temp_cast_5 , float4( 0,0,0,0 ));
			float4 lerpResult153 = lerp( lerpResult221 , ( ( clampResult102_g162 * lerpResult30_g162 ) * lerpResult159 ) , i.vertexColor.g);
			float temp_output_2_0_g164 = lerpResult153.r;
			float temp_output_3_0_g164 = ( 1.0 - temp_output_2_0_g164 );
			float3 appendResult7_g164 = (float3(temp_output_3_0_g164 , temp_output_3_0_g164 , temp_output_3_0_g164));
			o.Albedo = ( _WallColor * float4( ( ( temp_cast_0 * temp_output_2_0_g164 ) + appendResult7_g164 ) , 0.0 ) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD1;
				float3 worldNormal : TEXCOORD2;
				half4 color : COLOR0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.color = v.color;
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				surfIN.vertexColor = IN.color;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18935
95;555;1920;1019;-645.5757;-105.0038;1.075592;True;True
Node;AmplifyShaderEditor.CommentaryNode;195;784,64;Inherit;False;1035.503;693.9539;Set the color of the wall mesh;8;152;221;137;219;22;138;290;293;Coloration;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;163;784,-448;Inherit;False;605.6825;458.3387;Apply the Albedo only on wall sides;3;158;154;159;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;154;832,-400;Inherit;False;Property;_WallColor;WallColor;8;0;Create;True;0;0;0;False;0;False;0,0,0,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;158;912,-192;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;138;832,304;Inherit;False;Property;_StateColor;StateColor;7;1;[PerRendererData];Create;True;0;0;0;False;0;False;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;22;832,128;Float;False;Property;_TopColor;TopColor;0;1;[PerRendererData];Create;True;0;0;0;False;0;False;1,1,1,1;1,1,1,1;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;137;1108.652,256;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;219;1024,656;Inherit;False;Property;_LockLevel;LockLevel;11;1;[PerRendererData];Create;True;0;0;0;False;0;False;0;0;0;0.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;293;1692.195,484.7376;Inherit;False;Constant;_Float1;Float 1;12;0;Create;True;0;0;0;False;0;False;0.002;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;290;1637.146,591.087;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;159;1200,-176;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;286;1472,-176;Inherit;False;WallAlbedo;-1;;161;00f20638cc3ff814bb415742ea938c9a;0;2;25;COLOR;0,0,0,0;False;24;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;289;1843.289,382.0202;Inherit;False;Constant;_Float0;Float 0;12;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;292;1868.195,656.7376;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;152;1408,560;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;221;1392,368;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;1,1,1,1;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;112;-4853.469,-1925.04;Inherit;False;2152.462;1211.42;World UVs;18;56;57;55;45;53;106;107;50;67;46;49;68;51;65;66;62;63;52;;1,1,1,1;0;0
Node;AmplifyShaderEditor.LerpOp;153;1851.262,127.727;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;291;2033.195,356.7376;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;57;-3122.007,-1875.04;Inherit;False;371;280;zDiff;1;32;;1,1,1,1;0;0
Node;AmplifyShaderEditor.FunctionNode;288;1983.729,11.35514;Inherit;False;Lerp White To;-1;;164;047d7c189c36a62438973bad9d37b1c2;0;2;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;42;-1599.373,-1772.586;Inherit;False;284;257;Top/Bottom color albedo;1;40;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;126;-1932.39,-980.6438;Inherit;False;752.7186;478.8928;top or side?;4;122;123;124;121;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;29;-4433.972,-658.9631;Inherit;False;1529.44;281.6487;Plane detection;5;84;24;4;3;1;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;56;-3123.461,-1572.312;Inherit;False;371;280;xDiff;1;54;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;108;-4884.32,-2299.1;Inherit;False;528.5518;358.1757;Clipping;4;135;103;105;104;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;149;678.0279,914.8295;Inherit;False;1143.824;503.4043;We use the VertexColor info to offset a vertex by the property OffsetWeight on the Y axis;10;211;216;214;215;165;212;213;207;209;208;Vertex Color Offset;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;209;979.1916,1182.451;Inherit;False;Property;_MaxHeight;MaxHeight;10;1;[PerRendererData];Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;208;734.8373,1049.391;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;213;968.511,1283.362;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;165;1265.365,1070.339;Inherit;False;Property;_IndexOffset;IndexOffset;9;1;[PerRendererData];Create;True;0;0;0;False;0;False;0;-0.25;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;212;994.4156,986.1639;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;207;1142.628,1119.683;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;216;1445.943,1097.909;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;214;1549.607,1281.394;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;215;1536.695,990.8286;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;60;-2304.456,-1602.578;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;52;-3227.15,-1194.44;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;51;-3368.012,-1082.33;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCGrayscale;121;-1877.39,-929.751;Inherit;True;0;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;32;-3052.007,-1825.04;Inherit;True;Property;_MainTex;side Texture;1;0;Create;False;0;0;0;False;0;False;-1;None;e69b8317041ea3748a98c94f50060edd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;84;-3178.911,-628.0272;Float;True;SaturatedPlaneDetection;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;66;-3504.668,-1764.771;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PowerNode;50;-3811.84,-1126.183;Inherit;False;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;105;-4862.245,-2256.741;Float;False;Property;_ClippedHeight;_ClippedHeight;5;1;[PerRendererData];Create;True;0;0;0;False;0;False;0;3.24;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;85;-1461.96,-1452.749;Inherit;True;84;SaturatedPlaneDetection;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwitchByFaceNode;132;-209.7802,-1303.236;Inherit;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;-2647.567,-1718.273;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;118;-1098.108,-824.3998;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;65;-3480.473,-1517.573;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;46;-3889.119,-1759.051;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;67;-3698.462,-1635.914;Float;False;Property;_SideScale;SideScale;3;0;Create;True;0;0;0;False;0;False;0;0.23;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;62;-3340.53,-1764.207;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.IntNode;135;-4818.329,-2014.999;Float;False;Property;_ClippingOn;_ClippingOn;6;1;[PerRendererData];Create;True;0;0;0;False;0;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.ConditionalIfNode;122;-1671.39,-925.751;Inherit;True;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;106;-4398.99,-1657.686;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;INT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BreakToComponentsNode;4;-3933.621,-611.677;Inherit;True;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.BreakToComponentsNode;107;-4189.334,-1655.651;Inherit;True;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;123;-1854.39,-619.7512;Float;False;Constant;_Zero;Zero;9;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;1;-4422.439,-607.84;Inherit;True;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FractNode;63;-3321.497,-1512.012;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;211;1632.372,1073.294;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;134;-505.1236,-1202.284;Float;False;Constant;_Black;Black;7;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;55;-3042.724,-1204.686;Inherit;True;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;58;-2668.432,-1499.961;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;124;-1864.39,-722.7511;Float;False;Constant;_One;One;9;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;53;-4086.817,-1107.002;Float;False;Property;_UVWorldSharpness;UVWorldSharpness;2;0;Create;True;0;0;0;False;0;False;0;200;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;54;-3073.461,-1522.312;Inherit;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Instance;32;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-3692.026,-605.1294;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;103;-4599.47,-2250.4;Half;False;if(clipping<1)return 1@$ ///distance clipping$float distance = dot(worldPos, _Plane.xyz)@$distance += _Plane.w@$$//distance is <=0 if it's behind the plane, >0 if it's in front of it$if (distance <= 0 && worldPos.y>_ClippedHeight)${$	discard@$}$return 1@;0;Create;4;True;clippedHeight;FLOAT;0;In;;Float;False;True;_Plane;FLOAT4;0,0,0,0;In;;Float;False;True;worldPos;FLOAT3;0,0,0;In;;Float;False;True;clipping;INT;0;In;;Float;False;Clipping (returns 1);True;False;0;;False;4;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT3;0,0,0;False;3;INT;0;False;1;INT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;45;-4803.469,-1700.93;Float;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SwitchByFaceNode;95;-769.2914,-1457.402;Inherit;True;2;0;COLOR;0,0,0,0;False;1;COLOR;1,1,1,1;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;49;-3869.87,-1531.079;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.BreakToComponentsNode;68;-3656.108,-1063.893;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.DynamicAppendNode;129;-2490.547,-1019.935;Inherit;True;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.AbsOpNode;3;-4173.553,-610.0991;Inherit;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;40;-1549.373,-1722.586;Float;False;Constant;_Color0;Color 0;2;0;Create;True;0;0;0;False;0;False;0,0,0,1;0,0,0,0;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector4Node;104;-4868.691,-2182.764;Float;False;Property;_Plane;_Plane;4;1;[PerRendererData];Create;True;0;0;0;False;0;False;0,0,0,0;0,-0.2544242,0.9670928,0.8243346;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;39;-1075.712,-1516.856;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;294;1949.545,-149.9456;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2190.645,195.7993;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Ubiant/Diffuse/WhiteWallNoCut;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;139;False;0;True;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Absolute;0;;-1;-1;-1;-1;0;True;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;137;0;22;0
WireConnection;137;1;138;0
WireConnection;159;0;154;0
WireConnection;159;1;158;2
WireConnection;286;25;159;0
WireConnection;292;0;293;0
WireConnection;292;1;290;2
WireConnection;221;0;137;0
WireConnection;221;2;219;0
WireConnection;153;0;221;0
WireConnection;153;1;286;0
WireConnection;153;2;152;2
WireConnection;291;0;289;0
WireConnection;291;1;292;0
WireConnection;288;1;291;0
WireConnection;288;2;153;0
WireConnection;213;0;208;3
WireConnection;212;0;208;1
WireConnection;207;0;208;2
WireConnection;207;1;209;0
WireConnection;216;0;165;0
WireConnection;216;1;207;0
WireConnection;214;0;213;0
WireConnection;215;0;212;0
WireConnection;60;0;55;0
WireConnection;52;0;50;0
WireConnection;52;1;51;0
WireConnection;51;0;68;0
WireConnection;51;1;68;1
WireConnection;51;2;68;2
WireConnection;121;0;129;0
WireConnection;32;1;62;0
WireConnection;84;0;24;0
WireConnection;66;0;46;0
WireConnection;66;1;67;0
WireConnection;50;0;3;0
WireConnection;50;1;53;0
WireConnection;132;0;95;0
WireConnection;132;1;134;0
WireConnection;118;0;122;0
WireConnection;65;0;67;0
WireConnection;65;1;49;0
WireConnection;46;0;107;0
WireConnection;46;1;107;1
WireConnection;62;0;66;0
WireConnection;122;0;121;0
WireConnection;122;2;124;0
WireConnection;122;3;123;0
WireConnection;122;4;123;0
WireConnection;106;0;45;0
WireConnection;106;1;103;0
WireConnection;4;0;3;0
WireConnection;107;0;106;0
WireConnection;63;0;65;0
WireConnection;211;0;215;0
WireConnection;211;1;216;0
WireConnection;211;2;214;0
WireConnection;55;0;52;0
WireConnection;54;1;63;0
WireConnection;24;0;4;0
WireConnection;24;1;4;2
WireConnection;103;0;105;0
WireConnection;103;1;104;0
WireConnection;103;2;45;0
WireConnection;103;3;135;0
WireConnection;95;0;39;0
WireConnection;49;0;107;2
WireConnection;49;1;107;1
WireConnection;68;0;50;0
WireConnection;129;0;55;0
WireConnection;3;0;1;0
WireConnection;39;0;40;0
WireConnection;39;1;60;0
WireConnection;39;2;85;0
WireConnection;294;0;154;0
WireConnection;294;1;288;0
WireConnection;0;0;294;0
ASEEND*/
//CHKSM=7D393D0E825EFB0E9CCCE3F528EC018A0AE706A7